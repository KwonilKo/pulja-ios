//
//  SettingChangeScoreViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/01/04.
//

import UIKit

class SettingChangeScoreViewController: UIViewController {

    
    @IBOutlet var mainView: UIView!
    
    
    @IBOutlet weak var topScore: UILabel!
    
    @IBOutlet weak var bottomScore: UILabel!
    
    @IBOutlet weak var setTitle: UILabel!
    
    var titleName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.mainView.roundedTop(10)
        }
        
        setTitle.text = titleName
    }
    
    @IBAction func close(_ sender: Any) {
        
        dismiss(animated: true) {
            
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
