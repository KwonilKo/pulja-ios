//
//  SettingUserInfoViewController.swift
//  pulja
//
//  Created by 고권일 on 2021/12/30.
//

import UIKit
import MaterialComponents.MaterialBottomSheet
import Alamofire
import ChannelIOFront

class SettingUserInfoViewController: PuljaBaseViewController, MDCBottomSheetControllerDelegate {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var id: UILabel!
    
    @IBOutlet weak var phone: UILabel!
    
    @IBOutlet weak var lbPhone: UILabel!
    @IBOutlet weak var birthday: UILabel!
    
    @IBOutlet weak var schoolInfo: UILabel!
    
    @IBOutlet weak var parentphone: UILabel!
    
    
    @IBOutlet var passwordFirstView: UIView!
    @IBOutlet var logoutStackView: UIStackView!
    @IBOutlet var getOutView: UIStackView!
    
    var userinfo: User?
    var schoolName : String = ""
    var schoolNum : String = ""
    var schoolType : String = ""
    var schoolId : String = ""
    
    
    @IBOutlet var labelName: UILabel!
    @IBOutlet var firstStackView: UIStackView!
    
    @IBOutlet var secondStackView: UIStackView!
    
    @IBOutlet var idPasswordView: UIView!
    
    @IBOutlet var logoutView: UIView!
    @IBOutlet var userInfoText: UILabel!
    @IBOutlet var userInfoSettingVIew: UIView!
    
    
    @IBOutlet var lineView: UIView!
    
    @IBOutlet var cellPhoneView: UIView!
    
    
    @IBOutlet weak var snsView: UIView!
    
    @IBOutlet weak var snsIcon: UIImageView!
    
    @IBOutlet weak var lbSnsType: UILabel!
    
    
    @IBOutlet var phoneSecondView: UIView!
    
    
    @IBOutlet var birthdaySecondView: UIView!
    
    
    @IBOutlet var gradeSecondView: UIView!
    
    
    @IBOutlet var parentPhoneSecondView: UIView!
    
    
    @IBOutlet var getOutLastView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        firstStackView.addHorizontalSeparators(color: .Foggy)
//        secondStackView.addHorizontalSeparators(color: .Foggy)
                // Do any additional setup after loading the view.
        
        showCoach()
    
    }
    
    func showCoach() {
        if let userType = self.myInfo?.userType, userType == "C" {
            //회원 정보와 관련된 view 삭제
            userInfoSettingVIew.isHidden = true
            userInfoText.isHidden = true
            self.logoutStackView.topAnchor.constraint(equalTo: self.idPasswordView.bottomAnchor, constant: 12).isActive = true
//            self.logoutView.isUserInteractionEnabled = true
            //탈퇴하기 view 삭제
            getOutView.isHidden = true

            
        } else {
            
            if let snsType =  self.myInfo?.snsType {
                
                if snsType == "a" {
                    self.snsIcon.image = UIImage(resource : R.image.iconApple)
                    lbSnsType.text = "Apple"
                    passwordFirstView.isHidden = true
                    lineView.isHidden = true
                } else if snsType == "n" {
                    self.snsIcon.image = UIImage(resource : R.image.iconNaver)
                    lbSnsType.text = "네이버"
                    passwordFirstView.isHidden = true
                    lineView.isHidden = true
                } else if snsType == "k" {
                    self.snsIcon.image = UIImage(resource : R.image.iconKakao)
                    lbSnsType.text = "카카오"
                    passwordFirstView.isHidden = true
                    lineView.isHidden = true
                } else {
                    snsIcon.isHidden = true
                    lbSnsType.text = "정보없음"
                    
                }
            } else {
                snsIcon.isHidden = true
                lbSnsType.text = "정보없음"
            }
            
            cellPhoneView.isHidden = true
            
        }
    }
    

   
    /*     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func changePassword(_ sender: Any) {
        UIView.animate(withDuration: 0.1, animations: {
            self.passwordFirstView.backgroundColor = UIColor.Foggy.withAlphaComponent(0.6)
        }, completion: {finished in
            self.passwordFirstView.backgroundColor = UIColor.white
            if let vc = R.storyboard.setting.settingPasswordViewController() {
            vc.userinfo = self.userinfo
            vc.authCallback = { (userinfo : User?) in
                self.alert(title: nil, message: "비밀번호가 변경되었습니다", button: "확인")
                self.viewDidLoad()
            }
            self.navigationController?.pushViewController(vc, animated:true)
            }
        })
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        CommonUtil.tabbarController?.tabBar.isHidden = true
        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        
        CommonUtil.coachTabbarController?.tabBar.isHidden = true
        CommonUtil.coachTabbarController?.bottomNavBar.isHidden = true
        
        LoadingView.show()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
            self.getMyInfo()
        }
    }
    
    private func getMyInfo() {
        UserAPI.shared.myInfo(userSeq: Int(myInfo!.userSeq)).done {
            res in
            if let bool = res.success, bool == true {
                guard let user = res.data else {
                    return
                }
                
                self.userinfo = user
                
                // 이름 뿌려주기
                var name : String = self.userinfo?.username ?? "정보 없음"
                if name == "" { name = "정보 없음"}
                
                self.name.text = name
                self.name.textColor = UIColor.black
                
                // 아이디 뿌려주기
                var userId : String = self.userinfo?.userId ?? "정보 없음"
                if userId == "" { userId = "정보 없음"}

                self.id.text = userId
                self.id.textColor = UIColor.black
               
                // 전화번호 뿌려주기
                var phonenumber : String = self.userinfo?.phonenumber ?? ""
                if phonenumber == "" {
                    phonenumber = "정보 없음"
                } else {
                    if phonenumber.count < 11 {
                        phonenumber = ""
                    } else {
                        phonenumber = "\(phonenumber.substring(from : 0, to : 2))-\(phonenumber.substring(from : 3, to : 6))-\(phonenumber.substring(from : 7, to : 10))"
                    }
                }
                
                self.phone.text = phonenumber
                self.phone.textColor = UIColor.black
                
                self.lbPhone.text = phonenumber
                self.lbPhone.textColor = UIColor.black

                
                // 생년월일 뿌려주기
                var birthday : String = self.userinfo?.birthday ?? ""
                if birthday == "" {
                    birthday = "정보 없음"
                } else {
                    birthday = "\(birthday.substring(from : 0, to : 3))년 \(birthday.substring(from : 4, to : 5))월 \(birthday.substring(from : 6, to : 7))일"
                }
                
                self.birthday.text = birthday
                self.birthday.textColor = UIColor.black
                
                // 학교/학년 뿌려주기
                var schoolName : String = self.userinfo?.schoolName ?? "정보 없음"
                if schoolName == "" { schoolName = "정보 없음"}
                self.schoolName = schoolName
                
                var schoolId : String = self.userinfo?.schoolId ?? "정보 없음"
                if schoolId == "" { schoolId = "정보 없음"}
                self.schoolId = schoolId
                
                
                var sg : Int = self.userinfo?.schoolNum ?? -1
                switch sg {
                case 1:
                    self.schoolNum = "1학년"
                case 2:
                    self.schoolNum = "2학년"
                case 3:
                    self.schoolNum = "3학년"
                case 4:
                    self.schoolNum = "검정고시"
                default:
                    self.schoolNum = "N수생"
                }
//                if let sg = self.userinfo?.schoolNum {
//                    self.schoolNum = "\(sg)학년"
//                } else {
//                    self.schoolNum = "정보 없음"
//                }
                
                if let stype = self.userinfo?.schoolType {
                    switch stype {
                    case "1" :
                        self.schoolType = "중학교"
                    case "2" :
                        self.schoolType = "고등학교"
                    default :
                        self.schoolType = ""
                    }
                }
                //학교, 학년 둘다 정보 없는 경우 -> 정보 없음이라고만 표기
                if (self.schoolName == "정보 없음" && self.schoolNum == "정보 없음") {
                    self.schoolInfo.text = "정보 없음"
                } else {
                    self.schoolInfo.text = "\(self.schoolName), \(self.schoolType) \(self.schoolNum)"
                self.schoolInfo.textColor = UIColor.black
                }
                
                // 부모님 전번 뿌려주기
                var parentsPhone : String = self.userinfo?.parentsPhone ?? ""
                if parentsPhone == "" {
                    parentsPhone = "정보 없음"
                } else {
                    parentsPhone = "\(parentsPhone.substring(from : 0, to : 2))-\(parentsPhone.substring(from : 3, to : 6))-\(parentsPhone.substring(from : 7, to : 10))"
                }
                
                self.parentphone.text = parentsPhone
                self.parentphone.textColor = UIColor.black
            }
        }.catch { error in
            
        }.finally {
            LoadingView.hide()
        }
    }
    
    
    
    @IBAction func changeParentPhone(_ sender: Any) {
        UIView.animate(withDuration: 0.1, animations: {
            self.parentPhoneSecondView.backgroundColor = UIColor.Foggy.withAlphaComponent(0.6)
        }, completion: {finished in
            self.parentPhoneSecondView.backgroundColor = UIColor.white
            if let vc = R.storyboard.setting.settingParentPhoneViewController()
            {

            //전화번호 뿌려주기
//            var phonenumber : String = self.userinfo?.phonenumber ?? "정보 없음"
//            if phonenumber == "" { phonenumber = "정보 없음"}
            
            
            vc.userPhone = self.parentphone.text!
            vc.userinfo = self.userinfo
            vc.authCallback = { (phonenumber: String?, userinfo: User? ) in
                
                //후처리
                self.alert(title: nil, message: "부모님 전화번호가 변경되었습니다", button: "확인")
                self.viewDidLoad()
                
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
            }})
    }
        
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        CommonUtil.tabbarController?.tabBar.isHidden = true
//        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        CommonUtil.tabbarController?.tabBar.isHidden = false
    
        CommonUtil.coachTabbarController?.tabBar.isHidden = false
        CommonUtil.coachTabbarController?.bottomNavBar.isHidden = false
    }
    
    
    @IBAction func changePhoneCoach(_ sender: Any) {
        UIView.animate(withDuration: 0.1, animations: {
            self.cellPhoneView.backgroundColor = UIColor.Foggy.withAlphaComponent(0.6)
        }, completion: {finished in
            self.cellPhoneView.backgroundColor = UIColor.white
            if let vc = R.storyboard.setting.settingPhoneViewController()
            {
                
                //전화번호 뿌려주기
    //            var phonenumber : String = self.userinfo?.phonenumber ?? "정보 없음"
    //            if phonenumber == "" { phonenumber = "정보 없음"}
                vc.userPhone = self.phone.text!
                vc.userinfo = self.userinfo
                
                vc.authCallback = { (phonenumber: String?, userinfo: User? ) in
                    
                    //후처리
                    self.alert(title: nil, message: "전화번호가 변경되었습니다", button: "확인")
                    self.viewDidLoad()
                    
                }
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        })
          
        
    }
    

    @IBAction func getOutAction(_ sender: Any) {
        UserAPI.shared.dropOutCheckValidReq(userSeq: Int(self.myInfo!.userSeq)).done { res in
            if let dropoutPossible = res.data?.dropoutPossible, dropoutPossible == true {
                if let dropoutUserType = res.data?.dropoutUserType {
                   if (dropoutUserType == "f") {
                        self.goDropOut();
                    } else {
                        self.goDropOut();
                    }   
                }
            } else {
                self.goDropOut()
            }
            
        }.catch { error in
            print(error.localizedDescription)
        }
    }
    
    func goDropOut(){
        UIView.animate(withDuration: 0.1, animations: {
           self.getOutLastView.backgroundColor = UIColor.Foggy.withAlphaComponent(0.6)
        }, completion: {finished in
           self.getOutLastView.backgroundColor = UIColor.white
           if let vc = R.storyboard.setting.outViewController() {
               self.navigationController?.pushViewController(vc, animated: true)
           }
        })
    }
    
    func notDropOut() {
            self.alert(title: "", message: "현재 수강중인 회원이 탈퇴를 원하시는\n경우, 풀자 고객 센터로 문의 부탁드립니다.", button: "탈퇴 신청하기", subButton: "닫기").done { b in
                if b {
                                    
                    UserAPI.shared.makeEnquireEventLog().done { res in
                       if let suc = res.success, suc == true {
                           self.initChennelTalk()
                       } else {
                           
                       }
                    }.catch { error in
                        print(error.localizedDescription)
                    }
                }
            }
        }
    
    func initChennelTalk(){
                
                guard let info = self.myInfo else { return }
                guard let userName = info.username else { return }
                guard let userId = info.hashUserSeq else { return }
                
                let profile = Profile()
                  .set(name: userName)


                let buttonOption = ChannelButtonOption.init(
                    position: .right,
                  xMargin: 16,
                  yMargin: 23
                )

                let bootConfig = BootConfig.init(
                    pluginKey: Const.CHANNELTALK_PLUGIN_KEY,
                  memberId: userId,
        //          memberHash: "",
                  profile: profile,
                  channelButtonOption: buttonOption,
                  hidePopup: false,
                  trackDefaultEvent: true,
                  language: .korean
                )

                ChannelIO.boot(with: bootConfig) { (bootStatus, user) in
                    if bootStatus == .success, let user = user {
                        // success
                        ChannelIO.showMessenger()
                    } else {
                        // show failed reason from bootStatus
                    }
                }
                

            }
    
    
    
    @IBAction func changePhone(_ sender: Any) {
        UIView.animate(withDuration: 0.1, animations: {
            self.phoneSecondView.backgroundColor = UIColor.Foggy.withAlphaComponent(0.6)
        }, completion: {finished in
            self.phoneSecondView.backgroundColor = UIColor.white
            if let vc = R.storyboard.setting.settingPhoneViewController()
            {
                
                //전화번호 뿌려주기
    //            var phonenumber : String = self.userinfo?.phonenumber ?? "정보 없음"
    //            if phonenumber == "" { phonenumber = "정보 없음"}
                vc.userPhone = self.phone.text!
                vc.userinfo = self.userinfo
                
                vc.authCallback = { (phonenumber: String?, userinfo: User? ) in
                    
                    //후처리
                    self.alert(title: nil, message: "전화번호가 변경되었습니다", button: "확인")
                    self.viewDidLoad()
                    
                }
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        })
          
    }
    
    
    
    
    @IBAction func ibBack(_ sender: Any) {
        print("여기 들어옴")
        print(self.navigationController)
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func logoutButton(_ sender: Any) {
        UIView.animate(withDuration: 0.1, animations: {
            self.logoutView.backgroundColor = UIColor.Foggy.withAlphaComponent(0.6)
        }, completion: {finished in
            self.logoutView.backgroundColor = UIColor.white
            CommonUtil.shared.logout()
        })
    }
    
    @IBAction func searchSchool(_ sender: Any) {
        UIView.animate(withDuration: 0.1, animations: {
            self.gradeSecondView.backgroundColor = UIColor.Foggy.withAlphaComponent(0.6)
        }, completion: {finished in
            self.gradeSecondView.backgroundColor = UIColor.white
            if let vc = R.storyboard.setting.settingSearchSchoolViewController() {
            // 학교, 학년 기존 정보 뿌려주기
            vc.userinfo = self.userinfo
            vc.sName = self.schoolName
            vc.sNum = "\(self.schoolNum)"
            vc.sType = self.schoolType
            vc.sId = self.schoolId
            
            vc.authCallback = { (sName : String?, sType: String?, sNum : String?, userinfo: User? ) in
                // 후처리
                guard let sname = sName else { return }
                guard let stype = sType else { return }
                guard let snum = sNum else { return }
                guard let user = userinfo else { return }
                
                self.schoolName = sname
                self.schoolType = stype
                self.schoolNum = snum
                self.userinfo = user
                
                self.schoolInfo.text = "\(sname), \(stype) \(snum)"
                self.viewDidLoad()
            }
            
            // MDC 바텀 시트로 설정
            let bottomSheet = MDCBottomSheetController(contentViewController: vc)
            bottomSheet.mdc_bottomSheetPresentationController?.preferredSheetHeight = 200
            bottomSheet.delegate = self

                           // 보여주기
            self.present(bottomSheet, animated: true, completion: nil)
            }})
            
        }
        
//        let vc = R.storyboard.setting.settingSearchSchoolViewController()!
        
        
        
        
    
    
    @IBAction func searchDate(_ sender: Any) {
        UIView.animate(withDuration: 0.1, animations: {
            self.birthdaySecondView.backgroundColor = UIColor.Foggy.withAlphaComponent(0.6)
        }, completion: {finished in
            self.birthdaySecondView.backgroundColor = UIColor.white
            if let vc = R.storyboard.setting.settingDateViewController() {
            
                vc.userinfo = self.userinfo
            vc.authCallback = { (sDate : String?, userinfo: User?) in
                
                // 후처리
                if let sdate = sDate, sdate != "정보 없음" {
                    self.birthday.text = sdate
                    self.viewDidLoad()
                }
            
            }
            // MDC 바텀 시트로 설정
            let bottomSheet = MDCBottomSheetController(contentViewController: vc)
            bottomSheet.mdc_bottomSheetPresentationController?.preferredSheetHeight = 425
            bottomSheet.delegate = self

                           // 보여주기
            self.present(bottomSheet, animated: true, completion: nil)
            }
        })
//
//        self.present(vc, animated: true)

        
    }
        
    
    
}


extension UIStackView {
    func addHorizontalSeparators(color : UIColor) {
        var i = self.arrangedSubviews.count - 1
        var track = 0
//        while track < i {
//            let separator = createSeparator(color: color)
//
//            insertArrangedSubview(separator, at: 2*track + 1)
//
//            separator.widthAnchor.constraint(equalTo: self.widthAnchor, constant: -24.0).isActive = true
//            separator.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12.0).isActive = true
//            track += 1
//        }
        while i >= 1 {
            let separator = createSeparator(color: color)
//            separator.translatesAutoresizingMaskIntoConstraints = false
            insertArrangedSubview(separator, at: i)
            separator.widthAnchor.constraint(equalTo: self.widthAnchor, constant: -24.0).isActive = true
            separator.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12.0).isActive = true
            i -= 1
        }
    }

    private func createSeparator(color : UIColor) -> UIView {
        let separator = UIView()
        separator.heightAnchor.constraint(equalToConstant: 0.25).isActive = true
        separator.backgroundColor = color
        return separator
    }
}
