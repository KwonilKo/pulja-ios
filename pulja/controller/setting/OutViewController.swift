//
//  OutViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/02/05.
//

import UIKit
import MaterialComponents
import ChannelIOFront

class OutViewController: PuljaBaseViewController, MDCBottomSheetControllerDelegate {
    
    
    @IBOutlet var check1: UIImageView!
    @IBOutlet var subview1: UIView!
    @IBOutlet var check2: UIImageView!
    
    
    @IBOutlet var subview2: UIView!
    @IBOutlet var check3: UIImageView!
    

    @IBOutlet var subview3: UIView!
    @IBOutlet var check4: UIImageView!
    
    
    @IBOutlet var exitText: UILabel!
    @IBOutlet var subview4: UIView!
    
    var status = ["check1" : false, "check2" : false, "check3" : false, "check4" : false ]
    
    var exit = true {
        didSet {
            if (!check1.isHidden || !check2.isHidden || !check3.isHidden || !check4.isHidden) {
                self.exitButton.backgroundColor = UIColor.red10
                self.exitButton.isUserInteractionEnabled = true
            } else {
                self.exitButton.backgroundColor = UIColor.Foggy
                self.exitButton.isUserInteractionEnabled = false
            }
        }
    }
    
    
    
    @IBOutlet var mainStackView: UIStackView!
    
    @IBOutlet var exitButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        check1.isHidden = true
        check2.isHidden = true
        check3.isHidden = true
        check4.isHidden = true
        self.exitButton.isUserInteractionEnabled = false
        
        // Do any additional setup after loading the view.
        
//        //제스처 추가
        let gesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender: )))
        mainStackView.addGestureRecognizer(gesture)
        
        exitText.text = "푸링이는 \(myInfo!.username!)님과의\n추억을 돌아보고 있어요"
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    var touchedIdx : Int?
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        
        let location = sender.location(in: mainStackView)
        var locationInView = CGPoint.zero
        let subViews = mainStackView.subviews
        
        for subView in subViews {
            locationInView = subView.convert(location, from: mainStackView)
            if subView.isKind(of: UIView.self) {
                if subView.point(inside: locationInView, with: nil) {
                    // this view contains that point
                    self.touchedIdx = subViews.firstIndex(of: subView)!
                    print("self.touchedIdx: \(self.touchedIdx)")
                    break
                }
            }
        }
        
        let originalBool = self.status[Array(self.status.keys)[self.touchedIdx!]]
        
        for (i, k) in self.status.keys.enumerated() {
            if originalBool == false {
                if i == self.touchedIdx! {
                    self.status[k] = !originalBool!
                } else {
                    self.status[k] = originalBool
                }
            } else {
                if i == self.touchedIdx! {
                    self.status[k] = !originalBool!
                }
            }
        }
        
        //update the button
        self.updateButton()
        
        
    }
    var checkedIdx: Int?
    func updateButton() -> Bool {
        let temp = [check1, check2, check3, check4]
        for (i,v) in self.status.values.enumerated() {
            if v == true {
                checkedIdx = i
                temp[i]!.isHidden = false
            } else {
                temp[i]!.isHidden = true
            }
        }
        
        
        for (i,v) in self.status.values.enumerated() {
            if v == true {
                self.exitButton.isUserInteractionEnabled = true
                self.exitButton.backgroundColor = UIColor.red10
                return true
            }
        }
        self.exitButton.isUserInteractionEnabled = false
        self.exitButton.backgroundColor = UIColor.Foggy
        return true
    }
    
    
    @IBAction func ibBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func initChennelTalk(){
        
        guard let info = self.myInfo else { return }
        guard let userName = info.username else { return }
        guard let userId = info.hashUserSeq else { return }
        
        let profile = Profile()
            .set(name: userName)
        
        
        let buttonOption = ChannelButtonOption.init(
            position: .right,
            xMargin: 16,
            yMargin: 23
        )
        
        let bootConfig = BootConfig.init(
            pluginKey: Const.CHANNELTALK_PLUGIN_KEY,
            memberId: userId,
            //          memberHash: "",
            profile: profile,
            channelButtonOption: buttonOption,
            hidePopup: false,
            trackDefaultEvent: true,
            language: .korean
        )
        
        ChannelIO.boot(with: bootConfig) { (bootStatus, user) in
            if bootStatus == .success, let user = user {
                // success
                ChannelIO.showMessenger()
            } else {
                // show failed reason from bootStatus
            }
        }
    }
    
    @IBAction func exitButtonPressed(_ sender: Any) {
        self.dropOutAlert()
    }
    
    
    func dropOutAlert(){
        
        self.alertExit(title: "아쉬운 마음에.. 한번 더 확인할게요", message: "탈퇴 시 계정 복구는 불가능해요.\n정말로 떠나시나요..?",
                               button: "탈퇴할게요", subButton: "한번 더 생각해 볼게요").done { b in
                    if b {
                        //탈퇴하기
                        print("탈퇴하기")
                        let reason: String?
                        switch self.checkedIdx! {
                        case 0:
                            reason = "서비스 이용이 어렵고 불편해요."
                        case 1:
                            reason = "내가 원하는 학습 콘텐츠가 없어요."
                        case 2:
                            reason = "서비스 오류가 빈번하게 발생해요."
                        default:
                            reason = "기타"
                        }
                        
                        
                        UserAPI.shared.dropOut(dropOutReason: reason!, userSeq: Int(self.myInfo!.userSeq)).done { res in
                            if let suc = res.success, suc == true {
                                print("탈퇴 api 성공")
                                CommonUtil.shared.logout(false)
                            } else {
                                print("탈퇴 오류")
                            }
                        }.catch { error in
                            print(error.localizedDescription)
                        }
                        
                        
                    } else {
                        //한번 더 생각해 볼게요
                        print("한번 더 생각해 볼게요")
                    }
                }
    }
    
    
    
    func notDropOut() {
        self.alert(title: "", message: "현재 수강중인 회원이 탈퇴를 원하시는\n경우, 풀자 고객 센터로 문의 부탁드립니다.", button: "탈퇴 신청하기", subButton: "닫기").done { b in
            if b {
                                
                UserAPI.shared.makeEnquireEventLog().done { res in
                   if let suc = res.success, suc == true {
                       self.initChennelTalk()
                   } else {
                       
                   }
                }.catch { error in
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        CommonUtil.tabbarController?.tabBar.isHidden = true
        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        CommonUtil.tabbarController?.tabBar.isHidden = true
//        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        CommonUtil.tabbarController?.tabBar.isHidden = false
    }
}
