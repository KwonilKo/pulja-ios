//
//  SettingPasswordViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/01/02.
//

import UIKit
import CoreData
import PromiseKit
import MaterialComponents.MaterialBottomSheet
import Kingfisher


class SettingPasswordViewController: PuljaBaseViewController, MDCBottomSheetControllerDelegate {

    @IBOutlet var ivSolidCheck: UIImageView!
    
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfNewPassword: UITextField!
    @IBOutlet weak var tfNewPasswordCheck: UITextField!
    
    @IBOutlet weak var passwordValidView: UIView!
    
    @IBOutlet weak var passwordConfirmView: UIView!
    
    
    @IBOutlet weak var ivConfirmCheck: UIImageView!
    
    @IBOutlet weak var lbConfirmCheck: UILabel!
    
    
    @IBOutlet weak var btComplete: UIButton!
    
    @IBOutlet weak var ivLengthCheck: UIImageView!
    @IBOutlet weak var lbLengthCheck: UILabel!
    
    
    @IBOutlet weak var ivTextCheck: UIImageView!
    
    @IBOutlet weak var lbTextCheck: UILabel!
    
    @IBOutlet weak var ivNumberCheck: UIImageView!
    
    @IBOutlet weak var lbNumberCheck: UILabel!
    
    var authCallback : ((_ userinfo : User?) -> Void)?

    var userinfo : User?
    
    var activeTextField : UITextField? = nil
    
    var isPassword = false
    var isNewPassword = false
    var isConfirm = false
    
    var isPwdSecure = true

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btComplete.isUserInteractionEnabled = false

        // Do any additional setup after loading the view.
        
        self.tfPassword.layer.cornerRadius = 10.0
        self.tfPassword.layer.borderWidth = 0.5
        self.tfPassword.layer.backgroundColor = UIColor.white.cgColor
        self.tfPassword.layer.borderColor = UIColor.Foggy.cgColor
        self.tfPassword.setPadding(left: 12, right: 12)
        
        self.tfPassword.disableAutoFill()
        self.tfPassword.autocorrectionType = .no

        self.tfNewPassword.layer.cornerRadius = 10.0
        self.tfNewPassword.layer.borderWidth = 0.5
        self.tfNewPassword.layer.backgroundColor = UIColor.white.cgColor
        self.tfNewPassword.layer.borderColor = UIColor.Foggy.cgColor
        self.tfNewPassword.setPadding(left: 12, right: 12)
        
        self.tfNewPassword.disableAutoFill()
        self.tfNewPassword.autocorrectionType = .no

        
        self.tfNewPasswordCheck.layer.cornerRadius = 10.0
                self.tfNewPasswordCheck.layer.borderWidth = 0.5
                self.tfNewPasswordCheck.layer.backgroundColor = UIColor.white.cgColor
                self.tfNewPasswordCheck.layer.borderColor = UIColor.Foggy.cgColor
                self.tfNewPasswordCheck.setPadding(left: 12, right: 12)
        
        self.tfNewPasswordCheck.disableAutoFill()
        self.tfNewPasswordCheck.autocorrectionType = .no

        
        self.tfPassword.addDoneButtonOnKeyboard()
        self.tfNewPassword.addDoneButtonOnKeyboard()
        self.tfNewPasswordCheck.addDoneButtonOnKeyboard()
        
        //옵져버 추가
        NotificationCenter.default.addObserver(self, selector: #selector(SettingPasswordViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SettingPasswordViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
                       
        self.tfPassword.delegate = self
        self.tfNewPassword.delegate = self
        self.tfNewPasswordCheck.delegate = self

        
        self.passwordValidView.isHidden = true
        self.passwordConfirmView.isHidden = true
        
        self.btComplete.backgroundColor = UIColor.Foggy
        self.btComplete.isUserInteractionEnabled = false
        

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            self.view.endEditing(true)

        self.tfPassword.layer.borderColor = UIColor.Foggy.cgColor
        self.tfNewPassword.layer.borderColor = UIColor.Foggy.cgColor
        self.tfNewPasswordCheck.layer.borderColor = UIColor.Foggy.cgColor

    }
    
    
   
    @IBAction func ibBack(_ sender: Any) {
        print("여기들어옴")
        print(self.navigationController)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func findPassword(_ sender: Any) {
        let vc = R.storyboard.main.idpwdViewController()!
        vc.viewTitle = "비밀번호 찾기"
        vc.url = "\(Const.DOMAIN)/login_find_password_id"
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true) {

        }
        
    }
    
    // MARK: - Input
    @objc func keyboardWillShow(notification: NSNotification) {
            
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            
            // if keyboard size is not available for some reason, dont do anything
           return
        }
        
        var shouldMoveViewUp = false
        
        // if active text field is not nil
        if let activeTextField = activeTextField {
            
            let bottomOfTextField = activeTextField.convert(activeTextField.bounds, to: self.view).maxY;
            let topOfKeyboard = self.view.frame.height - keyboardSize.height
            
            if bottomOfTextField > topOfKeyboard {
                shouldMoveViewUp = true
            }
        }
        
        if(shouldMoveViewUp) {
            self.view.frame.origin.y = 0 - keyboardSize.height
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
    }

    
    
    @IBAction func currPassword(_ sender: UITextField) {
        self.passwordValidView.isHidden = true
        print("came into curr password")
        if sender == tfPassword, let textCount = tfPassword.text?.count {
            
            self.passwordValidView.isHidden = true
//            self.tfPassword.layer.borderColor = UIColor.Foggy.cgColor

             if textCount == 0 {

//                 self.tfPassword.layer.borderColor = UIColor.Foggy.cgColor
                 return

             } else {
//                 print("여기 들어옴")
//
                 self.passwordValidView.isHidden = true

                 self.isPassword =  self.passwordCheck(tfcheck: tfPassword, isCurrBool: true)

             }
            self.allCheck()
//            self.tfPassword.layer.borderColor = UIColor.Foggy.cgColor
             
         }
    }
    
    
    
    
    @IBAction func passwordFocus(_ sender: UITextField) {
//        tfPassword.isSecureTextEntry = isPwdSecure
        tfNewPassword.isSecureTextEntry = isPwdSecure
       sender.disableAutoFill()
       sender.autocorrectionType = .no
    
    }
    
    
    @IBAction func passwordEditingChange(_ sender: UITextField) {

        print("password editing change came here")
        if sender == tfNewPassword, let textCount = tfNewPassword.text?.count {


             if textCount == 0 {
                 self.passwordValidView.isHidden = true

//                 self.tfNewPassword.layer.borderColor = UIColor.Foggy.cgColor
                 return

             } else {

                 self.passwordValidView.isHidden = false

                 self.isNewPassword =  self.passwordCheck(tfcheck: tfNewPassword, isCurrBool: false)

             }
            self.allCheck()
//            self.tfNewPassword.layer.borderColor = UIColor.Foggy.cgColor

         }
    }
    
    @IBAction func PrebtSecure(_ sender: Any) {
        tfPassword.isSecureTextEntry.toggle()
        isPwdSecure = tfPassword.isSecureTextEntry
        
        if tfPassword.isSecureTextEntry {
            
            if let image = UIImage(named: "iconSolidEye") {
                (sender as AnyObject).setImage(image, for: .normal)
            }
        } else {
            if let image = UIImage(named: "iconSolidEyeOff") {
                (sender as AnyObject).setImage(image, for: .normal)
            }
        }
        
        
    }
    @IBAction func btSecure(_ sender: Any) {
        tfNewPassword.isSecureTextEntry.toggle()
        isPwdSecure = tfNewPassword.isSecureTextEntry
        
        if tfNewPassword.isSecureTextEntry {
            
            if let image = UIImage(named: "iconSolidEye") {
                (sender as AnyObject).setImage(image, for: .normal)
            }
        } else {
            if let image = UIImage(named: "iconSolidEyeOff") {
                (sender as AnyObject).setImage(image, for: .normal)
            }
        }
        
    }
    
    @IBAction func btSecureConfirm(_ sender: Any) {
        
        
        tfNewPasswordCheck.isSecureTextEntry.toggle()
                
        if tfNewPasswordCheck.isSecureTextEntry {
            if let image = UIImage(named: "iconSolidEye") {
                (sender as AnyObject).setImage(image, for: .normal)
            }
        } else {
            if let image = UIImage(named: "iconSolidEyeOff") {
                (sender as AnyObject).setImage(image, for: .normal)
            }
        }
        
    }
    
    
    
    @IBAction func confirmEditingChange(_ sender: UITextField) {
        if sender == tfNewPasswordCheck, let textCount = tfNewPasswordCheck.text?.count {
                        
             if textCount == 0 {
                 self.passwordConfirmView.isHidden = true
//                 self.tfNewPasswordCheck.layer.borderColor = UIColor.Foggy.cgColor
                 return
             } else {
                 self.passwordConfirmView.isHidden = false
                 self.isConfirm = self.confirmCheck()
                
             }
            
            self.allCheck()
            
//            self.tfNewPasswordCheck.layer.borderColor = UIColor.purpleishBlue.cgColor
             
         }
        
        
        
        
    }
    
    func confirmCheck() -> Bool {
            var isValid = false
            
            if let pwd = tfNewPassword.text, let pwdConfirm = tfNewPasswordCheck.text {
                if pwd == pwdConfirm {
                    lbConfirmCheck.textColor = UIColor.puljaRealblack
                    lbConfirmCheck.font = UIFont.systemFont(ofSize: 14)
                    lbConfirmCheck.text = "비밀번호와 일치해요."
                    ivSolidCheck.isHidden = false
//                    ivConfirmCheck.isHidden = true
//                    ivTextCheck.isHidden = true
//                    ivLengthCheck.isHidden = true
                    isValid =  true
                } else {
                    lbConfirmCheck.textColor = UIColor.puljaRed
                    lbConfirmCheck.font = UIFont.boldSystemFont(ofSize: 14)
                    ivSolidCheck.isHidden = true
//                    ivConfirmCheck.isHidden = false
//                    ivTextCheck.isHidden = false
//                    ivLengthCheck.isHidden = false
                    lbConfirmCheck.text = "비밀번호와 일치하지 않아요."
                    
                    isValid =  false
                }
            }
            
            return isValid
    }
    
    
    func passwordCheck(tfcheck: UITextField!, isCurrBool: Bool ) -> Bool {
        
            var isValid  = true
            if let cnt = tfcheck.text?.count, let text =  tfcheck.text {
                
        
                if( cnt < 7) {
                    ivLengthCheck.isHidden = true
                    lbLengthCheck.textColor = UIColor.reddishOrange
                    lbLengthCheck.font = UIFont.boldSystemFont(ofSize: 14)
                    
                    isValid = false
                    
                } else {
                    ivLengthCheck.isHidden = false
                    lbLengthCheck.textColor = UIColor.puljaBlack
                    lbLengthCheck.font = UIFont.systemFont(ofSize: 14)
                    
                    
                }
                
                if isNumber(checkStr: text) {
                    ivNumberCheck.isHidden = false
                    lbNumberCheck.textColor = UIColor.puljaBlack
                    lbNumberCheck.font = UIFont.systemFont(ofSize: 14)
                    
                    
                } else {
                    ivNumberCheck.isHidden = true
                    lbNumberCheck.textColor = UIColor.reddishOrange
                    lbNumberCheck.font = UIFont.boldSystemFont(ofSize: 14)
                    
                    isValid = false
                }
                
                if isText(checkStr: text) {
                    ivTextCheck.isHidden = false
                    lbTextCheck.textColor = UIColor.puljaBlack
                    lbTextCheck.font = UIFont.systemFont(ofSize: 14)
                    
                } else {
                    ivTextCheck.isHidden = true
                    lbTextCheck.textColor = UIColor.reddishOrange
                    lbTextCheck.font = UIFont.boldSystemFont(ofSize: 14)
                    
                    isValid = false
                }
                
            } else {
                isValid = false
            }
        
            if isCurrBool {
                passwordValidView.isHidden = true
            } else {
            
                if isValid {
                    passwordValidView.isHidden = true
                } else {
                    passwordValidView.isHidden = false
                }
            }
            
            return isValid
        }

    func allCheck(){
        print("비밀번호: \(isPassword), \(isNewPassword), \(isConfirm)")
        if isPassword && isNewPassword && isConfirm {
            self.passwordConfirmView.isHidden = true
            btComplete.setTitleColor(UIColor.white, for: .normal)
            btComplete.backgroundColor = UIColor.Purple
            btComplete.isUserInteractionEnabled = true
            btComplete.setTitle("변경하기", for: .normal)
        } else {
            btComplete.setTitleColor(UIColor.white, for: .normal)
            btComplete.backgroundColor = UIColor.Foggy
            btComplete.isUserInteractionEnabled = false
            btComplete.setTitle("변경하기", for: .normal)
            
        }
              
    }
    
    func isNumber(checkStr:String?) -> Bool {
//        let pwRegEx = "[A-Za-z0-9!_@$%^&+=]{8,20}"
        
        guard checkStr != nil else { return false }
        
        let pwRegEx = "^(?=.*[0-9]).{1,}$"
        let pred = NSPredicate(format:"SELF MATCHES %@", pwRegEx)
        return pred.evaluate(with: checkStr)
    }
    
    func isText(checkStr:String?) -> Bool {
//        let pwRegEx = "[A-Za-z0-9!_@$%^&+=]{8,20}"

        guard checkStr != nil else { return false }

        let pwRegEx = "^(?=.*[A-Za-z]).{1,}$"
        let pred = NSPredicate(format:"SELF MATCHES %@", pwRegEx)

         return pred.evaluate(with: checkStr)

    }
    
    
    @IBAction func submit(_ sender: Any) {
        
        guard let currPassword = tfPassword.text else { return }
        guard let newPassword = tfNewPassword.text else { return }
        guard let newPasswordCheck = tfNewPasswordCheck.text else { return }
        let vc = PuljaBaseViewController()
        
        //새로운 비밀번호와 다시 입력한 새로운 비밀번호가 일치하지 않는경우
        if newPassword != newPasswordCheck {
            print("기존 비밀번호가 틀렸거나 새로운 비밀번호가 서로 일치하지 않습니다1111")
            self.showToast(message: "현재 비밀번호를 다시 확인해주세요.")
//            self.alert(title: nil, message: "현재 비밀번호를 다시 확인해주세요.", button: "확인")
        } else {
            UserAPI.shared.changePassword(newPassword: newPassword, oldPassword: currPassword).done {
                res in
                //비밀번호 바꾸는거 성공한 경우
                if let success = res.success, success == true {
                    self.navigationController?.popViewController(animated: true, completion: {
                        self.authCallback?(self.userinfo)
                    })
                    
                    
                } //기존 비밀번호가 틀렸거나, 새로운 비밀번호가 3가지 format에 맞지 않은경우
                else {
                    print("기존 비밀번호가 틀렸거나 새로운 비밀번호가 서로 일치하지 않습니다22222")
                    self.showToast(message: "현재 비밀번호를 다시 확인해주세요.")
//                    self.alert(title: nil, message: "현재 비밀번호를 다시 확인해주세요.", button: "확인")
                }
            }.catch { error in
                print("기존 비밀번호가 틀렸거나 새로운 비밀번호가 서로 일치하지 않습니다22222")
                self.showToast(message: "현재 비밀번호를 다시 확인해주세요.")
//                self.alert(title: nil, message: "현재 비밀번호를 다시 확인해주세요.", button: "확인")
            }
        
            
        }
        
        
    }
    func showToast(message: String) {
        var heMinus : CGFloat = 160.0
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.first
            let top = window?.safeAreaInsets.top
            let bottom = window?.safeAreaInsets.bottom
            heMinus += bottom!
            print("top : \(String(describing: top))")
            print("bottom : \(String(describing: bottom))")
            
        } else if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let top = window?.safeAreaInsets.top
            let bottom = window?.safeAreaInsets.bottom
            heMinus += bottom!
            print("top : \(String(describing: top))")
            print("bottom : \(String(describing: bottom))")
        }
        let width = self.view.frame.size.width
//        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 150, y: self.view.frame.size.height-150, width: 300, height: 35))
        let toastLabel = UILabel(frame: CGRect(x: 12, y: self.view.frame.size.height - heMinus, width: width - 24, height: 35))
//        let toastLabel = UILabel(frame: CGRect(x: 12, y: self.view.frame.size.height - 200, width: width - 24, height: 35))
        
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        toastLabel.textAlignment = .center
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10
        toastLabel.clipsToBounds = true
//        toastLabel.translatesAutoresizingMaskIntoConstraints = false
//        toastLabel.layer.position = CGPoint(x: self.view.frame.midX, y: self.view.frame.maxY - 40)
//        toastLabel.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 30).isActive = true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: { toastLabel.alpha = 0.0 }, completion: {(isCompleted) in toastLabel.removeFromSuperview() })
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        CommonUtil.tabbarController?.tabBar.isHidden = true
        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        
        CommonUtil.coachTabbarController?.tabBar.isHidden = true
        CommonUtil.coachTabbarController?.bottomNavBar.isHidden = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        CommonUtil.tabbarController?.tabBar.isHidden = false
    
        CommonUtil.coachTabbarController?.tabBar.isHidden = false
        CommonUtil.coachTabbarController?.bottomNavBar.isHidden = false
    }
    
}


// MARK: - extension
extension SettingPasswordViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
        
        
        
        if textField == self.tfPassword {
            self.tfPassword.layer.borderWidth = 1.0
            self.tfPassword.layer.borderColor = UIColor.Purple.cgColor
            self.tfNewPassword.layer.borderWidth = 0.5
            self.tfNewPassword.layer.borderColor = UIColor.Foggy.cgColor
            self.tfNewPasswordCheck.layer.borderWidth = 0.5
            self.tfNewPasswordCheck.layer.borderColor = UIColor.Foggy.cgColor
       } else if textField == self.tfNewPassword {
           self.tfPassword.layer.borderWidth = 0.5
           self.tfPassword.layer.borderColor = UIColor.Foggy.cgColor
           self.tfNewPassword.layer.borderWidth = 1.0
           self.tfNewPassword.layer.borderColor = UIColor.Purple.cgColor
           self.tfNewPasswordCheck.layer.borderWidth = 0.5
           self.tfNewPasswordCheck.layer.borderColor = UIColor.Foggy.cgColor
       } else if textField == self.tfNewPasswordCheck {
           self.tfPassword.layer.borderWidth = 0.5
           self.tfPassword.layer.borderColor = UIColor.Foggy.cgColor
           self.tfNewPassword.layer.borderWidth = 0.5
           self.tfNewPassword.layer.borderColor = UIColor.Foggy.cgColor
           self.tfNewPasswordCheck.layer.borderWidth = 1.0
           self.tfNewPasswordCheck.layer.borderColor = UIColor.Purple.cgColor
       } else {
           self.tfPassword.layer.borderWidth = 0.5
           self.tfPassword.layer.borderColor = UIColor.Foggy.cgColor
           self.tfNewPassword.layer.borderWidth = 0.5
           self.tfNewPassword.layer.borderColor = UIColor.Foggy.cgColor
           self.tfNewPasswordCheck.layer.borderWidth = 0.5
           self.tfNewPasswordCheck.layer.borderColor = UIColor.Foggy.cgColor
       }
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeTextField = nil
    }
}
