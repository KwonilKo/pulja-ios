//
//  SettingParentPhoneViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/02/06.
//

import UIKit

class SettingParentPhoneViewController: PuljaBaseViewController, UITextFieldDelegate {

    @IBOutlet var ParentPhone: UILabel!
    
    @IBOutlet var changeButton: UIButton!
    var userPhone : String = ""
    var isCheckInput = false
    var userinfo : User?
    var authCallback : ((_ phonenumber : String?, _ userinfo : User?) -> Void)?
    
    @IBOutlet weak var keyboardHeightLayoutConstraint: NSLayoutConstraint!
    @IBOutlet var tfPhoneNumber: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        changeButton.isUserInteractionEnabled = false 
        // Do any additional setup after loading the view.
        self.ParentPhone.text = self.userPhone
        self.tfPhoneNumber.layer.cornerRadius = 10.0
        self.tfPhoneNumber.layer.borderWidth = 0.2
        self.tfPhoneNumber.layer.backgroundColor = UIColor.white.cgColor
        self.tfPhoneNumber.layer.borderColor = UIColor.lightBlueGrey.cgColor
//        self.tfPhoneNumber.textColor = UIColor.lightBlueGrey
        self.tfPhoneNumber.setPadding(left: 12, right: 12)
        
        self.tfPhoneNumber.keyboardType = .numberPad
        self.tfPhoneNumber.addDoneButtonOnKeyboard()
        
//        self.tfPhoneNumber.addTarget(self, action: #selector(SettingParentPhoneViewController.textFieldDidChange(_:)), for: .editingChanged)
//
        self.tfPhoneNumber.delegate = self
        
        
        NotificationCenter.default.addObserver(self,
                       selector: #selector(self.keyboardNotification(notification:)),
                       name: UIResponder.keyboardWillChangeFrameNotification,
                       object: nil)
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
       
   @objc func keyboardNotification(notification: NSNotification) {
       guard let userInfo = notification.userInfo else { return }

       let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
       let endFrameY = endFrame?.origin.y ?? 0
       let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
       let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
       let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
       let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)

       if endFrameY >= UIScreen.main.bounds.size.height {
         self.keyboardHeightLayoutConstraint?.constant = 20.0
       } else {
         self.keyboardHeightLayoutConstraint?.constant = endFrame?.size.height ?? 0.0
       }

       UIView.animate(
         withDuration: duration,
         delay: TimeInterval(0),
         options: animationCurve,
         animations: { self.view.layoutIfNeeded() },
         completion: nil)
     }
    
    
    
    @IBAction func submit(_ sender: Any) {
        let userPhone = self.tfPhoneNumber.text!.replacingOccurrences(of: "-", with: "")
        print("바꾼 부모 전화번호: \(userPhone)")
        UserAPI.shared.changeInfo(phonenumber: "", birthday: "", schoolId: "", schoolName: "", schoolNum: -1, schoolType: "", parentsPhone: userPhone, user : self.userinfo).done { res in
            if let bool = res.success, bool == true {
                
                self.navigationController?.popViewController(animated: true, completion: {
                    self.authCallback?(self.tfPhoneNumber.text, self.userinfo)
                })
                
            }
        }
    }
    //    @objc func textFieldDidChange(_ textField: UITextField) {
//        print("여기는?")
//        if textField == tfPhoneNumber {
//            print("여기여기 들어옴")
//           if let phoneCount = tfPhoneNumber.text?.count {
//               if phoneCount == 11 {
//                   isCheckInput = true
//                   print("유저가 맞게 폰번호 입력")
//               } else {
//                   isCheckInput = false
//                   print("유저가 틀리게 폰번호 입력")
//               }
//           }
////            agreeCheck()
//       }
//
//    }
    
//    @IBAction func phoneChange(_ sender: UITextField) {
//        print("여기는?")
//        if sender == tfPhoneNumber {
//            print("여기여기 들어옴")
//           if let phoneCount = tfPhoneNumber.text?.count {
//               if phoneCount > 0 {
//                   isCheckInput = true
//                   print("유저가 맞게 폰번호 입력")
//               } else {
//                   isCheckInput = false
//                   print("유저가 틀리게 폰번호 입력")
//               }
//           }
////            agreeCheck()
//       }
//
//    }
    
   
    
    @IBAction func ibBack(_ sender: Any) {
        
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        CommonUtil.tabbarController?.tabBar.isHidden = true
        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        CommonUtil.tabbarController?.tabBar.isHidden = true
//        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        CommonUtil.tabbarController?.tabBar.isHidden = false
    }

}

// MARK: - extension
extension SettingParentPhoneViewController {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == tfPhoneNumber {
            textField.formatPhoneNumber(range: range, string: string)
            
            
           if let phoneCount = tfPhoneNumber.text?.count {
               if phoneCount == 13 {
                   print("참")
                   isCheckInput = true
                   self.changeButton.isUserInteractionEnabled = true
                   self.changeButton.backgroundColor = UIColor.Purple
                   
               } else {
                   print("거짓")
                   isCheckInput = false
                   self.changeButton.isUserInteractionEnabled = false
                   self.changeButton.backgroundColor = UIColor.Foggy
               }
           }
            
            return false
        }
        
        return true
    }
}
