//
//  SettingDateViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/01/06.
//

import UIKit

class SettingDateViewController: PuljaBaseViewController {
    
    
    @IBOutlet weak var sDate: UITextField!
    
    
    @IBOutlet weak var pickerView: UIDatePicker!
    var userinfo : User?
    
    var authCallback : ((_ sDate : String?, _ userinfo : User?) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.sDate.layer.cornerRadius = 10.0
        self.sDate.layer.borderWidth = 0.2
        self.sDate.layer.backgroundColor = UIColor.puljaPaleGrey.cgColor
        self.sDate.layer.borderColor = UIColor.puljaPaleGrey.cgColor
        self.sDate.textColor = UIColor.lightBlueGrey
        
        self.sDate.setPadding(left: 12, right: 12)
        self.sDate.isUserInteractionEnabled = false
        self.pickerView.setValue(UIColor.black, forKeyPath: "textColor")
        self.sDate.addDoneButtonOnKeyboard()
    
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func datePickerView(_ sender: Any) {
        pickerView.addTarget(self, action: #selector(changed), for: .valueChanged)

        
        
        
    }
    
    @objc func changed() {
        let dateformatter = DateFormatter()
        dateformatter.dateStyle = .long
        dateformatter.timeStyle = .none
        dateformatter.locale = Locale(identifier: "ko")
        let date = dateformatter.string(from: pickerView.date)
        sDate.text = date
        
    }
    @IBAction func save(_ sender: Any) {
        print("저장 버튼 누름")
        let birthformat = DateFormatter()
        birthformat.dateFormat = "yyyy년 MM월 dd일"
        print("생일은 \(self.sDate.text!) 입니다")
        guard let birth = birthformat.date(from: self.sDate.text!) else { return }
        
        //"yyyy년 MM월 dd일" -> "yyyyMMdd"로 바꿔주기
        birthformat.dateFormat = "yyyyMMdd"
        let convert_birth = birthformat.string(from: birth)
        print("바꾼 생일 : \(convert_birth)")
        //새로운 생년월일 정보 업데이트 해주기
        UserAPI.shared.changeInfo(phonenumber: "", birthday: convert_birth, schoolId: "", schoolName: "", schoolNum: -1,schoolType: "", parentsPhone: "", user : userinfo).done { res in
            if let bool = res.success, bool == true {
                print("생일 성공적으로 바꿈")
                self.authCallback?(self.sDate.text, self.userinfo)
                self.dismiss(animated: true)
            }
        }
        
    }



    @IBAction func close(_ sender: Any) {
        dismiss(animated: true) { }
    }
}
