import UIKit
import SwiftyJSON

class SettingSearchSchoolHelperViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var schoolName: DesignableUITextField!
    
    @IBOutlet weak var searchView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    //    @IBOutlet weak var searchTableView: UITableView!
    
    var schoolNameId: [String : String] = [:]
    var schoolId: String? = ""
    var schoolLists : [School]?
    var authCallback : ((_ sName : String?, _ sId : String?) -> Void)?
    
    
    @IBOutlet var btComplete: UIButton!
    
    @IBOutlet weak var keyboardHeightLayoutConstraint: NSLayoutConstraint!
    //    var authCallback : (_ result : String) -> Void = { _ in}

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        self.schoolName.layer.cornerRadius = 10.0
//        self.schoolName.layer.borderWidth = 0.2
//        self.schoolName.layer.backgroundColor = UIColor.puljaPaleGrey.cgColor
//        self.schoolName.layer.borderColor = UIColor.puljaPaleGrey.cgColor
//        self.schoolName.setPadding(left: 12, right: 12)
        
//        self.schoolName.addDoneButtonOnKeyboard()
        self.schoolName.delegate = self
        
        self.btComplete.isUserInteractionEnabled = false
        
        
        
        NotificationCenter.default.addObserver(self,
                       selector: #selector(self.keyboardNotification(notification:)),
                       name: UIResponder.keyboardWillChangeFrameNotification,
                       object: nil)
        
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
           
   @objc func keyboardNotification(notification: NSNotification) {
       guard let userInfo = notification.userInfo else { return }

       let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
       let endFrameY = endFrame?.origin.y ?? 0
       let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
       let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
       let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
       let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)

       if endFrameY >= UIScreen.main.bounds.size.height {
         self.keyboardHeightLayoutConstraint?.constant = 20.0
       } else {
         self.keyboardHeightLayoutConstraint?.constant = endFrame?.size.height ?? 0.0
       }

       UIView.animate(
         withDuration: duration,
         delay: TimeInterval(0),
         options: animationCurve,
         animations: { self.view.layoutIfNeeded() },
         completion: nil)
     }
    
    
    @IBAction func search(_ sender: UITextField) {
        print("들어옴들어옴")
        //유저가 검색하는 순간, 순간 api 호출
        if schoolName.text?.trimmingCharacters(in: .whitespaces) == "" || schoolName.text == "학교명을 입력해주세요" {
            btComplete.isUserInteractionEnabled = false
            btComplete.backgroundColor = UIColor.Foggy
            
        } else {
            btComplete.isUserInteractionEnabled = true
            btComplete.backgroundColor = UIColor.Purple
        }
        
        
        UserAPI.shared.schoolList(schoolname: schoolName.text!).done {
            res in
            if let jsonSchool:[School] = res.data, jsonSchool.count > 0 {
                self.schoolLists = jsonSchool
                self.tableView.reloadData()
            
            }
            

        }
    
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
  
    @IBAction func close(_ sender: Any) {
        dismiss(animated: true) { }
        
        
    }
    
    @IBAction func save(_ sender: Any) {
        authCallback?(self.schoolName.text, self.schoolId)
        dismiss(animated: true)
//        let vc = R.storyboard.setting.settingSearchSchoolViewController()!
//        if let schoolname = self.schoolName.text, schoolname != "" {
//            print("새로운 학교이름은 \(schoolname) 입니다")
//            vc.sName = schoolname
//        }
        
        
        //api 업데이트 해줄 것
        
        
    }
    
    
    
}


extension SettingSearchSchoolHelperViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.schoolLists?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? SearchSchoolTableViewCell else { return UITableViewCell()}
        
        if let suggestion = self.schoolLists?[indexPath.row] {
            cell.schoolName.font = UIFont(name:"AppleSDGothicNeo-Regular", size:14)
            cell.schoolName.textColor = UIColor.puljaBlack
            cell.schoolName.text = suggestion.schulNm
            
//            cell.schoolAddress.font = UIFont(name:"AppleSDGothicNeo", size:10)
            cell.schoolAddress.font = UIFont.boldSystemFont(ofSize: 10)
            cell.schoolAddress.textColor = UIColor.grey
            cell.schoolAddress.text = suggestion.schulRdnma
            self.schoolNameId[cell.schoolName.text!] = suggestion.schulCode
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? SearchSchoolTableViewCell else { return }
        self.schoolName.text = cell.schoolName.text
        self.schoolId = self.schoolNameId[self.schoolName.text!]
        
        }
}



