//
//  SettingSearchSchoolViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/01/04.
//

import UIKit
import MaterialComponents.MaterialBottomSheet


class SettingSearchSchoolViewController: UIViewController, MDCBottomSheetControllerDelegate {
        

    @IBOutlet var mainView: UIView!
    
    
    @IBOutlet weak var schoolName: UILabel!
    
    
    @IBOutlet var schoolView: UIView!
    
    
    @IBOutlet var gradeView: UIView!
    @IBOutlet weak var schoolGrade: UILabel!
    
    // 학교 정보
    var sId: String = ""
    var sName : String = ""
    
    // 학년 정보
    var sNum: String = ""
    var sType: String = ""
    var userinfo : User?
    
    var authCallback : ((_ sName : String?, _ sType: String?, _ sNum: String?, _ userinfo : User?) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.mainView.roundedTop(10)
        }
        schoolName.text = self.sName
        schoolGrade.text = "\(self.sType) \(self.sNum)"
    
   
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func close(_ sender: Any) {
        
        dismiss(animated: true) {
            
        }
    }
    
    @IBAction func searchSchool(_ sender: Any) {
        UIView.animate(withDuration: 0.1, animations: {
            self.schoolView.backgroundColor = UIColor.Foggy.withAlphaComponent(0.6)
        }, completion: {finished in
            self.schoolView.backgroundColor = UIColor.white
            if let vc = R.storyboard.setting.settingSearchSchoolHelperViewController() {
            
            vc.authCallback = { (sName : String?, sId : String?) in
                print("schoolId: \(sId), schoolName: \(sName)")
                //후처리
                if let schoolname = sName, schoolname != "", let sid = sId, sid != "" {
                    self.sName = schoolname
                    print("학교이름: ", self.sName)
                    self.sId = sid
                    print("학교 코드 번호: ", self.sId)
                    self.viewDidLoad()
                } else if let schoolname = sName {
                    self.sName = schoolname
                    self.sId = "S000000000"
                    self.viewDidLoad()
                }
            }
                
                self.present(vc, animated: true)
            
            }})
    
    }
    
    
    @IBAction func searchGrade(_ sender: Any) {
        UIView.animate(withDuration: 0.1, animations: {
            self.gradeView.backgroundColor = UIColor.Foggy.withAlphaComponent(0.6)
        }, completion: {finished in
            self.gradeView.backgroundColor = UIColor.white
            if let vc = R.storyboard.setting.settingSearchGradeHelperViewController() {
                vc.authCallback = { (sType : String?, sNum : String?) in
                    // 후처리
                    if let stype = sType, let snum = sNum {
                        self.sType = stype
                        self.sNum = snum
                        self.viewDidLoad()
                    }
                
                }
                // MDC 바텀 시트로 설정
                let bottomSheet = MDCBottomSheetController(contentViewController: vc)
                bottomSheet.mdc_bottomSheetPresentationController?.preferredSheetHeight = 350
                bottomSheet.delegate = self

                               // 보여주기
                self.present(bottomSheet, animated: true, completion: nil)
    //
    //        self.present(vc, animated: true)

            }})
        
    }
    
    
    
    @IBAction func save(_ sender: Any) {
        
        // schoolid 해결, 학교 코드가 없거나, ""으로 되어 있는 경우
        let sid: String
        if (self.sId == "" || self.sId == "정보 없음") {
            sid = ""
        } else {
            sid = self.sId
        }
        print("추가되는 학교 코드 : ", sid)
        
        // schoolname 해결
        let sname = self.sName
        print("추가되는 학교 이름 : ", self.sName)
        
        // school num 해결
        let snum: Int
        switch self.sNum {
        case "1학년":
            snum = 1
        case "2학년":
            snum = 2
        case "3학년":
            snum = 3
        case "검정고시":
            snum = 4
        default:
            snum = 5
        }
        
        // school type 해결
        let stype: String
        switch self.sType {
        case "중학교" :
            stype = "1"
        case "고등학교" :
            stype = "2"
        default:
            stype = "3"
        }
        
        print("이 학생은 \(self.sType) \(self.sNum) 입니다")
        print("이 학생은 \(stype) \(snum) 입니다")
        //새로운 학교(코드, 이름)/학년(고등학교, 1학년) 정보 업데이트 해주기
        UserAPI.shared.changeInfo(phonenumber: "", birthday: "", schoolId: sid, schoolName: sname, schoolNum: snum, schoolType: stype, parentsPhone: "", user : userinfo).done { res in
            if let bool = res.success, bool == true {
                self.authCallback?(self.sName, self.sType, self.sNum, self.userinfo)
                self.dismiss(animated: true)
            }
        }
        
        
    }
    
    
    @objc func pickerExit() {
           /// picker와 같은 뷰를 닫는 함수
           self.view.endEditing(true)
        
        
       }
    

}

//extension SettingSearchSchoolViewController: UIPickerViewDelegate, UIPickerViewDataSource {
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        return 1
//    }
//
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        return list[row]
//    }
//
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        return list.count
//    }
//
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        print(list[row])
//    }
//}
