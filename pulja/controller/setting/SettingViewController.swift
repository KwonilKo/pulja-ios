//
//  SettingViewController.swift
//  pulja
//
//  Created by 고권일 on 2021/12/29.
//

import UIKit
import Alamofire
import PromiseKit
import ChannelIOFront

class SettingViewController: PuljaBaseViewController  {
    
    
    @IBOutlet var firstMangeView: UIView!
    
    @IBOutlet var phoneManageView: UIView!
    
    
    @IBOutlet var liveChattingManageView: UIView!
    
    
    @IBOutlet var serviceFirstView: UIView!
    
    
    @IBOutlet var serviceSecondView: UIView!
    
    
    @IBOutlet var serviceForthView: UIView!
    @IBOutlet var serviceThirdView: UIView!
    
    @IBOutlet var firstStackView: UIStackView!
    
    @IBOutlet weak var chatSwitch: UISwitch!
    
    @IBOutlet var secondStackView: UIStackView!
    
    @IBOutlet weak var studySwitch: UISwitch!
    
    
    @IBOutlet var thirdStackView: UIStackView!
    @IBOutlet weak var marketSwitch: UISwitch!
    
    var marketSwitchBool: Bool?
    
    
    @IBOutlet weak var lbVersionInfo: UILabel!
    
    //@IBOutlet weak var btUpdate: UIButton!
    @IBOutlet weak var btUpdate: UIImageView!
    
    @IBOutlet weak var btUpdateWidth: NSLayoutConstraint!
    
    
    @IBOutlet var magaStudyAlarmView: UIView!
    
    
    @IBOutlet var studyAlarmLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        self.firstStackView.addHorizontalSeparators(color: .Foggy)
        //        self.secondStackView.addHorizontalSeparators(color: .Foggy)
        //        self.thirdStackView.addHorizontalSeparators(color: .Foggy)
        
        // Do any additional setup after loading the view.
        chatSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        studySwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        marketSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        
        //학습관리 알림 설정 모두 제공
        //2022.05.16 이정훈
        //        if self.myInfo?.userType == "C" {
        //            self.magaStudyAlarmView.isHidden = false
        //        } else {
        //            self.magaStudyAlarmView.isHidden = true
        //        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkAppVer()
        LoadingView.show()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
            self.getUserNotification()
        }
    }
    
    private func getUserNotification() {
        UserAPI.shared.userNotification(userSeq: Int(myInfo!.userSeq)).done { res in
            if let suc = res.success, suc == true {
                guard let data = res.data else { return }
                //                print("chatting: \(data.chatting!)")
                //                print("studying: \(data.study!)")
                //                print("marketing: \(data.marketing!)")
                
                let chatswitch = (data.chatting ?? "Y" == "Y") ? true : false
                let studyswitch = (data.study ?? "Y"  == "Y") ? true : false
                let marketswitch = (data.marketing ?? "Y" == "Y") ? true : false
                self.marketSwitchBool = marketswitch
                
                self.chatSwitch.setOn(chatswitch, animated: false)
                self.studySwitch.setOn(studyswitch, animated: false)
                self.marketSwitch.setOn(marketswitch, animated: false)
            }
        }.catch { error in
            LoadingView.hide()
        }.finally{
            LoadingView.hide()
        }
    }
    
    @IBAction func callPhone(_ sender: Any) {
        let phone = "02-3415-0736"
        guard let number = URL(string: "tel://" + phone) else { return }
        UIView.animate(withDuration: 0.1, animations: {
            self.phoneManageView.backgroundColor = UIColor.Foggy.withAlphaComponent(0.6)
        }, completion: {finished in
            self.phoneManageView.backgroundColor = UIColor.white
            UIApplication.shared.open(number)
        })
        
    }
    
    
    @IBAction func connectChannel(_ sender: Any) {
        
        UIView.animate(withDuration: 0.1, animations: {
            self.liveChattingManageView.backgroundColor = UIColor.Foggy.withAlphaComponent(0.6)
        }, completion: {finished in
            self.liveChattingManageView.backgroundColor = UIColor.white
            self.initChennelTalk()
        })
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func serviceButton(_ sender: Any) {
        
        UIView.animate(withDuration: 0.1, animations: {
            self.serviceFirstView.backgroundColor = UIColor.Foggy.withAlphaComponent(0.6)
        }, completion: {finished in
            self.serviceFirstView.backgroundColor = UIColor.white
            let vc = R.storyboard.main.idpwdViewController()!
            vc.viewTitle = "서비스 이용 약관"
            vc.url = "https://slow-process-b56.notion.site/In-Progress-9af3811274b44464af87d5ff1d40db80"
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true) {
                
            }
        })
    }
    
    @IBAction func privacyButton(_ sender: Any) {
        UIView.animate(withDuration: 0.1, animations: {
            self.serviceSecondView.backgroundColor = UIColor.Foggy.withAlphaComponent(0.6)
        }, completion: {finished in
            self.serviceSecondView.backgroundColor = UIColor.white
            let vc = R.storyboard.main.idpwdViewController()!
            vc.viewTitle = "개인 정보 처리 방침"
            vc.url = "https://slow-process-b56.notion.site/In-Progress-cc2ac9057d2e4c5e92a51ae87cf00dd9"
            vc.modalPresentationStyle = .fullScreen
            
            self.present(vc, animated: true) {
                
            }
        })
    }
    
    @IBAction func chatSwitchActivity(_ sender: Any) {
        
    }
    
    @IBAction func studySwitchActivity(_ sender: Any) {
        let result = (studySwitch.isOn) ? "Y" : "N"
        UserAPI.shared.userNotificationUpdate(userSeq: Int(myInfo!.userSeq), chatting: "", marketing: "", notice: "", study: result).done { res in
            if let suc = res.success, suc == true {
                print("성공적으로 바꿈")
            } else {
                print("에러남")
            }
        }
    }
    
    
    @IBAction func firstManageButtonPressed(_ sender: Any) {
        
        UIView.animate(withDuration: 0.1, animations: {
            self.firstMangeView.backgroundColor = UIColor.Foggy.withAlphaComponent(0.6)
        }, completion: {finished in
            self.firstMangeView.backgroundColor = UIColor.white
            let vc = R.storyboard.setting.settingUserInfoViewController()!
            self.navigationController?.pushViewController(vc, animated: true)
            
        })
    }
    
    
    
    
    
    @IBAction func marketingSwitchActivity(_ sender: UISwitch) {
        
        let nowDate = Date().toString(.noDotdate).toDate(.noDotdate)
        let stringDate = nowDate.toString(.dotdate)
        
        var result : String = ""
        sender.setOn(marketSwitchBool!, animated: true)
        
        if marketSwitchBool! {
            result = "N"
            self.alert(title: "마케팅 정보 수신 동의 철회", message: "수강권 할인 등 혜택에 대한\n알림을 더 이상 받을 수 없어요.", button: "다음에", subButton: "철회하기").done { res in
                if !res {
                    UserAPI.shared.userNotificationUpdate(userSeq: Int(self.myInfo!.userSeq), chatting: "", marketing: result, notice: "", study: "").done { res in
                        if let suc = res.success, suc == true {
                            self.showToast(message: "\(stringDate) 마케팅 정보 수신 동의 처리가 철회되었어요.")
                            sender.setOn(false, animated:true)
                            self.marketSwitchBool = false
                        }
                    }
                }
            }
        } else {
            result = "Y"
            self.marketSwitchBool = true
            sender.setOn(true, animated: true)
            UserAPI.shared.userNotificationUpdate(userSeq: Int(myInfo!.userSeq), chatting: "", marketing: result, notice: "", study: "").done { res in
                if let suc = res.success, suc == true {
                    self.showToast(message: "\(stringDate) 마케팅 정보 수신 동의 처리가 완료되었어요.")
                    
                }
            }
        }
    }
    
    
    
    
    
    @IBAction func openSourceButton(_ sender: Any) {
        UIView.animate(withDuration: 0.1, animations: {
            self.serviceThirdView.backgroundColor = UIColor.Foggy.withAlphaComponent(0.6)
        }, completion: {finished in
            self.serviceThirdView.backgroundColor = UIColor.white
            let vc = R.storyboard.main.idpwdViewController()!
            vc.viewTitle = "오픈소스 라이선스"
            vc.url = "https://slow-process-b56.notion.site/In-Progress-3ec186dec0ce46b4a1b47c6403992629"
            vc.modalPresentationStyle = .fullScreen
            
            
            self.present(vc, animated: true) {
                
            }
        })
        
        
        
        
    }
    
    @IBAction func updateVersionButton(_ sender: Any) {
        if !btUpdate.isHidden {
            self.openAppStore()
        }
    }
    
    
    func checkAppVer(){
        if let currentVersion = CheckUpdate.shared.getBundle(key: "CFBundleShortVersionString") {
            
            
            self.lbVersionInfo.text = "ver\(currentVersion)"
            self.lbVersionInfo.textColor = R.color.puljaBlack()
            self.btUpdate.isHidden = true
            self.btUpdateWidth.constant = 0
            
            _ =  CheckUpdate.shared.getAppInfo { (info, error) in
                if let appStoreAppVersion = info?.version {
                    if let error = error {
                        print("error getting app store version: ", error)
                    } else if appStoreAppVersion == currentVersion {
                        print("Already on the last app version: ",currentVersion)
                    } else {
                        
                        print("Needs update: AppStore Version: \(appStoreAppVersion) > Current version: ",currentVersion)
                        DispatchQueue.main.async {
                            self.lbVersionInfo.text = "업데이트가 필요해요."
                            self.lbVersionInfo.textColor = R.color.purple()
                            self.btUpdateWidth.constant = 24
                            self.btUpdate.isHidden = false
                            //                                   let gesture1 = UITapGestureRecognizer(target: self, action: #selector(self.openAppStore(sender: )))
                            //self.lbVersionInfo.isUserInteractionEnabled = true
                            //self.lbVersionInfo.addGestureRecognizer(gesture1)
                        }
                        
                        
                        
                    }
                }
            }
        }
        
    }
    
    @objc func openAppStore() {
        
        guard let url = URL(string: Const.MARKET_URL) else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    func showToast(message: String) {
        var heMinus : CGFloat = 96.0
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.first
            let top = window?.safeAreaInsets.top
            let bottom = window?.safeAreaInsets.bottom
            heMinus += bottom!
            print("top : \(String(describing: top))")
            print("bottom : \(String(describing: bottom))")
            
        } else if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let top = window?.safeAreaInsets.top
            let bottom = window?.safeAreaInsets.bottom
            heMinus += bottom!
            print("top : \(String(describing: top))")
            print("bottom : \(String(describing: bottom))")
        }
        
        
        let width = self.view.frame.size.width
        let toastLabel = UILabel(frame: CGRect(x: 12, y: self.view.frame.size.height - heMinus, width: width - 24, height: 35))
        
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        toastLabel.textAlignment = .center
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10
        toastLabel.clipsToBounds = true
        //        toastLabel.translatesAutoresizingMaskIntoConstraints = false
        //        toastLabel.layer.position = CGPoint(x: self.view.frame.midX, y: self.view.frame.maxY - 40)
        //        toastLabel.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 30).isActive = true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: { toastLabel.alpha = 0.0 }, completion: {(isCompleted) in toastLabel.removeFromSuperview() })
        
    }
    
    func initChennelTalk(){
        guard let info = self.myInfo else { return }
        guard let userName = info.username else { return }
        guard let userId = info.hashUserSeq else { return }
        
        let profile = Profile()
            .set(name: userName)
        
        let buttonOption = ChannelButtonOption.init(
            position: .right,
            xMargin: 16,
            yMargin: 23
        )
        
        let bootConfig = BootConfig.init(
            pluginKey: Const.CHANNELTALK_PLUGIN_KEY,
            memberId: userId,
            //          memberHash: "",
            profile: profile,
            channelButtonOption: buttonOption,
            hidePopup: false,
            trackDefaultEvent: true,
            language: .korean
        )
        
        ChannelIO.boot(with: bootConfig) { (bootStatus, user) in
            if bootStatus == .success, let user = user {
                // success
                ChannelIO.showMessenger()
            } else {
                // show failed reason from bootStatus
            }
        }
    }
}
