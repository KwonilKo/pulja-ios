//
//  SettingPhoneViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/01/03.
//

import UIKit
import MaterialComponents.MaterialBottomSheet
import PromiseKit


class SettingPhoneViewController: PuljaBaseViewController, UITextFieldDelegate, MDCBottomSheetControllerDelegate {
    
    
    @IBOutlet var changeButton: UIButton!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var tfPhoneNumber: UITextField!
    
    @IBOutlet weak var keyboardHeightLayoutConstraint: NSLayoutConstraint!
    var isCheckInput = false
    var userinfo : User?
    var userName : String = ""
    var userPhone : String  = ""
    
    var authCallback : ((_ phonenumber : String?, _ userinfo : User?) -> Void)?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        changeButton.isUserInteractionEnabled = false
        // Do any additional setup after loading the view.
        self.phone.text = self.userPhone
        self.tfPhoneNumber.layer.cornerRadius = 10.0
        self.tfPhoneNumber.layer.borderWidth = 0.2
        self.tfPhoneNumber.layer.backgroundColor = UIColor.white.cgColor
        self.tfPhoneNumber.layer.borderColor = UIColor.lightBlueGrey.cgColor
//        self.tfPhoneNumber.textColor = UIColor.lightBlueGrey
        self.tfPhoneNumber.setPadding(left: 12, right: 12)
        self.tfPhoneNumber.keyboardType = .numberPad
        self.tfPhoneNumber.addDoneButtonOnKeyboard()
        self.tfPhoneNumber.delegate = self
        
        NotificationCenter.default.addObserver(self,
               selector: #selector(self.keyboardNotification(notification:)),
               name: UIResponder.keyboardWillChangeFrameNotification,
               object: nil)
        
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
      }
    
    @objc func keyboardNotification(notification: NSNotification) {
        guard let userInfo = notification.userInfo else { return }

        let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        let endFrameY = endFrame?.origin.y ?? 0
        let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
        let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
        let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
        let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)

        if endFrameY >= UIScreen.main.bounds.size.height {
          self.keyboardHeightLayoutConstraint?.constant = 20.0
        } else {
          self.keyboardHeightLayoutConstraint?.constant = endFrame?.size.height ?? 0.0
        }

        UIView.animate(
          withDuration: duration,
          delay: TimeInterval(0),
          options: animationCurve,
          animations: { self.view.layoutIfNeeded() },
          completion: nil)
      }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func ibBack(_ sender: Any) {
        print("뒤로 가는 버튼 클릭함")
        self.navigationController?.popViewController(animated: true)
    }
    
    
//    @IBAction func phoneChange(_ sender: UITextField) {
//        print("내전화번호여기는:?")
//        if sender == tfPhoneNumber {
//           if let phoneCount = tfPhoneNumber.text?.count {
//               if phoneCount > 0 {
//                   isCheckInput = true
//               } else {
//                   isCheckInput = false
//               }
//           }
////            agreeCheck()
//       }
//    }

    @IBAction func phoneAuthReq(_ sender: Any) {
        
        guard let phone = tfPhoneNumber.text else {
            return
        }
        
        if !isPhone(candidate: phone) {
            alert(title: "전화번호를 확인해주세요.", message: "전화번호를 올바른 형식으로\n입력했는지 확인해주세요.", button: "다시 시도하기")
            return

        }
        
        userPhone = phone.replacingOccurrences(of: "-", with: "")
        self.reqAuthCode()
    
    
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("내전화번호여기는:?")
        if textField == tfPhoneNumber {
           if let phoneCount = tfPhoneNumber.text?.count {
               if phoneCount == 13 {
                   print("참")
                   isCheckInput = true
                   self.changeButton.isUserInteractionEnabled = true
                   self.changeButton.backgroundColor = UIColor.Purple
                   
               } else {
                   print("거짓")
                   isCheckInput = false
                   self.changeButton.isUserInteractionEnabled = false
                   self.changeButton.backgroundColor = UIColor.Foggy
               }
           }
//            agreeCheck()
       }
        
    }
    
    func reqAuthCode(){
        let authCode = AuthCodeReq(cert: nil, email: nil, name: userName, phone: userPhone, service: "join", type: "P")

                SignAPI.shared.authCodeReq(authCode: authCode).done { res in
                    if res.success ?? false {
                        self.openAuthCode(isNew:true, timerNum:0)
                    }
                }.catch { err in
                    self.alert(title: nil, message: err.localizedDescription, button: "다시 시도하기").done { b in
                        if b {
                            self.reqAuthCode()
                        }
                    }
                }
    }
    
    func openAuthCode(isNew: Bool, timerNum: Int ){
        
        let vc = R.storyboard.main.authCodeViewController()!
        
        vc.authCodeReq = AuthCodeReq(cert: nil, email: nil, name: userName, phone: userPhone, service: "join", type: "P")
        vc.isNew = isNew
        if timerNum > 0 {
            vc.timerNum = timerNum
        }
        

        vc.authCallback = { resultCode in
                   print("callback \(resultCode)")
       //            self.memoBottomSheet(idx)
            
            //타이머 종료 후 처리...
            if resultCode == "expire" {
            
                self.alert(title: "인증번호가 만료됐어요.", message: "3분이 지나 인증번호가 만료됐어요.\n인증번호를 다시 한번 받아주세요.", button: "인증번호 다시 받기").done { b in
                            print("authCallback: \(b)")
                        if b {
                            self.showToast(message: "인증번호를 다시 보냈어요.")
                            self.reqAuthCode()
                        }
                    }
                
            } else if resultCode == "success" {
                print("여기 성공적으로 들어옴")
                //새로운 핸드펀 번호 업데이트 해주기
                print(self.tfPhoneNumber.text)
                UserAPI.shared.changeInfo(phonenumber: self.userPhone, birthday: "", schoolId: "", schoolName: "", schoolNum: -1, schoolType: "", parentsPhone: "", user : self.userinfo).done { res in
                    if let bool = res.success, bool == true {
                        self.navigationController?.popViewController(animated: true, completion: {
                            self.authCallback?(self.tfPhoneNumber.text, self.userinfo)
                        })
//                        self.navigationController?.popViewController(animated: true)
//                        self.alert(title: nil, message: "전화번호가 변경되었습니다", button: "확인")
                    } else {
                        print("인증번호는 맞았지만 전화번호 바뀜 안됨")
                    }
                }
                
//                firstly {
//                    UserAPI.shared.myInfo().done {
//                        res in
//                        if let bool = res.success, bool == true{
//                            guard let temp_user = res.data else {
//                                return
//                            }
//                            user = temp_user
//                            user?.phonenumber = self.userPhone
//                        }
//                    }
//                }.then{
//                    UserAPI.shared.changeInfo(phonenumber: "", birthday: convert_birth, schoolId: "", schoolName: "", schoolNum: -1,schoolType: "", parentsPhone: "", user : userinfo).done { res in
//                        if let bool = res.success, bool == true {
//                            self.authCallback?(self.sDate.text, self.userinfo)
//                            self.dismiss(animated: true)
//                        }
//                    }
//
//                    UserAPI.shared.changeInfo(phonenumber: self.userPhone, birthday: "", schoolId: "", schoolName: "", schoolNum: -1,schoolType: "", parentsPhone: "", user : user)
//                }.done { res in
//                    print("성공 여부 출력: \(res.success)")
//                    if let bool = res.success, bool == true {
//                        print("전화번호 성공적으로 바뀜")
//                        self.navigationController?.popViewController(animated: true)
//                        self.alert(title: nil, message: "전화번호가 변경되었습니다", button: "확인")
//                    }
//                }
            
                
            } else if resultCode == "retry" {
                
//                self.showToast(message: "인증번호를 다시 보냈어요")
                self.reqAuthCode()
                
//                self.alert(title: "인증번호를 다시 보냈어요.", message: "받은 인증번호는 3분이 지나면 만료되니,\n그 뒤에는 인증번호를 다시 받아주세요.", button: "인증번호 입력하기").done { b in
//                            print("authCallback: \(b)")
//                        if b {
//                            self.reqAuthCode()
//                        }
//                    }

                
                
            } else if let sec = Int(resultCode) {
                
                self.alert(title: "인증번호를 확인해주세요.", message: "입력하신 번호가 발송된 번호와 다르거나,\n받은 지 3분이 지나 만료된 번호예요.", button: "인증번호 다시 받기", subButton: "다시 시도하기").done { b in
                    if b {
                        self.reqAuthCode()
                        self.showToast(message: "인증번호를 다시 보냈어요.")
                    } else {
                        self.openAuthCode(isNew: false, timerNum: sec)
                    }
                }
            }
        }
                       
        // MDC 바텀 시트로 설정
        let bottomSheet = MDCBottomSheetController(contentViewController: vc)
        bottomSheet.mdc_bottomSheetPresentationController?.preferredSheetHeight = 353
        bottomSheet.delegate = self
        
                       // 보여주기
        self.present(bottomSheet, animated: true, completion: nil)
    }
    
    
    
    
    // MARK: - Validate
    
    func isPhone(candidate: String) -> Bool {
        
        let number = candidate.replacingOccurrences(of: "-", with: "")
        let regex = "^01([0-9])([0-9]{3,4})([0-9]{4})$"
               
        return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: number)
    }
    
    
    func showToast(message: String) {
        var heMinus : CGFloat = 330.0
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.first
            let top = window?.safeAreaInsets.top
            let bottom = window?.safeAreaInsets.bottom
            heMinus += bottom!
            print("top : \(String(describing: top))")
            print("bottom : \(String(describing: bottom))")
            
        } else if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let top = window?.safeAreaInsets.top
            let bottom = window?.safeAreaInsets.bottom
            heMinus += bottom!
            print("top : \(String(describing: top))")
            print("bottom : \(String(describing: bottom))")
        }
        
        
        
        
        let width = self.view.frame.size.width
        let toastLabel = UILabel(frame: CGRect(x: 10, y: self.view.frame.size.height - heMinus, width: width - 20, height: 35))
        
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        toastLabel.textAlignment = .center
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10
        toastLabel.clipsToBounds = true
//        toastLabel.translatesAutoresizingMaskIntoConstraints = false
//        toastLabel.layer.position = CGPoint(x: self.view.frame.midX, y: self.view.frame.maxY - 40)
//        toastLabel.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 30).isActive = true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: { toastLabel.alpha = 0.0 }, completion: {(isCompleted) in toastLabel.removeFromSuperview() })
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        CommonUtil.tabbarController?.tabBar.isHidden = true
        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        CommonUtil.coachTabbarController?.tabBar.isHidden = true
        CommonUtil.coachTabbarController?.bottomNavBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        CommonUtil.tabbarController?.tabBar.isHidden = true
//        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        CommonUtil.tabbarController?.tabBar.isHidden = false
        CommonUtil.coachTabbarController?.tabBar.isHidden = false
        CommonUtil.coachTabbarController?.bottomNavBar.isHidden = false
    
    }

}



    
// MARK: - extension
extension SettingPhoneViewController {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == tfPhoneNumber {
            textField.formatPhoneNumber(range: range, string: string)
            
            
            if let phoneCount = tfPhoneNumber.text?.count {
                if phoneCount == 13 {
                    print("참")
                    isCheckInput = true
                    self.changeButton.isUserInteractionEnabled = true
                    self.changeButton.backgroundColor = UIColor.Purple
                    
                } else {
                    print("거짓")
                    isCheckInput = false
                    self.changeButton.isUserInteractionEnabled = false
                    self.changeButton.backgroundColor = UIColor.Foggy
                }
            }
            
            
            return false
        }
        
        return true
    }
}

    

