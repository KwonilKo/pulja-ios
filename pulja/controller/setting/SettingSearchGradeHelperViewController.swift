//
//  SettingSearchGradeHelperViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/01/05.
//

import UIKit

class SettingSearchGradeHelperViewController: UIViewController,
UIPickerViewDelegate, UIPickerViewDataSource
{
    let totalCount = 6
    let pickerViewCnt = 1
    
    var grades = ["중학교 1학년", "중학교 2학년", "중학교 3학년","고등학교 1학년", "고등학교 2학년", "고등학교 3학년", "검정고시", "N수"]
    var authCallback : ((_ sType : String?, _ sNum : String?) -> Void)?
    
    
    @IBOutlet weak var sGrade: UITextField!
    
    
    @IBOutlet weak var pickerView: UIPickerView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.sGrade.layer.cornerRadius = 10.0
        self.sGrade.layer.borderWidth = 0.2
        self.sGrade.layer.backgroundColor = UIColor.puljaPaleGrey.cgColor
        self.sGrade.layer.borderColor = UIColor.puljaPaleGrey.cgColor
        self.sGrade.setPadding(left: 12, right: 12)
        self.sGrade.textColor = UIColor.lightBlueGrey
        self.sGrade.addDoneButtonOnKeyboard()
        self.sGrade.isUserInteractionEnabled = false
    
    }
    
    @IBAction func sGradeSubmit(_ sender: Any) {
        let sType : String?
        let sNum : String?
        
        switch self.sGrade.text {
        case "중학교 1학년":
            sType = "중학교"
            sNum = "1학년"
        case "중학교 2학년":
            sType = "중학교"
            sNum = "2학년"
        case "중학교 3학년":
            sType = "중학교"
            sNum = "3학년"
        case "고등학교 1학년":
            sType = "고등학교"
            sNum = "1학년"
        case "고등학교 2학년":
            sType = "고등학교"
            sNum = "2학년"
        case "고등학교 3학년":
            sType = "고등학교"
            sNum = "3학년"
        case "검정고시":
            sType = ""
            sNum = "검정고시"
        default:
            sType = ""
            sNum = "N수"
        }
        authCallback?(sType, sNum)
        dismiss(animated: true)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func close(_ sender: Any) {
        dismiss(animated: true) { }
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return pickerViewCnt
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return grades.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return grades[row]

    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? { return NSAttributedString(string: self.grades[row], attributes: [.foregroundColor:UIColor.puljaBlack])
        
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        sGrade.text = grades[row]
    }
    
    

}
