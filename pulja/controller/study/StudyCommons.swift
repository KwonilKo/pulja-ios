//
//  StudyCommons.swift
//  pulja
//
//  Created by 고권일 on 2022/01/18.
//

import Foundation
import UIKit

public class StudyCommons {
    
    struct userResponse {  //number, addSeq 필요함 
        var qId: Int?
        var answer: String?
        var isMultipleChoice : Bool?
        var isMultipleAnswers : Bool?
        var number: Int?
        var addSeq : Int? //무료기능인 경우 problemSeq와 동일 
    }
    
    
    
    static let shared = StudyCommons()
    var delegate : CustomCurrViewDelegate?
    var myCurriculum : [CurriculumSimpleInfo]?
    var mainStackView : UIStackView!
    var restOfC: Int?
    var touchedIndex: Int?
    
    //내 커리큘럼, 진행중/종료 커리큘럼 뿌려주는 helper function 
    func initStackView(mainStackView : UIStackView!, myCurriculum : [CurriculumSimpleInfo]?, isBool: Bool, restOfC: Int?, isCompleted  : Bool = false) -> UIStackView! {
        print("여기 델리게이트 : \(self.delegate)")
        self.restOfC = restOfC
        self.mainStackView = mainStackView
        self.myCurriculum = myCurriculum
        let fruitList = ["Avocado", "Peach", "Cucumber", "Kiwi", "Banana", "Apple", "GreenApple"]
        
        //터치 이벤트 추가
        let gesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender: )))
        mainStackView.addGestureRecognizer(gesture)
        
//        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longTap))
//        longGesture.minimumPressDuration = 0.3
//        mainStackView.addGestureRecognizer(longGesture)
        
        if myCurriculum?.count ?? 0 < 1
        {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor.white
            view.heightAnchor.constraint(equalToConstant: 66).isActive = true
            view.cornerRadius = 10
            view.borderWidth = 0.2
            view.borderColor = UIColor.white
            
            // 과일 이미지 추가
            let imageView = UIImageView()
            view.addSubview(imageView)
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 12).isActive = true
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 12).isActive = true
            imageView.image = UIImage(resource: R.image.iconNoCurri)
            
            let lbunit = UILabel()
            
            lbunit.font = UIFont.fontWithName(type: .regular, size: 14)
            lbunit.textColor = UIColor.Dust
            view.addSubview(lbunit)
            lbunit.translatesAutoresizingMaskIntoConstraints = false
            lbunit.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 12).isActive = true
            lbunit.centerYAnchor.constraint(equalTo: imageView.centerYAnchor).isActive = true
            if isCompleted
            {
                lbunit.text = "종료된 커리큘럼이 없어요."
            }
            else
            {
                lbunit.text = "진행 중인 커리큘럼이 없어요."
            }
            
            
            mainStackView.addArrangedSubview(view)
        }
        else
        {
            for (idx, curriculum) in myCurriculum!.enumerated() {
                let isend = curriculum.isEndCurriculum ?? -1
                if ((idx < 2 && isBool) || !isBool) {
                    //메인 뷰 추가
                    let view = UIView()
                    view.translatesAutoresizingMaskIntoConstraints = false
                    view.backgroundColor = UIColor.white
                    view.heightAnchor.constraint(equalToConstant: 66).isActive = true
                    view.cornerRadius = 10
                    view.borderWidth = 0.2
                    view.borderColor = UIColor.white
                    
                    // 과일 이미지 추가
                    let imageView = UIImageView()
                    view.addSubview(imageView)
                    imageView.translatesAutoresizingMaskIntoConstraints = false
                    imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 12).isActive = true
                    imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 12).isActive = true
                    imageView.image = UIImage.init(named: "\(fruitList[idx % 7]).pdf")
                    
                    //과목/대단원 명 추가
                    let lbunit = UILabel()
                    
                    lbunit.font = UIFont.init(name:"AppleSDGothicNeo-Bold", size:14)
                    lbunit.textColor = UIColor.puljaBlack
                    view.addSubview(lbunit)
                    lbunit.translatesAutoresizingMaskIntoConstraints = false
                    lbunit.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 12).isActive = true
                    lbunit.topAnchor.constraint(equalTo: view.topAnchor, constant: 15).isActive = true
                    lbunit.text = "\(curriculum.subjectName!) | \(curriculum.unit1Name!)"
                    
                    //강의 수 문제 수 추가
                    let lblecProb = UILabel()
                    lblecProb.font = UIFont(name:"AppleSDGothicNeo-Bold", size:12)
                    lblecProb.textColor = UIColor.puljaRed
                    view.addSubview(lblecProb)
                    lblecProb.translatesAutoresizingMaskIntoConstraints = false
                    lblecProb.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 12).isActive = true
                    lblecProb.topAnchor.constraint(equalTo: lbunit.bottomAnchor, constant: 4).isActive = true
                   
                    let rvt = curriculum.restVideoCnt ?? -1
                    let rpt = curriculum.restProblemCnt ?? -1
                    if( rvt == 0 && rpt == 0) {
                        lblecProb.textColor = UIColor(red: 41.0 / 255.0 , green: 41.0 / 255.0, blue: 41.0 / 255.0, alpha: 0.6)
                        imageView.image = UIImage.init(named: "\(fruitList[idx % 7]).pdf")
                        lblecProb.text = "학습 완료"
                    } else {
                        imageView.image = UIImage.init(named: "\(fruitList[idx % 7])New.pdf")
                        lblecProb.text = "강의 \(rvt), 문제 \(rpt)개 미학습"
                    }
    //                if curriculum.isEndCurriculum == 1 {
    //                    lblecProb.textColor = UIColor(red: 41.0 / 255.0 , green: 41.0 / 255.0, blue: 41.0 / 255.0, alpha: 0.6)
    //                    imageView.image = UIImage.init(named: "\(fruitList[idx % 7]).pdf")
    //                    lblecProb.text = "학습 완료"
    //                } else {
    //                    imageView.image = UIImage.init(named: "\(fruitList[idx % 7])New.pdf")
    //                    lblecProb.text = "강의 \(curriculum.restVideoCnt!), 문제 \(curriculum.restProblemCnt!)개 미학습"
    //                }
                    
                    mainStackView.addArrangedSubview(view)
                }
            }
        }
        
        
        if restOfC != nil {
            // 뷰 추가
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor.white
            view.heightAnchor.constraint(equalToConstant: 42).isActive = true
            view.cornerRadius = 10
            view.borderWidth = 0.2
            view.borderColor = UIColor.white
            
            // 이미지 추가
            let imageView = UIImageView()
            view.addSubview(imageView)
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 12).isActive = true
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 22).isActive = true
            imageView.image = UIImage.init(named: "More2.pdf")

            // 텍스트 추가
            let lblecProb = UILabel()
            lblecProb.font = UIFont(name:"AppleSDGothicNeo-Bold", size:12)
            lblecProb.textColor = UIColor.puljaBlack
            view.addSubview(lblecProb)
            lblecProb.translatesAutoresizingMaskIntoConstraints = false
            lblecProb.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 24).isActive = true
            lblecProb.topAnchor.constraint(equalTo: view.topAnchor, constant: 8).isActive = true
            lblecProb.textColor = UIColor(red: 41.0 / 255.0 , green: 41.0 / 255.0, blue: 41.0 / 255.0, alpha: 0.4)
            lblecProb.text = "커리큘럼 \(restOfC!)개가 더 있어요."
            mainStackView.addArrangedSubview(view)
        }
        
        return mainStackView

    }
    
    //터치 이벤트시, 커리큘럼 상세 화면으로 보내기
    @objc func handleTap(sender: UITapGestureRecognizer) {
        let location = sender.location(in: mainStackView)
        var locationInView = CGPoint.zero
        let subViews = mainStackView.subviews
        print("length of mainStackView is \(subViews.count)")
        for subView in subViews {
            locationInView = subView.convert(location, from: mainStackView)
            if subView.isKind(of: UIView.self) {
                if subView.point(inside: locationInView, with: nil) {
                    // this view contains that point
                    print("터치한 인덱스는 \(subViews.firstIndex(of: subView))")
                    self.touchedIndex = subViews.firstIndex(of: subView)

                    //기능
                    if (subViews.firstIndex(of: subView) != 0  &&
                        subViews.firstIndex(of: subView)!-1 < self.myCurriculum!.count)  {
                        let cuInfo = self.myCurriculum![subViews.firstIndex(of: subView)!-1] //수정 필요
                        self.delegate?.didTouchView(cuInfo, self.touchedIndex!)
                        break;
                    } else if (self.restOfC != nil && subViews.firstIndex(of: subView) != 0)  {
                        self.delegate?.didShowNext(true, self.touchedIndex!)
                       break;
                   }
                    
                    
                }
            }
        }
    }
    
    
//    @objc func longTap(sender: UIGestureRecognizer) {
//        print("Long tap")
//        if sender.state == .ended {
//            print("UIGestureRecognizerStateEnded")
//            //Do Whatever You want on End of Gesture
//            if let i = self.touchedIndex {
//                mainStackView.subviews[i].backgroundColor = UIColor.white
//            }
//            
//        }
//        else if sender.state == .began {
//            print("UIGestureRecognizerStateBegan.")
//            //Do Whatever You want on Began of Gesture
//            let location = sender.location(in: mainStackView)
//            var locationInView = CGPoint.zero
//            let subViews = mainStackView.subviews
//            print("length of mainStackView is \(subViews.count)")
//            for subView in subViews {
//                locationInView = subView.convert(location, from: mainStackView)
//                if subView.isKind(of: UIView.self) {
//                    if subView.point(inside: locationInView, with: nil) {
//                        // this view contains that point
//                        print("터치한 인덱스는 \(subViews.firstIndex(of: subView))")
//                        self.touchedIndex = subViews.firstIndex(of: subView)
//                        mainStackView.subviews[touchedIndex!].backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
//                        break;
//    
//                    }
//                }
//            }
//           
//        }
//    }
    
}

class CustomViewTapGestureRecognizer: UITapGestureRecognizer {
    var touchedIndex: Int?

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesBegan(touches, with: event)
        let location = touches.first?.location(in: self.view)
        var locationInView = CGPoint.zero
        let subviews = self.view?.subviews
        for subview in subviews! {
            locationInView = subview.convert(location!, from : self.view)
            if subview.isKind(of: UIView.self) {
                if subview.frame.contains(location!) {
                    let touchedIdx = subviews?.firstIndex(of: subview)
                    self.touchedIndex = touchedIdx

                    DispatchQueue.main.async {
                        UIView.animate(withDuration: 0.2, animations: {
                            self.view?.subviews[touchedIdx!].backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)}, completion: { finished in
                                self.view?.subviews[touchedIdx!].backgroundColor = UIColor.white
                        })
                    }

//                    if (touchedIdx != 0 && touchedIdx! - 1 < subviews!.count) {
//                        self.view?.subviews[touchedIdx!].backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
//                        break;
//                    } else {
//                        self.view?.subviews[touchedIdx!].backgroundColor = UIColor.white
//                    }
//                    print("터치한 인덱스는 touchedIdx: \(touchedIdx)")
                } else {
                    print("터치영역 밖")
//                    self.view?.subviews[touchedIdx!].backgroundColor = UIColor.white
                }
//                if subview.point(inside: locationInView, with: nil) {
//                    let touchedIdx = subviews?.firstIndex(of: subview)
//                    print("터치한 인덱스는 touchedIdx")
//                }
            }
        }
        
    }
    
    
//    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
//        super.touchesMoved(touches, with: event)
//
//
//        let location = touches.first?.location(in: self.view)
//        var locationInView = CGPoint.zero
//        let subviews = self.view?.subviews
//        for subview in subviews! {
//            locationInView = subview.convert(location!, from : self.view)
//            if subview.isKind(of: UIView.self) {
//                if subview.frame.contains(location!) {
//                    let touchedIdx = subviews?.firstIndex(of: subview)
//                    self.touchedIndex = touchedIdx
//
//                    DispatchQueue.main.async {
//                        UIView.animate(withDuration: 0.2, animations: {
//                            self.view?.subviews[touchedIdx!].backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)}, completion: { finished in
//                                self.view?.subviews[touchedIdx!].backgroundColor = UIColor.white
//                        })
//                    }
//
////                    if (touchedIdx != 0 && touchedIdx! - 1 < subviews!.count) {
////                        self.view?.subviews[touchedIdx!].backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
////                        break;
////                    } else {
////                        self.view?.subviews[touchedIdx!].backgroundColor = UIColor.white
////                    }
////                    print("터치한 인덱스는 touchedIdx: \(touchedIdx)")
//                } else {
//                    print("터치영역 밖")
////                    self.view?.subviews[touchedIdx!].backgroundColor = UIColor.white
//                }
////                if subview.point(inside: locationInView, with: nil) {
////                    let touchedIdx = subviews?.firstIndex(of: subview)
////                    print("터치한 인덱스는 touchedIdx")
////                }
//            }
//        }
//
//
//
//
//
////        if let point: CGPoint = touches.first?.location(in: self.view) {
////            print("view width: \(self.view?.frame.size.width) , height: \(self.view?.frame.size.height)")
////            print("point: ", point)
////            // If the finger was dragged outside of the button...
////            if ((point.x < 0 || point.x > (self.view?.frame.size.width)!) ||
////                (point.y < 0 || point.y > (self.view?.frame.size.height)!)) {
////                // Do whatever you need here
////                self.view?.backgroundColor = UIColor.white
////            } else {
////                self.view?.backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
////            }
////        }
//    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesEnded(touches, with: event)
        
        if let touchedEndIdx = self.touchedIndex {
            self.view?.subviews[touchedEndIdx].backgroundColor = UIColor.white
        }
    }
         
    
//    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent) {
//        super.touchesCancelled(touches, with: event)
//        if highlightOnTouch {
//            self.view?.backgroundColor = UIColor.white
//        }
//    }

}







protocol CustomCurrViewDelegate : AnyObject {
    func didTouchView(_ cuInfo: CurriculumSimpleInfo?, _ idx: Int)
    
    func didShowNext(_ isBool: Bool?, _ idx: Int)
}



extension UIView {
    enum ViewSide: String {
        case Left = "Left", Right = "Right", Top = "Top", Bottom = "Bottom"
    }

    func removeBorder(toSide side: ViewSide) {
        guard let sublayers = self.layer.sublayers else { return }
        var layerForRemove: CALayer?
        for layer in sublayers {
            if layer.name == side.rawValue {
                layerForRemove = layer
            }
        }
        if let layer = layerForRemove {
            layer.removeFromSuperlayer()
        }
    }
}
