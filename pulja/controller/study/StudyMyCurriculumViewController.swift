//
//  StudyMyCurriculumViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/01/17.
//

import UIKit

class StudyMyCurriculumViewController: PuljaBaseViewController, CustomCurrViewDelegate {
    
    
    @IBOutlet var headTitle: UILabel!
    @IBOutlet var diagnosisReportView: UIView!
    
    @IBOutlet weak var ProgressStackView: UIStackView!
    @IBOutlet weak var FinishStackView: UIStackView!
    @IBOutlet weak var btnChat: UIButton!
    var progressCurriculum: [CurriculumSimpleInfo] = []
    var finishCurriculum: [CurriculumSimpleInfo] = []
    
    var ProgressDelegate : CustomCurrViewDelegate?
    var ProgressStudyCommons : StudyCommons?
    
    var FinishDelegate : CustomCurrViewDelegate?
    var FinishStudyCommons : StudyCommons?
    var userSeq : Int?
    var data: [StudentsSchedule]?
    var tag_idx : Int = 0
    var username: String?
    
    @IBOutlet var currView: UIView!
    
    @IBOutlet var finishView: UIView!
    
    @IBOutlet var weeklyReportView: UIView!
    var connectFromCoach: Bool = false 
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
//        self.ListStudyDaysCurr = [:]
//        self.ListStudyDaysExpand = [:]
//        self.OrderedListStudyDaysCurr = [:]
//        self.OrderedListStudyDaysExpand = [:]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.longTapReport(sender:)))
        longGesture.minimumPressDuration = 0.3
        self.diagnosisReportView.addGestureRecognizer(longGesture)
        
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.tapReport(_:)))
        self.diagnosisReportView.addGestureRecognizer(gesture)
        
        
        
        let longGesture2 = UILongPressGestureRecognizer(target: self, action: #selector(self.longTapWeeklyReport(sender:)))
        longGesture2.minimumPressDuration = 0.3
        self.weeklyReportView.addGestureRecognizer(longGesture2)
        
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(self.tapWeeklyReport(_:)))
        self.weeklyReportView.addGestureRecognizer(gesture2)
        
        // Do any additional setup after loading the view.
        
        

    }
    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(animated)
        
        
//        CommonUtil.tabbarController?.tabBar.isHidden = true
//        CommonUtil.tabbarController?.bottomNavBar.isHidden = true

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.connectFromCoach {
            CommonUtil.coachTabbarController?.tabBar.isHidden = false
            CommonUtil.coachTabbarController?.bottomNavBar.isHidden = false
        } else {
            CommonUtil.tabbarController?.bottomNavBar.isHidden = false
            CommonUtil.tabbarController?.tabBar.isHidden = false
        }
    }
    override func viewWillAppear(_ animated: Bool) {
       
        if self.connectFromCoach {
            self.userSeq = data![tag_idx].userSeq
            self.username = data![tag_idx].username
            self.headTitle.text = "\(self.username!) 학생"
            
            CommonUtil.coachTabbarController?.tabBar.isHidden = true
            CommonUtil.coachTabbarController?.bottomNavBar.isHidden = true
            
        } else {
            CommonUtil.tabbarController?.tabBar.isHidden = true
            CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        }
        
        super.viewWillAppear(animated)
        
        
//        self.view.isHidden = true
        self.currView.isHidden = true
        self.finishView.isHidden = true
        self.mainStackInit()
        LoadingView.show()
        //진행중
        self.ProgressStudyCommons = StudyCommons()
        self.ProgressStudyCommons?.delegate = self
        //완료
        self.FinishStudyCommons = StudyCommons()
        self.FinishStudyCommons?.delegate = self
        
        self.progressCurriculum = []
        self.finishCurriculum = []
        
        StudyAPI.shared.myCurriculumList(userSeq: self.userSeq!).done {
            res in
            if let bool = res.success, bool == true {
                guard let data = res.data else { return }
                for ci in data {
                    if ci.isEndCurriculum == 1 {
                        self.finishCurriculum += [ci]
                    } else {
                        self.progressCurriculum += [ci]
                    }
                }
                
                // 진행중인 커리큘럼
                self.ProgressStudyCommons?.myCurriculum = self.progressCurriculum
                self.ProgressStackView = self.ProgressStudyCommons?.initStackView(mainStackView: self.ProgressStackView, myCurriculum: self.progressCurriculum, isBool: false, restOfC: nil)
                
                // 종료된 커리큘럼
                self.FinishStudyCommons?.myCurriculum = self.finishCurriculum
                self.FinishStackView = self.FinishStudyCommons?.initStackView(mainStackView: self.FinishStackView, myCurriculum: self.finishCurriculum, isBool: false, restOfC: nil, isCompleted: true)
        
            }
        }.catch { error in
            print(error.localizedDescription)
        }.finally{
            LoadingView.hide()
            self.currView.isHidden = false
            self.finishView.isHidden = false
        }
        
        
        checkUnreadMessage()
    }
    
    
    func mainStackInit() {
        self.ProgressStackView.subviews[1...].map({ $0.removeFromSuperview() })
        self.FinishStackView.subviews[1...].map({ $0.removeFromSuperview() })
    }
    
    
    func checkUnreadMessage()
    {
        let payType = self.myInfo?.payType ?? "f"
        
        if payType == "p"
        {
            CommonUtil.shared.checkNewMessage { hasNew in
                
                if hasNew > 0
                {
                    self.btnChat.setImage(UIImage(resource:R.image.groupChatNew), for: .normal)
                }
                else
                {
                    self.btnChat.setImage(UIImage(resource:R.image.groupChat), for: .normal)
                }
            }
        }
        else
        {
            self.btnChat.setImage(UIImage(resource:R.image.groupChat), for: .normal)
        }

    }
    @IBAction func btnChatPressed(_ sender: Any) {
      
        
        
        if self.connectFromCoach {
            //CommonUtil.shared.goChatting(viewController: self, userSeq: self.userSeq!)
        } else {
            //CommonUtil.shared.goChatting(viewController: self)
        }
      
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @objc func longTapWeeklyReport(sender: UIGestureRecognizer) {
        print("Long tap")
        if sender.state == .ended {
            print("UIGestureRecognizerStateEnded")
            //Do Whatever You want on End of Gesture
            self.weeklyReportView.backgroundColor = UIColor.white
            
        }
        else if sender.state == .began {
            self.weeklyReportView.backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
            
        }
    }
    
    
    
    @objc func tapWeeklyReport(_ sender : UIGestureRecognizer) {
        let vc = R.storyboard.main.idpwdViewController()!
        vc.viewTitle = "주간 학습 리포트"
        vc.url = "https://www.pulja.co.kr/student_report_mobile?userSeq=" + "\(self.userSeq!)"
        vc.modalPresentationStyle = .overFullScreen
        UIView.animate(withDuration: 0.1, animations: {
            self.weeklyReportView.backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
        }, completion:
            { finished in
            self.weeklyReportView.backgroundColor = UIColor.white
            self.present(vc, animated: true) {

            }
            })
        
        
    }
    
    @objc func longTapReport(sender: UIGestureRecognizer) {
        print("Long tap")
        if sender.state == .ended {
            print("UIGestureRecognizerStateEnded")
            //Do Whatever You want on End of Gesture
            self.diagnosisReportView.backgroundColor = UIColor.white
            
        }
        else if sender.state == .began {
            self.diagnosisReportView.backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
            
        }
    }
    
    
    
    @objc func tapReport(_ sender: UIGestureRecognizer) {
        let vc = R.storyboard.study.diagnosisReportViewController()!
        vc.tabReport = true
        vc.userSeq = self.userSeq
        vc.diagnosisReportFree = true
        vc.connectFromCoach = self.connectFromCoach
        vc.username = self.username
        
        UIView.animate(withDuration: 0.1, animations: {
            self.diagnosisReportView.backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
        }, completion:
            { finished in
            self.diagnosisReportView.backgroundColor = UIColor.white
            self.navigationController?.pushViewController(vc, animated: true)
            })
        
    }
    
    @IBAction func ibBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func didTouchView(_ cuInfo: CurriculumSimpleInfo?, _ idx: Int) {
        var b: Bool?
        let storyboard = UIStoryboard(name: "Study", bundle: nil)
//        let vc = storyboard.instantiateViewController(identifier: "myCurriculumViewController")
        let vc = R.storyboard.study.myCurriculumViewController()!
        
        vc.connectFromCoach = self.connectFromCoach
        vc.userSeq = self.userSeq!
        vc.currSeq = cuInfo!.currSeq!
        print("StudyMyCurriculumViewController, userSeq: \(vc.userSeq), currSeq: \(vc.currSeq)")
        
        UIView.animate(withDuration: 0.1, animations: {
            let result = self.progressCurriculum.filter { e1 in e1.restVideoCnt == cuInfo!.restVideoCnt && e1.unit1Name == cuInfo!.unit1Name && e1.subjectName == cuInfo!.subjectName && e1.restProblemCnt == cuInfo!.restProblemCnt && e1.isEndCurriculum == cuInfo!.isEndCurriculum && e1.currSeq == cuInfo!.currSeq}
            if result.count > 0 {
                b = true
                self.ProgressStackView.subviews[idx].backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
            } else {
                b = false
                self.FinishStackView.subviews[idx].backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
            }
    
        }, completion: { finished in
            if let bool = b, bool {
                self.ProgressStackView.subviews[idx].backgroundColor = UIColor.white
            } else {
                self.FinishStackView.subviews[idx].backgroundColor = UIColor.white
            }
            self.navigationController?.pushViewController(vc, animated: true)
        })
        
    }
    func didShowNext(_ isBool: Bool?, _ idx: Int) {
        
    }
}
