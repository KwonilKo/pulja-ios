//
//  DiagnosisNextMainViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/01/24.
//

import UIKit
class DiagnosisNextMainViewController: UIViewController, UITextFieldDelegate {
    var grade : String?
    
    @IBOutlet weak var tfGrade: UITextField!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    var keyHeight: CGFloat?
    var userSeq : Int?
    var activeTextField : UITextField? = nil
    var diagnosisSeq : Int?
    var schoolGrade : String?
    var delegate : PuljaBaseViewController?
    var fromSeven = false
    var fromStudy = false
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonUtil.tabbarController?.tabBar.isHidden = true
        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
    }
    
    
    override func viewDidLoad() {
        print("여기 들어옴?")
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(DiagnosisNextMainViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        self.tfGrade.addDoneButtonOnKeyboard()
        self.tfGrade.addTarget(self, action: #selector(MultipleChoiceViewController.textFieldDidChange(_:)), for: .editingChanged)
        self.tfGrade.delegate = self
        
    }
    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(animated)
        

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        CommonUtil.tabbarController?.tabBar.isHidden = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        
    }
    @IBAction func touchKeyboard(_ sender: UITextField) {
        
        
    }
    
    
    @IBAction func notKnow(_ sender: Any) {
        self.tfGrade.text = "1"
        self.ibNext(sender)
    }
    
    
//     MARK: - Input
    @objc func keyboardWillShow(notification: NSNotification) {

        if let keyboardFrame: NSValue = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            
            let f1 = self.view.getConvertedFrame(fromSubview: self.tfGrade) ?? CGRect(x: 0, y: 0, width: 0, height: 0)
            let f2 = self.view.getConvertedFrame(fromSubview: self.bottomView) ?? CGRect(x: 0, y: 0, width: 0, height: 0)
            
            let y1 = f1.origin.y + f1.size.height
            let y2 = f2.origin.y - keyboardRectangle.height + 40
           
            if y2 < y1 {
                return
            }
                
            self.bottomConstraint.constant = keyboardRectangle.height - 40
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }

         }
            
         
    }
    
    @IBAction func nextTime(_ sender: Any) {
        if self.fromSeven
        {
            if self.fromStudy
            {
                self.navigationController?.popToRootViewController(animated: true)
            }
            else
            {
                if let vcDetail = R.storyboard.main.tabbarController(){
                    self.navigationController?.setViewControllers([vcDetail], animated: false)
                }
            }
            
        }
        else
        {
            self.navigationController?.popToRootViewController(animated: true)
        }
        
    }
    
    @IBAction func ibBack(_ sender: Any) {
//        self.dismiss(animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func ibNext(_ sender: Any) {
        self.bottomView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        
        let vc = R.storyboard.study.diagnosisFinalMainViewController()!
        vc.delegate = self.delegate 
        vc.schoolGrade = self.schoolGrade
        if (self.tfGrade.text! != "" && self.tfGrade.text != nil && self.tfGrade.text! != "1~9") {
            print("디폴트 아님: \(self.tfGrade.text!)")
            vc.goalGrade = Int(self.tfGrade.text!)
        } else {    //디폴트
            print("디폴드 goalGrade는 1")
            vc.goalGrade = 1
        }
        vc.diagnosisSeq = self.diagnosisSeq
        vc.userSeq = self.userSeq
        vc.fromSeven = self.fromSeven
        vc.fromStudy = self.fromStudy
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        self.checkMaxLength(textField: textField, maxLength: 1)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeTextField = nil
        self.bottomConstraint.constant = -10
    }
    
    func checkMaxLength(textField: UITextField!, maxLength: Int) {
        if (textField.text?.count ?? 0 > maxLength) {
            textField.deleteBackward()
        }
    }

}




