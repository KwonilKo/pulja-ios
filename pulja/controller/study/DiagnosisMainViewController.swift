//
//  DiagnosisMainViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/01/24.
//

import UIKit

class DiagnosisMainViewController: UIViewController {

    @IBOutlet weak var btnNext: DesignableButton!
    @IBOutlet weak var btnMiddleSchool: DesignableButton!
    @IBOutlet weak var btnHighSchool: DesignableButton!
    
    @IBOutlet weak var select4View: UIView!
    @IBOutlet weak var select5View: UIView!
    
    
    @IBOutlet weak var btnChoice1: DesignableButton!
    @IBOutlet weak var btnChoice2: DesignableButton!
    @IBOutlet weak var btnChoice3: DesignableButton!
    @IBOutlet weak var btnChoice4: DesignableButton!
    @IBOutlet weak var btnChoice5: DesignableButton!
    
    
    var selectedTabIndex = 0
    var selectedChoiceIndex = -1

    var userSeq: Int?
    var diagnosisSeq: Int? 
    var delegate : PuljaBaseViewController?
    

    var grade : String?
    var touchedIdx : Int?
    
    var status : [String: [Bool]] = ["middle" : [false, false, false], "high": [false, false, false, false, false]]
    
    var temp :[UILabel] = []
    
    var fromSeven = false
    var fromStudy = false
    
    var userId: String?
    
    @IBOutlet weak var btnLaterTry: UIButton!
    

    
    override func viewDidLoad() {
       
        if self.fromSeven
        {
            self.btnLaterTry.isHidden = false
        }
        viewInit()
    }

    func viewInit()
    {
        select4View.isHidden = true
        select5View.isHidden = true
        btnNext.isEnabled = false
        
        if self.userSeq == nil
        {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let container = appDelegate.persistentContainer
            do {
                let myInfos = try container.viewContext.fetch(MyInfo.fetchRequest()) as! [MyInfo]
                if myInfos.count > 0
                {
                    let myInfo = myInfos[0]
                    self.userSeq = Int(myInfo.userSeq ?? 0)
                    self.userId = myInfo.userId
                }
                
            } catch {
                print(error.localizedDescription)
            }
            
            
        }
        
        if Const.isAirBridge {
            CommonUtil.shared.ABEvent(
                category: "diagnosis_poll_try",
                customs: ["userseq":self.userSeq!, "user_id":self.userId!]
            )
        } else {
            print("[ABLog]", "-----------------------------------------", separator: " ")
            print("[ABLog]", "category: diagnosis_poll_try", separator: " ")
            print("[ABLog]", "userseq: \(self.userSeq ?? 0)", separator: " ")
            print("[ABLog]", "user_id: \(self.userId ?? "")", separator: " ")
            print("[ABLog]", "-----------------------------------------", separator: " ")
        }
        checkDefaultSchoolInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonUtil.tabbarController?.tabBar.isHidden = true
        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
    }
    
    
    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(animated)
        

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        CommonUtil.tabbarController?.tabBar.isHidden = false
    }

    func checkDefaultSchoolInfo()
    {
        if let schoolInfo = CommonUtil.shared.getSchoolInfo()
        {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let container = appDelegate.persistentContainer
            do {
                let myInfos = try container.viewContext.fetch(MyInfo.fetchRequest()) as! [MyInfo]
                if myInfos.count > 0
                {
                    let myInfo = myInfos[0]
                    let userSeq = myInfo.userSeq
                    let key = "\(userSeq)"

                    if let idxSchool = schoolInfo[key]
                    {
                        switch (idxSchool) {
                        case 1 :
                            selectMiddleSchool()
                            setSelect1()
                            break
                        case 2:
                            selectMiddleSchool()
                            setSelect2()
                            break
                        case 3:
                            selectMiddleSchool()
                            setSelect3()
                            break
                        case 4:
                            selectHighSchool()
                            setSelect1()
                            break
                        case 5:
                            selectHighSchool()
                            setSelect2()
                            break
                        case 6:
                            selectHighSchool()
                            setSelect3()
                            break
                        case 7:
                            selectHighSchool()
                            setSelect4()
                            break
                        case 8:
                            selectHighSchool()
                            setSelect5()
                            break
                        default:
                            break
                        }
                        
                    }
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    func selectMiddleSchool()
    {
        selectedTabIndex = 0
        
        btnMiddleSchool.backgroundColor = .OffWhite
        btnMiddleSchool.titleLabel?.font = UIFont.fontWithName(type: .bold, size: 16)
        btnMiddleSchool.setTitleColor(.puljaBlack, for: .normal)
        
        btnHighSchool.backgroundColor = .white
        btnHighSchool.titleLabel?.font = UIFont.fontWithName(type: .bold, size: 16)
        btnHighSchool.setTitleColor(.grey, for: .normal)
        
        resetChoice()

        UIView.performWithoutAnimation {
            btnChoice1.setTitle("중학교 1학년", for: .normal)
            btnChoice2.setTitle("중학교 2학년", for: .normal)
            btnChoice3.setTitle("중학교 3학년", for: .normal)
            btnChoice1.layoutIfNeeded()
            btnChoice2.layoutIfNeeded()
            btnChoice3.layoutIfNeeded()
        }
        
        select4View.isHidden = true
        select5View.isHidden = true
    }
    
    func selectHighSchool()
    {
        selectedTabIndex = 1
        
        btnHighSchool.backgroundColor = .OffWhite
        btnHighSchool.titleLabel?.font = UIFont.fontWithName(type: .bold, size: 16)
        btnHighSchool.setTitleColor(.puljaBlack, for: .normal)
        
        btnMiddleSchool.backgroundColor = .white
        btnMiddleSchool.titleLabel?.font = UIFont.fontWithName(type: .bold, size: 16)
        btnMiddleSchool.setTitleColor(.grey, for: .normal)
        
        resetChoice()
        
        UIView.performWithoutAnimation {
            btnChoice1.setTitle("고등학교 1학년", for: .normal)
            btnChoice2.setTitle("고등학교 2학년", for: .normal)
            btnChoice3.setTitle("고등학교 3학년", for: .normal)
            btnChoice1.layoutIfNeeded()
            btnChoice2.layoutIfNeeded()
            btnChoice3.layoutIfNeeded()
        }
        
        
        
        select4View.isHidden = false
        select5View.isHidden = false
    }
    
    
    func setSelect1()
    {
        selectedChoiceIndex = 0
        
        setSelectChoice(button: btnChoice1)
        resetSingleChoice(button: btnChoice2)
        resetSingleChoice(button: btnChoice3)
        resetSingleChoice(button: btnChoice4)
        resetSingleChoice(button: btnChoice5)
        
        enableNext()
    }
    
    func setSelect2()
    {
        selectedChoiceIndex = 1
        setSelectChoice(button: btnChoice2)
        resetSingleChoice(button: btnChoice1)
        resetSingleChoice(button: btnChoice3)
        resetSingleChoice(button: btnChoice4)
        resetSingleChoice(button: btnChoice5)
        
        enableNext()
    }
    func setSelect3()
    {
        selectedChoiceIndex = 2
        setSelectChoice(button: btnChoice3)
        resetSingleChoice(button: btnChoice1)
        resetSingleChoice(button: btnChoice2)
        resetSingleChoice(button: btnChoice4)
        resetSingleChoice(button: btnChoice5)
        
        enableNext()
    }
    func setSelect4()
    {
        selectedChoiceIndex = 3
        setSelectChoice(button: btnChoice4)
        resetSingleChoice(button: btnChoice1)
        resetSingleChoice(button: btnChoice2)
        resetSingleChoice(button: btnChoice3)
        resetSingleChoice(button: btnChoice5)
        
        enableNext()
    }
    func setSelect5()
    {
        selectedChoiceIndex = 4
        setSelectChoice(button: btnChoice5)
        resetSingleChoice(button: btnChoice1)
        resetSingleChoice(button: btnChoice2)
        resetSingleChoice(button: btnChoice3)
        resetSingleChoice(button: btnChoice4)
        
        enableNext()
    }
    
    
    @IBAction func btnLaterTryPressed(_ sender: Any) {
        
        if self.fromStudy
        {
            self.navigationController?.popToRootViewController(animated: false)
        }
        else
        {
            if let vcDetail = R.storyboard.main.tabbarController(){
                self.navigationController?.setViewControllers([vcDetail], animated: false)
            }
        }
    }
  
    
    
    @IBAction func ibBack(_ sender: Any) {
//        self.dismiss(animated : true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnMiddleSchoolPressed(_ sender: Any) {
        if selectedTabIndex == 0 {
            return
        }
        selectMiddleSchool()
        
    }
    @IBAction func btnHighSchoolPressed(_ sender: Any) {
        
        if selectedTabIndex == 1 {
            return
        }
        selectHighSchool()
    }
    
    
    @IBAction func btnChoice1Pressed(_ sender: Any) {
        if selectedChoiceIndex == 0 {
            return
        }
        setSelect1()
        
    }
    @IBAction func btnChoice2Pressed(_ sender: Any) {
        if selectedChoiceIndex == 1 {
            return
        }
        setSelect2()
    }
    @IBAction func btnChoice3Pressed(_ sender: Any) {
        if selectedChoiceIndex == 2 {
            return
        }
        setSelect3()
    }
    @IBAction func btnChoice4Pressed(_ sender: Any) {
        if selectedChoiceIndex == 3 {
            return
        }
        setSelect4()
    }
    @IBAction func btnChoice5Pressed(_ sender: Any) {
        if selectedChoiceIndex == 4 {
            return
        }
        setSelect5()
    }
    

    
    func resetChoice()
    {
        selectedChoiceIndex = -1
        
        resetSingleChoice(button: btnChoice1)
        resetSingleChoice(button: btnChoice2)
        resetSingleChoice(button: btnChoice3)
        resetSingleChoice(button: btnChoice4)
        resetSingleChoice(button: btnChoice5)
        
        btnNext.setTitleColor(.Dust, for: .normal)
        btnNext.setBackgroundColor(.Foggy, for: .normal)
        btnNext.isEnabled = false
    }
    
    func setSelectChoice(button : UIButton)
    {
        button.setTitleColor(.Purple, for: .normal)
        button.layer.borderColor = UIColor.Purple.cgColor
        button.layer.borderWidth = 1.5
        button.titleLabel?.font = UIFont.fontWithName(type: .bold, size: 16)
    }
    
    func resetSingleChoice(button : UIButton)
    {
        button.titleLabel?.font = UIFont.fontWithName(type: .regular, size: 16)
        button.setTitleColor(.OffBlack, for: .normal)
        button.layer.borderColor = UIColor.Foggy.cgColor
        button.layer.borderWidth = 1
    }
    
    func enableNext()
    {
        btnNext.setTitleColor(.white, for: .normal)
        btnNext.setBackgroundColor(.Purple, for: .normal)
        btnNext.isEnabled = true
    }
    
    
    @IBAction func ibNext(_ sender: Any) {
        
        let vc = R.storyboard.study.diagnosisNextMainViewController()!
        vc.delegate = self.delegate
        vc.diagnosisSeq = self.diagnosisSeq
        vc.userSeq = self.userSeq
        vc.fromSeven = self.fromSeven
        vc.fromStudy = self.fromStudy
        
        var nzSchoolInfo = 0
        if selectedTabIndex == 0
        {
            if selectedChoiceIndex == 0  {
                vc.schoolGrade = "m1"
                nzSchoolInfo = 1
            } else if selectedChoiceIndex == 1 {
                vc.schoolGrade = "m2"
                nzSchoolInfo = 2
            } else if selectedChoiceIndex == 2 {
                vc.schoolGrade = "m3"
                nzSchoolInfo = 3
            }
        }
        else if selectedTabIndex == 1
        {
            if selectedChoiceIndex == 0  {
                vc.schoolGrade = "h1"
                nzSchoolInfo = 4
            } else if selectedChoiceIndex == 1 {
                vc.schoolGrade = "h2"
                nzSchoolInfo = 5
            } else if selectedChoiceIndex == 2 {
                vc.schoolGrade = "h3"
                nzSchoolInfo = 6
            }
            else if selectedChoiceIndex == 3 {
                vc.schoolGrade = "hn"
                nzSchoolInfo = 7
            }
            else if selectedChoiceIndex == 4 {
                vc.schoolGrade = "nb"
                nzSchoolInfo = 8
            }
        }
        
        let key = "\(self.userSeq ?? 0)"
        CommonUtil.shared.setSchoolInfo(info: [key : nzSchoolInfo])
        self.navigationController?.pushViewController(vc, animated: true)
        
//        nextButton.backgroundColor = UIColor.submitButtonPressed
//        let vc = R.storyboard.study.diagnosisNextMainViewController()!
//        vc.delegate = self.delegate
//        let temp : [Bool]?
//        if let gradeTemp = self.grade {
//            switch gradeTemp {
//            case "middle":
//                guard let temp = self.status["middle"] else { return }
//                if temp[0] == true  {
//                    vc.schoolGrade = "m1"
//                } else if temp[1] == true {
//                    vc.schoolGrade = "m2"
//                } else if temp[2] == true {
//                    vc.schoolGrade = "m3"
//                }
//            default :
//                guard let temp = self.status["high"] else { return }
//                if temp[0] == true {
//                    vc.schoolGrade = "h1"
//                } else if temp[1] == true {
//                    vc.schoolGrade = "h2"
//                } else if temp[2] == true {
//                    vc.schoolGrade = "h3"
//                } else if temp[3] == true {
//                    vc.schoolGrade = "hn"
//                } else if temp[4] == true {
//                    vc.schoolGrade = "nb"
//                }
//            }
//        } else {
//            vc.schoolGrade = "정보없음"
//        }
//        vc.diagnosisSeq = self.diagnosisSeq
//        vc.userSeq = self.userSeq
//
//        UIView.animate(withDuration: 0.2, animations: {
//
//        }, completion:
//                        { finished in
//            self.buttonView.backgroundColor = UIColor.white
//            self.navigationController?.pushViewController(vc, animated: true)})

    }
     
}
