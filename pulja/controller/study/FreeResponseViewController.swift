//
//  FreeResponseViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/01/28.
//

import UIKit
import MaterialComponents

class FreeResponseViewController: PuljaBaseViewController,  UITextFieldDelegate, MDCBottomSheetControllerDelegate {

    
    
    @IBOutlet var ivProblem: UIImageView!
    var problemUrl : URL?
    
   
    @IBOutlet var problemIVHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var tfAnswer: UITextField!
        
    
    @IBOutlet var answerView: AnswerSwipeView!
    
    var button = UIButton()
    override func viewDidLoad() {
        super.viewDidLoad()
            
        
        //문제 load
        self.ivProblem.kf.indicatorType = .activity
        self.ivProblem.kf.setImage(with: self.problemUrl,  options: [.transition(.fade(0.2))])
        let height = self.ivProblem.contentClippingRect.height
        
        if self.problemIVHeight.constant < height {
            self.problemIVHeight.constant = height
        }
        
        
        // Do any additional setup after loading the view.
        tfAnswer.addTarget(self, action: #selector(FreeResponseViewController.textFieldDidChange(_:)), for: .editingChanged)
        
        NotificationCenter.default.addObserver(self, selector: #selector(FreeResponseViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
       
        self.tfAnswer.delegate = self
        
        //답안 제출하기 버튼
        self.view.addSubview(button)
        button.backgroundColor = UIColor.Purple
        button.cornerRadius = 10
        button.borderWidth = 0.2
        button.setTitleColor(.white, for: .highlighted)
        button.setTitle("답안 제출하기", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        button.topAnchor.constraint(equalTo: tfAnswer.bottomAnchor, constant: 16).isActive = true
        button.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        button.widthAnchor.constraint(equalTo: tfAnswer.widthAnchor).isActive = true
        
        button.isHidden = true
        
    }
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if tfAnswer.text == "" {
            button.isHidden = true
            answerView.isHidden = false
        } else {
            answerView.isHidden = true
            button.isHidden = false
            self.view.addSubview(button)
            
            
        }

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    //     MARK: - Input
    @objc func keyboardWillShow(notification: NSNotification) {

        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {

            // if keyboard size is not available for some reason, dont do anything
           return
        }

        self.bottomView.bottomAnchor.constraint(equalTo: view.superview!.bottomAnchor, constant: -keyboardSize.height).isActive = true
    }
        
    @IBAction func lbquit(_ sender: Any) {
        self.alert(title: "어디가세요 😭", message: "지금 그만두면 이전 문제들의 답안이 제출되어 다시 풀 수 없어요",
                   button: "제출하고 나가기", subButton: "이어서 풀기")
    }
    
    
    @IBAction func lbBack(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
}
