//
//  AnswerSwipeView.swift
//  pulja
//
//  Created by 고권일 on 2022/01/26.
//

import UIKit

@IBDesignable class AnswerSwipeView: UIView {
    
    var trackStatus: Int?
    var userResponseList: [StudyCommons.userResponse]?
    var uR: StudyCommons.userResponse?
    
    var videoCnt: Int?
    var problemCnt : Int?
    var userSeq: Int?
    var currSeq: Int?
    var cunitSeq: Int?
    var questionId: Int?
    var answer: String?
    var delegateViewController : MultipleChoiceViewController?
    var ansToServer: String?
    
    //시간 측정
    var startTime : DispatchTime?
    var endTime : DispatchTime?
    
    
    /// Radius of background view
    @IBInspectable var radius: CGFloat = 10.0
    
    /// Background hint view
    @IBInspectable var hint: String = "잘 모르겠어요"
    
    /// drag image
    @IBInspectable var image: UIImage?
    
    /// state
    
    private var xcoord : CGFloat = 0 {
        didSet {
            if isInit {
                xcoord = swipeImageView.frame.origin.x
                alphaValue = 0.25 + xcoord / frame.width
            }
        }
    }
    
    private var alphaValue : CGFloat = 0 {
        didSet {
            if isInit {
                if alphaValue == 0.25 {
                    self.backgroundColor = UIColor.Foggy
                } else {
                    self.backgroundColor = addColor(multiplyColor(color1, by: alphaValue), with: multiplyColor(color2, by: 1-alphaValue))
                }
            }
        }
    }
    private var isSuccess: Bool = false
    
    public var isInit: Bool = false
    
    private var width: CGFloat = 1
    private var colorIdx: Int = 0

    
    private let color1 = UIColor(red: 85.0/255.0, green: 62.0/255, blue: 255.0/255.0, alpha: 1)
    private let color2 = UIColor(red: 197.0/255.0, green: 197.0/255, blue: 216.0/255.0, alpha: 1)
    
    //color function
    func addColor(_ color1: UIColor, with color2: UIColor) -> UIColor {
        var (r1, g1, b1, a1) = (CGFloat(0), CGFloat(0), CGFloat(0), CGFloat(0))
        var (r2, g2, b2, a2) = (CGFloat(0), CGFloat(0), CGFloat(0), CGFloat(0))

        color1.getRed(&r1, green: &g1, blue: &b1, alpha: &a1)
        color2.getRed(&r2, green: &g2, blue: &b2, alpha: &a2)

        // add the components, but don't let them go above 1.0
        return UIColor(red: min(r1 + r2, 1), green: min(g1 + g2, 1), blue: min(b1 + b2, 1), alpha: (a1 + a2) / 2)
    }
    
    func multiplyColor(_ color: UIColor, by multiplier: CGFloat) -> UIColor {
        var (r, g, b, a) = (CGFloat(0), CGFloat(0), CGFloat(0), CGFloat(0))
        color.getRed(&r, green: &g, blue: &b, alpha: &a)
        return UIColor(red: r * multiplier, green: g * multiplier, blue: b * multiplier, alpha: a)
    }
    
    
    /// Completion handler
    private var completionHandler: ((_ isSuccess: Bool) -> ())?
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
        let a = swipeImageView.frame.origin.x / frame.width
        self.backgroundColor = addColor(multiplyColor(color1, by: a), with: multiplyColor(color2, by: 1-a))
        
        if !isInit {
            isInit = true
            setUp()
        }
    }
    
    public var hintLabel: UILabel = UILabel()
    
    public var swipeImageView: UIImageView = UIImageView() {

        didSet {
            let a = swipeImageView.frame.origin.x / frame.width
            self.backgroundColor = addColor(multiplyColor(color1, by: a), with: multiplyColor(color2, by: 1-a))
        }
        
    }
    
    
    var initPos : CGPoint?

    
    public func setUp() {
        print("스와이프 뷰 셋업")
        
        //잘 모르겠어요 텍스트 처리
//        hintLabel.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
//        hintLabel.frame = CGRect(x: swipeImageView.frame.width, y: 0, width: frame.width - swipeImageView.frame.width, height: frame.height)
        hintLabel.frame = CGRect(x: swipeImageView.frame.width / 2, y: 0, width: frame.width - swipeImageView.frame.width, height: frame.height)
//        hintLabel.frame = CGRect(x: swipeImageView.frame.width, y: 0, width: frame.width, height: frame.height)
        self.addSubview(hintLabel)
        hintLabel.font = UIFont(name:"AppleSDGothicNeo-Bold", size:16)
        hintLabel.textColor = UIColor.puljaBlack
        hintLabel.textAlignment = .center
        hintLabel.text = hint

        
        // 사각형 뷰 안에 화살표 추가
        swipeImageView.frame = CGRect(x: 4, y: 4, width: 1.7 * (frame.height - 8), height: frame.height - 8)
        width = 1.7 * (frame.height - 8)
        swipeImageView.image = UIImage(named: "rightarrow")
        swipeImageView.contentMode = .center
        swipeImageView.backgroundColor = UIColor.white
        swipeImageView.cornerRadius = 10
        swipeImageView.borderWidth = 0.2
        
        self.addSubview(swipeImageView)
        swipeImageView.isUserInteractionEnabled = true
        initPos = swipeImageView.frame.origin
        

        
    }
    
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        
        if let touch = touches.first, swipeImageView == touch.view {
            hintLabel.isHidden = true
            let touchedPosition = touch.location(in: self)
            xcoord = touchedPosition.x
            
            if touchedPosition.x > (frame.height/2)  {
                swipeImageView.center = CGPoint(x: touchedPosition.x, y: swipeImageView.frame.midY)
                colorIdx = Int(touchedPosition.x) / Int(width)
//                changeColor()
            }
            
            
        }
    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        if let touch = touches.first, swipeImageView == touch.view {
            let touchedPosition = touch.location(in: self)
            xcoord = touchedPosition.x
//            xcoord = swipeImageView.frame.origin.x
//            alphaValue = 0.25 + xcoord / frame.width
            setToDefault()
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        if let touch = touches.first, swipeImageView == touch.view {
            let touchedPosition = touch.location(in: self)
            xcoord = touchedPosition.x
//            xcoord = swipeImageView.frame.origin.x
//            alphaValue = 0.25 + xcoord / frame.width
            setToDefault()
        }
    }
//    private func fadeColor() {
//        let xpos =
//    }
    
    /// Set to default
    private func setToDefault() {

        if (self.frame.midX) * 1.5 > swipeImageView.center.x { // Move to initial
            hintLabel.isHidden = false
            UIView.animate(withDuration: 0.25, animations: {
                self.swipeImageView.frame.origin = CGPoint(x: 5, y: 5)
                self.alphaValue = 0.25
                self.hintLabel.text = "잘 모르겠어요"
                self.hintLabel.isHidden = false
                self.swipeImageView.alpha = 1.0
            }) { (isFinished) in
                if isFinished {
                    self.isSuccess = false
                    
                    if let complete = self.completionHandler {
                        complete(self.isSuccess)
                    }
                }
            }
        }
        else if (self.frame.midX) * 1.5 < swipeImageView.center.x { // Move to final (답안 제출하기
            let xPosi = self.frame.width - (self.swipeImageView.frame.width)
//            let xPosi = self.frame.width - (self.swipeImageView.frame.width + 5)
            
            // 문제 푼 시간 측정
            self.endTime = DispatchTime.now()
            let nanoDuration = self.endTime!.uptimeNanoseconds - self.startTime!.uptimeNanoseconds
            let temp = Double(nanoDuration) / 1_000_000_000
            print("객관식 문제 푸는데 걸린 시간은 \(temp) 초 입니다")
            let duration = Double(round(10 * Double(nanoDuration) / 1_000_000_000)/10)
            
            UIView.animate(withDuration: 0.25, animations: {
                self.swipeImageView.frame.origin = CGPoint(x: xPosi, y: 5)
                self.xcoord = self.swipeImageView.frame.origin.x
                self.hintLabel.centerYAnchor.constraint(equalTo: (self.delegateViewController?.answerView.centerYAnchor)!).isActive = true
//                self.hintLabel.centerXAnchor.constraint(equalTo: (self.delegateViewController?.answerView.centerXAnchor)!).isActive = true //추가
                self.hintLabel.text = "답안 제출하기"
                self.hintLabel.textAlignment = .center
                self.hintLabel.textColor = UIColor.white
                self.hintLabel.isHidden = false
                self.swipeImageView.alpha = 0
            }) { (isFinished) in
                if isFinished {
                    
                    
                    self.isSuccess = true
                    print("애니메이션 끝나고 api호출 전")
                    self.answer = "잘 모르겠음."
                    self.ansToServer = (self.delegateViewController?.isMultipleChoice == true) ? "⑥" : "잘 모르겠음."
                    print("########## 모르겠어요 선택 후 서버에 보낸 정답정보: \(self.ansToServer) ##################")
                    
//                    self.answer = (self.delegateViewController?.isMultipleChoice == true) ? "⑥" : "잘 모르겠음."
                    
                    
                    self.delegateViewController?.setupAnimationCustomView(constant: 1)
                    self.delegateViewController?.ivProblem.isHidden = true
                    self.delegateViewController?.headerView.isHidden = true
                    self.delegateViewController?.lineView.isHidden = true
                    LoadingView.showSkeleton()
                    
                    
                    //무료기능 추가
                    //무료기능 인 경우
                    if let isAI = self.delegateViewController?.isAIRecommend, isAI == true {
                        
                        self.delegateViewController?.tfAnswer.resignFirstResponder()
                        //중단원 진단 인 경우
                        if let isAIDiag = self.delegateViewController?.isAIDiagnosis, isAIDiag == true {
                            guard let problemSeq = self.delegateViewController?.problemSeq else { return }
                            guard let questionId = self.delegateViewController?.questionId else { return }
                            guard let number = self.delegateViewController?.number else { return }
                            
                            PreparationAPI.shared.diagnosisStudyAnswer(problemSeq: problemSeq, duration: duration, questionId: questionId,
                                                                       userAnswer: self.ansToServer!, userSeq: self.userSeq!).done
                            {
                                res in
                                if let suc = res.success, suc == true {
                                    self.delegateViewController?.uR!.answer = "잘 모르겠음."
                                    self.delegateViewController?.userResponseList![number-1] = (self.delegateViewController?.uR!)!
                                    
                                    guard let data = res.data else { return }
                                    guard let last = data.last else { return }
                                    
                                    
                                    if self.delegateViewController?.last! == true { //마지막 문제인 경우
                                        self.delegateViewController?.showAIDiagResult()
                                    } else {
                                        
                                        self.delegateViewController?.last = last
                                        self.delegateViewController?.uR = StudyCommons.userResponse()
                                        self.delegateViewController?.number! += 1
                                        self.delegateViewController?.lbProbNum.text = "\(data.totalCnt!)문제 중 \(number+1)번 문제"
                                        self.delegateViewController?.isMultipleChoice = data.problemType == "cho" ? true : false
                                        self.uR?.isMultipleChoice = self.delegateViewController?.isMultipleChoice
                                        self.delegateViewController?.problemUrl = URL(string: data.problemUrl!)
                                        self.delegateViewController?.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                                        
                                        //questionId찾기
                                        self.delegateViewController?.questionId = data.questionId
                                        self.delegateViewController?.answerView.questionId = self.questionId
                                        
                                        //userResponse 업데이트
                                        self.delegateViewController?.uR?.qId = self.questionId!
                                        self.delegateViewController?.uR?.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                                        self.delegateViewController?.answerView.uR = self.delegateViewController?.uR
                                        self.delegateViewController?.viewprobImg()
                                        
                                        if (self.delegateViewController?.number!)! > (self.delegateViewController?.userResponseList!.count)! {    //새로운 문제 푼 경우
                                            self.delegateViewController?.userResponseList! += [(self.delegateViewController?.uR!)!]
                                        }
                                        self.delegateViewController?.initialize()
                                    }
                                    
                                }
                            }.catch {
                                err in
                                LoadingView.hide()
                                
                                let vc = R.storyboard.preparation.preparationErrorViewController()!
                                vc.cameFromOnboard = ((self.delegateViewController?.cameFromOnboard!) != nil)
                                self.delegateViewController?.navigationController?.pushViewController(vc, animated: true)
                                
                            }.finally {
                                self.delegateViewController?.ivProblem.isHidden = false
                                self.delegateViewController?.headerView.isHidden = false
                                self.delegateViewController?.lineView.isHidden = false
                                LoadingView.hide()
                            }
                            
                        } else {    //유형 학습 인 경우
                            guard let problemSeq = self.delegateViewController?.problemSeq else { return }
                            guard let questionId = self.delegateViewController?.questionId else { return }
                            guard let number = self.delegateViewController?.number else { return }
                            
                            PreparationAPI.shared.weakCategoryStudyAnswer(problemSeq: problemSeq, duration: duration, questionId: questionId, userAnswer: self.ansToServer!).done{ res in
                                if let suc = res.success, suc == true {
                                    //정답 정보 저장
                                    self.delegateViewController?.uR!.answer = "잘 모르겠음."
                                    guard let num = self.delegateViewController?.number else { return }
                                    self.delegateViewController?.userResponseList![num - 1] = (self.delegateViewController?.uR!)!
                                    
                                    guard let data = res.data else { return }
//                                    guard let last = data.last else { return }
                                    
                                    if self.delegateViewController?.last! == true { //중단원 진단 마지막 문제 품
                                        LoadingView.hide()
                                        print("취약 유형 학습 마지막 문제 품")
                                        self.delegateViewController?.showAIWeakResult()
                                        
                                    } else {
                                        //취약 유형 추가 부분
                                        self.delegateViewController?.totalQ = data.totalCnt!
                                        self.delegateViewController?.trackT = data.title!
                                        self.delegateViewController?.number = data.number
                                        
                                        self.delegateViewController?.last = data.last
                                        self.delegateViewController?.uR = StudyCommons.userResponse()
                                        self.delegateViewController?.lbProbNum.text = "\(data.totalCnt!)문제 중 \(data.number!)번 문제"
                                        self.delegateViewController?.isMultipleChoice = data.problemType == "cho" ? true : false
                                        self.delegateViewController?.problemUrl = URL(string: data.problemUrl!)
                                        self.delegateViewController?.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                                        
                                        //questionId찾기
                                        self.delegateViewController?.questionId = data.questionId
                                        self.delegateViewController?.answerView.questionId = self.delegateViewController?.questionId
                                        
                                        //userResponse 업데이트
                                        self.delegateViewController?.uR?.isMultipleChoice = self.delegateViewController?.isMultipleChoice
                                        self.delegateViewController?.uR?.number = data.number
                                        self.delegateViewController?.uR?.qId = self.delegateViewController?.questionId!
                                        self.delegateViewController?.uR?.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                                        self.delegateViewController?.answerView.uR = self.delegateViewController?.uR
                                        self.delegateViewController?.viewprobImg()
                                        
                                        if (self.delegateViewController?.number!)! > (self.delegateViewController?.userResponseList!.count)! {    //새로운 문제 푼 경우
                                            self.delegateViewController?.userResponseList! += [(self.delegateViewController?.uR!)!]
                                        }
                                        
                                        self.delegateViewController?.initialize()
                                        
                                    }
                                }
                            }.catch {
                                err in
                                LoadingView.hide()
                                
                                let vc = R.storyboard.preparation.preparationErrorViewController()!
                                vc.cameFromOnboard = ((self.delegateViewController?.cameFromOnboard!) != nil)
                                self.delegateViewController?.navigationController?.pushViewController(vc, animated: true)
                                
                            }.finally {
                                LoadingView.hide()
                                if (self.delegateViewController?.number ?? 0) - 1 <= (self.delegateViewController?.problemCnt ?? 0 ) - 1 {
                                    self.delegateViewController?.ivProblem.isHidden = false
                                    self.delegateViewController?.headerView.isHidden = false
                                    self.delegateViewController?.lineView.isHidden = false
                                }
                            }
                        }
                        
                    } else {
                    
                    
                        if let isDiag = self.delegateViewController?.isDiagnosis, isDiag == true {
                            guard let diagnosisInput = self.delegateViewController?.diagnosisInput else { return }
                            guard let diagnosisSeq = self.delegateViewController?.diagnosisSeq! else { return }
                            guard let nodeSeq = diagnosisInput.nodeSeq else { return }
                            guard let number = diagnosisInput.number else { return }
                            guard let stepCount = diagnosisInput.stepCount else { return }
                            guard let testSeq = diagnosisInput.testSeq else { return }
                            guard let questionId = self.delegateViewController?.questionId else { return }
                            
                            
                            self.delegateViewController?.tfAnswer.resignFirstResponder()
                            
                            //진단평가 그 다음 문제
                            StudyAPI.shared.diagnosisProgress(diagnosisSeq: diagnosisSeq, nodeSeq: nodeSeq, number: number,
                                                              stepCount: stepCount, testSeq: testSeq,
                                                              answer: self.ansToServer!, duration: duration, questionId: questionId).done
                            { res in
                                if let suc = res.success, suc == true {
                                    
    //                                if  number == 1 {
    //                                    if let userSeq = self.userSeq, let userId = self.delegateViewController?.userId {
    //                                        CommonUtil.shared.ABEvent(category: "diagnosis_problem_try", customs: ["userseq": userSeq, "user_id": userId])
    //                                    }
    //                                }
                                    
    //                                self.uR?.answer = self.answer! //지움 05.25.22
                                    self.delegateViewController?.uR!.answer = "잘 모르겠음." //새로 추가 05.25.22
    //                                self.delegateViewController?.userResponseList! += [self.uR!] //지움 05.25.22
                                    self.delegateViewController?.userResponseList![number - 1] = (self.delegateViewController?.uR!)! //새로 추가 05.25.22
                                    
                                    guard let data = res.data else { return }
                                    guard let isEnd = data.isEnd else { return }
                                    
                                    self.delegateViewController?.last = (isEnd == "N") ? false : true
                                    print("여기 last: \(self.delegateViewController?.last!)")
                                    if self.delegateViewController?.last! == true {
                                        print("마지막 문제 response 성공")
                                        self.delegateViewController?.showDiagnosisResult()
                                    } else {
                                        guard let diagnosisinput = data.diagnosisInput else { return }
                                        guard let problem = data.problem else { return }
                                        self.delegateViewController?.diagnosisInput = diagnosisinput
                                        //userResponse 생성
                                        self.uR = StudyCommons.userResponse()
                                        
                                        //객관식, 주관식 여부
                                        self.delegateViewController?.number! += 1
                                        self.delegateViewController?.isMultipleChoice = problem.problemType == "cho" ? true : false
                                        self.delegateViewController?.uR?.isMultipleChoice = self.delegateViewController?.isMultipleChoice
                                        self.delegateViewController?.problemUrl = URL(string: problem.problemUrl!)
                                        self.delegateViewController?.isMultipleAnswers = (problem.answerCnt! > 1) ? true : false
                                        
                                        //questionId찾기
                                        self.delegateViewController?.questionId = problem.questionId
                                        self.delegateViewController?.answerView.questionId = self.delegateViewController?.questionId
                                        
                                        //userResponse 업데이트
                                        self.delegateViewController?.uR?.qId = self.delegateViewController?.questionId!
                                        self.delegateViewController?.uR?.isMultipleAnswers = (problem.answerCnt! > 1) ? true : false
                                        self.delegateViewController?.answerView.uR = self.delegateViewController?.uR
                                        self.delegateViewController?.viewprobImg()
                                        
                                        if (self.delegateViewController?.number!)! > (self.delegateViewController?.userResponseList!.count)! {    //새로운 문제 푼 경우
                                            self.delegateViewController?.userResponseList! += [(self.delegateViewController?.uR!)!]
                                        }
                                        
                                        self.delegateViewController?.initialize()
                                    }
                                    
                                }
                            }.catch { error in
                                
                            }.finally {
                                self.delegateViewController?.ivProblem.isHidden = false
                                self.delegateViewController?.headerView.isHidden = false
                                self.delegateViewController?.lineView.isHidden = false
                                LoadingView.hide()
                                
                            }
                        } else {
                            
                            self.delegateViewController?.tfAnswer.resignFirstResponder()
                            
                            //코칭 테스트 인 경우, 그 다음 문제 받아오기
                            if let iscoaching = self.delegateViewController?.isCoachingTest, iscoaching == true {
                                let num = self.delegateViewController?.number
                                let addseq = self.delegateViewController?.userResponseList![num! - 1].addSeq ?? 0
                                
                                
                                StudyAPI.shared.coachingTestAnswer(addSeq: addseq, duration: duration, number: (self.delegateViewController?.number!)!, questionId: (self.delegateViewController?.questionId!)!, studyDay: (self.delegateViewController?.studyDate!)!, userAnswer: self.ansToServer!, userSeq: (self.delegateViewController?.userSeq!)!).done
                                { res in
                                    if let success = res.success, success == true {
                                        print("코칭테스트 결과 반환 성공")
                                        print("\(self.delegateViewController?.uR!)")
                                        self.delegateViewController?.uR!.answer = "잘 모르겠음."
                                        guard let num = self.delegateViewController?.number else { return }
                                        
                                        
    //                                    //기존 답 바꾼 경우
    //                                    if num - 1 < self.userResponseList!.count {
    //                                        self.delegateViewController?.userResponseList![num - 1] = self.uR!
    //                                    } else { //새로운 문제 답 쓴 경우
    //                                        self.delegateViewController?.userResponseList! += [self.uR!]
    //                                    }
                                        self.delegateViewController?.userResponseList![num - 1] = (self.delegateViewController?.uR!)!
                                        print("코칭테스트잘모르겠어요선택후userResponselist: \(self.delegateViewController?.userResponseList!)")

                                        
                                        guard let data = res.data else { return }
                                        
                                        
                                        if self.delegateViewController?.last! == true {
    //                                        self.delegateViewController?.navigationController?.popViewController(animated: false)
                                            self.delegateViewController?.showCoachSolveResult()
                                        } else {
                                        // 수정 본
    //                                        self.delegateViewController?.trackStatus! += 1
                                            //                self.delegateViewController?.problemCnt! -= 1
    //                                        self.delegateViewController?.trackQ! += 1
                                            self.delegateViewController?.trackT! = data.title!
                                            self.delegateViewController?.totalQ! = data.totalCnt!
                                            self.delegateViewController?.last = data.last
                                            self.delegateViewController?.addSeq = data.addSeq
                                            self.delegateViewController?.number = data.number
                                            self.delegateViewController?.lbProbNum.text = "\(data.totalCnt!)문제 중 \(data.number!)번 문제"
                                            self.delegateViewController?.lbCurr.text = "코칭 테스트 | \(data.title!)"
                                            //userResponse 생성
                                            self.delegateViewController?.uR = StudyCommons.userResponse()
                                            //객관식, 주관식 여부
                                            self.delegateViewController?.isMultipleChoice = data.problemType == "cho" ? true : false
                                            self.delegateViewController?.uR?.isMultipleChoice = self.delegateViewController?.isMultipleChoice
                                            self.delegateViewController?.problemUrl = URL(string: data.problemUrl!)
                                            //questionId찾기
                                            self.delegateViewController?.questionId = data.questionId
                                            self.delegateViewController?.answerView.questionId = self.delegateViewController?.questionId
                                            //userResponse 업데이트
                                            self.delegateViewController?.uR?.qId = self.delegateViewController?.questionId!
                                            self.delegateViewController?.uR?.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                                            self.delegateViewController?.answerView.uR = self.delegateViewController?.uR
                                            self.delegateViewController?.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                                            self.delegateViewController?.uR?.addSeq = data.addSeq
                                            self.delegateViewController?.uR?.number = data.number
                                            self.delegateViewController?.viewprobImg()
                                            
                                            
                                            if (self.delegateViewController?.number!)! > (self.delegateViewController?.userResponseList!.count)! {    //새로운 문제 푼 경우
                                                self.delegateViewController?.userResponseList! += [(self.delegateViewController?.uR!)!]
                                            }
                                            
                                            //초기화
                                            self.delegateViewController?.initialize()
                                        }
                                        
                                    }
                                }.catch { error in
                                    
                                }.finally {
                                    LoadingView.hide()
    //                                if self.delegateViewController?.last! == false {
                                    if self.delegateViewController?.number ?? 0 <= (self.delegateViewController?.totalQ ?? 0 )  {
                                        self.delegateViewController?.ivProblem.isHidden = false
                                        self.delegateViewController?.headerView.isHidden = false
                                        self.delegateViewController?.lineView.isHidden = false
                                    }
                                }
                                
                            } else {
                            
                            
                                //커리큘럼 학습하는 경우, 그 다음 문제 받아오기
                                StudyAPI.shared.curriculumStudyAnswer(cunitSeq: self.cunitSeq!, currSeq: self.currSeq!, duration: duration,
                                                                      questionId: self.uR!.qId!, userAnswer: self.ansToServer!, userSeq: self.userSeq!).done
                                { res in
                                    if let success = res.success, success == true {
                                        print("잘 모르겠음.선택 후 api 성공함")
                                        self.delegateViewController?.uR!.answer = "잘 모르겠음."
                                        guard let num = self.delegateViewController?.number else { return }
                                       
                                        
    //                                    //기존 답 바꾼 경우
    //                                    if num < self.userResponseList!.count {
    //                                        self.delegateViewController?.userResponseList![num - 1] = self.uR!
    //                                    } else { //새로운 문제 답 쓴 경우
    //                                        self.delegateViewController?.userResponseList! += [self.uR!]
    //                                    }
                                        self.delegateViewController?.userResponseList![num - 1] = (self.delegateViewController?.uR!)!
                                        
                                        guard let data = res.data else { return }
                                        
                                        
                                        if self.delegateViewController?.last! == true {
                                            self.delegateViewController?.showSolveResult(answer: self.ansToServer!, duration: duration)
                                        } else {
                                            // 수정 본
    //                                        self.delegateViewController?.trackStatus! += 1
    //                                        self.delegateViewController?.trackQ! += 1
                                            
                                            self.delegateViewController?.trackT! = data.title!
                                            self.delegateViewController?.totalQ! = data.totalCnt!
                                            self.delegateViewController?.last = data.last
                    
                                            self.delegateViewController?.lbProbNum.text = "\(data.totalCnt!)문제 중 \(data.number!)번 문제"
                                            self.delegateViewController?.lbCurr.text = "내 커리큘럼 | \(data.title!)"
                                            
                                            //userResponse 생성
                                            self.delegateViewController?.uR = StudyCommons.userResponse()
                                            
                                            //객관식, 주관식 여부
                                            self.delegateViewController?.isMultipleChoice = data.problemType == "cho" ? true : false
                                            self.delegateViewController?.uR?.isMultipleChoice = self.delegateViewController?.isMultipleChoice
                                            self.delegateViewController?.problemUrl = URL(string: data.problemUrl!)
                                            
                                            //questionId찾기
                                            self.delegateViewController?.questionId = data.questionId
                                            self.delegateViewController?.answerView.questionId = self.delegateViewController?.questionId
                                            
                                            //userResponse 업데이트
                                            self.delegateViewController?.uR?.qId = self.delegateViewController?.questionId!
                                            self.delegateViewController?.uR?.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                                            self.delegateViewController?.answerView.uR = self.delegateViewController?.uR
                                            self.delegateViewController?.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                                            self.delegateViewController?.number = data.number
                                            self.delegateViewController?.viewprobImg()
                                            
                                            if (self.delegateViewController?.number!)! > (self.delegateViewController?.userResponseList!.count)! {    //새로운 문제 푼 경우
                                                self.delegateViewController?.userResponseList! += [(self.delegateViewController?.uR!)!]
                                            }
                                            
                                            //초기화
                                            self.delegateViewController?.initialize()
                                        }
                                    
                                    }
                                }.catch { error in
                                    
                                }.finally {
                                    LoadingView.hide()
                                    if (self.delegateViewController?.number ?? 0) - 1 <= (self.delegateViewController?.problemCnt ?? 0 ) - 1 {
                                        self.delegateViewController?.ivProblem.isHidden = false
                                        self.delegateViewController?.headerView.isHidden = false
                                        self.delegateViewController?.lineView.isHidden = false
                                    }
                                    
    //                                if self.delegateViewController?.last! == false {
    //                                    self.delegateViewController?.ivProblem.isHidden = false
    //                                    self.delegateViewController?.headerView.isHidden = false
    //                                    self.delegateViewController?.lineView.isHidden = false
    //                                }
                                }
                            }
                        }
                    
                        
                        
                        if let complete = self.completionHandler {
    //                        self.delegateViewController?.answerView.setUp()
                            complete(self.isSuccess)
                            
                        }
                    }
                }
            }
        }
    }
    
    /// Handle action
    ///
    /// - Parameter completed: with completion
    public func handleAction(_ completed: @escaping((_ isSuccess: Bool) -> ()) ) {
        completionHandler = completed
    }
    
    /// Update hint value
    ///
    /// - Parameter text: by string
    public func updateHint(text: String) {
        hintLabel.text = text
    }

    

}
