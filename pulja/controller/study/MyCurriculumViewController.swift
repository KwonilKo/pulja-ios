//
//  MyCurriculumViewController.swift
//  pulja
//
//  Created by jeonghoonlee on 2021/12/30.
//

import UIKit
import PromiseKit
import Pageboy
import Collections


class MyCurriculumViewController: PuljaBaseViewController, UITableViewDataSource, UITableViewDelegate, CustomFooterViewDelegate, CustomTableViewDelegate {
    
    
    @IBOutlet weak var delayedButton: UIButton!
    @IBOutlet weak var lbCurriculum: UILabel!
    @IBOutlet weak var lbState: UILabel!
    @IBOutlet weak var lbCurriculumDetail: UILabel!
    
    @IBOutlet weak var isChatNew: UIImageView!
    
    
    let headerMaxHeight:CGFloat = 0.0
    var userSeq : Int = 0
    var currSeq : Int = 0
    var connectFromCoach: Bool = false
    
    @IBOutlet weak var CurriculumTableView: UITableView! {
        didSet{
            CurriculumTableView.contentInset = UIEdgeInsets(top: headerMaxHeight, left: 0, bottom: -10.0, right: -20.0)
        }
    }
    
    var isOpen = false
    
    var ListStudyDaysCurr : [String : [String: [CurriculumUnit]]] = [:]
    var OrderedListStudyDaysCurr : OrderedDictionary<String, OrderedDictionary<String, [CurriculumUnit]>> = [:]
    
    var ListStudyDaysExpand : [String : [String : Bool]] = [:]
    var OrderedListStudyDaysExpand : OrderedDictionary<String, OrderedDictionary<String, Bool>> = [:]
    
    var todayCurriculum : [CurriculumUnit]?
    var trackStatus : Int?
    var selectedStatus: String?
    
    var resultStatus: String?   //cell
    var resultCurrIdx : Int?    //cell
    
    var curriculuminfo = ""
    
    @IBOutlet var emptyView: UIView!
    
    @IBOutlet var topView: UIView!
    
    func initEmpty() {
        self.CurriculumTableView.isHidden = true
        self.emptyView.isHidden = false
    }
    @IBOutlet weak var btnChat: UIButton!
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        
        
        
        print("MyCurriculumViewController, userSeq: \(self.userSeq), currSeq: \(self.currSeq)")
        self.emptyView.isHidden = true
        
        //밀린학습보기 버튼 이미지를 텍스트 오른쪽에 붙이기
        delayedButton.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        delayedButton.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        delayedButton.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)

        CurriculumTableView.isScrollEnabled = true
        CurriculumTableView.separatorStyle = .none
        // Do any additional setup after loading the view.
        
        
        //셀 등록
        self.CurriculumTableView.register(StudyCurriculumTableViewCell.nib(), forCellReuseIdentifier: "StudyCurriculumTableViewCell")
        self.CurriculumTableView.register(StudyCurriculumTableViewCell.nib(), forCellReuseIdentifier: "StudyCurriculumTableViewCell")
        self.CurriculumTableView.register(StudyCurriculumMultipleTableViewCell.nib(), forCellReuseIdentifier: "StudyCurriculumMultipleTableViewCell")
        
        //헤더
        let headerNib = UINib(nibName: "StudyCurriculumHeaderView", bundle: nil)
        CurriculumTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "StudyCurriculumHeaderView")
        
        //푸터
        let footerNib = UINib(nibName: "StudyCurriculumFooterView", bundle: nil)
        CurriculumTableView.register(footerNib, forHeaderFooterViewReuseIdentifier: "StudyCurriculumFooterView")
        
        
        if #available(iOS 15.0, *) {
            CurriculumTableView.sectionHeaderTopPadding = 0
        }
        
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.connectFromCoach {
            
            CommonUtil.coachTabbarController?.tabBar.isHidden = true
            CommonUtil.coachTabbarController?.bottomNavBar.isHidden = true
            
        } else {
            
            CommonUtil.tabbarController?.tabBar.isHidden = true
            CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        }
        
        checkUnreadMessage()
    }
    func checkUnreadMessage()
    {
        let payType = self.myInfo?.payType ?? "f"
        
        if payType == "p"
        {
            CommonUtil.shared.checkNewMessage { hasNew in
                
                if hasNew > 0
                {
                    self.isChatNew.isHidden = false
//                    self.btnChat.setImage(UIImage(resource:R.image.groupChatNew), for: .normal)
                }
                else
                {
                    self.isChatNew.isHidden = true
//                    self.btnChat.setImage(UIImage(resource:R.image.groupChat), for: .normal)
                }
            }
        }
        else
        {
            self.isChatNew.isHidden = true
//            self.btnChat.setImage(UIImage(resource:R.image.groupChat), for: .normal)
        }

    }
    override func viewDidAppear(_ animated: Bool) {
        
        
        self.ListStudyDaysCurr = [:]
        self.ListStudyDaysExpand = [:]
        self.OrderedListStudyDaysCurr = [:]
        self.OrderedListStudyDaysExpand = [:]
        
        
        super.viewDidAppear(animated)
        
        
        
        
        LoadingView.show()
        StudyAPI.shared.myCurriculumDetail(userSeq: self.userSeq, currSeq: self.currSeq).done { res in
            if let bool = res.success, bool == true {
                self.todayCurriculum = res.data?.todayCurriculumUnit ?? []
                
                if self.todayCurriculum!.count == 0 { self.initEmpty() }
                //오늘 학습 커리큘럼 셀 정리
                for tD in self.todayCurriculum! {
                    
                    var curr = self.ListStudyDaysCurr[tD.studyDay!] ?? [String : [CurriculumUnit]]()
                    var open = self.ListStudyDaysExpand[tD.studyDay!] ?? [String : Bool]()
                    
                    switch tD.studyStatusName! {
                    case "학습 완료":
                        if curr.keys.contains("학습 완료") {
                            curr["학습 완료"]! += [tD]
                        } else {
                            curr["학습 완료"] = [tD]
                        }
                        open["학습 완료"] = true
                    case "학습 중":
                        if curr.keys.contains("학습 중") {
                            curr["학습 중"]! += [tD]
                        } else {
                            curr["학습 중"] = [tD]
                        }
                        open["학습 중"] = true
                    default:
                        if curr.keys.contains("학습 전") {
                            curr["학습 전"]! += [tD]
                        } else {
                            curr["학습 전"] = [tD]
                        }
                        open["학습 전"] = true

                    }
                    self.ListStudyDaysExpand[tD.studyDay!] = open
                    self.ListStudyDaysCurr[tD.studyDay!] = curr
                    
                }
                
                //커리큘럼 정렬 하기
                self.sortCurriculum(curriculum : self.ListStudyDaysCurr, expand: self.ListStudyDaysExpand)
                print("커리큘럼정렬끝:\(self.OrderedListStudyDaysCurr)")
                
                //해당 커리큘럼 meta data 정보 뿌려주기
                guard let notDoneStudyAmount = res.data?.notDoneStudyAmount else { return }
                guard let curriculumInfo = res.data?.curriculumInfo else { return }
                
                print("체크포인트2")
                self.curriculuminfo = curriculumInfo.unit1Name!
                self.lbCurriculum.text = "\(curriculumInfo.subjectName!)\n\(self.curriculuminfo)"
                
                self.lbState.text = "\(curriculumInfo.gradeName!.substring(from:0 , to: 0))등급 | Lv\(curriculumInfo.level!) | \(curriculumInfo.currTypeName!)"
                self.lbCurriculumDetail.text = "강의 \(notDoneStudyAmount.videoCnt!)강, 문제 \(notDoneStudyAmount.problemCnt!)개가 밀렸어요."
                self.todayCurriculum = res.data?.todayCurriculumUnit
                let addHeight = self.todayCurriculum?.count ?? 0
            
                print("커리큘럼 정보: \(self.todayCurriculum)")
                if self.ListStudyDaysCurr.count > 0 {
                    self.CurriculumTableView.reloadData()
                }
                
            }
        }.catch { error in
            
        }.finally {
            LoadingView.hide()
        }
        

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.connectFromCoach {
            
            CommonUtil.coachTabbarController?.tabBar.isHidden = false
            CommonUtil.coachTabbarController?.bottomNavBar.isHidden = false
            
        } else {
            CommonUtil.tabbarController?.tabBar.isHidden = false
            CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        }

    }
    func sortHelper (curriculum: [CurriculumUnit]) -> [CurriculumUnit] {
        var sortedCurriculum = curriculum
        let temp = ["학습 중": 0, "학습 전": 1, "학습 완료":2]
        return sortedCurriculum.sorted { (lhs, rhs) in
            if temp[lhs.studyStatusName!] == temp[rhs.studyStatusName!] {
                return lhs.cuOrder! < rhs.cuOrder!
            }
            return temp[lhs.studyStatusName!]! < temp[rhs.studyStatusName!]!
        }
    }
    
    func sortCurriculum (curriculum : [String : [String: [CurriculumUnit]]], expand: [String : [String : Bool]] )
    {
        // sort by dates
        var UnorderedDates = Array(curriculum.keys)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        let orderedDates = UnorderedDates.sorted { dateFormatter.date(from: $0)!
            < dateFormatter.date(from: $1)!
        }
        print("오더 데이트: \(orderedDates)")
        
        for d in orderedDates {
            let eachDayCurriculum = curriculum[d]!
            let eachDayExpand = expand[d]!
            let temp = ["학습 중": 0, "학습 전": 1, "학습 완료" : 2]
            
            // 학습 전/중, 학습 완료 sort하기 (ListStudyDaysCurr)
//            var ordCurriculum = eachDayCurriculum.sorted(by : { $0.0.count > $1.0.count })
            self.OrderedListStudyDaysCurr[d] = [:]
            var ordCurr = eachDayCurriculum.sorted(by : { temp[$0.0]! < temp[$1.0]! })
            
            for ele in ordCurr {
                var ordEachCurriculum = self.sortHelper(curriculum: ele.1)
                self.OrderedListStudyDaysCurr[d]![ele.0] = ordEachCurriculum
            }
            
            // 학습 중, 학습 전, 학습 완료 sort하기 (ListStudyDaysExpand)
            
            var ordExpand = eachDayExpand.sorted(by : { temp[$0.0]! < temp[$1.0]! })
            self.OrderedListStudyDaysExpand[d] = [:]
            for ele in ordExpand {
                self.OrderedListStudyDaysExpand[d]![ele.0] = ele.1
            }
        }
        
    }
    
    
    
    @IBAction func ibBack(_ sender: Any) {
//        self.dismiss(animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let StringDates = Array(self.OrderedListStudyDaysCurr.keys)
        if StringDates.count == 0 {
            return .leastNonzeroMagnitude
        }
        
        
        let date = StringDates[indexPath.section]
        guard let CellCurriculum = self.OrderedListStudyDaysCurr[date]![self.resultStatus!]
        else {
            return 68
        }
        
        if CellCurriculum.count == 1 {
          
            return 68
            
        }
        else
        {
            if let openStatus = self.OrderedListStudyDaysExpand[date]![self.resultStatus!], openStatus == true {
                return 68
                
            } else {
             
                return 76
            }
        }
            
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.OrderedListStudyDaysCurr.count == 0 { return 0 }
        
        let skey = Array(self.OrderedListStudyDaysCurr.keys)[section]
        let ExpandCurr = self.OrderedListStudyDaysExpand[skey]!
        var numRows = 0
        
        for (each, expandCurr) in ExpandCurr {
            if expandCurr {
                let temp = self.OrderedListStudyDaysCurr[skey]![each]
                numRows += temp!.count
            } else {
                numRows += 1
            }
        }
        return numRows
    }
    
    func tableViewHelper(oneDayCurr : OrderedDictionary<String, [CurriculumUnit]>, oneDayExpand: OrderedDictionary<String, Bool>, Date : String, rowIndex : Int) {
        
        let eachCurr = self.OrderedListStudyDaysCurr[Date]!
        let eachExpand = self.OrderedListStudyDaysExpand[Date]!
        
        var temp = 0
        
        for (i, k) in oneDayCurr.keys.enumerated() {
            let temp2 = (oneDayExpand[k] == true ? oneDayCurr[k]!.count : 1)
            if rowIndex >= temp + temp2 {
                temp += temp2
            } else {
                resultStatus = k
                resultCurrIdx = rowIndex - temp
                break
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let StringDates = Array(self.OrderedListStudyDaysCurr.keys)
//        if StringDates.count == 0 { return tableView.dequeueReusableCell(withIdentifier: "StudyCurriculumTableViewCell", for: indexPath)
//            as! StudyCurriculumTableViewCell }
        let Date = StringDates[indexPath.section]
        
        self.tableViewHelper(oneDayCurr: self.OrderedListStudyDaysCurr[Date]!, oneDayExpand: self.OrderedListStudyDaysExpand[Date]!, Date: Date, rowIndex: indexPath.row)
        
        let CellCurriculum = self.OrderedListStudyDaysCurr[Date]![self.resultStatus!]!
        let oneCurr : CurriculumUnit? = self.OrderedListStudyDaysCurr[Date]![self.resultStatus!]![self.resultCurrIdx!]
        
        if CellCurriculum.count == 1 {
            
            let cell:StudyCurriculumTableViewCell = tableView.dequeueReusableCell(withIdentifier: "StudyCurriculumTableViewCell", for: indexPath)
            as! StudyCurriculumTableViewCell
        
            cell.currUnitName = oneCurr!.currUnitName
            cell.studyStatusName = oneCurr!.studyStatusName
            cell.videoCnt = oneCurr!.videoCnt
            cell.problemCnt = oneCurr!.problemCnt
            cell.sectionIdx = indexPath.section
            cell.totalSection = self.OrderedListStudyDaysCurr.keys.count
            cell.cunitSeq = oneCurr!.cunitSeq
            cell.currSeq = oneCurr!.currSeq
            cell.userSeq = self.userSeq
            cell.studyDate = Date
            cell.delegateController = self
            
            if self.connectFromCoach, oneCurr!.studyStatusName != "학습 완료" {
                cell.btArrow.isHidden = true
            } else {
                cell.btArrow.isHidden = false
            }
            
            cell.awakeFromNib()
            cell.connectFromCoach = self.connectFromCoach

            let backgroundview = UIView()
            backgroundview.backgroundColor = UIColor.puljaBlack
            cell.selectedBackgroundView = backgroundview
//            cell.selectedBackgroundView = UIView()
//            cell.selectedBackgroundView?.backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
            
            return cell
            
        } else {
            
            let cell:StudyCurriculumMultipleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "StudyCurriculumMultipleTableViewCell", for: indexPath)
            as! StudyCurriculumMultipleTableViewCell
            
            //펼침 유무에 따른 셀에 들어갈 ListStudyDaysCurr 분류
            let KeyEachCurriculum = Array(self.OrderedListStudyDaysCurr[Date]!.keys)
            let keyStatus: String?
                        
            cell.secDate = Date
            cell.rowStatus = self.resultStatus!
            cell.ListStudyDaysCurr = self.OrderedListStudyDaysCurr
            cell.ListStudyDaysExpand = self.OrderedListStudyDaysExpand
            cell.currUnitName = oneCurr!.currUnitName
            cell.studyStatusName = oneCurr!.studyStatusName
            cell.videoCnt = oneCurr!.videoCnt
            cell.problemCnt = oneCurr!.problemCnt
            cell.sectionIdx = indexPath.section
            cell.totalSection = self.OrderedListStudyDaysCurr.keys.count
            cell.cunitSeq = oneCurr!.cunitSeq
            cell.currSeq = oneCurr!.currSeq
            cell.userSeq = self.userSeq
            cell.studyDate = Date
            cell.delegate = self
            cell.delegateController = self
            cell.connectFromCoach = self.connectFromCoach
            
            if connectFromCoach, cell.isOpened, let status = oneCurr?.studyStatusName, status != "학습 완료" {
                cell.cellButton.isHidden = true
            } else {
                cell.cellButton.isHidden = false
            }
            
            let backgroundview = UIView()
            backgroundview.backgroundColor = UIColor.puljaBlack
            cell.selectedBackgroundView = backgroundview
            
//            cell.selectedBackgroundView = UIView()
//            cell.selectedBackgroundView?.backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
            
            cell.awakeFromNib()
            
            return cell
            
        }
        
    }
    var touchFooter = true 
    func didTouchCell(_ secDate: String, _ rowStatus: String, _ ListStudyDaysExpand: OrderedDictionary<String, OrderedDictionary<String, Bool>>, _ indexPath: IndexPath?) {
        
        UIView.setAnimationsEnabled(false)
        touchFooter = true
        
        
        
        //눌렀기 때문에 isOpened = true로 업데이트 해주기
        self.OrderedListStudyDaysExpand[secDate]![rowStatus] = true
        
        let secIdx = Array(self.OrderedListStudyDaysCurr.keys).firstIndex(where: { $0.hasPrefix(secDate) })!
        let statusidx = Array(self.OrderedListStudyDaysExpand[secDate]!.keys).firstIndex(where: { $0.hasPrefix(rowStatus)})
        selectedStatus = rowStatus
        
    
        self.trackStatus = statusidx
        self.isOpen = false
        CurriculumTableView.reloadData()
        UIView.setAnimationsEnabled(true)
       
    }
    
    
    func didTouchFooter(_ secDate: String, _ ListStudyDaysExpand: OrderedDictionary<String, OrderedDictionary<String, Bool>>) {
        UIView.setAnimationsEnabled(false)
        var isopened : OrderedDictionary<String, Bool> = [:]
        for (eachcurr, isopen) in ListStudyDaysExpand[secDate]! {
            isopened[eachcurr] = false
        }
        let secIdx = Array(self.OrderedListStudyDaysCurr.keys).firstIndex(where: { $0.hasPrefix(secDate) })!
        self.isOpen.toggle()
        self.OrderedListStudyDaysExpand[secDate]! = isopened
        print("클릭 후, \(self.OrderedListStudyDaysExpand)")
        
        CurriculumTableView.reloadSections(IndexSet(secIdx ... secIdx), with: .none)
        UIView.setAnimationsEnabled(true)
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let cell = tableView.cellForRow(at: indexPath)
//        cell?.selectedBackgroundView?.backgroundColor
//        = UIColor.puljaBlack.withAlphaComponent(0.1)
//        CurriculumTableView.deselectRow(at: indexPath, animated: true)
//
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int{
        
        return self.OrderedListStudyDaysCurr.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        //오늘의 커리큘럼이 없는 경우, 아예 보여주지 않음
        if self.OrderedListStudyDaysCurr.keys.count > 0 {
            return 30
        }
        return .leastNonzeroMagnitude
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = CurriculumTableView.dequeueReusableHeaderFooterView(withIdentifier: "StudyCurriculumHeaderView") as! StudyCurriculumHeaderView
        let DateString = Array(self.OrderedListStudyDaysCurr.keys)
        
        header.isBigCircle = true
        header.lbdate = DateString[section]
        header.backgroundColor = UIColor.white
        header.awakeFromNib()
        
        
        return header
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if self.ListStudyDaysCurr.count == 0 {
            return .leastNonzeroMagnitude
        }
        return !isOpen ? 34 : .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        //푸터
        let footerNib = UINib(nibName: "StudyCurriculumFooterView", bundle: nil)
        CurriculumTableView.register(footerNib, forHeaderFooterViewReuseIdentifier: "StudyCurriculumFooterView")
        
        let footer = CurriculumTableView.dequeueReusableHeaderFooterView(withIdentifier: "StudyCurriculumFooterView") as! StudyCurriculumFooterView
        
        
        let StringDates = Array(self.ListStudyDaysCurr.keys)
        let Date = StringDates[section]
        
        footer.ListStudyDaysExpand = self.OrderedListStudyDaysExpand
        footer.ListStudyDaysCurr = self.OrderedListStudyDaysCurr
        footer.secDate = Date
        footer.sectionIdx = section
        footer.totalSection = self.OrderedListStudyDaysCurr.keys.count
        footer.delegate = self
        footer.awakeFromNib()

        return footer
    }
    
    @IBAction func btnChatPressed(_ sender: Any) {
      
        if self.connectFromCoach {
            //CommonUtil.shared.goChatting(viewController: self, userSeq: self.userSeq)
        } else {
            //CommonUtil.shared.goChatting(viewController: self)
        }
//        CommonUtil.shared.goChatting(viewController: self)
      
    }

    @IBAction func ibNext(_ sender: Any) {
        let vc = R.storyboard.study.delayedCurriculumTableViewController()!
        vc.userSeq = userSeq
        vc.currSeq = currSeq
        vc.connectFromCoach = self.connectFromCoach
//        self.present(vc, animated: true)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func ibPrevious(_ sender: Any) {
        let vc = R.storyboard.study.previousCurriculumTableViewController()!
        vc.userSeq = userSeq
        vc.currSeq = currSeq
        vc.lbcurrname = self.curriculuminfo
        vc.connectFromCoach = self.connectFromCoach
//        vc.lbCurrName.text = self.curriculuminfo
//        self.present(vc, animated: true)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

