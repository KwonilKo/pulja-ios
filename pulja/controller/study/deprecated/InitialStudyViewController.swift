//
//  InitialStudyViewController.swift
//  pulja
//
//  Created by kwonilko on 2022/02/24.
//

import UIKit

class InitialStudyViewController: UIViewController {

    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.backgroundColor = UIColor.OffWhite
        
        super.viewDidLoad()

        var payType = ""
        var myInfo:MyInfo? = nil
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let container = appDelegate.persistentContainer
        do {
            let myInfos = try container.viewContext.fetch(MyInfo.fetchRequest())
            if myInfos.count > 0
            {
                myInfo = myInfos[0]
                payType = myInfo?.payType ?? ""
            }
        } catch {
            print(error.localizedDescription)
        }
        
        let userSeq = myInfo?.userSeq ?? 0
        let vc = R.storyboard.study.studyCurriculumViewController()!
        vc.userType = payType
        vc.userSeq = Int(userSeq)
        self.navigationController?.pushViewController(vc, animated: false)
        
        
//        if payType == Const.PayType.free.rawValue || payType == Const.PayType.bought.rawValue
//        {
//            let vc = R.storyboard.study.studyMainViewController()!
//            vc.currtext = "무료 체험하기"
//            vc.userType = payType
//            vc.userSeq = Int(userSeq)
//            self.navigationController?.pushViewController(vc, animated: false)
//        }
//        else
//        {
//            let vc = R.storyboard.study.studyCurriculumViewController()!
//            vc.userType = payType
//            vc.userSeq = Int(userSeq)
//            self.navigationController?.pushViewController(vc, animated: false)
//        }
        
//        var hasCurriculum : Bool = false
//
//        StudyAPI.shared.myCurriculumList(userSeq: Int(userSeq)).done { res in
//            if let suc = res.success, suc == true {
//                guard let data = res.data else { return }
//                if data.count > 0 {
//                    hasCurriculum = true
//                }
//            }
//        }.catch { error in
//
//        }.finally {
//            switch hasCurriculum {
//            case false :
//                let vc = R.storyboard.study.studyMainViewController()!
//                vc.currtext = "무료 체험하기"
//                vc.userType = payType
//                vc.userSeq = Int(userSeq)
//                self.navigationController?.pushViewController(vc, animated: false)
//
//            default:
//                let vc = R.storyboard.study.studyCurriculumViewController()!
//                vc.userType = payType
//                vc.userSeq = Int(userSeq)
//                self.navigationController?.pushViewController(vc, animated: false)
//
//            }
//        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

