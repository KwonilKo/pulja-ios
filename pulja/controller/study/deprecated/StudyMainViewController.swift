//
//  StudyMainViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/01/17.
//

import UIKit

class StudyMainViewController: PuljaBaseViewController {

    
    @IBOutlet weak var middleDiagnosis: UIView!
    
    @IBOutlet var currText: UILabel!
    var currtext: String?
    @IBOutlet weak var highDiagnosis: UIView!
    
    @IBOutlet weak var btnChat: UIButton!
    var userSeq: Int?
    
    
    @IBOutlet var text1: UILabel!
    
    
    @IBOutlet var text2: UILabel!
    
    
    override func viewDidLoad() {
        
        if self.userType! == "f" {
            currtext = "무료 체험하기"
        } else {
            currtext = "수강권 구매하기"
        }
        
        self.currText.text = currtext
        
        super.viewDidLoad()

        checkUnreadMessage()
        checkDiagnosisTest()
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        checkUnreadMessage()
        
        LoadingView.show()
        //진단평가 gesture 추가
        let gesture1 = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender: )))
        middleDiagnosis.addGestureRecognizer(gesture1)
        
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(handleTap2(sender: )))
        highDiagnosis.addGestureRecognizer(gesture2)
        LoadingView.hide()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func checkUnreadMessage()
    {
        let payType = self.myInfo?.payType ?? "f"
        
        if payType == "p"
        {
            CommonUtil.shared.checkNewMessage { hasNew in
                
                if hasNew > 0
                {
                    self.btnChat.setImage(UIImage(resource:R.image.groupChatNew), for: .normal)
                }
                else
                {
                    self.btnChat.setImage(UIImage(resource:R.image.groupChat), for: .normal)
                }
            }
        }
        else
        {
            self.btnChat.setImage(UIImage(resource:R.image.groupChat), for: .normal)
        }

    }
    var userType : String?
    
    //터치 이벤트시, 커리큘럼 상세 화면으로 보내기
    @objc func handleTap(sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Study", bundle: nil)
        StudyAPI.shared.diagnosisReport(userSeq: self.userSeq!, diagnosisSeq: 1).done { res in
            if let suc = res.success, suc == true { //진단평가 본 기록이 있는 경우
                
                let testSeq = res.data?.testSeq ?? 0
                if testSeq == 0
                {
                    let vc = R.storyboard.study.diagnosisMainViewController()!
                    vc.delegate = self
                    vc.diagnosisSeq = 1
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else
                {
                    let vc = R.storyboard.study.diagnosisReportViewController()!
                    vc.tabReport = true
                    vc.userSeq = self.userSeq
                    vc.diagnosisSeq = 1
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                
            } else {
                let vc = R.storyboard.study.diagnosisMainViewController()!
                vc.delegate = self
                vc.diagnosisSeq = 1
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    
    @objc func handleTap2(sender: UITapGestureRecognizer) {
        let storyboard = UIStoryboard(name: "Study", bundle: nil)
        StudyAPI.shared.diagnosisReport(userSeq: self.userSeq!, diagnosisSeq: 2).done { res in
            if let suc = res.success, suc == true { //진단평가 본 기록이 있는 경우
                
                let testSeq = res.data?.testSeq ?? 0
                if testSeq == 0
                {
                    let vc = R.storyboard.study.diagnosisMainViewController()!
                    vc.delegate = self
                    vc.diagnosisSeq = 2
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else
                {
                    let vc = R.storyboard.study.diagnosisReportViewController()!
                    vc.tabReport = true
                    vc.userSeq = self.userSeq
                    vc.diagnosisSeq = 2
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                
            } else {
                let vc = R.storyboard.study.diagnosisMainViewController()!
                vc.delegate = self
                vc.diagnosisSeq = 2
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }

    @IBAction func btnChatPressed(_ sender: Any) {
      
        //CommonUtil.shared.goChatting(viewController: self)
      
    }
    
    @IBAction func nextButton(_ sender: Any) {
        if self.userType! == "f" {
            if let url = URL(string: "\(Const.DOMAIN)/free_trial") {
                UIApplication.shared.open(url, options: [:])
            }
        } else {
            let vc = R.storyboard.main.idpwdViewController()!
            vc.viewTitle = "수강권 결제"
            vc.url = "\(Const.DOMAIN)/subscription_landing"
            vc.modalPresentationStyle = .overFullScreen
            
            self.present(vc, animated: true) {

            }
        }
        
    }
    
    func checkDiagnosisTest() {
        guard let userType = self.userType else { return }
        
        if userType != "p" {
            StudyAPI.shared.diagnosisReport(userSeq: self.userSeq!, diagnosisSeq: 1).done { res in
                if let suc = res.success, suc == true {
                    guard let data = res.data else { return }
                    guard let testSeq = data.testSeq else { return }
                    
                
                    if testSeq != 0 {
                        guard let testDate = data.testDate else { return }
                        let year = testDate.substring(from: 0, to: 3)
                        let month = testDate.substring(from: 6, to: 7)
                        let day = testDate.substring(from: 10, to: 11)
                        self.text1.text = "\(year).\(month).\(day) 진단 완료"
                        
                    }
                }
            }
            StudyAPI.shared.diagnosisReport(userSeq: self.userSeq!, diagnosisSeq: 2).done { res in
                if let suc = res.success, suc == true {
                    guard let data = res.data else { return }
                    guard let testSeq = data.testSeq else { return }
                    
                    if testSeq != 0 {
                        guard let testDate = data.testDate else { return }
                        let year = testDate.substring(from: 0, to: 3)
                        let month = testDate.substring(from: 6, to: 7)
                        let day = testDate.substring(from: 10, to: 11)
                        self.text2.text = "\(year).\(month).\(day) 진단 완료"
                        
                    }
                }
            }
        }
        
    }
    
}


