//
//  DiagnosisReportViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/01/29.
//

import UIKit
import DropDown
import Foundation
import PromiseKit

class DiagnosisReportViewController: PuljaBaseViewController {

    var isGoToRegister = false
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet var testDate: UILabel!
    
   
    @IBOutlet var recText: UILabel!
    @IBOutlet var diagnosisText: UILabel!
    @IBOutlet var gaolText2: UILabel!
    @IBOutlet var goalText1: UILabel!
    
    @IBOutlet var diagnosisName: UILabel!
    
   
    @IBOutlet weak var recBlock2: UIView!
    
    @IBOutlet var targetGrade: UILabel!
    
    
    @IBOutlet var topView: UIView!
    @IBOutlet var targetAvgConceptPoint: UILabel!
    
    
    @IBOutlet var avgConceptPoint: UILabel!
    
    @IBOutlet var currGrade: UILabel!
    
    
    @IBOutlet weak var recBlock: UIView!
    
    
    @IBOutlet var correctRate: UILabel!
    
    @IBOutlet var totalDuration: UILabel!
    
    
    @IBOutlet var totalAverageDuration: UILabel!
    
    
    @IBOutlet var recommendDesc1: UILabel!
    
    @IBOutlet var recommendCurrName1: UILabel!
    
    
    @IBOutlet var recommendCurrName2: UILabel!
    @IBOutlet var recommendDesc2: UILabel!
    
    
    @IBOutlet var recCurrStart: UIButton!
    
    @IBOutlet weak var svBottomHeight: NSLayoutConstraint!
    
    
    var diagnosisFinish : DiagnosisFinish?
    
    var conceptListGroup : [DiagnosisContent]?
    
    
    var targetConceptListGroup : [DiagnosisContent]?
    var dropDown = DropDown()
    var username : String?
    var fromSeven = false
    var fromStudy = false
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var totalView: UIView!

    
    @IBOutlet weak var recBlock1: UIView!
    var userType : String?
    @IBOutlet var buttonText: UILabel!
    @IBOutlet var buttonView: UIView!
    
    var diagnosisUserConceptIndexList : [DiagnosisUserConceptIndexList]?
    var targetConceptListIndexList : [DiagnosisUserConceptIndexList]?
    @IBOutlet var diagnosisStackView: UIStackView!
    var tabReport: Bool = false
    var userSeq : Int? 
    var diagnosisSeq : Int?
    var diagnosisReportFree : Bool = false
    var freeUserMakeCurr = true //true 인 경우에만 자동커리큘럼생성
    var connectFromCoach: Bool = false
    var prevPayType : String = ""
    
    var buttonIdx = 0 {
        didSet {
            
//            let temp = dropDown.dataSource[buttonIdx]
//            print("여기 들어옴: \(dropDown.dataSource[buttonIdx])")
            
            guard let group =  self.conceptListGroup , group.count > 0 else { return }
            self.buttonText.text = self.conceptListGroup![buttonIdx].unit1name
            
            self.diagnosisUserConceptIndexList = self.conceptListGroup![buttonIdx].diagnosisUserConceptIndexList
            self.targetConceptListIndexList = self.targetConceptListGroup![buttonIdx].diagnosisUserConceptIndexList

            self.initDiagnosisStackView(bidx: buttonIdx)
 
        }
    }
    
    func initMakeCurr() {
//        var payType = ""
//        var myInfo:MyInfo? = nil
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let container = appDelegate.persistentContainer
//        do {
//            let myInfos = try container.viewContext.fetch(MyInfo.fetchRequest())
//            if myInfos.count > 0
//            {
//                myInfo = myInfos[0]
//                payType = myInfo?.payType ?? Const.PayType.free.rawValue
//            }
//        } catch {
//            print(error.localizedDescription)
//        }
        let payType = myInfo?.payType ?? Const.PayType.free.rawValue
        let userSeq = myInfo?.userSeq ?? 0
        
        if payType == Const.PayType.free.rawValue
        {
            self.btnClose.isHidden = false
            self.recCurrStart.setTitle("추천 커리큘럼으로 7일 체험 신청하기", for: .normal)
            self.recCurrStart.isHidden = false
        }
        else if payType == Const.PayType.bought.rawValue || payType == Const.PayType.paid.rawValue
        {
            if self.fromSeven || self.prevPayType == Const.PayType.week.rawValue
            {
                self.recCurrStart.setTitle("다음으로", for: .normal)
                self.recCurrStart.isHidden = false
            }
            else
            {
                self.btnClose.isHidden = false
                self.recCurrStart.isHidden = true
                self.svBottomHeight.constant = 0
                
//                self.scrollView.translatesAutoresizingMaskIntoConstraints = false
//                self.scrollView.bottomAnchor.constraint(equalTo: self.totalView.bottomAnchor).isActive = true
            }
            
        }
        else if payType == Const.PayType.week.rawValue
        {
            self.recCurrStart.setTitle("다음으로", for: .normal)
            self.recCurrStart.isHidden = false
        }
        
        /*
        StudyAPI.shared.myCurriculumList(userSeq: Int(userSeq)).done { res in
            if let suc = res.success, suc == true {
                guard let data = res.data else { return }
                if data.count > 0 {
                    self.freeUserMakeCurr = false
                }
            }
        }.catch { error in
            
        }.finally {
            if self.connectFromCoach {
                self.recCurrStart.isHidden = true
                self.svBottomHeight.constant = 0
            } else {
                switch self.freeUserMakeCurr {
                case true :
                    //self.recCurrStart.setTitle("추천 커리큘럼으로 학습 시작하기", for: .normal)
                    if payType == Const.PayType.free.rawValue
                    {
                        self.btnClose.isHidden = false
                        self.recCurrStart.setTitle("추천 커리큘럼으로 7일 체험 신청하기", for: .normal)
                        self.recCurrStart.isHidden = false
                    }
                    else if payType == Const.PayType.bought.rawValue || payType == Const.PayType.paid.rawValue
                    {
                        self.recCurrStart.isHidden = true
                        self.scrollView.translatesAutoresizingMaskIntoConstraints = false
                        self.scrollView.bottomAnchor.constraint(equalTo: self.totalView.bottomAnchor).isActive = true
                    }
                    else if payType == Const.PayType.week.rawValue
                    {
                        self.recCurrStart.setTitle("다음으로", for: .normal)
                        self.recCurrStart.isHidden = false
                    }
                    
                default:
                    self.recCurrStart.isHidden = true
                    self.scrollView.translatesAutoresizingMaskIntoConstraints = false
                    self.scrollView.bottomAnchor.constraint(equalTo: self.totalView.bottomAnchor).isActive = true
                }
            }
            
            
        }
        */
        
        
    }
    
    func initalize() {
        topView.isHidden = false
        scrollView.isHidden = false
        //recCurrStart.isHidden = false
        CommonUtil.hasDiagnosis = true
        
        if self.connectFromCoach {
            self.diagnosisText.text = "\(self.username!)님의\n진단평가 리포트"
            self.recText.text = "\(self.username!)님 추천 커리큘럼"
            
        } else {
            var nameD : String = ""
            if let n = myInfo!.username, n != "username" {
                nameD = n + "님의"
            }
            self.diagnosisText.text = "\(nameD)\n진단평가 리포트"
            
            var nameC: String = ""
            if let n = myInfo!.username, n != "username" {
                nameC = n + "님"
            }
            self.recText.text = "\(nameC) 추천 커리큘럼"
        }
        
        
        
        guard let diagnosisFinish = self.diagnosisFinish else { return }
        
        print("1블록 통과 전")
        //1 블록
        self.diagnosisName.text = diagnosisFinish.diagnosisName
        self.testDate.text = diagnosisFinish.testDate
        print("1블록 통과 후")

        print("2블록 통과 전")
        //2 블록
        self.targetGrade.text = "\(diagnosisFinish.targetGrade ?? 6)등급"
        self.targetAvgConceptPoint.text = "\(Int(diagnosisFinish.targetAvgConceptPoint ?? 0))점"
        self.currGrade.text = "\(diagnosisFinish.currGrade ?? 6)등급"
        self.avgConceptPoint.text = "\(Int(diagnosisFinish.avgConceptPoint ?? 0))점"
        print("2블록 통과 후")

        //3 블록
        self.conceptListGroup = diagnosisFinish.conceptListGroup
        self.targetConceptListGroup = diagnosisFinish.targetConceptListGroup
        self.initialize_concept_block()
        
        self.buttonIdx = 0

        
        //4 블록
        let correctRate = diagnosisFinish.correctRate ?? 0
        let totalDuration = diagnosisFinish.totalDuration ?? 0
        let targetTotalDuration = diagnosisFinish.targetTotalDuration ?? 0
        let averageDuration = diagnosisFinish.averageDuration ?? 0
        let targetAverageDuration = diagnosisFinish.targetAverageDuration ?? 0
        self.correctRate.text = "\(Int(correctRate.rounded()))%"
        
        var min : Int?
        var sec : Int?
        var diff : Int?
        min = Int(totalDuration / 60)
        sec = Int(totalDuration) - 60 * min!
        self.totalDuration.text = "\(min!)분 \(sec!)초"
        diff = Int(totalDuration) - Int(targetTotalDuration)
        
        if diff! > 0 {
            min = Int(diff! / 60)
            sec = diff! - 60 * min!
            goalText1.text = "목표등급 평균 대비 \(min!)분 \(sec!)초 느려요"
        } else {
            min = Int(-diff! / 60)
            sec = -diff! - 60 * min!
            goalText1.text = "목표등급 평균 대비 \(min!)분 \(sec!)초 빨라요"
            
        }
        
        min = Int(averageDuration / 60)
        sec = Int(averageDuration) - 60 * min!
        self.totalAverageDuration.text = "\(min!)분 \(sec!)초"
        
        diff = Int(averageDuration) - Int(targetAverageDuration)
        
        if diff! > 0 {
            min = Int(diff! / 60)
            sec = diff! - 60 * min!
            gaolText2.text = "목표등급 평균 대비 \(min!)분 \(sec!)초 느려요"
        } else {
            min = Int(-diff! / 60)
            sec = -diff! - 60 * min!
            gaolText2.text = "목표등급 평균 대비 \(min!)분 \(sec!)초 빨라요"
            
        }
        
        //5 블록
        if diagnosisFinish.recommendCurriculums?.count ?? 0 > 1 {
            self.recommendDesc1.text = "\(diagnosisFinish.recommendCurriculums?[0].recommendDesc ?? "")"
            
            let currName1 = "\(diagnosisFinish.recommendCurriculums?[0].currName ?? "")"
            self.recommendCurrName1.text = currName1.replacingOccurrences(of: "lv", with: "Lv")
            
            self.recommendDesc2.text = "\(diagnosisFinish.recommendCurriculums?[1].recommendDesc ?? "")"
            
            
            let currName2 = "\(diagnosisFinish.recommendCurriculums?[1].currName ?? "")"
            self.recommendCurrName2.text = currName2.replacingOccurrences(of: "lv", with: "Lv")
            
        } else  if diagnosisFinish.recommendCurriculums?.count ?? 0 > 0{
            self.recBlock2.isHidden = true
            self.recBlock1.translatesAutoresizingMaskIntoConstraints = false
            self.recBlock1.bottomAnchor.constraint(equalTo: self.recBlock.bottomAnchor, constant: 24).isActive = true
            
            self.recommendDesc1.text = "\(diagnosisFinish.recommendCurriculums?[0].recommendDesc ?? "")"
            
            let currName1 = "\(diagnosisFinish.recommendCurriculums?[0].currName ?? "")"
            
            self.recommendCurrName1.text = currName1.replacingOccurrences(of: "lv", with: "Lv")
        }
       //6 블록 뉴
        //사용자 payType갱신
        //2022.05.24
        HomeAPI.shared.initialView().done { res in
            if let suc = res.success, suc == true {
                guard let data = res.data else { return }
                if let userType = data.userType
                {
                    let infos = try self.container.viewContext.fetch(MyInfo.fetchRequest())
                    if infos.count > 0
                    {
                        let newMyInfo = infos[0]
                        newMyInfo.setValue(userType, forKey: "payType")
                        try self.container.viewContext.save()
                        
                        let newMyInfos = try self.container.viewContext.fetch(MyInfo.fetchRequest())
                        self.myInfo = newMyInfos[0]
                    }
                }
                self.initMakeCurr()
            }
        }.catch{error in
            
        }.finally {
            
        }
        
        //home은 너무 느림.
//        HomeAPI.shared.home().done { homRes in
//
//            if let userType = homRes.data?.appHomeView?.userType
//            {
//
//                let infos = try self.container.viewContext.fetch(MyInfo.fetchRequest())
//                if infos.count > 0
//                {
//                    let newMyInfo = infos[0]
//                    newMyInfo.setValue(userType, forKey: "payType")
//                    try self.container.viewContext.save()
//
//                    let newMyInfos = try self.container.viewContext.fetch(MyInfo.fetchRequest())
//                    self.myInfo = newMyInfos[0]
//                }
//
//            }
//
//            self.initMakeCurr()
//
//        }.catch { error in
//
//
//        }.finally {
//
//        }
        
        
//        self.initMakeCurr()
        
        
//        //6 블록
//        var payType = ""
//
//        if let myInfo = self.myInfo
//        {
//            payType = myInfo.payType ?? ""
//        }
//
//        self.userType = payType
//        print("이 유저의 유저타입: \(payType)")
//        if payType == "f" {
//            self.recCurrStart.setTitle("추천 커리큘럼으로 무료 체험하기", for: .normal)
//        } else if payType == "b" {
//            self.recCurrStart.setTitle("추천 커리큘럼으로 학습 시작하기", for: .normal)
//        } else {
//            self.recCurrStart.isHidden = true
//            self.scrollView.translatesAutoresizingMaskIntoConstraints = false
//            self.scrollView.bottomAnchor.constraint(equalTo: self.totalView.bottomAnchor).isActive = true
//        }
        
        
        
//        HomeAPI.shared.initialView().done { res in
//            if let suc = res.success, suc == true {
//                guard let data = res.data else { return }
//                self.userType = data.userType
//                print("이 유저의 유저타입: \(data.userType!)")
//                if data.userType == "f" {
//                    self.recCurrStart.setTitle("추천 커리큘럼으로 무료 체험하기", for: .normal)
//                } else if data.userType == "b" {
//                    self.recCurrStart.setTitle("추천 커리큘럼으로 학습 시작하기", for: .normal)
//                } else {
//                    self.recCurrStart.isHidden = true
//                    self.scrollView.translatesAutoresizingMaskIntoConstraints = false
//                    self.scrollView.bottomAnchor.constraint(equalTo: self.totalView.bottomAnchor).isActive = true
//                }
//            }
//        }
        
        

        
        
    }
    
    
    // MARK: - Make Curriculum
    func makeCurriculum() {
        
        if let userSeq = myInfo?.userSeq, let currSeq = diagnosisFinish?.recommendCurriculums?[0].currSeq {
            
            self.topView.isHidden = true
            self.scrollView.isHidden = true
            self.recCurrStart.isHidden = true
            
            LoadingView.diagshow1(message: "커리큘럼을 생성중입니다..")
            
            StudyAPI.shared.makeCurriculum(userSeq: userSeq, currSeq: currSeq).done { res in
                if let success = res.success, success == true {
//                    CommonUtil.shared.goStudy()
                    self.navigationController?.popToRootViewController(animated: false)
                    //커리큘럼으로 이동.
                }
            }.catch { err in
                print("뭔가 잘못됨")
            }.finally {
                self.topView.isHidden = false
                self.scrollView.isHidden = false
                self.recCurrStart.isHidden = false
                
                LoadingView.hide()
            }
        } else {
            LoadingView.hide()
        }
        
    }
    
    var sectionList: [String?] = []
    
    func initialize_concept_block() {
        let numSec = self.conceptListGroup?.count
        guard let conceptListG = self.conceptListGroup else { return }
        dropDown.textFont = UIFont.boldSystemFont(ofSize: 12)
        dropDown.dataSource = []
        for x in self.conceptListGroup! {
            dropDown.dataSource += [x.unit1name!]
            sectionList += [x.unit1name]
        }
        dropDown.anchorView = buttonView
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        
    }
    
    
    
    override func viewDidLoad() {
        
        
        topView.isHidden = true
        scrollView.isHidden = true
        recCurrStart.isHidden = true
        
//        self.totalView.isHidden = true
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender: )))
        buttonView.addGestureRecognizer(gesture)
        super.viewDidLoad()
        
        self.prevPayType = self.myInfo?.payType ?? "f"
        
        let userSeq = myInfo?.userSeq
        print("***** userseq: \(userSeq) ********")
        if tabReport == false { //진단평가 보고 나서 리포트 보는 경우
            let promise = Promise<DiagnosisFinishRes> { value in
                //processing
                print("api 호출 시작함")
                StudyAPI.shared.diagnosisFinishResult().done { res in
                    if let succ = res.success, succ == true {
                        guard let data = res.data else { return }
                        self.diagnosisFinish = data
                        print("initialize 들어가기 직전")
                        print("initialize 끝난 직후")
                        LoadingView.hide()
                        
                    }
                }.catch { error in
                    
                }.finally {
                    self.topView.isHidden = true
                    self.scrollView.isHidden = true
                    self.recCurrStart.isHidden = true
                    LoadingView.diagshow3()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        
                        print("노티 로딩3")
                        LoadingView.hide()
                        self.initalize()
                        
                    }
                }
                DispatchQueue.main.async {
                    LoadingView.diagshow1(message: "답안을 채점하는 중이에요.")
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                    LoadingView.hide()
                    print("노티 로딩2")
                    LoadingView.diagshow2()
                    
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
//                        LoadingView.hide()
//                        print("노티 로딩3")
//                        LoadingView.diagshow3()
//                    }
                    
                }
            }.done { value in
//                LoadingView.hide()
                print("promise done 끝남")
            }.catch { error in
            }.finally {
                print("최종적으로 실행함")
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    LoadingView.diagshow3()
                    print("노티 로딩3")
                    LoadingView.hide()
                }
                
            }
        }
        else {  //진단평가 탭 리포트 화면 누른 경우 또는 진단평가 본 기록이 있어서 바로 리포트를 생성해서 보여주는 경우
            LoadingView.show()
            if diagnosisReportFree  {
                print("diagnoss tab 화면 눌러서 diagnosisreportviewcontroller에 들어옴")
                guard let userseq = self.userSeq else { return }
                StudyAPI.shared.diagnosisReportFree(userSeq: userseq).done { res in
                    if let succ = res.success, succ == true {
                        guard let data = res.data else { return }
                        self.diagnosisFinish = data
                        self.initalize()
                        self.totalView.isHidden = false
                    }
                }.catch { error in
                    print(error.localizedDescription)
                }.finally {
                    LoadingView.hide()
//                    self.svBottomHeight.constant = 0
                }
            } else {
                print("(수강권 구매 안한경우)진단평가 본 기록이 있어서 응시기록 화면 보여줌")
                guard let userseq = self.userSeq else { return }
                guard let diagnosisseq = self.diagnosisSeq else { return }
                
                StudyAPI.shared.diagnosisReport(userSeq: userseq, diagnosisSeq: diagnosisseq).done { res in
                    if let succ = res.success, succ == true {
                        guard let data = res.data else { return }
                        self.diagnosisFinish = data
                        self.initalize()
                        self.totalView.isHidden = false
                    }
                }.catch { error in
                    print(error.localizedDescription)
                }.finally {
                    LoadingView.hide()
                    
//                    self.svBottomHeight.constant = 0
                }
            }
        }
        
        if self.connectFromCoach {
            recCurrStart.isHidden = true
//            svBottomHeight.constant = 0
        }

        
    }
    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(animated)
        CommonUtil.tabbarController?.tabBar.isHidden = true
        CommonUtil.tabbarController?.bottomNavBar.isHidden = true

    }
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        if !self.isGoToRegister
//        {
//            CommonUtil.tabbarController?.bottomNavBar.isHidden = false
//            CommonUtil.tabbarController?.tabBar.isHidden = false
//        }
//        
//    }

    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
    
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        dropDown.show()
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("선택한 아이템 : \(item)")
            print("인덱스 : \(index)")
//            unit2Button.setTitle(self.conceptListGroup![index].unit1name, for: .highlighted)
            self.buttonIdx = index
            self.dropDown.clearSelection()
        }
        
    }
    
    //color function
    func addColor(_ color1: UIColor, with color2: UIColor) -> UIColor {
        var (r1, g1, b1, a1) = (CGFloat(0), CGFloat(0), CGFloat(0), CGFloat(0))
        var (r2, g2, b2, a2) = (CGFloat(0), CGFloat(0), CGFloat(0), CGFloat(0))

        color1.getRed(&r1, green: &g1, blue: &b1, alpha: &a1)
        color2.getRed(&r2, green: &g2, blue: &b2, alpha: &a2)

        // add the components, but don't let them go above 1.0
        return UIColor(red: min(r1 + r2, 1), green: min(g1 + g2, 1), blue: min(b1 + b2, 1), alpha: (a1 + a2) / 2)
    }
    
    func multiplyColor(_ color: UIColor, by multiplier: CGFloat) -> UIColor {
        var (r, g, b, a) = (CGFloat(0), CGFloat(0), CGFloat(0), CGFloat(0))
        color.getRed(&r, green: &g, blue: &b, alpha: &a)
        return UIColor(red: r * multiplier, green: g * multiplier, blue: b * multiplier, alpha: a)
    }

    
    
    @IBAction func lbBack(_ sender: Any) {
        
        let payType = myInfo?.payType ?? Const.PayType.free.rawValue
        
        if self.connectFromCoach {
            self.navigationController?.popViewController(animated: true)
        } else {
            if payType == Const.PayType.free.rawValue { // self.freeUserMakeCurr {
                self.navigationController?.popToRootViewController(animated: true)
            }
            else if self.fromSeven
            {
                if self.fromStudy
                {
                    self.navigationController?.popToRootViewController(animated: true)
                }
                else
                {
                    if let vcDetail = R.storyboard.main.tabbarController(){
                        self.navigationController?.setViewControllers([vcDetail], animated: false)
                    }
                }
                
            }
            else {
                if self.tabReport {
                    self.navigationController?.popViewController(animated: true)
                } else {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
        
        
//        if self.tabReport {
//            self.navigationController?.popViewController(animated: false)
//        } else {
//            CommonUtil.shared.goStudy()
//            self.navigationController?.popToRootViewController(animated: false)
        
    }
    
    
    @IBAction func nextButton(_ sender: Any) {

    }
    
    
    func initDiagnosisStackView(bidx : Int) {
        guard let diagnosisUserConceptIndexList = self.diagnosisUserConceptIndexList else { return }
        //stack view 초기화
        if self.diagnosisStackView.subviews.count > 1 {
            for i in 0..<self.diagnosisStackView.subviews.count-1 {
                self.diagnosisStackView.subviews[i+1].isHidden = true
            }
        }
        
        for (idx, du) in diagnosisUserConceptIndexList.enumerated() {
            //메인 뷰 추가
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor.white
            if idx == self.diagnosisUserConceptIndexList!.count - 1 {
                view.heightAnchor.constraint(equalToConstant: 55).isActive = true
            } else {
                view.heightAnchor.constraint(equalToConstant: 45).isActive = true
            }
            view.cornerRadius = 10
            view.borderWidth = 0.2
            view.borderColor = UIColor.white
            
            //프로그래스 뷰 추가
            let pg = UIProgressView()
            view.addSubview(pg)
            pg.translatesAutoresizingMaskIntoConstraints = false
            pg.topAnchor.constraint(equalTo: view.topAnchor, constant: 15).isActive = true
            pg.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 14).isActive = true
            pg.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -28).isActive = true
//            pg.trailingAnchor.constraint(equalTo: buttonView.trailingAnchor).isActive = true
            pg.heightAnchor.constraint(equalToConstant: 2.0).isActive = true
            pg.progressTintColor = UIColor.Purple
            pg.trackTintColor = UIColor.Foggy
            pg.progress = Float(du.score!/100.0)
            
            //이미지 추가
            let imageView = UIImageView()
            view.addSubview(imageView)
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.bottomAnchor.constraint(equalTo: pg.topAnchor).isActive = true
            imageView.leadingAnchor.constraint(equalTo: pg.trailingAnchor, constant: -4).isActive = true
            imageView.image = UIImage.init(named: "flag.pdf")
            
            
            //내 unit2score label 추가
            let myUnit2score = UILabel()
            myUnit2score.font = UIFont.init(name:"AppleSDGothicNeo-Bold", size:12)
            myUnit2score.textColor = UIColor.Purple
            view.addSubview(myUnit2score)
            myUnit2score.translatesAutoresizingMaskIntoConstraints = false
            myUnit2score.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
            myUnit2score.topAnchor.constraint(equalTo: pg.bottomAnchor, constant: 8).isActive = true
            
            var unit2name = du.name
            
            if let startIndex = unit2name?.index(of: " "), let endIndex = unit2name?.endIndex {
                let range = startIndex..<endIndex
                unit2name = unit2name?.substring(with: range)
            }
            
            
            
            
            myUnit2score.text = "\(unit2name!) \(Int(du.score!.rounded()))점"
            
            //목표 unit2score label 추가
            let targetUnit2Score = UILabel()
            targetUnit2Score.font = UIFont.init(name:"AppleSDGothicNeo-Bold", size:12)
            targetUnit2Score.textColor = UIColor.Foggy
            view.addSubview(targetUnit2Score)
            targetUnit2Score.translatesAutoresizingMaskIntoConstraints = false
            targetUnit2Score.trailingAnchor.constraint(equalTo: pg.trailingAnchor).isActive = true
            targetUnit2Score.topAnchor.constraint(equalTo: pg.bottomAnchor, constant: 8).isActive = true
            targetUnit2Score.text = "\(targetGrade.text!) 평균 \(Int(targetConceptListIndexList![idx].score!.rounded()))점"
            
            diagnosisStackView.addArrangedSubview(view)
            
        }
        
        
    }
    
}
