

import UIKit
import Collections
import OrderedCollections



class PreviousCurriculumTableViewController: PuljaBaseViewController, UITableViewDataSource, UITableViewDelegate, CustomTableViewDelegate, CustomFooterViewDelegate {
    
    
    let headerMaxHeight = 0.0
    var userSeq : Int = 0
    var currSeq : Int = 0
    
   
    @IBOutlet var emptyView: UIView!
    
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var lbCurrName: UILabel!
    
    @IBOutlet weak var CurriculumTableView: UITableView!{
    didSet{
        CurriculumTableView.contentInset = UIEdgeInsets(top: headerMaxHeight, left: 0, bottom: -10.0, right: -20.0)
    }}
    
    var notDoneCurriculum : [CurriculumUnit] = []
    
    var ListStudyDaysCurr : [String : [String: [CurriculumUnit]]] = [:]
    var OrderedListStudyDaysCurr : OrderedDictionary<String, OrderedDictionary<String, [CurriculumUnit]>> = [:]
    
    var ListStudyDaysExpand : [String : [String : Bool]] = [:]
    var OrderedListStudyDaysExpand : OrderedDictionary<String, OrderedDictionary<String, Bool>> = [:]
    var resultStatus: String?   //cell
    var resultCurrIdx : Int?    //cell
    var lbcurrname : String = ""
    var touchFooter = false
    var connectFromCoach: Bool = false 
    
    func initEmpty() {
        self.CurriculumTableView.isHidden = true
        self.emptyView.isHidden = false
    }
    
    
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//
//        self.ListStudyDaysCurr = [:]
//        self.ListStudyDaysExpand = [:]
//        self.OrderedListStudyDaysCurr = [:]
//        self.OrderedListStudyDaysExpand = [:]
//    }
    
    
    override func viewDidLoad() {
//        LoadingView.show()

        self.lbCurrName.text = lbcurrname
        print("previousCurriculumViewController, userSeq: \(self.userSeq), currSeq: \(self.currSeq)")
        super.viewDidLoad()
        self.emptyView.isHidden = true
        
        CurriculumTableView.separatorStyle = .none
        
        //셀 등록
        self.CurriculumTableView.register(StudyCurriculumTableViewCell.nib(), forCellReuseIdentifier: "StudyCurriculumTableViewCell")
        self.CurriculumTableView.register(StudyCurriculumTableViewCell.nib(), forCellReuseIdentifier: "StudyCurriculumTableViewCell")
        self.CurriculumTableView.register(StudyCurriculumMultipleTableViewCell.nib(), forCellReuseIdentifier: "StudyCurriculumMultipleTableViewCell")
        
        //헤더
        let headerNib = UINib(nibName: "StudyCurriculumHeaderView", bundle: nil)
        CurriculumTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "StudyCurriculumHeaderView")
        
        //푸터
        let footerNib = UINib(nibName: "StudyCurriculumFooterView", bundle: nil)
        CurriculumTableView.register(footerNib, forHeaderFooterViewReuseIdentifier: "StudyCurriculumFooterView")
        
        if #available(iOS 15.0, *) {
            CurriculumTableView.sectionHeaderTopPadding = 0
        }
        
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        //초기화
        self.ListStudyDaysCurr = [:]
        self.ListStudyDaysExpand = [:]
        self.OrderedListStudyDaysCurr = [:]
        self.OrderedListStudyDaysExpand = [:]
        
        super.viewWillAppear(animated)
        
        if self.connectFromCoach {
            
            CommonUtil.coachTabbarController?.tabBar.isHidden = true
            CommonUtil.coachTabbarController?.bottomNavBar.isHidden = true
            
        } else {
            CommonUtil.tabbarController?.tabBar.isHidden = true
            CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        }
        
        initialize()
        
        checkUnreadMessage()

    }
    
    
    func checkUnreadMessage()
           {
               let payType = self.myInfo?.payType ?? "f"
               
               if payType == "p"
               {
                   CommonUtil.shared.checkNewMessage { hasNew in
                       
                       if hasNew > 0
                       {
                           self.btnChat.setImage(UIImage(resource:R.image.groupChatNew), for: .normal)
                       }
                       else
                       {
                           self.btnChat.setImage(UIImage(resource:R.image.groupChat), for: .normal)
                       }
                   }
               }
               else
               {
                   self.btnChat.setImage(UIImage(resource:R.image.groupChat), for: .normal)
               }

           }
    
    
    func initialize() {
        LoadingView.show()
        StudyAPI.shared.myCurriculumUnitList(userSeq: userSeq, currSeq: currSeq ).done { res in
            if let bool = res.success, bool == true {
                self.notDoneCurriculum = res.data ?? []
                if self.notDoneCurriculum.count == 0 { self.initEmpty() }
                
                for nD in self.notDoneCurriculum {
                    var curr = self.ListStudyDaysCurr[nD.studyDay!] ?? [String : [CurriculumUnit]]()
                    var open = self.ListStudyDaysExpand[nD.studyDay!] ?? [String : Bool]()
                    switch nD.studyStatusName! {
                    case "학습 완료":
                        if curr.keys.contains("학습 완료") {
                            curr["학습 완료"]! += [nD]
                        } else {
                            curr["학습 완료"] = [nD]
                        }
                        open["학습 완료"] = false
                    default:
                        if curr.keys.contains("학습 전/중") {
                            curr["학습 전/중"]! += [nD]
                        } else {
                            curr["학습 전/중"] = [nD]
                        }
                        open["학습 전/중"] = false
                    }
//                    open["학습 완료"] = false
                    self.ListStudyDaysExpand[nD.studyDay!] = open
                    self.ListStudyDaysCurr[nD.studyDay!] = curr
                    
                }
                print("밀린 학습 커리큘럼 정보")
                print(self.ListStudyDaysCurr)
                self.sortCurriculum(curriculum: self.ListStudyDaysCurr, expand: self.ListStudyDaysExpand) //커리큘럼 정렬
                if self.ListStudyDaysCurr.count > 0 {
                    self.CurriculumTableView.reloadData()
                }
            }
        }.catch { error in
            print("dddd\(error.localizedDescription)")
            
        }.finally {
            LoadingView.hide()
        }
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        self.touchFooter = false
//        self.ListStudyDaysCurr = [:]
//        self.ListStudyDaysExpand = [:]
//        self.OrderedListStudyDaysCurr = [:]
//        self.OrderedListStudyDaysExpand = [:]
//        
//        super.viewDidAppear(animated)
//        
//        
//        self.initialize()
//
//    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.connectFromCoach {
            
            CommonUtil.coachTabbarController?.tabBar.isHidden = false
            CommonUtil.coachTabbarController?.bottomNavBar.isHidden = false
            
        } else {
            CommonUtil.tabbarController?.tabBar.isHidden = false
            CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        }
    }
    
    
    func sortHelper (curriculum: [CurriculumUnit]) -> [CurriculumUnit] {
        var sortedCurriculum = curriculum
        let temp = ["학습 중": 0, "학습 전": 1, "학습 완료":2]
        return sortedCurriculum.sorted { (lhs, rhs) in
            if temp[lhs.studyStatusName!] == temp[rhs.studyStatusName!] {
                return lhs.cuOrder! < rhs.cuOrder!
            }
            return temp[lhs.studyStatusName!]! < temp[rhs.studyStatusName!]!
        }
    }
        
    func sortCurriculum (curriculum : [String : [String: [CurriculumUnit]]], expand: [String : [String : Bool]] )
    {
        // sort by dates
        var UnorderedDates = Array(curriculum.keys)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        let orderedDates = UnorderedDates.sorted { dateFormatter.date(from: $0)!
            < dateFormatter.date(from: $1)!
        }
        print("오더 데이트: \(orderedDates)")
        
        for d in orderedDates {
            let eachDayCurriculum = curriculum[d]!
            let eachDayExpand = expand[d]!
            
            // 학습 전/중, 학습 완료 sort하기 (ListStudyDaysCurr)
            var ordCurriculum = eachDayCurriculum.sorted(by : { $0.0.count > $1.0.count })
            self.OrderedListStudyDaysCurr[d] = [:]
            for ele in ordCurriculum {
                var ordEachCurriculum = self.sortHelper(curriculum: ele.1)
                self.OrderedListStudyDaysCurr[d]![ele.0] = ordEachCurriculum
            }
            
            // 학습 전/중, 학습 완료 sort하기 (ListStudyDaysExpand)
            var ordExpand = eachDayExpand.sorted(by : { $0.0.count > $1.0.count })
            self.OrderedListStudyDaysExpand[d] = [:]
            for ele in ordExpand {
                self.OrderedListStudyDaysExpand[d]![ele.0] = ele.1
            }
        }
        
        print("정렬 안된 커리큘럼 : \(self.ListStudyDaysCurr)")
        print("정렬 된 커리큘럼: \(self.OrderedListStudyDaysCurr)")
        
    }
    
    
    @IBAction func ibBack(_ sender: Any) {
//        self.dismiss(animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnChatPressed(_ sender: Any) {
        //CommonUtil.shared.goChatting(viewController: self)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int{
        print("섹션 개수는 \(self.ListStudyDaysCurr.count)")
        return self.OrderedListStudyDaysCurr.count
    }
    

    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        let StringDates = Array(self.OrderedListStudyDaysCurr.keys)
//        if StringDates.count == 0 {
//            return 0
//        }
        
        let date = StringDates[indexPath.section]
        //해당 셀에 들어갈 커리컬륨 정보를 저장해주는 helper function
        self.tableViewHelper(oneDayCurr: self.OrderedListStudyDaysCurr[date]!, oneDayExpand: self.OrderedListStudyDaysExpand[date]!,
                             Date: date, rowIndex: indexPath.row)
        guard let CellCurriculum = self.OrderedListStudyDaysCurr[date]![self.resultStatus!]
        else {
            return 68
        }
        
        if CellCurriculum.count == 1 {
          
            return 68
            
        }
        else
        {
            if let openStatus = self.OrderedListStudyDaysExpand[date]![self.resultStatus!], openStatus == true {
                return 68
                
            } else {
             
                return 76
            }
        }

    }
            
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.OrderedListStudyDaysCurr.count == 0 { return 0 }
        
        let skey = Array(self.OrderedListStudyDaysCurr.keys)[section]
        print("키 출력: \(self.OrderedListStudyDaysCurr.keys)")
        let ExpandCurr = self.OrderedListStudyDaysExpand[skey]!
        var numRows = 0
        
        for (each, expandCurr) in ExpandCurr {
            if expandCurr {
                let temp = self.OrderedListStudyDaysCurr[skey]![each]
                print("여기 들어옴,\(skey), \(each), \(expandCurr), \(temp!.count)")
                numRows += temp!.count
            } else {
                numRows += 1
            }
        }
        print(ExpandCurr)
        print("날짜: , \(skey), \(section) 섹션에는 \(numRows) 개의 row필요함")
//        if numRows != 1 {
//            numRows -= 1
//        }
        return numRows
    }
    
    func tableViewHelper(oneDayCurr : OrderedDictionary<String, [CurriculumUnit]>, oneDayExpand: OrderedDictionary<String, Bool>, Date : String, rowIndex : Int) {
        
        let eachCurr = self.OrderedListStudyDaysCurr[Date]!
        let eachExpand = self.OrderedListStudyDaysExpand[Date]!
        
        var temp = 0
        
        for (i, k) in oneDayCurr.keys.enumerated() {
            let temp2 = (oneDayExpand[k] == true ? oneDayCurr[k]!.count : 1)
            if rowIndex >= temp + temp2 {
                temp += temp2
            } else {
                resultStatus = k
                resultCurrIdx = rowIndex - temp
                break
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("cellforrowAt 들어와서 뿌려주")
        let StringDates = Array(self.OrderedListStudyDaysCurr.keys)
        if StringDates.count == 0 { return tableView.dequeueReusableCell(withIdentifier: "StudyCurriculumTableViewCell", for: indexPath)
            as! StudyCurriculumTableViewCell }
        
        let Date = StringDates[indexPath.section]
        
        
        self.tableViewHelper(oneDayCurr: self.OrderedListStudyDaysCurr[Date]!, oneDayExpand: self.OrderedListStudyDaysExpand[Date]!, Date: Date, rowIndex: indexPath.row)
        
        let CellCurriculum = self.OrderedListStudyDaysCurr[Date]![self.resultStatus!]!
        let oneCurr : CurriculumUnit? = self.OrderedListStudyDaysCurr[Date]![self.resultStatus!]![self.resultCurrIdx!]
        
        if CellCurriculum.count == 1 {
            
            let cell:StudyCurriculumTableViewCell = tableView.dequeueReusableCell(withIdentifier: "StudyCurriculumTableViewCell", for: indexPath)
            as! StudyCurriculumTableViewCell

            cell.currUnitName = oneCurr!.currUnitName
            cell.studyStatusName = oneCurr!.studyStatusName
            cell.videoCnt = oneCurr!.videoCnt
            cell.problemCnt = oneCurr!.problemCnt
            cell.sectionIdx = indexPath.section
            cell.totalSection = self.OrderedListStudyDaysCurr.keys.count
            cell.cunitSeq = oneCurr!.cunitSeq
            cell.currSeq = oneCurr!.currSeq
            cell.userSeq = self.userSeq
            cell.studyDate = Date
            cell.delegateController = self
            cell.connectFromCoach = self.connectFromCoach
            
            if self.connectFromCoach, oneCurr!.studyStatusName != "학습 완료" {
                cell.btArrow.isHidden = true
            } else {
                cell.btArrow.isHidden = false
            }

            cell.awakeFromNib()
            
            
            return cell
            
        } else {
            
            let cell:StudyCurriculumMultipleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "StudyCurriculumMultipleTableViewCell", for: indexPath)
            as! StudyCurriculumMultipleTableViewCell
                        
            cell.secDate = Date
            cell.rowStatus = resultStatus!
            cell.ListStudyDaysCurr = self.OrderedListStudyDaysCurr
            cell.ListStudyDaysExpand = self.OrderedListStudyDaysExpand
            cell.currUnitName = oneCurr!.currUnitName
            cell.studyStatusName = oneCurr!.studyStatusName
            cell.videoCnt = oneCurr!.videoCnt
            cell.problemCnt = oneCurr!.problemCnt
            cell.sectionIdx = indexPath.section
            cell.totalSection = self.OrderedListStudyDaysCurr.keys.count
            cell.cunitSeq = oneCurr!.cunitSeq
            cell.currSeq = oneCurr!.currSeq
            cell.userSeq = self.userSeq
            cell.studyDate = Date
            cell.connectFromCoach = self.connectFromCoach
            cell.delegate = self
            cell.delegateController = self
            cell.awakeFromNib()
            
            return cell
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = CurriculumTableView.dequeueReusableHeaderFooterView(withIdentifier: "StudyCurriculumHeaderView") as! StudyCurriculumHeaderView
        
        //마지막 헤더면 isBigCircle = true로 해주기
        let DateString = Array(self.OrderedListStudyDaysCurr.keys)
        header.isBigCircle = false 
        header.lbdate = DateString[section]
        header.sectionIdx = section
        header.totalSection = self.OrderedListStudyDaysCurr.keys.count
        header.awakeFromNib()
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        var Height : CGFloat?
        
        //섹션 안에 있는 row들 isOpen 유무에 따라 헤더 유무 결정
        let StrStudy = Array(self.OrderedListStudyDaysCurr.keys)[section]
        let OneDayCurr = self.OrderedListStudyDaysExpand[StrStudy]!

        var isHidden = true
        for (_, isopen) in OneDayCurr {
            if isopen {
                print("여기 들어옴")
                isHidden = false
                Height = 34
                break
            }
        }
        if !isHidden  {
            print("footer의 위치는 \(section)섹션입니다")
            return Height!
        } else {
            return .leastNonzeroMagnitude
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        //푸터
        let footerNib = UINib(nibName: "StudyCurriculumFooterView", bundle: nil)
        CurriculumTableView.register(footerNib, forHeaderFooterViewReuseIdentifier: "StudyCurriculumFooterView")
        
        let footer = CurriculumTableView.dequeueReusableHeaderFooterView(withIdentifier: "StudyCurriculumFooterView") as! StudyCurriculumFooterView
        let StringDates = Array(self.OrderedListStudyDaysCurr.keys)
        let Date = StringDates[section]
        
        footer.ListStudyDaysExpand = self.OrderedListStudyDaysExpand
        footer.ListStudyDaysCurr = self.OrderedListStudyDaysCurr
        footer.secDate = Date
        footer.sectionIdx = section
        footer.totalSection = self.OrderedListStudyDaysCurr.keys.count
        
        footer.delegate = self
        footer.awakeFromNib()

        return footer
    }
    
    //셀 닫기 함수
    func didTouchFooter(_ secDate: String, _ ListStudyDaysExpand: OrderedDictionary<String, OrderedDictionary<String, Bool>>) {
        
        let currentOffset = self.CurriculumTableView.contentOffset
        print("푸터 클릭하기 전 : \(currentOffset)")
                UIView.setAnimationsEnabled(false)
        let secIdx = Array(self.OrderedListStudyDaysCurr.keys).firstIndex(where: { $0.hasPrefix(secDate) })!
        var isopened : OrderedDictionary<String, Bool> = [:]
        for (eachcurr, isopen) in ListStudyDaysExpand[secDate]! {
            isopened[eachcurr] = false
        }
        self.OrderedListStudyDaysExpand[secDate]! = isopened
        print("클릭 후, \(self.OrderedListStudyDaysExpand)")
//        self.CurriculumTableView.beginUpdates()
        self.CurriculumTableView.reloadSections(IndexSet(secIdx ... secIdx), with: UITableView.RowAnimation.automatic)
//        self.CurriculumTableView.endUpdates()
        UIView.setAnimationsEnabled(true)
//        self.CurriculumTableView.contentOffset.y += 300
        print("푸터 클릭한 후 : \(self.CurriculumTableView.contentOffset)")
        
        
    }
    
    
    //셀 펼치기 함수
    func didTouchCell(_ secDate: String, _ rowStatus: String, _ listStudyDaysExpand : OrderedDictionary<String, OrderedDictionary<String, Bool>>, _ indexPath: IndexPath?) {
        UIView.setAnimationsEnabled(false)
        self.touchFooter = true
        //눌렀기 때문에 isOpened = true로 업데이트 해주기
        self.OrderedListStudyDaysExpand[secDate]![rowStatus] = true
        let secIdx = Array(self.OrderedListStudyDaysCurr.keys).firstIndex(where: { $0.hasPrefix(secDate) })!
        let statusidx = Array(self.OrderedListStudyDaysExpand[secDate]!.keys).firstIndex(where: { $0.hasPrefix(rowStatus)})
        print("셀클릭")
//        self.CurriculumTableView.beginUpdates()
        CurriculumTableView.reloadSections(IndexSet(secIdx ... secIdx), with: UITableView.RowAnimation.automatic)
//        self.CurriculumTableView.endUpdates()
        
        UIView.setAnimationsEnabled(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


