//
//  StudyVideoViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/02/04.
//

import UIKit
import WebKit
import AirBridge

class StudyVideoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, WKUIDelegate, WKNavigationDelegate {
    
    
   
    @IBOutlet var lbMainTitle: UILabel!
    

    @IBOutlet var lbSubTitle: UILabel!
    
    
    @IBOutlet var expalinVideoHeight: NSLayoutConstraint!
    @IBOutlet var explainView: UIView!
    
//    @IBOutlet var wvExplain: WKWebView!
    
    @IBOutlet var videoTableView: UITableView!
    
    @IBOutlet var videoView: UIView!
    
    
    @IBOutlet var nextProbButton: UIButton!
    var userSeq: Int?
    var cunitSeq: Int?
    var currSeq: Int?
    var videoCnt: Int?
    var problemCnt: Int?
    var studyDate : String?
    var celldelegateViewController: PuljaBaseViewController?
    var boolCompleteVideoProb : Bool = false
    var boolC: Bool = false
    
    var videoLectures : [StudyVideo] = []
    var currVideo: StudyVideo? {
        didSet {
            if currVideo != nil {
                self.lbMainTitle.text = currVideo!.title!
                self.lbMainTitle.text = currVideo!.title!
                q = Int(currVideo!.videoDuration! / 60 )
                r = currVideo!.videoDuration! - 60 * q!
                self.lbSubTitle.text = "\(q!)분 \(r!)초 | \(currVideo!.cateName!)"
                self.setUpVideo()
                self.nextProbButton.isHidden = (self.problemCnt! > 0) ? false : true
            }
        }
    }
    
    var q: Int?
    var r: Int?
    var webView: WKWebView!
    
    var currIndex = 0
    var connectFromCoach: Bool = false
    
    
//    //무료기능 추가
//    var cameFromOnboard = false
//    var problemSeq = 0
//    var weakcat4videoLectures : [WeakCategoryVideo] = []
//    var weakCurrVideo : WeakCategoryVideo? {
//        didSet {
//            if weakCurrVideo != nil {
//                self.lbMainTitle.text = weakCurrVideo!.title!
//                q = Int(weakCurrVideo!.videoDuration! / 60 )
//                r = weakCurrVideo!.videoDuration! - 60 * q!
//                self.lbSubTitle.text = "\(q!)분 \(r!)초 | \(weakCurrVideo!.cateName!)"
//                self.setUpVideo()
//                self.nextProbButton.isHidden = (self.problemCnt! > 0) ? false : true
//            }
//        }
//    }
//    var isAIRecommend = false
    
    
    
    override func loadView() {
        super.loadView()
        let contentController = WKUserContentController()
        AirBridge.webInterface()?.inject(to: contentController, withWebToken: Const.AB_WEB_TOKEN)
        let script =
            "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport');" +
                "meta.setAttribute('content', 'width=device-width, initial-scale = 1.0, maximum-scale=5.0, user-scalable = yes');" +
        "document.getElementsByTagName('head')[0].appendChild(meta);"
        let userScript = WKUserScript(source: script,
                                      injectionTime: WKUserScriptInjectionTime.atDocumentEnd,
                                      forMainFrameOnly: true)
        contentController.addUserScript(userScript)
        
        // Bridge 등록
        contentController.add(self, name: "currentDuration")
        contentController.add(self, name: "pause")
       
        
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        
        let config = WKWebViewConfiguration()
        config.preferences = preferences
        config.userContentController = contentController
        
        self.webView = WKWebView(frame: .zero, configuration: config)
        self.webView?.frame = self.explainView.bounds
        self.webView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.explainView.addSubview(self.webView!)
        
        self.webView?.navigationDelegate = self
        self.webView?.uiDelegate = self
        
        
    }
    
    private var authCookie: HTTPCookie? {
          let cookie = HTTPCookie(properties: [
           .domain: Const.COOKIE_DOMAIN,
              .path: "/",
              .name: "JWT-TOKEN",
              .value: CommonUtil.shared.getAccessToken(),
          ])
          return cookie
      }
          
  private var refreshCookie: HTTPCookie? {
      let cookie = HTTPCookie(properties: [
          .domain: Const.COOKIE_DOMAIN,
          .path: "/",
          .name: "REFRESH-TOKEN",
          .value: CommonUtil.shared.getRefreshToken(),
      ])
      return cookie
  }
    
    func setUpVideo() {
//        LoadingView.show()
        guard let url = URL(string: self.currVideo!.videoUrl!) else {
            expalinVideoHeight.constant = 0
            return
        }
//        guard let vlecSeq = self.currVideo!.vlecSeq, let videoKey = self.currVideo!.videoKey, let cunitSeq = cunitSeq, let currSeq = currSeq  else {
//            expalinVideoHeight.constant = 0
//            return
//        }
//
//        let urlString = "\(Const.DOMAIN)/app_video?cunitSeq=\(cunitSeq)&currSeq=\(currSeq)&vlecSeq=\(vlecSeq)&videoKey=\(videoKey)"
//
//        guard let url =  URL(string: urlString) else {
//            expalinVideoHeight.constant = 0
//            return
//        }


        let urlRequest = URLRequest(url: url)

        if let authCookie = authCookie, let refreshCookie = refreshCookie {
            self.webView?.configuration.websiteDataStore.httpCookieStore.setCookie(authCookie, completionHandler: {
                self.webView?.configuration.websiteDataStore.httpCookieStore.setCookie(refreshCookie, completionHandler: {
                    self.webView?.load(urlRequest)
                })
            })
        }
        
        

        
        self.videoView.isHidden = false
        self.videoTableView.isHidden = false
    }
    
    
    
    override func viewDidLoad() {
        self.videoView.isHidden = true
        self.videoTableView.isHidden = true
        self.nextProbButton.isHidden = true
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if self.boolCompleteVideoProb {
            self.nextProbButton.setTitle("풀이 결과 보기", for: .normal)
        }
        
        
        
        //셀 등록
        self.videoTableView.register(StudyVideoTableViewCell.nib(), forCellReuseIdentifier: "StudyVideoTableViewCell")
        
        guard let userseq = self.userSeq else { return }
        guard let cunitseq = self.cunitSeq else { return }
        
        
        getVideoList()
        getProblemResult()
    }
    
    func getVideoList(){
//        LoadingView.show()
                StudyAPI.shared.curriculumStudyVideo(cunitSeq: cunitSeq!, userSeq: userSeq!).done { res in
                    if let bool = res.success, bool == true {
                        print("성공적으로 영상 api 수신함")
                        guard let data = res.data else { return }
                        for (i, d) in data.enumerated() {
                            if i == 0, self.currVideo == nil {
                                self.currVideo = d
                            }
                            
                        }
                        
                        self.videoLectures = data
                        self.videoTableView.reloadData()
                    }
                    
                }.catch { error in
                }.finally {
                    print("파이널리 호출함")
//                    LoadingView.hide()
                }
    }
    
    
    
    func getProblemResult(){
        StudyAPI.shared.studyProblemResult(userSeq: userSeq!, cunitSeq: cunitSeq!).done { res in
            if let bool = res.success, bool == true {
                if let resultBool = res.data, resultBool == true {
                    self.boolCompleteVideoProb = resultBool
                    if self.boolCompleteVideoProb {
                        self.nextProbButton.setTitle("풀이 결과 보기", for: .normal)
                    }
                }
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.connectFromCoach {
            
            CommonUtil.coachTabbarController?.tabBar.isHidden = true
            CommonUtil.coachTabbarController?.bottomNavBar.isHidden = true
            
        } else {
            CommonUtil.tabbarController?.tabBar.isHidden = true
            CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(animated)
        if self.boolCompleteVideoProb && self.boolC {
            self.navigationController?.popViewController(animated: true)
        }
        

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        CommonUtil.tabbarController?.tabBar.isHidden = false
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 58.0;//Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("테이블 뷰 로우 개수: \(self.videoLectures.count)")
        
        return self.videoLectures.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:StudyVideoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "StudyVideoTableViewCell", for: indexPath) as! StudyVideoTableViewCell
        
        cell.selectionStyle = .none
        
        cell.video = self.videoLectures[indexPath.row]
                   
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.currVideo = self.videoLectures[indexPath.row]
        self.currIndex = indexPath.row
    }
    
    @IBAction func ibBack(_ sender: Any) {
//        self.dismiss(animated : true )
        self.webView.evaluateJavaScript("appPause()", completionHandler: {(result, error) in
            if let result = result {
                print(result)
            }
        })
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func nextProb(_ sender: Any) {
        if self.boolCompleteVideoProb {
            let vc = R.storyboard.study.studySolveResultViewController()!
            LoadingView.show()
            StudyAPI.shared.curriculumStudyResult(cunitSeq: self.cunitSeq!, currSeq: self.currSeq!, studyDate: self.studyDate!, userSeq: self.userSeq!).done {
                res in
                if let suc = res.success, suc == true {
                    print("풀이 결과 리스펀스 성공함")
                    guard let solveResult = res.data else { return }
                    let thisWeekStudy = solveResult.thisWeekStudy!
                    let lastWeekStudy = solveResult.lastWeekStudy!
                    let nowStudy = solveResult.nowStudy!
                    
                    let vc = R.storyboard.study.studySolveResultViewController()!
                    vc.userSeq = self.userSeq!
                    vc.thisWeekStudy = thisWeekStudy
                    vc.lastWeekStudy = lastWeekStudy
                    vc.nowStudy = nowStudy
                    vc.currSeq = self.currSeq
                    vc.cunitSeq = self.cunitSeq
                    vc.connectFromCoach = self.connectFromCoach
                    self.boolC = true
                    self.navigationController?.pushViewController(vc, animated: true)
//                    self.navigationController?.popViewController(animated: false, completion:{
//                        self.celldelegateViewController?.navigationController?.pushViewController(vc, animated: false)
//                    })
                }
            }.catch { error in
                
            }.finally {
                LoadingView.hide()
            }
            
        } else {
            print("버튼 누름")
            StudyAPI.shared.curriculumStudyStepInitial(cunitSeq: self.cunitSeq!, currSeq: self.currSeq!, userSeq: self.userSeq!).done { res in
                if let bool = res.success, bool == true {
                    let vc = R.storyboard.study.multipleChoiceViewController()!
                    vc.problemCnt = self.problemCnt
                    vc.videoCnt = self.videoCnt
                    vc.userSeq = self.userSeq
                    vc.currSeq = self.currSeq
                    vc.cunitSeq = self.cunitSeq
    //                vc.trackStatus = 0
                    vc.userResponseList = []
                    vc.studyDate = self.studyDate
                    vc.isLecture = true
                    vc.celldelegateViewController = self.celldelegateViewController
                    vc.videodelegateViewController = self

                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
        }
        
    }
    func webView(_ webView: WKWebView,
                     didFinish navigation: WKNavigation!){
        print("loadview숨김")
//        LoadingView.hide()
    }
    
}


extension StudyVideoViewController: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        switch message.name {
            
        case "pause":
            
            getVideoList()
            
        case "currentDuration":
            
            guard let currentDuration = message.body as? Int else {
                return
            }
            
//            self.videoLectures[currIndex].maxDuration = currentDuration
//            self.videoTableView.reloadData()

            print("currentDuration : \(message.body)")
            
        default:
            break
        }
    }
}
