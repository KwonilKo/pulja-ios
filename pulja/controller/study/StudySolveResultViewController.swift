//
//  StudySolveResultViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/02/09.
//

import UIKit
import Foundation

class StudySolveResultViewController: PuljaBaseViewController {

    
    
    @IBOutlet var lbSolvedProb: UILabel!
    var lbsolvedprob : String?
    
    @IBOutlet var lbCorrectProb: UILabel!
    var lbcorrectprob : String?
    
    @IBOutlet var lbSolvedTime: UILabel!
    var lbsolvedtime : String?
    
    @IBOutlet var lbThisWeekSolvedProb: UILabel!
    var lbthisweeksolvedprob: String?
    
    @IBOutlet var lbCorrectRate: UILabel!
    var lbcorrectrate: String?
    
    
    @IBOutlet var lbProbDiff: UILabel!
    var lbprobdiff : String?
    
    @IBOutlet var imgProbDiff: UIImageView!
    var imgprobdiff : String?
    
    
    @IBOutlet var lbCorrectRateDiff: UILabel!
    var lbcorrectratediff: String?
    
    @IBOutlet var imgCorrectRateDiff: UIImageView!
    var imgcorrectratediff : String?
    
    
    @IBOutlet weak var questionsResultView: UIView!
    
    @IBOutlet weak var btProblemResult: UIButton!
    
    
    var thisWeekStudy : ThisWeekStudy?
    var lastWeekStudy : LastWeekStudy?
    var nowStudy: NowStudy?
    
    var currSeq: Int?
    var cunitSeq: Int?
    var studyDay: String?
    var userSeq : Int = 0 //코치가 접속한 경우 학생 Userseq
    
    var connectFromCoach : Bool = false
    
    
    //무료기능 추가
    var cameFromOnboard = false
    var problemSeq : Int?
    
    @IBOutlet var header: UILabel!
    
    func initialize() {
        guard let tws = self.thisWeekStudy else { return }
        guard let lws = self.lastWeekStudy else { return }
        guard let ns = self.nowStudy else { return }
        if studyDay != nil {
            self.header.text = "코칭 테스트 결과"
        }
        
        //방금 푼 문제
        lbSolvedProb.text = "\(ns.problemCnt!)개"
        let correctRatio = Int(Double(ns.correctCnt! * 100 / ns.problemCnt!).rounded())
        lbCorrectProb.text = "\(ns.correctCnt!)개(\(correctRatio)%)"
        let solvedTimeQ = Int(ns.studyTime!/60)
        let solvedTimeR = ns.studyTime! - 60 * solvedTimeQ
        lbSolvedTime.text = "\(solvedTimeQ)분 \(solvedTimeR)초"
        
        //주간 지표
        lbThisWeekSolvedProb.text = "\(tws.problemCnt!)개"
        if (tws.problemCnt! - lws.problemCnt!) > 0 {
            imgProbDiff.image = UIImage(named: "arrow-sm-up")
            lbProbDiff.text = "\(tws.problemCnt! - lws.problemCnt!)개"
            lbProbDiff.textColor = UIColor.Green
            
        } else {
            imgProbDiff.image = UIImage(named: "arrow-sm-down")
            lbProbDiff.text = "\(lws.problemCnt! - tws.problemCnt!)개"
            lbProbDiff.textColor = UIColor.Foggy
        }
        
        let thisWeekRatio = ( tws.problemCnt! == 0 ) ? 0 : Double(tws.correctCnt! * 100 / tws.problemCnt!)
        let lastWeekRatio = ( lws.problemCnt! == 0 ) ? 0 : Double(lws.correctCnt! * 100 / lws.problemCnt!)
        
        lbCorrectRate.text = "\(Int(thisWeekRatio.rounded()))%"
        if thisWeekRatio >= lastWeekRatio {
            imgCorrectRateDiff.image = UIImage(named: "arrow-sm-up")
            lbCorrectRateDiff.text = "\(Int((thisWeekRatio - lastWeekRatio).rounded()))%P"
            lbCorrectRateDiff.textColor = UIColor.Green
        } else {
            imgCorrectRateDiff.image = UIImage(named: "arrow-sm-down")
            lbCorrectRateDiff.text = "\(Int((lastWeekRatio - thisWeekRatio).rounded()))%P"
            lbCorrectRateDiff.textColor = UIColor.Foggy
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialize()
        
        let image = R.image.iconSolidCheveronRight()!.withRenderingMode(.alwaysTemplate)
        btProblemResult.setImage(image, for: .normal)
        btProblemResult.tintColor = UIColor.white
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(goQuestionsResult(recognizer:)))
        questionsResultView.addGestureRecognizer(gesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
        if self.problemSeq == nil {
            if self.connectFromCoach {
                
                CommonUtil.coachTabbarController?.tabBar.isHidden = true
                CommonUtil.coachTabbarController?.bottomNavBar.isHidden = true
                
            } else {
                CommonUtil.tabbarController?.tabBar.isHidden = true
                CommonUtil.tabbarController?.bottomNavBar.isHidden = true
            }
        } else {
            CommonUtil.tabbarController?.tabBar.isHidden = true
            CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(animated)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        if self.problemSeq == nil {
            if self.connectFromCoach {
                
                CommonUtil.coachTabbarController?.tabBar.isHidden = false
                CommonUtil.coachTabbarController?.bottomNavBar.isHidden = false
                
            } else {
                CommonUtil.tabbarController?.tabBar.isHidden = false
                CommonUtil.tabbarController?.bottomNavBar.isHidden = false
            }
        } else {
            CommonUtil.tabbarController?.tabBar.isHidden = false
            CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        }

    }
    @objc func goQuestionsResult(recognizer: UITapGestureRecognizer){
        
        
        if self.problemSeq == nil {
            if let resultVC = R.storyboard.review.problemResultViewController() {
                if let currSeq = currSeq, let cunitSeq = cunitSeq {
                    
                    resultVC.currSeq = currSeq
                    resultVC.cunitSeq = cunitSeq
                } else {
                    resultVC.studyDay = self.studyDay
                }
                resultVC.userSeq = self.userSeq
                resultVC.connectFromCoach = self.connectFromCoach
                self.navigationController?.pushViewController(resultVC, animated: true)
            }
        } else {
            if let resultVC = R.storyboard.review.problemResultViewController() {
                if let problemSeq = problemSeq {
                    resultVC.cameFromOnboard = cameFromOnboard
                    resultVC.problemSeq = problemSeq
                }
                self.navigationController?.pushViewController(resultVC, animated: true)
            }
            
        }
        
        
       
        
    }
    
    @IBAction func goQuestionResult(_ sender: Any) {
        if self.problemSeq == nil {
            if let resultVC = R.storyboard.review.problemResultViewController() {
                if let currSeq = currSeq, let cunitSeq = cunitSeq {
                    
                    resultVC.currSeq = currSeq
                    resultVC.cunitSeq = cunitSeq
                } else {
                    resultVC.studyDay = self.studyDay
                }
                resultVC.userSeq = self.userSeq
                resultVC.connectFromCoach = self.connectFromCoach
                self.navigationController?.pushViewController(resultVC, animated: true)
            }
        } else {
            if let resultVC = R.storyboard.review.problemResultViewController() {
                if let problemSeq = problemSeq {
                    resultVC.cameFromOnboard = cameFromOnboard
                    resultVC.problemSeq = problemSeq
                }
                self.navigationController?.pushViewController(resultVC, animated: true)
            }
            
        }
    }
    
    
    
    @IBAction func lbSubmit(_ sender: Any) {
        
        if self.problemSeq == nil {
            self.navigationController?.popViewController(animated: true)
        } else {
            if self.cameFromOnboard == true {
                if let vcDetail = R.storyboard.main.tabbarController() {
                    self.navigationController?.setViewControllers([vcDetail], animated: true)
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//                        guard let userType = self.myInfo?.userType else { return }
//                        if userType != "p" {
//                            vcDetail.selectedIndex = 2
//                            vcDetail.bottomNavBar.selectedItem = vcDetail.bottomNavBar.items[2]
//                        }
//                    }
//                    self.navigationController?.setViewControllers([vcDetail], animated: true)
                }
            } else {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
        
        
    }
    
}
