//
//  CustomViewTapGestureRecognizer.swift
//  pulja
//
//  Created by kwonilko on 2022/03/29.
//

import UIKit

class CustomViewTapGestureRecognizer: UITapGestureRecognizer {
    

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesBegan(touches, with: event)

  
        self.view?.backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
        
    }
    
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesMoved(touches, with: event)
        
        self.view?.backgroundColor = UIColor.white


//        for touch in touches {
//            print("view width: \(self.view?.frame.size.width) , height: \(self.view?.frame.size.height)")
//            let location = touch.location(in: self.view)
////            print("point: ", point)
//            // If the finger was dragged outside of the button...
//            if ((location.x < 0 || location.x > (self.view?.frame.size.width)!) ||
//                (location.y < 0 || location.y > (self.view?.frame.size.height)!)) {
//                // Do whatever you need here
//                self.view?.backgroundColor = UIColor.white
//            } else {
//                self.view?.backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
//            }
//        }

       
//        if let bool = self.view?.frame.contains(touch.location(in: self.view)), bool == false {
//                self.view?.backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
//        } else {
//                self.view?.backgroundColor = UIColor.white
//        }
        



//        if let point: CGPoint = touches.first?.location(in: self.view) {
//            print("view width: \(self.view?.frame.size.width) , height: \(self.view?.frame.size.height)")
//            print("point: ", point)
//            // If the finger was dragged outside of the button...
//            if ((point.x < 0 || point.x > (self.view?.frame.size.width)!) ||
//                (point.y < 0 || point.y > (self.view?.frame.size.height)!)) {
//                // Do whatever you need here
//                self.view?.backgroundColor = UIColor.white
//            } else {
//                self.view?.backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
//            }
//        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
        super.touchesEnded(touches, with: event)
         
        self.view?.backgroundColor = UIColor.white
        
    }
    
//    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent) {
//        super.touchesCancelled(touches, with: event)
//        if highlightOnTouch {
//            self.view?.backgroundColor = UIColor.white
//        }
//    }

}
