//
//  StudyCurriculumViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/01/17.
//

import UIKit
import ChannelIOFront
//import PageCallSDK

class StudyCurriculumViewController: PuljaBaseViewController, CustomCurrViewDelegate {
    
    @IBOutlet weak var btnBannerNext: UIButton!
    @IBOutlet weak var coachBgView: UIView!
    @IBOutlet weak var coachBackgroundView: UIView!
    
    @IBOutlet weak var currStatusBgView: UIView!
    
    @IBOutlet weak var currBgView: UIView!
    
    @IBOutlet weak var diagBgView: UIView!
    
    
    @IBOutlet weak var currText: UILabel!
    var currtext: String?
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var emptyView: UIView!
    
    
    
    
    @IBOutlet weak var middleDiagnosis: UIView!
    
    
    @IBOutlet weak var highDiagnosis: UIView!
    @IBOutlet weak var mainStackView: UIStackView!

    
    @IBOutlet var text1: UILabel!
    
    @IBOutlet var text2: UILabel!
    
    
    @IBOutlet weak var diagnosisStackView: UIStackView!
    @IBOutlet weak var btnChat: UIButton!
    var numCurriculum : Int = 0
    var remainCurriculum : Int = 0
    var myCurriculum : [CurriculumSimpleInfo]? = []
    var delegate : CustomCurrViewDelegate?
    var studyCommons : StudyCommons?
    var userSeq : Int?
    var userType : String?
    
    
    let fruitList = ["Avocado", "Peach", "Cucumber", "Kiwi", "Banana", "Apple", "GreenApple"]
    
    
    @IBOutlet var diagView: UIView!
    @IBOutlet var coachView: UIView!
    @IBOutlet var currView: UIView!
    
    @IBOutlet var mainView: UIView!
    
    
    @IBOutlet var alarmImage: UIImageView!
    
    
    @IBOutlet var alarmText: UILabel!
    
    
    @IBOutlet var alarmSubText: UILabel!
    
    var studyAlarm : String?
    var coachurl: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let info = self.myInfo
        {
            self.userSeq = Int(info.userSeq)
            self.userType = info.payType ?? ""
        }
        
        let gesture1 = UITapGestureRecognizer(target: self, action: #selector(handleTap1(sender: )))
        middleDiagnosis.addGestureRecognizer(gesture1)
        
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(handleTap2(sender: )))
        highDiagnosis.addGestureRecognizer(gesture2)
        
//        checkDiagnosisTest()
        
    
    }
    
    
    
    func initStudyAlarm(data: CurriculumSimpleMain ) {
        guard let sA = data.studyAlarm else { return }
        self.studyAlarm = sA
        
        self.coachurl = data.coachingRoomUrl
        
        print("initStudyAlarm 들어옴 ", sA)
        print("userSeq: ", self.userSeq)
        
        self.coachBgView.isHidden = false
        
        switch sA {
        case "T" :
            alarmImage.image = UIImage(named: "Bell")
            alarmText.text = "띵동! 코칭 테스트가 도착했어요."
            alarmSubText.text = "코칭 테스트 보러가기"
        case "R" :
            alarmImage.image = UIImage(named: "Bell")
            alarmText.text = "띵동! 지난주 학습 리포트가 도착했어요."
            alarmSubText.text = "주간 학습 리포트 보러가기"
        case "C" :
            alarmImage.image = UIImage(named: "hand")
            alarmText.text = "코치님과의 화상코칭이 곧 시작돼요."
            alarmSubText.text = "1:1 코칭 수업 입장하기"
        default :
            self.coachBgView.isHidden = true
//            self.currView.translatesAutoresizingMaskIntoConstraints = false
//            self.currView.topAnchor.constraint(equalTo: self.mainView.topAnchor, constant: 12.0).isActive = true
            
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        CommonUtil.tabbarController?.tabBar.isHidden = true 
        
        
        CommonUtil.tabbarController?.tabBar.isHidden = false
        CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        
        self.userType = self.myInfo?.payType ?? Const.PayType.free.rawValue
        checkUnreadMessage()
        
        let payType = self.myInfo?.payType ?? "f"
        if payType == "p"
        {
            self.scrollView.isHidden = false
            self.emptyView.isHidden = true
            checkDiagnosisTest()
            
            self.coachBgView.isHidden = true
            self.currStatusBgView.isHidden = true
            self.currBgView.isHidden = true

            
            diagnosisStackView.isHidden = false
            self.studyCommons = StudyCommons()
            self.studyCommons?.delegate = self
            
            self.checkStackView()
        }
        else
        {
            self.scrollView.isHidden = true
            self.emptyView.isHidden = false
        }
        
        
        
        
        
    }
    
    func checkStackView()
    {
        guard let payType = self.myInfo?.payType else { return }
        
        if payType == Const.PayType.week.rawValue
        {
            self.currStatusBgView.isHidden = false
            self.currText.text = "커리큘럼 배정 전이에요."
        }
        else if payType == Const.PayType.free.rawValue
        {
            
            self.coachBgView.isHidden = false
            self.coachBackgroundView.backgroundColor = R.color.green()
            alarmImage.image = UIImage(resource: R.image.iconRocket)
            alarmText.text = "나만의 코치쌤과 내 취약 유형 정복하자!"
            alarmSubText.text = "풀자 7일 체험 신청하기"
            alarmText.textColor = UIColor.white
            alarmSubText.textColor = UIColor.white
            self.currStatusBgView.isHidden = false
            self.currText.text = "커리큘럼 배정 전이에요."
            
            self.btnBannerNext.setImage(R.image.iconWhiteRight(), for: .normal)
            
        }
        else if payType == Const.PayType.bought.rawValue
        {
            self.currStatusBgView.isHidden = false
            self.currText.text = "진행 중인 커리큘럼이 없어요."
        }
        else if payType == Const.PayType.paid.rawValue
        {
            self.currBgView.isHidden = false
            
            self.mainStackInit()
            LoadingView.show()
            StudyAPI.shared.studyMain(userSeq: self.userSeq!).done { res in
                if let bool = res.success, bool == true {
                    guard let data = res.data else { return }

                    
                    //studyAlarm에 따른 분기처리
                    self.initStudyAlarm(data: data)
                    
                    self.myCurriculum = data.curriculumList!
                    self.studyCommons?.myCurriculum = data.curriculumList

                    if data.curriculumList!.count < 1
                    {
                        self.currBgView.isHidden = true
                        self.currStatusBgView.isHidden = false
                        self.currText.text = "커리큘럼 배정 전이에요."
                    }
                    else
                    {
                        //Study Commons 파일 호출
                        self.mainStackView = self.studyCommons?.initStackView(mainStackView: self.mainStackView, myCurriculum: self.myCurriculum, isBool: true, restOfC: data.restOfCurriculum)
                    }


                }


            }.catch { error in
                print(error.localizedDescription)
            }.finally{
                LoadingView.hide()
            }
        }
    }
    func mainStackInit() {
        self.mainStackView.subviews[1...].map({ $0.removeFromSuperview() })
    }
    
    
    func checkUnreadMessage()
    {
        let payType = self.myInfo?.payType ?? "f"
        
        if payType == "p"
        {
            CommonUtil.shared.checkNewMessage { hasNew in
                
                if hasNew > 0
                {
                    self.btnChat.setImage(UIImage(resource:R.image.groupChatNew), for: .normal)
                }
                else
                {
                    self.btnChat.setImage(UIImage(resource:R.image.groupChat), for: .normal)
                }
            }
        }
        else
        {
            self.btnChat.setImage(UIImage(resource:R.image.groupChat), for: .normal)
        }

    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
    @IBAction func btnChatPressed(_ sender: Any) {
      
        //CommonUtil.shared.goChatting(viewController: self)
      
    }
    
    
    @IBAction func nextButton(_ sender: Any) {
        
        guard let payType = self.myInfo?.payType else { return }
        
        //구매이력이 있고 현재 활성화된 것이 없는 사용자는 결제화면으로 이동.
        //정책확인 필요.
        if payType == Const.PayType.bought.rawValue
        {
            if let tabbar = CommonUtil.tabbarController
            {
                CommonUtil.shared.showAlert(title: "수강권이 종료되었어요.", message: "풀자 웹사이트에서 수강권 결제 후\n학습을 이어갈 수 있어요.", button: "확인", viewController: tabbar)
            }
            
//            let vc = R.storyboard.main.idpwdViewController()!
//            vc.viewTitle = "수강권 결제"
//            vc.url = "\(Const.DOMAIN)/subscription_landing"
//            vc.modalPresentationStyle = .overFullScreen
//            self.present(vc, animated: true) {}
        }
        else
        {
            let vc = R.storyboard.study.studyMyCurriculumViewController()!
            vc.userSeq = self.userSeq
            self.navigationController?.pushViewController(vc, animated: true)
        }
            
        
        
        
        
        
    }
    
    
    
    @IBAction func lbCoach(_ sender: Any) {
        
        
        guard let payType = self.myInfo?.payType else { return }
        
        
        if payType == Const.PayType.free.rawValue
        {
            self.eventSevenBanner() //신청여부 체크 후 신청.
        }
        else if payType == Const.PayType.paid.rawValue
        {
            guard let studyalarm = self.studyAlarm else { return }
            
            switch studyalarm {
            case "T":
                let vc = R.storyboard.study.coachTestViewController()!
                vc.userSeq = self.userSeq!
                
        //      ---sample---
                self.navigationController?.pushViewController(vc, animated: true)
            case "R":
                let vc = R.storyboard.main.idpwdViewController()!
                vc.viewTitle = "주간 학습 리포트"
                vc.url = "https://www.pulja.co.kr/student_report_mobile?userSeq=" + "\(self.userSeq!)"
                vc.modalPresentationStyle = .overFullScreen
                StudyAPI.shared.weeklyReport(userSeq: self.userSeq!).done { res in
                    if let suc = res.success, suc == true {
                        print("진단리포트 확인함")
                    }
                }
                self.present(vc, animated: true) {

                }
            default:
                guard let url = self.coachurl else { return }
                
                self.loadPageCall(strUrl: url)

            }
        }
        
        
    }
    
    func eventSevenBanner(){
        
        
        //7일체험 신청 가능한지 먼저 확인하고.....
        LoadingView.show()
        let userSeq = self.myInfo?.userSeq ?? 0
        
        UserAPI.shared.is_available_seven_req(userSeq: Int(userSeq)).done { res in
            
            CommonUtil.tabbarController?.tabBar.isHidden = true
            CommonUtil.tabbarController?.bottomNavBar.isHidden = true
            
        }.catch { error in
        
            CommonUtil().showAlert(title: "7일체험을 신청할 수 없어요.", message: "풀자 무료체험 이력 혹은 수강 이력이 있는 경우 7일체험 신청이 불가능해요.", button: "확인", subButton: "문의하기", viewController: self) { isSend in
                
                if !isSend
                {
                    self.initChennelTalk()
                }
            }
            
        }.finally {
            LoadingView.hide()
        }
                        
        
        
    }
    
    
       
    
    func didShowNext(_ isBool: Bool?, _ idx: Int) {
        guard let b = isBool else { return }
        if b {
            let vc = R.storyboard.study.studyMyCurriculumViewController()!
            vc.userSeq = self.userSeq
            
            UIView.animate(withDuration: 0.1, animations: {
                self.mainStackView.subviews[idx].backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
            }, completion: { finished in
                self.mainStackView.subviews[idx].backgroundColor = UIColor.white
                self.navigationController?.pushViewController(vc, animated: true)
            })
            
//            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    func didTouchView(_ cuInfo: CurriculumSimpleInfo?, _ idx : Int) {
        let storyboard = UIStoryboard(name: "Study", bundle: nil)
        let vc = R.storyboard.study.myCurriculumViewController()!
        vc.userSeq = self.userSeq!
        vc.currSeq = cuInfo!.currSeq!
        
//        vc.userSeq = 457
//        vc.currSeq = 2405
        print("userSeq: \(vc.userSeq), currSeq: \(vc.currSeq)")
//        LoadingView.show()
        
        
       
        UIView.animate(withDuration: 0.1, animations: {
            self.mainStackView.subviews[idx].backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
        }, completion: { finished in
            self.mainStackView.subviews[idx].backgroundColor = UIColor.white
            self.navigationController?.pushViewController(vc, animated: true)
        })
        
        
    }
    
    @objc func handleTap1(sender: UITapGestureRecognizer) {
//        self.middleDiagnosis.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        
        guard let usertype = self.userType else { return }
        let tabReport = (userType == "p") ? false : true
        if tabReport {
            StudyAPI.shared.diagnosisReport(userSeq: self.userSeq!, diagnosisSeq: 1).done { res in
                if let suc = res.success, suc == true { //진단평가 본 기록이 있는 경우
                    
                    let testSeq = res.data?.testSeq ?? 0
                    if testSeq == 0
                    {
                        let vc = R.storyboard.study.diagnosisMainViewController()!
                        vc.delegate = self
                        vc.diagnosisSeq = 1
                        UIView.animate(withDuration: 0.1, animations: {
                            self.middleDiagnosis.backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
                        }, completion:
                            { finished in
                            self.middleDiagnosis.backgroundColor = UIColor.white
                            self.navigationController?.pushViewController(vc, animated: true)
                        })
            
                    }
                    else
                    {
                        let vc = R.storyboard.study.diagnosisReportViewController()!
                        vc.tabReport = true
                        vc.userSeq = self.userSeq
                        vc.diagnosisSeq = 1
                        UIView.animate(withDuration: 0.1, animations: {
                            self.middleDiagnosis.backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
                        }, completion:
                            { finished in
                            self.middleDiagnosis.backgroundColor = UIColor.white
                            self.navigationController?.pushViewController(vc, animated: true)
                        })
                    }
                    
                    
                } else {
                    let vc = R.storyboard.study.diagnosisMainViewController()!
                    vc.delegate = self
                    vc.diagnosisSeq = 1
                    UIView.animate(withDuration: 0.1, animations: {
                        self.middleDiagnosis.backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
                    }, completion:
                        { finished in
                        self.middleDiagnosis.backgroundColor = UIColor.white
                        self.navigationController?.pushViewController(vc, animated: true)
                    })
                }
            }
            
        } else {
            let storyboard = UIStoryboard(name: "Study", bundle: nil)
            let vc = R.storyboard.study.diagnosisMainViewController()!
            vc.diagnosisSeq = 1
            UIView.animate(withDuration: 0.1, animations: {
                self.middleDiagnosis.backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
            }, completion:
                { finished in
                self.middleDiagnosis.backgroundColor = UIColor.white
                self.navigationController?.pushViewController(vc, animated: true)
            })
        }
        
        
//        self.present(vc, animated: true)
        
    }
    
    @objc func handleTap2(sender: UITapGestureRecognizer) {
        guard let usertype = self.userType else { return }
        let tabReport = (userType == "p") ? false : true
        if tabReport {
            StudyAPI.shared.diagnosisReport(userSeq: self.userSeq!, diagnosisSeq: 2).done { res in
                if let suc = res.success, suc == true { //진단평가 본 기록이 있는 경우
                    
                    let testSeq = res.data?.testSeq ?? 0
                    if testSeq == 0
                    {
                        let vc = R.storyboard.study.diagnosisMainViewController()!
                        vc.delegate = self
                        vc.diagnosisSeq = 2
                        UIView.animate(withDuration: 0.1, animations: {
                            self.highDiagnosis.backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
                        }, completion:
                            { finished in
                            self.highDiagnosis.backgroundColor = UIColor.white
                            self.navigationController?.pushViewController(vc, animated: true)
                        })
                    }
                    else
                    {
                        let vc = R.storyboard.study.diagnosisReportViewController()!
                        vc.tabReport = true
                        vc.userSeq = self.userSeq
                        vc.diagnosisSeq = 2
                        UIView.animate(withDuration: 0.1, animations: {
                            self.highDiagnosis.backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
                        }, completion:
                            { finished in
                            self.highDiagnosis.backgroundColor = UIColor.white
                            self.navigationController?.pushViewController(vc, animated: true)
                        })
                    }
                    
                    
                } else {
                    let vc = R.storyboard.study.diagnosisMainViewController()!
                    vc.delegate = self
                    vc.diagnosisSeq = 2
                    UIView.animate(withDuration: 0.1, animations: {
                        self.highDiagnosis.backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
                    }, completion:
                        { finished in
                        self.highDiagnosis.backgroundColor = UIColor.white
                        self.navigationController?.pushViewController(vc, animated: true)
                    })
                }
            }
            
        } else {
            let storyboard = UIStoryboard(name: "Study", bundle: nil)
            let vc = R.storyboard.study.diagnosisMainViewController()!
            vc.diagnosisSeq = 2
            UIView.animate(withDuration: 0.1, animations: {
                self.highDiagnosis.backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
            }, completion:
                { finished in
                self.highDiagnosis.backgroundColor = UIColor.white
                self.navigationController?.pushViewController(vc, animated: true)
            })
        }
    }
    
    func checkDiagnosisTest() {
        guard let userType = self.userType else { return }
        
        if userType != "p" {
            StudyAPI.shared.diagnosisReport(userSeq: self.userSeq!, diagnosisSeq: 1).done { res in
                if let suc = res.success, suc == true {
                    guard let data = res.data else { return }
                    guard let testSeq = data.testSeq else { return }
                    
                
                    if testSeq != 0 {
                        guard let testDate = data.testDate else { return }
                        let year = testDate.substring(from: 0, to: 3)
                        let month = testDate.substring(from: 6, to: 7)
                        let day = testDate.substring(from: 10, to: 11)
                        self.text1.text = "\(year).\(month).\(day) 진단 완료"
                        
                    }
                }
            }
            StudyAPI.shared.diagnosisReport(userSeq: self.userSeq!, diagnosisSeq: 2).done { res in
                if let suc = res.success, suc == true {
                    guard let data = res.data else { return }
                    guard let testSeq = data.testSeq else { return }
                    
                    if testSeq != 0 {
                        guard let testDate = data.testDate else { return }
                        let year = testDate.substring(from: 0, to: 3)
                        let month = testDate.substring(from: 6, to: 7)
                        let day = testDate.substring(from: 10, to: 11)
                        self.text2.text = "\(year).\(month).\(day) 진단 완료"
                        
                    }
                }
            }
        }
        
    }
    
    // MARK: PageCall
    func loadPageCall(strUrl: String) {
        let vc = R.storyboard.home.pageCallViewController()!
        vc.callUrl = strUrl
        self.present(vc, animated: true, completion: nil)
        
//       DispatchQueue.main.async {
//           let pageCall = PageCall.sharedInstance()
//           pageCall.delegate = self
//           #if DEBUG
//           #else
//           // pagecall log
//           pageCall.redirectLogToDocuments(withTimeInterval: 4)
//           #endif
//           // PageCall MainViewController present
//           pageCall.mainViewController!.modalPresentationStyle = .popover
//           self.present(pageCall.mainViewController!, animated: true, completion: {
//               pageCall.webViewLoadRequest(withURLString: strUrl)
//           })
//       }
    }
    
    
    func initChennelTalk(){
           
           guard let info = self.myInfo else { return }
           guard let userName = info.username else { return }
           guard let userId = info.hashUserSeq else { return }
           
           let profile = Profile()
             .set(name: userName)


           let buttonOption = ChannelButtonOption.init(
               position: .right,
             xMargin: 16,
             yMargin: 23
           )

           let bootConfig = BootConfig.init(
               pluginKey: Const.CHANNELTALK_PLUGIN_KEY,
             memberId: userId,
   //          memberHash: "",
             profile: profile,
             channelButtonOption: buttonOption,
             hidePopup: false,
             trackDefaultEvent: true,
             language: .korean
           )

           ChannelIO.boot(with: bootConfig) { (bootStatus, user) in
               if bootStatus == .success, let user = user {
                   // success
                   ChannelIO.showMessenger()
               } else {
                   // show failed reason from bootStatus
               }
           }
           

       }

    
    
}


// MARK: PageCallDelegate
//extension StudyCurriculumViewController: PageCallDelegate {
//    func pageCallDidClose() {
//        print("pageCallDidClose")
//    }
//
//    func pageCallDidReceive(_ message: WKScriptMessage) {
//        print("pageCallDidReceive message")
//    }
//    
//    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
//        print("webView decidePolicyFor navigationAction")
//        decisionHandler(.allow)
//    }
//}


