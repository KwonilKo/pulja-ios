//
//  MultipleChoiceViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/01/26.
//

import UIKit
import MaterialComponents
import PromiseKit
import Pageboy
import PencilKit

class MultipleChoiceViewController: PuljaBaseViewController,
                                    MDCBottomSheetControllerDelegate, UITextFieldDelegate, UIScrollViewDelegate, PKCanvasViewDelegate, PKToolPickerObserver {

    var userResponseList: [StudyCommons.userResponse]?
    var uR: StudyCommons.userResponse?
    
    @IBOutlet var svProblemImage: UIScrollView!
    @IBOutlet var lineView: UIView!
    @IBOutlet var headerView: UIView!
    @IBOutlet var answerView: AnswerSwipeView!
    
    @IBOutlet var multipleOptions: [UIButton]!
    
    @IBOutlet var ivProblem: UIImageView!
    var problemUrl : URL?
    
    @IBOutlet var multipleStack: UIStackView!
    
    @IBOutlet var lbCurr: UILabel!
    
    @IBOutlet var lbProbNum: UILabel!
    
    @IBOutlet weak var btQuit: UIButton!
    
    @IBOutlet weak var quitHeight: NSLayoutConstraint!
    
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    @IBOutlet var ibBack: UIButton!
    @IBOutlet var bottomView: UIView!
    
    
    @IBOutlet var totalView: UIView!
    
    @IBOutlet var problemIVHeight: NSLayoutConstraint!
    
    
    
    @IBOutlet weak var btPencil: UIButton!
    @IBOutlet weak var pencilToolView: UIView!
    @IBOutlet weak var pkCanvasView: PKCanvasView!
    var toolPicker: PKToolPicker!
    
    
    
    var keyboardSize: CGRect?
    
    var indexOfOneAndOnly : [Int]?
    var drawView = AnswerSwipeView()
    
    var isInit = false
    
    var button = UIButton()
    var tfAnswer = UITextField()
    var userId: String?
    var userSeq: Int?
    var currSeq: Int?
    var cunitSeq: Int?
    var problemCnt: Int?
    var videoCnt: Int?
    var questionId: Int?
//    var trackStatus: Int?
//    var trackQ: Int?
    var totalQ: Int?
    var trackT: String?
    var isButtonAction: Bool?
    
    var startTime : DispatchTime?
    var endTime : DispatchTime?
    var startT : Date?
    var isMultipleAnswers : Bool = false
    var studyDate : String?
    var last: Bool?
    var celldelegateViewController : PuljaBaseViewController?
    var videodelegateViewController : StudyVideoViewController?
    var isLecture: Bool = false
    var ansToServer: String?
    var diagnosisSeq : Int?
    var isDiagnosis : Bool? = false
    var fromSeven = false
    var fromStudy = false
    var diagnosisInput : DiagnosisInput?
    var delegate : PuljaBaseViewController?
    var diagnosisLast: Bool?
    var isCoachingTest: Bool?
    var addSeq: Int?
    
    
    
    var number: Int? {
        didSet {
            if let a = self.isAIRecommend, a == true {
                if let d = self.isAIDiagnosis, d == true {
                    self.ibBack.isHidden = true
                } else {
                    if self.isLecture == true {
                        if let n = self.number, n == 1 {
                            self.ibBack.isHidden = false
                            self.ibBack.setTitle("이전 강의로", for: .normal)
                        } else {
                            self.ibBack.isHidden = false
                            self.ibBack.setTitle("이전 문제로", for: .normal)
                        }
                    } else {
                        if let n = self.number, n == 1 {
                            self.ibBack.isHidden = true
                        } else {
                            self.ibBack.isHidden = false
                            self.ibBack.setTitle("이전 문제로", for: .normal)
                        }
                    }
                }
            } else {
                if let b = self.isDiagnosis, b == true {
                    self.ibBack.isHidden = true
                } else {
                    if self.isLecture == true {
                        if let n = self.number, n == 1 {
                            self.ibBack.isHidden = false
                            self.ibBack.setTitle("이전 강의로", for: .normal)
                        } else {
                            self.ibBack.isHidden = false
                            self.ibBack.setTitle("이전 문제로", for: .normal)
                        }
                    } else {
                        if let n = self.number, n == 1 {
                            self.ibBack.isHidden = true
                        } else {
                            self.ibBack.isHidden = false
                            self.ibBack.setTitle("이전 문제로", for: .normal)
                        }
                    }
                }
            }
        }
    }
    var hasComeFromLecture = false
    
    var isMultipleChoice : Bool = true {
        didSet {
            if !isMultipleChoice {  //주관식인 경우
                self.answerView.isHidden = false
                self.bottomView.isHidden = false
//                self.setupAnimationCustomView(constant: -1)
                //주관식 문제 셋업
                self.multipleStack.isHidden = true
                self.tfAnswer.isHidden = false
                
                if !(self.tfAnswer.text?.isEmpty ?? true) {
//                if let t = tfAnswer.text, t != "정답을 입력하세요" {
//                    print("여기 문제의 text: \(t)")
                    self.button.isHidden = false
                    self.answerView.isHidden = true
                } else {
                    self.button.isHidden = true
                    self.answerView.isHidden = false
                }
                
            } else {    //객관식인 경우
                self.bottomView.isHidden = false
//                self.setupAnimationCustomView(constant: -1)
                self.multipleStack.isHidden = false
                self.tfAnswer.isHidden = true
                self.answerView.isHidden = false
            }
        }
    }
    
    
    //무료기능 추가
    var aidelegateController: UIViewController?
    var problemSeq = -1
    var cameFromOnboard : Bool?
    var unit2Sname = ""
    var isAIRecommend : Bool?
    var isAIDiagnosis : Bool?
    var weakLast = false
    
    
    
    func createtfAnswer() {
        //add some delegations
        tfAnswer.keyboardType = .default
        tfAnswer.addTarget(self, action: #selector(MultipleChoiceViewController.textFieldDidChange(_:)), for: .editingChanged)
        
        //design
        self.bottomView.addSubview(tfAnswer)
        tfAnswer.borderStyle = .roundedRect
        tfAnswer.placeholder = "정답을 입력하세요"
        tfAnswer.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 18)
        tfAnswer.textColor = UIColor.puljaBlack
        //tfAnswer.clearsOnBeginEditing = true
//        tfAnswer.isUserInteractionEnabled = true
        tfAnswer.addDoneButtonOnKeyboard()
        tfAnswer.returnKeyType = .done
        
        //setting constraints
        tfAnswer.translatesAutoresizingMaskIntoConstraints = false
        tfAnswer.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        tfAnswer.topAnchor.constraint(equalTo: self.lbCurr.bottomAnchor, constant: 16).isActive = true
        tfAnswer.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        tfAnswer.widthAnchor.constraint(equalTo: self.answerView.widthAnchor).isActive = true
    
        self.tfAnswer.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    //새로운 문제(그 다음 문제) 푸는 경우
    func initialize() {
        if pkCanvasView != nil {
            pkCanvasView.drawing = PKDrawing()
        }

//        ibBack.isHidden = false
//        ibBack.setTitle("이전 문제로", for: .normal)

        //answerswipeview에 전달
        self.answerView.userResponseList = self.userResponseList
//        self.answerView.trackStatus = self.trackStatus
        
        //그 전 문제 푸는 경우
        guard let num = self.number else { return }
        if num < self.userResponseList!.count { /// < 를 <= 으로 바꿈
            print("현재위치: \(num), 총 길이: \(self.userResponseList!.count)")
            print("현재위치userResponseL: \(self.userResponseList!)")
            self.previous()
        } else {
            //세로운 문제 푸는 경우
            
            if self.userResponseList![num-1].answer != nil {
                self.previous()
            } else {

                //버튼 초기화
                self.answerView.isHidden = false
                indexOfOneAndOnly = nil
    //            tfAnswer.text = "정답을 입력하세요"
                for m in multipleOptions {
                    m.isSelected = true
                    m.backgroundColor = UIColor.white
                    m.tintColor = UIColor.puljaBlack
                    m.isSelected = false
                }
                //initialize answerView
                self.answerView.setUp()
                self.answerView.swipeImageView.alpha = 1.0
                self.answerView.backgroundColor = UIColor.Foggy
                self.answerView.problemCnt = problemCnt
                self.answerView.videoCnt = videoCnt
                self.answerView.userSeq = userSeq
                self.answerView.currSeq = currSeq
                self.answerView.cunitSeq = cunitSeq
                self.answerView.delegateViewController = self
                
                button.isHidden = true
            }
        }
    }
        
    func viewprobImg() {
        guard let num = self.number else { return }
//        let tempQ = (num - 1 < self.userResponseList!.count) ? self.userResponseList![num - 1].qId : self.questionId
        let tempQ = self.questionId
        let qUrl = URL(string: "\(Const.PROBLEM_IMAGE_URL)\(tempQ ?? 0).jpg")
        
        print("현재 문제 아이디: \(tempQ)")
        
        //문제 load
        self.ivProblem.kf.indicatorType = .activity
        self.ivProblem.kf.setImage(with: qUrl, options: [.transition(.fade(0.2))],
                                   completionHandler: {
            result in
            switch result{
            case .success(let value):
                
                let image = value.image
                self.ivProblem.image = image.aspectFitImage(inRect: self.ivProblem.frame)
                self.ivProblem.contentMode = .top
                self.svProblemImage.isHidden = false
                print("사진 불러오는데 성공함")
            default:
                print("사진 불러오는데 실패함")
                return
            }
        })
        
        //푸는 타임 측정
        startTime = DispatchTime.now()
        self.answerView.startTime = startTime
        
        let height = self.ivProblem.contentClippingRect.height
        
        if self.problemIVHeight.constant < height {
            self.problemIVHeight.constant = height
        }
    }
    
    func stringsToIndex(numStrings: String?) {
        var intIdx : [Int] = []
        for nS in numStrings! {
            intIdx += [Int(String(nS))! - 1]
        }
        self.indexOfOneAndOnly = intIdx
    }
    
    
    
    func changeServerAnswer(answer: String?, isMultiple: Bool?, isMultipleAnswers: Bool?) -> String? {
        guard let ans = answer else { return "" }
        guard let isMultiple = isMultiple else { return "" }
        guard let isMultipleAnswers = isMultipleAnswers else { return "" }
        
        var newAnswer = ""
        if isMultiple {
            newAnswer = self.serverToMultipleChoice(answer: ans)
        } else {
            newAnswer = ans
        }
        return newAnswer
    }
    
    func serverToMultipleChoice(answer : String?) -> String {
        var result = ""
        if answer == "잘 모르겠음." || answer == "⑥" {
            result = "잘 모르겠음."
        } else {
            for a in answer! {
                switch a {
                case "①":
                    result += "1"
                case "②":
                    result += "2"
                case "③":
                    result += "3"
                case "④":
                    result += "4"
                default:
                    result += "5"
                }
            }
        }
        return result
    }
    
    
    func previousHelper(number: Int) {
        self.questionId = self.userResponseList![number - 1].qId
        self.uR?.qId = self.questionId
        self.answerView.uR = self.uR
        
        
        if let isAIRecommend = self.isAIRecommend, isAIRecommend == true {
            
            self.lbProbNum.text = "\(self.totalQ!)문제 중 \(self.number!)번 문제"
            
        } else {
            if let isDiag = self.isDiagnosis, isDiag == false {
                self.lbProbNum.text = "\(self.totalQ!)문제 중 \(self.number!)번 문제"
                self.lbCurr.text = "내 커리큘럼 | \(self.trackT!)"
            }
        
        }
        
//        if let isAIRecommend = self.isAIRecommend, isAIRecommend == false
//        {
//            if let isdiagnosis = self.isDiagnosis, isdiagnosis == false {
//                self.lbProbNum.text = "\(self.totalQ!)문제 중 \(self.number!)번 문제"
//                self.lbCurr.text = "내 커리큘럼 | \(self.trackT!)"
//            }
//        } else {
//
//            self.lbProbNum.text = "\(self.totalQ!)문제 중 \(self.number!)번 문제"
//
//        }
        
        self.isMultipleChoice = self.userResponseList![number - 1].isMultipleChoice!
        self.isMultipleAnswers = self.userResponseList![number - 1].isMultipleAnswers!
        
        self.viewprobImg()
        
        if isMultipleChoice { //그 전 문제 객관식 인 경우
            print("그 전 문제 : \(self.userResponseList![number - 1])")
            //잘 모르겠음을 선택한 경우
            if self.userResponseList![number - 1].answer! == "잘 모르겠음." {
                print("객관식 잘 모르겠음. 선택함")
                self.indexOfOneAndOnly = nil
                
                //초기화
                self.answerView.setUp()
                self.answerView.swipeImageView.alpha = 1.0
                self.answerView.backgroundColor = UIColor.Foggy
                self.touchButtonPrevEmpty()
            } else {    //그렇지 않은 경우(1,2,3,4,5중 하나 선택)
                
                //초기화여기추가함02222
                self.answerView.setUp()
                self.answerView.swipeImageView.alpha = 1.0
                self.answerView.backgroundColor = UIColor.Foggy
                
                self.stringsToIndex(numStrings: self.userResponseList![number - 1].answer)
                print("선택했던 객관식 답들: \(self.indexOfOneAndOnly!)")
                self.touchButtonPrev()
            }
            
        } else {   //그 전 문제 주관식 인 경우
            let userAnswer = self.userResponseList![number - 1].answer
//            self.tfAnswer.text = (userAnswer == "잘 모르겠음.") ? "정답을 입력하세요" : self.userResponseList![trackStatus!].answer
            self.tfAnswer.text = (userAnswer == "잘 모르겠음.") ? "" : self.userResponseList![number - 1].answer
            //잘 모르겠음 선택한 경우
            if userAnswer == "" || userAnswer == "잘 모르겠어요" || userAnswer == "잘 모르겠음." {
                button.isHidden = true
                answerView.isHidden = false
                //초기화
                self.answerView.setUp()
                self.answerView.swipeImageView.alpha = 1.0
                self.answerView.backgroundColor = UIColor.Foggy
            } else {    //그렇지 않은 경우
                print("그 전 문제 답2: \(self.tfAnswer.text)")
                button.isHidden = false
                answerView.isHidden = true
            }
            print("그 전 문제 답: \(self.tfAnswer.text)")
        }
    }
    
    
    //이전 문제 푸는 경우
    func previous() {
        guard let num = self.number else { return }
        
        self.uR = self.userResponseList![num - 1]
        
        //분기 처리
        //1. 그 전 문제 api 호출 필요한 경우
        
        
        //무료 기능 추가
        if let isAIDiagnosis = self.isAIDiagnosis, isAIDiagnosis == false
        {
            let promise = Promise<AIRecommendStepRes> { value in
                
                PreparationAPI.shared.weakCategoryStudyStep(problemSeq: self.problemSeq, number: num).done
                { res in
                    if let suc = res.success, suc == true {
                        print("그 전문제 api받아서 성공적으로 뿌림")
                        guard let data = res.data else { return }
                        self.isMultipleChoice = data.problemType == "cho" ? true : false
                        self.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                        
                        
                        var uR = self.userResponseList![num - 1]
                        uR.qId = data.questionId
                        uR.number = data.number
                        uR.isMultipleChoice = self.isMultipleChoice
                        uR.isMultipleAnswers = self.isMultipleAnswers
                        uR.answer = self.changeServerAnswer(answer: data.userAnswer, isMultiple: uR.isMultipleChoice, isMultipleAnswers: uR.isMultipleAnswers)
                        self.userResponseList![num - 1] = uR
                        self.uR = uR
                        print("여기만 고치면됨")
                        print(self.userResponseList)
                    } else {
                        print("그 전문제 api 성공적으로 못받음")
                    }
                }.catch { error in
                    print(error.localizedDescription)
                }.finally {
                    print("까다로운 구간, 여기 들어옴")
                    self.ivProblem.isHidden = false
                    self.headerView.isHidden = false
                    self.lineView.isHidden = false
                    LoadingView.hide()
                    self.previousHelper(number: num)
                }
                
            }
        } else {
        
            //코칭 테스트 인 경우
            
            if self.uR?.answer == nil {
                
                self.ivProblem.isHidden = true
                self.headerView.isHidden = true
                self.lineView.isHidden = true
                LoadingView.showSkeleton()
                
                if let iscoachingtest = self.isCoachingTest, iscoachingtest == true {
                    let promise = Promise<CoachingTestRes> { value in
                        StudyAPI.shared.coachingTestStepPrev(number: num, studyDay: self.studyDate!, userSeq: self.userSeq!).done
                        {
                            res in
                            if let suc = res.success, suc == true {
                                print("그 전문제 api받아서 성공적으로 뿌림")
                                guard let data = res.data else { return }
                                self.isMultipleChoice = data.problemType == "cho" ? true : false
                                self.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                                
                                
                                var uR = self.userResponseList![num - 1]
                                uR.qId = data.questionId
                                uR.addSeq = data.addSeq
                                uR.number = data.number
                                uR.isMultipleChoice = self.isMultipleChoice
                                uR.isMultipleAnswers = self.isMultipleAnswers
                                uR.answer = self.changeServerAnswer(answer: data.userAnswer, isMultiple: uR.isMultipleChoice, isMultipleAnswers: uR.isMultipleAnswers)
                                self.userResponseList![num - 1] = uR
                                self.uR = uR
                                print("코칭, 여기만 고치면됨")
                                print(self.userResponseList)
                            } else {
                                print("코칭, 그 전문제 api 성공적으로 못받음")
                            }
                        }.catch { error in
                            print(error.localizedDescription)
                        }.finally {
                            print("까다로운 구간, 여기 들어옴")
                            self.ivProblem.isHidden = false
                            self.headerView.isHidden = false
                            self.lineView.isHidden = false
                            LoadingView.hide()
                            
                            self.previousHelper(number: num)
                        }
                    }
                } else {
                    let promise = Promise<StudyStepRes> { value in
                        StudyAPI.shared.curriculumStudyStep(cunitSeq: self.cunitSeq!, currSeq: self.currSeq!, number: num, userSeq: self.userSeq!).done
                        { res in
                            if let suc = res.success, suc == true {
                                print("그 전문제 api받아서 성공적으로 뿌림")
                                guard let data = res.data else { return }
                                self.isMultipleChoice = data.problemType == "cho" ? true : false
                                self.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                                
                                
                                var uR = self.userResponseList![num - 1]
                                uR.qId = data.questionId
                                uR.number = data.number
                                uR.isMultipleChoice = self.isMultipleChoice
                                uR.isMultipleAnswers = self.isMultipleAnswers
                                uR.answer = self.changeServerAnswer(answer: data.userAnswer, isMultiple: uR.isMultipleChoice, isMultipleAnswers: uR.isMultipleAnswers)
                                self.userResponseList![num - 1] = uR
                                self.uR = uR
                                print("여기만 고치면됨")
                                print(self.userResponseList)
                            } else {
                                print("그 전문제 api 성공적으로 못받음")
                            }
                        }.catch { error in
                            print(error.localizedDescription)
                        }.finally {
                            print("까다로운 구간, 여기 들어옴")
                            self.ivProblem.isHidden = false
                            self.headerView.isHidden = false
                            self.lineView.isHidden = false
                            LoadingView.hide()
                            self.previousHelper(number: num)
                        }
                    }
                }
            }
            else {
                self.ivProblem.isHidden = true
                self.headerView.isHidden = true
                self.lineView.isHidden = true
                LoadingView.showSkeleton()
                
                self.previousHelper(number: num)
                
                self.ivProblem.isHidden = false
                self.headerView.isHidden = false
                self.lineView.isHidden = false
                LoadingView.hide()
                
                
            }
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    
    
    //초기 number값에 따른 userResponseList 초기화
    func initializeUserResponseList() {
        guard let num = self.number else { return }
        if num == 1 {
            var uR = StudyCommons.userResponse()
            self.userResponseList = [uR]
        } else { // dummy값으로 채우기
            var count = 0
            while count < num  {
                var uR = StudyCommons.userResponse()
                self.userResponseList! += [uR]
                count += 1
            }
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        self.userId = self.myInfo?.userId
        
        self.headerView.isHidden = true
        self.lineView.isHidden = true
        
        svProblemImage.minimumZoomScale = 1.0
        svProblemImage.maximumZoomScale = 2.0
        svProblemImage.delegate = self
        
        self.createtfAnswer()
        tfAnswer.isHidden = true
        
        //userResponse 생성
        self.uR = StudyCommons.userResponse()
        
        //answerswipeview에 전달
        self.answerView.userResponseList = self.userResponseList
//        self.answerView.trackStatus = self.trackStatus
        
        self.svProblemImage.isHidden = true
        self.bottomView.isHidden = true //객관식, 주관식 여부에 따라서 달라짐
        
        
        //버튼 초기화
        indexOfOneAndOnly = nil
//        tfAnswer.text = "정답을 입력하세요"
        for m in multipleOptions {
            m.isSelected = true
            m.backgroundColor = UIColor.white
            m.tintColor = UIColor.puljaBlack
            m.isSelected = false
        }
        //initialize answerView
        self.answerView.setUp()
        self.answerView.swipeImageView.alpha = 1.0
        self.answerView.backgroundColor = UIColor.Foggy
        self.answerView.problemCnt = problemCnt
        self.answerView.videoCnt = videoCnt
        self.answerView.userSeq = userSeq
        self.answerView.currSeq = currSeq
        self.answerView.cunitSeq = cunitSeq
        self.answerView.delegateViewController = self
        
        print("답안제출하기버튼생성")
        //답안 제출하기 버튼
        self.bottomView.addSubview(button)
        button.backgroundColor = UIColor.Purple
        button.cornerRadius = 10
        button.borderWidth = 0.2
        button.titleLabel?.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 16)
        button.setTitle("답안 제출하기", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        
        button.leadingAnchor.constraint(equalTo: answerView.leadingAnchor).isActive = true
        button.topAnchor.constraint(equalTo: answerView.topAnchor).isActive = true
        button.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        button.widthAnchor.constraint(equalTo: answerView.widthAnchor).isActive = true
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)

    //        button.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 12.0).isActive = true
//        button.topAnchor.constraint(equalTo: self.multipleStack.bottomAnchor, constant: 12.0).isActive = true
//        button.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
//        button.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 12.0).isActive = true
        
        
        button.isHidden = true
        
//        self.bottomView.translatesAutoresizingMaskIntoConstraints = false
//        self.bottomConstraint.constant = -200
        
        self.ivProblem.isHidden = true
        LoadingView.showSkeleton()
        
        
        
        //무료기능 추가
        //무료기능 인 경우
        if let isAI = self.isAIRecommend, isAI == true {
            guard let cFromOnboard = self.cameFromOnboard else { return }
                
            self.ibBack.isHidden = true
            self.ibBack.isUserInteractionEnabled = false
//            quitHeight.constant = 80
//            self.btQuit.setTitle("그만하기", for: .normal)
//            self.btQuit.setBackgroundImage("iconSolidXX", for: .normal)
//            self.btQuit.translatesAutoresizingMaskIntoConstraints = false
            
            
            if let isAIDiag = self.isAIDiagnosis, isAIDiag == true {
                
                //ab180 진단 시도 이벤트 보냄
                if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
                    if Const.isAirBridge {
                        CommonUtil.shared.ABEvent(
                            category: "preparation_diagnosis_try", customs: ["userseq": userSeq, "user_id": userId]
                        )
                    } else {
                        print("[ABLog]", "-----------------------------------------", separator: " ")
                        print("[ABLog]", "category: preparation_diagnosis_try", separator: " ")
                        print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                        print("[ABLog]", "user_id: \(userId)", separator: " ")
                        print("[ABLog]", "-----------------------------------------", separator: " ")
                    }
                }
                
                //AI 중단원 진단인 경우
                PreparationAPI.shared.diagnosisStart(problemSeq: self.problemSeq).done {
                    res in
                    if let success = res.success, success == true {
                        guard let data = res.data else { return }
                        guard let isEnd = data.last else { return }
                        
                        self.last = data.last
                        self.number = data.number
                        self.diagnosisLast = isEnd
                        self.lbCurr.text = self.unit2Sname
                        self.lbProbNum.text = "\(data.totalCnt!)문제 중 \(self.number!)번 문제"
                        
                        //초기 userResponseList initialization
                        self.initializeUserResponseList() // 추가(05.25.22)
                        
                        //userResponse 생성
                        self.uR = StudyCommons.userResponse()
                    
                        
                        //객관식, 주관식 여부
                        self.isMultipleChoice = data.problemType == "cho" ? true : false
                        self.uR?.isMultipleChoice = self.isMultipleChoice
                        self.problemUrl = URL(string: data.problemUrl!)
                        self.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                        
                        //questionId찾기
                        self.questionId = data.questionId
                        self.answerView.questionId = self.questionId
                        
                        //userResponse 업데이트
                        self.uR?.qId = self.questionId!
                        self.uR?.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                        self.answerView.uR = self.uR
                        
                        self.userResponseList![self.number! - 1] = self.uR! //중요 (추가 05.25.22)
                        self.viewprobImg()
                        
                    }
                }.catch { error in
                    print(error)
//                    let errorUserInfo : NSDictionary? = ((error as Any) as! NSError).userInfo["error"] as? NSDictionary
//                    let message = errorUserInfo?.allValues[0] as! String
//                    self.alert(title: "AI 서버에서 문제 생김", message: message, button: "노짱한테 신고하기")
                    
                    let vc = R.storyboard.preparation.preparationErrorViewController()!
                    vc.cameFromOnboard = self.cameFromOnboard!
                    self.navigationController?.pushViewController(vc, animated: true)
                }.finally {
                    LoadingView.hide()
                    self.ivProblem.isHidden = false
                    self.headerView.isHidden = false
                    self.lineView.isHidden = false
    //                self.tfAnswer.isHidden = false
                }
            } else {
                self.ibBack.isUserInteractionEnabled = true
                
                //ab180 유형학습 시도 이벤트 보냄
                if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
                    if Const.isAirBridge {
                        CommonUtil.shared.ABEvent(
                            category: "preparation_weakstudy_try", customs: ["userseq": userSeq, "user_id": userId]
                        )
                    } else {
                        print("[ABLog]", "-----------------------------------------", separator: " ")
                        print("[ABLog]", "category: preparation_weakstudy_try", separator: " ")
                        print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                        print("[ABLog]", "user_id: \(userId)", separator: " ")
                        print("[ABLog]", "-----------------------------------------", separator: " ")
                    }
                }
                
                //취약유형학습인 경우
                PreparationAPI.shared.weakCategoryStudyStepInit(problemSeq: self.problemSeq).done {
                    res in
                    if let suc = res.success, suc == true {
                        guard let data = res.data else { return }
                        guard let isEnd = data.last else { return }
                        
                        self.last = data.last
                        self.number = data.number
                        self.weakLast = isEnd
                        self.lbCurr.text = data.title
                        self.lbProbNum.text = "\(data.totalCnt!)문제 중 \(self.number!)번 문제"
                        self.totalQ = data.totalCnt!
                        
                        //초기 userResponseList initialization
                        self.initializeUserResponseList() // 추가(05.25.22)
                        
                        //userResponse 생성
                        self.uR = StudyCommons.userResponse()
                    
                        
                        //객관식, 주관식 여부
                        self.isMultipleChoice = data.problemType == "cho" ? true : false
                        self.uR?.isMultipleChoice = self.isMultipleChoice
                        self.problemUrl = URL(string: data.problemUrl!)
                        self.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                        
                        //questionId찾기
                        self.questionId = data.questionId
                        self.answerView.questionId = self.questionId
                        
                        //userResponse 업데이트
                        self.uR?.qId = self.questionId!
                        self.uR?.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                        self.uR?.number = self.number
                        self.answerView.uR = self.uR
                        
                        self.userResponseList![self.number! - 1] = self.uR! //중요 (추가 05.25.22)
                        self.viewprobImg()
                    }
                }.catch { error in
                }.finally {
                    LoadingView.hide()
                    self.ivProblem.isHidden = false
                    self.headerView.isHidden = false
                    self.lineView.isHidden = false
                }
                
            }
            
            
        } else { //무료 기능 아닌 경우
            
            if let isDiag = self.isDiagnosis, isDiag == true {
                
                
                if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
                    if Const.isAirBridge {
                        CommonUtil.shared.ABEvent(
                            category: "diagnosis_problem_try", customs: ["userseq": userSeq, "user_id": userId]
                        )
                    } else {
                        print("[ABLog]", "-----------------------------------------", separator: " ")
                        print("[ABLog]", "category: diagnosis_problem_try", separator: " ")
                        print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                        print("[ABLog]", "user_id: \(userId)", separator: " ")
                        print("[ABLog]", "-----------------------------------------", separator: " ")
                    }
                }
                
                self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
                
                self.ibBack.isHidden = true
                self.ibBack.isUserInteractionEnabled = false
                
                
                quitHeight.constant = 80
                self.btQuit.setTitle("그만하기", for: .normal)
                self.btQuit.setBackgroundImage(nil, for: .normal)
                self.btQuit.translatesAutoresizingMaskIntoConstraints = false
                
                //처음 문제 진단평가
                StudyAPI.shared.diagnosisStart(diagnosisSeq: self.diagnosisSeq!, nodeSeq:0, number: 0, stepCount: 1, testSeq: 0).done {
                    res in
                    if let success = res.success, success == true {
                        guard let data = res.data else { return }
                        guard let diagnosisinput = data.diagnosisInput else { return }
                        guard let problem = data.problem else { return }
                        guard let isEnd = data.isEnd else { return }
                        
                        self.number = 1
                        self.diagnosisInput = diagnosisinput
                        self.diagnosisLast = (isEnd == "N") ? false : true
                        self.lbProbNum.isHidden = true
                        self.lbCurr.text = "진단평가"
                        
                        //초기 userResponseList initialization
                        self.initializeUserResponseList() // 추가(05.25.22)
                        
                        //userResponse 생성
                        self.uR = StudyCommons.userResponse()
                    
                        
                        //객관식, 주관식 여부
                        self.isMultipleChoice = problem.problemType == "cho" ? true : false
                        self.uR?.isMultipleChoice = self.isMultipleChoice
                        self.problemUrl = URL(string: problem.problemUrl!)
                        self.isMultipleAnswers = (problem.answerCnt! > 1) ? true : false
                        
                        //questionId찾기
                        self.questionId = problem.questionId
                        self.answerView.questionId = self.questionId
                        
                        //userResponse 업데이트
                        self.uR?.qId = self.questionId!
                        self.uR?.isMultipleAnswers = (problem.answerCnt! > 1) ? true : false
                        self.answerView.uR = self.uR
                        
                        self.userResponseList![self.number! - 1] = self.uR! //중요 (추가 05.25.22)
                        self.viewprobImg()
                        
                    }
                }.catch { error in
                }.finally {
                    LoadingView.hide()
                    self.ivProblem.isHidden = false
                    self.headerView.isHidden = false
                    self.lineView.isHidden = false
    //                self.tfAnswer.isHidden = false
                }
        
            } else {
                
    //            self.btQuit.frame.size = CGSize(width: 28.0, height: 28.0)
                self.quitHeight.constant = 20
                self.btQuit.setTitle("", for: .normal)
    //            self.btQuit.contentMode = .scaleToFill
    //            self.btQuit.setBackgroundImage(R.image.iconSolidXX(), for: .normal)
                self.btQuit.tintColor = R.color.foggy()
                self.btQuit.translatesAutoresizingMaskIntoConstraints = false
                
                //코칭 테스트 인 경우
                if let iscoachingtest = self.isCoachingTest, iscoachingtest == true {
                    StudyAPI.shared.coachingTestStep(studyDay: self.studyDate!, userSeq: self.userSeq!).done { res in
                        if let suc = res.success, suc == true {
                            guard let data = res.data else { return }
                            
                            self.number = data.number
                            self.addSeq = data.addSeq
                            self.totalQ = data.totalCnt!
                            self.trackT = data.title!
                            
                            self.lbProbNum.text = "\(self.totalQ!)문제 중 \(self.number!)번 문제"
                            self.lbCurr.text = "코칭 테스트 | \(self.trackT!)"
                            self.last = data.last
                            
                            //초기 userResponseList initialization
                            self.initializeUserResponseList()
                            
                            //userResponse 생성
                            self.uR = StudyCommons.userResponse()
                            
                            //객관식, 주관식 여부
                            self.isMultipleChoice = data.problemType == "cho" ? true : false
                            self.uR?.isMultipleChoice = self.isMultipleChoice
                            self.problemUrl = URL(string: data.problemUrl!)
                            self.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                            
                            //questionId찾기
                            self.questionId = data.questionId
                            self.answerView.questionId = self.questionId
                            
                            //userResponse 업데이트
                            self.uR?.number = self.number
                            self.uR?.addSeq = self.addSeq
                            self.uR?.qId = self.questionId!
                            self.uR?.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                            self.answerView.uR = self.uR
                            
                            self.userResponseList![self.number! - 1] = self.uR! //중요
                            self.viewprobImg()
                            
                        }
                    }.catch { error in
                        
                    }.finally {
                        LoadingView.hide()
                        self.ivProblem.isHidden = false
                        self.headerView.isHidden = false
                        self.lineView.isHidden = false
                    }
                }
                else {
                    //처음 문제 학습 over 강의 학습
                    StudyAPI.shared.curriculumStudyStepInitial(cunitSeq: cunitSeq!, currSeq: currSeq!, userSeq: userSeq!).done {
                        res in
                        if let success = res.success, success == true {
                            guard let data = res.data else { return }
                            
                            self.number = data.number!
                            self.totalQ = data.totalCnt!
                            self.trackT = data.title!
                            
                            self.lbProbNum.text = "\(self.totalQ!)문제 중 \(self.number!)번 문제"
                            self.lbCurr.text = "내 커리큘럼 | \(self.trackT!)"
                            self.last = data.last
                            
                            //초기 userResponseList initialization
                            self.initializeUserResponseList()
                            
                            //userResponse 생성
                            self.uR = StudyCommons.userResponse()
                            
                            //객관식, 주관식 여부
                            self.isMultipleChoice = data.problemType == "cho" ? true : false
                            self.uR?.isMultipleChoice = self.isMultipleChoice
                            self.problemUrl = URL(string: data.problemUrl!)
                            self.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                            
                            //questionId찾기
                            self.questionId = data.questionId
                            self.answerView.questionId = self.questionId
                            
                            //userResponse 업데이트
                            self.uR?.number = self.number
                            self.uR?.qId = self.questionId!
                            self.uR?.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                            self.answerView.uR = self.uR
                            
                            self.userResponseList![self.number! - 1] = self.uR! //중요
                            self.viewprobImg()
                            
                        }
                    }.catch { error in
                        
                    }.finally {
                        LoadingView.hide()
                        self.ivProblem.isHidden = false
                        self.headerView.isHidden = false
                        self.lineView.isHidden = false
        //                self.tfAnswer.isHidden = false
                    }
                }
            }
        }
        
        
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonUtil.tabbarController?.tabBar.isHidden = true
        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        
        initPencil()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(animated)
        
        initPencil()
        

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        CommonUtil.tabbarController?.tabBar.isHidden = false
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            
            self.bottomConstraint.constant = keyboardRectangle.height - 26
            
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }

         }
     }
    
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.bottomConstraint.constant = 0
    }
    
    
    func setupAnimationCustomView(constant: Int){

//        UIView.animate(withDuration: 0.3, delay: 0 , options: .curveEaseOut, animations: {
//            print(self.button.frame.origin.y)
//            var translationY : CGFloat = 0
//            if let tabBarController = self.tabBarController {
//                translationY = self.bottomView.frame.height + tabBarController.tabBar.frame.size.height - 12.0
//            } else {
//                translationY = self.bottomView.frame.height - 12.0
//            }
//            self.bottomView.transform = CGAffineTransform(translationX: 0, y:  translationY * CGFloat(constant))
//        }, completion: {_ in
//
//        })
    }
    
    
    func indexToStrings(indexAnswers: [Int]?) -> String {
        var answer = ""
        if isMultipleChoice {
            if isMultipleAnswers {
                for i in self.indexOfOneAndOnly! {
                    answer += String(i + 1)
                }
            } else {
                answer += String(self.indexOfOneAndOnly![0] + 1)
            }
        } else {
            answer = tfAnswer.text!
            
            /////////// 여기 추가함!!!!
            if answer == "" {
                answer = "잘 모르겠음."
            }
        }
        return answer
    }
    
    
    func showAIDiagResult() {
        
        //ab180 진단 완료 이벤트 보냄
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: "preparation_diagnosis_complete",
                    customs: ["userseq": userSeq, "user_id": userId]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "category: preparation_diagnosis_complete", separator: " ")
                print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                print("[ABLog]", "user_id: \(userId)", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
        }
    }
    func showAIWeakResult()
    {
        //ab180 유형 학습 완료 이벤트 보냄
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: "preparation_weakstudy_complete", customs: ["userseq": userSeq, "user_id": userId]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "category: preparation_weakstudy_complete", separator: " ")
                print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                print("[ABLog]", "user_id: \(userId)", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
        }
    }
    
    
    //객관식 submit (buttonAction은 viewDidLoad하면 실행이 같이 되버림..)
    @objc func buttonAction(sender: UIButton!) {
        print("버튼진짜선택하")
        //키보드 내림
        self.tfAnswer.resignFirstResponder()
        
        
        endTime = DispatchTime.now()
        let nanoDuration = endTime!.uptimeNanoseconds - startTime!.uptimeNanoseconds
        let temp = Double(nanoDuration) / 1_000_000_000
        print("문제 푸는데 걸린 시간은 \(temp) 초 입니다")
        let duration = (Double(round(10 * Double(nanoDuration) / 1_000_000_000)/10))
        print("문제 푸는데 걸린 시간은 \(duration) 초 입니다")
        
        var answer = self.indexToStrings(indexAnswers: self.indexOfOneAndOnly)
        if answer == "정답을 입력하세요" {
            answer = "잘 모르겠음."
        }
        self.ansToServer = self.sendAnswerToServer(answer : answer)
        self.tfAnswer.text = ""
        
        self.setupAnimationCustomView(constant: 1)
        
        self.ivProblem.isHidden = true
        self.headerView.isHidden = true
        self.lineView.isHidden = true
        LoadingView.showSkeleton()
        
        
        
        //무료기능 인 경우
        if let isAI = self.isAIRecommend, isAI == true {
            
            //중단원 진단 인 경우
            if let isAIDiag = self.isAIDiagnosis, isAIDiag == true {
                PreparationAPI.shared.diagnosisStudyAnswer(problemSeq: self.problemSeq , duration: duration,
                                                           questionId: self.questionId!, userAnswer: self.ansToServer!, userSeq: self.userSeq!).done
                {
                    res in
                    if let suc = res.success, suc == true {
                        
                        //정답 정보 저장
                        self.uR?.answer = answer
                        self.userResponseList![self.number! - 1] = self.uR!
                        
                        guard let data = res.data else { return }
                        guard let last = data.last else { return }
                        
                        if self.last! { //중단원 진단 마지막 문제 품
                            LoadingView.hide()
                            self.showAIDiagResult()
                            
                        } else {
                            
                            self.last = last
                            self.uR = StudyCommons.userResponse()
                            self.number! += 1
                            self.lbProbNum.text = "\(data.totalCnt!)문제 중 \(self.number!)번 문제"
                            self.isMultipleChoice = data.problemType == "cho" ? true : false
                            self.uR?.isMultipleChoice = self.isMultipleChoice
                            self.problemUrl = URL(string: data.problemUrl!)
                            self.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                            
                            //questionId찾기
                            self.questionId = data.questionId
                            self.answerView.questionId = self.questionId
                            
                            //userResponse 업데이트
                            self.uR?.qId = self.questionId!
                            self.uR?.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                            self.answerView.uR = self.uR
                            self.viewprobImg()
                            
                            if self.number! > self.userResponseList!.count {    //새로운 문제 푼 경우
                                self.userResponseList! += [self.uR!]
                            }
                            
                            self.initialize()
                            
                        }
                    }
                }.catch { //AI 서버에서 문제 생김
                    err in
                    
                    LoadingView.hide()
                    let vc = R.storyboard.preparation.preparationErrorViewController()!
                    vc.cameFromOnboard = self.cameFromOnboard!
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                    
//
                }.finally {
                    LoadingView.hide()
                    self.ivProblem.isHidden = false
                    self.headerView.isHidden = false
                    self.lineView.isHidden = false
                }
            } else { //유형 학습인 경우
               
                PreparationAPI.shared.weakCategoryStudyAnswer(problemSeq: self.problemSeq , duration: duration,
                                                              questionId: self.questionId!, userAnswer: self.ansToServer!).done
                { res in
                    if let suc = res.success, suc == true {
                        //정답 정보 저장
                        self.uR?.answer = answer
                        self.userResponseList![self.number! - 1] = self.uR!
                        
                        guard let data = res.data else { return }
                        guard let last = data.last else { return }
                        
                        if self.last! { //중단원 진단 마지막 문제 품
                            LoadingView.hide()
                            print("취약 유형 학습 마지막 문제 품")
                            self.showAIWeakResult()
                            
                        } else {
                            //취약 유형 추가 부분
                            self.totalQ = data.totalCnt!
                            self.trackT = data.title!
                            self.number = data.number
                            
                            
                            self.last = last
                            self.uR = StudyCommons.userResponse()
                            self.lbProbNum.text = "\(data.totalCnt!)문제 중 \(self.number!)번 문제"
                            self.isMultipleChoice = data.problemType == "cho" ? true : false
                            self.problemUrl = URL(string: data.problemUrl!)
                            self.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                            
                            //questionId찾기
                            self.questionId = data.questionId
                            self.answerView.questionId = self.questionId
                            
                            //userResponse 업데이트
                            self.uR?.isMultipleChoice = self.isMultipleChoice
                            self.uR?.number = self.number
                            self.uR?.qId = self.questionId!
                            self.uR?.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                            self.answerView.uR = self.uR
                            self.viewprobImg()
                            
                            if self.number! > self.userResponseList!.count {    //새로운 문제 푼 경우
                                self.userResponseList! += [self.uR!]
                            }
                            
                            self.initialize()
                            
                        }
                    }
                }.catch {
                    err in
                    LoadingView.hide()
                    let vc = R.storyboard.preparation.preparationErrorViewController()!
                    vc.cameFromOnboard = self.cameFromOnboard!
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }.finally {
                    LoadingView.hide()
                    self.ivProblem.isHidden = false
                    self.headerView.isHidden = false
                    self.lineView.isHidden = false
                }
            }
            
        } else {
        
            if let isDiag = self.isDiagnosis, isDiag == true {
                guard let diagnosisInput = self.diagnosisInput else { return }
                //진단평가 그 다음 문제
                StudyAPI.shared.diagnosisProgress(diagnosisSeq: self.diagnosisSeq!, nodeSeq: diagnosisInput.nodeSeq!, number: diagnosisInput.number!,
                                                  stepCount: diagnosisInput.stepCount!, testSeq: diagnosisInput.testSeq!, answer: self.ansToServer!, duration: duration, questionId: self.questionId!).done
                { res in
                    if let suc = res.success, suc == true {
                        
    //                    if let number = self.diagnosisInput?.number, number == 1 {
    //                        print("stepCount:\(number)")
    //                        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
    //                            CommonUtil.shared.ABEvent(category: "diagnosis_problem_try", customs: ["userseq": userSeq, "user_id": userId])
    //                        }
    //                    }
                        
    //                    self.trackStatus! += 1
                        self.uR?.answer = answer
    //                    self.userResponseList! += [self.uR!] //새로 고침(05.25.22)
                        self.userResponseList![self.number! - 1] = self.uR! //새로 고침(05.25.22)
                        
                        guard let data = res.data else { return }
                        guard let isEnd = data.isEnd else { return }
                        
                        self.diagnosisLast = (isEnd == "N") ? false : true
                        if self.diagnosisLast! {
                            //ab180 이벤트 보내기
                            if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
                                if Const.isAirBridge {
                                    CommonUtil.shared.ABEvent(
                                        category: "diagnosis_complete",
                                        customs: ["userseq": userSeq, "user_id": userId]
                                    )
                                } else {
                                    print("[ABLog]", "-----------------------------------------", separator: " ")
                                    print("[ABLog]", "category: diagnosis_complete", separator: " ")
                                    print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                                    print("[ABLog]", "user_id: \(userId)", separator: " ")
                                    print("[ABLog]", "-----------------------------------------", separator: " ")
                                }
                            }
                            
                            LoadingView.hide()
                            print("마지막 문제 response 성공")
                            self.showDiagnosisResult()
                        } else {
                            guard let diagnosisinput = data.diagnosisInput else { return }
                            guard let problem = data.problem else { return }
                            self.diagnosisInput = diagnosisinput
                            //userResponse 생성
                            self.uR = StudyCommons.userResponse()
                            
                            //객관식, 주관식 여부
                            self.number! += 1
                            self.isMultipleChoice = problem.problemType == "cho" ? true : false
                            self.uR?.isMultipleChoice = self.isMultipleChoice
                            self.problemUrl = URL(string: problem.problemUrl!)
                            self.isMultipleAnswers = (problem.answerCnt! > 1) ? true : false
                            
                            //questionId찾기
                            self.questionId = problem.questionId
                            self.answerView.questionId = self.questionId
                            
                            //userResponse 업데이트
                            self.uR?.qId = self.questionId!
                            self.uR?.isMultipleAnswers = (problem.answerCnt! > 1) ? true : false
                            self.answerView.uR = self.uR
                            self.viewprobImg()
                            
                            if self.number! > self.userResponseList!.count {    //새로운 문제 푼 경우
                                self.userResponseList! += [self.uR!]
                            }
                            
                            self.initialize()
                        }
                        
                    }
                }.catch { error in
                    
                }.finally {
                    LoadingView.hide()
                    self.ivProblem.isHidden = false
                    self.headerView.isHidden = false
                    self.lineView.isHidden = false
                }
            } else {
                if let iscoaching = self.isCoachingTest, iscoaching == true {
                    //코칭 테스트 푸는 경우
                    let addseq = self.userResponseList![self.number! - 1].addSeq ?? 0
                    StudyAPI.shared.coachingTestAnswer(addSeq: addseq, duration: duration, number: self.number!, questionId: self.questionId!, studyDay: self.studyDate!, userAnswer: self.ansToServer!, userSeq: userSeq!).done
                    { res in
                        if let success = res.success, success == true {
                            print("코칭테스트 결과 반환 성공")
                            guard let num = self.number else { return }
                            self.uR?.answer = answer
                            self.userResponseList![self.number! - 1] = self.uR!

                            print("문제 풀고 나서 userResponseList")
                            print(self.userResponseList!)
                            
                            guard let data = res.data else { return }
                            
                            if self.last! {
    //                            self.navigationController?.popViewController(animated: false)
                                self.showCoachSolveResult()
                //              self.showSolveResult(answer: answer, duration: duration)
                            } else {
                              // 수정 본
                                self.totalQ = data.totalCnt!
                                self.trackT = data.title!
    //                            self.trackStatus! += 1
                                //          self.problemCnt! -= 1
    //                            self.trackQ! += 1
                                self.last = data.last
                                self.addSeq = data.addSeq
                                self.number = data.number
                                self.lbProbNum.text = "\(self.totalQ!)문제 중 \(self.number!)번 문제"
                                self.lbCurr.text = "코칭 테스트 | \(self.trackT!)"
                                
                                
                                //userResponse 생성
                                self.uR = StudyCommons.userResponse()
                                //객관식, 주관식 여부
                                self.isMultipleChoice = data.problemType == "cho" ? true : false
                                self.uR?.isMultipleChoice = self.isMultipleChoice
                                self.problemUrl = URL(string: data.problemUrl!)
                                self.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                                self.uR?.isMultipleAnswers = self.isMultipleAnswers
                                //questionId찾기
                                self.questionId = data.questionId
                                self.answerView.questionId = self.questionId
                                
                                //userResponse 업데이트
                                self.uR?.number = self.number
                                self.uR?.addSeq = self.addSeq
                                self.uR?.qId = self.questionId!
                                self.answerView.uR = self.uR
                                self.viewprobImg()
                                
                                //초기화
                                print("number: \(self.number!)")
                                if self.number! > self.userResponseList!.count {    //새로운 문제 푼 경우
                                    self.userResponseList! += [self.uR!]
                                }
                                
                                self.initialize()
                            }
                        }
                        
                    }.catch { error in
                        
                    }.finally {
                        LoadingView.hide()
    //                    if self.last! == false {
                        if self.number ?? 0 <= self.totalQ ?? 0 {
                            self.ivProblem.isHidden = false
                            self.headerView.isHidden = false
                            self.lineView.isHidden = false
                        }
                    }
                }
                else {
                    //커리큘럼 학습하는 경우
                    StudyAPI.shared.curriculumStudyAnswer(cunitSeq: cunitSeq!, currSeq: currSeq!, duration: duration,
                                                          questionId: questionId!, userAnswer: self.ansToServer!, userSeq: userSeq!).done
                    { res in
                        if let success = res.success, success == true {
                            
                            self.uR?.answer = answer
                        
                            
                            self.userResponseList![self.number! - 1] = self.uR!
                            
    //                        //기존 답 바꾼 경우
    //                        if self.number! <= self.userResponseList!.count {
    //                            print("답 바꾸었음")
    //                            print(self.uR!)
    //                            self.userResponseList![self.number! - 1] = self.uR!
    //
    //                        }
                            
                            
                            print("문제 풀고 나서 userResponseList")
                            print(self.userResponseList!)
                            
                            guard let data = res.data else { return }
                            
                            if self.last! {
                                self.showSolveResult(answer: answer, duration: duration)
                            } else {
                                // 수정 본
                                self.totalQ = data.totalCnt!
                                self.trackT = data.title!
                                self.number = data.number
    //                            self.trackStatus! += 1
            //                    self.problemCnt! -= 1
    //                            self.trackQ! += 1
                                self.last = data.last
                                
                                self.lbProbNum.text = "\(self.totalQ!)문제 중 \(self.number!)번 문제"
                                self.lbCurr.text = "내 커리큘럼 | \(self.trackT!)"
                                
                                //userResponse 생성
                                self.uR = StudyCommons.userResponse()
                                
                                
                                //객관식, 주관식 여부
                                self.isMultipleChoice = data.problemType == "cho" ? true : false
                                self.uR?.isMultipleChoice = self.isMultipleChoice
                                self.problemUrl = URL(string: data.problemUrl!)
                                self.isMultipleAnswers = (data.answerCnt! > 1) ? true : false
                                self.uR?.isMultipleAnswers = self.isMultipleAnswers
                                
                                //questionId찾기
                                self.questionId = data.questionId
                                self.answerView.questionId = self.questionId
                                
                                //userResponse 업데이트
                                self.uR?.number = self.number
                                self.uR?.qId = self.questionId!
                                self.answerView.uR = self.uR
                                self.viewprobImg()
                                
                                //초기화
                                print("number: \(self.number!)")
                                if self.number! > self.userResponseList!.count {    //새로운 문제 푼 경우
                                    self.userResponseList! += [self.uR!]
                                }
                                
                                
                                self.initialize()
                            }
                        }
                    }.catch { error in
                            
                    }.finally {
                        LoadingView.hide()
    //                    if self.last! == false {
                        if (self.number ?? 0) - 1 <= (self.problemCnt ?? 0) - 1 {
                            self.ivProblem.isHidden = false
                            self.headerView.isHidden = false
                            self.lineView.isHidden = false
                        }
                    }
                }
                
            }
        }
            
        

    }
    func showDiagnosisResult() {
        let vc = R.storyboard.study.diagnosisReportViewController()!
        vc.fromSeven = self.fromSeven
        vc.fromStudy = self.fromStudy
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //코칭 마지막 문제 분기 처리
    func showCoachSolveResult() {
        
        self.headerView.isHidden = true
        self.lineView.isHidden = true
        self.svProblemImage.isHidden = true
        self.bottomView.isHidden = true
        
        //키보드 내림
        self.tfAnswer.resignFirstResponder()
        
//        LoadingView.show()
        StudyAPI.shared.coachingTestResult(studyDay: self.studyDate!, userSeq: self.userSeq!).done {
            res in
            if let suc = res.success, suc == true {
                print("풀이 결과 리스펀스 성공함")
                let solveResult = res.data!
                let thisWeekStudy = solveResult.thisWeekStudy!
                let lastWeekStudy = solveResult.lastWeekStudy!
                let nowStudy = solveResult.nowStudy!
                
                let vc = R.storyboard.study.studySolveResultViewController()!
                vc.thisWeekStudy = thisWeekStudy
                vc.lastWeekStudy = lastWeekStudy
                vc.nowStudy = nowStudy
                vc.studyDay = self.studyDate
                
                self.view.isHidden = true
                self.headerView.isHidden = true
//                if self.isLecture {
//                    self.videodelegateViewController?.view.isHidden = true
//                }
                
                self.navigationController?.popViewController(animated: false, completion: {
                    
//                    if self.isLecture {
//                        self.videodelegateViewController?.navigationController?.popViewController(animated: false, completion : {
//                            self.navigationController?.pushViewController(vc, animated: false)
//                        })
//                    }
                    self.celldelegateViewController?.navigationController?.pushViewController(vc, animated: true)
                })
            }
        }.catch {
            error in
            print("풀이 결과 api 이상 있음")
        }.finally{
//            LoadingView.hide()
        }
        
    }
    
    //마지막 문제 분기 처리
    func showSolveResult(answer : String, duration: Double) {
//        self.ivProblem.isHidden = true
//        LoadingView.showSkeleton()
//        LoadingView.hide()
        self.headerView.isHidden = true
        self.lineView.isHidden = true
        self.svProblemImage.isHidden = true
        self.bottomView.isHidden = true
        
        //키보드 내림
        self.tfAnswer.resignFirstResponder()
        
//        LoadingView.show()
        StudyAPI.shared.curriculumStudyResult(cunitSeq: self.cunitSeq!, currSeq: self.currSeq!, studyDate: self.studyDate!, userSeq: self.userSeq!).done {
            res in
            if let suc = res.success, suc == true {
                print("풀이 결과 리스펀스 성공함")
                let solveResult = res.data!
                let thisWeekStudy = solveResult.thisWeekStudy!
                let lastWeekStudy = solveResult.lastWeekStudy!
                let nowStudy = solveResult.nowStudy!
                
                let vc = R.storyboard.study.studySolveResultViewController()!
                vc.thisWeekStudy = thisWeekStudy
                vc.lastWeekStudy = lastWeekStudy
                vc.nowStudy = nowStudy
                vc.currSeq = self.currSeq
                vc.cunitSeq = self.cunitSeq
                
                self.view.isHidden = true
                self.headerView.isHidden = true
                if self.isLecture {
                    self.videodelegateViewController?.view.isHidden = true
                }
                
                self.navigationController?.popViewController(animated: false, completion: {
                    
                    if self.isLecture {
                        self.videodelegateViewController?.navigationController?.popViewController(animated: false, completion : {
                            self.navigationController?.pushViewController(vc, animated: false)
                        })
                    }
                    self.celldelegateViewController?.navigationController?.pushViewController(vc, animated: false)
                })
            }
        }.catch {
            error in
            print("풀이 결과 api 이상 있음")
        }
        
    }
    
    
    
    func touchButtonPrevEmpty() {
        let choices = [0, 1, 2, 3, 4]
        for c in choices {
            multipleOptions[c].isSelected = true
            multipleOptions[c].tintColor = UIColor.puljaBlack
            multipleOptions[c].backgroundColor = UIColor.white
            multipleOptions[c].isSelected = false
        }
        
        self.button.isHidden = true
        self.answerView.isHidden = false
    }
  
    func touchButtonPrev() {
        print("touchButtonPrev 들어옴")
        self.button.isHidden = false
        let choices = [0, 1, 2, 3, 4]
        for c in choices {
            if !self.indexOfOneAndOnly!.contains(c) {
                //색칠 안한 상태로 바꿔놓음
                multipleOptions[c].isSelected = true
                multipleOptions[c].tintColor = UIColor.puljaBlack
                multipleOptions[c].backgroundColor = UIColor.white
                multipleOptions[c].isSelected = false
            } else {
                multipleOptions[c].isSelected = false
                multipleOptions[c].backgroundColor = UIColor.Purple
                multipleOptions[c].tintColor = UIColor.white
            }
        }
        
    }
    
    
    
    
    func multipleChoiceToServer(answer : String?) -> String {
        var result = ""
        if answer == "잘 모르겠음." {
            result = "⑥"
        } else {
            for a in answer! {
                switch a {
                case "1":
                    result += "①"
                case "2":
                    result += "②"
                case "3":
                    result += "③"
                case "4":
                    result += "④"
                default:
                    result += "⑤"
                }
            }
        }
        return result
    }
    
    func sendAnswerToServer(answer : String?) -> String {
        let newAnswer = (self.isMultipleChoice) ? self.multipleChoiceToServer(answer: answer) : answer
        print("########## 답안 선택 후 서버에 보낸 정답정보: \(newAnswer) ##################")
        return newAnswer!
    }
    
    
    func isButtonSelected(_ sender: UIButton) -> Bool {
        return (sender.isSelected == false && sender.backgroundColor! == UIColor.Purple && sender.tintColor! == UIColor.white)
    }
    
    @IBAction func touchButton(_ sender: UIButton) {
        
        if !isMultipleAnswers {  //한개만 선택하는 경우
            if indexOfOneAndOnly != nil {
                if multipleOptions.firstIndex(of: sender) != indexOfOneAndOnly![0] {
                    print("여기 클릭하하: \(multipleOptions.firstIndex(of: sender)!)")
                    //이전 클릭했던 버튼 초기화
                    multipleOptions[indexOfOneAndOnly![0]].isSelected = true
                    multipleOptions[indexOfOneAndOnly![0]].tintColor = UIColor.puljaBlack
                    multipleOptions[indexOfOneAndOnly![0]].backgroundColor = UIColor.white
                    multipleOptions[indexOfOneAndOnly![0]].isSelected = false
                    
                    //새로 클릭한 버튼 색칠
                    sender.isSelected = false
                    sender.backgroundColor = UIColor.Purple
                    sender.tintColor = UIColor.white
                    indexOfOneAndOnly = [multipleOptions.firstIndex(of: sender)!]
                } else { //한번 더 클릭 시
                    print("한번 더 클릭함")
                    print("answerview여부: \(answerView.isHidden)")
                    
                    //한번 더 클릭 시
                    sender.isSelected = true
                    sender.backgroundColor = UIColor.white
                    sender.tintColor = UIColor.puljaBlack
                    button.isHidden = true
                    sender.isSelected = false
                    indexOfOneAndOnly = nil
                }
                
            }   else {
                print("여기 클릭하: \(multipleOptions.firstIndex(of: sender)!)")
                
                sender.isSelected = false
                sender.backgroundColor = UIColor.Purple
                sender.tintColor = UIColor.white
                
                button.isHidden = false
                
                button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
                indexOfOneAndOnly = [multipleOptions.firstIndex(of: sender)!]
            }
        } else {    //여러개 객관식 선택하는 경우
            print("여러개 객관식 선택하는 화면 들어옴")
            guard let idx = multipleOptions.firstIndex(of: sender) else { return }
            if indexOfOneAndOnly == nil {
                indexOfOneAndOnly = []
            }
            //색칠해져 있는 경우
            if self.isButtonSelected(sender){
                print("여러개, \(idx) 색칠해져 있음")
                //색칠 안함
                multipleOptions[idx].isSelected = true
                multipleOptions[idx].tintColor = UIColor.puljaBlack
                multipleOptions[idx].backgroundColor = UIColor.white
                multipleOptions[idx].isSelected = false
                
                guard let idx2 = indexOfOneAndOnly!.firstIndex(of: idx) else { return }
                indexOfOneAndOnly!.remove(at: idx2)
                
            } else {
                print("여러개, \(idx) 색칠 안해져 있음")
                sender.isSelected = false
                sender.backgroundColor = UIColor.Purple
                sender.tintColor = UIColor.white
                indexOfOneAndOnly! += [idx]
            }
            
            if indexOfOneAndOnly!.count != 0 {
                button.isHidden = false
                button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            } else {
                button.isHidden = true
    
            }
          
        }
        
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.answerView.isHidden = false
//        if (tfAnswer.text == "" || tfAnswer.text == "정답을 입력하세요") {
////            tfAnswer.clearsOnBeginEditing = true
//            tfAnswer.text = "정답을 입력하세요"
//            button.isHidden = true
//            answerView.isHidden = false
//        }
        if (tfAnswer.text == "") {
//            tfAnswer.clearsOnBeginEditing = true
            button.isHidden = true
            answerView.isHidden = false
        }
        else {
//            tfAnswer.clearsOnBeginEditing = false
            button.isHidden = false
            answerView.isHidden = true
            
            
            //수정한 곳
//            button.removeTarget(self, action: #selector(buttonAction), for: .touchUpInside)
//            button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            
//            self.view.addSubview(button)
        }
        self.answerView.answer = tfAnswer.text
    }
    
//    @objc func keyboardWillShow(notification: NSNotification) {
//
//        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
//
//            // if keyboard size is not available for some reason, dont do anything
//           return
//        }
//        self.keyboardSize = keyboardSize
//        //키보드 위로 제출하기 보여줌
////        self.bottomView.bottomAnchor.constraint(equalTo: view.superview!.bottomAnchor, constant: -keyboardSize.height).isActive = true
//    }
    

    @IBAction func lbquit(_ sender: Any) {
        
        if self.cameFromOnboard != nil { //무료기능 추가
            if let isAI = self.isAIDiagnosis, isAI == true {
                self.alert(title: "어디가세요 😭", message: "지금 그만두면 이전 문제 풀이 기록이 모두 사라져요. 그래도 그만 푸시겠어요?",
                           button: "계속 풀기", subButton: "그만 풀기").done {
                    res in
                    if !res {
                        print("그만풀기")
                        self.view.isHidden = true
                        
                        if self.cameFromOnboard!
                        {
                            if let vcDetail = R.storyboard.main.tabbarController() {
                                self.navigationController?.setViewControllers([vcDetail], animated: true)
//                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//                                    guard let userType = self.myInfo?.userType else { return }
//                                    if userType != "p" {
//                                        vcDetail.selectedIndex = 2
//                                        vcDetail.bottomNavBar.selectedItem = vcDetail.bottomNavBar.items[2]
//                                    }
//                                }
//                                self.navigationController?.setViewControllers([vcDetail], animated: true)
                            }
                        }
                        else
                        {
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                        
                    } else {
                        print("계속 풀기")
                    }
                }
            } else {
                self.alert(title: "정말로 나가시겠어요? 😭", message: "지금까지 제출한 답은 저장되고 다음에 이어서 풀 수 있어요.",
                           button: "계속 풀기", subButton: "나가기").done {
                    res in
                    if !res {
                        print("그만풀기")
                        self.view.isHidden = true
                        
                        if self.cameFromOnboard!
                        {
                            if let vcDetail = R.storyboard.main.tabbarController() {
                                self.navigationController?.setViewControllers([vcDetail], animated: true)
//                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//                                    guard let userType = self.myInfo?.userType else { return }
//                                    if userType != "p" {
//                                        vcDetail.selectedIndex = 2
//                                        vcDetail.bottomNavBar.selectedItem = vcDetail.bottomNavBar.items[2]
//                                    }
//                                }
//                                self.navigationController?.setViewControllers([vcDetail], animated: true)
                            }
                        }
                        else
                        {
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                        
                    } else {
                        print("계속 풀기")
                    }
                }
            }
            
        } else {
            if let suc = self.isDiagnosis, suc == true {
                self.alert(title: "어디가세요 😭", message: "지금 그만두면 이전 문제 풀이 기록이 모두 사라져요. 그래도 그만 푸시겠어요?",
                           button: "계속 풀기", subButton: "그만 풀기").done {
                    res in
                    if !res {
                        print("그만풀기")
                        self.view.isHidden = true
                        
                        if self.fromSeven
                        {
                            if self.fromStudy
                            {
                                self.navigationController?.popToRootViewController(animated: true)
                            }
                            else
                            {
                                if let vcDetail = R.storyboard.main.tabbarController(){
                                    self.navigationController?.setViewControllers([vcDetail], animated: false)
                                }
                            }
                        }
                        else
                        {
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                        
                    } else {
                        print("계속 풀기")
                    }
                }
                
            } else {
                self.alert(title: "정말로 나가시겠어요? 😭", message: "지금까지 제출한 답은 저장되고 다음에 이어서 풀 수 있어요.",
                           button: "계속 풀기", subButton: "나가기").done {
                    res in
                    if !res {
                        print("나가기")
    //                    self.view.isHidden = true
                        if self.isLecture {
                            self.videodelegateViewController?.view.isHidden = true
                            self.navigationController?.popViewController(animated: false, completion: {
                                self.videodelegateViewController?.navigationController?.popViewController(animated: false)
                            })
                        }
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        print("계속 풀기")
                    }
                }
            }
        }
    }
    
    func firstProblemBack(duration: Double) {
        guard let num = self.number else { return }
        var oldAnswer = self.userResponseList![num - 1].answer
        var newAnswer : String?
        
        if (indexOfOneAndOnly == nil && self.isMultipleChoice == true) {    //객관식 잘 모르겠음
            if (oldAnswer == nil || oldAnswer == "" ) {
                oldAnswer = "잘 모르겠음."
            }
            newAnswer = "잘 모르겠음."
        } else {
            newAnswer = indexToStrings(indexAnswers: self.indexOfOneAndOnly)
        }
        if newAnswer == "정답을 입력하세요" || newAnswer == "" {  //주관식 잘 모르겠음(후자는 아무것도 선택안하고 백버튼 누른 경우)
            newAnswer = "잘 모르겠음."
        }
        if (oldAnswer == nil && self.isMultipleChoice == false) {
            oldAnswer = "잘 모르겠음."
        }
        
        
        //문제처리
        if (oldAnswer != newAnswer) {
            self.alert(title: "답안을 변경하셨네요!", message: "기존 선택한 답안을 삭제하고\n변경한 답안으로 저장할까요?",
                       button: "변경한 답안으로 저장하기", subButton: "다시 한번 확인하기").done {
                b in
                if !b {
                    print("다시 한번 확인하기")
                } else {
                    print("변경한 답안으로 저장하기")
                    self.userResponseList![num - 1].answer = newAnswer
                    self.ansToServer = self.sendAnswerToServer(answer : newAnswer)
                    let addseq = self.userResponseList![num - 1].addSeq ?? 0
                    
                    //무료기능 추가
                    if let isAI = self.isAIRecommend, isAI == true {
                        PreparationAPI.shared.weakCategoryStudyAnswer(problemSeq: self.problemSeq, duration: duration,
                                                                      questionId: self.questionId!, userAnswer: self.ansToServer!).done
                        {
                            res in
                            if let suc = res.success, suc == true {
                                self.last = false
                                self.number = num - 1
                                self.navigationController?.popViewController(animated: false)
                            }
    
                        }
                        
                    } else { //무료 기능 아닌 경우
                        if let iscoaching = self.isCoachingTest, iscoaching == true {
                            StudyAPI.shared.coachingTestAnswer(addSeq: addseq, duration: duration, number: num, questionId: self.questionId!, studyDay: self.studyDate!, userAnswer: self.ansToServer!, userSeq: self.userSeq!).done
                            { res in
                                if let suc = res.success, suc == true {
                                    self.last = false
                                    self.number = num - 1
                                    self.navigationController?.popViewController(animated: false)
                                }
                            }
                        } else {
                            StudyAPI.shared.curriculumStudyAnswer(cunitSeq: self.cunitSeq!, currSeq: self.currSeq!, duration: duration,
                                                                  questionId: self.questionId!, userAnswer: self.ansToServer!, userSeq: self.userSeq!).done
                            { res in
                                if let success = res.success, success == true {
                                    self.last = false
    //                                        self.trackStatus! -= 1
                                    self.number = num - 1
    //                                        self.trackQ! -= 1
                                    self.navigationController?.popViewController(animated: false)
                                }
                            }
                        }
                        
                        self.navigationController?.popViewController(animated: false)
                    }
                }
            }
        } else {
            self.navigationController?.popViewController(animated: false)
        }
        
    }
    
    
    
    @IBAction func lbBack(_ sender: Any) {
        
        //키보드 내림
        self.tfAnswer.resignFirstResponder()
        
        print("뒤로가기 버튼 누름, number: \(self.number!)")
        print("뒤로가기 버튼 누른 직후 userResponseList: \(self.userResponseList!)")
        endTime = DispatchTime.now()
        let nanoDuration = endTime!.uptimeNanoseconds - startTime!.uptimeNanoseconds
        let temp = Double(nanoDuration) / 1_000_000_000
        print("백버튼 누른 후, 문제 푸는데 걸린 시간은 \(temp) 초 입니다")
        let duration = (Double(round(10 * Double(nanoDuration) / 1_000_000_000)/10))
        print("백버튼 누른 후, 문제 푸는데 걸린 시간은 \(duration) 초 입니다")

        guard let num = self.number else { return }
        
        
        if num - 1 == 0 {
//            self.navigationController?.popViewController(animated: false)
            self.firstProblemBack(duration: duration)
        } else {
        
            guard let num = self.number else { return }
            //새로 쓴 답이 그 전과 다른 경우, alert창 띄우기
            var oldAnswer = self.userResponseList![num - 1].answer
            var newAnswer : String?
            if (indexOfOneAndOnly == nil && self.isMultipleChoice == true) {    //객관식 잘 모르겠음
                newAnswer = "잘 모르겠음."
            } else {
                newAnswer = indexToStrings(indexAnswers: self.indexOfOneAndOnly)
            }
            if newAnswer == "정답을 입력하세요" || newAnswer == "" {  //주관식 잘 모르겠음(후자는 아무것도 선택안하고 백버튼 누른 경우)
                newAnswer = "잘 모르겠음."
            }
            
            /////////여기추가
            if (oldAnswer == "" && self.isMultipleChoice == false) {
                oldAnswer = "잘 모르겠음."
            }
            print("여기oldAnswer: \(oldAnswer)")
            print("여기newAnswer: \(newAnswer)")
            
            if oldAnswer == nil && newAnswer == "잘 모르겠음." { //첫번째 문제 아니면서, 중간에 들어온 문제인데 아무 답도 선택안하고 백버튼 누르는 경우
//                self.userResponseList![self.number! - 1].answer = "잘 모르겠음."
                self.last = false
                self.number = num - 1
                self.initialize()
            } else {
            
                if (oldAnswer != newAnswer) {
                    if !(oldAnswer == "잘 모르겠음." && newAnswer == "잘 모르겠음.") {
                        self.alert(title: "답안을 변경하셨네요!", message: "기존 선택한 답안을 삭제하고\n변경한 답안으로 저장할까요?",
                                   button: "변경한 답안으로 저장하기", subButton: "다시 한번 확인하기").done {
                            b in
                            if !b {
                                print("다시 한번 확인하기")
                            } else {
                                print("변경한 답안으로 저장하기")
                                self.userResponseList![num - 1].answer = newAnswer
                                self.ansToServer = self.sendAnswerToServer(answer : newAnswer)
                                let addseq = self.userResponseList![num - 1].addSeq ?? 0
                                
                                //무료기능 추가
                                if let isAI = self.isAIRecommend, isAI == true {
                                    PreparationAPI.shared.weakCategoryStudyAnswer(problemSeq: self.problemSeq, duration: duration,
                                                                                  questionId: self.questionId!, userAnswer: self.ansToServer!).done
                                    {
                                        res in
                                        if let suc = res.success, suc == true {
                                            self.last = false
                                            self.number = num - 1
                                            self.initialize()
                                        }
                                    }
                                } else {
                                
                                    if let iscoaching = self.isCoachingTest, iscoaching == true {
                                        StudyAPI.shared.coachingTestAnswer(addSeq: addseq, duration: duration, number: num, questionId: self.questionId!, studyDay: self.studyDate!, userAnswer: self.ansToServer!, userSeq: self.userSeq!).done
                                        { res in
                                            if let suc = res.success, suc == true {
                                                self.last = false
                                                self.number = num - 1
                                                self.initialize()
                                            }
                                        }
                                    } else {
                                        StudyAPI.shared.curriculumStudyAnswer(cunitSeq: self.cunitSeq!, currSeq: self.currSeq!, duration: duration,
                                                                              questionId: self.questionId!, userAnswer: self.ansToServer!, userSeq: self.userSeq!).done
                                        { res in
                                            if let success = res.success, success == true {
                                                self.last = false
            //                                        self.trackStatus! -= 1
                                                self.number = num - 1
            //                                        self.trackQ! -= 1
                                                self.initialize()
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {    //예전 답과, 지금 답이 둘다 잘 모르겠어요 인 경우
                        self.last = false
    //                        self.trackStatus! -= 1
                        self.number = num - 1
    //                        self.trackQ! -= 1
                        self.initialize()
                    }
                } else {
                    self.last = false
    //                    self.trackStatus! -= 1
                    self.number = num - 1
    //                    self.trackQ! -= 1
                    self.initialize()
                }
            }
            
            
        }
    }
   
    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.ivProblem
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("didendediting1에서 textField: \(tfAnswer.text!)ddd")
//        self.answerView.isHidden = false
        if (tfAnswer.text == "" || tfAnswer.text == "정답을 입력하세요" || tfAnswer.text == "잘 모르겠음." || tfAnswer.text == " " || tfAnswer.text == nil) {
            print("didendediting2에서 textField: \(tfAnswer.text!)ddd")
//            tfAnswer.text = "정답을 입력하세요"
            button.isHidden = true
            
            self.answerView.setUp()
            self.answerView.swipeImageView.alpha = 1.0
            self.answerView.backgroundColor = UIColor.Foggy
            answerView.isHidden = false
        } else {
            print("didendediting3에서 textField: \(tfAnswer.text!)ddd")
            button.isHidden = false
            answerView.isHidden = true
            button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
//            self.view.addSubview(button)
        }
        self.answerView.answer = tfAnswer.text
        self.bottomConstraint.constant = 0
//        self.bottomView.bottomAnchor.constraint(equalTo: view.superview!.bottomAnchor,
//                                                constant: self.keyboardSize!.height).isActive = true
    }
    
    
    
    func initPencil(){
        // Set up the canvas view with the first drawing from the data model.
        pkCanvasView.delegate = self
        pkCanvasView.alwaysBounceVertical = false
        pkCanvasView.isOpaque = false
        
        // Set up the tool picker
        if #available(iOS 14.0, *) {
            toolPicker = PKToolPicker()
        } else {
            // Set up the tool picker, using the window of our parent because our view has not
            // been added to a window yet.
            guard let window = view.window else { return }
            toolPicker = PKToolPicker.shared(for:   window)
        }
        
        toolPicker.setVisible(false, forFirstResponder: pkCanvasView)
        toolPicker.addObserver(pkCanvasView)
        toolPicker.addObserver(self)
//        updateLayout(for: toolPicker)
        pkCanvasView.becomeFirstResponder()
        
        if #available(iOS 14.0, *) {
            pkCanvasView.allowsFingerDrawing = true

        } else {
            pkCanvasView.allowsFingerDrawing = true
        }
       
    }
    
    
    @IBAction func popupPencilView(_ sender: Any) {
        pkCanvasView.isHidden = false
        pencilToolView.isHidden = false
        btPencil.isHidden = true
        if #available(iOS 14.0, *) {
            toolPicker.setVisible(true, forFirstResponder: pkCanvasView)
        } else {
            guard let window = view.window else { return }
            toolPicker.setVisible(true, forFirstResponder: pkCanvasView)
        }
    }
    
    @IBAction func pencilClose(_ sender: Any) {
        pkCanvasView.isHidden = true
        pencilToolView.isHidden = true
        btPencil.isHidden = false
        if #available(iOS 14.0, *) {
            toolPicker.setVisible(false, forFirstResponder: pkCanvasView)
        } else {
            guard let window = view.window else { return }
            toolPicker.setVisible(false, forFirstResponder: pkCanvasView)
        }
    }
    
    @IBAction func pencilClear(_ sender: Any) {
        pkCanvasView.drawing = PKDrawing()
    }
}

