//
//  StudyNavigationViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/02/14.
//

import UIKit
import CoreData
class StudyNavigationViewController: UINavigationController {
   
//    public func pushViewController(_ viewController: UIViewController, animated: Bool, completion:@escaping (()->())) {
//        CATransaction.setCompletionBlock(completion)
//        CATransaction.begin()
//        self.pushViewController(viewController, animated: animated)
//        CATransaction.commit()
//    }
    
//    func popViewController(animated: Bool, completion:@escaping (()->())) -> UIViewController? {
//            CATransaction.setCompletionBlock(completion)
//            CATransaction.begin()
//            let poppedViewController = self.popViewController(animated: animated)
//            CATransaction.commit()
//            return poppedViewController
//        }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        LoadingView.show()
//        self.view.backgroundColor = UIColor.OffWhite
//        
//        
//
//        var payType = ""
//        var myInfo:MyInfo? = nil
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let container = appDelegate.persistentContainer
//        do {
//            let myInfos = try container.viewContext.fetch(MyInfo.fetchRequest())
//            if myInfos.count > 0
//            {
//                myInfo = myInfos[0]
//                payType = myInfo?.payType ?? ""
//            }
//        } catch {
//            print(error.localizedDescription)
//        }
//        
//        let userSeq = myInfo?.userSeq ?? 0
//        var hasCurriculum : Bool = false
//        
//        StudyAPI.shared.myCurriculumList(userSeq: Int(userSeq)).done { res in
//            if let suc = res.success, suc == true {
//                guard let data = res.data else { return }
//                if data.count > 0 {
//                    hasCurriculum = true
//                }
//            }
//        }.catch { error in
//            
//        }.finally {
//            switch hasCurriculum {
//            case false :
//                let vc = R.storyboard.study.studyMainViewController()!
//                vc.currtext = "무료 체험하기"
//                vc.userType = payType
//                vc.userSeq = Int(userSeq)
//                self.pushViewController(vc, animated: false)
//                
//            default:
//                let vc = R.storyboard.study.studyCurriculumViewController()!
//                vc.userType = payType 
//                vc.userSeq = Int(userSeq)
//                self.pushViewController(vc, animated: false)
//                
//            }
//        }
        
        
        
//        if payType == "f" {
//            print("여기1")
//            let vc = R.storyboard.study.studyMainViewController()!
//            vc.currtext = "무료 체험하기"
//            vc.userType = "f"
//            vc.userSeq = Int(userSeq)
//            self.pushViewController(vc, animated: false)
//        } else if payType == "b" {
//            print("여기2")
//            let vc = R.storyboard.study.studyMainViewController()!
//            vc.userType = "b"
//            vc.userSeq = Int(userSeq)
//            vc.currtext = "수강권 구매하기"
//            self.pushViewController(vc, animated: false)
//        } else if payType == "p" {
//            print("여기3")
//            let vc = R.storyboard.study.studyCurriculumViewController()!
//            vc.userSeq = Int(userSeq)
//            self.pushViewController(vc, animated: false)
//        }
//        LoadingView.hide()
        
    
        
//        LoadingView.show()
//        HomeAPI.shared.initialView().done { res in
//            if let suc = res.success, suc == true {
//                guard let data = res.data else { return }
//                if data.userType! == "f" {
//                    print("여기1")
//                    let vc = R.storyboard.study.studyMainViewController()!
//                    vc.currtext = "무료 체험하기"
//                    vc.userType = "f"
//                    vc.userSeq = data.userSeq
//                    self.pushViewController(vc, animated: false)
//                } else if data.userType! == "b" {
//                    print("여기2")
//                    let vc = R.storyboard.study.studyMainViewController()!
//                    vc.userType = "b"
//                    vc.userSeq = data.userSeq
//                    vc.currtext = "수강권 구매하기"
//                    self.pushViewController(vc, animated: false)
//                } else if data.userType! == "p" {
//                    print("여기3")
//                    let vc = R.storyboard.study.studyCurriculumViewController()!
//                    vc.userSeq = data.userSeq
//                    self.pushViewController(vc, animated: false)
//                }
//            }
//        }.catch { error in
//
//        }.finally {
//            LoadingView.hide()
//        }
//
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UINavigationController {
    public func pushViewController(_ viewController: UIViewController, animated: Bool, completion:@escaping (()->())) {
        CATransaction.setCompletionBlock(completion)
        CATransaction.begin()
        self.pushViewController(viewController, animated: animated)
        CATransaction.commit()
    }
    public func popViewController(animated: Bool, completion:@escaping (()->())) -> UIViewController? {
            CATransaction.setCompletionBlock(completion)
            CATransaction.begin()
            let poppedViewController = self.popViewController(animated: animated)
            CATransaction.commit()
            return poppedViewController
    }
}

extension UINavigationController: UIGestureRecognizerDelegate {

    open override func viewDidLoad() {
        super.viewDidLoad()
        interactivePopGestureRecognizer?.delegate = self
    }

    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
//        CommonUtil.tabbarController?.tabBar.isHidden = true
//        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        return viewControllers.count > 1
    }
}


