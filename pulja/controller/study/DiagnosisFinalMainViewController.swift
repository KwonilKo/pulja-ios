//
//  DiagnosisFinalMainViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/01/24.
//

import UIKit
import PromiseKit

class DiagnosisFinalMainViewController: PuljaBaseViewController {
    
    var schoolGrade : String?
    var goalGrade: Int?
    var diagnosisSeq: Int?
    var userSeq: Int?
    var delegate : PuljaBaseViewController?
    var fromSeven = false
    var fromStudy = false
    @IBOutlet var nameText: UILabel!
    
    @IBOutlet var diagImg: UIImageView!
    @IBOutlet var title1: UILabel!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        var temp: String?
        if myInfo!.username != "username" {
            temp = "\(myInfo!.username!) 학생을 위한"
        } else {
            temp = ""
        }
        self.nameText.text = "\(temp!) AI 진단평가 생성 완료"
        
        
        // Do any additional setup after loading the view.
        if let ds = self.diagnosisSeq, ds == 1 {
//            self.title1.text = "중고등 개념 연계 진단평가"
            self.title1.text = "수학(상)\n실력점검 진단평가"
            self.diagImg.image = UIImage(named: "MiddleDiagnosisTime")
        } else {
            self.title1.text = "수학(상), (하)\n실력점검 진단평가"
//            self.title1.text = "고등학교 수학 진단평가"
            self.diagImg.image = UIImage(named: "highdiagnosis")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonUtil.tabbarController?.tabBar.isHidden = true
        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
    }
    
    
    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(animated)
        

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        CommonUtil.tabbarController?.tabBar.isHidden = false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func nextTime(_ sender: Any) {
        
        
        if self.fromSeven
        {
            if self.fromStudy
            {
                self.navigationController?.popToRootViewController(animated: true)
            }
            else
            {
                if let vcDetail = R.storyboard.main.tabbarController(){
                    self.navigationController?.setViewControllers([vcDetail], animated: false)
                }
            }
            
        }
        else
        {
            self.navigationController?.popToRootViewController(animated: true)
        }
        
    }
    
    @IBAction func ibBack(_ sender: Any) {
//        self.dismiss(animated: true )
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ibNext(_ sender: Any) {
        print("diagnosisSeq: \(self.diagnosisSeq!)")
//        self.userSeq = 6
        StudyAPI.shared.diagnosisStep1(diagnosisSeq: self.diagnosisSeq!, goalGrade: self.goalGrade!, schoolGrade: self.schoolGrade!, userSeq: self.userSeq!).done {
            res in
            if let bool = res.success, bool == true {
                
                
                
                
                let vc = R.storyboard.study.multipleChoiceViewController()!
                vc.delegate = self
                vc.userSeq = self.userSeq
                vc.userResponseList = []
//                vc.trackStatus = 0
                vc.diagnosisSeq = self.diagnosisSeq
                vc.isDiagnosis = true
                vc.fromSeven = self.fromSeven
                vc.fromStudy = self.fromStudy
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
//        firstly {
//            StudyAPI.shared.diagnosisStep1(diagnosisSeq: self.diagnosisSeq!, goalGrade: self.goalGrade!, schoolGrade: self.schoolGrade!, userSeq: self.userSeq!)
//        }.then { b in
////            print("진단설문결과 api 성공적으로 받아옴")
//            StudyAPI.shared.diagnosisStart(diagnosisSeq: self.diagnosisSeq!, nodeSeq:0, number: 0, stepCount: 1, testSeq: 0)
//        }.done { res in
//            if let suc = res.success, suc == true {
//                print("첫문제 api 성공적으로 받아옴 ")
//                let vc = R.storyboard.study.multipleChoiceViewController()!
//                vc.userSeq = self.userSeq
//                vc.userResponseList = []
//                vc.trackStatus = 0
//
//            }
//        }.catch { error in
//            print(error.localizedDescription)
//        }
        
       
        
    }
    
}
