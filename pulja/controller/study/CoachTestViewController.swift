//
//  CoachTestViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/02/05.
//

import UIKit
import Collections
import OrderedCollections


class CoachTestViewController: PuljaBaseViewController, UITableViewDataSource,
UITableViewDelegate, CustomTableViewDelegate {
    
    
    
    @IBOutlet var emptyView: UIView!
    
    @IBOutlet weak var btnChat: UIButton!
    
    let headerMaxHeight = 0.0
    @IBOutlet var CurriculumTableView: UITableView!{
        didSet{
            CurriculumTableView.contentInset = UIEdgeInsets(top: headerMaxHeight, left: 0, bottom: -10.0, right: -20.0)
        }}
    
    var coachTestCurriculum : [CoachingTestDetail] = []
    
    var ListStudyDaysCurr : [String : [String : [CoachingTestDetail]]] = [:]
    var OrderedListStudyDaysCurr : OrderedDictionary<String, OrderedDictionary<String, [CoachingTestDetail]>> = [:]
   
    var userSeq : Int = 0
    var currSeq : Int = 0
    var cunitSeq : Int = 0
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonUtil.tabbarController?.tabBar.isHidden = true
        CommonUtil.tabbarController?.bottomNavBar.isHidden = true

        checkUnreadMessage()

    }
    
    func checkUnreadMessage()
               {
                   let payType = self.myInfo?.payType ?? "f"
                   
                   if payType == "p"
                   {
                       CommonUtil.shared.checkNewMessage { hasNew in
                           
                           if hasNew > 0
                           {
                               self.btnChat.setImage(UIImage(resource:R.image.groupChatNew), for: .normal)
                           }
                           else
                           {
                               self.btnChat.setImage(UIImage(resource:R.image.groupChat), for: .normal)
                           }
                       }
                   }
                   else
                   {
                       self.btnChat.setImage(UIImage(resource:R.image.groupChat), for: .normal)
                   }

               }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.ListStudyDaysCurr = [:]
        self.OrderedListStudyDaysCurr = [:]
        
        super.viewDidAppear(animated)
        
        
        LoadingView.show()
        StudyAPI.shared.coachingTestDetail(userSeq: userSeq).done { res in
            if let bool = res.success, bool == true {
                self.coachTestCurriculum = res.data ?? []
                if self.coachTestCurriculum.count == 0 { self.initEmpty() }
                print("코칭 테스트: \(self.coachTestCurriculum)")
                
                
                for cD in self.coachTestCurriculum {
                    self.OrderedListStudyDaysCurr[cD.studyDay!] = [:]
                    self.OrderedListStudyDaysCurr[cD.studyDay!]![cD.studyStatus!] = [cD]

                }
                
                self.CurriculumTableView.reloadData()
                
            }
        }.catch { error in
            
        }.finally {
            LoadingView.hide()
        }
        

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        CommonUtil.tabbarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.emptyView.isHidden = true
        
        //디자인
        CurriculumTableView.contentInsetAdjustmentBehavior = .never
        CurriculumTableView.separatorStyle = .none
        if #available(iOS 15.0, *) {
            CurriculumTableView.sectionHeaderTopPadding = 0
        }
        
        
        //헤더 등록
        let headerNib = UINib(nibName: "StudyCurriculumHeaderView", bundle: nil)
        self.CurriculumTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "StudyCurriculumHeaderView")
        
        //셀 등록
        self.CurriculumTableView.register(StudyCurriculumTableViewCell.nib(), forCellReuseIdentifier: "StudyCurriculumTableViewCell")
        
//        StudyAPI.shared.coachingTestDetail(userSeq: userSeq).done { res in
//            if let bool = res.success, bool == true {
//                self.coachTestCurriculum = res.data ?? []
//                if self.coachTestCurriculum.count == 0 { self.initEmpty() }
//                print("코칭 테스트: \(self.coachTestCurriculum)")
//
//
//                for cD in self.coachTestCurriculum {
//                    self.OrderedListStudyDaysCurr[cD.studyDay!] = [:]
//                    self.OrderedListStudyDaysCurr[cD.studyDay!]![cD.studyStatus!] = [cD]
//
//                }
//
//                self.CurriculumTableView.reloadData()
//
//            }
//        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.OrderedListStudyDaysCurr.count == 0 { return 0 }
        
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int{
        print("섹션 개수는 \(self.ListStudyDaysCurr.count)")
        return self.OrderedListStudyDaysCurr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let StringDates = Array(self.OrderedListStudyDaysCurr.keys)
        let Date = StringDates[indexPath.section]
        
        let CellCurriculum = self.OrderedListStudyDaysCurr[Date]!
        let oneCurr = CellCurriculum.values.first![0] as! CoachingTestDetail
        let cell:StudyCurriculumTableViewCell = tableView.dequeueReusableCell(withIdentifier: "StudyCurriculumTableViewCell", for: indexPath)
        as! StudyCurriculumTableViewCell
        
        cell.isCoachingTest = true
    
        cell.currUnitName = "코칭 테스트"
        cell.studyStatusName = CellCurriculum.keys.first! as! String
        cell.videoCnt = 0
        cell.problemCnt = oneCurr.problemCount
        cell.sectionIdx = indexPath.section
        cell.totalSection = self.OrderedListStudyDaysCurr.count
        cell.studyDate = self.OrderedListStudyDaysCurr.keys[indexPath.section]
//        cell.cunitSeq = self.cunitSeq
//        cell.currSeq = self.currSeq
        cell.userSeq = self.userSeq
        cell.delegateController = self
        cell.awakeFromNib()
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.OrderedListStudyDaysCurr.keys.count > 0 {
            return 30
        }
        return .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = CurriculumTableView.dequeueReusableHeaderFooterView(withIdentifier: "StudyCurriculumHeaderView") as! StudyCurriculumHeaderView
        
        //마지막 헤더면 isBigCircle = true로 해주기
        if self.OrderedListStudyDaysCurr.keys.count > 0 {
            let DateString = Array(self.OrderedListStudyDaysCurr.keys)
            header.isCoachingTest = true 
            header.isBigCircle = false
            header.lbdate = DateString[section]
            header.sectionIdx = section
            header.totalSection = self.OrderedListStudyDaysCurr.keys.count
            header.awakeFromNib()
        }
        
        return header
    }

    
    
    func didTouchCell(_ secDate: String, _ rowStatus: String, _ ListStudyDaysExpand: OrderedDictionary<String, OrderedDictionary<String, Bool>>, _ indexPath: IndexPath?) {
       
    }
    

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        CurriculumTableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    
    
    func initEmpty() {
        self.CurriculumTableView.isHidden = true
        self.emptyView.isHidden = false
    }
    
    
    
    
    
    
    
    
    
    

    @IBAction func ibBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func btnChatPressed(_ sender: Any) {
        //CommonUtil.shared.goChatting(viewController: self)
    }
    
}
