//
//  ReviewDetailViewController.swift
//  pulja
//
//  Created by 김병헌 on 2022/01/27.
//

import UIKit
import WebKit

class ReviewDetailViewController: UIViewController, UIScrollViewDelegate, WKUIDelegate {
   
    

    
    @IBOutlet weak var btBookMark: UIButton!
    
    
    
    @IBOutlet weak var ivProblem: UIImageView!
    
    
    @IBOutlet weak var svContent: UIScrollView!
    
    @IBOutlet weak var problemIVHeight: NSLayoutConstraint!
    
    @IBOutlet weak var svProblemImage: UIScrollView!
    
    
    @IBOutlet weak var answerContents: UIView!
    
    @IBOutlet weak var svExplain: UIScrollView!
    @IBOutlet weak var ivExplain: UIImageView!
    
    
    @IBOutlet weak var wvExplain: WKWebView!
    
    @IBOutlet weak var explainView: UIView!
    
    
    @IBOutlet weak var explainVideoHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var explainIVHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var answerContentsHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var lbAnswer: UILabel!
    
    
    
    
    var reviewQuestion:ReviewQuestion?
    var reqParam:AnswerInfoReq?
    
    var bookMarkSeq : Int?
    
    var isBookMarkChange = false
    
    var detailBookMarkCallback : (_ result : Int?) -> Void = { _ in}
    
    
    var answerOrignHeight:CGFloat = 0
    var videoHeight:CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if (reviewQuestion?.bookMarkSeq) != nil {
            btBookMark.setImage(R.image.iconSolidStar(), for: .normal)
        } else {
            btBookMark.setImage(R.image.iconOutlineStar(), for: .normal)
        }
        
        bookMarkSeq = reviewQuestion?.bookMarkSeq
        
        svProblemImage.minimumZoomScale = 1.0
        svProblemImage.maximumZoomScale = 2.0
        
        svExplain.minimumZoomScale = 1.0
        svExplain.maximumZoomScale = 2.0
        
        answerContents.isHidden = true

//        initAnswerView()
        initData()
    }
    
    func initAnswerView(){
        answerOrignHeight = answerContentsHeight.constant
        answerContentsHeight.constant = 0
        view.layoutIfNeeded()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
   public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
       return self.ivProblem
   }
    
   
    
    
    func initData() {
        
        if let reviewQuestion = reviewQuestion {
            reqParam = AnswerInfoReq(problemType: reviewQuestion.problemType, questionId: reviewQuestion.questionId, solveSeq: reviewQuestion.solveSeq, userSeq: nil)
            
            CommonAPI.shared.answerInfo(answerInfoReq: reqParam!).done { res in
                guard let data = res.data  else {
                    self.dismiss(animated: true)
                    return
                }
                
                let url = URL(string:"\(Const.PROBLEM_IMAGE_URL)\(data.questionId ?? 0).jpg")
                self.ivProblem.kf.indicatorType = .activity
                self.ivProblem.kf.setImage(with: url,  options: [.transition(.fade(0.2))])
                let height = self.ivProblem.contentClippingRect.height
                
                if self.problemIVHeight.constant < height {
                    self.problemIVHeight.constant = height
                }
                print("problem height::::\(height)")
                
                let explainUrl = URL(string:"\(Const.EXPLAIN_IMAGE_URL)\(data.questionId ?? 0).jpg")
                self.ivExplain.kf.indicatorType = .activity
                self.ivExplain.kf.setImage(with: explainUrl,  options: [.transition(.fade(0.2))])
                let epheight = self.ivExplain.contentClippingRect.height
                
                print("explain height::::\(epheight)")
                if self.explainIVHeight.constant < epheight {
                    self.explainIVHeight.constant = epheight
                    let addHeight =  epheight - self.explainIVHeight.constant
                    self.answerContentsHeight.constant += addHeight
                    self.answerOrignHeight = self.answerContentsHeight.constant
                }
                
                
                self.initExplainWebview(movieUrl: data.movieURL)
                self.initAnswerView()
                
                if var userAnswer = data.userAnswer, var answerTxt = data.answerTxt  {
                    
                    var startIndex = answerTxt.startIndex
                    var endIndex = answerTxt.endIndex
                    if let index = answerTxt.index(of: "$$  {") {
                        
                        startIndex = answerTxt.index(index, offsetBy: 5)
                        print(index)
                    }

                    if let index = answerTxt.index(of: "}  $$") {
                        endIndex = index
                        print(index)
                    }
                    
                    let range = startIndex..<endIndex
                    answerTxt = answerTxt.substring(with: range)
                    self.lbAnswer.text = "내답: \(userAnswer)   정답: \(answerTxt)"
                }
                
            }

        }
    }
    
    func initExplainWebview(movieUrl:String?){
        guard let url = URL(string:movieUrl!) else {
            
            explainVideoHeight.constant = 0
            return
        }
        
//        let url = URL(string:"https://www.apple.com")
        let urlRequest = URLRequest(url: url)
        self.wvExplain.load(urlRequest)
        
        self.wvExplain.uiDelegate = self

        
    }
    
    
    
    
    @IBAction func setBookMark(_ sender: Any) {
        
        var req : BookMarkReq = BookMarkReq(bookMarkSeq: self.reviewQuestion?.bookMarkSeq
                                            , solveSeq: self.reviewQuestion?.solveSeq
                                            , userSeq: nil
                                            , problemType: self.reviewQuestion?.problemType)
       
        ReviewAPI.shared.bookMark(bookMarkReq: req).done { res in
           
            self.reviewQuestion?.bookMarkSeq = res.data?.bookMarkSeq
           
            if self.bookMarkSeq == res.data?.bookMarkSeq {
                self.isBookMarkChange = false
            } else {
                self.isBookMarkChange = true
            }

            if (self.reviewQuestion?.bookMarkSeq) != nil {
                self.btBookMark.setImage(R.image.iconSolidStar(), for: .normal)
            } else {
                self.btBookMark.setImage(R.image.iconOutlineStar(), for: .normal)
            }
           
        }
        
    }
    
    
    
    @IBAction func goBack(_ sender: Any) {
        if isBookMarkChange {
            detailBookMarkCallback (self.reviewQuestion?.bookMarkSeq )
        }
//        navigationController?.popViewController(animated: true)
        dismiss(animated: true)
    }
    
    
    
    @IBAction func showAnswer(_ sender: UIButton) {
        if answerContentsHeight.constant == 0 {
            sender.setImage(R.image.iconSolidCheveronUp(), for: .normal)
            answerContents.isHidden = false
            answerContentsHeight.constant = self.answerOrignHeight
            self.view.setNeedsUpdateConstraints()
            self.view.layoutIfNeeded()
            
        } else {
            sender.setImage(R.image.iconSolidCheveronDown(), for: .normal)
            answerContentsHeight.constant = 0
            self.answerContents.isHidden = true
            self.view.setNeedsUpdateConstraints()
            self.view.layoutIfNeeded()
            
        }
        
    }
    
    
    
   
}
