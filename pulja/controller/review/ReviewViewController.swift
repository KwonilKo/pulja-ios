//
//  ReviewViewController.swift
//  pulja
//
//  Created by 김병헌 on 2021/11/30.
//

import UIKit
import Tabman
import Pageboy


protocol ReviewDelegate {
    func wrongChange(isWrong : Bool)
    func scrollTop()
    
    func unitInfo(units: [UnitInfo])
    
    func subjectInfo(subjects:[SubjectInfo])
}



class ReviewViewController: TabmanViewController, PageboyViewControllerDataSource, TMBarDataSource {
    
    @IBOutlet weak var TabView: UIView!
    
    @IBOutlet weak var switchWrong: UISwitch!
    
    
    @IBOutlet weak var btScrollTopBottomHeight: NSLayoutConstraint!
    
    private var viewControllers: Array<UIViewController> = []
    
    let tabTitle = ["복습", "보관함"]
    
    var isWrongOnly = false
    
    var units:[UnitInfo] = []
    
    var subjects:[SubjectInfo] = []
    
    var reviewDelegate: ReviewDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let vc2 = R.storyboard.review.reviewListViewController()!
        let vc3 = R.storyboard.review.reviewListViewController()!
//        let vc3 = R.storyboard.review.favoriteListViewController()!
        viewControllers.append(vc2)
        viewControllers.append(vc3)
        
        
        // Set PageboyViewControllerDataSource dataSource to configure page view controller.
        dataSource = self

        // Create a bar
        let bar = TMBarView.ButtonBar()
        
        // Customize bar properties including layout and other styling.
        bar.layout.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        bar.layout.contentMode = .fit
        bar.layout.interButtonSpacing = 0.0

        bar.fadesContentEdges = false
        bar.spacing = 0.0
        bar.backgroundView.style = .flat(color: UIColor.paleGrey)
        // Set tint colors for the bar buttons and indicator.
        bar.buttons.customize {
        //                 $0.tintColor = UIColor.tabmanForeground.withAlphaComponent(0.4)
            $0.tintColor = UIColor.tabmanSecondary
            $0.font =  UIFont.systemFont(ofSize: 24, weight: .heavy) //UIFont.systemFont(ofSize: 24, weight: .bold)
            $0.selectedTintColor = .tabmanForeground
            $0.adjustsFontForContentSizeCategory = false
            
//            if #available(iOS 11, *) {
//                $0.adjustsFontForContentSizeCategory = true
//            }
        }
        bar.indicator.weight = .custom(value: 3)
        bar.indicator.cornerStyle = .square
        bar.indicator.tintColor = .tabmanForeground

        // Add bar to the view - as a .systemBar() to add UIKit style system background views.
        //             addBar(bar.systemBar(), dataSource: self, at: .top)
        addBar(bar, dataSource: self, at: .custom(view: TabView, layout: nil))
        
        
        switchWrong.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
//        switchWrong.tintColor =  UIColor.puljaPaleGrey
//        switchWrong.onTintColor =  UIColor.puljaPaleGrey
        
        CommonAPI.shared.unitList(unit2Keyword: "").done { res in
            
            self.units = res.data!
            self.reviewDelegate?.unitInfo(units: res.data!)
        }
        
        CommonAPI.shared.subjectList().done { res in
            
            self.subjects = res.data!
            
            self.reviewDelegate?.subjectInfo(subjects: res.data!)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
//        print ("self.view.frame.height:::\(self.view.frame.height)")
//        let tabHeight = self.tabBarController?.tabBar.frame.height ?? 0
//        print ("tabHeight:::\(tabHeight)")
//        btScrollTopBottomHeight.constant = CGFloat(btHeight) + tabHeight
        
//        self.view.layoutIfNeeded()
//           self.tabBarController?.hidesBottomBarWhenPushed = true
    }
    
    
    
    
    func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
        viewControllers.count // How many view controllers to display in the page view controller.
        }
        
    func viewController(for pageboyViewController: PageboyViewController, at index: PageboyViewController.PageIndex) -> UIViewController? {
        print(index)
        
        guard let vc = viewControllers[index] as? ReviewListViewController  else {
            return viewControllers[index]
        }
        
        vc.isCorrect = isWrongOnly ? "N" : nil
        vc.listType = index == 0 ? "R" : "B"
        
        if vc.units.isEmpty {
            vc.units = units
        }
        
        if vc.subjects.isEmpty {
            vc.subjects = subjects
        }
        
        reviewDelegate = vc
        
        
        
        return vc // View controller to display at a specific index for the page view controller.
        
    }
    
    func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
        nil // Default page to display in the page view controller (nil equals default/first index).
    }
    
    func barItem(for bar: TMBar, at index: Int) -> TMBarItemable {
        let item =  TMBarItem(title: tabTitle[index])
        if index == 0 {
            //            item.badgeValue = "1"
        }
        return  item// Item to display for a specific index in the bar.
    }
    
    
    @IBAction func wrongChange(_ sender: Any) {
        
//        if switchWrong.isOn {
//
//            switchWrong.thumbTintColor = UIColor.purpleishBlue
//
//        } else {
//
//            switchWrong.thumbTintColor = UIColor.white
//
//        }
        
        isWrongOnly = switchWrong.isOn
        
        reviewDelegate?.wrongChange(isWrong: isWrongOnly)
    }
    
    
    @IBAction func scrollTop(_ sender: Any) {
        
        reviewDelegate?.scrollTop()
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
