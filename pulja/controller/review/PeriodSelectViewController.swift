//
//  PeriodSelectViewController.swift
//  pulja
//
//  Created by 김병헌 on 2022/01/25.
//

import UIKit
import PanModal

class PeriodSelectViewController: UIViewController, PanModalPresentable {
    

    var startDate:String = ""
    var endDate:String = ""
    
    var resultDic : [String:String] = [:]
    
    var selectPeriodCallback : (_ result : [String:String] ) -> Void = { _ in}

    
    @IBOutlet weak var lbStartDate: UILabel!
    
    @IBOutlet weak var lbEndDate: UILabel!
    
    @IBOutlet weak var btApply: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if startDate == "" {
//            let sdt = Date().getSomeDayDate(day: -1)
//            startDate = sdt.toString(.noDotdate)
//            lbStartDate.text =  "\(sdt.toString(.koYMD))부터"
            lbStartDate.text =  "시작일을 선택해주세요."
        } else {
            
            lbStartDate.text = "\(startDate.toDate(.noDotdate).toString(.koYMD))부터"
        }

        
        if endDate == "" {
//            endDate = Date().toString(.noDotdate)
//            lbEndDate.text = "\(Date().toString(.koYMD))까지"
            lbEndDate.text = "종료일을 선택해주세요."
        } else {
            lbEndDate.text = "\(endDate.toDate(.noDotdate).toString(.koYMD))까지"
        }

//        btApply.setBGColor(.Green, for: .highlighted)
        // Do any additional setup after loading the view.
    }
    
    
    
    
    
    @IBAction func selectStartDate(_ sender: Any) {
        
        let vc = R.storyboard.review.datePickerViewController()!
        vc.selectType = "s"
        vc.startDate = startDate
        vc.selectDateCallback = { res in
            self.startDate = res
            self.lbStartDate.text =  "\(res.toDate(.noDotdate).toString(.koYMD))부터"
        }
        present(vc, animated: true, completion: nil)

//        presentPanModal(vc)
        
    }
    
    
    @IBAction func selectEndDate(_ sender: Any) {

        let vc = R.storyboard.review.datePickerViewController()!
        vc.selectType = "e"
        vc.endDate = endDate
        vc.startDate = startDate
        vc.selectDateCallback = { res in
            self.endDate = res
            self.lbEndDate.text =  "\(res.toDate(.noDotdate).toString(.koYMD))까지"
        }
        present(vc, animated: true, completion: nil)

//        presentPanModal(vc)

    }
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
       if let presentableController = viewControllerToPresent as? PanModalPresentable, let controller = presentableController as? UIViewController {
           controller.modalPresentationStyle = .custom
           controller.modalPresentationCapturesStatusBarAppearance = true
           controller.transitioningDelegate = PanModalPresentationDelegate.default
           super.present(controller, animated: flag, completion: completion)
           return
       }
       super.present(viewControllerToPresent, animated: flag, completion: completion)
   }
    
    
    @IBAction func applyFilter(_ sender: Any) {
        
        if startDate == "" {
            if endDate != "" {
                startDate = endDate
            }
        } else if endDate == "" {
            if startDate != "" {
                endDate = startDate 
            }
        }
        
        resultDic = [
            "startDate" :startDate,
            "endDate" : endDate
        ]
        
        selectPeriodCallback(resultDic)
        
        dismiss(animated: true)
    }
    
    
    
    @IBAction func close(_ sender: Any) {
        dismiss(animated: true)
    }
    
    
    @IBAction func resetPeriod(_ sender: Any) {
        
        startDate = ""
        endDate = ""
        lbStartDate.text =  "시작일을 선택해주세요."
        lbEndDate.text = "종료일을 선택해주세요."
        
    }
    
    

    
    // MARK: - PanModal
    var panScrollable: UIScrollView?
    
    var showDragIndicator: Bool {
            return false
        }

    
    var shortFormHeight: PanModalHeight {
        return .contentHeight(256)
    }
    
    // 펼쳐졌을 때
   var longFormHeight: PanModalHeight {
       // Top 부터 0 만큼 떨어지게 설정
       return .contentHeight(256)
   }
    
   

    var anchorModalToLongForm: Bool {
        return false
    }
    
    var cornerRadius: CGFloat {
        return 10
    }
    
    var panModalBackgroundColor : UIColor {
            return R.color.puljaBlack60()!
        }

}
