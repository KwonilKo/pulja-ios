//
//  UnitSelectViewController.swift
//  pulja
//
//  Created by 김병헌 on 2022/01/05.
//

import UIKit
import PanModal
import OrderedCollections
import M13Checkbox

class UnitSelectViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, PanModalPresentable {
    
    let recentMaxHeight:CGFloat = 146.0
    let recentMinHeight:CGFloat = 0.0
        
    
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var cvSelectedUnit: UICollectionView! // 선택 단원 가로 스크롤
    
    
    @IBOutlet weak var lbNotiSelectUnit: UILabel!
    
    @IBOutlet weak var lbEmptyData: UILabel!
    
    @IBOutlet weak var tfSearch: DesignableUITextField!
    
    
    
    @IBOutlet weak var recentUnitView: UIView! //최근 공부한 단원 영역
    
    @IBOutlet weak var recentUnitViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var recentUnitFirstWidth: NSLayoutConstraint!
    
    
    
    
    // 최근 공부한 단원 1
    @IBOutlet weak var recentUnitFirst: UIView!
    
    @IBOutlet weak var lbRecentUnit2First: UILabel!
    
    @IBOutlet weak var lbRecentUnit1First: UILabel!
    
    @IBOutlet weak var cbRecentUnitFirst: M13Checkbox!
    
    // 최근 공부한 단원 2
    @IBOutlet weak var recentUnitSecond: UIView!
    
    @IBOutlet weak var lbRecentUnit2Second: UILabel!
    
    @IBOutlet weak var lbRecentUnit1Second: UILabel!
    
    @IBOutlet weak var cbRecentUnitSecond: M13Checkbox!
    
    
    @IBOutlet weak var recentEmptyView: UIView!
    
    
    
    @IBOutlet weak var svUnitList: UIScrollView!
    
    @IBOutlet weak var cvUnitList: UICollectionView!  //단원목록
    
    
    @IBOutlet weak var cvUnitHeight: NSLayoutConstraint!
    
    @IBOutlet weak var headerHeight: NSLayoutConstraint! {
            didSet {
                headerHeight.constant = recentMaxHeight
            }
        }
    
    @IBOutlet weak var bottomFillterView: UIView!
    
    @IBOutlet weak var bottomFillterViewHeight: NSLayoutConstraint!
    
    var selectedUnitInfoArr : [UnitInfo] = []  // 선택한 단원
    var recentUnitsArr : [UnitInfo] = []  // 최근 학습 단원 데이터
    
    var hiddenSections = Set<Int>() //접힌 섹션정보
    
    var unitInfoArr : [[UnitInfo]] = []  // 과목별 단원 콜렉션 뷰 데이터
    var unitInfoDic : OrderedDictionary<Int, Array<UnitInfo>> = [:]  //과목별 단원 콜렉션뷰용 그룹핑 dic
    
    var units: [UnitInfo] = [] //섹션에 넣을 단원정보
    var subjects: [SubjectInfo] = [] //전체 과목 정보.
    var subjectSection:[SubjectInfo] = [] //섹션별 과목명
    
    var unitCallback : (_ result : String) -> Void = { _ in}
    
    var selectUnitInfoCallback : (_ result : [UnitInfo]) -> Void = { _ in}
    
    var activeTextField : UITextField? = nil
   
    var isSearchState = false  //검색상태
    
    var searchUnits : [UnitInfo] = [] //섹션에 넣을 단원정보
    var hiddenUnits = Set<Int>() // 검색용 단원숨김 정보.
    
    // bottomsheet 처음 열렸을때 높이 체크용.
    var isShortFormEnabled = true
    
    var oldSearch:[IndexPath] = []
    
    var searchText:String? = nil
    
    var recentCellHeight: CGFloat =  88
    var recentCellWidth: CGFloat = 156
    var recentDefaultSpaceHeight: CGFloat =  58
    var recentDefaultViewHeight : CGFloat  = 146
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.cvSelectedUnit.delegate = self
        self.cvSelectedUnit.dataSource = self
        
        self.cvUnitList.delegate = self
        self.cvUnitList.dataSource = self
        
        self.cvSelectedUnit.register(UnitCollectionViewCell.nib(), forCellWithReuseIdentifier: "UnitCollectionViewCell")
        self.cvSelectedUnit.register(UnitCollectionReusableView.nib(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "UnitCollectionReusableView")

        self.cvUnitList.register(UnitCollectionViewCell.nib(), forCellWithReuseIdentifier: "UnitCollectionViewCell")
        self.cvUnitList.register(UnitCollectionReusableView.nib(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "UnitCollectionReusableView")
        
        mainView.roundedTop(10)
        
        self.tfSearch.addDoneButtonOnKeyboard()
        
//        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UnitSelectViewController.backgroundTap))
//                        self.view.addGestureRecognizer(tapGestureRecognizer)
                        // Do any additional setup after loading the view.
                        NotificationCenter.default.addObserver(self, selector: #selector(UnitSelectViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
                        NotificationCenter.default.addObserver(self, selector: #selector(UnitSelectViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
                
        self.tfSearch.delegate = self
        
        
        oldSearch = getAllIndexPathsInSection(section : 0)
        searchUnits = units
        
        setUnits()
        
        initHideSection()
        
        setRecentUnit()
        
        
        if selectedUnitInfoArr.isEmpty {
            lbNotiSelectUnit.isHidden = false
        } else {
            lbNotiSelectUnit.isHidden = true
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(UnitSelectViewController.rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
    }
        
    deinit {
       NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: nil)
    }
        
    @objc func rotated() {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            
        } else {
            print("Portrait")
        }
        
    // Do any additional setup after loading the view.
          DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
              self.mainView.roundedTop(10)
          }
    }
    //MARK: - Data Setting
    //최근 공부한 단원 설정.
    func setRecentUnit(){

        cbRecentUnitFirst?.stateChangeAnimation = .bounce(.fill)
        cbRecentUnitFirst?.tintColor = UIColor.purpleishBlue
        cbRecentUnitFirst?.secondaryTintColor = UIColor.lightBlueGrey
        cbRecentUnitFirst?.checkmarkLineWidth = CGFloat(4)
        cbRecentUnitFirst?.boxLineWidth = CGFloat(1)

        cbRecentUnitSecond?.stateChangeAnimation = .bounce(.fill)
        cbRecentUnitSecond?.tintColor = UIColor.purpleishBlue
        cbRecentUnitSecond?.secondaryTintColor = UIColor.lightBlueGrey
        cbRecentUnitSecond?.checkmarkLineWidth = CGFloat(4)
        cbRecentUnitSecond?.boxLineWidth = CGFloat(1)

        
        
        CommonAPI.shared.recentStudyUnit2().done { res in
            self.recentUnitsArr = res.data ?? []
            self.setRecentUnitView()
            self.recentUnitViewHeight.constant = self.recentCellHeight + self.recentDefaultSpaceHeight
            
            self.setRecentCheckData()
            
        }.catch { err in
            self.recentUnitView.isHidden = true
            self.recentUnitViewHeight.constant = 0
        }
        
    }
    
    func setRecentUnitView() {
        if recentUnitsArr.isNotEmpty {
           self.recentUnitView.isHidden = false
            self.recentUnitViewHeight.constant = self.recentCellHeight + self.recentDefaultSpaceHeight
            self.lbRecentUnit1First.text = recentUnitsArr[0].unit1SName
           self.lbRecentUnit2First.text = recentUnitsArr[0].unit2SName

           if recentUnitsArr.count > 1 {
               self.lbRecentUnit1Second.text = recentUnitsArr[1].unit1SName
               self.lbRecentUnit2Second.text = recentUnitsArr[1].unit2SName
               self.recentEmptyView.isHidden = true
           } else {
//               self.recentUnitFirstWidth.constant = self.recentCellWidth
               self.recentUnitSecond.isHidden = true
               self.recentEmptyView.isHidden = false
           }
       }else {
           self.recentUnitView.isHidden = true
           self.recentUnitFirst.isHidden = true
           self.recentUnitSecond.isHidden = true
           self.recentEmptyView.isHidden = true
           self.recentUnitViewHeight.constant = 0
       }
        
    }
    
    
    @IBAction func firstUnitCbValueChanged(_ sender: M13Checkbox) {
        switch sender.checkState {
            
        case .unchecked:
            if recentUnitsArr.isNotEmpty {
                if self.selectedUnitInfoArr.isNotEmpty {
                    if self.selectedUnitInfoArr.contains(where: { $0.unit2Seq == recentUnitsArr[0].unit2Seq }) {
                        self.selectedUnitInfoArr.removeObject(recentUnitsArr[0])
                    }
                }
            }
            setChangeCheckData()
            break
            
        case .checked:
            if recentUnitsArr.isNotEmpty {
                if self.selectedUnitInfoArr.isNotEmpty {
                    if !self.selectedUnitInfoArr.contains(where: { $0.unit2Seq == recentUnitsArr[0].unit2Seq }) {
                        self.selectedUnitInfoArr.append(recentUnitsArr[0])
                    }
                } else {
                    self.selectedUnitInfoArr.append(recentUnitsArr[0])
                }
            }
            setChangeCheckData()
            break

        case .mixed:
                break
        }
    }
    
    @IBAction func secondUnitCbValueChanged(_ sender: M13Checkbox) {
        switch sender.checkState {
            case .unchecked:
            if recentUnitsArr.isNotEmpty {
               if self.selectedUnitInfoArr.isNotEmpty {
                   if self.selectedUnitInfoArr.contains(where: { $0.unit2Seq == recentUnitsArr[1].unit2Seq }) {
                       self.selectedUnitInfoArr.removeObject(recentUnitsArr[1])
                   }
               }
           
            }
            setChangeCheckData()
            break
            
        case .checked:
            if recentUnitsArr.isNotEmpty {
                if self.selectedUnitInfoArr.isNotEmpty {
                    if !self.selectedUnitInfoArr.contains(where: { $0.unit2Seq == recentUnitsArr[1].unit2Seq }) {
                        self.selectedUnitInfoArr.append(recentUnitsArr[1])
                    }
                } else {
                    self.selectedUnitInfoArr.append(recentUnitsArr[1])
                }
            }
            setChangeCheckData()
            break
            
        case .mixed:
                break
        
        }
        
    }
    
    // 최근 공부한 단원 체크시 단원, 선택 단원 체크여부 변경.
    func setChangeCheckData(){
        
        self.cvSelectedUnit.reloadData()
        self.cvUnitList.reloadData()
                       
                       self.cvUnitList.performBatchUpdates { [weak self] in
                           if let arr = self?.selectedUnitInfoArr, arr.isEmpty  {
                               self?.lbNotiSelectUnit.isHidden = false
                           } else {
                               self?.lbNotiSelectUnit.isHidden = true
                           }
                       }
                           
                       self.cvSelectedUnit.performBatchUpdates { [weak self] in
                           if let arr = self?.selectedUnitInfoArr, arr.isEmpty {
                               self?.lbNotiSelectUnit.isHidden = false
                           } else {
                               self?.lbNotiSelectUnit.isHidden = true
                           }
                       }
    }
    
    
    // 단원, 선택 체크시 최근 공부한 단원 체크여부 변경.
    func setRecentCheckData() {
        
        cbRecentUnitFirst.checkState = .unchecked
        cbRecentUnitSecond.checkState = .unchecked
        
        if selectedUnitInfoArr.isNotEmpty {
            if recentUnitsArr.isNotEmpty {
                for  idx in 0 ..< recentUnitsArr.count {
                    if self.selectedUnitInfoArr.contains(where: { $0.unit2Seq == recentUnitsArr[idx].unit2Seq }) {
                        if idx == 0 {
                            cbRecentUnitFirst.checkState = .checked
                        } else if idx == 1 {
                            cbRecentUnitSecond.checkState = .checked
                        }
                    }
                }
            }
        }
    }
    
    //단원정보 콜렉션뷰 목록 세팅.
    func setUnits () {
                
        unitInfoDic = OrderedDictionary(grouping: units) { (unit) -> Int in
            return unit.subjectSeq!
        }
        
        var highSchoolSection = [SubjectInfo]()
        var middleSchoolSection = [SubjectInfo]()
        var keyCount = 0
        for key in unitInfoDic.keys {
            keyCount += 1
            let subject = subjects.filter { $0.subjectSeq == key}
            switch keyCount {
            case 1...4:
                highSchoolSection.append(subject[0])
            default:
                middleSchoolSection.append(subject[0])
            }
        }
        
        self.subjectSection += middleSchoolSection
        self.subjectSection += highSchoolSection
        
        var highSchoolUnitArr = [[UnitInfo]]()
        var middleSchoolUnitArr = [[UnitInfo]]()
        var unitCount = 0
        for unit in unitInfoDic.values {
            unitCount += 1
            switch unitCount {
            case 1...4:
                highSchoolUnitArr.append(unit)
            default:
                middleSchoolUnitArr.append(unit)
            }
        }
        
        self.unitInfoArr += middleSchoolUnitArr
        self.unitInfoArr += highSchoolUnitArr
        
        setSVHeight()
    }
    
    
    func setSVHeight(){
        let height = cvUnitList.collectionViewLayout.collectionViewContentSize.height
        self.cvUnitHeight.constant = height
        self.view.layoutIfNeeded()
    }
    
    // MARK: - CollectionView
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        var cnt = 0
        
        if collectionView == cvUnitList {
            if isSearchState {
                cnt = 1
            } else {
                cnt = self.unitInfoArr.count
            }
            
        } else {
            cnt = 1
        }
        
        return cnt
    }
       
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
            case UICollectionView.elementKindSectionHeader:
            
                if collectionView == cvUnitList {
                    let headerView:UnitCollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "UnitCollectionReusableView", for: indexPath) as! UnitCollectionReusableView
                    
                    print (indexPath.section)
                    
                    let expandImage = indexPath.section == 0 ? R.image.iconSolidCheveronUp() : R.image.iconSolidCheveronDown()
                    
                    headerView.btExpand.tag = indexPath.section
                    headerView.btExpand.setImage(expandImage, for: .normal)
                    headerView.btExpand.addTarget(self, action: #selector(self.hideSection(sender:)), for: .touchUpInside)
                    
                    if let subjectSeq = subjectSection[/*exists:*/ indexPath.section].subjectSeq, let subjectName = subjectSection[/*exists:*/ indexPath.section].name {
                        
                        //let subjectName = subjectSeq < 3 ? "고등\(subjectName)" : subjectName
                        
                        var subject = ""
                        
                        switch subjectSeq {
                        case 1...2:
                            subject = "고등\(subjectName)"
                        case 10...15:
                            subject = "\(subjectName)"
                            subject.insert(contentsOf: "학수학 ", at: subject.index(subject.startIndex, offsetBy: 1))
                        default:
                            subject = "\(subjectName)"
                        }
                        
                        headerView.lbUnitTitle.text = subject
                    }
                    
                    return headerView
                } else {
                    
                    let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "UnitCollectionReusableView", for: indexPath)
                                       
                    return headerView
                }
            default:
                assert(false, "false")
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "UnitCollectionReusableView", for: indexPath)
                               
            return headerView
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {

        if collectionView == cvUnitList {
            if isSearchState {
                let width: CGFloat = 0
                let height: CGFloat = 0
                return CGSize(width: width, height: height)

            } else {
                let width: CGFloat = collectionView.frame.width
                let height: CGFloat = 44
                return CGSize(width: width, height: height)

            }
            
        } else {
            
            let width: CGFloat = 0
            let height: CGFloat = 0
            return CGSize(width: width, height: height)
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat =  156
        let height: CGFloat =  88
        
        let bounds = UIScreen.main.bounds
        let diviceWidth = bounds.size.width
        
        if collectionView == cvUnitList {


            let itemsPerRow: CGFloat = 2
            let widthPadding = 13 * (itemsPerRow + 1)
//            let cellWidth = (collectionView.frame.size.width - widthPadding) / itemsPerRow
            let cellWidth = (diviceWidth - widthPadding) / itemsPerRow
            print("cellWidth:: \(cellWidth)")
            let w = width - cellWidth
//            let cellHeight = height + ( -w * (height / width))
            let cellHeight = height
            
            recentCellHeight = cellHeight
            recentCellWidth = cellWidth
            return CGSize(width: cellWidth, height: cellHeight)

        } else {
            return CGSize(width: width, height: height)
        }
        
    }

    
    
    @objc
   private func hideSection(sender: UIButton) {
       let section = sender.tag
       print("section:\(section)")
       func indexPathsForSection() -> [IndexPath] {
           var indexPaths = [IndexPath]()
           
           for row in 0..<self.unitInfoArr[/*exists:*/section].count {
               indexPaths.append(IndexPath(row: row,
                                           section: section))
           }
           
           return indexPaths
       }
       
       if self.hiddenSections.contains(section) {
           sender.setImage(R.image.iconSolidCheveronUp(), for: .normal)
           self.hiddenSections.remove(section)
           self.cvUnitList.insertItems(at: indexPathsForSection())
       } else {
           sender.setImage(R.image.iconSolidCheveronDown(), for: .normal)
           self.hiddenSections.insert(section)
           self.cvUnitList.deleteItems(at: indexPathsForSection())
       }
       
       setSVHeight()
   }
    
    // 초기 접힘 상태.
    func initHideSection(){
        for section in 0 ..< unitInfoArr.count {
            if section > 0 {
                func indexPathsForSection() -> [IndexPath] {
                    var indexPaths = [IndexPath]()
                    
                    for row in 0..<self.unitInfoArr[/*exists:*/section].count {
                        indexPaths.append(IndexPath(row: row, section: section))
                    }
                    
                    return indexPaths
                }
                
                if self.hiddenSections.contains(section) {
                    self.hiddenSections.remove(section)
                    self.cvUnitList.insertItems(at: indexPathsForSection())
                } else {
                    self.hiddenSections.insert(section)
                    self.cvUnitList.deleteItems(at: indexPathsForSection())
                }
            }
        }
        
        setSVHeight()

    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
        var cnt = 0
        
        if collectionView == cvUnitList {
            
            if isSearchState {
                cnt = self.searchUnits.count
            } else {
                if self.hiddenSections.contains(section) {
                    cnt = 0
                } else {
                    cnt = self.unitInfoArr[/*exists:*/section].count
                }
            }
            
            
        } else if collectionView == cvSelectedUnit {
            cnt = self.selectedUnitInfoArr.count
        }
        
        
        return cnt
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell:UnitCollectionViewCell
        
        if collectionView == cvUnitList {
            cell = cvUnitList.dequeueReusableCell(withReuseIdentifier: "UnitCollectionViewCell", for: indexPath) as! UnitCollectionViewCell
            
            
            if isSearchState {
                
                
                cell.unitInfo = searchUnits[indexPath.row]
                cell.searchText = self.searchText
                
                if self.selectedUnitInfoArr.isEmpty {
                    cell.checkbox.checkState = .unchecked
                } else {
                    if self.selectedUnitInfoArr.contains(where: { $0.unit2Seq == cell.unitInfo.unit2Seq }) {
                        cell.checkbox.checkState = .checked
                    } else {
                        cell.checkbox.checkState = .unchecked
                    }
                }
                
                cell.checkCallback = { isCheck in
                    print("callback \(isCheck)")
                    print("indexPath.section \(indexPath.section), indexPath.row \(indexPath.row)")
                    
                    if isCheck {
                        self.selectedUnitInfoArr.append(self.searchUnits[indexPath.row])
                    } else {
                        self.selectedUnitInfoArr.removeObject(cell.unitInfo)
                    }
                    
                    self.cvSelectedUnit.reloadData()
                    
                    self.cvUnitList.performBatchUpdates { [weak self] in
                        
                        if self != nil {
                            if let arr = self?.selectedUnitInfoArr, arr.isEmpty  {
                                self?.lbNotiSelectUnit.isHidden = false
                            } else {
                                self?.lbNotiSelectUnit.isHidden = true
                            }
                            
                            self?.setRecentCheckData()

                        }
                    }
                        
                    self.cvSelectedUnit.performBatchUpdates { [weak self] in
                        if self != nil {
                            if let arr = self?.selectedUnitInfoArr, arr.isEmpty {
                                self?.lbNotiSelectUnit.isHidden = false
                            } else {
                                self?.lbNotiSelectUnit.isHidden = true
                            }
                            self?.setRecentCheckData()

                        }
                    }
                    
                }
                
                
                
                if let sText = searchText {
                                
                    // NSMutableAttributedString Type으로 바꾼 text를 저장
                    let attributedStr = NSMutableAttributedString(string: cell.lbUnit2.text!)

                    // text의 range 중에서 "Bonus"라는 글자는 UIColor를 blue로 변경
                    attributedStr.addAttribute(.foregroundColor, value: UIColor.blue, range: (cell.lbUnit2.text! as NSString).range(of: sText))
                    attributedStr.addAttribute(.font, value: UIFont.systemFont(ofSize: 14.0, weight: .bold), range: (cell.lbUnit2.text! as NSString).range(of: sText))
                    // 설정이 적용된 text를 label의 attributedText에 저장
                    cell.lbUnit2.attributedText = attributedStr
                    
                }
                
                
                
                
                
            } else {
                cell.unitInfo = unitInfoArr[indexPath.section][indexPath.row]
                
                if self.selectedUnitInfoArr.isEmpty {
                    cell.checkbox.checkState = .unchecked
                } else {
                    if self.selectedUnitInfoArr.contains(where: { $0.unit2Seq == cell.unitInfo.unit2Seq }) {
                        cell.checkbox.checkState = .checked
                    } else {
                        cell.checkbox.checkState = .unchecked
                    }
                }
                
                cell.checkCallback = { isCheck in
                    print("callback \(isCheck)")
                    print("indexPath.section \(indexPath.section), indexPath.row \(indexPath.row)")
                    
                    if isCheck {
                        self.selectedUnitInfoArr.append(self.unitInfoArr[indexPath.section][indexPath.row])
                    } else {
                        self.selectedUnitInfoArr.removeObject(cell.unitInfo)
                    }
                    
                    self.cvSelectedUnit.reloadData()
                    
                    self.cvUnitList.performBatchUpdates { [weak self] in
                        
                        if self != nil {
                            if let arr = self?.selectedUnitInfoArr, arr.isEmpty  {
                                self?.lbNotiSelectUnit.isHidden = false
                            } else {
                                self?.lbNotiSelectUnit.isHidden = true
                            }
                            
                            self?.setRecentCheckData()

                        }
                    }
                        
                    self.cvSelectedUnit.performBatchUpdates { [weak self] in
                        if self != nil {
                            if let arr = self?.selectedUnitInfoArr, arr.isEmpty {
                                self?.lbNotiSelectUnit.isHidden = false
                            } else {
                                self?.lbNotiSelectUnit.isHidden = true
                            }
                            self?.setRecentCheckData()

                        }
                    }
                    
                }
            }
            
            
            
            
        } else {
            cell = cvSelectedUnit.dequeueReusableCell(withReuseIdentifier: "UnitCollectionViewCell", for: indexPath) as! UnitCollectionViewCell
            
            if isSearchState {
                cell.unitInfo = selectedUnitInfoArr[indexPath.row]
                
                cell.checkbox.checkState = .checked
                
                cell.checkCallback = { isCheck in
                    print("callback \(isCheck)")
                    print("indexPath.section \(indexPath.section), indexPath.row \(indexPath.row)")
                    
                    if isCheck {
                        self.selectedUnitInfoArr.append(self.searchUnits[indexPath.row])
                    } else {
                        self.selectedUnitInfoArr.remove(at: indexPath.row)
                    }
                    
                    self.cvSelectedUnit.reloadData()
                    self.cvUnitList.reloadData()
                    
                    self.cvSelectedUnit.performBatchUpdates { [weak self] in
                        
                        if self != nil {
                            if let arr = self?.selectedUnitInfoArr, arr.isEmpty {
                                self?.lbNotiSelectUnit.isHidden = false
                            } else {
                                self?.lbNotiSelectUnit.isHidden = true
                            }
                            
                            self?.setRecentCheckData()
                        }
                    }
                }
                
                
                
                
            } else {
                cell.unitInfo = selectedUnitInfoArr[indexPath.row]
                
                cell.checkbox.checkState = .checked
                
                cell.checkCallback = { isCheck in
                    print("callback \(isCheck)")
                    print("indexPath.section \(indexPath.section), indexPath.row \(indexPath.row)")
                    
                    if isCheck {
                        self.selectedUnitInfoArr.append(self.unitInfoArr[indexPath.section][indexPath.row])
                    } else {
                        self.selectedUnitInfoArr.remove(at: indexPath.row)
                    }
                    
                    self.cvSelectedUnit.reloadData()
                    self.cvUnitList.reloadData()
                    
                    self.cvSelectedUnit.performBatchUpdates { [weak self] in
                        
                        if self != nil {
                            if let arr = self?.selectedUnitInfoArr, arr.isEmpty {
                                self?.lbNotiSelectUnit.isHidden = false
                            } else {
                                self?.lbNotiSelectUnit.isHidden = true
                            }
                            
                            self?.setRecentCheckData()
                        }
                    }
                }
            }
            
            
                    
        }
        cell.layoutIfNeeded()
        
        return cell
    }
    
    
    var isTyping = false
    
    
    // MARK: - Search
    @IBAction func tfSearchChanged(_ sender: UITextField) {
        if sender == tfSearch, let textCount = tfSearch.text?.count {

             if textCount == 0 {
                 self.searchText = nil
                 if isSearchState == true {
                     self.oldSearch = getAllIndexPathsInSection(section: 0)
                     isSearchState = false
                     cvUnitList.reloadData()
                     setSVHeight()
                 }

                 recentUnitView.isHidden = false
                 self.recentUnitViewHeight.constant = self.recentCellHeight + self.recentDefaultSpaceHeight

                 return
             } else {
                 
                 
                 if isSearchState == false {
                     isSearchState = true

                     self.cvUnitList.reloadData()

                     if let searchText = tfSearch.text {
                         searchUnit(sText:searchText)
                     }

                 } else {

                     if let searchText = tfSearch.text {

                         searchUnit(sText:searchText)
                     }

                 }

                 recentUnitView.isHidden = true
                 self.recentUnitViewHeight.constant = 0


             }

         }
    }
    
    func getAllIndexPathsInSection(section : Int) -> [IndexPath] {
        let count = searchUnits.count
        return (0..<count).map { IndexPath(row: $0, section: section) }
    }
    
    
    
    func searchUnit(sText:String){
        
        self.searchText = sText
        
        func indexPathsForSection() -> [IndexPath] {
           
            var indexPaths = [IndexPath]()
            var idx = 0
            for row in 0..<units.count {
                if units[row].unit2SName!.contains(sText) {
                    self.searchUnits.append(units[row])
                    indexPaths.append(IndexPath(row: idx, section: 0))
                    idx = idx+1
                }
            }
            return indexPaths
        }

        searchUnits.removeAll()
        
        let cnt = self.cvUnitList.numberOfItems(inSection: 0)
        var indexPaths = [IndexPath]()
        for row in 0..<cnt{
            indexPaths.append(IndexPath(row: row, section: 0))
        }
        self.cvUnitList.deleteItems(at: indexPaths)
        self.cvUnitList.insertItems(at:  indexPathsForSection())
        
        setSVHeight()
        
    }
    
    
    // MARK: - Input
       @objc func keyboardWillShow(notification: NSNotification) {
               
           guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
               
               // if keyboard size is not available for some reason, dont do anything
              return
           }
           
           var shouldMoveViewUp = false
           
           // if active text field is not nil
           if let activeTextField = activeTextField {
               
               let bottomOfTextField = activeTextField.convert(activeTextField.bounds, to: self.view).maxY;
               let topOfKeyboard = self.view.frame.height - keyboardSize.height
               
               if bottomOfTextField > topOfKeyboard {
                   shouldMoveViewUp = true
               }
           }
           
           if(shouldMoveViewUp) {
               self.view.frame.origin.y = 0 - keyboardSize.height
           }
           
//           willTransition(to: .shortForm)
           setLongHeight()
       }

       @objc func keyboardWillHide(notification: NSNotification) {
           self.view.frame.origin.y = 0
       }
       
       @objc func backgroundTap(_ sender: UITapGestureRecognizer) {
           // go through all of the textfield inside the view, and end editing thus resigning first responder
           // ie. it will trigger a keyboardWillHide notification
           self.view.endEditing(true)
       }
    
    
    @IBAction func resetSetting(_ sender: Any) {
        
        selectedUnitInfoArr.removeAll()
        cvSelectedUnit.reloadData()
        cvUnitList.reloadData()
        setRecentCheckData()
        
        if selectedUnitInfoArr.isEmpty {
            lbNotiSelectUnit.isHidden = false
        } else {
            lbNotiSelectUnit.isHidden = true
        }
    }
    
    
    @IBAction func applySetting(_ sender: Any) {
        
        selectUnitInfoCallback(selectedUnitInfoArr)
        dismiss(animated: true)
    }
    
    
    
    
    @IBAction func close(_ sender: Any) {
        
        dismiss(animated: true, completion: nil )
        
    }
    
    
    
    
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
            if let presentableController = viewControllerToPresent as? PanModalPresentable, let controller = presentableController as? UIViewController {
                controller.modalPresentationStyle = .custom
                controller.modalPresentationCapturesStatusBarAppearance = true
                controller.transitioningDelegate = PanModalPresentationDelegate.default
                super.present(controller, animated: flag, completion: completion)
                return
            }
            super.present(viewControllerToPresent, animated: flag, completion: completion)
        }
    
    
    
    // MARK: - PanModal
    var panScrollable: UIScrollView? {
            return svUnitList
        }
    var showDragIndicator: Bool {
            return false
        }
    
    var shortFormHeight: PanModalHeight {
        return isShortFormEnabled ? .contentHeight(300.0) : longFormHeight
    }
    
    // 펼쳐졌을 때
   var longFormHeight: PanModalHeight {
       // Top 부터 0 만큼 떨어지게 설정
       return .maxHeightWithTopInset(0)
   }
    
    var cornerRadius: CGFloat {
           return 10
       }

    var panModalBackgroundColor : UIColor {
        return R.color.puljaBlack60()!
    }
    
   

    var scrollIndicatorInsets: UIEdgeInsets {
        let bottomOffset = presentingViewController?.bottomLayoutGuide.length ?? 0
        return UIEdgeInsets(top: headerView.frame.size.height, left: 0, bottom: bottomOffset, right: 0)
    }

    var anchorModalToLongForm: Bool {
        return false
    }

    func shouldPrioritize(panModalGestureRecognizer: UIPanGestureRecognizer) -> Bool {
        let location = panModalGestureRecognizer.location(in: view)
        return headerView.frame.contains(location)
    }
    

    func willTransition(to state: PanModalPresentationController.PresentationState) {
        
        guard isShortFormEnabled, case .longForm = state else { return }

        isShortFormEnabled = false
//        bottomFillterViewHeight.isActive = false
        bottomFillterViewHeight.priority = UILayoutPriority(750)
//        bottomFillterViewHeight.isActive = true
        panModalSetNeedsLayoutUpdate()
    }
    
    func setLongHeight(){
        
        guard isShortFormEnabled else { return }
               
        isShortFormEnabled = false
        bottomFillterViewHeight.priority = UILayoutPriority(750)

        panModalSetNeedsLayoutUpdate()
        panModalTransition(to: .longForm)
        
    }
}


extension UnitSelectViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
            
        if textField == self.tfSearch {
            self.tfSearch.layer.borderWidth = 1.0
            self.tfSearch.layer.borderColor = UIColor.Purple.cgColor
        }
            
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeTextField = nil
        
        if textField == self.tfSearch {
                    self.tfSearch.layer.borderWidth = 1.0
                    self.tfSearch.layer.borderColor = UIColor.grey.cgColor
                }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        
        return true
    }
}
