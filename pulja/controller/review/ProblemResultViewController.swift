//
//  ProblemResultViewController.swift
//  pulja
//
//  Created by 김병헌 on 2022/02/19.
//

import UIKit

class ProblemResultViewController: PuljaBaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    var window: UIWindow?
    
    @IBOutlet weak var switchWrong: UISwitch!
    
    @IBOutlet weak var tvProblemList: UITableView!
    
    @IBOutlet weak var emptyView: UIView!
    
    @IBOutlet weak var btBack: UIButton!
    
    var isCorrect : String = ""
    
    var favoriteIndex : Int? = nil
    
//    var detailIndex: Int? = nil

    var cellDatas: [ReviewQuestion] = []
    
    var currSeq:Int? = nil
    
    var cunitSeq:Int? = nil
    
    var problemType:String? = nil
    
    var studyDay:String? = nil
    
    var userSeq: Int = 0
    var connectFromCoach: Bool = false
    
    var cameFromOnboard = false
    var problemSeq : Int?
    
    var resultType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btBack.tintColor = UIColor.Foggy
        
        self.tvProblemList.register(ProblemTableViewCell.nib(), forCellReuseIdentifier: "ProblemTableViewCell")

        switchWrong.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        switchWrong.isOn = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        
//        studyDay = "20210802"
        getQuestions()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.problemSeq == nil {
            if self.connectFromCoach {
                
                CommonUtil.coachTabbarController?.tabBar.isHidden = true
                CommonUtil.coachTabbarController?.bottomNavBar.isHidden = true
                
            } else {
                CommonUtil.tabbarController?.tabBar.isHidden = true
                CommonUtil.tabbarController?.bottomNavBar.isHidden = true
            }
        } else {
            CommonUtil.tabbarController?.tabBar.isHidden = true
            CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        if self.problemSeq == nil {
//            if self.connectFromCoach {
//
//                CommonUtil.coachTabbarController?.tabBar.isHidden = false
//                CommonUtil.coachTabbarController?.bottomNavBar.isHidden = false
//
//            } else {
//                CommonUtil.tabbarController?.tabBar.isHidden = false
//                CommonUtil.tabbarController?.bottomNavBar.isHidden = false
//            }
//        } else {
//            CommonUtil.tabbarController?.tabBar.isHidden = false
//            CommonUtil.tabbarController?.bottomNavBar.isHidden = false
//        }
    }
    
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        CommonUtil.tabbarController?.bottomNavBar.isHidden = false
//
//    }
    
    @IBAction func wrongChanged(_ sender: UISwitch) {
        
        if sender.isOn {
            isCorrect = "N"
        } else {
            isCorrect = ""
        }
        
        getQuestions()

    }
    
    
    func getQuestions() {
        LoadingView.show()
        
        if studyDay != nil {
            getTestQuestions()
        } else if currSeq != nil {
            getCurriculumQuestions()
        } else if problemSeq != nil {
            getAIQuestions()
        }
    }
    
    func getAIQuestions() {
        guard let problemSeq = self.problemSeq else { return }
        
        PreparationAPI.shared.weakCategoryResultQuestions(isCorrect: isCorrect, problemSeq: problemSeq, resultType: self.resultType).done {
            res in
            if let suc = res.success, suc == true
            {
                
                LoadingView.hide()
                
                guard let questions = res.data, questions.count > 0 else  {
                     
                    self.emptyView.isHidden = false
                    return
                }
                self.emptyView.isHidden = true
                
                self.cellDatas = questions
                self.tvProblemList.reloadData()
                
            }
        }
        
    }
    
    func getTestQuestions() {
        
        guard let studyDay = studyDay, var userseq = myInfo?.userSeq else {
            return
        }
        
        //코치가 접속한 경우
        if self.userSeq != 0 {
            userseq = Int64(self.userSeq)
        }

               
        StudyAPI.shared.testQuestions(studyDay: studyDay, isCorrect: isCorrect, userSeq: userseq).done { res in
           
           LoadingView.hide()
            
           guard let questions = res.data, questions.count > 0 else  {
               self.emptyView.isHidden = false
               return
           }
           self.emptyView.isHidden = true
           
           self.cellDatas = questions
           self.tvProblemList.reloadData()

       }
        
    }
    
    func getCurriculumQuestions(){
        
        guard let currSeq = currSeq , let cunitSeq = cunitSeq, var userseq = myInfo?.userSeq else {
                    
            return
                
        }
        
        //코치가 접속한 경우
        if self.userSeq != 0 {
            userseq = Int64(self.userSeq)
        }

        
        StudyAPI.shared.questions(currSeq: currSeq, cunitSeq: cunitSeq, isCorrect: isCorrect, userSeq: userseq).done { res in
            
            LoadingView.hide()
            guard let questions = res.data, questions.count > 0 else  {
                 
                self.emptyView.isHidden = false
                return
            }
            self.emptyView.isHidden = true
            
            self.cellDatas = questions
            self.tvProblemList.reloadData()

        }
    }
    
    
    @objc func reviewDetailFromButton(sender: UIButton){
        
//        detailIndex = sender.tag
        reviewDetail(detailIndex: sender.tag)
    }
    
    @objc func reviewDetailFromImage(sender: UIImageView){
        
//        detailIndex = sender.tag
        reviewDetail(detailIndex: sender.tag)
    }
    
    func reviewDetail(detailIndex: Int){
        
        let vc = R.storyboard.preparation.preparationAnswerViewController()!
        vc.cameFromAISolve = false
        vc.reviewQuestion = self.cellDatas[detailIndex]

        vc.detailBookMarkCallback = { result in
                    self.cellDatas[detailIndex].bookMarkSeq = result
                    self.tvProblemList.reloadData()
        }
        
//        let vc = R.storyboard.review.reviewContentViewController()! //.reviewDetailViewController()!
//        vc.hero.isEnabled = true
//        vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
//
//        vc.reviewQuestion = self.cellDatas[detailIndex]
//        vc.userSeq = self.userSeq
//
//        vc.detailBookMarkCallback = { result in
//            self.cellDatas[detailIndex].bookMarkSeq = result
//            self.tvProblemList.reloadData()
//        }
        
        self.window?.rootViewController = UINavigationController(rootViewController: self)
        window?.makeKeyAndVisible()
        
//        self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated: true, completion: nil)

    }
    
    @objc func favoriteChange(sender: UIButton){
        favoriteIndex  = sender.tag
        
        var req : BookMarkReq = BookMarkReq(bookMarkSeq: self.cellDatas[favoriteIndex!].bookMarkSeq
                                            , solveSeq: self.cellDatas[favoriteIndex!].solveSeq
                                            , userSeq: nil
                                            , problemType: self.cellDatas[favoriteIndex!].problemType)
        
        
        
        
        
        
        ReviewAPI.shared.bookMark(bookMarkReq: req).done { res in
            
            self.cellDatas[self.favoriteIndex!].bookMarkSeq = res.data?.bookMarkSeq
            
            self.tvProblemList.reloadData()
        }
        
        
    }
    
    
    
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 247
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellDatas.count
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let index = indexPath.row
        
        let cell:ProblemTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProblemTableViewCell", for: indexPath) as! ProblemTableViewCell
                    
        
        cell.question = cellDatas[indexPath.row]
        cell.btFavorite.tag = index
        cell.btFavorite.addTarget(self, action: #selector(favoriteChange(sender:)), for: .touchUpInside)
        cell.btDetail.tag = index
        cell.btDetail.addTarget(self, action: #selector(reviewDetailFromButton(sender:)), for: .touchUpInside)


        cell.btDetail2.tag = index
        cell.btDetail2.addTarget(self, action: #selector(reviewDetailFromButton(sender:)), for: .touchUpInside)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
            let startColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
            let endColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.88)
            cell.gradientView.applyGradient(isVertical: true, colorArray: [startColor, endColor])
        }
        
        return cell
    }
    
    
    @IBAction func btBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    

}
