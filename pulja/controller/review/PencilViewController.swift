//
//  PencilViewController.swift
//  pulja
//
//  Created by 김병헌 on 2022/08/22.
//

import UIKit
import PencilKit

class PencilViewController: UIViewController, PKCanvasViewDelegate {
    
    
    
    @IBOutlet weak var drawView: UIView!
    var pencilKitCanvas =  PKCanvas()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let canvasView = PKCanvasView(frame:drawView.bounds)
//        drawView.addSubview(canvasView)
//        drawView.tool = PKInkingTool(.pen ,color: .black, width: 30)
//        drawView.delegate = self
//        drawView.becomeFirstResponder()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
//        setUpCanvasView()
//        addPencilKit()
    }
     
//    func setUpCanvasView() {
//
//            // This function can be used for setting up the Canvas drawing policy
//    //        canvasView.drawingPolicy = PKCanvasViewDrawingPolicy(rawValue: <#T##UInt#>)
//
//            if let window = view.window, let toolPicker = PKToolPicker.shared(for: window) {
//                // As soon as user start integrating with the CanvasView, toolPicker will be visible
//                toolPicker.setVisible(true, forFirstResponder: drawView)
//                toolPicker.addObserver(drawView)
//
//            }
//        }

    @IBAction func close(_ sender: Any) {
        dismiss(animated: false)
    }
    
    
    override func viewDidLayoutSubviews() {
          super.viewDidLayoutSubviews()
          updateCanvasOrientation(with: view.bounds)
      }
    
    //MARK: - iOS override properties
        override var prefersHomeIndicatorAutoHidden: Bool {
              return true
          }
        
        override var prefersStatusBarHidden: Bool {
            return true;
        }
 
    private func addPencilKit() {
           view.backgroundColor = .clear
           
           pencilKitCanvas  = createPencilKitCanvas(frame: view.frame, delegate: self)
        pencilKitCanvas.backgroundColor = UIColor.grey.withAlphaComponent(0.2)
        drawView.addSubview(pencilKitCanvas)
         }
    
   
}

extension PencilViewController: PencilKitInterface { }

extension PencilViewController: PencilKitDelegate { }
