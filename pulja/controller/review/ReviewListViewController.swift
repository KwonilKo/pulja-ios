//
//  ReviewListViewController.swift
//  pulja
//
//  Created by 김병헌 on 2021/12/29.
//

import UIKit
import PanModal
import PromiseKit


import ChannelIOFront


class ReviewListViewController: PuljaBaseViewController, UITableViewDataSource, UITableViewDelegate, ReviewDelegate {
    
    var window: UIWindow?
    
    let headerMaxHeight:CGFloat = 67.0
    let headerMinHeight:CGFloat = 0.0
    
    
    var isPaging: Bool = false
    var hasNextPage: Bool = false
    var cellDatas: [ReviewQuestion] = []
    
    
    
    var selectedUnitInfoArr : [UnitInfo] = []  // 선택한 단원
    
    var units: [UnitInfo] = []
    var subjects: [SubjectInfo] = []
    
    var questionReq : ReviewQuestionReq?
    var endDate : String = ""
    var isCorrect : String?
    var page : Int = 0
    var currPage : Int = 0
    var startDate : String = ""
    var unit2Seqs : String?
    var userSeq : String?
    
    var listType = "R"
    
    var emptyText = ["아직 풀어본 문제가 없어요.", "를 눌러 중요한 문제는 따로 보관하세요.", "설정한 필터에 해당하는 문제가 없어요." ]
    
    var favoriteIndex : Int? = nil
    var detailIndex: Int? = nil
    
    var isSearchReload = false
    
    @IBOutlet weak var searchUnitView: UIView!
    
    @IBOutlet weak var lbSearchUnitCount: UILabel!
    
    @IBOutlet weak var searchPeriodView: UIView!
    
    @IBOutlet weak var ivSearchPeriodCheck: UIImageView!
    
    @IBOutlet weak var tvProblemList: UITableView!{
        didSet{
            tvProblemList.contentInset = UIEdgeInsets(top: headerMaxHeight, left: 0, bottom: 70, right: 0)
        }
    }
    
    @IBOutlet weak var emptyView: UIView!
    
    @IBOutlet weak var lbEmpty: UILabel!
    
    @IBOutlet weak var btEmpty: UIButton!
    
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var headerHeight: NSLayoutConstraint! {
        didSet {
            headerHeight.constant = headerMaxHeight
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //ab180 이벤트 보내기
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: "pulja2.0_review_enter", customs: ["userseq": userSeq, "user_id": userId]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "category: pulja2.0_review_enter", separator: " ")
                print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                print("[ABLog]", "user_id: \(userId)", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
        }

        // Do any additional setup after loading the view.
        self.tvProblemList.register(ProblemTableViewCell.nib(), forCellReuseIdentifier: "ProblemTableViewCell")
        self.tvProblemList.register(LoadingTableViewCell.nib(), forCellReuseIdentifier: "LoadingTableViewCell")
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.searchUnit(_:)))
        self.searchUnitView.addGestureRecognizer(gesture)

        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(self.searchPeriod(_:)))
        self.searchPeriodView.addGestureRecognizer(gesture2)

        
        let checkImage = R.image.iconSolidCheck()!.withRenderingMode(.alwaysTemplate)
        let margin:CGFloat = 3
        ivSearchPeriodCheck.image = checkImage.withInset(UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin))
        ivSearchPeriodCheck.tintColor = UIColor.white
        
        emptyView.isHidden = true
        
        questionReq = ReviewQuestionReq(endDate: nil, isCorrect: nil, page: page, startDate: nil, unit2Seqs: nil, userSeq: nil)
        
        
//        initChennelTalk()
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if isSearchReload == true || page == 0 {
            startPaging()
        }
        
    }
    
    func startPaging(){
        
        LoadingView.show()
        
        if selectedUnitInfoArr.isNotEmpty {
            var seqs = ""
            
            for unit in selectedUnitInfoArr {
                seqs += "\(unit.unit2Seq!),"
            }
            
            unit2Seqs  = seqs
            lbSearchUnitCount.isHidden = false
            lbSearchUnitCount.text = "\(selectedUnitInfoArr.count)"
            
        } else {
            unit2Seqs =  nil
            lbSearchUnitCount.isHidden = true
        }
        
        if startDate == "" && endDate == "" {
            ivSearchPeriodCheck.isHidden = true
        } else {
            ivSearchPeriodCheck.isHidden = false
        }
        
        paging()
    }
    
    @IBAction func resetFilter(_ sender: Any) {
        selectedUnitInfoArr.removeAll()
        startDate = ""
        endDate = ""
        startPaging()
    }
    
    func paging() {
        
        //ab180 이벤트 보내기
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: "pulja2.0_review_enter", customs: ["userseq": userSeq, "user_id": userId]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "category: pulja2.0_review_enter", separator: " ")
                print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                print("[ABLog]", "user_id: \(userId)", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
        }
        
        guard questionReq != nil else {
            LoadingView.hide()
            return
        }
        
        if page == 0 {
            self.cellDatas.removeAll()
        } else if  isSearchReload == true {
            page = 0
            self.cellDatas.removeAll()
        }
        
        isSearchReload = false

       
        questionReq?.page = page
        questionReq?.startDate = startDate
        questionReq?.endDate = endDate
        questionReq?.isCorrect = isCorrect
        questionReq?.unit2Seqs = unit2Seqs
        questionReq?.userSeq = userSeq
        
        
        
        ReviewAPI.shared.questions(reviewReq: questionReq!, listType: listType).done { response in

            LoadingView.hide()
            
            DispatchQueue.main.async {
                self.tvProblemList.tableFooterView = nil
            }
              
            guard let data = response.data else {
                self.setEmptyView()
                return
            }
              
            if data.first!  && data.empty! { //최초 호출결과 없으면
                self.setEmptyView()
            } else {
                self.emptyView.isHidden = true
            }
              
            if let content = data.content {
                self.cellDatas.append(contentsOf: content)
            }
              
            self.hasNextPage = !data.last!
            self.isPaging = false
                         
              
            self.tvProblemList.reloadData()
            
        }.catch { error in
            
            
        }
        
        
    }
    
    func setEmptyView() {
        if (self.unit2Seqs == "" || unit2Seqs == nil) && self.startDate == "" && self.endDate == "" {
           if self.listType == "R" {
               self.lbEmpty.text = self.emptyText[0]
           } else {
               let attributedString = NSMutableAttributedString(string: "")
               let imageAttachment = NSTextAttachment()
               imageAttachment.image = UIImage(named: R.image.iconOutlineStar.name)
               attributedString.append(NSAttributedString(attachment: imageAttachment))
               attributedString.append(NSAttributedString(string: self.emptyText[1]))
               self.lbEmpty.attributedText = attributedString
           }
           self.btEmpty.isHidden = true
       } else {
           self.lbEmpty.text = self.emptyText[2]
           self.btEmpty.isHidden = false
       }
       self.emptyView.isHidden = false
    }
    
    
    @objc func searchUnit(_ sender:UIGestureRecognizer){
        
        let vc = R.storyboard.review.unitSelectViewController()!
        vc.selectedUnitInfoArr = self.selectedUnitInfoArr
        vc.units = self.units
        vc.subjects = self.subjects
               
        vc.selectUnitInfoCallback = { result in
            self.isSearchReload = true
            self.selectedUnitInfoArr =  result
            
        }
        present(vc, animated: true, completion: nil)
//        presentPanModal(vc)
    }
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
       if let presentableController = viewControllerToPresent as? PanModalPresentable, let controller = presentableController as? UIViewController {
           controller.modalPresentationStyle = .custom
           controller.modalPresentationCapturesStatusBarAppearance = true
           controller.transitioningDelegate = PanModalPresentationDelegate.default
           super.present(controller, animated: flag, completion: completion)
           return
       }
       super.present(viewControllerToPresent, animated: flag, completion: completion)
   }
    
    
    @objc func searchPeriod(_ sender:UIGestureRecognizer){

        let vc = R.storyboard.review.periodSelectViewController()!

        vc.startDate = startDate
        vc.endDate = endDate
        
        vc.selectPeriodCallback = { result in
            self.isSearchReload = true
            self.startDate = result["startDate"]!
            self.endDate = result["endDate"]!
            
            if self.startDate == "" && self.endDate == "" {
                self.ivSearchPeriodCheck.isHidden = true
            } else {
                self.ivSearchPeriodCheck.isHidden = false
            }
        }
        present(vc, animated: true, completion: nil)
//        presentPanModal(vc)
    }
    
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        //테블릿인지 아닌지 확인
        switch UIDevice.current.userInterfaceIdiom {
            case .pad:
            let ratio = CGFloat(9.0 / 16.0)
            let height = (self.view.frame.size.width - 24.0) * ratio
                return 280
            default:
                return 247
        }
        
        return 247
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellDatas.count
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let index = indexPath.row
        
        let cell:ProblemTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProblemTableViewCell", for: indexPath) as! ProblemTableViewCell
        if cellDatas.count > index {
            cell.question = cellDatas[index]
        } else {
            print("out of range~~~~~~~")
        }
        
        cell.btFavorite.tag = index
        cell.btFavorite.addTarget(self, action: #selector(favoriteChange(sender:)), for: .touchUpInside)
        cell.btDetail.tag = index
        cell.btDetail.addTarget(self, action: #selector(reviewDetail(sender:)), for: .touchUpInside)

        cell.btDetail2.tag = index
        cell.btDetail2.addTarget(self, action: #selector(reviewDetail(sender:)), for: .touchUpInside)
        
        //테블릿인지 아닌지 확인
//        switch UIDevice.current.userInterfaceIdiom {
//            case .pad:
//            cell.gradientViewTopConstraint.constant = cell.frame.size.height - 107.0
//            default:
//                break
//        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
            let startColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
            let endColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.88)
            cell.gradientView.applyGradient(isVertical: true, colorArray: [startColor, endColor])
        }

        return cell
    }
    
    
    @objc func reviewDetail(sender: UIButton){
        
        guard self.cellDatas.count > sender.tag else { return }
        detailIndex = sender.tag
        
        let vc = R.storyboard.preparation.preparationAnswerViewController()!
        vc.cameFromAISolve = false
        vc.reviewQuestion = self.cellDatas[detailIndex!]
        let heroid = "problem\(detailIndex!)"
//        vc.heroId = heroid
        vc.detailBookMarkCallback = { result in
                    self.cellDatas[self.detailIndex!].bookMarkSeq = result
                    self.tvProblemList.reloadData()
        }
        
        
//        let vc = R.storyboard.review.reviewContentViewController()! //.reviewDetailViewController()!
//        vc.hero.isEnabled = true
//        vc.hero.modalAnimationType = .selectBy(presenting: .push(direction: .left), dismissing: .pull(direction: .right))
//        vc.reviewQuestion = self.cellDatas[detailIndex!]
//        let heroid = "problem\(detailIndex!)"
//        vc.heroId = heroid
//
//        vc.detailBookMarkCallback = { result in
//            self.cellDatas[self.detailIndex!].bookMarkSeq = result
//            self.tvProblemList.reloadData()
//        }
        
        
        self.window?.rootViewController = UINavigationController(rootViewController: self)
        window?.makeKeyAndVisible()
        
//        self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated: true, completion: nil)

    }
    
    @objc func favoriteChange(sender: UIButton){
        
        guard self.cellDatas.count > sender.tag else { return }
        favoriteIndex = sender.tag
        
        var req : BookMarkReq = BookMarkReq(bookMarkSeq: self.cellDatas[favoriteIndex!].bookMarkSeq
                                            , solveSeq: self.cellDatas[favoriteIndex!].solveSeq
                                            , userSeq: nil
                                            , problemType: self.cellDatas[favoriteIndex!].problemType)
        
        
        
        
        
        
        ReviewAPI.shared.bookMark(bookMarkReq: req).done { res in
            
//            if let bSeq = res.data?.bookMarkSeq {
//
//            }
            
            self.cellDatas[self.favoriteIndex!].bookMarkSeq = res.data?.bookMarkSeq
            
            self.tvProblemList.reloadData()
            
        }
        
        
    }
    
    
    
    
    
    
    
    
    
    
        
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        // paging
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let height = scrollView.frame.height
       
        if offsetY > (contentHeight - height - 100) {
            if isPaging == false && hasNextPage {
                beginPaging()
            }
        }
        
        

        
        // header
        if offsetY < 0 {
            let h = max(abs(offsetY), headerMinHeight)
            
            if h >= 67 {
                headerHeight.constant = 67
            } else {
                headerHeight.constant = h
            }
        } else {
            headerHeight.constant = headerMinHeight
        }
        
        let offset = -offsetY
//        print("offset::\(offset)")
        var percentage = (offset)/67
        if percentage < 1.0 {
            percentage = percentage*0.5
        }
        headerView.alpha = percentage
        
    }
    
    
    
    func beginPaging() {
        
        self.tvProblemList.tableFooterView = createSpinnerFooter()
            
        isPaging = true
        
        self.page = self.page + 1
        self.currPage = page
        self.paging()
        
    }
    
    
    private func createSpinnerFooter() -> UIView {
            
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 100))
        let spinner = UIActivityIndicatorView()

        spinner.center = footerView.center
        footerView.addSubview(spinner)
        spinner.startAnimating()
            
        return footerView
    }
    
    
    func wrongChange(isWrong: Bool) {
        if isWrong {
            isCorrect = "N"
        } else {
            isCorrect = nil
        }
        
        page = 0
        paging()
    }
    
    
    func scrollTop() {
        
        guard self.cellDatas.count > 0 else { return }
        // 1
        let topRow = IndexPath(row: 0, section: 0)
            
        // 2
        self.tvProblemList.scrollToRow(at: topRow, at: .top, animated: true)
    }
    
    func unitInfo(units: [UnitInfo]) {
        if self.units.isEmpty {
            self.units = units
        }
    }
    
    func subjectInfo(subjects: [SubjectInfo]) {
        if self.subjects.isEmpty {
            self.subjects = subjects
        }
    }

    
    func initChennelTalk(){
        
        let profile = Profile()
          .set(name: "김병헌")
//          .set(propertyKey: KEY, value: VALUE)

        let buttonOption = ChannelButtonOption.init(
            position: .right,
          xMargin: 16,
          yMargin: 23
        )

        let bootConfig = BootConfig.init(
          pluginKey: "77e29843-e342-46b1-bc18-7c679893018b",
          memberId: "kbh8809",
//          memberHash: "",
          profile: profile,
          channelButtonOption: buttonOption,
          hidePopup: false,
          trackDefaultEvent: true,
          language: .korean
        )
//        ChannelIO.boot(with: bootConfig)
        ChannelIO.boot(with: bootConfig) { (bootStatus, user) in
            if bootStatus == .success, let user = user {
                // success
                self.setQuestion()
            } else {
                // show failed reason from bootStatus
            }
        }
        
        
        
        
        
//        ChannelIO('updateUser', {
//                    profile: {
//                        question_id : question_id.toString()
//                    },
//                }, function onUpdateUser(error, user) {
//                    if (error) {
//                        console.error(error)
//                    } else {
//                        console.log('updateUser success', user)
//                    }
//                })
        
        // 질문 태그 채널톡 추가
//              ChannelIO('addTags', ['질문'], function onAddTags(error, user) {
//                  if (error) {
//                      console.error(error)
//                  } else {
//                      console.log('addTags success', user)
//                  }
//              })

        
        

    }
    
    func setQuestion(){
        var profileQuestion: [String:Any] = [:]
                
                // name
                profileQuestion["question_id"] = "108"
                let userData = UpdateUserParamBuilder()
                  .with(language: .korean)
                  .with(profile: profileQuestion)
                  .build()
                
                ChannelIO.updateUser(param:userData) { (error, user) in
                    if let user = user, error == nil {
                        self.setTags()
                     } else if let error = error {
                       // error, see error
                     }
                }
    }
    func setTags(){
        var testTags: [String] = []
                testTags.append("질문")

                ChannelIO.addTags(testTags) { (error, user) in
                  if let user = user {
                    // success
//                      ChannelIO.showMessenger()
                      ChannelIO.showChannelButton()
                  } else if let error = error {
                    // check reason
                  }
                }
    }
}
