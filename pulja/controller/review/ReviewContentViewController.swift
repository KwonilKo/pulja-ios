//
//  ReviewContentViewController.swift
//  pulja
//
//  Created by jeonghoonlee on 2022/02/17.
//

import UIKit
import WebKit
import ChannelIOFront
import Kingfisher
import AirBridge
import PencilKit

class ReviewContentViewController: PuljaBaseViewController , UIScrollViewDelegate, WKUIDelegate, WKScriptMessageHandler, WKNavigationDelegate, PKCanvasViewDelegate, PKToolPickerObserver{
    

    @IBOutlet weak var btBookMark: UIButton!
    @IBOutlet weak var ivProblem: UIImageView!
    @IBOutlet weak var problemIVHeight: NSLayoutConstraint!
    @IBOutlet weak var svProblemImage: UIScrollView!
    @IBOutlet weak var answerContents: UIView!
    
    @IBOutlet weak var svExplain: UIScrollView!
    @IBOutlet weak var ivExplain: UIImageView!
    @IBOutlet weak var wvExplain: WKWebView!
    @IBOutlet weak var explainView: UIView!
    @IBOutlet weak var explainVideoHeight: NSLayoutConstraint!
    @IBOutlet weak var explainIVHeight: NSLayoutConstraint!
    @IBOutlet weak var answerContentsHeight: NSLayoutConstraint!
    @IBOutlet weak var lbAnswer: UILabel!
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var bottomFloatingView: UIView!
    
    @IBOutlet weak var btPencil: UIButton!
    
    
    @IBOutlet weak var btPencilClear: UIButton!
    @IBOutlet weak var btPencilUndo: UIButton!
    @IBOutlet weak var btPencilRedo: UIButton!
    @IBOutlet weak var btPencilClose: UIButton!
    
    
    @IBOutlet weak var pencilToolView: UIView!
    @IBOutlet weak var pkCanvasView: PKCanvasView!
    var toolPicker: PKToolPicker!
    
    var reviewQuestion:ReviewQuestion?
    var reqParam:AnswerInfoReq?
    var answerInfo:AnswerInfo?
    
    var bookMarkSeq : Int?
    
    var isBookMarkChange = false
    
    var userSeq :Int = 0
    
    var detailBookMarkCallback : (_ result : Int?) -> Void = { _ in}
    
    
    var answerOrignHeight:CGFloat = 0
    var videoHeight:CGFloat = 0
    
    var heroId :String?
    
    var isExplainTab = false
    
    var isCountdown = false
    
    var dispatchWorkItem :DispatchWorkItem?
    
    var updateLayout: (() -> Void)?
    
    static let p = ResizingImageProcessor(referenceSize: .init(width: UIScreen.main.bounds.width*2, height: CGFloat.infinity), mode: .aspectFit)
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonUtil.tabbarController?.tabBar.isHidden = true
        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
          super.viewDidAppear(animated)

            initPencil()
      }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CommonUtil.tabbarController?.tabBar.isHidden = false
        CommonUtil.tabbarController?.bottomNavBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewInit()
        initData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        ChannelIO.shutdown()
    }
    func viewInit()
    {
        if (reviewQuestion?.bookMarkSeq) != nil {
            btBookMark.setBackgroundImage(R.image.iconSolidStar(), for: .normal)
        } else {
            btBookMark.setBackgroundImage(R.image.iconOutlineStar(), for: .normal)
        }
        
        bookMarkSeq = reviewQuestion?.bookMarkSeq
       
        answerContents.isHidden = true
        self.lbTitle.text = self.reviewQuestion?.categoryName ?? ""
        
        btPencil.isHidden = false

    }
    
    
    func initData() {
        
        if let reviewQuestion = reviewQuestion {
            
            let userseq :String? = (self.userSeq == 0) ? nil : String(self.userSeq)
            
            reqParam = AnswerInfoReq(problemType: reviewQuestion.problemType, questionId: reviewQuestion.questionId, solveSeq: reviewQuestion.solveSeq, userSeq: userseq)
            
            CommonAPI.shared.answerInfo(answerInfoReq: reqParam!).done { res in
                guard let data = res.data  else {
                    self.dismiss(animated: true)
                    return
                }
                self.answerInfo = data
                
                let url = URL(string:"\(Const.PROBLEM_IMAGE_URL)\(data.questionId ?? 0).jpg")
//                self.ivProblem.kf.indicatorType = .activity
//                self.ivProblem.kf.setImage(with: url,  options: [.transition(.fade(0.2))])
                
                
//                self.ivProblem.kf.setImage(with: url)
                
                
                self.ivProblem.kf.setImage(with: url) { r in
                    if case .success(let value) = r {
                        print("problem:: \(value.image.size.width) x \(value.image.size.height)")

                        self.updateLayout?()
                        
                        let ratio = value.image.size.width / value.image.size.height
                        let newHeight = self.ivProblem.frame.width / ratio
                        if self.problemIVHeight.constant < newHeight {
                            let addHeight =  newHeight - self.problemIVHeight.constant
                            self.problemIVHeight.constant = newHeight
                            self.answerContentsHeight.constant += addHeight
                            self.answerOrignHeight = self.answerContentsHeight.constant
                            self.view.layoutIfNeeded()
                        }
                        
                    } else {
                    }
                }
                
//                let height = self.ivProblem.contentClippingRect.height
//
//                if self.problemIVHeight.constant < height {
//                    self.problemIVHeight.constant = height
//                }
                
                
//                print("problem height::::\(height)")
                
                let explainUrl = URL(string:"\(Const.EXPLAIN_IMAGE_URL)\(data.questionId ?? 0).jpg")
                self.ivExplain.kf.indicatorType = .activity
//                self.ivExplain.kf.setImage(with: explainUrl,  options: [.transition(.fade(0.2))])
                
                
                self.ivExplain.kf.setImage(with: explainUrl, options: [.transition(.fade(1))]) { r in
                    if case .success(let value) = r {
                        print("\(value.image.size.width) x \(value.image.size.height)")

                        self.updateLayout?()
                        
                        let ratio = value.image.size.width / value.image.size.height
                        let newHeight = self.ivExplain.frame.width / ratio
                        if self.explainIVHeight.constant < newHeight {
                            let addHeight =  newHeight - self.explainIVHeight.constant
                            
                            self.explainIVHeight.constant = newHeight
                            self.answerContentsHeight.constant += addHeight
                            self.answerOrignHeight = self.answerContentsHeight.constant

                            
                            self.view.layoutIfNeeded()
                        }
                        
                    } else {
                    }
                }
                
                
                
//                let epheight = self.ivExplain.contentClippingRect.height
//
//                print("explain height::::\(epheight)")
//                if self.explainIVHeight.constant < epheight {
//                    self.explainIVHeight.constant = epheight
//                    let addHeight =  epheight - self.explainIVHeight.constant
//                    self.answerContentsHeight.constant += addHeight
//                    self.answerOrignHeight = self.answerContentsHeight.constant
//                }
                
//                let movieUrl = "\(Const.DOMAIN)/app_video?cunitSeq=265794&currSeq=3161&vlecSeq=0&videoKey=3x9MBXI9&cateType=&cateIndex=0&videoType=solve"
//                self.initExplainWebview(movieUrl: movieUrl)
                self.initExplainWebview(movieUrl: data.movieURL, movieCode: data.movieCode)
                
                if var userAnswer = data.userAnswer, var answerTxt = data.answerTxt  {
                    
                    var startIndex = answerTxt.startIndex
                    var endIndex = answerTxt.endIndex
                    if let index = answerTxt.index(of: "$$  {") {
                        
                        startIndex = answerTxt.index(index, offsetBy: 5)
                        print(index)
                    }

                    if let index = answerTxt.index(of: "}  $$") {
                        endIndex = index
                        print(index)
                    }
                    
                    let range = startIndex..<endIndex
                    answerTxt = String(answerTxt[range]) // .substring(with: range)
                    self.lbAnswer.text = "내가 선택했던 답: \(userAnswer)"
//                    self.lbAnswer.text = "내답: \(userAnswer)   정답: \(answerTxt)"
                }
                
            }

        }
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {

        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
    
    
    private var authCookie: HTTPCookie? {
             let cookie = HTTPCookie(properties: [
              .domain: Const.COOKIE_DOMAIN,
                 .path: "/",
                 .name: "JWT-TOKEN",
                 .value: CommonUtil.shared.getAccessToken(),
             ])
             return cookie
         }
             
     private var refreshCookie: HTTPCookie? {
         let cookie = HTTPCookie(properties: [
             .domain: Const.COOKIE_DOMAIN,
             .path: "/",
             .name: "REFRESH-TOKEN",
             .value: CommonUtil.shared.getRefreshToken(),
         ])
         return cookie
     }
    
    func initExplainWebview(movieUrl:String?, movieCode:String?){
        
        guard let code = movieCode, code != "" else {
            
            explainVideoHeight.constant = 0
            return
        }

        
        guard let url = URL(string:movieUrl!) else {
            
            explainVideoHeight.constant = 0
            return
        }
        
        
        let contentController = WKUserContentController()
        AirBridge.webInterface()?.inject(to: contentController, withWebToken: Const.AB_WEB_TOKEN)
        
        let script =
            "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport');" +
                "meta.setAttribute('content', 'width=device-width, initial-scale = 1.0, maximum-scale=5.0, user-scalable = yes');" +
        "document.getElementsByTagName('head')[0].appendChild(meta);"
        let userScript = WKUserScript(source: script,
                                      injectionTime: WKUserScriptInjectionTime.atDocumentEnd,
                                      forMainFrameOnly: true)
        contentController.addUserScript(userScript)
        
        // Bridge 등록
        contentController.add(self, name: "currentDuration")
        contentController.add(self, name: "pause")
       
        
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        
        let config = WKWebViewConfiguration()
        config.preferences = preferences
        config.userContentController = contentController
        
        self.wvExplain = WKWebView(frame: .zero, configuration: config)
        self.wvExplain?.frame = self.explainView.bounds
        self.wvExplain?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.explainView.addSubview(self.wvExplain!)

        
        let urlRequest = URLRequest(url: url)
        
        
        if let authCookie = authCookie, let refreshCookie = refreshCookie {
            self.wvExplain?.configuration.websiteDataStore.httpCookieStore.setCookie(authCookie, completionHandler: {
                self.wvExplain?.configuration.websiteDataStore.httpCookieStore.setCookie(refreshCookie, completionHandler: {
                    self.wvExplain?.load(urlRequest)
                })
            })
        }
        
        
//        self.wvExplain.load(urlRequest)
        self.wvExplain.uiDelegate = self
        self.wvExplain?.navigationDelegate = self

    }
    
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        
    }

    
    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        if scrollView == self.svProblemImage
        {
            return self.ivProblem
        }
        else //if scrollView == self.svExplain
        {
            return self.ivExplain
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btnQuestionPressed(_ sender: Any) {
        
        self.initChennelTalk()
        
    }
    
    @IBAction func setBookMark(_ sender: Any) {
        
        let req : BookMarkReq = BookMarkReq(bookMarkSeq: self.reviewQuestion?.bookMarkSeq
                                            , solveSeq: self.reviewQuestion?.solveSeq
                                            , userSeq: nil
                                            , problemType: self.reviewQuestion?.problemType)
       
        ReviewAPI.shared.bookMark(bookMarkReq: req).done { res in
           
            self.reviewQuestion?.bookMarkSeq = res.data?.bookMarkSeq
           
            if self.bookMarkSeq == res.data?.bookMarkSeq {
                self.isBookMarkChange = false
            } else {
                self.isBookMarkChange = true
            }

            if (self.reviewQuestion?.bookMarkSeq) != nil {
                self.btBookMark.setBackgroundImage(R.image.iconSolidStar(), for: .normal)
            } else {
                self.btBookMark.setBackgroundImage(R.image.iconOutlineStar(), for: .normal)
            }
           
        }.catch { error in
            
        }.finally {
            
        }
        
    }
    
    
    
    @IBAction func goBack(_ sender: Any) {
        if isBookMarkChange {
            detailBookMarkCallback (self.reviewQuestion?.bookMarkSeq )
        }
        
        if dispatchWorkItem != nil {
            dispatchWorkItem?.cancel()
        }
        
        if isExplainTab == true {
           actionLog(actionType:"solving_close")
        }
        dismiss(animated: true)
    }
    
    @IBAction func segmentSelected(_ sender: UISegmentedControl) {
        
        let idx = sender.selectedSegmentIndex
        
        dispatchWorkItem = DispatchWorkItem() { [self] in
            actionLog(actionType:"solve_explain_view")
        }
        
        if idx == 0
        {
            self.svProblemImage.isHidden = false
            self.answerContents.isHidden = true
            self.btPencil.isHidden = false
            dispatchWorkItem?.cancel()
            
            if isExplainTab == true {
                isExplainTab = false
                actionLog(actionType:"solving_close")
            }

        }
        else
        {
            self.svProblemImage.isHidden = true
            self.answerContents.isHidden = false
            self.btPencil.isHidden = true
            if isExplainTab == false {
                isExplainTab = true
                actionLog(actionType:"solving_enter")
            }
            
            if isCountdown == false {
                isCountdown = true
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: dispatchWorkItem!)
            
        }
    }
    
    
    func actionLog(actionType:String){
        
        guard let data = answerInfo  else {
            return
        }
        
        let req = UserActionReq(actionType: actionType, cunitSeq: data.cunitSeq, currSeq: data.currSeq, movieCode: data.movieCode, question_id: data.questionId, userAnswer: data.userAnswer, userSeq: data.userSeq, vlecSeq: 0)
        
        CommonAPI.shared.userAction(userActionReq: req).done { res in
            
        }.catch { err in
            
        }
        

    }
    
    
    func initChennelTalk(){
        
        guard let info = self.myInfo else { return }
        guard let userName = info.username else { return }
        guard let userId = info.hashUserSeq else { return }
        
        let profile = Profile()
          .set(name: userName)


        let buttonOption = ChannelButtonOption.init(
            position: .right,
          xMargin: 16,
          yMargin: 23
        )

        let bootConfig = BootConfig.init(
            pluginKey: Const.CHANNELTALK_PLUGIN_KEY, //Const.CHANNELTALK_PLUGIN_KEY2 채널톡 분리시 2로 바꾸면됨.
          memberId: userId,
//          memberHash: "",
          profile: profile,
          channelButtonOption: buttonOption,
          hidePopup: false,
          trackDefaultEvent: true,
          language: .korean
        )

        ChannelIO.boot(with: bootConfig) { (bootStatus, user) in
            if bootStatus == .success, let user = user {
                // success
                self.setQuestion()
            } else {
                // show failed reason from bootStatus
            }
        }
        

    }
    
    func setQuestion(){
        guard let questionId = self.reviewQuestion?.questionId else { return }
        var profileQuestion: [String:Any] = [:]
                
                // name
                profileQuestion["question_id"] = "\(questionId)"
                let userData = UpdateUserParamBuilder()
                  .with(language: .korean)
                  .with(profile: profileQuestion)
                  .build()
                
                ChannelIO.updateUser(param:userData) { (error, user) in
                    if let user = user, error == nil {
                        self.setTags()
                     } else if let error = error {
                       // error, see error
                     }
                }
    }

    func setTags(){
        var testTags: [String] = []
                testTags.append("질문")

                ChannelIO.addTags(testTags) { (error, user) in
                  if let user = user {
                    // success
                      
                      ChannelIO.showMessenger()
//                      ChannelIO.showChannelButton()
                  } else if let error = error {
                    // check reason
                  }
                }
    }
    
    
    
    func initPencil(){
        // Set up the canvas view with the first drawing from the data model.
        pkCanvasView.delegate = self
        pkCanvasView.alwaysBounceVertical = false
        pkCanvasView.isOpaque = false
        
        // Set up the tool picker
        if #available(iOS 14.0, *) {
            toolPicker = PKToolPicker()
        } else {
            // Set up the tool picker, using the window of our parent because our view has not
            // been added to a window yet.
            guard let window = view.window else { return }
            toolPicker = PKToolPicker.shared(for:   window)
          
        }
        
        toolPicker.setVisible(false, forFirstResponder: pkCanvasView)
        toolPicker.addObserver(pkCanvasView)
        toolPicker.addObserver(self)
//        updateLayout(for: toolPicker)
        pkCanvasView.becomeFirstResponder()
        
        if #available(iOS 14.0, *) {
            pkCanvasView.allowsFingerDrawing = true

        } else {
            pkCanvasView.allowsFingerDrawing = true
        }
       
    }
    
    
    @IBAction func popupPencilView(_ sender: Any) {
        pkCanvasView.isHidden = false
        pencilToolView.isHidden = false
        if #available(iOS 14.0, *) {
            toolPicker.setVisible(true, forFirstResponder: pkCanvasView)
        } else {
            guard let window = view.window else { return }
            toolPicker.setVisible(true, forFirstResponder: pkCanvasView)
        }
    }
    
    @IBAction func pencilClose(_ sender: Any) {
        pkCanvasView.isHidden = true
        pencilToolView.isHidden = true
        if #available(iOS 14.0, *) {
            toolPicker.setVisible(false, forFirstResponder: pkCanvasView)
        } else {
            guard let window = view.window else { return }
            toolPicker.setVisible(false, forFirstResponder: pkCanvasView)
        }
        
    }
    
    @IBAction func pencilCrear(_ sender: Any) {
        pkCanvasView.drawing = PKDrawing()
    }
    
}
