//
//  DatePickerViewController.swift
//  pulja
//
//  Created by 김병헌 on 2022/01/26.
//

import UIKit
import PanModal

class DatePickerViewController: UIViewController, PanModalPresentable {
    
    
    var selectDateCallback : (_ result : String ) -> Void = { _ in}

    var selectType = "s"  // s - start, e - end
    
    var changeDateValue = ""
    
    var startDate = ""
    var endDate = ""
    
    @IBOutlet weak var datePickerView: UIDatePicker!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        
        if selectType == "s" {
            
            if changeDateValue == "" {
                let sdt = Date().getSomeDayDate(day: -1)
//                datePickerView.date = sdt
                
                
                if startDate == "" {
                        changeDateValue = sdt.toString(.noDotdate)
                } else {
                    changeDateValue = startDate
                }
                
                datePickerView.date = changeDateValue.toDate(.noDotdate)
            }
            
           
            
            datePickerView.maximumDate = Date()

            
        } else if selectType == "e" {
            
            if changeDateValue == "" {
//                datePickerView.date = Date()
                
                if endDate == "" {
                    changeDateValue = Date().toString(.noDotdate)
                } else {
                    changeDateValue = endDate
                }
                datePickerView.date = changeDateValue.toDate(.noDotdate)
            }
            
            
            
            datePickerView.minimumDate = startDate.toDate(.noDotdate)
            
        }
        datePickerView.addTarget(self, action: #selector(onDidChangeDate(sender:)), for: .valueChanged)
        
    }
    
    
    @objc func onDidChangeDate(sender: UIDatePicker) {
       let dateFormatter: DateFormatter = DateFormatter()
       dateFormatter.dateFormat = "yyyyMMdd"
       
       let selectedDate: String = dateFormatter.string(from: sender.date)
        
        changeDateValue = selectedDate
        
    }
    
    
    @IBAction func confirmDate(_ sender: Any) {
        
        selectDateCallback(changeDateValue)
        dismiss(animated: true)
        
    }
    
    
    
    @IBAction func cancel(_ sender: Any) {
        
        dismiss(animated: true)
    }
    
    
    // MARK: - PanModal
    var panScrollable: UIScrollView?
    
    var showDragIndicator: Bool {
        return false
    }

    
    var shortFormHeight: PanModalHeight {
        return .contentHeight(256)
    }
    
    // 펼쳐졌을 때
   var longFormHeight: PanModalHeight {
       return .contentHeight(256)
   }
    
   

    var anchorModalToLongForm: Bool {
        return false
    }

    
    var cornerRadius: CGFloat {
        return 10
    }
    
    var panModalBackgroundColor : UIColor {
            return R.color.puljaBlack60()!
        }


}
