//
//  HomeViewController.swift
//  pulja
//
//  Created by 김병헌 on 2021/11/24.
//

import UIKit
import Kingfisher
import JTAppleCalendar
import FlareLane
import ChannelIOFront
import AirBridge

class HomeViewController: PuljaBaseViewController, UIScrollViewDelegate {
    
    
    @IBOutlet weak var statusBarViewBG: UIView!
    @IBOutlet weak var statusBarView: UIView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var btChat: BadgeButton!
    
    @IBOutlet weak var bgViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var zeroTimeView: UIView!
    
    
    @IBOutlet weak var ivTop: UIImageView!
    
    @IBOutlet weak var graView: UIView!
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var stackViewTop: UIStackView!
    
    @IBOutlet weak var lbTopMessage: UILabel!
    
    @IBOutlet weak var todayScheduleView: UIView!
    
    @IBOutlet weak var lbTodaySchedule: UIButton!
    
    @IBOutlet weak var btFree: UIButton!
    
    @IBOutlet weak var lbFreeInfo: UIButton!
    
    @IBOutlet weak var lbWeekInfo: UILabel!
    
    @IBOutlet weak var thisWeekStudyHourView: UIView!
    
    @IBOutlet weak var lbThisWeekStudyHour: UILabel!
    @IBOutlet weak var thisWeekStudyMinView: UIView!
    
    @IBOutlet weak var lbThisWeekStudyMin: UILabel!
    
    @IBOutlet weak var lbLastWeekTimeDiff: UIButton!
    
    
    @IBOutlet weak var constraintsCurrWeekHourY: NSLayoutConstraint!
    
    
    @IBOutlet weak var lbThisWeekStudyVideo: UILabel!
    @IBOutlet weak var lbThisWeekTotalVideo: UILabel!
    @IBOutlet weak var btVideoPercent: UIButton!

    @IBOutlet weak var lbVideoTitle: UILabel!
    
    @IBOutlet weak var btCateTooltip: UIButton!
    
    @IBOutlet weak var btProblemPercent: UIButton!
    @IBOutlet weak var lbThisWeekStudyProblem: UILabel!
    @IBOutlet weak var lbThisWeekTotalProblem: UILabel!
    
    @IBOutlet weak var lbProblemTitle: UILabel!
    
    
    @IBOutlet weak var KDCVideo: KDCircularProgress!
    
    
    @IBOutlet weak var KDCProblem: KDCircularProgress!
    
  
    @IBOutlet weak var svContents: UIScrollView!
    
    @IBOutlet weak var refreshView: UIView!
    
    // MARK: - middle banner
    
    @IBOutlet weak var middleBannerAreaViewHeight: NSLayoutConstraint!
    @IBOutlet weak var middleBannerAreaView: UIView!
    
    @IBOutlet weak var middleBannerView: UIView!
    
    @IBOutlet weak var ivMiddleBannerLogo: UIImageView!
    
    @IBOutlet weak var lbMiddleBannerUp: UILabel!
    
    @IBOutlet weak var lbMiddleBannerDown: UILabel!
    
    
    
    @IBOutlet weak var rcView: UIView!
    
    
    
    //calendar
    // MARK: - calendar
    
    @IBOutlet weak var calendarAreaViewHeight: NSLayoutConstraint!
    @IBOutlet weak var calendarAreaView: UIView!
    
    @IBOutlet weak var calendarView: JTACMonthView!
    
    @IBOutlet weak var lbWeekNumber: UILabel!
    
    
    @IBOutlet weak var btToday: UIButton!
    @IBOutlet weak var btPrev: UIButton!
    
    @IBOutlet weak var btNext: UIButton!
    
    @IBOutlet weak var scheduleView: UIView!
    
    @IBOutlet weak var lbEmptySchedule: UILabel!
    
    @IBOutlet weak var scheduleStackView: UIStackView!
    
    @IBOutlet weak var lbScheduleCoachingTag: UILabel!
    @IBOutlet weak var lbScheduleVideoTag: UILabel!
    @IBOutlet weak var lbScheduleProblemTag: UILabel!
    @IBOutlet weak var lbScheduleCoachingDesc: UILabel!
    @IBOutlet weak var lbScheduleVideoDesc: UILabel!
    @IBOutlet weak var lbScheduleProblemDesc: UILabel!
    @IBOutlet weak var ivScheduleCoachingCheckbox: UIImageView!
    @IBOutlet weak var ivScheduleVideoCheckbox: UIImageView!
    @IBOutlet weak var ivScheduleProblemCheckbox: UIImageView!
    
    
    
    var numberOfRows = 1
    let formatter = DateFormatter()
    var testCalendar = Calendar.current
    var generateInDates: InDateCellGeneration = .forFirstMonthOnly
    var generateOutDates: OutDateCellGeneration = .off
    var prePostVisibility: ((CellState, TodayJTACalendarViewCell?)->())?
    var hasStrictBoundaries = false
    let disabledColor = UIColor.lightGray
    let enabledColor = UIColor.blue
    var monthSize: MonthSize? = nil

    let arrDayString:[String] = ["", "일", "월", "화", "수", "목", "금", "토"]
    
    var currSchedule:[StudySchedule]? = nil
    
    var bgColor:UIColor? = nil
    
    
    var viewUserType = ""
    
    var homeInfo: Home? = nil
    
    var isFirst = true
    var isHomeViewReload = false
    
    var refreshControl : UIRefreshControl!
    
    var weekCurrDate = Date().toString(.noDotdate)
    
    var isCompleteCateStudy = true
    
    var inCompleteProblemSeq = 0
    
    var toolTipView : UIView?
    
    @objc func refresh()
      {
          // Code to refresh table view
          
          self.getHomeData()
          
          self.weekCurrDate = Date().toString(.noDotdate)
          
          calendarView.reloadData()
          calendarView.scrollToDate(getMonday(date:Date()))
          calendarView.selectDates([Date()])
          let today = Date().toString(.noDotdate)
          
          DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
              self.getStudySchedule(startDate: today, endDate: today)
          }
          
         
      }
    
    @objc func touchViewRemoveToolTip(sender: UIGestureRecognizer) {
           if let toolTip = self.toolTipView
           {
               DispatchQueue.main.async {
                   UIView.animate(withDuration: 0.3, animations: {
    
                       toolTip.alpha = 0
                       
                   }, completion: {finished in
                       
                       toolTip.removeFromSuperview()
                       self.toolTipView = nil
                   })
               }
           }
       
       
   }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // ?표 누른 경우 제거
        if let toolTip = self.toolTipView
        {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3, animations: {
 
                    toolTip.alpha = 0
                    
                }, completion: {finished in
                    
                    toolTip.removeFromSuperview()
                    //self.toolTipView = nil
                })
            }
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(touchViewRemoveToolTip(sender:)))
        self.bgView.addGestureRecognizer(tapGesture)
        
        super.viewWillAppear(animated)
        
        if  UserDefaults.standard.bool(forKey: "ud_event_seven_free_cancel") {
           let userDefault = UserDefaults.standard
          userDefault.setValue(false, forKey: "ud_event_seven_free_cancel")
          userDefault.synchronize()

            if let username = self.myInfo?.username {
                alert(title: "7일체험 신청 취소 완료", message: "\(username)님의\n7일체험 신청이 취소되었어요.", button: "확인")
            }
           
       }
        
        //다른 화면에서 payType이 바뀌었을 수 있으므로 화면보일때 다시 갱신.
        let payType = self.myInfo?.payType ?? Const.PayType.free.rawValue
        viewUserType = payType
//        self.setupMiddleBanner()
        
//        //여기 추가
//        if let data =  homeInfo, isFirst  {
//            isFirst = false
//            isHomeViewReload = true
//
//            self.view.isHidden = true
//            LoadingView.show()
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
//
//                self.setHomeData(data:data)
//                self.view.isHidden = false
//                LoadingView.hide()
//
//            }
//
//        } else {
//            self.view.isHidden = true
//            LoadingView.show()
//            getHomeData()
//        }
        
        
        
        
        
        self.setupMiddleBannerWeakCate()
        
        
        
    }
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        let event = ABInAppEvent()
        event?.setCategory(ABCategory.viewHome)
        event?.send()
       
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(touchViewRemoveToolTip(sender:)))
        self.bgView.addGestureRecognizer(tapGesture)
        
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        // Do any additional setup after loading the view.
        
        lbTopMessage.text = "오늘 학습 시간 설정하기"
        
        svContents.delegate = self
        
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.white
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        svContents.refreshControl = refreshControl
        
       
        if let data =  homeInfo, isFirst  {
            isFirst = false
            isHomeViewReload = true
            
            self.view.isHidden = true
            LoadingView.show()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                
                self.setHomeData(data:data)
                self.view.isHidden = false
                LoadingView.hide()
                
            }
            
        } else {
            self.view.isHidden = true
            LoadingView.show()
            getHomeData()
        }
        
        
        
        
        
        btFree.tintColor = UIColor.white
        
        
        let today = Date().toString(.noDotdate)
        
        getStudySchedule(startDate: today, endDate: today)
        
        btPrev.setImageTintColor(UIColor.puljaBlack)
        
        btPrev.addRightBorderWithColor(color:  UIColor.Foggy, width: 0.5)
        
        setupCalendarView()
        
        //Crashlytics test
//        let button = UIButton(type: .roundedRect)
//             button.frame = CGRect(x: 20, y: 50, width: 100, height: 30)
//             button.setTitle("Test Crash", for: [])
//             button.addTarget(self, action: #selector(self.crashButtonTapped(_:)), for: .touchUpInside)
//             view.addSubview(button)
        
        
        //remote push notification을 선택하여 앱이 실행된 경우 체크
        if let userInfo = CommonUtil.userInfoRemote
        {
            if let isFlareLane = userInfo["isFlareLane"] as? Int ,
               let url = userInfo["url"] as? String,
               isFlareLane > 0
            {
                var target = ""
                var txCurrSeq = "0"
                let components = URLComponents(string: url)
                let items = components?.queryItems ?? []
                for item in items
                {
                    if item.name == "target"
                    {
                        target = item.value ?? ""
                    }
                    else if item.name == "currSeq"
                    {
                        txCurrSeq = item.value ?? "0"
                    }
                }
                let currSeq = Int(txCurrSeq) ?? 0
                if let vcDetail = CommonUtil.tabbarController , target == "study" , currSeq > 0{
                    let userSeq = self.myInfo?.userSeq ?? 0
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    
                        vcDetail.selectedIndex = 1
                        vcDetail.bottomNavBar.selectedItem = vcDetail.bottomNavBar.items[1]
                        let vc = R.storyboard.study.myCurriculumViewController()!
                        vc.userSeq = Int(userSeq)
                        vc.currSeq = currSeq
                        if let navigationControlller = vcDetail.viewControllers?[1] as? UINavigationController , let rootVc = R.storyboard.study.studyCurriculumViewController()
                        {
                            navigationControlller.setViewControllers([rootVc], animated: false)
                            navigationControlller.pushViewController(vc, animated: true)
                        }
                        
                    }
                }
            }
            CommonUtil.userInfoRemote = nil
//            CommonUtil.shared.showAlert(title: "noti", message: payload.description, button: "ok", viewController: self)
        }
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
        
        
        pushCheck()
        
    }
    
    func pushCheck(){
        
        if (UIApplication.shared.delegate as? AppDelegate)?.isAppLaunchOptionsCheck == false {
            (UIApplication.shared.delegate as? AppDelegate)?.isAppLaunchOptionsCheck = true
            // 앱 종료 중일때 푸시로 진입한경우 처리.
            if let payload = (UIApplication.shared.delegate as? AppDelegate)?.appLaunchOptions as? [UIApplication.LaunchOptionsKey: Any]
            {
                if let userInfo = payload[.remoteNotification] as? [AnyHashable: Any] {
                    deepLinkOpen(userInfo: userInfo)
                }
            }

        }

    }
    
    deinit {
       NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: nil)
    }
            
    @objc func rotated() {
        self.weekCurrDate = Date().toString(.noDotdate)
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")

            // Do any additional setup after loading the view.
             calendarView.reloadData()
             calendarView.scrollToDate(getMonday(date:Date()))
             calendarView.selectDates([Date()])
             let today = Date().toString(.noDotdate)
             
             DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                 self.getStudySchedule(startDate: today, endDate: today)
                 
                 if let data =  self.homeInfo  {
                    self.setHomeData(data:data)
                }
             }

           

        } else {
            print("Portrait")
            
            calendarView.reloadData()
            calendarView.scrollToDate(getMonday(date:Date()))
            calendarView.selectDates([Date()])
            let today = Date().toString(.noDotdate)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.getStudySchedule(startDate: today, endDate: today)
                
                if let data =  self.homeInfo  {
                   self.setHomeData(data:data)
               }
            }

        }
        
    }
    
    
    
    
    
    //Crashlytics test
//    @IBAction func crashButtonTapped(_ sender: AnyObject) {
//        let numbers = [0]
//        let _ = numbers[1]
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        self.updateTotalUnreadMessageCountBadge()
        
        
        if  isFirst  == false , isHomeViewReload == false{
            print("getHomeData==")
            getHomeData()
        }
        self.checkUnreadMessage()
        
        isHomeViewReload = false
        
        
        CommonUtil.tabbarController?.tabBar.isHidden = false
        CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        
        let vc = R.storyboard.home.puljaWebViewController()!
    }
    
    func getHomeData() {
        
//        self.view.isHidden = true
//        LoadingView.show()
        HomeAPI.shared.home().done { res in
            guard let data = res.data else {
                return
            }
            self.setHomeData(data: data)
            
//            self.refreshControl = UIRefreshControl() ///바꿈
            
            
            
            self.refreshControl.endRefreshing()
        }.catch {error in
            
        }.finally {
            self.view.isHidden = false
            LoadingView.hide()
        }
    }
    
    func setHomeData(data:Home){
        
        self.homeInfo = data
        
        //PayType 추가.
        //p : paid user
        //f : free user
        //b : paid user before
        //c : coach
        if let userType = self.homeInfo?.appHomeView?.userType , let info = self.myInfo
        {
            info.setValue(userType, forKey: "payType")
            do {
                try self.container.viewContext.save()
            } catch {
                
            }
            
            let userSeq = info.userSeq
            FlareLane.setUserId(userId: "\(userSeq)")
        }
        
        
        // 홈 색상 이미지 세팅.
        if let homeView = data.appHomeView {
            self.bgColor = CommonUtil.shared.hexStringToUIColor(hex: homeView.viewColor!)
            

            self.downloadImage(with: homeView.viewImage!) { image in
                self.ivTop.image = image?.aspectFitImage(inRect: self.ivTop.frame)
                self.ivTop.contentMode = .right
            }
            self.statusBarViewBG.backgroundColor = self.bgColor
//            self.statusBarView.backgroundColor = self.bgColor
            
            if let msg = homeView.viewText {
                
                //이모지.
                let transform = "Any-Hex/Java"
                let output = msg.mutableCopy() as! NSMutableString
                CFStringTransform(output, nil, transform as NSString, true)
                
                //줄바꿈.
                let message = output.replacingOccurrences(of: "\\n", with: "\n")
                
                self.lbTopMessage.text = message
            }

            
//            self.view.backgroundColor = self.bgColor!
            self.refreshView.backgroundColor = self.bgColor!
            
            self.graView.applyGradient(isVertical: true, colorArray: [self.bgColor!, UIColor.OffWhite])
            self.bgView.backgroundColor = UIColor.OffWhite
//            self.bgView.applyGradient(isVertical: true, colorArray: [self.bgColor!, UIColor.OffWhite])
            
            if let userType = homeView.userType {
                self.viewUserType = userType
                if userType == "p" {
                    self.lbFreeInfo.isHidden = true
                    self.lbTodaySchedule.isHidden = false
                } else  {
                    self.lbFreeInfo.isHidden = false
                    self.lbTodaySchedule.isHidden = true
                }
                
//                self.setupMiddleBanner()
                DispatchQueue.main.async {
                    self.setupMiddleBannerWeakCate()
                }
                
            }
        }
        
        
        // 이번 주 학습
        if let todaySchedule = data.todayStudyScheduleText, todaySchedule != "" {
            self.lbTodaySchedule.setTitle("오늘 학습 시간 \(todaySchedule)", for: .normal)
        } else {
            self.lbTodaySchedule.setTitle("오늘 학습 시간 설정하기", for: .normal)
        }
        
        self.lbWeekInfo.text = data.weekInfo
        self.lbWeekInfo.sizeToFit()
        
        var thisWeekStudyTime = 0
        var lastWeekStudyTime = 0
        
        //취약유형 학습 완료 여부.
        if let weakCateStudyAmount = data.weakCateStudyAmount {
            isCompleteCateStudy = weakCateStudyAmount.completeCateStudy ?? true
            inCompleteProblemSeq = weakCateStudyAmount.problemSeq ?? 0
        }
        
        
        if self.viewUserType == "p" {
            
            thisWeekStudyTime = data.thisWeekStudyTime ?? 0
            lastWeekStudyTime = data.lastWeekStudyTime ?? 0

        } else {
            if let w = data.weakCateStudyAmount {
                thisWeekStudyTime = w.thisWeekStudyTime ?? 0
                lastWeekStudyTime = w.lastWeekStudyTime ?? 0

            }
        }
        
        if thisWeekStudyTime == 0 {
            
            self.thisWeekStudyHourView.isHidden = true
            self.thisWeekStudyMinView.isHidden = true
            self.zeroTimeView.isHidden = false
            
        } else {
            
            self.zeroTimeView.isHidden = true
            self.thisWeekStudyMinView.isHidden = false
            self.thisWeekStudyMinView.fadeTransition(0.1)

            if data.thisWeekStudyTime! > 3600 {
                self.constraintsCurrWeekHourY.constant = 37
                self.thisWeekStudyHourView.isHidden = false
                self.lbThisWeekStudyHour.text = CommonUtil.shared.convertDurationHour(duration: thisWeekStudyTime )
                self.lbThisWeekStudyHour.sizeToFit()
                self.lbThisWeekStudyMin.text = CommonUtil.shared.convertDurationMin(duration: thisWeekStudyTime )
                self.lbThisWeekStudyMin.sizeToFit()
            } else {
                self.lbThisWeekStudyMin.text = CommonUtil.shared.convertDurationMin(duration: thisWeekStudyTime )
                self.lbThisWeekStudyMin.sizeToFit()
                self.constraintsCurrWeekHourY.constant = 24
                self.thisWeekStudyHourView.isHidden = true
            }
            
        }

        
        // 지난 주 학습 시간 비교.
        if lastWeekStudyTime == 0 && thisWeekStudyTime == 0  {
            self.lbLastWeekTimeDiff.isHidden = true
        } else {
            
            let currWT = thisWeekStudyTime
            let lastWT = lastWeekStudyTime
                
            var diffTime =  currWT - lastWT
            
            if diffTime > 0 {
                self.lbLastWeekTimeDiff.setImage(R.image.iconSolidArrowSmUp(), for: .normal)
            } else {
                diffTime = diffTime * -1
                self.lbLastWeekTimeDiff.setImage(R.image.iconSolidArrowSmDown(), for: .normal)
            }
            
            self.lbLastWeekTimeDiff.setTitle(CommonUtil.shared.convertDurationString(duration: diffTime), for: .normal)
            self.lbLastWeekTimeDiff.sizeToFit()
            
            self.lbLastWeekTimeDiff.isHidden = false
            
        }
        
        if self.viewUserType == "p" {
            
            lbVideoTitle.text = "완료한 강의"
            btCateTooltip.isHidden = true
            btVideoPercent.isHidden = false
            
            lbProblemTitle.text = "완료한 문제"
            btProblemPercent.isHidden = false
            
            
            if let thisWSP = data.thisWeekStudyProblem
                        , let thisWSV = data.thisWeekStudyVideo
                        , let thisWTP = data.thisWeekTotalProblem
                        , let thisWTV = data.thisWeekTotalVideo {
                        
                        self.lbThisWeekStudyProblem.text = "\(thisWSP)"
                        self.lbThisWeekTotalProblem.text = "/\(thisWTP) 문제"
                        
                        self.lbThisWeekStudyVideo.text = "\(thisWSV)"
                        self.lbThisWeekTotalVideo.text = "/\(thisWTV) 강"
                        
                        let videoPercent = thisWTV > 0 ? thisWSV*100 / thisWTV : 0
                        let problemPercent = thisWTP > 0 ? thisWSP*100 / thisWTP : 0
                        
                        self.btVideoPercent.setTitle("\(videoPercent)%", for: .normal)
                        self.btProblemPercent.setTitle("\(problemPercent)%", for: .normal)
                        
                        let videoAngle = videoPercent * 360 / 100
                        let problemAngle = problemPercent * 360 / 100
                        
                        self.KDCVideo.animate(toAngle: Double(videoAngle), duration: 1, completion: nil);
                        self.KDCProblem.animate(toAngle: Double(problemAngle), duration: 1, completion: nil);
                    }
        } else {
            lbVideoTitle.text = "정복한 취약유형"
            btCateTooltip.isHidden = false
            btVideoPercent.isHidden = true
            
            lbProblemTitle.text = "평균 정답률"
            btProblemPercent.isHidden = true
            var completeCategoryCount = 0
            var weakCategoryCount = 0
            var correctRate = 0
            if let weakCateStudyAmount = data.weakCateStudyAmount {
                completeCategoryCount = weakCateStudyAmount.completeCategoryCount ?? 0
                weakCategoryCount = weakCateStudyAmount.weakCategoryCount ?? 0
                correctRate = weakCateStudyAmount.correctRate ?? 0

                self.lbThisWeekStudyVideo.text = "\(completeCategoryCount)"
                self.lbThisWeekTotalVideo.text = "개/\(weakCategoryCount)개"
                
                self.lbThisWeekStudyProblem.text = "\(correctRate)"
                
            } else {
                self.lbThisWeekStudyVideo.text = "0"
                self.lbThisWeekTotalVideo.text = "/0개"
                
                self.lbThisWeekStudyProblem.text = "0"
            }
            
            self.lbThisWeekTotalProblem.text = "%"
            
            
            let catePercent = weakCategoryCount > 0 ? completeCategoryCount*100 / weakCategoryCount : 0
            let problemPercent = correctRate
            
          
            let cateAngle = catePercent * 360 / 100
            let problemAngle = problemPercent * 360 / 100
          
            self.KDCVideo.animate(toAngle: Double(cateAngle), duration: 1, completion: nil);
            self.KDCProblem.animate(toAngle: Double(problemAngle), duration: 1, completion: nil);
            
            
            
        }
        
        
        
    }
    
    
    @IBAction func goTodayStudySetting(_ sender: Any) {
        
        
    }
    
    
    
    
    
    @IBAction func btnQuestionMarkPressed(_ sender: Any) {
        
        if let toolTip = self.toolTipView
        {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3, animations: {
 
                    toolTip.alpha = 0
                    
                }, completion: {finished in
                    
                    toolTip.removeFromSuperview()
                    self.toolTipView = nil
                })
            }
        }
        else
        {
            if let frame = self.rcView.getConvertedFrame(fromSubview: self.btCateTooltip)
            {
                let desc = "내가 또래 친구들보다 성취도가 높은\n취약 유형을 풀자 AI가 분석했어요."
                self.toolTipView =  CommonUtil.shared.addTooTip(frame: frame, desc: desc, parent: self.rcView, font: UIFont.fontWithName(type: .regular, size: 12), lineHeight: 14.0)
                
            }
        }
    
        
        
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        // paging
        let offsetY = scrollView.contentOffset.y
        let percentage = (offsetY)/104
        let chatColor =  1 - percentage < 0.7 ? 0.7 : 1 - percentage
        
        btChat.tintColor = UIColor(red: chatColor, green: chatColor, blue: chatColor, alpha: 1)
        
        self.statusBarView.backgroundColor = UIColor.OffWhite.withAlphaComponent(percentage)
        self.titleView.backgroundColor =  UIColor.OffWhite.withAlphaComponent(percentage)
    }
    
    
    
    
    func downloadImage(with urlString : String , imageCompletionHandler: @escaping (UIImage?) -> Void){
        
//        KingfisherManager.shared.cache.clearMemoryCache()
//           KingfisherManager.shared.cache.clearDiskCache()
//           KingfisherManager.shared.cache.cleanExpiredDiskCache()
        
            guard let url = URL.init(string: urlString) else {
                return  imageCompletionHandler(nil)
            }
            let resource = ImageResource(downloadURL: url)
        let processor = SVGProcessor(size: CGSize(width: 200, height: 200))
        KingfisherManager.shared.retrieveImage(with: resource, options: [.processor(processor)], progressBlock: nil) { result in
                switch result {
                case .success(let value):
                    imageCompletionHandler(value.image)
                case .failure:
                    imageCompletionHandler(nil)
                }
            }
        }
    
    
    func setupMiddleBannerWeakCate(){
            
            middleBannerAreaView.isHidden = false
           
            
            if viewUserType == Const.PayType.paid.rawValue {
                
                if let v = homeInfo?.incompleteVideo, let p = homeInfo?.incompleteProblem {
                    if v == 0 && p == 0 {
                        middleBannerAreaView.isHidden = true
                        middleBannerAreaViewHeight.constant = 0
                    } else {
                        middleBannerAreaView.isHidden = false
                        middleBannerAreaViewHeight.constant = 88
                        lbMiddleBannerUp.textColor = UIColor.puljaBlack
                        lbMiddleBannerUp.text = "강의 \(v)강, 문제 \(p)개가 밀렸어요."
                        lbMiddleBannerDown.textColor = UIColor.puljaBlack
                        lbMiddleBannerDown.text = "지금 바로 학습하러 가기"
                        middleBannerView.backgroundColor = R.color.yellow()
                        ivMiddleBannerLogo.image = R.image.todayWarnningCircle()
                        btFree.tintColor = UIColor.puljaBlack
                    }
                } else {
                    middleBannerAreaView.isHidden = true
                    middleBannerAreaViewHeight.constant = 0
                }
                calendarAreaView.isHidden = false
                calendarAreaViewHeight.constant = 262
                
            } else {
                
                //취약유형 학습 완료시
                if isCompleteCateStudy
                {
                    ivMiddleBannerLogo.image = R.image.iconRocket2()
                    lbMiddleBannerUp.text = "진단 한 번으로 내 취약 유형 받아봐요!"
                    lbMiddleBannerDown.text = "내 취약 유형 정복하러 가기"
                } else {
                    ivMiddleBannerLogo.image = R.image.iconFire()
                    lbMiddleBannerUp.text = "학습 데이터를 쌓을수록 분석이 더 정확해져요."
                    lbMiddleBannerDown.text = "내 취약 유형 정복하러 가기"
                }
                
                lbMiddleBannerUp.textColor = UIColor.black
                lbMiddleBannerDown.textColor = UIColor.black
                middleBannerView.backgroundColor = UIColor.white
                btFree.tintColor = UIColor.Dust
                
                calendarAreaView.isHidden = true
                calendarAreaViewHeight.constant = 0
                
                bgViewHeight.constant = 930 - 262
            }
        }
    
    func setupMiddleBanner(){
        
        middleBannerAreaView.isHidden = false
       
        
        if viewUserType == Const.PayType.paid.rawValue {
            
            if let v = homeInfo?.incompleteVideo, let p = homeInfo?.incompleteProblem {
                if v == 0 && p == 0 {
                    middleBannerAreaView.isHidden = true
                    middleBannerAreaViewHeight.constant = 0
                } else {
                    middleBannerAreaView.isHidden = false
                    middleBannerAreaViewHeight.constant = 88
                    lbMiddleBannerUp.textColor = UIColor.puljaBlack
                    lbMiddleBannerUp.text = "강의 \(v)강, 문제 \(p)개가 밀렸어요."
                    lbMiddleBannerDown.textColor = UIColor.puljaBlack
                    lbMiddleBannerDown.text = "지금 바로 학습하러 가기"
                    middleBannerView.backgroundColor = R.color.yellow()
                    ivMiddleBannerLogo.image = R.image.todayWarnningCircle()
                    btFree.tintColor = UIColor.puljaBlack
                }
            } else {
                middleBannerAreaView.isHidden = true
                middleBannerAreaViewHeight.constant = 0
            }
            
        } else if viewUserType == Const.PayType.free.rawValue {
            
            //진단평가를 봤다면 7일체험 배너
            if CommonUtil.hasDiagnosis
            {
                ivMiddleBannerLogo.image = R.image.iconRocket()
                lbMiddleBannerUp.text = "나만의 코치쌤과 내 취약 유형 정복하자!"
                lbMiddleBannerDown.text = "풀자 7일 체험 신청하기"
                lbMiddleBannerUp.textColor = UIColor.white
                lbMiddleBannerDown.textColor = UIColor.white
                
                middleBannerView.backgroundColor = R.color.green()
            }
            //진단을 본적이 없다면 진단평가 배너.
            else
            {
                ivMiddleBannerLogo.image = R.image.iconPencil()
                lbMiddleBannerUp.text = "나의 정확한 실력을 점검하고 싶다면?"
                lbMiddleBannerDown.text = "풀자 AI 진단평가로 확인하기"
                lbMiddleBannerUp.textColor = UIColor.white
                lbMiddleBannerDown.textColor = UIColor.white
                middleBannerView.backgroundColor = R.color.offRed()
            }
            
            
            
//            ivMiddleBannerLogo.image = R.image.todayLogoCircle()
//            lbMiddleBannerUp.text = "풀자가 처음이라면?"
//            lbMiddleBannerDown.text = "무료 체험으로 시작해보세요."
//            middleBannerView.backgroundColor =  UIColor.fromGradientWithDirection(.custom(95), frame: middleBannerView.frame,  colors: [UIColor.hex("553EFF"), UIColor.hex("FFD2D2")], locations:[0.22, 1])

            
        } else if viewUserType == Const.PayType.bought.rawValue {

            middleBannerAreaView.isHidden = true
//            ivMiddleBannerLogo.image = R.image.todayLogoCircle()
//            lbMiddleBannerUp.text = "나만의 커리큘럼으로 1:1 코칭까지"
//            lbMiddleBannerDown.text = "풀자와 수학 공부 시작하기"
//            middleBannerView.backgroundColor =  UIColor.fromGradientWithDirection(.custom(95), frame: middleBannerView.frame,  colors: [UIColor.hex("553EFF"), UIColor.hex("FFD2D2")], locations:[0.22, 1])
            
            
        } else if viewUserType == Const.PayType.week.rawValue{
            
            ivMiddleBannerLogo.image = R.image.iconPencil()
            lbMiddleBannerUp.textColor = UIColor.white
            lbMiddleBannerDown.textColor = UIColor.white
            lbMiddleBannerUp.text = "나의 정확한 실력을 점검하고 싶다면?"
            lbMiddleBannerDown.text = "풀자 AI 진단평가로 확인하기"
            middleBannerView.backgroundColor = R.color.offRed()
        }
        else {
            
            middleBannerAreaView.isHidden = true
            //여기에 들어올일은 없을 것 같으나 방어코드
            //진단평가를 봤다면 7일체험 배너
//            if CommonUtil.hasDiagnosis
//            {
//                ivMiddleBannerLogo.image = R.image.iconRocket()
//                lbMiddleBannerUp.text = "나만의 코치쌤과 내 취약 유형 정복하자!"
//                lbMiddleBannerDown.text = "풀자 7일 체험 신청하기"
//                middleBannerView.backgroundColor = R.color.green()
//            }
//            //진단을 본적이 없다면 진단평가 배너.
//            else
//            {
//                ivMiddleBannerLogo.image = R.image.iconRocket()
//                lbMiddleBannerUp.text = "나의 정확한 실력을 점검하고 싶다면?"
//                lbMiddleBannerDown.text = "풀자 AI 진단평가로 확인하기"
//                middleBannerView.backgroundColor = R.color.offRed()
//            }
            
//            ivMiddleBannerLogo.image = R.image.todayLogoCircle()
//            lbMiddleBannerUp.text = "풀자가 처음이라면?"
//            lbMiddleBannerDown.text = "무료 체험으로 시작해보세요."
////            middleBannerView.backgroundColor = R.color.purple()
//            middleBannerView.backgroundColor =  UIColor.fromGradientWithDirection(.custom(95), frame: middleBannerView.frame,  colors: [UIColor.hex("553EFF"), UIColor.hex("FFD2D2")], locations:[0.22, 1])
        }
        
    }
    
    func setGradient(){
        var view = middleBannerView!
        view.frame = CGRect(x: 0, y: 0, width: 351, height: 76)

        view.backgroundColor = .white


        var shadows = UIView()

        shadows.frame = view.frame

        shadows.clipsToBounds = false

        view.addSubview(shadows)


        let shadowPath0 = UIBezierPath(roundedRect: shadows.bounds, cornerRadius: 5)

        let layer0 = CALayer()

        layer0.shadowPath = shadowPath0.cgPath

        layer0.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor

        layer0.shadowOpacity = 1

        layer0.shadowRadius = 4

        layer0.shadowOffset = CGSize(width: 0, height: 1)

        layer0.bounds = shadows.bounds

        layer0.position = shadows.center

        shadows.layer.addSublayer(layer0)


        var shapes = UIView()

        shapes.frame = view.frame

        shapes.clipsToBounds = true

        view.addSubview(shapes)


        let layer1 = CAGradientLayer()

        layer1.colors = [

          UIColor(red: 0.333, green: 0.243, blue: 1, alpha: 1).cgColor,

          UIColor(red: 1, green: 0.825, blue: 0.825, alpha: 1).cgColor

        ]

        layer1.locations = [0.22, 1]

        layer1.startPoint = CGPoint(x: 0.25, y: 0.5)

        layer1.endPoint = CGPoint(x: 0.75, y: 0.5)

        layer1.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 1.09, b: 1.46, c: -1.46, d: 23.27, tx: 0.9, ty: -11.41))

        layer1.bounds = shapes.bounds.insetBy(dx: -0.5*shapes.bounds.size.width, dy: -0.5*shapes.bounds.size.height)

        layer1.position = shapes.center

        shapes.layer.addSublayer(layer1)


        shapes.layer.cornerRadius = 5

        shapes.layer.borderWidth = 0.25

        shapes.layer.borderColor = UIColor(red: 0.8, green: 0.808, blue: 0.816, alpha: 1).cgColor


        var parent = self.view!

        parent.addSubview(view)

        view.translatesAutoresizingMaskIntoConstraints = false

        view.widthAnchor.constraint(equalToConstant: 351).isActive = true

        view.heightAnchor.constraint(equalToConstant: 76).isActive = true

        view.leadingAnchor.constraint(equalTo: parent.leadingAnchor, constant: 12).isActive = true

        view.topAnchor.constraint(equalTo: parent.topAnchor, constant: 542).isActive = true
//        middleBannerView.addSubview(view)
    }
    
    func sendToWeakCatPractice(inCompleteProblemSeq : Int) {
        
        PreparationAPI.shared.weakCategoryStudyInfo(problemSeq: inCompleteProblemSeq)
            .done { res in
                if let suc = res.success, suc == true
                {
                    guard let data = res.data else { return }
                    guard let vCount = data.videoCount else { return }
                    guard let pCount = data.problemCount else { return }
                    guard let vResult = data.videoResult else { return }
                    
                    //유형 강의 학습을 해야 하는 경우
                    if vCount > 0
                    {

                    } else if pCount > 0 { //강의 학습 없이 문제 풀어야 하는 경우
                        let vc = R.storyboard.study.multipleChoiceViewController()!
                        vc.cameFromOnboard = false
                        vc.aidelegateController = self
                        vc.userResponseList = []
                        vc.problemSeq = inCompleteProblemSeq
                        vc.problemCnt = pCount
                        vc.videoCnt = 0
                        vc.isAIRecommend = true //무료기능 여부
                        vc.isAIDiagnosis = false //취약 유형 학습 이므로
                        vc.isLecture = false
                        
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                    
                    
                }
            }
        
    }
    
    @IBAction func middleBannerAction(_ sender: Any) {
        
        // 취약유형 학습 분기시.
        if viewUserType == Const.PayType.paid.rawValue {
            if let v = homeInfo?.incompleteVideo, let p = homeInfo?.incompleteProblem {
                if v > 0 || p > 0 {
                    goStudy()
                }
            }
        }
    }
    
    
    func goStudy(){
        if let tabBarVC = tabBarController as? TabbarController {
            tabBarVC.selectedIndex = 1
            tabBarVC.bottomNavBar.selectedItem = tabBarVC.bottomNavBar.items[1]
        }
    }
    
    func goFree(){
        if let url = URL(string: "https://www.pulja.co.kr/free_trial") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    func goPay(){
        let vc = R.storyboard.main.idpwdViewController()!
        vc.viewTitle = "수강권 결제"
        vc.url = "\(Const.DOMAIN)/subscription_landing"
        vc.modalPresentationStyle = .fullScreen

        self.present(vc, animated: true) {
        }

        
//        if let url = URL(string: "https://www.pulja.co.kr/subscription_landing") {
//                   UIApplication.shared.open(url, options: [:])
//               }
    }
    
    
    
    // MARK: - chat function
    func checkUnreadMessage()
    {
        let payType = self.myInfo?.payType ?? "f"
        
        if payType == "p"
        {
            CommonUtil.shared.checkNewMessage { hasNew in
                if hasNew > 0
                {
                    self.chatBadge(isMessage:true)
                }
                else
                {
                    self.chatBadge(isMessage:false)
                }
            }
        }
        else
        {
            self.chatBadge(isMessage:false)
        }
    }
    
    
    
    func updateTotalUnreadMessageCountBadge() {
        
    }
    
    func chatBadge(isMessage:Bool){
        if(isMessage){
            btChat.showBadge(blink: false, text: nil)
            btChat.badgeView?.backgroundColor = .red
        } else {
            btChat.hideBadge()
        }
    }
    
    
    @IBAction func goChat(_ sender: Any) {
    
    }
    
    // MARK: - calendar function
    func getStudySchedule(startDate:String, endDate:String){
        
        HomeAPI.shared.studySchedule(startDate: startDate, endDate: endDate).done { res in
            if let data = res.data {
                if ( data.count > 0){
                    self.lbWeekNumber.text = data[0].weekInfo
                    self.currSchedule = data
                    self.setStudyScheduleList()
                }
            }
        }.catch {
            err in
        }.finally {
        }
    }
    
    func setStudyScheduleWeekCount(startDate:String, endDate:String){
        
        HomeAPI.shared.studySchedule(startDate: startDate, endDate: endDate).done { res in
            if let data = res.data {
                if ( data.count > 0){
                    self.lbWeekNumber.text = data[0].weekInfo
                }
            }
        }
    }
    
    
    func setStudyScheduleList(){
        
        guard currSchedule != nil ,  let schedule = currSchedule?[0] else {
            
            return
        }
        
        if schedule.videoSchedule == "", schedule.problemSchedule == "", schedule.coachingSchedule == "" {
            
            scheduleStackView.isHidden = true
            
        } else {
            
            scheduleStackView.isHidden = false
            
            
            if schedule.videoSchedule != "" {
                lbScheduleVideoTag.borderColor = UIColor.Purple
                lbScheduleVideoTag.textColor = UIColor.Purple
                lbScheduleVideoDesc.textColor = UIColor.puljaBlack
                lbScheduleVideoDesc.text = schedule.videoSchedule
                ivScheduleVideoCheckbox.isHidden = false
                if schedule.videoComplete == true {
                    ivScheduleVideoCheckbox.image = R.image.iconSolidCheckSm()
                } else {
                    ivScheduleVideoCheckbox.image = R.image.iconOutlineCheckSm()
                }
                
            } else {
                lbScheduleVideoTag.borderColor = UIColor.Foggy
                lbScheduleVideoTag.textColor = UIColor.Foggy
                
                lbScheduleVideoDesc.textColor = UIColor.Foggy
                lbScheduleVideoDesc.text = "학습할 강의가 없어요."
                ivScheduleVideoCheckbox.isHidden = true
            }
            
            
            if schedule.coachingSchedule != "" {
                lbScheduleCoachingTag.borderColor = UIColor.OffRed
                lbScheduleCoachingTag.textColor = UIColor.OffRed
                lbScheduleCoachingDesc.textColor = UIColor.puljaBlack
                lbScheduleCoachingDesc.text = schedule.coachingSchedule
                ivScheduleCoachingCheckbox.isHidden = false
                if schedule.coachingComplete == true {
                    ivScheduleCoachingCheckbox.image = R.image.iconSolidCheckSm()
                } else {
                    ivScheduleCoachingCheckbox.image = R.image.iconOutlineCheckSm()
                }
                
            } else {
                lbScheduleCoachingTag.borderColor = UIColor.Foggy
                lbScheduleCoachingTag.textColor = UIColor.Foggy
                
                lbScheduleCoachingDesc.textColor = UIColor.Foggy
                lbScheduleCoachingDesc.text = "코칭 일정이 없어요."
                ivScheduleCoachingCheckbox.isHidden = true
            }
            
            
            if schedule.problemSchedule != "" {
                lbScheduleProblemTag.borderColor = UIColor.Green
                lbScheduleProblemTag.textColor = UIColor.Green
                lbScheduleProblemDesc.textColor = UIColor.puljaBlack
                lbScheduleProblemDesc.text = schedule.problemSchedule
                ivScheduleProblemCheckbox.isHidden = false
                if schedule.problemComplete == true {
                    ivScheduleProblemCheckbox.image = R.image.iconSolidCheckSm()
                } else {
                    ivScheduleProblemCheckbox.image = R.image.iconOutlineCheckSm()
                }
                
            } else {
                lbScheduleProblemTag.borderColor = UIColor.Foggy
                lbScheduleProblemTag.textColor = UIColor.Foggy
                
                lbScheduleProblemDesc.textColor = UIColor.Foggy
                lbScheduleProblemDesc.text = "학습할 문제가 없어요."
                ivScheduleProblemCheckbox.isHidden = true
            }

            
            
        }
        
            
        
    }
    
    
    func setupCalendarView(){
        //        calendarView.allowsMultipleSelection = true
        
//        self.calendarView.visibleDates {[unowned self] (visibleDates: DateSegmentInfo) in
//            self.setupViewsOfCalendar(from: visibleDates)
//        }
//
        calendarView.scrollingMode = .stopAtEachCalendarFrame
        calendarView.showsHorizontalScrollIndicator = false
        
        
        calendarView.scrollToDate(getMonday(date:Date()))
        calendarView.selectDates([Date()])
        


    }
    
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        guard let startDate = visibleDates.monthDates.first?.date else {
            return
        }
        let month = testCalendar.dateComponents([.month], from: startDate).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        // 0 indexed array
        let year = testCalendar.component(.year, from: startDate)
//        monthLabel.text = monthName + " " + String(year)
    }
    
    
    func handleCellConfiguration(cell: JTACDayCell?, cellState: CellState) {
           handleCellSelection(view: cell, cellState: cellState)
           handleCellTextColor(view: cell, cellState: cellState)
   //        prePostVisibility?(cellState, cell as? CellView)
       }
    
    // Function to handle the calendar selection
    func handleCellSelection(view: JTACDayCell?, cellState: CellState) {
        guard let myCustomCell = view as? TodayJTACalendarViewCell else {return }
        
        if cellState.isSelected {
//            myCustomCell.lbDay.text = "오늘"
            myCustomCell.lbDay.textColor = UIColor.puljaBlack
            myCustomCell.lbDate.borderColor = UIColor.purpleishBlue
            myCustomCell.lbDate.textColor = UIColor.purpleishBlue
            myCustomCell.lbDate.font = UIFont.boldSystemFont(ofSize: 14)
            
            let selectDate = cellState.date.toString(.noDotdate)
            
            let startmon = formatter.string(from: self.getMonday(date: weekCurrDate.toDate(.noDotdate)))
            let endmon = formatter.string(from: self.getMonday(date: weekCurrDate.toDate(.noDotdate)).changeDays(addDays: 6))
            
            if(selectDate >= startmon && endmon <= endmon) {
                getStudySchedule(startDate:selectDate, endDate:selectDate)
            }
            
        } else {
            myCustomCell.lbDay.textColor = UIColor.Foggy
            myCustomCell.lbDate.borderColor = UIColor.Foggy
            myCustomCell.lbDate.textColor = UIColor.Foggy
            myCustomCell.lbDate.font = UIFont.systemFont(ofSize: 14)

        }
    }
    
    // Function to handle the text color of the calendar
   func handleCellTextColor(view: JTACDayCell?, cellState: CellState) {
       guard let myCustomCell = view as? TodayJTACalendarViewCell  else {
           return
       }
               
       if cellState.isSelected {
//               myCustomCell.dayLabel.textColor = .white
       } else {
//               myCustomCell.dayLabel.textColor = .black
       }
       
//           myCustomCell.isHidden = cellState.dateBelongsTo != .thisMonth
       
   }

    
    func getMonday(date:Date) -> Date {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM-dd-e-EEEE"    //e는 1~7(sun~sat)

        let day = formatter.string(from:date)
        let today = day.components(separatedBy: "-") // [0] = MMM, [1] = dd, [2] = e(1), [3] = EEEE(Sunday)
        guard let interval = Double(today[2]) else{ return date }
        
        var dateComponent = DateComponents()
        
        var monday = 2
        if interval == 1 {
            monday = -6
        } else {
            monday = Int(2 - interval)
        }
        
        dateComponent.day = Int(monday)
        let mondayDate = Calendar.current.date(byAdding: dateComponent, to: date)

        return mondayDate!
    }
    
    
    @IBAction func nextWeek(_ sender: Any) {
        
        
        var dateComponent = DateComponents()
            dateComponent.day = 7
        self.weekCurrDate = (Calendar.current.date(byAdding: dateComponent, to: weekCurrDate.toDate(.noDotdate))?.toString(.noDotdate))!
        self.calendarView.scrollToSegment(.next)
    }
    
    
    @IBAction func prevWeek(_ sender: Any) {
        
        var dateComponent = DateComponents()
            dateComponent.day = -7
        self.weekCurrDate = (Calendar.current.date(byAdding: dateComponent, to: weekCurrDate.toDate(.noDotdate))?.toString(.noDotdate))!
        
        self.calendarView.scrollToSegment(.previous)
    }
    
    @IBAction func currWeek(_ sender: Any) {
        self.weekCurrDate = Date().toString(.noDotdate)
        self.calendarView.scrollToDate(getMonday(date:Date()))
    }
    
    
}


// MARK: - calendar extension
extension HomeViewController: JTACMonthViewDelegate, JTACMonthViewDataSource {
    
    
    func configureCalendar(_ calendar: JTACMonthView) -> ConfigurationParameters {
        
        
        let currentDate = Date()

        let monthsToEnd = 2
        var dateComponent = DateComponents()
        dateComponent.month = monthsToEnd
        var futureDate = Calendar.current.date(byAdding: dateComponent, to: currentDate)
        
        futureDate = getMonday(date: futureDate!)
        
        let monthsToStart = -2
        var dateComponent2 = DateComponents()
        dateComponent2.month = monthsToStart
        var pastDate = Calendar.current.date(byAdding: dateComponent2, to: currentDate)
        
        pastDate = getMonday(date: pastDate!)
        
        
            
        formatter.dateFormat = "yyyyMMdd"
        formatter.timeZone = TimeZone(abbreviation: "KST")
        formatter.locale = Locale(identifier: "ko_KR")
        
        
        
        let pDate = pastDate!.toString(.noDotdate)
            
        let fDate = futureDate!.toString(.noDotdate)
            
        let startDate = formatter.date(from: pDate)!
        let endDate = formatter.date(from: fDate)!
        
            
        let parameters = ConfigurationParameters(startDate: pastDate!,
                                                     endDate: futureDate!,
                                                     numberOfRows: numberOfRows,
                                                     calendar: testCalendar,
                                                     generateInDates: generateInDates,
                                                     generateOutDates: generateOutDates,
                                                     firstDayOfWeek: .monday,
                                                     hasStrictBoundaries: hasStrictBoundaries)
            
        return parameters
        
    }
        
    func configureVisibleCell(myCustomCell: TodayJTACalendarViewCell, cellState: CellState, date: Date, indexPath: IndexPath) {
        
        myCustomCell.lbDate.text = cellState.text
        myCustomCell.lbDay.text =  arrDayString[cellState.day.rawValue]
        
        if testCalendar.isDateInToday(date) {
            myCustomCell.lbDay.text = "오늘"
            myCustomCell.lbDay.textColor = UIColor.puljaBlack
            myCustomCell.lbDate.borderColor = UIColor.purpleishBlue
            myCustomCell.lbDate.textColor = UIColor.purpleishBlue
            myCustomCell.lbDate.font = UIFont.boldSystemFont(ofSize: 14)
        } else {
            myCustomCell.lbDay.textColor = UIColor.Foggy
            myCustomCell.lbDate.borderColor = UIColor.Foggy
            myCustomCell.lbDate.textColor = UIColor.Foggy
            myCustomCell.lbDate.font = UIFont.systemFont(ofSize: 14)
        }
        
        handleCellConfiguration(cell: myCustomCell, cellState: cellState)
        
        
        if cellState.text == "1" {
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM"
            let month = formatter.string(from: date)
//                myCustomCell.monthLabel.text = "\(month) \(cellState.text)"
        } else {
//                myCustomCell.monthLabel.text = ""
        }
    }
    
    func calendar(_ calendar: JTACMonthView, willDisplay cell: JTACDayCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        // This function should have the same code as the cellForItemAt function
        let myCustomCell = cell as! TodayJTACalendarViewCell
        configureVisibleCell(myCustomCell: myCustomCell, cellState: cellState, date: date, indexPath: indexPath)
    }
    
    func calendar(_ calendar: JTACMonthView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTACDayCell {
        let myCustomCell = calendar.dequeueReusableCell(withReuseIdentifier: "CalendarWeekViewCell", for: indexPath) as! TodayJTACalendarViewCell
        configureVisibleCell(myCustomCell: myCustomCell, cellState: cellState, date: date, indexPath: indexPath)
        return myCustomCell
    }

    func calendar(_ calendar: JTACMonthView, didDeselectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        handleCellConfiguration(cell: cell, cellState: cellState)
    }

    func calendar(_ calendar: JTACMonthView, didSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        handleCellConfiguration(cell: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTACMonthView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        
        print("After: \(calendar.currentSection()), visibleDates:\(visibleDates.monthDates.first?.date)")
        
        guard let startDate = visibleDates.monthDates.first?.date else {
            return
        }
        
        
//        var dateComponent = DateComponents()
//        dateComponent.day = 1
//        let mondayDate = Calendar.current.date(byAdding: dateComponent, to: startDate)
//        if let currDate = mondayDate?.toString(.noDotdate) {
//            setStudyScheduleWeekCount(startDate: currDate, endDate: currDate)
//        }
        
        print("weekCurrDate \(weekCurrDate)")
        setStudyScheduleWeekCount(startDate: self.weekCurrDate, endDate: self.weekCurrDate)
        
        
        
    }
    
    func calendar(_ calendar: JTACMonthView, willScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        setupViewsOfCalendar(from: visibleDates)
    }
    
    
    func calendarSizeForMonths(_ calendar: JTACMonthView?) -> MonthSize? {
        return monthSize
    }
    
    
    func initChennelTalk(){
        
        guard let info = self.myInfo else { return }
        guard let userName = info.username else { return }
        guard let userId = info.hashUserSeq else { return }
        
        let profile = Profile()
          .set(name: userName)


        let buttonOption = ChannelButtonOption.init(
            position: .right,
          xMargin: 16,
          yMargin: 23
        )

        let bootConfig = BootConfig.init(
            pluginKey: Const.CHANNELTALK_PLUGIN_KEY,
          memberId: userId,
//          memberHash: "",
          profile: profile,
          channelButtonOption: buttonOption,
          hidePopup: false,
          trackDefaultEvent: true,
          language: .korean
        )

        ChannelIO.boot(with: bootConfig) { (bootStatus, user) in
            if bootStatus == .success, let user = user {
                // success
                ChannelIO.showMessenger()
            } else {
                // show failed reason from bootStatus
            }
        }
        

    }
    
    
    private func deepLinkOpen(userInfo: [AnyHashable: Any]){
        
        if let isFlareLane = userInfo["isFlareLane"] as? Int ,
           let url = userInfo["url"] as? String,
           isFlareLane > 0
        {

            
            var target = ""
            var txproblemSeq = "0"
            var txpCnt = "0"
            let components = URLComponents(string: url)
            let items = components?.queryItems ?? []
            for item in items
            {
                if item.name == "target"
                {
                    target = item.value ?? ""
                }
                else if item.name == "problemSeq"
                {
                    txproblemSeq = item.value ?? "0"
                } else if item.name == "pCnt"
                {
                    txpCnt = item.value ?? "0"
                }
            }
            let problemSeq = Int(txproblemSeq) ?? 0
            let pCnt = Int(txpCnt) ?? 0
            
            //로그인 상태에서만 이동.
            if let _ = CommonUtil.shared.getAccessToken(),  target == "preparation" , problemSeq > 0
            {
                do {
                    let myInfos = try (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext.fetch(MyInfo.fetchRequest()) as! [MyInfo]
                    if myInfos.count > 0
                    {
                        let myInfo = myInfos[0]
                        let userSeq = myInfo.userSeq
                        if let vcDetail = CommonUtil.tabbarController{
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            

                                
                            }
                        }
                        
                    }
                } catch {
                    
                }
                return
            }
        }
    }
}


