import Foundation

/// The `PagingItem` protocol is used to generate menu items for all
/// the view controllers, without having to actually allocate them
/// before they are needed. You can store whatever you want in here
/// that makes sense for what you're displaying.
public protocol PagingPhoto {
    var identifier: Int { get }
    func isEqual(to item: PagingPhoto) -> Bool
    func isBefore(item: PagingPhoto) -> Bool
}

extension PagingPhoto where Self: Equatable {
    public func isEqual(to item: PagingPhoto) -> Bool {
        guard let item = item as? Self else { return false }
        return self == item
    }
}

extension PagingPhoto where Self: Comparable {
    public func isBefore(item: PagingPhoto) -> Bool {
        guard let item = item as? Self else { return false }
        return self < item
    }
}

extension PagingPhoto where Self: Hashable {
    public var identifier: Int {
        return hashValue
    }
}
