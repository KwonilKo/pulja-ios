//
//  PhotoPageViewController.swift
//  pulja
//
//  Created by kwonilko on 2022/03/24.
//

import UIKit
import Parchment



class PhotoPageViewController: UIPageViewController {

    var tag_idx : Int = 0
    var cellDatas : [StudyPhotoContent] = []
    var currentIdx : Int = 0
    var photoViewControllerDelegate : CoachStudyPhotoViewController?
    var userSeq : Int = 0
    var coachSeq : Int = 0
    var username: String = ""
    var totalElements : Int = 0
    let threshold = 20
    var page : Int = 0
    var last : Bool = false

    var isSwipe: Bool = false


    var completeHandler : ((Int)->())?

    var left : Bool = false
    var right : Bool = true
//    var photo: PhotoPage?

    var flag: Bool = true

//    var vcArray: [UIViewController] = {
//        let vc = self.vcInstance(name: "coachStudyDetailPhotoViewController") as! CoachStudyDetailPhotoViewController
//        vc.page = self.page
//        vc.last = self.last
//        vc.cellDatas = self.cellDatas
//        vc.tag_idx = self.tag_idx
//        vc.photoViewControllerDelegate = self.photoViewControllerDelegate
//        vc.userSeq = self.userSeq
//        vc.coachSeq = self.coachSeq
//        vc.username = self.username
//        vc.totalElements = self.totalElements
//        return [vc]
//    }()

//    private func vcInstance(name: String) -> UIViewController{
//        return UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: name)
//    }

//    override init(transitionStyle style: UIPageViewController.TransitionStyle, navigationOrientation: UIPageViewController.NavigationOrientation, options: [UIPageViewController.OptionsKey : Any]? = nil) {
//            super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: options)
//        }
//    required init?(coder: NSCoder) {
//            fatalError("init(coder:) has not been implemented")
//        }


    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.dataSource = nil
        self.dataSource = self
        
        CommonUtil.coachTabbarController?.tabBar.isHidden = true
        CommonUtil.coachTabbarController?.bottomNavBar.isHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CommonUtil.coachTabbarController?.bottomNavBar.isHidden = false
        CommonUtil.coachTabbarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.delegate = self
        self.dataSource = self
//
//        // 첫 번째 페이지를 기본 페이지로 설정
//        if let firstVC = vcArray.first {
//            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
//        }

        let controller = R.storyboard.home.coachStudyDetailPhotoViewController()!
        controller.page = self.page
        controller.last = self.last
        controller.cellDatas = self.cellDatas
        controller.tag_idx = self.tag_idx
        controller.photoViewControllerDelegate = self.photoViewControllerDelegate
        controller.userSeq = self.userSeq
        controller.currentIdx = self.currentIdx
        controller.coachSeq = self.coachSeq
        controller.username = self.username
        controller.totalElements = self.totalElements
        controller.index = 0
        self.setViewControllers([controller], direction: .forward, animated: true, completion: nil)


    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PhotoPageViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
            if completed {
                print("******스와이프 백 제스쳐 받음*******")
                print("*******지금 currentIdx: \(self.currentIdx)받음*******")
                let vc = previousViewControllers[0] as! CoachStudyDetailPhotoViewController
                print("*******지금 previous view controller currentIdx: \(vc.currentIdx)받음*******")
                vc.currentIdx = self.currentIdx
                
//                DispatchQueue.main.async() {
//                        self.dataSource = nil
//                        self.dataSource = self
//                }
//                self.completeHandler?(self.currentIdx)
//                if self.right {
//                    completeHandler?(-1)
//                } else {
//                    completeHandler?(1)
//                }
                isSwipe = false
            }
        }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {

        if isSwipe  {
            return nil
        }

        isSwipe = true

        print("viewcontroller BEFORE")

        // 이전
        if self.currentIdx == 0 {
            return nil
        }
        

//        return vcArray[0]
        self.currentIdx -= 1
        self.right = true
        self.left = false

        let controller = R.storyboard.home.coachStudyDetailPhotoViewController()!
        controller.page = self.page
        controller.last = self.last
        controller.cellDatas = self.cellDatas
        controller.tag_idx = self.tag_idx
        controller.photoViewControllerDelegate = self.photoViewControllerDelegate
        controller.userSeq = self.userSeq
        controller.currentIdx = self.currentIdx
        controller.coachSeq = self.coachSeq
        controller.username = self.username
        controller.totalElements = self.totalElements

        controller.index = (viewController as? CoachStudyDetailPhotoViewController)!.index - 1
        

//        let controller = pageViewController.viewControllers![0] as! CoachStudyDetailPhotoViewController
//        controller.page = self.page
//        controller.last = self.last
//        controller.cellDatas = self.cellDatas
//        controller.tag_idx = self.tag_idx
//        controller.photoViewControllerDelegate = self.photoViewControllerDelegate
//        controller.userSeq = self.userSeq
//        controller.currentIdx = self.currentIdx
//        controller.coachSeq = self.coachSeq
//        controller.username = self.username
//        controller.totalElements = self.totalElements

//        (vcArray[0] as! CoachStudyDetailPhotoViewController).currentIdx -= 1
//        flag = true
//        self.setViewControllers([controller], direction: .forward, animated: false, completion: nil)
        
        return controller

    }
//    // Forward, check if this IS NOT the last controller
//    func pageViewController(pageViewController: UIPageViewController,
//        viewControllerAfterViewController CoachStudyDetailPhotoViewController: UIViewController) -> UIViewController? {
//
//        print("forward 패스")
//        return vcArray[0]
//    }
//
//
//    // Check if this IS NOT the first controller
//    func pageViewController(pageViewController: UIPageViewController,
//        viewControllerBeforeViewController CoachStudyDetailPhotoViewController: UIViewController) -> UIViewController? {
//
//
//        print("backward 패스")
//        return vcArray[0]
//
//    }



//    func pageViewController(pageViewController: UIPageViewController, willTransitionToViewControllers pendingViewControllers: [AnyObject]) {
//        // the page view controller is about to transition to a new page, so take note
//        // of the index of the page it will display.  (We can't update our currentIndex
//        // yet, because the transition might not be completed - we will check in didFinishAnimating:)
//        if let itemController = pendingViewControllers[0] as? PageItemController {
//            nextIndex = itemController.itemIndex
//        }
//    }




    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {


        if isSwipe  {
            return nil
        }

        isSwipe = true
        print("viewcontroller AFTER")

        if self.currentIdx == self.totalElements - 1 {
            return nil
        }
        
        if (self.cellDatas.count - self.currentIdx <= threshold && !self.last) {
            self.getMorePhotos()
        }
        
        
        self.currentIdx += 1
        self.left = true
        self.right = false

        let controller = R.storyboard.home.coachStudyDetailPhotoViewController()!
        controller.page = self.page
        controller.last = self.last
        controller.cellDatas = self.cellDatas
        controller.tag_idx = self.tag_idx
        controller.photoViewControllerDelegate = self.photoViewControllerDelegate
        controller.userSeq = self.userSeq
        controller.currentIdx = self.currentIdx
        controller.coachSeq = self.coachSeq
        controller.username = self.username
        controller.totalElements = self.totalElements

        controller.index = (viewController as? CoachStudyDetailPhotoViewController)!.index + 1
        
//        self.setViewControllers([controller], direction: .forward, animated: false, completion: nil)
//
        return controller
    }
    
    
    func getMorePhotos() {
        
        self.page += 1
        LoadingView.show()
        CoachAPI.shared.getStudyImage(coachSeq: coachSeq, page: page, size: 20, userSeq: userSeq).done
        { res in
            if let suc = res.success, suc == true {
                guard let data = res.data else { return }
                if let content = data.content {
                    self.cellDatas.append(contentsOf: content)
                }
                self.last = data.last!
            }
        }.catch { error in
            print(error)
        }.finally {
            LoadingView.hide()
        }
        
    }
    
    


    
    
    
    
    
    
    
    
//    func pagingViewController(_: PagingViewController, pagingItemAt index: Int) -> PagingItem {
//        let x = PagingIndexItem(index: index, title: "hello world")
//        print("첫번째 델리게이트 성공")
//        return x
//    }
//
//    func pagingViewController(_: PagingViewController, viewControllerAt index: Int) -> UIViewController {
//        print("두번째 델리게이트 성공")
//        let vc = CoachStudyDetailPhotoViewController()
//        vc.page = photo!.page
//        vc.last = photo!.last
//        vc.cellDatas = photo!.cellDatas
//        vc.tag_idx = photo!.tag_idx
//        vc.photoViewControllerDelegate = photo!.photoViewControllerDelegate
//        vc.userSeq = photo!.userSeq
//        vc.coachSeq = photo!.coachSeq
//        vc.username = photo!.username
//        vc.totalElements = photo!.totalElements
//        return vc
//    }

//    func numberOfViewControllers(in _: PageViewController) -> Int {
//        return 1
////        return self.cellDatas.count
//    }

}



//class PhotoPageViewController : PagingViewController {
//
//    var tag_idx : Int = 0
//    var cellDatas : [StudyPhotoContent] = []
//    var currentIdx : Int = 0
//    var photoViewControllerDelegate : CoachStudyPhotoViewController?
//    var userSeq : Int = 0
//    var coachSeq : Int = 0
//    var username: String = ""
//    var totalElements : Int = 0
//    let threshold = 20
//    var page : Int = 0
//    var last : Bool = false
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.dataSource = self
//    }
//
//
//}
//
//
//extension PhotoPageViewController: PagingViewControllerDataSource {
//    func pagingViewController(_: PagingViewController, pagingItemAt index: Int) -> PagingItem {
//        return PagingIndexItem(index: index, title: cellDatas[index].photo!)
//    }
//
//    func pagingViewController(_: PagingViewController, viewControllerAt index: Int) -> UIViewController {
//        let photo = R.storyboard.home.coachStudyDetailPhotoDemoViewController()!
//        photo.tag_idx = tag_idx
//        photo.cellDatas = cellDatas
//        photo.currentIdx = index
//        photo.photoViewControllerDelegate = photoViewControllerDelegate
//        photo.userSeq = userSeq
//        photo.coachSeq = coachSeq
//        photo.username = username
//        photo.totalElements = totalElements
//        photo.page = page
//        photo.last = last
//
//        return photo
//    }
//
//    func numberOfViewControllers(in _: PagingViewController) -> Int {
//        return cellDatas.count
//    }
//}
