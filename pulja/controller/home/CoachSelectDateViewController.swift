//
//  CoachSelectDateViewController.swift
//  pulja
//
//  Created by kwonilko on 2022/03/17.
//

import UIKit

class CoachSelectDateViewController: UIViewController {
    
    let currentDate = Date()
    var sDate: String?
    @IBOutlet var pickerView: UIDatePicker!
    
    var authCallback: ((_ year: Int, _ month: Int, _ day: Int  ) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //선택한 날짜로 scroll 해주기
//        let greg = Calendar(identifier: .gregorian)
//        let now = Date()
//        var componentsMax = greg.dateComponents([.year, .month, .day, .hour, .minute, .second], from: now)
//        componentsMax.year = currentDate.year
//        componentsMax.month = 12
//        componentsMax.day = 31
//
//        var componentsMin = greg.dateComponents([.year, .month, .day, .hour, .minute, .second], from: now)
//        componentsMin.year = currentDate.year
//        componentsMin.month = 1
//        componentsMin.day = 1
//
//        pickerView.minimumDate = greg.date(from: componentsMin)!
//        pickerView.maximumDate = greg.date(from: componentsMax)!

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func datePickerView(_ sender: Any) {
        pickerView.addTarget(self, action: #selector(changed), for: .valueChanged)
    }
    
    @objc func changed() {
        let dateformatter = DateFormatter()
        dateformatter.dateStyle = .long
        dateformatter.timeStyle = .none
        dateformatter.locale = Locale(identifier: "ko")
        let date = dateformatter.string(from: pickerView.date)
        print("피커뷰에서 선택한 날짜: \(date)")
        
    }
    
    
    @IBAction func save(_ sender: Any) {
        
//        let year = pickerView.date.year
//        let month = pickerView.date.month
//        let day = pickerView.date.day
//        
//        self.authCallback?(year, month, day)
//        self.dismiss(animated: true)
//        
    }
    
        
    
    @IBAction func close(_ sender: Any) {
        dismiss(animated: true) {}
    }
    

}

//extension Date {
//    public var year: Int {
//        return Calendar.current.component(.year, from: self)
//    }
//
//    public var month: Int {
//         return Calendar.current.component(.month, from: self)
//    }
//
//    public var day: Int {
//         return Calendar.current.component(.day, from: self)
//    }
//
//    public var monthName: String {
//        let nameFormatter = DateFormatter()
//        nameFormatter.dateFormat = "MMMM" // format January, February, March, ...
//        return nameFormatter.string(from: self)
//    }
//}
