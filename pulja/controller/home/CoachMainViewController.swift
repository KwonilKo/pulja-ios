//
//  CoachMainViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/03/07.
//

import UIKit
import JTAppleCalendar
import MaterialComponents


class CoachMainViewController: PuljaBaseViewController, MDCBottomSheetControllerDelegate  {

    
    @IBOutlet var lbName: UILabel!
    @IBOutlet var scrollview: UIScrollView!
    
    @IBOutlet var calendarView: JTACMonthView!
    
    @IBOutlet var shortViewHeight: NSLayoutConstraint!
    
    @IBOutlet var initBigViewHeight: NSLayoutConstraint!
    @IBOutlet var selectDateButton: UIButton!
    
    @IBOutlet var shortView: UIView!
    //calendar
    var numberOfRows = 1
    let formatter = DateFormatter()
    var testCalendar = Calendar.current
    var generateInDates: InDateCellGeneration = .forFirstMonthOnly
    var generateOutDates: OutDateCellGeneration = .off
    var prePostVisibility: ((CellState, TodayCoachJTACalendarViewCell?)->())?
    var hasStrictBoundaries = false
    let disabledColor = UIColor.lightGray
    let enabledColor = UIColor.blue
    var monthSize: MonthSize? = nil
    let arrDayString:[String] = ["", "일", "월", "화", "수", "목", "금", "토"]
   
    var selectedDate : Date = Date() {
        didSet {
            let year = selectedDate.get(.year)
            let month = selectedDate.get(.month)
            let day = selectedDate.get(.day)
            let weekday = self.weekday(year: year, month: month, day: day)
            selectDateButton.setTitle("\(month)월 \(day)일 (\(weekday!))", for: .normal)
            
        }
    }
    var schedule: [Schedule]?
//    var selectedDate : Date = Date()  {
//        didSet {
//            let formatter = DateFormatter()
//            formatter.dateFormat = "yyyy-MM-dd"
//            let newdate = formatter.string(from: self.getMonday(date: self.selectedDate))
//            if self.scheduleList != nil {
//                let findSchedule = scheduleList!.filter { $0.scheduleDate == newdate }
//                self.setUpShortView(scheduleList: findSchedule[0].scheduleList)
//            }
//        }
//    }
    var currSchedule:[StudySchedule]? = nil
    
    var bgColor:UIColor? = nil
    
    var selectedYear: Int = 0
    var selectedMonth: Int = 0
    var selectedDay: Int = 0
    var selectedNameDay: String = ""
    var boolFirstTime: Bool = false
    var data: [StudentsSchedule]?
    var numAddSubview: Int = 0
    
    @IBOutlet var initBigView: UIView!
    var scheduleList: [CoachingHome]?
    
    
    @IBOutlet var shortStackView: UIStackView!
    
    var refreshControl : UIRefreshControl!
    var isFirst = true
    var isHomeViewReload = false
    
    
    @objc func refresh() {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        let startmon = formatter.string(from: self.getMonday(date: selectedDate))
        let endmon = formatter.string(from: self.getMonday(date: selectedDate).changeDays(addDays: 6))
        getSchedule(startDate: startmon, endDate: endmon)
        processAPI()
        setupCalendarView()
        setUpInitialDate()
        self.refreshControl.endRefreshing()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectDateButton.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        selectDateButton.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        selectDateButton.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        
        self.calendarView.minimumLineSpacing = 4
        self.calendarView.minimumInteritemSpacing = 4
        // Do any additional setup after loading the view.
        initBigView.translatesAutoresizingMaskIntoConstraints = false
        
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.Foggy
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        scrollview.refreshControl = refreshControl
        
        
//        if let d = data, isFirst {
//            isFirst = false
//            isHomeViewReload = true
            
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyyMMdd"
//        let startmon = formatter.string(from: self.getMonday(date: selectedDate))
//        let endmon = formatter.string(from: self.getMonday(date: selectedDate).changeDays(addDays: 6))
//
//        self.lbName.text = "\(self.myInfo!.username!) 코치님"
        
//        getSchedule(startDate: startmon, endDate: endmon)
//        processAPI()
//        setupCalendarView()
//        setUpInitialDate()
        self.refreshControl.endRefreshing()
//        }
        
        
    }
    
    
    func setUpShortView() {
        guard let sList = self.schedule else { return }
//        shortView.translatesAutoresizingMaskIntoConstraints = false
//        shortViewHeight.constant = 140.0 + CGFloat(scheduleList!.count) * 38.0
        if sList.count == 0 {
            //각 뷰에 공통적으로 들어가는 라인 뷰
            let commonView = UIView()
            
            commonView.borderColor = UIColor.white
            commonView.backgroundColor = UIColor.Foggy
            commonView.alpha = 0.5
            
            let oneSubview = UIView()
            oneSubview.translatesAutoresizingMaskIntoConstraints = false
            oneSubview.backgroundColor = UIColor.clear
            
            self.shortStackView.addArrangedSubview(oneSubview)
            oneSubview.heightAnchor.constraint(equalToConstant: 50).isActive = true
            
            let textLabel = UILabel()
            textLabel.font = UIFont.init(name:"AppleSDGothicNeo-Bold", size:12)
            textLabel.text = "화상 코칭 일정이 없어요."
            textLabel.textColor = UIColor.Foggy
            self.shortStackView.addSubview(textLabel)
            textLabel.translatesAutoresizingMaskIntoConstraints = false
            textLabel.leadingAnchor.constraint(equalTo: oneSubview.leadingAnchor, constant: 12).isActive = true
            textLabel.topAnchor.constraint(equalTo: oneSubview.topAnchor, constant: 12).isActive = true
            
            oneSubview.addSubview(commonView)
            
            commonView.translatesAutoresizingMaskIntoConstraints = false
            commonView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
            commonView.leadingAnchor.constraint(equalTo: oneSubview.leadingAnchor, constant: 8).isActive = true
            commonView.trailingAnchor.constraint(equalTo: oneSubview.trailingAnchor, constant: -8).isActive = true
            commonView.topAnchor.constraint(equalTo: oneSubview.topAnchor, constant: 0).isActive = true
            
            let commonView2 = UIView()
            
            commonView2.borderColor = UIColor.white
            commonView2.backgroundColor = UIColor.Foggy
            
            oneSubview.addSubview(commonView2)
            
            commonView2.translatesAutoresizingMaskIntoConstraints = false
            commonView2.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
            commonView2.leadingAnchor.constraint(equalTo: oneSubview.leadingAnchor, constant: 8).isActive = true
            commonView2.trailingAnchor.constraint(equalTo: oneSubview.trailingAnchor, constant: -8).isActive = true
            commonView2.bottomAnchor.constraint(equalTo: oneSubview.bottomAnchor, constant: -12).isActive = true
            
        } else {
            for i in 0..<sList.count {
                
                //각 뷰에 공통적으로 들어가는 라인 뷰
                let commonView = UIView()
                
                commonView.borderColor = UIColor.white
                commonView.backgroundColor = UIColor.Foggy
                commonView.alpha = 0.5
                
                let oneSubview = UIView()
                oneSubview.translatesAutoresizingMaskIntoConstraints = false
                oneSubview.backgroundColor = UIColor.clear
                
                self.shortStackView.addArrangedSubview(oneSubview)
                
                if i == sList.count - 1 {
                    oneSubview.heightAnchor.constraint(equalToConstant: 50).isActive = true
                } else {
                oneSubview.heightAnchor.constraint(equalToConstant: 38).isActive = true
                }

                
                let timelabel = UILabel()
                timelabel.font = UIFont.init(name:"AppleSDGothicNeo-Bold", size:12)
                timelabel.text = "\(sList[i].ctime!)"
                timelabel.textColor = UIColor.puljaBlack
               
                self.shortStackView.addSubview(timelabel)
                timelabel.translatesAutoresizingMaskIntoConstraints = false
                timelabel.leadingAnchor.constraint(equalTo: oneSubview.leadingAnchor, constant: 16).isActive = true
                timelabel.topAnchor.constraint(equalTo: oneSubview.topAnchor, constant: 12).isActive = true
                
                let namelabel = UILabel()
                namelabel.font = UIFont.systemFont(ofSize: 12.0)
                namelabel.text = "\(sList[i].username!)"
                namelabel.textColor = UIColor.puljaBlack
               
                self.shortStackView.addSubview(namelabel)
                namelabel.translatesAutoresizingMaskIntoConstraints = false
                namelabel.leadingAnchor.constraint(equalTo: timelabel.trailingAnchor, constant: 12).isActive = true
                namelabel.topAnchor.constraint(equalTo: oneSubview.topAnchor, constant: 12).isActive = true
                
                
                oneSubview.addSubview(commonView)
                
                commonView.translatesAutoresizingMaskIntoConstraints = false
                commonView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
                commonView.leadingAnchor.constraint(equalTo: oneSubview.leadingAnchor, constant: 8).isActive = true
                commonView.trailingAnchor.constraint(equalTo: oneSubview.trailingAnchor, constant: -8).isActive = true
                commonView.topAnchor.constraint(equalTo: oneSubview.topAnchor, constant: 0).isActive = true
                
                
                let imageview = UIImageView()
                
                oneSubview.addSubview(imageview)
                imageview.translatesAutoresizingMaskIntoConstraints = false
                imageview.leadingAnchor.constraint(equalTo: namelabel.trailingAnchor, constant: 4).isActive = true
                imageview.topAnchor.constraint(equalTo: oneSubview.topAnchor, constant: 12).isActive = true
                if sList[i].statusText! == "결석" {
                    imageview.image = UIImage.init(named: "attendNo.pdf")
                } else if sList[i].statusText! == "출석" {
                    imageview.image = UIImage.init(named: "attendCheck.pdf")
                } else if sList[i].statusText! == "지각" {
                    imageview.image = UIImage.init(named: "attendLate.pdf")
                } else if sList[i].statusText! == "보충" {
                    imageview.image = UIImage.init(named:"attendSupplement.pdf")
                } else {
                    oneSubview.willRemoveSubview(imageview)
                }
                
                
                
                if (sList.count > 0 && i == sList.count - 1 ) {
                    //각 뷰에 공통적으로 들어가는 라인 뷰
                    let commonView2 = UIView()
                    
                    commonView2.borderColor = UIColor.white
                    commonView2.backgroundColor = UIColor.Foggy
                    
                    oneSubview.addSubview(commonView2)
                    
                    commonView2.translatesAutoresizingMaskIntoConstraints = false
                    commonView2.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
                    commonView2.leadingAnchor.constraint(equalTo: oneSubview.leadingAnchor, constant: 8).isActive = true
                    commonView2.trailingAnchor.constraint(equalTo: oneSubview.trailingAnchor, constant: -8).isActive = true
                    commonView2.bottomAnchor.constraint(equalTo: oneSubview.bottomAnchor, constant: -12).isActive = true
                }
                
                

            }
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        let startmon = formatter.string(from: self.getMonday(date: selectedDate))
        let endmon = formatter.string(from: self.getMonday(date: selectedDate).changeDays(addDays: 6))

        self.lbName.text = "\(self.myInfo!.username!) 코치님"

        getSchedule(startDate: startmon, endDate: endmon)
        processAPI()
        setupCalendarView()
        setUpInitialDate()
        
    }
    
    func processAPI() {
        self.initBigView.isHidden = true
        LoadingView.show()
        
        let userSeq = self.myInfo?.userSeq ?? 0
        CoachAPI.shared.getCoachStudents(coachSeq : Int(userSeq)).done {
            res in
            if let suc = res.success, suc == true {
                self.data = res.data
                guard let data = self.data else { return }
                self.numAddSubview = data.count
                
            }
        }.catch { error in
            
        }.finally {
            self.initMakeSubviews()
            self.initBigView.isHidden = false
            LoadingView.hide()
        }
        
        
    }
    
    func processFirstSubview(oneStudent: StudentsSchedule) -> UIView {
        
        //각 뷰에 공통적으로 들어가는 라인 뷰
        let commonView = UIView()
        
        commonView.borderColor = UIColor.white
        commonView.backgroundColor = UIColor.Dust
        
        
        let firstView = UIView()
        firstView.translatesAutoresizingMaskIntoConstraints = false
        firstView.backgroundColor = UIColor.white
        firstView.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        firstView.cornerRadius = 10
        firstView.borderWidth = 0.2
        firstView.borderColor = UIColor.white
        
        
        let coachview = UIView()
        firstView.addSubview(coachview)
        coachview.translatesAutoresizingMaskIntoConstraints = false
        coachview.backgroundColor = UIColor.white
        coachview.cornerRadius = 5
        coachview.borderWidth = 0.5
        coachview.borderColor = UIColor.Foggy
        coachview.topAnchor.constraint(equalTo: firstView.topAnchor, constant: 20).isActive = true
        coachview.trailingAnchor.constraint(equalTo: firstView.trailingAnchor, constant: -16).isActive = true
        coachview.bottomAnchor.constraint(equalTo: firstView.bottomAnchor, constant: -15).isActive = true
        coachview.widthAnchor.constraint(equalToConstant: 62.0).isActive = true
        
        let dateview = UIView()
        
        firstView.addSubview(dateview)
        dateview.translatesAutoresizingMaskIntoConstraints = false
        dateview.backgroundColor = UIColor.white
        dateview.cornerRadius = 5
        dateview.borderWidth = 0.5
        dateview.borderColor = UIColor.Foggy
        dateview.topAnchor.constraint(equalTo: firstView.topAnchor, constant: 20).isActive = true
        dateview.trailingAnchor.constraint(equalTo: coachview.leadingAnchor, constant: -5).isActive = true
        dateview.bottomAnchor.constraint(equalTo: firstView.bottomAnchor, constant: -15).isActive = true
//        dateview.widthAnchor.constraint(equalToConstant: 30.0).isActive = true
        if let pDay = oneStudent.restPaymentDay, pDay > 99 {
            dateview.widthAnchor.constraint(equalToConstant: 40.0).isActive = true
        } else {
            dateview.widthAnchor.constraint(equalToConstant: 30.0).isActive = true
        }
//        dateview.bounds = dateview.frame.insetBy(dx: 4.0, dy: 0.0)
        
        
        let dateLabel = UILabel()
        dateLabel.font = UIFont.init(name:"AppleSDGothicNeo-Bold", size:10)
        if (oneStudent.restPaymentDay! >= -3 && oneStudent.restPaymentDay! < 0)
        {
            dateLabel.text = "종료"
            
        } else {
            dateLabel.text = "D-\(oneStudent.restPaymentDay!)"
        }
        dateLabel.textColor = UIColor.Dust
        firstView.addSubview(dateLabel)
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
//        dateLabel.trailingAnchor.constraint(equalTo: firstView.trailingAnchor, constant: -80).isActive = true
//        dateLabel.topAnchor.constraint(equalTo: firstView.topAnchor, constant: 16).isActive = true
        dateLabel.centerXAnchor.constraint(equalTo: dateview.centerXAnchor).isActive = true
        dateLabel.centerYAnchor.constraint(equalTo: dateview.centerYAnchor).isActive = true
        
        let coachLabel = UILabel()
        coachLabel.font = UIFont.init(name:"AppleSDGothicNeo-Bold", size:10)
        coachLabel.text = "남은 코칭 \(oneStudent.restCoachingCnt!)회"
        coachLabel.textColor = UIColor.Dust
        firstView.addSubview(coachLabel)
        coachLabel.translatesAutoresizingMaskIntoConstraints = false
//        coachLabel.trailingAnchor.constraint(equalTo: firstView.trailingAnchor, constant: -80).isActive = true
//        dateLabel.topAnchor.constraint(equalTo: firstView.topAnchor, constant: 16).isActive = true
        coachLabel.centerXAnchor.constraint(equalTo: coachview.centerXAnchor).isActive = true
        coachLabel.centerYAnchor.constraint(equalTo: coachview.centerYAnchor).isActive = true
        
        let nameLabel = UILabel()
        nameLabel.font = UIFont.init(name:"AppleSDGothicNeo-Bold", size:16)
        nameLabel.text = "\(oneStudent.username!) 학생"
        nameLabel.textColor = UIColor.puljaBlack
        firstView.addSubview(nameLabel)
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.topAnchor.constraint(equalTo: firstView.topAnchor, constant: 16).isActive = true
        nameLabel.leadingAnchor.constraint(equalTo: firstView.leadingAnchor, constant: 16).isActive = true
        
        firstView.addSubview(commonView)
        
        commonView.translatesAutoresizingMaskIntoConstraints = false
        commonView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        commonView.leadingAnchor.constraint(equalTo: firstView.leadingAnchor, constant: 8).isActive = true
        commonView.trailingAnchor.constraint(equalTo: firstView.trailingAnchor, constant: -8).isActive = true
        commonView.bottomAnchor.constraint(equalTo: firstView.bottomAnchor, constant: 0).isActive = true
        
        
        return firstView
    }
    
    func processSecondSubview(oneStudent: StudentsSchedule) -> UIView {
        //각 뷰에 공통적으로 들어가는 라인 뷰
        let commonView = UIView()
        
        commonView.borderColor = UIColor.white
        commonView.backgroundColor = UIColor.Dust
        
        let secondView = UIView()
        secondView.translatesAutoresizingMaskIntoConstraints = false
        secondView.backgroundColor = UIColor.white
        secondView.heightAnchor.constraint(equalToConstant: 60.0).isActive = true
        secondView.cornerRadius = 10
        secondView.borderWidth = 0.2
        secondView.borderColor = UIColor.white
        
        let curLabel = UILabel()
        curLabel.font = UIFont.init(name:"AppleSDGothicNeo-Regular", size:12)
        curLabel.text = "진행 커리큘럼"
        curLabel.textColor = UIColor.Dust
        secondView.addSubview(curLabel)
        curLabel.translatesAutoresizingMaskIntoConstraints = false
        curLabel.topAnchor.constraint(equalTo: secondView.topAnchor, constant: 12).isActive = true
        curLabel.leadingAnchor.constraint(equalTo: secondView.leadingAnchor, constant: 16).isActive = true
        
        let curName = UILabel()
        curName.font = UIFont.init(name:"AppleSDGothicNeo-Bold", size:12)
        if let onecurr = oneStudent.curriculumTitle, onecurr != "" {
            curName.text = "\(onecurr)"
            curName.textColor = UIColor.puljaBlack
        } else {
            curName.text = "진행 중인 커리큘럼 없음"
            curName.textColor = UIColor.Foggy
        }
        
        secondView.addSubview(curName)
        curName.translatesAutoresizingMaskIntoConstraints = false
        curName.topAnchor.constraint(equalTo: curLabel.bottomAnchor, constant: 8).isActive = true
        curName.leadingAnchor.constraint(equalTo: secondView.leadingAnchor, constant: 16).isActive = true
        
        secondView.addSubview(commonView)
        
        commonView.translatesAutoresizingMaskIntoConstraints = false
        commonView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        commonView.leadingAnchor.constraint(equalTo: secondView.leadingAnchor, constant: 8).isActive = true
        commonView.trailingAnchor.constraint(equalTo: secondView.trailingAnchor, constant: -8).isActive = true
        commonView.bottomAnchor.constraint(equalTo: secondView.bottomAnchor, constant: 0).isActive = true
        
        return secondView
    }
    
    func processThirdSubview(oneStudent: StudentsSchedule) -> UIView {
        //각 뷰에 공통적으로 들어가는 라인 뷰
        let commonView = UIView()
        
        commonView.borderColor = UIColor.white
        commonView.backgroundColor = UIColor.Dust
        
        let thidView = UIView()
        thidView.translatesAutoresizingMaskIntoConstraints = false
        thidView.backgroundColor = UIColor.white
        thidView.heightAnchor.constraint(equalToConstant: 60.0).isActive = true
        thidView.cornerRadius = 10
        thidView.borderWidth = 0.2
        thidView.borderColor = UIColor.white
        
        let nextVideoLabel = UILabel()
        nextVideoLabel.font = UIFont.init(name:"AppleSDGothicNeo-Regular", size:12)
        nextVideoLabel.text = "다음 화상 코칭"
        nextVideoLabel.textColor = UIColor.Dust
        thidView.addSubview(nextVideoLabel)
        nextVideoLabel.translatesAutoresizingMaskIntoConstraints = false
        nextVideoLabel.topAnchor.constraint(equalTo: thidView.topAnchor, constant: 12).isActive = true
        nextVideoLabel.leadingAnchor.constraint(equalTo: thidView.leadingAnchor, constant: 16).isActive = true

        let nextVideoName = UILabel()
        nextVideoName.font = UIFont.init(name:"AppleSDGothicNeo-Bold", size:12)
        if let nvd = oneStudent.nextCoaching, nvd != "" {
            nextVideoName.text = "\(nvd)"
            nextVideoName.textColor = UIColor.puljaBlack
        } else {
            nextVideoName.text = "예약된 화상 코칭 없음"
            nextVideoName.textColor = UIColor.Foggy
        }
        
        
        thidView.addSubview(nextVideoName)
        nextVideoName.translatesAutoresizingMaskIntoConstraints = false
        nextVideoName.topAnchor.constraint(equalTo: nextVideoLabel.bottomAnchor, constant: 8).isActive = true
        nextVideoName.leadingAnchor.constraint(equalTo: thidView.leadingAnchor, constant: 16).isActive = true
        
        thidView.addSubview(commonView)
        
        commonView.translatesAutoresizingMaskIntoConstraints = false
        commonView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        commonView.leadingAnchor.constraint(equalTo: thidView.leadingAnchor, constant: 8).isActive = true
        commonView.trailingAnchor.constraint(equalTo: thidView.trailingAnchor, constant: -8).isActive = true
        commonView.bottomAnchor.constraint(equalTo: thidView.bottomAnchor, constant: 0).isActive = true
        
        return thidView
        
    }
    
    func processFourthSubview(oneStudent: StudentsSchedule) -> UIView {
        //각 뷰에 공통적으로 들어가는 라인 뷰
        let commonView = UIView()
        
        commonView.borderColor = UIColor.white
        commonView.backgroundColor = UIColor.Dust
        
        let fourthView = UIView()
        fourthView.translatesAutoresizingMaskIntoConstraints = false
        fourthView.backgroundColor = UIColor.white
        fourthView.heightAnchor.constraint(equalToConstant: 60.0).isActive = true
        fourthView.cornerRadius = 10
        fourthView.borderWidth = 0.2
        fourthView.borderColor = UIColor.white
        
        let todayLabel = UILabel()
        todayLabel.font = UIFont.init(name:"AppleSDGothicNeo-Regular", size:12)
        todayLabel.text = "오늘 학습 시간"
        todayLabel.textColor = UIColor.Dust
        fourthView.addSubview(todayLabel)
        todayLabel.translatesAutoresizingMaskIntoConstraints = false
        todayLabel.topAnchor.constraint(equalTo: fourthView.topAnchor, constant: 12).isActive = true
        todayLabel.leadingAnchor.constraint(equalTo: fourthView.leadingAnchor, constant: 16).isActive = true

       
        let todayStudyList = oneStudent.todayStudyList ?? []
        
        if todayStudyList.count == 0 {
            let todayName = UILabel()
            todayName.font = UIFont.init(name:"AppleSDGothicNeo-Bold", size:12)
            todayName.textColor = UIColor.Foggy
            todayName.text = "오늘 학습 시간 인증 없음"
            
            fourthView.addSubview(todayName)
            todayName.translatesAutoresizingMaskIntoConstraints = false
            todayName.topAnchor.constraint(equalTo: todayLabel.bottomAnchor, constant: 8).isActive = true
            todayName.leadingAnchor.constraint(equalTo: fourthView.leadingAnchor, constant: 16).isActive = true
            
            
        } else {
            let interval = 70
            
            for (i, t) in todayStudyList.enumerated() {
                
                let todayName = UILabel()
                todayName.font = UIFont.init(name:"AppleSDGothicNeo-Regular", size:12)
                todayName.textColor = UIColor.puljaBlack
                if i == 0 {
                    todayName.text = "\(t.studyTime!)"
                } else {
                    todayName.text = "| \(t.studyTime!)"
                }
                
                // 인증 전, 인증 완료에 따른 이미지 넣기
                let imageview = UIImageView()
                if t.confirmTodayStudy! == true {
                    imageview.image = UIImage.init(named: "afterAuth")
                } else {
                    imageview.image = UIImage.init(named: "beforeAuth")
                }
                
                fourthView.addSubview(todayName)
                todayName.translatesAutoresizingMaskIntoConstraints = false
                todayName.topAnchor.constraint(equalTo: todayLabel.bottomAnchor, constant: 8).isActive = true
                todayName.leadingAnchor.constraint(equalTo: fourthView.leadingAnchor, constant: CGFloat(16 + interval * i)).isActive = true
                
                
                fourthView.addSubview(imageview)
                imageview.translatesAutoresizingMaskIntoConstraints = false
                imageview.leadingAnchor.constraint(equalTo: todayName.trailingAnchor, constant: 5.0).isActive = true
                imageview.topAnchor.constraint(equalTo: todayLabel.bottomAnchor, constant: 8).isActive = true
                
//                if i != todayStudyList.count - 1 {
//
//                    todayName.text! += "\(t.studyTime!) | "
//                } else {
//
//                    todayName.text! += "\(t.studyTime!)"
//                }
            }
        }
        
        
        
        fourthView.addSubview(commonView)
        
        commonView.translatesAutoresizingMaskIntoConstraints = false
        commonView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        commonView.leadingAnchor.constraint(equalTo: fourthView.leadingAnchor, constant: 8).isActive = true
        commonView.trailingAnchor.constraint(equalTo: fourthView.trailingAnchor, constant: -8).isActive = true
        commonView.bottomAnchor.constraint(equalTo: fourthView.bottomAnchor, constant: 0).isActive = true
        
        return fourthView
    }
    
    func processFifthSubview(oneStudent: StudentsSchedule) -> UIView {
        
        //각 뷰에 공통적으로 들어가는 라인 뷰
        let commonView = UIView()
        
        commonView.borderColor = UIColor.white
        commonView.backgroundColor = UIColor.Dust
        
        let fifthView = UIView()
        fifthView.translatesAutoresizingMaskIntoConstraints = false
        fifthView.backgroundColor = UIColor.white
        fifthView.heightAnchor.constraint(equalToConstant: 60.0).isActive = true
        fifthView.cornerRadius = 10
        fifthView.borderWidth = 0.2
        fifthView.borderColor = UIColor.white
        
        let weeklyLabel = UILabel()
        weeklyLabel.font = UIFont.init(name:"AppleSDGothicNeo-Regular", size:12)
        weeklyLabel.text = "주간 학습 진행률"
        weeklyLabel.textColor = UIColor.Dust
        fifthView.addSubview(weeklyLabel)
        weeklyLabel.translatesAutoresizingMaskIntoConstraints = false
        weeklyLabel.topAnchor.constraint(equalTo: fifthView.topAnchor, constant: 12).isActive = true
        weeklyLabel.leadingAnchor.constraint(equalTo: fifthView.leadingAnchor, constant: 16).isActive = true
        
        let weeklyName = UILabel()
        weeklyName.font = UIFont.init(name:"AppleSDGothicNeo-Bold", size:12)
        
        if let weekStudy = oneStudent.weekStudy {
            weeklyName.text = "\(weekStudy)"
            weeklyName.textColor = UIColor.puljaBlack
        }
       
        fifthView.addSubview(weeklyName)
        weeklyName.translatesAutoresizingMaskIntoConstraints = false
        weeklyName.topAnchor.constraint(equalTo: weeklyLabel.bottomAnchor, constant: 8).isActive = true
        weeklyName.leadingAnchor.constraint(equalTo: fifthView.leadingAnchor, constant: 16).isActive = true
        
        fifthView.addSubview(commonView)
        
        commonView.translatesAutoresizingMaskIntoConstraints = false
        commonView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        commonView.leadingAnchor.constraint(equalTo: fifthView.leadingAnchor, constant: 8).isActive = true
        commonView.trailingAnchor.constraint(equalTo: fifthView.trailingAnchor, constant: -8).isActive = true
        commonView.bottomAnchor.constraint(equalTo: fifthView.bottomAnchor, constant: 0).isActive = true
        
        return fifthView

    }
    
    func processSixthSubview(oneStudent: StudentsSchedule, idx: Int) -> UIView {
        
        let sixthView = UIView()
        sixthView.translatesAutoresizingMaskIntoConstraints = false
        sixthView.backgroundColor = UIColor.white
        sixthView.heightAnchor.constraint(equalToConstant: 65.0).isActive = true
        sixthView.cornerRadius = 10
        sixthView.borderWidth = 0.2
        sixthView.borderColor = UIColor.white
        
        //학습 인증 사진 보기, 학생 학습 공간 확인 영역 추가
    
        let subview1 = UIView()
        
        sixthView.addSubview(subview1)
        subview1.translatesAutoresizingMaskIntoConstraints = false
        subview1.backgroundColor = UIColor.white
        subview1.cornerRadius = 10
        subview1.borderWidth = 1
        subview1.borderColor = UIColor.Foggy
        
        // let calWidth = (initBigView.frame.size.width - 8*3)/2 + 8*2
        let width = (initBigView.frame.size.width - 24 - 8*3)/2
        subview1.widthAnchor.constraint(equalToConstant: width).isActive = true
        subview1.leadingAnchor.constraint(equalTo: sixthView.leadingAnchor, constant: 8).isActive = true
        subview1.topAnchor.constraint(equalTo: sixthView.topAnchor, constant: 12).isActive = true
        subview1.bottomAnchor.constraint(equalTo: sixthView.bottomAnchor, constant: -12).isActive = true
        
        
        //학습 인증 사진 보기
        let button1 = UIButton()
        button1.backgroundColor = UIColor.white
        button1.setTitle("학습 인증 사진 보기", for: .normal)
        button1.titleLabel?.font = UIFont.init(name:"AppleSDGothicNeo-Bold", size:12)
        button1.setTitleColor(UIColor.puljaBlack, for: .normal)
        button1.addTarget(self, action: #selector(checkStudyPhoto(_ :)), for: .touchUpInside)
        
        let image = UIImage(named: "iconSolidCheveronRight")
        button1.setImage(image, for: .normal)
        button1.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        button1.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        button1.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        
        sixthView.addSubview(button1)
        button1.translatesAutoresizingMaskIntoConstraints = false
        button1.centerXAnchor.constraint(equalTo: subview1.centerXAnchor).isActive = true
        button1.centerYAnchor.constraint(equalTo: subview1.centerYAnchor).isActive = true
        button1.tag = idx
        
        let subview2 = UIView()
        sixthView.addSubview(subview2)
        subview2.translatesAutoresizingMaskIntoConstraints = false
        
        subview2.backgroundColor = UIColor.white
        subview2.cornerRadius = 10
        subview2.borderWidth = 1
        subview2.borderColor = UIColor.Foggy
        subview2.widthAnchor.constraint(equalToConstant: width).isActive = true
//        subview2.leadingAnchor.constraint(equalTo: sixthView.leadingAnchor, constant: calWidth).isActive = true
        subview2.topAnchor.constraint(equalTo: sixthView.topAnchor, constant: 12).isActive = true
        subview2.trailingAnchor.constraint(equalTo: sixthView.trailingAnchor, constant: -8).isActive = true
        subview2.bottomAnchor.constraint(equalTo: sixthView.bottomAnchor, constant: -12).isActive = true
        
        
        // 학생 학습 공간 확인
        let button2 = UIButton()
        button2.backgroundColor = UIColor.white
        button2.setTitle("학생 학습 공간 확인", for: .normal)
        button2.titleLabel?.font = UIFont.init(name:"AppleSDGothicNeo-Bold", size:12)
        button2.setTitleColor(UIColor.puljaBlack, for: .normal)
        button2.tag = idx
        button2.addTarget(self, action: #selector(checkStudentStudySpace(_ :)), for: .touchUpInside)
        
        button2.setImage(image, for: .normal)
        button2.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        button2.titleLabel?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        button2.imageView?.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        
        sixthView.addSubview(button2)
        button2.translatesAutoresizingMaskIntoConstraints = false
        button2.centerXAnchor.constraint(equalTo: subview2.centerXAnchor).isActive = true
        button2.centerYAnchor.constraint(equalTo: subview2.centerYAnchor).isActive = true
        
        return sixthView
        
    }
    
    
    
    @objc func checkStudyPhoto(_ sender: UIButton) {
        print("학습 인증 사진")
        let vc = R.storyboard.home.coachStudyPhotoViewController()!
        vc.data = self.data
        vc.tag_idx = sender.tag
        vc.coachSeq = Int(self.myInfo?.userSeq ?? 0)
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func checkStudentStudySpace(_ sender: UIButton) {
        print("학생 학습 공간 확인")
        let vc = R.storyboard.study.studyMyCurriculumViewController()!
        vc.tag_idx = sender.tag
        vc.data = self.data
        vc.connectFromCoach = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func processOneSubview(idx: Int) -> UIStackView {
        let oneStudent = data![idx]
        
        let stackView = UIStackView()
        stackView.axis = NSLayoutConstraint.Axis.vertical
        stackView.distribution  = UIStackView.Distribution.equalSpacing
        stackView.spacing = 0
        
        // 첫번째 뷰
        let firstView = processFirstSubview(oneStudent: oneStudent)
        
        // 두번째 뷰
        let secondView = processSecondSubview(oneStudent: oneStudent)
        
        // 세번째 뷰
        let thidView = processThirdSubview(oneStudent: oneStudent)

        // 네번째 뷰
        let fourthView = processFourthSubview(oneStudent: oneStudent)

        // 다섯번째 뷰
        let fifthView = processFifthSubview(oneStudent: oneStudent)

        // 여섯번째 뷰
        let sixthView = processSixthSubview(oneStudent: oneStudent, idx: idx)
        
        stackView.addArrangedSubview(firstView)
        stackView.addArrangedSubview(secondView)
        stackView.addArrangedSubview(thidView)
        stackView.addArrangedSubview(fourthView)
        stackView.addArrangedSubview(fifthView)
        stackView.addArrangedSubview(sixthView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
    
        
    }
    
    func initMakeSubviews() {
        
        //reset init big view
        for (i, v) in self.initBigView.subviews.enumerated() {
            if i > 0 {
                v.removeFromSuperview()
            }
        }
        
        
        //set 스크롤뷰 높이
        self.setScrollViewHeight()
        
        for i in 0..<self.numAddSubview {
            let lastView = initBigView.subviews.last
            
            //학생 한명에 해당하는 큰 뷰 만들고, 그 안에 스택뷰까지 추가하기
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor.white
            
            initBigView.addSubview(view)
            
            view.heightAnchor.constraint(equalToConstant: 355).isActive = true
            view.leadingAnchor.constraint(equalTo: initBigView.leadingAnchor, constant: 12).isActive = true
            view.widthAnchor.constraint(equalTo: lastView!.widthAnchor).isActive = true
            view.topAnchor.constraint(equalTo: lastView!.bottomAnchor, constant: 12).isActive = true

            view.cornerRadius = 10
            view.borderWidth = 0.2
            view.borderColor = UIColor.Foggy
            
            let makeStackview = processOneSubview(idx: i)
            initBigView.subviews[i+1].addSubview(makeStackview)
            makeStackview.leadingAnchor.constraint(equalTo: initBigView.leadingAnchor, constant: 12).isActive = true
            makeStackview.widthAnchor.constraint(equalTo: lastView!.widthAnchor).isActive = true
            makeStackview.topAnchor.constraint(equalTo: lastView!.bottomAnchor, constant: 12).isActive = true
           
            print("끝")
            

        }
    }
    
    func setUpInitialDate() {
//        let year = selectedDate.year
//        let month = selectedDate.month
//        let day = selectedDate.day
//        let newday = weekday(year: year, month: month, day: day)
//
//        let newDate = "\(month)월 \(day)일 (\(newday!))"
//        self.selectDateButton.setTitle(newDate, for: .normal)
        
//        // set 스크롤뷰 높이
//        let tabBarHeight = CommonUtil.coachTabbarController?.tabBar.frame.size.height ?? 50.0
//        let newheight = CGFloat(140 + (12 + 355) * self.numAddSubview + Int(tabBarHeight) + Int(self.shortStackView.frame.size.height))
//
//        initBigViewHeight.constant = newheight
//        self.scrollview.contentSize = CGSize(width: self.scrollview.frame.size.width, height: newheight)
    }
    
    
    func setupCalendarView() {
        
        calendarView.scrollingMode = .stopAtEachCalendarFrame
        calendarView.showsHorizontalScrollIndicator = true
        
        calendarView.scrollToDate(getMonday(date:Date()))
        print("Here is Date : \(getMonday(date:Date()))")
        
        calendarView.selectDates([Date()])
        
        
    }
    
    
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        guard let startDate = visibleDates.monthDates.first?.date else {
            return
        }
        let month = testCalendar.dateComponents([.month], from: startDate).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        // 0 indexed array
        let year = testCalendar.component(.year, from: startDate)
//        monthLabel.text = monthName + " " + String(year)
    }
    
    
    func setStudyScheduleWeekCount(startDate:String, endDate:String){
        
//        HomeAPI.shared.studySchedule(startDate: startDate, endDate: endDate).done { res in
//            if let data = res.data {
//                if ( data.count > 0){
//                    self.lbWeekNumber.text = data[0].weekInfo
//                }
//            }
//        }
    }
    
    /// 특정 연도, 월, 일에 대한 요일을 구하는 메소드
    ///
    /// - Parameter year: 연도
    /// - Parameter month: 월
    /// - Parameter day: 일
    ///
    /// - Returns: 요일에 해당하는 문자열 Ex) Sun, Sunday, S
    /// - note: day에 대한 안정성을 보장하지 않음
    ///         Ex) 2019-02-40을 넣으면 2월1일로부터 40일이 되는날(3월12일)의 요일이 반환됨
    func weekday(year: Int, month: Int, day: Int) -> String? {
        let calendar = Calendar(identifier: .gregorian)
        
        guard let targetDate: Date = {
            let comps = DateComponents(calendar:calendar, year: year, month: month, day: day)
            return comps.date
            }() else { return nil }
        
        let day = Calendar.current.component(.weekday, from: targetDate) - 1
        let detailedDay:String? = Calendar.current.shortWeekdaySymbols[day]
//        let temp = Calendar.current.shortWeekdaySymbols[day]
//        let detailedDay : String?
//
//        switch temp {
//        case "Sun" :
//            detailedDay = "일"
//        case "Mon" :
//            detailedDay = "월"
//        case "Tue" :
//            detailedDay = "화"
//        case "Wed" :
//            detailedDay = "수"
//        case "Thu" :
//            detailedDay = "목"
//        case "Fri" :
//            detailedDay = "금"
//        default :
//            detailedDay = "토"
//        }
        print("\(month)월 \(day)일 \(detailedDay!)")
        return detailedDay
    }
    
    
    func getMonday(date:Date) -> Date {
        print("here is input date: \(date)")
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM-dd-e-EEEE"    //e는 1~7(sun~sat)

        let day = formatter.string(from:date)
        let today = day.components(separatedBy: "-") // [0] = MMM, [1] = dd, [2] = e(1), [3] = EEEE(Sunday)
        guard let interval = Double(today[2]) else{ return date }
        
        var dateComponent = DateComponents()
        
        var monday = 2
        if interval == 1 {
            monday = -6
        } else {
            monday = Int(2 - interval)
        }
        
        dateComponent.day = Int(monday)
        let mondayDate = Calendar.current.date(byAdding: dateComponent, to: date)
        print("here is date222 : \(mondayDate!)")

        return mondayDate!
    }

    
    
    @IBAction func selectDate(_ sender: Any) {
        
        if let vc = storyboard?.instantiateViewController(withIdentifier: "coachSelectDateViewController")as?
            CoachSelectDateViewController {
            
            vc.authCallback = { (year: Int, month: Int, day: Int)
                in
                let newday = self.weekday(year: year, month: month, day: day)
                let newDate = "\(month)월 \(day)일 (\(newday!))"
                self.selectDateButton.setTitle(newDate, for: .normal)
                
                //선택한 날짜로 scroll 해주기
                let greg = Calendar(identifier: .gregorian)
                let now = Date()
                var components = greg.dateComponents([.year, .month, .day, .hour, .minute, .second], from: now)
                components.year = year
                components.month = month
                components.day = day
                
                self.selectedYear = year
                self.selectedMonth = month
                self.selectedDay = day
                self.selectedNameDay = newday!
                self.boolFirstTime = true
                
                let sendDate = greg.date(from: components)!
                self.selectedDate = sendDate
                self.calendarView.scrollToDate(self.getMonday(date: sendDate))
                self.calendarView.selectDates([sendDate])
                
                self.calendarView.reloadData()
                
            }
            
            // MDC 바텀 시트로 설정
            let bottomSheet = MDCBottomSheetController(contentViewController: vc)
            bottomSheet.mdc_bottomSheetPresentationController?.preferredSheetHeight = 425
            bottomSheet.delegate = self
            self.present(bottomSheet, animated: true, completion: nil)
            
        }
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func handleCellConfiguration(cell: JTACDayCell?, cellState: CellState) {
           handleCellSelection(view: cell, cellState: cellState)
           handleCellTextColor(view: cell, cellState: cellState)
   //        prePostVisibility?(cellState, cell as? CellView)
       }
    
    // Function to handle the calendar selection
    func handleCellSelection(view: JTACDayCell?, cellState: CellState) {
//        print("여기여기여기1 \(cellState.date.year)\(cellState.date.month)\(cellState.date.day)")
//        guard let myCustomCell = view as? TodayCoachJTACalendarViewCell else {return }
//
//        if cellState.isSelected {
//            print("선택된 놈: \(cellState.date.year)\(cellState.date.month)\(cellState.date.day)")
//            print("선택된 놈: \(cellState.date)")
//            print("********날짜 더함\(cellState.date.changeDays(addDays: 7))*************")
//            print("********날짜 더함\(cellState.date.changeDays(addDays: 35))*************")
//
//            myCustomCell.bgView.backgroundColor = UIColor.paledPurple
//            let selectDate = cellState.date.toString(.noDotdate)
//
//
//        } else {
//            let date = cellState.date
//
//            myCustomCell.bgView.backgroundColor = UIColor.OffWhite
//
//
//
//        }
    }
    
    // Function to handle the text color of the calendar
   func handleCellTextColor(view: JTACDayCell?, cellState: CellState) {
       guard let myCustomCell = view as? TodayCoachJTACalendarViewCell  else {
           return
       }
               
       if cellState.isSelected {
//               myCustomCell.dayLabel.textColor = .white
       } else {
//               myCustomCell.dayLabel.textColor = .black
       }
       
//           myCustomCell.isHidden = cellState.dateBelongsTo != .thisMonth
       
   }

    
    
    
    

}


// MARK: - calendar extension
extension CoachMainViewController: JTACMonthViewDelegate, JTACMonthViewDataSource {
    
    
    
    
    func configureCalendar(_ calendar: JTACMonthView) -> ConfigurationParameters {
        
        let currentDate = Date()

        let monthsToEnd = 12
        var dateComponent = DateComponents()
        dateComponent.month = monthsToEnd
        var futureDate = Calendar.current.date(byAdding: dateComponent, to: currentDate)
        
        futureDate = getMonday(date: futureDate!)
        
        let monthsToStart = -12
        var dateComponent2 = DateComponents()
        dateComponent2.month = monthsToStart
        var pastDate = Calendar.current.date(byAdding: dateComponent2, to: currentDate)
        
        pastDate = getMonday(date: pastDate!)
        
        
            
        formatter.dateFormat = "yyyyMMdd"
        formatter.timeZone = TimeZone(abbreviation: "KST")
        formatter.locale = Locale(identifier: "ko_KR")
        
        
        
        let pDate = pastDate!.toString(.noDotdate)
            
        let fDate = futureDate!.toString(.noDotdate)
            
        let startDate = formatter.date(from: pDate)!
        let endDate = formatter.date(from: fDate)!
        
            
        let parameters = ConfigurationParameters(startDate: pastDate!,
                                                     endDate: futureDate!,
                                                     numberOfRows: numberOfRows,
                                                     calendar: testCalendar,
                                                     generateInDates: generateInDates,
                                                     generateOutDates: generateOutDates,
                                                     firstDayOfWeek: .monday,
                                                     hasStrictBoundaries: hasStrictBoundaries)
            
        return parameters
        
    }
    
    func setScrollViewHeight() {
        //set 스크롤뷰 높이
        let tabBarHeight = CommonUtil.coachTabbarController?.tabBar.frame.size.height ?? 50.0
        var sCnt = 1;
        if let sd = self.schedule {
            sCnt = sd.count == 0 ? 1 : sd.count
        }
        
        let newheight = CGFloat(140 + (12 + 355) * self.numAddSubview + Int(tabBarHeight) + sCnt * 38)

        self.initBigViewHeight.constant = newheight
        self.scrollview.contentSize = CGSize(width: self.scrollview.frame.size.width, height: newheight)
    }
    
    
    func getSchedule(startDate: String, endDate: String) {
        let userSeq = self.myInfo?.userSeq ?? 0
        CoachAPI.shared.getCoachHome(coachSeq: Int(userSeq), endDate: endDate, startDate: startDate).done
        { res in
            if let suc = res.success, suc == true {
                guard let data = res.data else { return }
                self.scheduleList = data
                
            }
        }.catch { error in
            print(error)
            print("getSchedule 실패")
        }.finally {
            let f = DateFormatter()
            f.dateFormat = "yyyy-MM-dd"
            let selectedDate = f.string(from: self.selectedDate)
            
            // 바꾸고난 후
            if selectedDate.toDate(.date) <= endDate.toDate(.noDotdate) && selectedDate.toDate(.date) >= startDate.toDate(.noDotdate) {
                let findSchedule = self.scheduleList!.filter { $0.scheduleDate == selectedDate }
                if findSchedule.count > 0 {
                    self.schedule = findSchedule[0].scheduleList
                } else {
                    self.schedule = []
                }
            }
            self.initializeShortView()
            
            self.setUpShortView()
//            if self.schedule!.count > 0 {
//                self.setUpShortView()
//            }
            
            //set 스크롤뷰 높이
            self.setScrollViewHeight()
            self.calendarView.reloadData()
        }
        
        
    }
        
    func configureVisibleCell(myCustomCell: TodayCoachJTACalendarViewCell, cellState: CellState, date: Date, indexPath: IndexPath) {
//        print("색칠해야함:\(date)")
//        print("\(date.year)\(date.month)\(date.day)")
//        print("\(selectedYear)\(selectedMonth)\(selectedDay)")
//        if testCalendar.isDateInToday(date) {
//            myCustomCell.lbDate.text = "오늘"
//            myCustomCell.lbDate.textColor = UIColor.Purple
////
//            myCustomCell.bgView.cornerRadius = 10
//            myCustomCell.backgroundView?.backgroundColor = UIColor.paledPurple
//
//        }
//
//        else {
//            let findDay = weekday(year: date.year, month: date.month, day: date.day)
//            myCustomCell.lbDate.text = "\(cellState.date.day)(\(findDay!))"
//            myCustomCell.lbDate.textColor = (arrDayString[cellState.day.rawValue] == "일") ? UIColor.Red : UIColor.puljaBlack
//            myCustomCell.backgroundView?.backgroundColor = UIColor.OffWhite
//        }
//
//
//
//        handleCellConfiguration(cell: myCustomCell, cellState: cellState)
//

    }
    

    
    func calendar(_ calendar: JTACMonthView, willDisplay cell: JTACDayCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        print("393번줄 들어옴")
        print(cellState.date)
        // This function should have the same code as the cellForItemAt function
        let myCustomCell = cell as! TodayCoachJTACalendarViewCell
        configureVisibleCell(myCustomCell: myCustomCell, cellState: cellState, date: date, indexPath: indexPath)
        
        if scheduleList != nil {
           
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let searchdate = formatter.string(from: cellState.date)
            let findSchedule = scheduleList!.filter { $0.scheduleDate == searchdate }
            
            
            
            if findSchedule.count != 0 {
                let oneSchedule = findSchedule[0]
                if oneSchedule.scheduleList!.count == 1 {
                    myCustomCell.time1.text = "\(oneSchedule.scheduleList![0].ctime!)"
                    myCustomCell.time2.text = ""
                    myCustomCell.time3.text = ""
                    
                } else if oneSchedule.scheduleList!.count == 2 {
                    myCustomCell.time1.text = "\(oneSchedule.scheduleList![0].ctime!)"
                    myCustomCell.time2.text = "\(oneSchedule.scheduleList![1].ctime!)"
                    myCustomCell.time3.text = ""

                } else if oneSchedule.scheduleList!.count == 3 {
                    myCustomCell.time1.text = "\(oneSchedule.scheduleList![0].ctime!)"
                    myCustomCell.time2.text = "\(oneSchedule.scheduleList![1].ctime!)"
                    myCustomCell.time3.text = "\(oneSchedule.scheduleList![2].ctime!)"
                    
                } else {
                    myCustomCell.time1.text = "\(oneSchedule.scheduleList![0].ctime!)"
                    myCustomCell.time2.text = "\(oneSchedule.scheduleList![1].ctime!)"
                    myCustomCell.time3.text = "+\(oneSchedule.scheduleList!.count - 2)"
                  
                }
            } else {
                myCustomCell.time1.text = "_"
                myCustomCell.time2.text = ""
                myCustomCell.time3.text = ""
            }

        } else {
            myCustomCell.time1.text = "_"
            myCustomCell.time2.text = ""
            myCustomCell.time3.text = ""
        }
    
    }
    
    func calendar(_ calendar: JTACMonthView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTACDayCell {
        print("400번줄 들어옴")
        let myCustomCell = calendar.dequeueReusableCell(withReuseIdentifier: "CalendarCoachWeekViewCell", for: indexPath) as! TodayCoachJTACalendarViewCell
        configureVisibleCell(myCustomCell: myCustomCell, cellState: cellState, date: date, indexPath: indexPath)
//        myCustomCell.frame.size.width = 50
//        myCustomCell.frame.size.height = 75
        
        if scheduleList != nil {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let searchdate = formatter.string(from: cellState.date)
            
            let findSchedule = scheduleList!.filter { $0.scheduleDate == searchdate }
            
            if findSchedule.count != 0 {
                
                print("찾은 스케줄: \(findSchedule[0])")
                let oneSchedule = findSchedule[0]
                if oneSchedule.scheduleList!.count == 1 {
                    myCustomCell.time1.text = "\(oneSchedule.scheduleList![0].ctime!)"
                    myCustomCell.time2.text = ""
                    myCustomCell.time3.text = ""
                    
                } else if oneSchedule.scheduleList!.count == 2 {
                    myCustomCell.time1.text = "\(oneSchedule.scheduleList![0].ctime!)"
                    myCustomCell.time2.text = "\(oneSchedule.scheduleList![1].ctime!)"
                    myCustomCell.time3.text = ""
                    
                } else if oneSchedule.scheduleList!.count == 3 {
                    
                    myCustomCell.time1.text = "\(oneSchedule.scheduleList![0].ctime!)"
                    myCustomCell.time2.text = "\(oneSchedule.scheduleList![1].ctime!)"
                    myCustomCell.time3.text = "\(oneSchedule.scheduleList![2].ctime!)"
                } else {
                    
                    myCustomCell.time1.text = "\(oneSchedule.scheduleList![0].ctime!)"
                    myCustomCell.time2.text = "\(oneSchedule.scheduleList![1].ctime!)"
                    myCustomCell.time3.text = "+\(oneSchedule.scheduleList!.count - 2)"
                }
            } else {
                myCustomCell.time1.text = "_"
                myCustomCell.time2.text = ""
                myCustomCell.time3.text = ""
            }

        } else {
            myCustomCell.time1.text = "_"
            myCustomCell.time2.text = ""
            myCustomCell.time3.text = ""
        }
        
        
        
        
        return myCustomCell
    }

    func calendar(_ calendar: JTACMonthView, didDeselectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        print("410번줄 들어옴")
        print(cellState.date)
        handleCellConfiguration(cell: cell, cellState: cellState)
        
        
        
        
    }
    func initializeShortView() {
        for (i, subview) in self.shortStackView.subviews.enumerated() {
            if i > 0 {
                subview.isHidden = true
            }
        }
    }

    func calendar(_ calendar: JTACMonthView, didSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        
        self.selectedDate = date
        
        print("415번줄 들어옴")
        print(date)
        handleCellConfiguration(cell: cell, cellState: cellState)
       
        if scheduleList != nil {
            let f = DateFormatter()
            f.dateFormat = "yyyy-MM-dd"
            let selectedDate = f.string(from: date)
            let findSchedule = scheduleList!.filter { $0.scheduleDate == selectedDate }
            if findSchedule.count > 0 {
                self.schedule = findSchedule[0].scheduleList
            } else {
                self.schedule = []
            }
            initializeShortView()
            self.setUpShortView()
//            if findSchedule.count > 0
//            {
//                self.setUpShortView()
//            }
            //set 스크롤뷰 높이
            self.setScrollViewHeight()
            
        }
            
        
    }
    
    func calendar(_ calendar: JTACMonthView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        
        print("After: \(calendar.currentSection()), visibleDates:\(visibleDates.monthDates.first?.date)")
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMdd"
        let startmon = formatter.string(from: self.getMonday(date: visibleDates.monthDates.first!.date))
        let endmon = formatter.string(from: self.getMonday(date: visibleDates.monthDates.first!.date).changeDays(addDays: 6))
        
        print("스크롤뒤, startmon: \(startmon) endmon: \(endmon)")
        
        
        guard let startDate = visibleDates.monthDates.first?.date else {
            return
        }
        
        
        var dateComponent = DateComponents()
        dateComponent.day = 1
        let mondayDate = Calendar.current.date(byAdding: dateComponent, to: startDate)
        if let currDate = mondayDate?.toString(.noDotdate) {
            setStudyScheduleWeekCount(startDate: currDate, endDate: currDate)
            self.getSchedule(startDate: startmon, endDate: endmon)
            
        }
        
        
    }
    
    func calendar(_ calendar: JTACMonthView, willScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        setupViewsOfCalendar(from: visibleDates)
    }
    
    
    func calendarSizeForMonths(_ calendar: JTACMonthView?) -> MonthSize? {
        return monthSize
    }
    
    
    
}

extension Date {
    func get(_ components: Calendar.Component..., calendar: Calendar = Calendar.current) -> DateComponents {
        return calendar.dateComponents(Set(components), from: self)
    }

    func get(_ component: Calendar.Component, calendar: Calendar = Calendar.current) -> Int {
        return calendar.component(component, from: self)
    }
    
    func changeDays(addDays: Int) -> Date {
        
        let greg = Calendar(identifier: .gregorian)
        var components = greg.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self)
//        components.year = year
//        components.month = month
//        components.day = day + addDays
        
        let newDate = greg.date(from: components)!
        return newDate
    }
    
}




