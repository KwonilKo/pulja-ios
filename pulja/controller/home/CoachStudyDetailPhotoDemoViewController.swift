//
//  CoachStudyDetailPhotoDemoViewController.swift
//  pulja
//
//  Created by kwonilko on 2022/03/26.
//

import UIKit

class CoachStudyDetailPhotoDemoViewController: UIViewController, UITableViewDataSource,
                                               UITableViewDelegate {
    
    
    var tag_idx : Int = 0
    var cellDatas : [StudyPhotoContent] = []
    var currentIdx : Int = 0
    var photoViewControllerDelegate : CoachStudyPhotoViewController?
    var userSeq : Int = 0
    var coachSeq : Int = 0
    var username: String = ""
    var totalElements : Int = 0
    let threshold = 20
    var page : Int = 0
    var last : Bool = false
    var index : Int = 0
    
    
    
    @IBOutlet var photoTableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        photoTableView.register(UITableViewCell.self, forCellReuseIdentifier: "photoTableViewCell")
        
        
        
        
        CommonUtil.coachTabbarController?.tabBar.isHidden = true
        CommonUtil.coachTabbarController?.bottomNavBar.isHidden = true
        
        
    }
    
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "photoTableView", for: indexPath)
//        let example = Example.allCases[indexPath.row]
//        cell.textLabel?.text = example.title
//        return cell
//    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = photoTableView.dequeueReusableCell(withIdentifier: "photoTableViewCell", for: indexPath)
        
        cell.textLabel?.text = cellDatas[indexPath.row].photo!
        print("셀 히나의 텍스트: \(cellDatas[indexPath.row].photo!)")
        
        return cell
    }
    
    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return 1
    }

}
