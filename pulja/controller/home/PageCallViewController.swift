//
//  PageCallViewController.swift
//  pulja
//
//  Created by 김병헌 on 2022/03/02.
//

import UIKit
import WebKit
import AirBridge
//import PageCallSDK

class PageCallViewController: PuljaBaseViewController, WKUIDelegate, WKNavigationDelegate  {
    
    var callUrl:String? = nil
    
    let containerView = UIView()
    
    var webView: WKWebView!
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var btClose: UIButton!
    
    @IBOutlet weak var contentView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let url = callUrl else {
            dismiss(animated: false)
            return
        }
        
        //page call sdk
//        loadPageCall(strUrl: url)
        
        
        loadWebView(strUrl: url)
    }
    
    override func loadView() {
        super.loadView()
        let contentController = WKUserContentController()
        AirBridge.webInterface()?.inject(to: contentController, withWebToken: Const.AB_WEB_TOKEN)
       let script =
           "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport');" +
               "meta.setAttribute('content', 'width=device-width, initial-scale = 1.0, user-scalable = no');" +
       "document.getElementsByTagName('head')[0].appendChild(meta);"
       let userScript = WKUserScript(source: script,
                                     injectionTime: WKUserScriptInjectionTime.atDocumentEnd,
                                     forMainFrameOnly: true)
       contentController.addUserScript(userScript)
       
       
       let preferences = WKPreferences()
       preferences.javaScriptEnabled = true
       
       let config = WKWebViewConfiguration()
       config.preferences = preferences
       config.userContentController = contentController
        
        config.mediaTypesRequiringUserActionForPlayback = [];
        config.allowsInlineMediaPlayback = true;
        config.suppressesIncrementalRendering = false;
        config.applicationNameForUserAgent = "PagecallIos"
               
        config.allowsAirPlayForMediaPlayback = true;
       if #available(iOS 13.0, *) {
           config.defaultWebpagePreferences.preferredContentMode = .mobile;
       }
       if #available(iOS 14.0, *) {
           config.limitsNavigationsToAppBoundDomains = true
       }
        
       
       self.webView = WKWebView(frame: .zero, configuration: config)
        let osVersion = UIDevice.current.systemVersion
        self.webView?.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/\(osVersion) Safari/605.1.15"
       self.webView?.frame = self.contentView.bounds
       self.webView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
       self.contentView.addSubview(self.webView!)
       self.view.bringSubviewToFront(self.topView)
       
       self.webView?.navigationDelegate = self
       self.webView?.uiDelegate = self
        
    }
    
    func loadWebView(strUrl: String){
        
        let urlString = "\(Const.DOMAIN)/student_conference_room?url=\(strUrl)"

        guard let url =  URL(string: urlString) else {
                   return
        }
       
        let urlRequest = URLRequest(url: url)
       
        if let authCookie = authCookie, let refreshCookie = refreshCookie {
           self.webView?.configuration.websiteDataStore.httpCookieStore.setCookie(authCookie, completionHandler: {
               self.webView?.configuration.websiteDataStore.httpCookieStore.setCookie(refreshCookie, completionHandler: {
                   self.webView?.load(urlRequest)
               })
           })
        }
    }
    
    private var authCookie: HTTPCookie? {
              let cookie = HTTPCookie(properties: [
               .domain: Const.COOKIE_DOMAIN,
                  .path: "/",
                  .name: "JWT-TOKEN",
                  .value: CommonUtil.shared.getAccessToken(),
              ])
              return cookie
          }
              
      private var refreshCookie: HTTPCookie? {
          let cookie = HTTPCookie(properties: [
              .domain: Const.COOKIE_DOMAIN,
              .path: "/",
              .name: "REFRESH-TOKEN",
              .value: CommonUtil.shared.getRefreshToken(),
          ])
          return cookie
      }
    
    
    
    // MARK: PageCall
//    func loadPageCall(strUrl: String) {
////           DispatchQueue.main.async {
//           let pageCall = PageCall.sharedInstance()
//           pageCall.delegate = self
//           #if DEBUG
//           #else
//           // pagecall log
//           pageCall.redirectLogToDocuments(withTimeInterval: 4)
//           #endif
//           // PageCall MainViewController present
////               pageCall.mainViewController!.modalPresentationStyle = .popover
//
//
//           let controller = pageCall.mainViewController!
//           self.addChild(controller)
//           controller.view.translatesAutoresizingMaskIntoConstraints = false
//           self.containerView.addSubview(controller.view)
//
//           NSLayoutConstraint.activate([
//            controller.view.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor),
//            controller.view.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor),
//            controller.view.topAnchor.constraint(equalTo: self.containerView.topAnchor),
//            controller.view.bottomAnchor.constraint(equalTo: self.containerView.bottomAnchor)
//           ])
//
//
//           self.containerView.translatesAutoresizingMaskIntoConstraints = false
//           self.view.addSubview(self.containerView)
//
//           NSLayoutConstraint.activate([
//            self.containerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 10),
//            self.containerView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -10),
//            self.containerView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 10),
//            self.containerView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -10),
//           ])
//
//           pageCall.webViewLoadRequest(withURLString: strUrl)
//
//
//           controller.didMove(toParent: self)
//
//           self.view.bringSubviewToFront(self.btClose)
//
////               self.present(pageCall.mainViewController!, animated: true, completion: {
////                   pageCall.webViewLoadRequest(withURLString: strUrl)
////               })
////           }
//    }
    
    
    
    @IBAction func close(_ sender: Any) {
        alert(title: "알림", message: "방에서 나가시겠습니까?", button: "나가기", subButton: "머무르기").done { b in
            if b == true {
                self.dismiss(animated: true)
            }
        }
        
    }
    

}


// MARK: PageCallDelegate
//extension PageCallViewController: PageCallDelegate {
//    func pageCallDidClose() {
//        print("pageCallDidClose")
//    }
//
//    func pageCallDidReceive(_ message: WKScriptMessage) {
//        print("pageCallDidReceive message")
//    }
//
//    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
//        print("webView decidePolicyFor navigationAction")
//        decisionHandler(.allow)
//    }
//}
