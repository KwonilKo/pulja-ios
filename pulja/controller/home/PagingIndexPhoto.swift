//import UIKit
//import Parchment
//
///// An implementation of the `PagingItem` protocol that stores the
///// index and title of a given item. The index property is needed to
///// make the `PagingItem` comparable.
//public struct PagingIndexPhoto: PagingItem, Hashable, Comparable {
////    public static func == (lhs: PagingIndexPhoto, rhs: PagingIndexPhoto) -> Bool {
////        return lhs.tag_idx == rhs.tag_idx
////    }
//    
//    /// The index of the `PagingItem` instance
////    public let index: Int
//
//    /// The title used in the menu cells.
////    public let title: String
//
//    /// Creates an instance of `PagingIndexItem`
//    ///
//    /// Parameter index: The index of the `PagingItem`.
//    /// Parameter title: The title used in the menu cells.
//    ///
//    ///
//    var tag_idx : Int = 0
//    var cellDatas : [StudyPhotoContent] = []
//    var currentIdx : Int = 0
//    var photoViewControllerDelegate : CoachStudyPhotoViewController?
//    var userSeq : Int = 0
//    var coachSeq : Int = 0
//    var username: String = ""
//    var totalElements : Int = 0
//    let threshold = 20
//    var page : Int = 0
//    var last : Bool = false
//    
//    public init(tag_idx: Int, cellDatas: [StudyPhotoContent], currentIdx: Int, userSeq: Int, coachSeq: Int, username: String,
//                totalElements: Int, threshold: Int, page: Int, last: Bool) {
//        self.tag_idx = tag_idx
//        self.cellDatas = cellDatas
//        self.currentIdx = currentIdx
//        self.userSeq = userSeq
//        self.coachSeq = coachSeq
//        self.username = username
//        self.totalElements = totalElements
//        self.threshold = threshold
//        self.page = page
//        self.last = last
//    }
//
//    public static func < (lhs: PagingIndexPhoto, rhs: PagingIndexPhoto) -> Bool {
//        return lhs.tag_idx < rhs.tag_idx
//    }
//}
