//
//  CoachStudyDetailPhotoViewController.swift
//  pulja
//
//  Created by kwonilko on 2022/03/22.
//

import UIKit
import SwiftUI


class CoachStudyDetailPhotoViewController: UIViewController, UIScrollViewDelegate {
    
    
    
    @IBOutlet var svProblemImg: UIScrollView!
    
    @IBOutlet var headerLabel: UILabel!
    
    @IBOutlet var currentOverTotal: UILabel!
    @IBOutlet var problemImg: UIImageView!
    
    
    var tag_idx : Int = 0
    var cellDatas : [StudyPhotoContent] = []
    var currentIdx : Int = 0
    var photoViewControllerDelegate : CoachStudyPhotoViewController?
    var userSeq : Int = 0
    var coachSeq : Int = 0
    var username: String = ""
    var totalElements : Int = 0
    let threshold = 20
    var page : Int = 0
    var last : Bool = false
    var index : Int = 0
    
    @IBOutlet var problemIVHeight: NSLayoutConstraint!
    var pageViewController : PhotoPageViewController!
    
    
//    var swipeRight: UISwipeGestureRecognizer =  {
//        let gesture = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
//        gesture.direction = .right
//        self.problemImg.addGestureRecognizer(gesture)
//    }
//    var swipeLeft : UISwipeGestureRecognizer
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        svProblemImg.minimumZoomScale = 1.0
        svProblemImg.maximumZoomScale = 2.0
        svProblemImg.delegate = self
        initialize()
        
        CommonUtil.coachTabbarController?.tabBar.isHidden = true
        CommonUtil.coachTabbarController?.bottomNavBar.isHidden = true
        
//        configure()
//        pageViewController.setViewControllers([self], direction: .forward, animated: true, completion: nil)
//
//        setupDelegate()
        
        
    }
    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.problemImg
    }
    
    
    
    func reloadImage() {
        let url = URL(string: cellDatas[currentIdx].photo!)
        problemImg.kf.indicatorType = .activity
        problemImg.kf.setImage(with: url,  options: [.transition(.fade(0.2))])
//        problemImg.contentMode = .top
        
        
//        let height = self.problemImg.contentClippingRect.height
//        if self.problemIVHeight.constant < height {
//            self.problemIVHeight.constant = height
//        }
        
        self.currentOverTotal.text = "\(self.currentIdx + 1) / \(self.totalElements)"
    }
    func reloadText() {
        guard let date = self.cellDatas[currentIdx].startTime else { return }
        let year = date.substring(from: 0, to: 3)
        let month = date.substring(from: 4, to: 5)
        let day = date.substring(from: 6, to: 7)
        let hour = date.substring(from: 8, to: 9)
        let min = date.substring(from: 10, to: 11)
        
        self.headerLabel.text = "\(username) | \(year).\(month).\(day) \(hour):\(min)"
    }
    
    
    func initialize() {
        
        reloadImage()
        reloadText()
        
        
        
//        //제스처 추가
//        self.problemImg.isUserInteractionEnabled = true
//        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
//        swipeRight.direction = .right
//        self.problemImg.addGestureRecognizer(swipeRight)
//
//        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
//        swipeLeft.direction = .left
//        self.problemImg.addGestureRecognizer(swipeLeft)
        
    }
    
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        initialize()
//        CommonUtil.coachTabbarController?.tabBar.isHidden = true
//        CommonUtil.coachTabbarController?.bottomNavBar.isHidden = true
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        CommonUtil.coachTabbarController?.bottomNavBar.isHidden = false
//        CommonUtil.coachTabbarController?.tabBar.isHidden = false
//    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
//    lazy var pageViewController : UIPageViewController = {
//        let vc = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
//        return vc
//    }()
//
//    lazy var navigationView: UIView = {
//            let view = UIView()
//            view.backgroundColor = .lightGray
//
//            return view
//    }()
//
//    func setupDelegate() {
//        pageViewController.delegate = self
//        pageViewController.dataSource = self
//    }
//
//    func configure() {
//        view.addSubview(navigationView)
//        addChild(pageViewController)
//        view.addSubview(pageViewController.view)
//
//        navigationView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
//        navigationView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
//        navigationView.heightAnchor.constraint(equalToConstant: 525.0).isActive = true
//        navigationView.topAnchor.constraint(equalTo: self.currentOverTotal.topAnchor, constant: 86.0).isActive = true
//
//        pageViewController.view.leadingAnchor.constraint(equalTo: navigationView.leadingAnchor).isActive = true
//        pageViewController.view.trailingAnchor.constraint(equalTo: navigationView.trailingAnchor).isActive = true
//        pageViewController.view.topAnchor.constraint(equalTo: navigationView.topAnchor).isActive = true
//        pageViewController.view.bottomAnchor.constraint(equalTo: navigationView.bottomAnchor).isActive = true
//
//       pageViewController.didMove(toParent: self)
//
//
//    }
    
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//
//        if segue.identifier == "photoPageViewController" {
//            print("데이터 뿌려주는거 성공")
//            guard let vc = segue.destination as? PhotoPageViewController else {return}
//            pageViewController = vc
//
//            pageViewController.completeHandler = { (result) in
//                print("photodetailphotoviewcontroller current index: \(result)")
////                if result == -1 {
////                    let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
////                    swipeRight.direction = .right
////                    self.respondToSwipeGesture(gesture: swipeRight)
////                } else {
////                    let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
////                    swipeLeft.direction = .left
////                    self.respondToSwipeGesture(gesture: swipeLeft)
////                }
////                self.currentIdx = result
//                self.initialize()
//            }
//
//        }
//    }
            
    
    
    @IBAction func buttonBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true, completion: {
            self.photoViewControllerDelegate?.cellDatas = self.cellDatas
            self.photoViewControllerDelegate?.page = self.page
            self.photoViewControllerDelegate?.photoCollectionView.reloadData()
        })
        
    }
    var doNotReloadImgLeft : Bool = false
    var doNotReloadImgRight : Bool = false
    
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {

        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            doNotReloadImgLeft = (self.currentIdx == 0) ? true : false
            doNotReloadImgRight = (self.currentIdx == self.totalElements - 1) ? true : false
            
            switch swipeGesture.direction {
            case .right:
                print("Swiped right")
                self.currentIdx = max(0, self.currentIdx - 1)
            case .left:
                print("Swiped left")
                self.currentIdx = min(self.totalElements - 1, self.currentIdx + 1)
            default:
                break
            }
            
            //handle index
            if (self.cellDatas.count - self.currentIdx <= threshold && swipeGesture.direction == .left && !self.last)  {
                self.page += 1
                LoadingView.show()
                CoachAPI.shared.getStudyImage(coachSeq: coachSeq, page: page, size: 20, userSeq: userSeq).done
                { res in
                    if let suc = res.success, suc == true {
                        guard let data = res.data else { return }
                        if let content = data.content {
                            self.cellDatas.append(contentsOf: content)
                        }
                    }
                }.catch { error in
                    print(error)
                }.finally {
                    LoadingView.hide()
                    self.reloadImage()
                }
            } else {
                if (!self.doNotReloadImgLeft && !self.doNotReloadImgRight)
                {
                    print("reload image 함")
                    self.reloadImage()
                } else { //test
                    print("reload image 안함")
                }
            }
            //test
            print("지금 나는 \(self.currentIdx + 1) 번째 사진, reload left 상태: \(self.doNotReloadImgLeft), reload right 상태: \(self.doNotReloadImgRight)")
            
        }
    }
    
    
    

}


//extension CoachStudyDetailPhotoViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
//
//    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
//
//        return self
//    }
//
//    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
//
//        return self
//
//    }
//
//    func numberOfViewControllers(in _: UIPageViewController) -> Int {
//        return 1
////        return self.cellDatas.count
//    }
//}
