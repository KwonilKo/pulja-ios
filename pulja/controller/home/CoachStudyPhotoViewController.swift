//
//  CoachStudyPhotoViewController.swift
//  pulja
//
//  Created by kwonilko on 2022/03/22.
//

import UIKit

class CoachStudyPhotoViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet var photoCollectionView: UICollectionView!
    
    
    var isPaging : Bool = false
    var hasNextPage : Bool = false
    var cellDatas: [StudyPhotoContent] = []
    var page : Int = 0
    var currPage : Int = 0
    var coachSeq : Int?
    var userSeq : Int?
    var username : String?
    var data: [StudentsSchedule]?
    var isSearchReload : Bool = false
    var tag_idx : Int = 0
    var showFooter : Bool = false
    var totalElements : Int = 0
    var last : Bool = false
        
    @IBOutlet var emptyView: UIView!
    
    
    @IBOutlet weak var topGradientView: UIView!
    
    override func viewDidLoad() {
        self.emptyView.isHidden = true 
        super.viewDidLoad()
        
        self.userSeq = self.data![tag_idx].userSeq
        self.username = self.data![tag_idx].username
        
//         defalt cell


        let startColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.60)
        let endColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.0)
          
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.topGradientView.applyGradient(isVertical: true, colorArray: [startColor, endColor])
        }
        

        // Do any additional setup after loading the view.
        self.photoCollectionView.delegate = self
        self.photoCollectionView.dataSource = self
        
        let layout = photoCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionHeadersPinToVisibleBounds = true
        
        self.photoCollectionView.register(CoachStudyPhotoCollectionViewCell.nib(), forCellWithReuseIdentifier: "CoachStudyPhotoCollectionViewCell")
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let height = scrollView.frame.height

        if offsetY > (contentHeight - height) {
            if isPaging == false && hasNextPage {
                beginPaging()
            }
        }

    }

    func beginPaging() {
        self.showFooter = true
        isPaging = true
        self.page += 1
        self.currPage = page
        self.startPaging()


    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isSearchReload || page == 0 {
            startPaging()
        }
        
    }
    
    func startPaging() {
        LoadingView.show()
        
        
        if page == 0 {
            self.cellDatas.removeAll()
        } else if isSearchReload == true {
            page = 0
            self.cellDatas.removeAll()
        }
        isSearchReload = false
        
        CoachAPI.shared.getStudyImage(coachSeq: coachSeq!, page: page, size: 20, userSeq: userSeq!).done { res in
            LoadingView.hide()
            DispatchQueue.main.async {
                self.showFooter = false 
            }
            
            guard let data = res.data else {
                // set empty view hidden = false
                return
            }
            
            if data.first! && data.empty! {
                //set empty view hidden = false
                self.emptyView.isHidden = false
                print("empty view hidden false ")
            } else {
                self.emptyView.isHidden = true
                print("empty view hidden true")
            }
            
            if let content = data.content {
                self.cellDatas.append(contentsOf: content)
            }
            self.totalElements = data.totalElements!
            self.last = data.last!
            
            
            self.hasNextPage = !data.last!
            self.isPaging = false
            
            self.showFooter = true
            self.photoCollectionView.reloadData()
            
            
        }.catch { error in
            print("api 실패")
            print(error)
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    { //
        return cellDatas.count
        
    }

    func processTime(stringDate: String?) -> String {
        guard let stringdate = stringDate else { return "" }
        let year = Int(stringdate.substring(from: 0, to: 3))
        let month = Int(stringdate.substring(from: 4, to: 5))
        let day = Int(stringdate.substring(from: 6, to: 7))
        return "\(month!).\(day!)(\(weekday(year: year!, month: month!, day: day!)!))"
        
    }
    // 특정 연도, 월, 일에 대한 요일을 구하는 메소드
    ///
    /// - Parameter year: 연도
    /// - Parameter month: 월
    /// - Parameter day: 일
    ///
    /// - Returns: 요일에 해당하는 문자열 Ex) Sun, Sunday, S
    /// - note: day에 대한 안정성을 보장하지 않음
    ///         Ex) 2019-02-40을 넣으면 2월1일로부터 40일이 되는날(3월12일)의 요일이 반환됨
    func weekday(year: Int, month: Int, day: Int) -> String? {
        let calendar = Calendar(identifier: .gregorian)
        
        guard let targetDate: Date = {
            let comps = DateComponents(calendar:calendar, year: year, month: month, day: day)
            return comps.date
            }() else { return nil }
        
        let day = Calendar.current.component(.weekday, from: targetDate) - 1
        let detailedDay:String? = Calendar.current.shortWeekdaySymbols[day]
//        let temp = Calendar.current.shortWeekdaySymbols[day]
//        let detailedDay : String?
//
//        switch temp {
//        case "Sun" :
//            detailedDay = "일"
//        case "Mon" :
//            detailedDay = "월"
//        case "Tue" :
//            detailedDay = "화"
//        case "Wed" :
//            detailedDay = "수"
//        case "Thu" :
//            detailedDay = "목"
//        case "Fri" :
//            detailedDay = "금"
//        default :
//            detailedDay = "토"
//        }
        print("\(month)월 \(day)일 \(detailedDay!)")
        return detailedDay
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CoachStudyPhotoCollectionViewCell", for: indexPath) as? CoachStudyPhotoCollectionViewCell else { return UICollectionViewCell() }
//        // identifier MemoCell !
//        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MemoCell", for: indexPath)
//        var memo = memos[indexPath.item]
//        // MemoCell ! if var memoCell = cell as? MemoCell {
//        //SDWebImage !
//        memoCell.imageView.sd_setImage(with: URL(string: memo["image"].stringValue), completed: nil) }
//        return cell
        
        
        
//        if cellDatas.count > 0 {
            
            let url = URL(string: cellDatas[indexPath.row].photo!)
//            let data = try? Data(contentsOf: url!)
            cell.imageView.kf.indicatorType = .activity
            cell.imageView.kf.setImage(with: url,  options: [.transition(.fade(0.2))])


//            cell.dateLabel.text = cellDatas[indexPath.row].startTime
        cell.dateLabel.text = self.processTime(stringDate: cellDatas[indexPath.row].startTime)
            
            print("cel contentview width: \(cell.contentView.frame.size.width), height: \(cell.contentView.frame.size.height)")
           

            
            
//            cell.backgroundColor = UIColor.white
//            cell.backView.backgroundColor = UIColor.white

            return cell
//        } else {
//            return UICollectionViewCell()
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonUtil.coachTabbarController?.tabBar.isHidden = true
        CommonUtil.coachTabbarController?.bottomNavBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CommonUtil.coachTabbarController?.bottomNavBar.isHidden = false
        CommonUtil.coachTabbarController?.tabBar.isHidden = false
    }
    
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        let actualPosition = scrollView.panGestureRecognizer.translation(in: scrollView.superview)
        if (actualPosition.y > 0){
            // Dragging down
           
        } else{
            // Dragging up
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
            print("여기여기여기 들어오!!")
            switch kind {
            case UICollectionView.elementKindSectionHeader:
                guard
                    let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "coachStudyCollectionReusableView", for: indexPath) as? CoachStudyCollectionReusableView else {
                        fatalError("Invalid view type")

                }
                headerView.totalView.backgroundColor = UIColor.clear
                
                print("hiii \(headerView.totalView.backgroundColor)")
//                headerView.backButton.backgroundColor = UIColor.clear
                return headerView
            case UICollectionView.elementKindSectionFooter:
                guard
                    let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "coachStudyPhotoFooterView", for: indexPath) as? CoachStudyPhotoFooterView else {
                        fatalError("Invalid view type")

                }
                let spinner = UIActivityIndicatorView()
                footerView.footerView.backgroundColor = UIColor.white
                spinner.center = footerView.footerView.center
                footerView.addSubview(spinner)
                if !showFooter {
                    footerView.isHidden = true
                } else {
                    footerView.isHidden = false
                }
                
                return footerView
            
            default:
                guard
                    let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "coachStudyCollectionReusableView", for: indexPath) as? CoachStudyCollectionReusableView else {
                        fatalError("Invalid view type")

                }
                return headerView
//                assert(false, "Invalid element type")
            }
        }
    
    

    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

// cell layout
extension CoachStudyPhotoViewController: UICollectionViewDelegateFlowLayout {

    // 위 아래 간격
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 1
//    }

    // 옆 간격
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 1
//    }

    // cell 사이즈( 옆 라인을 고려하여 설정 )
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

//        let width = collectionView.frame.width / 3 - 6 ///  3등분하여 배치, 옆 간격이 1이므로 1을 빼줌
        
        let width = (collectionView.frame.width - 12) / 3 ///  3등분하여 배치, 옆 간격이 1이므로 1을 빼줌
       

        let size = CGSize(width: width, height: width)
        print("cellsize출력: \(collectionView.frame.width) : \(size)")
        return size
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
      didSelectItemAt indexPath: IndexPath) {
//        let vc = R.storyboard.home.coachStudyDetailPhotoViewController()!
        let vc = R.storyboard.home.photoPageViewController()!
//        let vc = R.storyboard.home.coachStudyDetailPhotoDemoViewController()!
        
        //누르자 마자 api 호출
        LoadingView.show()
        CoachAPI.shared.getStudyImage(coachSeq: self.coachSeq!, page: page+1, size: 20, userSeq: self.userSeq!).done {
            res in
            if let suc = res.success, suc == true {
                guard let data = res.data else { return }

                
                if let content = data.content {
                    self.cellDatas.append(contentsOf: content)
                    
                }
                // 이전
//                let photo = PhotoPage(tag_idx: self.tag_idx, cellDatas: self.cellDatas, currentIdx: indexPath.row, photoViewControllerDelegate: self, userSeq: self.userSeq!, coachSeq: self.coachSeq!, username: self.username!, totalElements: self.totalElements,  page: self.page, last: self.last)
//                vc.photo = photo
                
                
                vc.page = self.page
                vc.last = self.last
                vc.cellDatas = self.cellDatas
                vc.tag_idx = self.tag_idx
                vc.photoViewControllerDelegate = self
                vc.userSeq = self.userSeq!
                vc.currentIdx = indexPath.row
                vc.coachSeq = self.coachSeq!
                vc.username = self.username!
                vc.totalElements = self.totalElements
//
                //이후
                
            }
        }.catch { error in
            print(error)
        }.finally {
            LoadingView.hide()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
      
    }
}

