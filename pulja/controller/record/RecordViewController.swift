//
//  RecordViewController.swift
//  pulja
//
//  Created by HUN on 2022/12/30.
//

import UIKit

class RecordViewController: PuljaBaseViewController {
    // MARK: -  iPad UI Constants
    @IBOutlet weak var todayCardTop: NSLayoutConstraint! // 32
    @IBOutlet weak var cardMargin1: NSLayoutConstraint! // 20
    @IBOutlet weak var cardMargin2: NSLayoutConstraint! // 20
    @IBOutlet weak var cardMargin3: NSLayoutConstraint! // 20
    @IBOutlet weak var cardMargin4: NSLayoutConstraint! // 20
    @IBOutlet weak var cardMargin5: NSLayoutConstraint! // 20
    @IBOutlet weak var cardMargin6: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin1: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin2: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin3: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin4: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin5: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin6: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin7: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin8: NSLayoutConstraint! // 32
    @IBOutlet weak var padMargin9: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin10: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin11: NSLayoutConstraint! // 26
    @IBOutlet weak var padMargin12: NSLayoutConstraint! // 26
    @IBOutlet weak var padMargin13: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin14: NSLayoutConstraint! // 28
    @IBOutlet weak var todayBadgeLabel: UILabel! // 16
    @IBOutlet weak var todayProblemLabel1: UILabel! // 18
    @IBOutlet weak var todayProblemLabel2: UILabel! // 30
    @IBOutlet weak var todayProblemLabel3: UILabel! // 18
    @IBOutlet weak var todayProblemLabel4: UILabel! // 16
    @IBOutlet weak var stampHeight: NSLayoutConstraint! // 89
    @IBOutlet weak var stampWidth1: NSLayoutConstraint! // 56
    @IBOutlet weak var stampWidth2: NSLayoutConstraint! // 56
    @IBOutlet weak var stampWidth3: NSLayoutConstraint! // 56
    @IBOutlet weak var stampWidth4: NSLayoutConstraint! // 56
    @IBOutlet weak var stampWidth5: NSLayoutConstraint! // 56
    @IBOutlet weak var stampWidth6: NSLayoutConstraint! // 56
    @IBOutlet weak var stampWidth7: NSLayoutConstraint! // 56
    @IBOutlet weak var stampHeight1: NSLayoutConstraint! // 56
    @IBOutlet weak var stampHeight2: NSLayoutConstraint! // 56
    @IBOutlet weak var stampHeight3: NSLayoutConstraint! // 56
    @IBOutlet weak var stampHeight4: NSLayoutConstraint! // 56
    @IBOutlet weak var stampHeight5: NSLayoutConstraint! // 56
    @IBOutlet weak var stampHeight6: NSLayoutConstraint! // 56
    @IBOutlet weak var stampHeight7: NSLayoutConstraint! // 56
    @IBOutlet weak var secondTitle1: UILabel! // 22
    @IBOutlet weak var secondTitle2: UILabel! // 22
    @IBOutlet weak var secondTitle3: UILabel! // 22
    @IBOutlet weak var thirdTitle1: UILabel! // 22
    @IBOutlet weak var thirdTitle2: UILabel! // 22
    @IBOutlet weak var thirdTitle3: UILabel! // 22
    @IBOutlet weak var fourthTitle1: UILabel! // 22
    @IBOutlet weak var fourthTitle2: UILabel! // 22
    @IBOutlet weak var fourthTitle3: UILabel! // 22
    @IBOutlet weak var fourthTitle4: UILabel! // 22
    @IBOutlet weak var fifthTitle1: UILabel! // 22
    @IBOutlet weak var fifthTitle2: UILabel! // 22
    @IBOutlet weak var fifthTitle3: UILabel! // 22
    @IBOutlet weak var fifthTitle4: UILabel! // 22
    
    private func setPadConstants() {
        todayCardTop.constant = 32
        cardMargin1.constant = 20
        cardMargin2.constant = 20
        cardMargin3.constant = 20
        cardMargin4.constant = 20
        cardMargin5.constant = 20
        cardMargin6.constant = 28
        
        padMargin1.constant = 28
        padMargin2.constant = 28
        padMargin3.constant = 28
        padMargin4.constant = 28
        padMargin5.constant = 28
        padMargin6.constant = 28
        padMargin7.constant = 28
        padMargin8.constant = 32
        padMargin9.constant = 28
        padMargin10.constant = 28
        padMargin11.constant = 26
        padMargin12.constant = 26
        padMargin13.constant = 28
        padMargin14.constant = 28
        
        todayBadgeLabel.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        todayProblemLabel1.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        todayProblemLabel2.font = UIFont.systemFont(ofSize: 30, weight: .bold)
        todayProblemLabel3.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        todayProblemLabel4.font = UIFont.systemFont(ofSize: 16, weight: .regular)

        stampHeight.constant = 89
        stampWidth1.constant = 56
        stampWidth2.constant = 56
        stampWidth3.constant = 56
        stampWidth4.constant = 56
        stampWidth5.constant = 56
        stampWidth6.constant = 56
        stampWidth7.constant = 56
        stampHeight1.constant = 56
        stampHeight2.constant = 56
        stampHeight3.constant = 56
        stampHeight4.constant = 56
        stampHeight5.constant = 56
        stampHeight6.constant = 56
        stampHeight7.constant = 56
        
        secondTitle1.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        secondTitle2.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        secondTitle3.font = UIFont.systemFont(ofSize: 22, weight: .bold)

        thirdTitle1.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        thirdTitle2.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        thirdTitle3.font = UIFont.systemFont(ofSize: 22, weight: .bold)

        fourthTitle1.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        fourthTitle2.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        fourthTitle3.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        fourthTitle4.font = UIFont.systemFont(ofSize: 22, weight: .bold)

        fifthTitle1.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        fifthTitle2.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        fifthTitle3.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        fifthTitle4.font = UIFont.systemFont(ofSize: 22, weight: .bold)
    }
    
    // MARK: -  Scroll
    @IBOutlet weak var recordScrollView: UIScrollView!
    @IBOutlet weak var secondAnimChecker: UIView!
    @IBOutlet weak var thirdAnimChecker: UIView!
    @IBOutlet weak var fourthAnimChecker: UIView!
    @IBOutlet weak var fifthAnimChecker: UIView!
    
    // MARK: -  override
    override func viewDidLoad() {
        super.viewDidLoad()
        
        recordScrollView.delegate = self
        
        //ab180 이벤트 보내기
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: "pulja2.0_record_solve_enter",
                    customs: ["userseq": userSeq, "user_id": userId]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "category: pulja2.0_record_solve_enter", separator: " ")
                print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                print("[ABLog]", "user_id: \(userId)", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
            
            // colection banner
//            let bannerCheck1 = "isCollectionBanner_\(userSeq)"
//            let bannerCheck2 = "isCollectionBannerChecked_\(userSeq)"
//
//            UserDefaults.standard.removeObject(forKey: bannerCheck1)
//            UserDefaults.standard.removeObject(forKey: bannerCheck2)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // iPad
        if (UIDevice.current.userInterfaceIdiom == .pad) {
            setPadConstants()
        }
        
        initAnimation()
        LoadingView.show()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
            self.initData()
        }
    }
    
    // MARK: -  Set Data
    @IBOutlet weak var stampImage1: UIImageView!
    @IBOutlet weak var stampImage2: UIImageView!
    @IBOutlet weak var stampImage3: UIImageView!
    @IBOutlet weak var stampImage4: UIImageView!
    @IBOutlet weak var stampImage5: UIImageView!
    @IBOutlet weak var stampImage6: UIImageView!
    @IBOutlet weak var stampImage7: UIImageView!
    @IBOutlet weak var thirdTodayCount: UILabel!
    @IBOutlet weak var thirdYesterdayCount: UILabel!
    @IBOutlet weak var weekCount1: UILabel!
    @IBOutlet weak var weekCount2: UILabel!
    @IBOutlet weak var weekCount3: UILabel!
    @IBOutlet weak var weekCount4: UILabel!
    @IBOutlet weak var weekCount5: UILabel!
    @IBOutlet weak var weekCount6: UILabel!
    @IBOutlet weak var weekCount7: UILabel!
    @IBOutlet weak var lastWeekCorrectRate: UILabel!
    @IBOutlet weak var thisWeekCorrectRate: UILabel!
    
    private var weekCntList: [Int] = []
    private var todayCnt = 0
    private var yesterdayCnt = 0
    private var todayStudyDuration = ""
    private var yesterdayStudyDuration = ""
    private var isTodayDuration = false
    private var moreDuration = 0
    private var lastCorrectAngle = 0
    private var thisCorrectAngle = 0
    
    private var isSetData = false
    private func initData() {
        if let userSeq = self.myInfo?.userSeq {
            CurationAPI.shared.studyHistory(userSeq: Int(userSeq)).done { res in
                if let data = res.data {
                    //print("!!!-> \(data)")
                    self.isSetData = true
                    // 푼 문제 수
                    if let todaySolveCnt = data.todaySolveCnt {
                        self.todayCnt = todaySolveCnt
                        self.todayProblemLabel2.text = String(todaySolveCnt)
                        self.thirdTodayCount.text = String(todaySolveCnt)
                    }
                    
                    if let yesterdaySolveCnt = data.yesterdaySolveCnt {
                        self.yesterdayCnt = yesterdaySolveCnt
                        self.thirdYesterdayCount.text = String(yesterdaySolveCnt)
                    }
                    
                    // third title
                    self.thirdTitle1.text = "오늘은 어제보다"
                    if (self.yesterdayCnt > self.todayCnt) {
                        let cnt = self.yesterdayCnt - self.todayCnt
                        self.thirdTitle2.text = "\(cnt)개 "
                        
                        self.thirdTitle3.text = "덜 풀었어요"
                    } else if (self.yesterdayCnt < self.todayCnt) {
                        let cnt = self.todayCnt - self.yesterdayCnt
                        self.thirdTitle2.text = "\(cnt)개 "
                        
                        self.thirdTitle3.text = "더 풀었어요"
                    } else {
                        self.thirdTitle1.text = "오늘은 어제와"
                        self.thirdTitle2.text = ""
                        self.thirdTitle3.text = "동일하게 풀었어요"
                    }
                    
                    if let weekSolveCntList = data.weekSolveCntList {
                        self.weekCntList = weekSolveCntList
                        //print("weekSolveCntList-> \(self.weekCntList)")
                        // 학습도장
                        var count = 0
                        var stamp = 0
                        var studyAll = 0
                        for cnt in weekSolveCntList {
                            studyAll += cnt
                            let cntCheck = cnt > 0
                            switch count {
                            case 0 :
                                // stamp1
                                if (cntCheck) {
                                    self.stampImage1.image = UIImage(named : "iconStampOn")
                                    self.weekCount1.text = String(cnt)
                                    self.monTooltip.isHidden = false
                                    stamp += 1
                                } else {
                                    self.stampImage1.image = UIImage(named : "iconStampOff")
                                    self.monTooltip.isHidden = true
                                }
                            case 1 :
                                // stamp2
                                if (cntCheck) {
                                    self.stampImage2.image = UIImage(named : "iconStampOn")
                                    self.weekCount2.text = String(cnt)
                                    self.tueTooltip.isHidden = false
                                    stamp += 1
                                } else {
                                    self.stampImage2.image = UIImage(named : "iconStampOff")
                                    self.tueTooltip.isHidden = true
                                }
                            case 2 :
                                // stamp3
                                if (cntCheck) {
                                    self.stampImage3.image = UIImage(named : "iconStampOn")
                                    self.weekCount3.text = String(cnt)
                                    self.wedTooltip.isHidden = false
                                    stamp += 1
                                } else {
                                    self.stampImage3.image = UIImage(named : "iconStampOff")
                                    self.wedTooltip.isHidden = true
                                }
                            case 3 :
                                // stamp4
                                if (cntCheck) {
                                    self.stampImage4.image = UIImage(named : "iconStampOn")
                                    self.weekCount4.text = String(cnt)
                                    self.thuTooltip.isHidden = false
                                    stamp += 1
                                } else {
                                    self.stampImage4.image = UIImage(named : "iconStampOff")
                                    self.thuTooltip.isHidden = true
                                }
                            case 4 :
                                // stamp5
                                if (cntCheck) {
                                    self.stampImage5.image = UIImage(named : "iconStampOn")
                                    self.weekCount5.text = String(cnt)
                                    self.friTooltip.isHidden = false
                                    stamp += 1
                                } else {
                                    self.stampImage5.image = UIImage(named : "iconStampOff")
                                    self.friTooltip.isHidden = true
                                }
                            case 5 :
                                // stamp6
                                if (cntCheck) {
                                    self.stampImage6.image = UIImage(named : "iconStampOn")
                                    self.weekCount6.text = String(cnt)
                                    self.satTooltip.isHidden = false
                                    stamp += 1
                                } else {
                                    self.stampImage6.image = UIImage(named : "iconStampOff")
                                    self.satTooltip.isHidden = true
                                }
                            default :
                                // stamp7
                                if (cntCheck) {
                                    self.stampImage7.image = UIImage(named : "iconStampOn")
                                    self.weekCount7.text = String(cnt)
                                    self.sunTooltip.isHidden = false
                                    stamp += 1
                                } else {
                                    self.stampImage7.image = UIImage(named : "iconStampOff")
                                    self.sunTooltip.isHidden = true
                                }
                            }
                            count += 1
                            
                            self.todayProblemLabel4.text = "\(stamp)개"
                            
                            // fourth title
                            if (studyAll > 0) {
                                self.fourthTitle1.text = "이번주에"
                                self.fourthTitle2.text = "총 "
                                self.fourthTitle3.text = "\(studyAll)문제"
                                self.fourthTitle4.text = "를 풀었어요"
                            } else {
                                self.fourthTitle1.text = "이번 주엔 아직"
                                self.fourthTitle2.text = "학습을 시작하지 않았어요"
                                self.fourthTitle3.text = ""
                                self.fourthTitle4.text = ""
                            }
                        }
                    }
                    
                    // 어제 공부시간
                    var yesterdaySolve = 0
                    if let yesterdaySolveDuration = data.yesterdaySolveDuration {
                        yesterdaySolve = yesterdaySolveDuration
                        let (h,m,s) = self.secondsToHoursMinutesSeconds(seconds: yesterdaySolveDuration)
                        var hour = "\(h)"
                        if h < 10 {
                            hour = "0\(h)"
                        }
                        var minute = "\(m)"
                        if m < 10 {
                            minute = "0\(m)"
                        }
                        var second = "\(s)"
                        if s < 10 {
                            second = "0\(s)"
                        }
                        self.yesterdayStudyDuration = "\(hour):\(minute):\(second)"
                    }
                    
                    // 오늘 공부시간
                    var todaySolve = 0
                    if let todaySolveDuration = data.todaySolveDuration {
                        todaySolve = todaySolveDuration
                        let (h,m,s) = self.secondsToHoursMinutesSeconds(seconds: todaySolveDuration)
                        var hour = "\(h)"
                        if h < 10 {
                            hour = "0\(h)"
                        }
                        var minute = "\(m)"
                        if m < 10 {
                            minute = "0\(m)"
                        }
                        var second = "\(s)"
                        if s < 10 {
                            second = "0\(s)"
                        }
                        self.todayStudyDuration = "\(hour):\(minute):\(second)"
                    }
                    // second title
                    self.secondTitle1.text = "오늘은 어제보다"
                    if (yesterdaySolve > todaySolve) {
                        let studyTime = (yesterdaySolve / 60) - (todaySolve / 60)
                        if studyTime > 0 {
                            self.secondTitle2.text = "\(studyTime)분 "
                            
                        } else {
                            self.secondTitle2.text = "1분 "
                        }
                        
                        self.secondTitle3.text = "덜 공부했어요"
                    } else if (yesterdaySolve < todaySolve) {
                        let studyTime = (todaySolve / 60) - (yesterdaySolve / 60)
                        if studyTime > 0 {
                            self.secondTitle2.text = "\(studyTime)분 "
                            
                        } else {
                            self.secondTitle2.text = "1분 "
                        }
                        
                        self.secondTitle3.text = "더 공부했어요"
                    } else {
                        self.secondTitle1.text = "오늘은 어제와"
                        self.secondTitle2.text = ""
                        self.secondTitle3.text = "동일하게 공부했어요"
                    }
                    
                    // fifth title
                    var lastCorrect = 0
                    if let lastWeekCorrectRate = data.lastWeekCorrectRate {
                        lastCorrect = lastWeekCorrectRate
                        self.lastWeekCorrectRate.text = String(lastWeekCorrectRate)
                        self.lastCorrectAngle = lastWeekCorrectRate * 360 / 100
                    }
                    
                    var thisCorrect = 0
                    if let thisWeekCorrectRate = data.thisWeekCorrectRate {
                        thisCorrect = thisWeekCorrectRate
                        self.thisWeekCorrectRate.text = String(thisWeekCorrectRate)
                        self.thisCorrectAngle = thisWeekCorrectRate * 360 / 100
                    }
                    
                    self.fifthTitle1.text = "지난주보다"
                    self.fifthTitle2.text = "정답률이 "
                    if (lastCorrect > thisCorrect) {
                        self.fifthTitle3.text = "\(lastCorrect - thisCorrect)% "
                        self.fifthTitle4.text = "감소했어요"
                    } else if (lastCorrect < thisCorrect) {
                        self.fifthTitle3.text = "\(thisCorrect - lastCorrect)% "
                        self.fifthTitle4.text = "올랐어요"
                    } else {
                        self.fifthTitle1.text = "지난주와"
                        self.fifthTitle3.text = "\(thisCorrect)%"
                        self.fifthTitle4.text = "로 동일해요"
                    }
                    
                    // start second animation
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                        let height = self.recordScrollView.frame.height
                        let second = self.recordScrollView.convert(self.secondAnimChecker.frame.origin, to: nil)
                        if height - second.y > 500 {
                            self.startSecondAnimation()
                        }
                    }
                    
                }
            }
            .catch { err in
                LoadingView.hide()
            }.finally {
                LoadingView.hide()
            }
        }
    }
    
    private func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
      return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    // MARK: - Animation 초기화
    private func initAnimation() {
        // scroll to top
        recordScrollView.setContentOffset(CGPointMake(0, 0), animated: false)
        
        // second
        secondAnimationChecked = false
        todayScrollLabel.configure(with: "00:00:00")
        yesterdayScrollLabel.configure(with: "00:00:00")
        
        // third
        thirdAnimationChecked = false
        yesterdayChartFillSize.constant = 15
        todayChartFillSize.constant = 15
        yesterdayTooltipMargin.constant = 0
        todayTooltipMargin.constant = 0
        yesterdayTooltip.alpha = 0
        todayTooltip.alpha = 0
        
        // fourth
        fourthAnimationChecked = false
        monChartFillSize.constant = 15
        tueChartFillSize.constant = 15
        wedChartFillSize.constant = 15
        thuChartFillSize.constant = 15
        friChartFillSize.constant = 15
        satChartFillSize.constant = 15
        sunChartFillSize.constant = 15
        monTooltipMargin.constant = 0
        tueTooltipMargin.constant = 0
        wedTooltipMargin.constant = 0
        thuTooltipMargin.constant = 0
        friTooltipMargin.constant = 0
        satTooltipMargin.constant = 0
        sunTooltipMargin.constant = 0
        monTooltip.alpha = 0
        tueTooltip.alpha = 0
        wedTooltip.alpha = 0
        thuTooltip.alpha = 0
        friTooltip.alpha = 0
        satTooltip.alpha = 0
        sunTooltip.alpha = 0
        
        // fifth
        fifthAnimationChecked = false
        lastWeekProgress.animate(toAngle: Double(0), duration: 0, completion: nil)
        thisWeekProgress.animate(toAngle: Double(0), duration: 0, completion: nil)
    }
    
    // MARK: - 공부시간 Slot Label Animation
    @IBOutlet weak var todayScrollLabel: SlotScrollLabel!
    @IBOutlet weak var yesterdayScrollLabel: SlotScrollLabel!
    private var secondAnimationChecked = false
    
    private func startSecondAnimation() {
        if secondAnimationChecked { return }
        print("startSecondAnimation()")
        yesterdayScrollLabel.configure(with: yesterdayStudyDuration)
        yesterdayScrollLabel.animate()
        todayScrollLabel.configure(with: todayStudyDuration)
        todayScrollLabel.animate()
        secondAnimationChecked = true
    }
    
    // MARK: - 어제 오늘 비교 Chart Animation
    @IBOutlet weak var thirdChartBar: UIView!
    @IBOutlet weak var yesterdayChartFillSize: NSLayoutConstraint!
    @IBOutlet weak var yesterdayTooltipMargin: NSLayoutConstraint!
    @IBOutlet weak var todayChartFillSize: NSLayoutConstraint!
    @IBOutlet weak var todayTooltipMargin: NSLayoutConstraint!
    @IBOutlet weak var yesterdayTooltip: UIImageView!
    @IBOutlet weak var todayTooltip: UIImageView!
    private var thirdAnimationChecked = false
    
    private func startThirdAnimation() {
        if thirdAnimationChecked { return }
        print("startThirdAnimation()")
        
        let height = (thirdChartBar.frame.height - 56)
        
        var delay = 0.0
        if yesterdayCnt > 0 {
            delay = 0.35
            if yesterdayCnt > todayCnt {
                yesterdayChartFillSize.constant = height
            } else {
                let t = Double(todayCnt) / Double(yesterdayCnt)
                yesterdayChartFillSize.constant = height / t
            }
        }
        
        UIView.animate(
            withDuration: 0.7,
            delay: 0,
            options: .allowUserInteraction,
            animations: {
                self.view.layoutIfNeeded()
            }
        )
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delay) {
            if self.todayCnt > 0 {
                if self.todayCnt > self.yesterdayCnt {
                    self.todayChartFillSize.constant = height
                } else {
                    let t = Double(self.yesterdayCnt) / Double(self.todayCnt)
                    self.todayChartFillSize.constant = height / t
                }
            }
            
            UIView.animate(
                withDuration: 0.7,
                delay: 0,
                options: .allowUserInteraction,
                animations: {
                    self.view.layoutIfNeeded()
                }
            ) { _ in
                UIView.animate(withDuration: 0.2) {
                    self.todayTooltip.alpha = 1
                    self.yesterdayTooltip.alpha = 1
                    self.todayTooltipMargin.constant = 16
                    self.yesterdayTooltipMargin.constant = 16
                    UIView.animate(
                        withDuration: 0.2,
                        delay: 0,
                        options: .allowUserInteraction,
                        animations: {
                            self.view.layoutIfNeeded()
                        }
                    )
                }
            }
        }
        
        thirdAnimationChecked = true
    }
    
    // MARK: - 이번주 푼 문제수 Chart Animation
    @IBOutlet weak var fourthChartBar: UIView!
    @IBOutlet weak var monChartFillSize: NSLayoutConstraint!
    @IBOutlet weak var monTooltipMargin: NSLayoutConstraint!
    @IBOutlet weak var tueChartFillSize: NSLayoutConstraint!
    @IBOutlet weak var tueTooltipMargin: NSLayoutConstraint!
    @IBOutlet weak var wedChartFillSize: NSLayoutConstraint!
    @IBOutlet weak var wedTooltipMargin: NSLayoutConstraint!
    @IBOutlet weak var thuChartFillSize: NSLayoutConstraint!
    @IBOutlet weak var thuTooltipMargin: NSLayoutConstraint!
    @IBOutlet weak var friChartFillSize: NSLayoutConstraint!
    @IBOutlet weak var friTooltipMargin: NSLayoutConstraint!
    @IBOutlet weak var satChartFillSize: NSLayoutConstraint!
    @IBOutlet weak var satTooltipMargin: NSLayoutConstraint!
    @IBOutlet weak var sunChartFillSize: NSLayoutConstraint!
    @IBOutlet weak var sunTooltipMargin: NSLayoutConstraint!
    @IBOutlet weak var monTooltip: UIImageView!
    @IBOutlet weak var tueTooltip: UIImageView!
    @IBOutlet weak var wedTooltip: UIImageView!
    @IBOutlet weak var thuTooltip: UIImageView!
    @IBOutlet weak var friTooltip: UIImageView!
    @IBOutlet weak var satTooltip: UIImageView!
    @IBOutlet weak var sunTooltip: UIImageView!
    @IBOutlet weak var monChartFill: UIView!
    @IBOutlet weak var tueChartFill: UIView!
    @IBOutlet weak var wedChartFill: UIView!
    @IBOutlet weak var thuChartFill: UIView!
    @IBOutlet weak var friChartFill: UIView!
    @IBOutlet weak var satChartFill: UIView!
    @IBOutlet weak var sunChartFill: UIView!
    private var fourthAnimationChecked = false
    
    private func startFourthAnimation() {
        if fourthAnimationChecked { return }
        print("startFourthAnimation()")
        
        // get today
        let nowDate = Date() // 현재의 Date 날짜 및 시간
        let dateFormatter = DateFormatter() // Date 포맷 객체 선언
        dateFormatter.locale = Locale(identifier: "ko") // 한국 지정
        dateFormatter.dateFormat = "E요일" // Date 포맷 타입 지정
        let date_string = dateFormatter.string(from: nowDate)
        var currentDateIndex = 0
        switch date_string {
        case "월요일":
            monChartFill.backgroundColor = UIColor.Purple
        case "화요일":
            currentDateIndex = 1
            tueChartFill.backgroundColor = UIColor.Purple
        case "수요일":
            currentDateIndex = 2
            wedChartFill.backgroundColor = UIColor.Purple
        case "목요일":
            currentDateIndex = 3
            thuChartFill.backgroundColor = UIColor.Purple
        case "금요일":
            currentDateIndex = 4
            friChartFill.backgroundColor = UIColor.Purple
        case "토요일":
            currentDateIndex = 5
            satChartFill.backgroundColor = UIColor.Purple
        default:
            currentDateIndex = 6
            sunChartFill.backgroundColor = UIColor.Purple
        }
        
        var maxCnt = 0
        var count = 0
        for cnt in weekCntList {
            
            if cnt > maxCnt {
                maxCnt = cnt
            }
            
            if count != currentDateIndex {
                switch count {
                case 0 :
                    if cnt > 0 {
                        monChartFill.backgroundColor = UIColor.Paleblue
                    }
                case 1 :
                    if cnt > 0 {
                        tueChartFill.backgroundColor = UIColor.Paleblue
                    }
                case 2 :
                    if cnt > 0 {
                        wedChartFill.backgroundColor = UIColor.Paleblue
                    }
                case 3 :
                    if cnt > 0 {
                        thuChartFill.backgroundColor = UIColor.Paleblue
                    }
                case 4 :
                    if cnt > 0 {
                        friChartFill.backgroundColor = UIColor.Paleblue
                    }
                case 5 :
                    if cnt > 0 {
                        satChartFill.backgroundColor = UIColor.Paleblue
                    }
                default:
                    if cnt > 0 {
                        sunChartFill.backgroundColor = UIColor.Paleblue
                    }
                }
            }
            count += 1
        }
        
        var height = (fourthChartBar.frame.height - 48)
        
        if maxCnt == 0 {
           height = 15
        }
        
        //print("weekSolveCntList fourth -> \(self.weekCntList)")
        if weekCntList[0] == maxCnt{
            monChartFillSize.constant = height
        } else {
            if weekCntList[0] > 0 {
                let t = Double(maxCnt) / Double(weekCntList[0])
                monChartFillSize.constant = height / t
            }
        }
        
        if weekCntList[1] == maxCnt{
            tueChartFillSize.constant = height
        } else {
            if weekCntList[1] > 0 {
                let t = Double(maxCnt) / Double(weekCntList[1])
                tueChartFillSize.constant = height / t
            }
        }
        
        if weekCntList[2] == maxCnt{
            wedChartFillSize.constant = height
        } else {
            if weekCntList[2] > 0 {
                let t = Double(maxCnt) / Double(weekCntList[2])
                wedChartFillSize.constant = height / t
            }
        }
        
        if weekCntList[3] == maxCnt{
            thuChartFillSize.constant = height
        } else {
            if weekCntList[3] > 0 {
                let t = Double(maxCnt) / Double(weekCntList[3])
                thuChartFillSize.constant = height / t
            }
        }
        
        if weekCntList[4] == maxCnt{
            friChartFillSize.constant = height
        } else {
            if weekCntList[4] > 0 {
                let t = Double(maxCnt) / Double(weekCntList[4])
                friChartFillSize.constant = height / t
            }
        }
        
        if weekCntList[5] == maxCnt{
            satChartFillSize.constant = height
        } else {
            if weekCntList[5] > 0 {
                let t = Double(maxCnt) / Double(weekCntList[5])
                satChartFillSize.constant = height / t
            }
        }
        
        if weekCntList[6] == maxCnt{
            sunChartFillSize.constant = height
        } else {
            if weekCntList[6] > 0 {
                let t = Double(maxCnt) / Double(weekCntList[6])
                sunChartFillSize.constant = height / t
            }
        }
        
        UIView.animate(
            withDuration: 0.7,
            delay: 0,
            options: .allowUserInteraction,
            animations: {
                self.view.layoutIfNeeded()
            }
        ) { _ in
            UIView.animate(withDuration: 0.2) {
                self.monTooltip.alpha = 1
                self.tueTooltip.alpha = 1
                self.wedTooltip.alpha = 1
                self.thuTooltip.alpha = 1
                self.friTooltip.alpha = 1
                self.satTooltip.alpha = 1
                self.sunTooltip.alpha = 1
                self.monTooltipMargin.constant = 16
                self.tueTooltipMargin.constant = 16
                self.wedTooltipMargin.constant = 16
                self.thuTooltipMargin.constant = 16
                self.friTooltipMargin.constant = 16
                self.satTooltipMargin.constant = 16
                self.sunTooltipMargin.constant = 16
                
                UIView.animate(
                    withDuration: 0.2,
                    delay: 0,
                    options: .allowUserInteraction,
                    animations: {
                        self.view.layoutIfNeeded()
                    }
                )
            }
        }
        
        fourthAnimationChecked = true
    }
    
    // MARK: - 정답률 CircleProgress Animation
    @IBOutlet weak var lastWeekProgress: KDCircularProgress!
    @IBOutlet weak var thisWeekProgress: KDCircularProgress!
    private var fifthAnimationChecked = false
    
    private func startFifthAnimation() {
        if fifthAnimationChecked { return }
        print("startFifthAnimation()")
       
        lastWeekProgress.animate(toAngle: Double(lastCorrectAngle), duration: 0.5, completion: nil)
        thisWeekProgress.animate(toAngle: Double(thisCorrectAngle), duration: 0.5, completion: nil)
        
        fifthAnimationChecked = true
    }
}

extension RecordViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == recordScrollView {
            if isSetData {
                let height = scrollView.frame.height
                let second = scrollView.convert(self.secondAnimChecker.frame.origin, to: nil)
                let third = scrollView.convert(self.thirdAnimChecker.frame.origin, to: nil)
                let fourth = scrollView.convert(self.fourthAnimChecker.frame.origin, to: nil)
                let fifth = scrollView.convert(self.fifthAnimChecker.frame.origin, to: nil)
                
                if height - second.y > 500 {
                    startSecondAnimation()
                }
                
                if height - third.y > 1000 {
                    startThirdAnimation()
                }
                
                if height - fourth.y > 1500 {
                    startFourthAnimation()
                }
                
                if height - fifth.y > 2000 {
                    startFifthAnimation()
                }
            }
        }
    }
}
