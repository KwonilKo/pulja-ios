//
//  RecordEmptyViewController.swift
//  pulja
//
//  Created by HUN on 2022/12/30.
//

import UIKit
import MaterialComponents

class RecordEmptyViewController: PuljaBaseViewController, MDCBottomSheetControllerDelegate {
    // MARK: -  iPad UI Constants
    @IBOutlet weak var todayCardTop: NSLayoutConstraint! // 32
    @IBOutlet weak var cardMargin1: NSLayoutConstraint! // 20
    @IBOutlet weak var cardMargin2: NSLayoutConstraint! // 20
    @IBOutlet weak var cardMargin3: NSLayoutConstraint! // 20
    @IBOutlet weak var cardMargin4: NSLayoutConstraint! // 20
    @IBOutlet weak var cardMargin5: NSLayoutConstraint! // 20
    @IBOutlet weak var cardMargin6: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin1: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin2: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin3: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin4: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin5: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin6: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin7: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin8: NSLayoutConstraint! // 32
    @IBOutlet weak var padMargin9: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin10: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin11: NSLayoutConstraint! // 26
    @IBOutlet weak var padMargin12: NSLayoutConstraint! // 26
    @IBOutlet weak var padMargin13: NSLayoutConstraint! // 28
    @IBOutlet weak var padMargin14: NSLayoutConstraint! // 28
    @IBOutlet weak var todayBadgeLabel: UILabel! // 16
    @IBOutlet weak var todayProblemLabel1: UILabel! // 18
    @IBOutlet weak var todayProblemLabel2: UILabel! // 30
    @IBOutlet weak var todayProblemLabel3: UILabel! // 18
    @IBOutlet weak var todayProblemLabel4: UILabel! // 16
    @IBOutlet weak var stampHeight: NSLayoutConstraint! // 89
    @IBOutlet weak var stampWidth1: NSLayoutConstraint! // 56
    @IBOutlet weak var stampWidth2: NSLayoutConstraint! // 56
    @IBOutlet weak var stampWidth3: NSLayoutConstraint! // 56
    @IBOutlet weak var stampWidth4: NSLayoutConstraint! // 56
    @IBOutlet weak var stampWidth5: NSLayoutConstraint! // 56
    @IBOutlet weak var stampWidth6: NSLayoutConstraint! // 56
    @IBOutlet weak var stampWidth7: NSLayoutConstraint! // 56
    @IBOutlet weak var stampHeight1: NSLayoutConstraint! // 56
    @IBOutlet weak var stampHeight2: NSLayoutConstraint! // 56
    @IBOutlet weak var stampHeight3: NSLayoutConstraint! // 56
    @IBOutlet weak var stampHeight4: NSLayoutConstraint! // 56
    @IBOutlet weak var stampHeight5: NSLayoutConstraint! // 56
    @IBOutlet weak var stampHeight6: NSLayoutConstraint! // 56
    @IBOutlet weak var stampHeight7: NSLayoutConstraint! // 56
    private func setPadConstants() {
        todayCardTop.constant = 32
        cardMargin1.constant = 20
        cardMargin2.constant = 20
        cardMargin3.constant = 20
        cardMargin4.constant = 20
        cardMargin5.constant = 20
        cardMargin6.constant = 28
        
        padMargin1.constant = 28
        padMargin2.constant = 28
        padMargin3.constant = 28
        padMargin4.constant = 28
        padMargin5.constant = 28
        padMargin6.constant = 28
        padMargin7.constant = 28
        padMargin8.constant = 32
        padMargin9.constant = 28
        padMargin10.constant = 28
        padMargin11.constant = 26
        padMargin12.constant = 26
        padMargin13.constant = 28
        padMargin14.constant = 28
        
        todayBadgeLabel.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        todayProblemLabel1.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        todayProblemLabel2.font = UIFont.systemFont(ofSize: 30, weight: .bold)
        todayProblemLabel3.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        todayProblemLabel4.font = UIFont.systemFont(ofSize: 16, weight: .regular)

        stampHeight.constant = 89
        stampWidth1.constant = 56
        stampWidth2.constant = 56
        stampWidth3.constant = 56
        stampWidth4.constant = 56
        stampWidth5.constant = 56
        stampWidth6.constant = 56
        stampWidth7.constant = 56
        stampHeight1.constant = 56
        stampHeight2.constant = 56
        stampHeight3.constant = 56
        stampHeight4.constant = 56
        stampHeight5.constant = 56
        stampHeight6.constant = 56
        stampHeight7.constant = 56
    }
    
    // MARK: - Override
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //ab180 이벤트 보내기
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: "pulja2.0_record_nosolve_enter",
                    customs: ["userseq": userSeq, "user_id": userId]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "category: pulja2.0_record_nosolve_enter", separator: " ")
                print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                print("[ABLog]", "user_id: \(userId)", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        CommonUtil.tabbarController?.tabBar.isHidden = false
        CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        
        // iPad
        if (UIDevice.current.userInterfaceIdiom == .pad) {
            setPadConstants()
        }
        
        // 문제풀이 기록이 있는경우
        if UserDefaults.standard.bool(forKey: "hasSolve") {
            let vc = R.storyboard.record.recordViewController()!
            CommonUtil.tabbarController?.viewControllers?[2] = vc
        }
    }
    
    // MARK: -  Action
    @IBAction func pressedStudyStart(_ sender: UIButton) {
        showBottomSheet()
    }
    
    // MARK: -  Function
    private func showBottomSheet() {
        if let tabBar = CommonUtil.tabbarController {
            let vc = R.storyboard.preparation.changeEmptyUnitViewController()!
            vc.selectedUnit = tabBar.selectedUnit
            vc.selectedKeyword = tabBar.selectedKeywordName
            vc.selectedKeywordURL = tabBar.selectedKeywordURL
            
            vc.callback = { (type) in
                if type == "change" {
                    let vc = R.storyboard.preparation.keywordViewController()!
                    vc.cameFrom = "pulja2.0_record_nosolve_next"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else if type == "start" {
                    
                    var selectKeywordSeqs:[Int] = []
                    
                    if tabBar.selectedKeywordList?.count ?? 0 > 0 {
                        for keyword in tabBar.selectedKeywordList! {
                            selectKeywordSeqs.append(keyword.keywordSeq!)
                        }
                    }
                    
                    var unit2Seq:[Int] = []
                    if tabBar.unit2SeqList?.count ?? 0 > 0 {
                        for seq in tabBar.unit2SeqList! {
                            unit2Seq.append(seq)
                        }
                    }
                    
                    let keywordName = tabBar.selectedKeywordName ?? ""
                    
                    let vc = R.storyboard.preparation.preparationMultipleChoiceViewController()!
                    vc.cameFromOnboard = false
                    vc.cameFrom = "pulja2.0_record_nosolve_next"
                    vc.unit2Seqs = unit2Seq
                    vc.keywordSeqs = selectKeywordSeqs
                    vc.keywordName = keywordName

                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
            // MDC 바텀 시트로 설정
            let bottomSheet = MDCBottomSheetController(contentViewController: vc)
            bottomSheet.mdc_bottomSheetPresentationController?.preferredSheetHeight = 340
            bottomSheet.delegate = self
            
            // 보여주기
            self.present(bottomSheet, animated: true, completion: nil)
        }
    }
}
