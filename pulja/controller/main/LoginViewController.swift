//
//  LoginViewController.swift
//  pulja
//
//  Created by 김병헌 on 2021/11/19.
//

import UIKit
import CoreData
import PromiseKit
import MaterialComponents.MaterialBottomSheet
import FlareLane
import AirBridge

class LoginViewController: PuljaBaseViewController, MDCBottomSheetControllerDelegate {

    @IBOutlet weak var bottomView: UIView!
    
    
    @IBOutlet weak var tfUserId: UITextField!
    
    @IBOutlet weak var btIdClear: UIButton!
    
    
    @IBOutlet weak var tfPassword: UITextField!
    var iconClick = true
    
    @IBOutlet weak var autoSuggestionView: SwiftAutoSuggestionView!
    
    
    @IBOutlet weak var autoSuggestionTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var autoSuggestionBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var autoSuggestionRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var autoSuggestionLeftConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var autoSuggestionHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var btBack: UIButton!
    
    let emailAutoComplete = ["@gmail.com","@naver.com"]
    
    
    var activeTextField : UITextField? = nil
    
    var user:User?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.bottomView.roundedTop(10.0)
            
           

        }
        
        self.tfUserId.layer.cornerRadius = 10.0
        self.tfUserId.layer.borderWidth = 0.5
        self.tfUserId.layer.backgroundColor = UIColor.white.cgColor
        self.tfUserId.layer.borderColor = UIColor.lightBlueGrey.cgColor
        self.tfUserId.setPadding(left: 12, right: 12)
        
        
        self.tfPassword.layer.cornerRadius = 10.0
        self.tfPassword.layer.borderWidth = 0.5
        self.tfPassword.layer.backgroundColor = UIColor.white.cgColor
        self.tfPassword.layer.borderColor = UIColor.lightBlueGrey.cgColor
        self.tfPassword.setPadding(left: 12, right: 12)
        
        
        self.tfUserId.addDoneButtonOnKeyboard()
        self.tfPassword.addDoneButtonOnKeyboard()
               
               
//        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.backgroundTap))
//               self.view.addGestureRecognizer(tapGestureRecognizer)
               // Do any additional setup after loading the view.
               NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
               NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
               
        self.tfUserId.delegate = self
        self.tfPassword.delegate = self
        
        setupSuggestionView()
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
        
    }
    
    deinit {
       NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: nil)
    }
        
    @objc func rotated() {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            
        } else {
            print("Portrait")
        }
        
        // Do any additional setup after loading the view.
              DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                  self.bottomView.roundedTop(10.0)
                  
                 

              }
    }
   
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        navigationController?.setNavigationBarHidden(true, animated: animated)
        
    }
    
    

    private func setupSuggestionView() {
        autoSuggestionView.delegate = self
        autoSuggestionView.suggestionTopConstraint = autoSuggestionTopConstraint
        autoSuggestionView.suggestionBottomConstraint = autoSuggestionBottomConstraint
        autoSuggestionView.suggestionRightConstraint = autoSuggestionRightConstraint
        autoSuggestionView.suggestionLeftConstraint = autoSuggestionLeftConstraint
        autoSuggestionView.suggestionHeightConstraint = autoSuggestionHeightConstraint
        autoSuggestionView.suggestionDirection = .bottom
        
        
        autoSuggestionView.borderColor = UIColor.lightBlueGrey
        autoSuggestionView.borderWidth = 0.2
        autoSuggestionView.cornerRadius = 10.0
        autoSuggestionView.suggestionTableViewBackgroundColor = UIColor.paleGrey
        autoSuggestionView.backgroundColor = UIColor.paleGrey
        autoSuggestionView.suggestionTableView.separatorStyle = .none
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func ibBack(_ sender: Any) {
//        dismiss(animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            self.view.endEditing(true)

        self.tfUserId.layer.borderColor = UIColor.lightBlueGrey.cgColor

        self.tfPassword.layer.borderColor = UIColor.lightBlueGrey.cgColor

    }
    
    
    // MARK: - Input
    @objc func keyboardWillShow(notification: NSNotification) {
            
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            
            // if keyboard size is not available for some reason, dont do anything
           return
        }
        
        var shouldMoveViewUp = false
        
        // if active text field is not nil
        if let activeTextField = activeTextField {
            
            let bottomOfTextField = activeTextField.convert(activeTextField.bounds, to: self.view).maxY;
            let topOfKeyboard = self.view.frame.height - keyboardSize.height
            
            if bottomOfTextField > topOfKeyboard {
                shouldMoveViewUp = true
            }
        }
        
        if(shouldMoveViewUp) {
            self.view.frame.origin.y = 0 - keyboardSize.height
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    @objc func backgroundTap(_ sender: UITapGestureRecognizer) {
        // go through all of the textfield inside the view, and end editing thus resigning first responder
        // ie. it will trigger a keyboardWillHide notification
        self.view.endEditing(true)
        
        self.tfUserId.layer.borderColor = UIColor.lightBlueGrey.cgColor
               
               self.tfPassword.layer.borderColor = UIColor.lightBlueGrey.cgColor
    }
    
    
    @IBAction func userIdEditingChange(_ sender: UITextField) {
        if sender == tfUserId, let textCount = tfUserId.text?.count {
            
             if textCount == 0 {
                 
                 self.btIdClear.isHidden = true
            
//                 self.tfUserId.layer.borderWidth = 0.5
//                 self.tfUserId.layer.borderColor = UIColor.lightBlueGrey.cgColor

                 autoSuggestionView.hideSuggestion()
                 return
             } else {
                 
                 self.btIdClear.isHidden = false
                 
                 if (tfUserId.text!.contains("@")) {
                     
                     autoSuggestionView.hideSuggestion()
                     return
                 }
                 

                 let ac = ["\(tfUserId.text!)@naver.com",
                           "\(tfUserId.text!)@gmail.com",
                           "\(tfUserId.text!)@hanmail.net"
                 ]
                 autoSuggestionView.showSuggestion(with: ac as NSArray)
             }
//            self.tfUserId.layer.borderWidth = 1.0
//            self.tfUserId.layer.borderColor = UIColor.Purple.cgColor
             
         }
        
        
    }
    
    
    
    @IBAction func idClear(_ sender: Any) {
        
        self.tfUserId.text = ""
        self.btIdClear.isHidden = true
//        self.tfUserId.layer.borderWidth = 0.5
//         self.tfUserId.layer.borderColor = UIColor.lightBlueGrey.cgColor

         autoSuggestionView.hideSuggestion()
    }
    
    
    
    @IBAction func passwordEditingChange(_ sender: UITextField) {
        
        if sender == tfPassword, let textCount = tfPassword.text?.count {
                    
             if textCount == 0 {
//                 self.tfPassword.layer.borderWidth = 0.5
//                 self.tfPassword.layer.borderColor = UIColor.lightBlueGrey.cgColor
                 return
             }
            
//            self.tfPassword.layer.borderColor = UIColor.Purple.cgColor
//            self.tfPassword.layer.borderWidth = 1.0
         }
        
    }
    
    
    @IBAction func btSecure(_ sender: Any) {
        
        tfPassword.isSecureTextEntry.toggle()
        
        if tfPassword.isSecureTextEntry {
            if let image = UIImage(named: "iconSolidEye") {
                (sender as AnyObject).setImage(image, for: .normal)
            }
        } else {
            if let image = UIImage(named: "iconSolidEyeOff") {
                (sender as AnyObject).setImage(image, for: .normal)
            }
        }

        
    }
    

    @IBAction func findIdPwd(_ sender: Any) {
        
        // 바텀 시트로 쓰일 뷰컨트롤러 생성
//        let vc = UIStoryboard.init(name: R.storyboard.main.name, bundle: Bundle(for: LoginBottomViewController.self)).instantiateViewController(withIdentifier: "LoginBottomViewController") as! LoginBottomViewController
        
        let vc = R.storyboard.main.loginBottomViewController()!

        vc.callback = { res in
            print("callback \(res)")
//            self.memoBottomSheet(idx)
            
            if res == "id"{
                let vc = R.storyboard.main.idpwdViewController()!
               vc.modalTransitionStyle = .flipHorizontal
               vc.viewTitle = "아이디 찾기"
               vc.url = "\(Const.DOMAIN)/login_find_id"

               self.navigationController?.pushViewController(vc, animated: true)
                
            } else if res == "pwd" {
                
                let vc = R.storyboard.main.idpwdViewController()!
                vc.modalTransitionStyle = .flipHorizontal
                vc.viewTitle = "비밀번호 찾기"
                vc.url = "\(Const.DOMAIN)/login_find_password_id"

                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        }
                
                // MDC 바텀 시트로 설정
        let bottomSheet = MDCBottomSheetController(contentViewController: vc)
        bottomSheet.mdc_bottomSheetPresentationController?.preferredSheetHeight = 218
        bottomSheet.delegate = self
                
                // 보여주기
        present(bottomSheet, animated: true, completion: nil)
        
    }
    
    @IBAction func login(_ sender: Any) {
        
        LoadingView.show()
        
        
        guard let userId = tfUserId.text else { return }
        
        guard let password = tfPassword.text else { return }
        
        var uuid = ""
        var fcmToken = ""
        
        if  let token = UserDefaults.standard.string(forKey: Const.UD_DIVICE_TOKEN) {
            fcmToken = token
            uuid = UIDevice.current.identifierForVendor!.uuidString
            
            if let deviceToken = CommonUtil.shared.getDeviceToken()
            {
//                SBDMain.registerDevicePushToken(deviceToken, unique: false) { status, error in
//
//                    switch status {
//                    case .success:
//                        print("APNS Token is registered.")
//                    case .pending:
//                        print("Push registration is pending.")
//                    case .error:
//                        print("APNS registration failed with error: \(String(describing: error ?? nil))")
//
//                    @unknown default:
//                        print("Push registration: unknown default")
//                        assertionFailure()
//                    }
//                }
            }
            
            
        }
        
        firstly {
            SignAPI.shared.signin(id: userId, pw: password, fcmToken: fcmToken, uuid: uuid)
        }.then { res in
            when(fulfilled: self.setLoginUserInfo(signinRes: res), self.getHomeData())
        }.done { chatUserRes, homRes in
            
            if let cus = chatUserRes.data {
                var chat_ids:[String]? = []
                for chatUser in cus {
                    chat_ids?.append(chatUser.hashUserSeq!)
                }
                
                let jsonData = try? JSONEncoder().encode(cus)
                let jsonString = String.init(data: jsonData! , encoding: .utf8)

                
                let userDefault = UserDefaults.standard
                userDefault.setValue(chat_ids, forKey: Const.UD_CHAT_USER_IDS)
                userDefault.setValue(jsonString, forKey: Const.UD_CHAT_USER_JSON)

                userDefault.synchronize()
            } else {
                let userDefault = UserDefaults.standard
                userDefault.removeObject(forKey: Const.UD_CHAT_USER_IDS)
                userDefault.removeObject(forKey: Const.UD_CHAT_USER_JSON)
                userDefault.synchronize()
            }
            
            //PayType 추가.
            //p : paid user
            //f : free user
            //b : paid user before
            //c : coach
            if let userType = homRes.data?.appHomeView?.userType
            {
                let infos = try self.container.viewContext.fetch(MyInfo.fetchRequest())
                if infos.count > 0
                {
                    let myInfo = infos[0]
                    myInfo.setValue(userType, forKey: "payType")
                    try self.container.viewContext.save()
                }
            }
            
            
            if self.myInfo != nil, let id = self.myInfo?.hashUserSeq, let nick = self.myInfo?.username,  let userType = self.myInfo?.userType {
                LoadingView.hide()
                if let schoolNum = self.user?.schoolNum, schoolNum > 0 {
                    self.goMain(data:homRes.data)
                } else {
                    let vc = R.storyboard.main.snsJoinStep1ViewController()!
                    vc.isJoin = false
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            } else {
                LoadingView.hide()
            }
            
            
        }.catch { error in
            LoadingView.hide()
            
            let errtxt = error.localizedDescription
            
            var title:String? = nil
            var message:String? = errtxt
            var buttonText = "확인"
            
            var isDormant = false
            var userName:String? = nil
            
            if errtxt.contains("비밀번호")  {
                title = "다시 한번 확인해주세요."
                message = "해당하는 계정 정보가 없거나 비밀번호가 틀렸어요."
                buttonText = "다시 시도하기"
            } else if errtxt.contains("이메일") {
                title = "다시 한번 확인해주세요."
                message = "해당하는 계정 정보가 없거나 비밀번호가 틀렸어요."
                buttonText = "다시 시도하기"
            } else if errtxt.contains("휴면") {
                isDormant = true
                var startIndex = errtxt.startIndex
                var endIndex = errtxt.endIndex
                if let index = errtxt.index(of: "회원명:") {
                    startIndex = errtxt.index(index, offsetBy: 4)
                }
                let range = startIndex..<endIndex
                userName = errtxt.substring(with: range)
                print("회원명:\(userName)")
            } else if errtxt.contains("탈퇴") {
                title = "다시 한번 확인해주세요."
                message = "해당하는 계정 정보가 없거나 비밀번호가 틀렸어요."
                buttonText = "다시 시도하기"
            } else {
                message = "알 수 없는 오류가 발생했습니다."
            }
            
            if(isDormant){
                
                let vc = R.storyboard.main.sleepIDViewController()!
                vc.userId = userId
                vc.password = password
                vc.userName = userName
                vc.restoreCallback = { (b) in
                    if b {
                        self.login(self)
                    }
                }
                
                self.present(vc, animated: true, completion: nil)
                
                
            } else {
                
                self.alert(title: title, message: message, button: buttonText).done { b in
                    self.tfUserId.text = ""
                    self.tfPassword.text = ""
                }
                
            }
        }
    }
    
    func getHomeData() -> Promise<HomeRes> {
           return  HomeAPI.shared.home()
    }
    
    func getDiagnosisInfo(signinRes: SigninRes) -> Promise<DiagnosisHistoryListRes>{
        
        if let data = signinRes.data , let userSeq = data.user?.userSeq {
         
            return StudyAPI.shared.diagnosis_test_info(userSeq: userSeq)
        }
        return StudyAPI.shared.diagnosis_test_info(userSeq: 0)
        
    }
    
    func setLoginUserInfo(signinRes: SigninRes) -> Promise<ChatUserRes> {
        
        if let data = signinRes.data {
            
            do {
                self.user = data.user
                
                let infos = try self.container.viewContext.fetch(MyInfo.fetchRequest()) as! [MyInfo]
                for myinfo in infos {
                    self.container.viewContext.delete(myinfo)
                }
                
                let userEntity = NSEntityDescription.entity(forEntityName: "MyInfo", in: self.container.viewContext)
                let user = NSManagedObject(entity: userEntity!, insertInto: self.container.viewContext)
                user.setValue(data.user?.userSeq, forKey: "userSeq")
                user.setValue(data.user?.userId, forKey: "userId")
                user.setValue(data.user?.userType, forKey: "userType")
                user.setValue(data.user?.username, forKey: "username")
                user.setValue(data.user?.hashUserSeq, forKey: "hashUserSeq")
                user.setValue(data.user?.snsId, forKey: "snsId")
                user.setValue(data.user?.snsType, forKey: "snsType")
                user.setValue(data.user?.snsEmail, forKey: "snsEmail")
                user.setValue(data.user?.snsNick, forKey: "snsNick")
                
                try self.container.viewContext.save()
                
                let userSeq = data.user?.userSeq ?? 0
                FlareLane.setUserId(userId: "\(userSeq)")
                
                
                let newMyInfos = try self.container.viewContext.fetch(MyInfo.fetchRequest()) as! [MyInfo]
                self.myInfo = newMyInfos[0]
                
                if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
                    
                    let user = ABUser()
                    user.id = userId
                    user.email = userId
                    user.attributes = [
                        "userseq": userSeq as NSObject,
                    ]
                    
                    AirBridge.state().setUser(user)
                    
                    let event = ABInAppEvent()
                    event?.setCategory(ABCategory.signIn)
                    event?.send()
                }
            } catch {
                print(error.localizedDescription)
            }
            
            CommonUtil.shared.setAccessToken(token: data.accessToken!)
            CommonUtil.shared.setRefreshToken(token: data.refreshToken!)
            CommonUtil.shared.setHeader()
        }
        
        return ChatAPI.shared.chatUsers()
    }
    
    
    func goMain(data:Home?){
        // 문제풀이 userdefaults 초기화
        let userDefaultKey1 = "hasSolve"
        let userDefaultKey2 = "hasProblemSeq"
        
        UserDefaults.standard.removeObject(forKey: userDefaultKey1)
        UserDefaults.standard.removeObject(forKey: userDefaultKey2)
        
        if self.myInfo != nil, let userType = self.myInfo?.userType, let userSeq = self.myInfo?.userSeq {
            CurationAPI.shared.studyInfo(userSeq: Int(userSeq)).done { studyInfoRes in
                if let hasSolve = studyInfoRes.data?.hasSolve {
                    UserDefaults.standard.set(hasSolve, forKey: userDefaultKey1)
                    UserDefaults.standard.synchronize()
                }
                if let hasProblemSeq = studyInfoRes.data?.hasProblemSeq {
                    UserDefaults.standard.set(hasProblemSeq, forKey: userDefaultKey2)
                    UserDefaults.standard.synchronize()
                }
            }.catch { err in
                print(err)
            }.finally {
                if UserDefaults.standard.bool(forKey: userDefaultKey2) {
                    if userType == "C" {
                        self.goCoach(data: data)
                    } else {
                        self.goStudunt(data:data)
                    }
                } else {
                    let vc = R.storyboard.preparation.keywordViewController()!
                    vc.cameFromOnboard = true
                    vc.isModal = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    func goStudunt(data:Home?){
        let vcDetail = R.storyboard.main.tabbarController()
                   
        if let vc = vcDetail?.viewControllers![0] as? HomeViewController {
           vc.homeInfo = data
        }

        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate

        appDel.window?.rootViewController = vcDetail

        // A mask of options indicating how you want to perform the animations.
        let options: UIView.AnimationOptions = .transitionCrossDissolve

        // The duration of the transition animation, measured in seconds.
        let duration: TimeInterval = 0.3

        // Creates a transition animation.
        // Though `animations` is optional, the documentation tells us that it must not be nil. ¯\_(ツ)_/¯
        UIView.transition(with: appDel.window!, duration: duration, options: options, animations: {}, completion:
        { completed in
           // maybe do something on completion here
        })
    }
    
    
    func goCoach(data:Home?) {
           let vcDetail = R.storyboard.main.coachTabbarController()
                          
              if let vc = vcDetail?.viewControllers![0] as? HomeViewController {
                  vc.homeInfo = data
              }
             
              let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
              
              appDel.window?.rootViewController = vcDetail
              
              // A mask of options indicating how you want to perform the animations.
              let options: UIView.AnimationOptions = .transitionCrossDissolve

              // The duration of the transition animation, measured in seconds.
              let duration: TimeInterval = 0.3

              // Creates a transition animation.
              // Though `animations` is optional, the documentation tells us that it must not be nil. ¯\_(ツ)_/¯
              UIView.transition(with: appDel.window!, duration: duration, options: options, animations: {}, completion:
              { completed in
                  // maybe do something on completion here
              })
       }
    
}

// MARK: - extension
extension LoginViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
        
        if textField == self.tfUserId {
            self.tfUserId.layer.borderWidth = 1.0
            self.tfUserId.layer.borderColor = UIColor.Purple.cgColor
            self.tfPassword.layer.borderWidth = 0.5
            self.tfPassword.layer.borderColor = UIColor.lightBlueGrey.cgColor
        } else if textField == self.tfPassword {
            self.tfPassword.layer.borderWidth = 1.0
            self.tfPassword.layer.borderColor = UIColor.Purple.cgColor
            self.tfUserId.layer.borderWidth = 0.5
            self.tfUserId.layer.borderColor = UIColor.lightBlueGrey.cgColor
        } else {
            self.tfUserId.layer.borderWidth = 0.5
            self.tfUserId.layer.borderColor = UIColor.lightBlueGrey.cgColor
            self.tfPassword.layer.borderWidth = 0.5
            self.tfPassword.layer.borderColor = UIColor.lightBlueGrey.cgColor
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeTextField = nil
    }
}


extension LoginViewController: SwiftAutoSuggestionDelegate {
    func swiftAutosuggestionDidSelectCell(with data: Any) {
        //Use the data
        tfUserId.text = data as? String
        //Hide the suggestion
        autoSuggestionView.hideSuggestion()
    }
}
extension LoginViewController {
    static func initiate() -> LoginViewController {
        let vc = LoginViewController.withStoryboard(storyboard: .main)
        return vc
    }
}
