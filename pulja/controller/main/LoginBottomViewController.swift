//
//  LoginBottomViewController.swift
//  pulja
//
//  Created by 김병헌 on 2021/12/10.
//

import UIKit

class LoginBottomViewController: UIViewController {
    
    
    @IBOutlet weak var findId: UIView!
    
    @IBOutlet weak var findPwd: UIView!
    
    var callback : (_ findType :String) -> Void = { _ in}

    
    override func viewDidLoad() {
        super.viewDidLoad()

        let gesture = UITapGestureRecognizer(target: self, action: #selector(goFindId(_:)))

        self.findId.addGestureRecognizer(gesture)

        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(goFindPwd(_:)))

        self.findPwd.addGestureRecognizer(gesture2)
        
    }
    

    @IBAction func close(_ sender: Any) {
        
        dismiss(animated: true) {
//            self.callback(false)
        }
    }
    
    
    @objc func goFindId(_ sender:UIGestureRecognizer){
//        let sb = UIStoryboard(name: "Main", bundle: nil)
//        let vc = sb.instantiateViewController(withIdentifier: "IDPWDViewController") as! IDPWDViewController
        
//        let vc = R.storyboard.main.idpwdViewController()!
//        vc.viewTitle = "아이디 찾기"
//        vc.url = "\(Const.DOMAIN)/login_find_id"
//
//        vc.modalPresentationStyle = .overFullScreen
//
//        self.present(vc, animated: true) {
//
//        }
        
        
        
        
        dismiss(animated: true) {
            self.callback("id")
        }
        
    }
    
    @objc func goFindPwd(_ sender:UIGestureRecognizer){
//            let sb = UIStoryboard(name: "Main", bundle: nil)
//            let vc = sb.instantiateViewController(withIdentifier: "IDPWDViewController") as! IDPWDViewController
        
//            let vc = R.storyboard.main.idpwdViewController()!
//            vc.viewTitle = "비밀번호 찾기"
//            vc.url = "\(Const.DOMAIN)/login_find_password_id"
//            vc.modalPresentationStyle = .overFullScreen
//            self.present(vc, animated: true) {
//
//            }
        
        dismiss(animated: true) {
            self.callback("pwd")
        }

    }
    
}
