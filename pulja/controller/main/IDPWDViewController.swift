//
//  IDPWDViewController.swift
//  pulja
//
//  Created by 김병헌 on 2021/12/13.
//

import UIKit
import WebKit
import PromiseKit
import AirBridge

class IDPWDViewController: UIViewController, WKUIDelegate, WKNavigationDelegate  {

    
    @IBOutlet weak var lbTitle: UILabel!
    
    @IBOutlet weak var webView: WKWebView!
    
    
    @IBOutlet weak var webFrameView: UIView!
    var viewTitle:String?  = nil
    
    var url:String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let contentController = WKUserContentController()
        AirBridge.webInterface()?.inject(to: contentController, withWebToken: Const.AB_WEB_TOKEN)
              let script =
                  "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport');" +
                      "meta.setAttribute('content', 'width=device-width, initial-scale = 1.0, maximum-scale=5.0, user-scalable = yes');" +
              "document.getElementsByTagName('head')[0].appendChild(meta);"
              let userScript = WKUserScript(source: script,
                                            injectionTime: WKUserScriptInjectionTime.atDocumentEnd,
                                            forMainFrameOnly: true)
              contentController.addUserScript(userScript)
        
        
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        
        let config = WKWebViewConfiguration()
        config.preferences = preferences
        config.userContentController = contentController
        
        self.webView = WKWebView(frame: .zero, configuration: config)
        self.webView?.frame = self.webFrameView.bounds
        self.webView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.webFrameView.addSubview(self.webView!)

        

        lbTitle.text = viewTitle
        
        loadWebPage(url ?? "")
        
        webView?.navigationDelegate = self
        webView.uiDelegate = self
    }
    
    private var authCookie: HTTPCookie? {
              let cookie = HTTPCookie(properties: [
               .domain: Const.COOKIE_DOMAIN,
                  .path: "/",
                  .name: "JWT-TOKEN",
                  .value: CommonUtil.shared.getAccessToken(),
              ])
              return cookie
          }
              
      private var refreshCookie: HTTPCookie? {
          let cookie = HTTPCookie(properties: [
              .domain: Const.COOKIE_DOMAIN,
              .path: "/",
              .name: "REFRESH-TOKEN",
              .value: CommonUtil.shared.getRefreshToken(),
          ])
          return cookie
      }
    

    @IBAction func close(_ sender: Any) {
        dismiss(animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    private func loadWebPage(_ url: String) {
        guard let myUrl = URL(string: url) else {
            return
        }
        
        
        let urlRequest = URLRequest(url: myUrl)

        if let authCookie = authCookie, let refreshCookie = refreshCookie {
            self.webView?.configuration.websiteDataStore.httpCookieStore.setCookie(authCookie, completionHandler: {
                self.webView?.configuration.websiteDataStore.httpCookieStore.setCookie(refreshCookie, completionHandler: {
                    self.webView?.load(urlRequest)
                })
            })
        } else {
            self.webView?.load(urlRequest)
        }
                
        
//        let request = URLRequest(url: myUrl)
//        webView.load(request)
    }
    
    
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        let ac = UIAlertController(title: "알림", message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(ac, animated: true)
        completionHandler()
    }
    
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        guard let urlString = webView.url?.absoluteString else {
            return
        }
        debugPrint("redirect url: \(urlString)")
        if urlString == "http://www.pulja.co.kr/" {
            self.alert(title: "", message: "비밀번호가 변경되었습니다.", button: "확인")
        } else if urlString.contains("login_find_password_id") {
            self.webView?.reload()
        } else if urlString.contains("login_social_links") {
            
            if let snsType = getQueryStringParameter(url: urlString, param: "snsType"), let email = getQueryStringParameter(url: urlString, param: "snsEmail") {
                let vc = R.storyboard.main.snsLoginViewController()!
                  vc.modalTransitionStyle = .flipHorizontal
                  vc.snsEmail = email ?? ""
                  vc.snsType = snsType ?? ""
                  
                 self.navigationController?.pushViewController(vc, animated: true)
            }
            
            return
            
//        } else if urlString == "http://www.pulja.co.kr/login_social_links" {
//            self.loadWebPage("https://www.pulja.co.kr/login_social_links")
        } else {
            self.loadWebPage(urlString)
//            let urlRequest = URLRequest(url: urlString)
//            self.webView?.load(urlRequest)
        }
    }
    
    
    func alert(title:String?, message:String?, button:String?) -> Promise<Bool> {
           return Promise { seal in
               let btText:String = button ?? "확인"
               let titleText:String = title ?? "알림"
               let messageText:String = message ?? "오류가 발생하였습니다."
               
               
               let alert = R.storyboard.main.alertViewController()!
               
               alert.callback = { check in
                   seal.resolve(.fulfilled(check))
               }
               
               alert.titleText = titleText
               alert.messageText = messageText
               alert.buttonText = btText
               alert.isTwoButton = false
               alert.modalPresentationStyle = .overCurrentContext
               present(alert, animated: false, completion: nil)

           }
       }
    
    func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else {
            return nil
        }
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
