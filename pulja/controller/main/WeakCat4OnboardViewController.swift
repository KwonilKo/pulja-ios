//
//  WeakCat4OnboardViewController.swift
//  pulja
//
//  Created by kwonilko on 2022/08/03.
//

import UIKit
import SwiftyOnboard
import KakaoSDKCommon
import KakaoSDKAuth
import KakaoSDKUser
import NaverThirdPartyLogin
import Alamofire
import AuthenticationServices
import PromiseKit
import AirBridge
import FlareLane
import ChannelIOFront
import Lottie
import CoreData

class WeakCat4OnboardViewController: PuljaBaseViewController , NaverThirdPartyLoginConnectionDelegate {
    
    @IBOutlet weak var onboardView: UIView!
    var loginType : String = ""
    
    var swiftyOnboard :SwiftyOnboard? = nil
    let loginInstance = NaverThirdPartyLoginConnection.getSharedInstance()
    
    var user : User?
    
    
    @IBOutlet weak var animationView: LottieAnimationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        animationView.animationSpeed = 0.8
        animationView.play()
        
        loginInstance?.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        animationView.play()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    @objc func didBecomeActive(){
        LoadingView.hide()
        animationView.play()
    }
    
    @objc func rotated() {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            
        } else {
            print("Portrait")
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.swiftyOnboard?.removeFromSuperview()
            self.swiftyOnboard = SwiftyOnboard(frame: self.view.frame)
            self.onboardView.addSubview(self.swiftyOnboard!)
            self.swiftyOnboard!.style = .light
            self.swiftyOnboard!.delegate = self
            self.swiftyOnboard!.dataSource = self
        }
        
    }
    
    func showSnsLoginFailedAlert(errorMessage: String, snsType: String) {
        
        if Const.isAirBridge {
            CommonUtil.shared.ABEvent(
                category: "pulja2.0_snslogin_error",
                customs:["errorMessage" : errorMessage, "snsType": snsType]
            )
        } else {
            print("[ABLog]", "-----------------------------------------", separator: " ")
            print("[ABLog]", "category: pulja2.0_snslogin_error", separator: " ")
            print("[ABLog]", "errorMessage: \(errorMessage)", separator: " ")
            print("[ABLog]", "snsType: \(snsType)", separator: " ")
            print("[ABLog]", "-----------------------------------------", separator: " ")
        }
        
        CommonUtil().showHAlert(title: "로그인에 실패했어요", message: "다시 한번 시도 후에도 오류가\n지속될 경우 고객센터로 문의해주세요.", button: "확인", subButton: "문의하기", viewController: self) { isSend in
            if !isSend {
                self.initChennelTalk()
            }
        }
    }
    
    func initChennelTalk(){
        let profile = Profile()
            .set(name: "guest")
        
        let buttonOption = ChannelButtonOption.init(
            position: .right,
            xMargin: 16,
            yMargin: 23
        )
        
        let bootConfig = BootConfig.init(
            pluginKey: Const.CHANNELTALK_PLUGIN_KEY,
            memberId: "guest",
            //          memberHash: "",
            profile: profile,
            channelButtonOption: buttonOption,
            hidePopup: false,
            trackDefaultEvent: true,
            language: .korean
        )
        
        ChannelIO.boot(with: bootConfig) { (bootStatus, user) in
            if bootStatus == .success {
                // success
                ChannelIO.showMessenger()
            } else {
                // show failed reason from bootStatus
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func btnLoginPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "login", sender: nil)
    }
    
    @IBAction func btnKakaoPressed(_ sender: Any) {
        if (UserApi.isKakaoTalkLoginAvailable()) {
            LoadingView.show()
            UserApi.shared.loginWithKakaoTalk {(oauthToken, error) in
                if let error = error {
                    print("kakaotalk login() failed -> \(error)")
                    // 알수없는오류
                    if (error.localizedDescription.contains("Unknown")) {
                        self.showSnsLoginFailedAlert(
                            errorMessage: error.localizedDescription,
                            snsType: "k"
                        )
                    }
                    LoadingView.hide()
                } else {
                    print("loginWithKakaoTalk() success.")
                    
                    //do something
                    _ = oauthToken
                    
                    UserApi.shared.me() { (user, error) in
                        if let error = error {
                            print("kakaotalk me() failed -> \(error)")
                            LoadingView.hide()
                            // 알수없는오류
                            if (error.localizedDescription.contains("Unknown")) {
                                self.showSnsLoginFailedAlert(
                                    errorMessage: error.localizedDescription,
                                    snsType: "k"
                                )
                            }
                        } else {
                            LoadingView.hide()
                            print("kakaotalk me() success.")
                            let name = user?.kakaoAccount?.profile?.nickname ?? ""
                            let email = user?.kakaoAccount?.email ?? ""
                            let kakaoId = user?.id ?? 0
                            print(name)
                            print(email)
                            //do something
                            _ = user
                            
                            let snsUserReq = SnsUserReq(fcmToken: nil, marketAccept: nil, osType: "i", snsAppleCode: nil, snsEmail: email, snsId: String(kakaoId), snsNick: name, snsToken: oauthToken?.accessToken, snsType: "k", uuid: nil)
                            
                            self.snsEmailCheck(snsUserReq: snsUserReq)
                        }
                    }
                }
            }
        } else {
            LoadingView.show()
            UserApi.shared.loginWithKakaoAccount {(oauthToken, error) in
                if let error = error {
                    print("kakao login() failed -> \(error)")
                    // 알수없는오류
                    if (error.localizedDescription.contains("Unknown")) {
                        self.showSnsLoginFailedAlert(
                            errorMessage: error.localizedDescription,
                            snsType: "k"
                        )
                    }
                    LoadingView.hide()
                }
                else {
                    print("loginWithKakao() success.")
                    
                    //do something
                    _ = oauthToken
                    
                    UserApi.shared.me() { (user, error) in
                        if let error = error {
                            print("kakao me() failed -> \(error)")
                            // 알수없는오류
                            if (error.localizedDescription.contains("Unknown")) {
                                self.showSnsLoginFailedAlert(
                                    errorMessage: error.localizedDescription,
                                    snsType: "k"
                                )
                            }
                            LoadingView.hide()
                        } else {
                            LoadingView.hide()
                            print("kakao me() success.")
                            let name = user?.kakaoAccount?.profile?.nickname ?? ""
                            let email = user?.kakaoAccount?.email ?? ""
                            let kakaoId = user?.id ?? 0
                            print(name)
                            print(email)
                            //do something
                            _ = user
                            
                            let snsUserReq = SnsUserReq(fcmToken: nil, marketAccept: nil, osType: "i", snsAppleCode: nil, snsEmail: email, snsId: String(kakaoId), snsNick: name, snsToken: oauthToken?.accessToken, snsType: "k", uuid: nil)
                            
                            self.snsEmailCheck(snsUserReq: snsUserReq)
                        }
                    }
                }
            }
        }
    }
    
    func snsEmailCheck(snsUserReq:SnsUserReq){
        LoadingView.show()
        SnsLoginAPI.shared.snsEmailCheck(snsUserReq: snsUserReq).done { chkRes in
            LoadingView.hide()
            if let checkEmailData = chkRes.data {
                
                let checkEmail = checkEmailData.snsEmail
                let checkServiceType = checkEmailData.snsType
                let checkSnsId = checkEmailData.snsId
                
                if checkEmail == nil { //없으면 가입.
                    
                    self.puljaJoinPolicy(snsUserReq: snsUserReq)
                    
                } else if snsUserReq.snsId == checkSnsId && snsUserReq.snsType == checkServiceType { //같으면 로그인
                    
                    self.puljaLogin(snsUserReq: snsUserReq)
                    
                } else if snsUserReq.snsEmail == checkEmail && snsUserReq.snsType != checkServiceType  { //다르면 다른 소셜 서비스 로그인 안내 패이지.
                    
                    print("다른 소셜 로그인 서비스 정보.")
                    
                    let vc = R.storyboard.main.snsLoginViewController()!
                    vc.modalTransitionStyle = .flipHorizontal
                    vc.snsEmail = checkEmail ?? ""
                    vc.snsType = checkServiceType ?? ""
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    self.alert(title: nil, message: "가입 할 수 없는 계정입니다.", button: "확인")
                }
            }else {
                self.puljaJoinPolicy(snsUserReq: snsUserReq)
            }
        }
    }
    
    @IBAction func btnNaverPressed(_ sender: Any) {
        LoadingView.show()
        loginInstance?.resetToken()
        if(loginInstance?.accessToken == nil) {
            loginInstance?.requestThirdPartyLogin()
        } else {
            if let isValidAccessToken = loginInstance?.isValidAccessTokenExpireTimeNow() {
                if !isValidAccessToken {
                    loginInstance?.requestAccessTokenWithRefreshToken()
                } else {
                    getInfo()
                }
            } else {
                loginInstance?.requestThirdPartyLogin()
            }
        }
    }
    
    @IBAction func btnApplePressed(_ sender: Any) {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            if let userID = UserDefaults.standard.string(forKey: "currentUserIdentifier") {
                appleIDProvider.getCredentialState(forUserID: userID) { (credentialState, error) in
                    switch credentialState {
                    case .authorized:
                        // The Apple ID credential is valid.
                        // 유저를 확인하여 바로 로그인한다.
                        break
                    case .revoked:
                        // The Apple ID credential is revoked.
                        UserDefaults.standard.removeObject(forKey: "currentUserIdentifier")
                        self.startSignInWithAppleFlow()
                        break
                    case .notFound:
                        // No credential was found, so show the sign-in UI.
                        break
                    default:
                        break
                    }
                }
            } else {
                self.startSignInWithAppleFlow()
            }
        }
    }
    
    // 네이버 로그인에 성공한 경우 호출
    func oauth20ConnectionDidFinishRequestACTokenWithAuthCode() {
        LoadingView.hide()
        print("Success login")
        getInfo()
    }
    
    // 네이버 referesh token
    func oauth20ConnectionDidFinishRequestACTokenWithRefreshToken() {
        getInfo()
    }
    
    // 네이버 로그아웃
    func oauth20ConnectionDidFinishDeleteToken() {
        LoadingView.hide()
        print("log out")
    }
    
    // 네이버 모든 error
    func oauth20Connection(_ oauthConnection: NaverThirdPartyLoginConnection!, didFailWithError error: Error!) {
        LoadingView.hide()
        print("error = \(error.localizedDescription)")
        // 알수없는오류
        if (error.localizedDescription.contains("server_error") || error.localizedDescription.contains("Internal Server Error")) {
            self.showSnsLoginFailedAlert(
                errorMessage: error.localizedDescription,
                snsType: "n"
            )
        }
    }
    
    
    // 네이버 RESTful API, id가져오기
    func getInfo() {
        guard let isValidAccessToken = loginInstance?.isValidAccessTokenExpireTimeNow() else { return }
        
        if !isValidAccessToken {
            return
        }
        
        guard let tokenType = loginInstance?.tokenType else { return }
        guard let accessToken = loginInstance?.accessToken else { return }
        
        print("accessToken : \(accessToken)")
        
        
        
        LoadingView.show()
        SignAPI.shared.naverLogin(tokenType: tokenType, accessToken: accessToken).done { res in
            
            if let result = res.response
            {
                let naverUniqueId = result.id ?? ""
                let email = result.email ?? ""
                let name = result.name ?? ""
                let gender = result.gender ?? ""
                let age = result.age ?? ""
                let birthday = result.birthday ?? ""
                
                print(naverUniqueId)
                print(email)
                print(name)
                print(gender)
                print(age)
                print(birthday)
                let snsUserReq = SnsUserReq(fcmToken: nil, marketAccept: nil, osType: "i", snsAppleCode: nil, snsEmail: email, snsId: naverUniqueId, snsNick: name, snsToken: accessToken, snsType: "n", uuid: nil)
                
                self.snsEmailCheck(snsUserReq: snsUserReq)
            }
            
        }.catch { error in
            
            
        }.finally {
            LoadingView.hide()
        }
        
    }
    
    @available(iOS 13, *)
    func startSignInWithAppleFlow() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    func puljaJoinPolicy(snsUserReq:SnsUserReq){
        
        guard let checkEmail =  snsUserReq.snsEmail else {
            alert(title: nil, message: "가입 할 수 없는 계정입니다.", button: "확인")
            return
        }
        
        var snsUser = snsUserReq;
        let vc = R.storyboard.main.joinPolicyViewController()!
        //                  vc.signupReq = signupReq
        //                  vc.studentName = userName
        //                  vc.fromSeven = self.fromSeven
        //                  vc.selectedSchoolIndex = self.selectedSchoolIndex
        //                  vc.selectedUnit1Index = self.selectedUnit1Index
        
        //무료기능 선택한 중단원 그 다음 컨트롤러 전달
        //                  vc.selectedUnitInfo = self.selectedUnitInfo
        //                  vc.cameFromOnboard = self.cameFromOnboard
        
        
        vc.modalTransitionStyle = .flipHorizontal
        vc.policyCallback = { resultCode in
            
            if resultCode != nil {
                
                snsUser.marketAccept = resultCode
                
                self.puljaJoin(snsUserReq: snsUser)
                
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func puljaJoin(snsUserReq:SnsUserReq) {
        
        guard let checkEmail =  snsUserReq.snsEmail else {
            alert(title: nil, message: "가입 할 수 없는 계정입니다.", button: "확인")
            return
        }
        
        LoadingView.show()
        
        var snsUser = snsUserReq
        
        var uuid = ""
        var fcmToken = ""
        
        if  let token = UserDefaults.standard.string(forKey: Const.UD_DIVICE_TOKEN) {
            fcmToken = token
            uuid = UIDevice.current.identifierForVendor!.uuidString
            
            snsUser.fcmToken = fcmToken
            snsUser.uuid = uuid
        }
        
        firstly {
            //                SignAPI.shared.signin(id: userId, pw: password, fcmToken: fcmToken, uuid: uuid)
            SnsLoginAPI.shared.snsJoin(snsUserReq: snsUser)
        }.then { res in
            when(fulfilled: self.setLoginUserInfo(signinRes: res), self.getHomeData())
        }.done { chatUserRes, homRes in
            
            self.setAirBridgeSignUpEvent(snsUserReq: snsUser)
            
            if let cus = chatUserRes.data {
                var chat_ids:[String]? = []
                for chatUser in cus {
                    chat_ids?.append(chatUser.hashUserSeq!)
                }
                
                let jsonData = try? JSONEncoder().encode(cus)
                let jsonString = String.init(data: jsonData! , encoding: .utf8)
                
                
                let userDefault = UserDefaults.standard
                userDefault.setValue(chat_ids, forKey: Const.UD_CHAT_USER_IDS)
                userDefault.setValue(jsonString, forKey: Const.UD_CHAT_USER_JSON)
                
                userDefault.synchronize()
            } else {
                let userDefault = UserDefaults.standard
                userDefault.removeObject(forKey: Const.UD_CHAT_USER_IDS)
                userDefault.removeObject(forKey: Const.UD_CHAT_USER_JSON)
                userDefault.synchronize()
            }
            
            //PayType 추가.
            //p : paid user
            //f : free user
            //b : paid user before
            //c : coach
            if let userType = homRes.data?.appHomeView?.userType
            {
                let infos = try self.container.viewContext.fetch(MyInfo.fetchRequest())
                if infos.count > 0
                {
                    let myInfo = infos[0]
                    myInfo.setValue(userType, forKey: "payType")
                    try self.container.viewContext.save()
                }
                
            }
            
            
            if self.myInfo != nil, let id = self.myInfo?.hashUserSeq, let nick = self.myInfo?.username,  let userType = self.myInfo?.userType {
                
                LoadingView.hide()
                
                let vc = R.storyboard.main.snsJoinStep1ViewController()!
                vc.isJoin = true
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else {
                LoadingView.hide()
            }
            
            
        }.catch { error in
            LoadingView.hide()
            
            let errtxt = error.localizedDescription
            
            var title:String? = nil
            var message:String? = errtxt
            var buttonText = "확인"
            
            var isDormant = false
            var userName:String? = nil
            
            if errtxt.contains("비밀번호")  {
                title = "다시 한번 확인해주세요."
                message = "해당하는 계정 정보가 없거나 비밀번호가 틀렸어요."
                buttonText = "다시 시도하기"
            } else if errtxt.contains("이메일") {
                title = "다시 한번 확인해주세요."
                message = "해당하는 계정 정보가 없거나 비밀번호가 틀렸어요."
                buttonText = "다시 시도하기"
            } else if errtxt.contains("휴면") {
                isDormant = true
                var startIndex = errtxt.startIndex
                var endIndex = errtxt.endIndex
                if let index = errtxt.index(of: "회원명:") {
                    startIndex = errtxt.index(index, offsetBy: 4)
                }
                let range = startIndex..<endIndex
                userName = errtxt.substring(with: range)
                print("회원명:\(userName)")
            } else if errtxt.contains("탈퇴") {
                title = "다시 한번 확인해주세요."
                message = "탈퇴 등의 사유로 접속할 수 없어요.\n다른 계정을 사용해주세요."
                buttonText = "확인"
            } else {
                message = "알 수 없는 오류가 발생했습니다."
            }
            
            if(isDormant){
                
                let vc = R.storyboard.main.snsLoginViewController()!
                vc.modalTransitionStyle = .flipHorizontal
                vc.snsEmail = checkEmail ?? ""
                vc.snsType = ""
                
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else {
                
                self.alert(title: title, message: message, button: buttonText).done { b in
                    //                            self.tfUserId.text = ""
                    //                            self.tfPassword.text = ""
                }
                
            }
            //            print("@@# error \(error.localizedDescription)")
        }
    }
    
    
    func puljaLogin(snsUserReq:SnsUserReq) {
        LoadingView.show()
        
        var snsUser = snsUserReq
        
        var uuid = ""
        var fcmToken = ""
        
        if  let token = UserDefaults.standard.string(forKey: Const.UD_DIVICE_TOKEN) {
            fcmToken = token
            uuid = UIDevice.current.identifierForVendor!.uuidString
            
            snsUser.fcmToken = fcmToken
            snsUser.uuid = uuid
        }
        
        firstly {
            SnsLoginAPI.shared.snsLogin(snsUserReq: snsUser)
        }.then { res in
            
            when(fulfilled: self.setLoginUserInfo(signinRes: res), self.getHomeData())
        }.done { chatUserRes, homRes in
            
            if let cus = chatUserRes.data {
                var chat_ids:[String]? = []
                for chatUser in cus {
                    chat_ids?.append(chatUser.hashUserSeq!)
                }
                
                let jsonData = try? JSONEncoder().encode(cus)
                let jsonString = String.init(data: jsonData! , encoding: .utf8)
                
                
                let userDefault = UserDefaults.standard
                userDefault.setValue(chat_ids, forKey: Const.UD_CHAT_USER_IDS)
                userDefault.setValue(jsonString, forKey: Const.UD_CHAT_USER_JSON)
                
                userDefault.synchronize()
            } else {
                let userDefault = UserDefaults.standard
                userDefault.removeObject(forKey: Const.UD_CHAT_USER_IDS)
                userDefault.removeObject(forKey: Const.UD_CHAT_USER_JSON)
                userDefault.synchronize()
            }
            
            //PayType 추가.
            //p : paid user
            //f : free user
            //b : paid user before
            //c : coach
            if let userType = homRes.data?.appHomeView?.userType
            {
                let infos = try self.container.viewContext.fetch(MyInfo.fetchRequest())
                if infos.count > 0
                {
                    let myInfo = infos[0]
                    myInfo.setValue(userType, forKey: "payType")
                    try self.container.viewContext.save()
                }
                
            }
            
            
            if self.myInfo != nil, let id = self.myInfo?.hashUserSeq, let nick = self.myInfo?.username,  let userType = self.myInfo?.userType {
                
                LoadingView.hide()
                
                if let user = self.user, let schoolNum = user.schoolNum , schoolNum > 0 {
                    self.goMain(data:homRes.data)
                    
                } else {
                    let vc = R.storyboard.main.snsJoinStep1ViewController()!
                    vc.isJoin = false
                    self.navigationController?.pushViewController(vc, animated: true)   
                }
            } else {
                LoadingView.hide()
            }
            
            
        }.catch { error in
            LoadingView.hide()
            
            let errtxt = error.localizedDescription
            //print("!!! \(errtxt)")
            var title:String? = nil
            var message:String? = errtxt
            var buttonText = "확인"
            
            _ = false
            var _:String? = nil
            
            if errtxt.contains("비밀번호")  {
                title = "다시 한번 확인해주세요."
                message = "해당하는 계정 정보가 없거나 비밀번호가 틀렸어요."
                buttonText = "다시 시도하기"
            } else if errtxt.contains("이메일") {
                title = "다시 한번 확인해주세요."
                message = "해당하는 계정 정보가 없거나 비밀번호가 틀렸어요."
                buttonText = "다시 시도하기"
            } else if errtxt.contains("탈퇴") {
                title = "다시 한번 확인해주세요."
                message = "탈퇴 등의 사유로 접속할 수 없어요.\n다른 계정을 사용해주세요."
                buttonText = "확인"

            } else {
                message = "알 수 없는 오류가 발생했습니다."
            }
            
            self.alert(title: title, message: message, button: buttonText).done { b in
                //                            self.tfUserId.text = ""
                //                            self.tfPassword.text = ""
            }
        }
    }
    
    func getHomeData() -> Promise<HomeRes> {
        return  HomeAPI.shared.home()
    }
    
    func getDiagnosisInfo(signinRes: SigninRes) -> Promise<DiagnosisHistoryListRes>{
        
        if let data = signinRes.data , let userSeq = data.user?.userSeq {
            
            return StudyAPI.shared.diagnosis_test_info(userSeq: userSeq)
        }
        return StudyAPI.shared.diagnosis_test_info(userSeq: 0)
        
    }
    
    func setLoginUserInfo(signinRes: SigninRes) -> Promise<ChatUserRes> {
        
        if let data = signinRes.data {
            
            do {
                self.user = data.user
                
                let infos = try self.container.viewContext.fetch(MyInfo.fetchRequest()) as! [MyInfo]
                for myinfo in infos {
                    self.container.viewContext.delete(myinfo)
                }
                
                let userEntity = NSEntityDescription.entity(forEntityName: "MyInfo", in: self.container.viewContext)
                let user = NSManagedObject(entity: userEntity!, insertInto: self.container.viewContext)
                user.setValue(data.user?.userSeq, forKey: "userSeq")
                user.setValue(data.user?.userId, forKey: "userId")
                user.setValue(data.user?.userType, forKey: "userType")
                user.setValue(data.user?.username, forKey: "username")
                user.setValue(data.user?.hashUserSeq, forKey: "hashUserSeq")
                
                user.setValue(data.user?.snsId, forKey: "snsId")
                user.setValue(data.user?.snsType, forKey: "snsType")
                user.setValue(data.user?.snsEmail, forKey: "snsEmail")
                user.setValue(data.user?.snsNick, forKey: "snsNick")
                
                
                try self.container.viewContext.save()
                
                let userSeq = data.user?.userSeq ?? 0
                FlareLane.setUserId(userId: "\(userSeq)")
                
                
                let newMyInfos = try self.container.viewContext.fetch(MyInfo.fetchRequest()) as! [MyInfo]
                self.myInfo = newMyInfos[0]
                
                if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
                    
                    let user = ABUser()
                    user.id = userId
                    user.email = userId
                    user.attributes = [
                        "userseq": userSeq as NSObject,
                    ]
                    
                    AirBridge.state().setUser(user)
                    
                    let event = ABInAppEvent()
                    event?.setCategory(ABCategory.signIn)
                    event?.send()
                }
            } catch {
                print(error.localizedDescription)
                
            }
            
            CommonUtil.shared.setAccessToken(token: data.accessToken!)
            CommonUtil.shared.setRefreshToken(token: data.refreshToken!)
            CommonUtil.shared.setHeader()
        }
        
        
        return ChatAPI.shared.chatUsers()
        
    }
    
    func goMain(data:Home?){
        // 문제풀이 userdefaults 초기화
        let userDefaultKey1 = "hasSolve"
        let userDefaultKey2 = "hasProblemSeq"
        
        UserDefaults.standard.removeObject(forKey: userDefaultKey1)
        UserDefaults.standard.removeObject(forKey: userDefaultKey2)
        
        if self.myInfo != nil, let userType = self.myInfo?.userType, let userSeq = self.myInfo?.userSeq {
            
            CurationAPI.shared.studyInfo(userSeq: Int(userSeq)).done { studyInfoRes in
                if let hasSolve = studyInfoRes.data?.hasSolve {
                    UserDefaults.standard.set(hasSolve, forKey: userDefaultKey1)
                    UserDefaults.standard.synchronize()
                }
                if let hasProblemSeq = studyInfoRes.data?.hasProblemSeq {
                    UserDefaults.standard.set(hasProblemSeq, forKey: userDefaultKey2)
                    UserDefaults.standard.synchronize()
                }
            }.catch { err in
                print(err)
            }.finally {
                if UserDefaults.standard.bool(forKey: userDefaultKey2) {
                    if userType == "C" {
                        self.goCoach(data: data)
                    } else {
                        self.goStudunt(data:data)
                    }
                } else {
                    let vc = R.storyboard.preparation.keywordViewController()!
                    vc.cameFromOnboard = true
                    vc.isModal = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    
    func goStudunt(data:Home?){
        
        let vcDetail = R.storyboard.main.tabbarController()
        
        if let vc = vcDetail?.viewControllers![0] as? HomeViewController {
            vc.homeInfo = data
        }
        
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDel.window?.rootViewController = vcDetail
        
        // A mask of options indicating how you want to perform the animations.
        let options: UIView.AnimationOptions = .transitionCrossDissolve
        
        // The duration of the transition animation, measured in seconds.
        let duration: TimeInterval = 0.3
        
        // Creates a transition animation.
        // Though `animations` is optional, the documentation tells us that it must not be nil. ¯\_(ツ)_/¯
        UIView.transition(with: appDel.window!, duration: duration, options: options, animations: {}, completion:
                            { completed in
            // maybe do something on completion here
        })
        
        //        if let d = data, let h = d.appHomeView, let userType = h.userType{
        //               if userType != "p" {
        //                   vcDetail?.selectedIndex = 2
        //                   vcDetail?.bottomNavBar.selectedItem = vcDetail?.bottomNavBar.items[2]
        //               }
        //           }
        
        
    }
    
    
    func goCoach(data:Home?) {
        let vcDetail = R.storyboard.main.coachTabbarController()
        
        if let vc = vcDetail?.viewControllers![0] as? HomeViewController {
            vc.homeInfo = data
        }
        
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDel.window?.rootViewController = vcDetail
        
        // A mask of options indicating how you want to perform the animations.
        let options: UIView.AnimationOptions = .transitionCrossDissolve
        
        // The duration of the transition animation, measured in seconds.
        let duration: TimeInterval = 0.3
        
        // Creates a transition animation.
        // Though `animations` is optional, the documentation tells us that it must not be nil. ¯\_(ツ)_/¯
        UIView.transition(with: appDel.window!, duration: duration, options: options, animations: {}, completion:
                            { completed in
            // maybe do something on completion here
        })
    }
    
    func setAirBridgeSignUpEvent(snsUserReq:SnsUserReq){
        var snsService = ""
        if snsUserReq.snsType == "a" {
            snsService = "apple"
        } else if snsUserReq.snsType == "n"  {
            snsService = "naver"
        } else if snsUserReq.snsType == "k"  {
            snsService = "kakao"
        }
        
        //우선 에어브릿지 sign-up이벤트 세팅
        let user = ABUser()
        user.id = snsUserReq.snsEmail
        user.email = snsUserReq.snsEmail
        
        AirBridge.state().setUser(user)
        let event = ABInAppEvent()
        event?.setCategory(ABCategory.signUp)
        event?.setAction("social_login")
        event?.setLabel(snsService)
        event?.send()
    }
    
    
}

extension WeakCat4OnboardViewController : ASAuthorizationControllerPresentationContextProviding, ASAuthorizationControllerDelegate
{
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    @available(iOS 13, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            guard let code = appleIDCredential.authorizationCode else { return }
            guard let token = appleIDCredential.identityToken else { return }
            
            let apple_id = appleIDCredential.user
            
            let apple_code = String(data: code, encoding: .utf8)
            let apple_id_token = String(data: token, encoding: .utf8)
            print(apple_id)
            //print(apple_code)
            //print(apple_id_token)
            var appleEmail = appleIDCredential.email ?? nil
            var appleFullName =  (appleIDCredential.fullName?.familyName ?? "") + (appleIDCredential.fullName?.givenName ?? "")
            
            
            
            if let aId = UserDefaults.standard.string(forKey: "appleId"), aId ==  apple_id {
                
                if let ae = UserDefaults.standard.string(forKey: "appleEmail") {
                    appleEmail = ae
                }
                
                if let an = UserDefaults.standard.string(forKey: "appleFullName") {
                    appleFullName = an
                }
                
            } else if appleEmail != nil {
                
                let userDefault = UserDefaults.standard
                userDefault.setValue(apple_id, forKey: "appleId")
                userDefault.setValue(appleEmail, forKey: "appleEmail")
                userDefault.setValue(appleFullName, forKey: "appleFullName")
                userDefault.synchronize()
                
            }
            
            //print(appleEmail)
            //print(appleFullName)
            
            let snsUserReq = SnsUserReq(fcmToken: nil, marketAccept: nil, osType: "i", snsAppleCode: apple_code, snsEmail: appleEmail, snsId: apple_id, snsNick: appleFullName, snsToken: apple_id_token, snsType: "a", uuid: nil)
            
            self.snsEmailCheck(snsUserReq: snsUserReq)
            
        }
    }
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
        // print("Sign in with Apple errored: \(error)")
        
        guard let error = error as? ASAuthorizationError else {return}
                
             switch error.code {
             case .unknown:
                 print("Sign in with Apple errored: Unknow")
                 self.showSnsLoginFailedAlert(
                     errorMessage: error.localizedDescription,
                     snsType: "a"
                 )
                 break
             default:
                 print("Sign in with Apple errored: Default")
                 break
             }
    }
}

extension WeakCat4OnboardViewController : SwiftyOnboardDelegate, SwiftyOnboardDataSource {
    
    func swiftyOnboardNumberOfPages(_ swiftyOnboard: SwiftyOnboard) -> Int {
        //        return 3
        return 2
    }
    
    func swiftyOnboardPageForIndex(_ swiftyOnboard: SwiftyOnboard, index: Int) -> SwiftyOnboardPage? {
        let view = WeakCat4OnboardPage.instanceFromNib() as? WeakCat4OnboardPage
        if (view?.frame.size.width)! < CGFloat(375) {
            view?.image.widthAnchor.constraint(equalToConstant: (view?.frame.size.width)!).isActive = true
            view?.image.heightAnchor.constraint(equalToConstant: (view?.frame.size.width)!).isActive = true
        }
        
        //        view?.image.image = UIImage(named: "WeakCat4Onboard\(index+1)")
        view?.image.image = UIImage(named: "WeakCat4Onboard\(index+2)")
        
        
        
        
        //        view?.lbTitle.setMinimumLineHeight(minimumLineHeight: 33.6, alignment: .center)
        
        //        if UIDevice.current.userInterfaceIdiom == .pad {
        //            view?.bottomHeight.constant = 30
        //        }
        
        
        //        else {
        //            view?.bottomHeight.constant = 186
        //        }
        //        if index == 0 {
        //            view?.lbTitle.text = "오늘 배운 수학 개념\n정확히 이해하셨나요?"
        //        } else if index == 1 {
        //            view?.lbTitle.text = "원하는 단원으로\n10분만에 진단 받고"
        //        } else {
        //            view?.lbTitle.text = "1분 짤강과 문제 세트로\n취약 유형 정복까지!"
        //        }
        return view
    }
    
    func swiftyOnboardViewForBackground(_ swiftyOnboard: SwiftyOnboard) -> UIView? {
        let view = WeakCat4OnboardPage.instanceFromNib() as? WeakCat4OnboardPage
        if (view?.frame.size.width)! < CGFloat(375) {
            view?.image.widthAnchor.constraint(equalToConstant: (view?.frame.size.width)!).isActive = true
            view?.image.heightAnchor.constraint(equalToConstant: (view?.frame.size.width)!).isActive = true
        }
        return view
    }
    
    func swiftyOnboardViewForOverlay(_ swiftyOnboard: SwiftyOnboard) -> SwiftyOnboardOverlay? {
        
        let overlay = WeakCat4OnboardOverlay.instanceFromNib() as? WeakCat4OnboardOverlay
        overlay?.lbTitle.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 24)
        overlay?.dotTopHeight.constant = CGFloat((overlay?.dotTopHeight.constant)! * view.frame.size.height / 667)
        print("dotTopHeight: \(overlay?.dotTopHeight.constant)")
        overlay?.dotBottomHeight.constant = CGFloat((overlay?.dotBottomHeight.constant ?? 0) * view.frame.size.height / 667)
        print("dotBottomHeight: \(overlay?.dotBottomHeight.constant)")
        //        overlay?.skip.addTarget(self, action: #selector(handleSkip), for: .touchUpInside)
        //        overlay?.buttonContinue.addTarget(self, action: #selector(handleContinue), for: .touchUpInside)
        return overlay
    }
    
    func swiftyOnboardOverlayForPosition(_ swiftyOnboard: SwiftyOnboard, overlay: SwiftyOnboardOverlay, for position: Double) {
        let overlay = overlay as! WeakCat4OnboardOverlay
        let currentPage = round(position)
        
        //        overlay.lbTitle.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 24)
        overlay.pcDot.currentPage = Int(currentPage)
        overlay.pageControl.currentPage = Int(currentPage)
        
        
        
        //        overlay.lbTitle.font = UIFont.fontWithName(type: .bold, size: 24)
        
        //높이 조절
        //        overlay.dotTopHeight.constant = CGFloat(overlay.dotTopHeight.constant * view.frame.size.height / 667)
        //        overlay.dotBottomHeight.constant = CGFloat(overlay.dotBottomHeight.constant * view.frame.size.height / 667)
        //        overlay.labelBottomHeight.constant = CGFloat(overlay.labelBottomHeight.constant * view.frame.size.height / 667)
        
        //이미지 세팅
        //        overlay.imgView.image = UIImage(named: "WeakCat4Onboard\(currentPage+1)")
        //
        //        if currentPage == 0 {
        //            overlay.lbTitle.text = "오늘 배운 수학 개념\n정확히 이해했나요?"
        //        } else if currentPage == 1 {
        //            overlay.lbTitle.text = "원하는 단원으로\n10분만에 진단 받고"
        //        } else {
        //            overlay.lbTitle.text = "1분 짤강과 문제 세트로\n취약 유형 정복까지!"
        //        }
        
        if currentPage == 0 {
            overlay.lbTitle.text = "단 5문제로\n내 취약점 확인하고"
        } else if currentPage == 1 {
            overlay.lbTitle.text = "1분 짤강과 문제 세트로\n취약 유형 정복까지!"
        }
        
        //        overlay.buttonContinue.tag = Int(position)
        //        if currentPage == 0.0 || currentPage == 1.0 {
        //            overlay.buttonContinue.setTitle("Continue", for: .normal)
        //            overlay.skip.setTitle("Skip", for: .normal)
        //            overlay.skip.isHidden = false
        //        } else {
        //            overlay.buttonContinue.setTitle("Get Started!", for: .normal)
        //            overlay.skip.isHidden = true
        //        }
    }
}
