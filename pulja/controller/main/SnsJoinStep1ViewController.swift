//
//  SnsJoinStep1ViewController.swift
//  pulja
//
//  Created by 김병헌 on 2022/09/23.
//

import UIKit
import PromiseKit

class SnsJoinStep1ViewController: UIViewController {

    @IBOutlet weak var btnNext: DesignableButton!
    @IBOutlet weak var btnMiddleSchool: DesignableButton!
    @IBOutlet weak var btnHighSchool: DesignableButton!
    
    @IBOutlet weak var select4View: UIView!
    @IBOutlet weak var select5View: UIView!
    
    
    @IBOutlet weak var btnChoice1: DesignableButton!
    @IBOutlet weak var btnChoice2: DesignableButton!
    @IBOutlet weak var btnChoice3: DesignableButton!
    @IBOutlet weak var btnChoice4: DesignableButton!
    @IBOutlet weak var btnChoice5: DesignableButton!
    
    
    var selectedTabIndex = 0
    var selectedChoiceIndex = -1

    var userSeq: Int?
    var delegate : PuljaBaseViewController?
    

    var grade : String?
    var touchedIdx : Int?
    
    var status : [String: [Bool]] = ["middle" : [false, false, false], "high": [false, false, false, false, false]]
    
    var temp :[UILabel] = []
    
    
    var userId: String?
    
    var schoolType : String?
    var schoolNum : Int?
    var user: User?
    
    var isJoin = false
    
    override func viewDidLoad() {
       
        viewInit()
        
        //getUpdate()
    }

    func viewInit()
    {
        select4View.isHidden = true
        select5View.isHidden = true
        btnNext.isEnabled = false
        
        if self.userSeq == nil
        {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let container = appDelegate.persistentContainer
            do {
                let myInfos = try container.viewContext.fetch(MyInfo.fetchRequest()) as! [MyInfo]
                if myInfos.count > 0
                {
                    let myInfo = myInfos[0]
                    self.userSeq = Int(myInfo.userSeq)
                    self.userId = myInfo.userId
                }
                
            } catch {
                print(error.localizedDescription)
            }
            
            
        }
        
        if Const.isAirBridge {
            CommonUtil.shared.ABEvent(
                category: "diagnosis_poll_try",
                customs: ["userseq": self.userSeq!, "user_id": self.userId!]
            )
        } else {
            print("[ABLog]", "-----------------------------------------", separator: " ")
            print("[ABLog]", "category: diagnosis_poll_try", separator: " ")
            print("[ABLog]", "userseq: \(self.userSeq!)", separator: " ")
            print("[ABLog]", "user_id: \(self.userId!)", separator: " ")
            print("[ABLog]", "-----------------------------------------", separator: " ")
        }
       
        checkDefaultSchoolInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonUtil.tabbarController?.tabBar.isHidden = true
        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func viewDidDisappear(_ animated: Bool) {

        super.viewDidDisappear(animated)
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true

    }
    
    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(animated)
        

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        CommonUtil.tabbarController?.tabBar.isHidden = false
    }

    func checkDefaultSchoolInfo()
    {
        if let schoolInfo = CommonUtil.shared.getSchoolInfo()
        {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let container = appDelegate.persistentContainer
            do {
                let myInfos = try container.viewContext.fetch(MyInfo.fetchRequest()) as! [MyInfo]
                if myInfos.count > 0
                {
                    let myInfo = myInfos[0]
                    let userSeq = myInfo.userSeq
                    let key = "\(userSeq)"

                    if let idxSchool = schoolInfo[key]
                    {
                        switch (idxSchool) {
                        case 1 :
                            selectMiddleSchool()
                            setSelect1()
                            break
                        case 2:
                            selectMiddleSchool()
                            setSelect2()
                            break
                        case 3:
                            selectMiddleSchool()
                            setSelect3()
                            break
                        case 4:
                            selectHighSchool()
                            setSelect1()
                            break
                        case 5:
                            selectHighSchool()
                            setSelect2()
                            break
                        case 6:
                            selectHighSchool()
                            setSelect3()
                            break
                        case 7:
                            selectHighSchool()
                            setSelect4()
                            break
                        case 8:
                            selectHighSchool()
                            setSelect5()
                            break
                        default:
                            break
                        }
                        
                    }
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    func selectMiddleSchool()
    {
        selectedTabIndex = 0
        
        btnMiddleSchool.backgroundColor = .OffWhite
        btnMiddleSchool.titleLabel?.font = UIFont.fontWithName(type: .bold, size: 16)
        btnMiddleSchool.setTitleColor(.puljaBlack, for: .normal)
        
        btnHighSchool.backgroundColor = .white
        btnHighSchool.titleLabel?.font = UIFont.fontWithName(type: .bold, size: 16)
        btnHighSchool.setTitleColor(.Foggy, for: .normal)
        
        resetChoice()

        UIView.performWithoutAnimation {
            btnChoice1.setTitle("1학년", for: .normal)
            btnChoice2.setTitle("2학년", for: .normal)
            btnChoice3.setTitle("3학년", for: .normal)
            btnChoice1.layoutIfNeeded()
            btnChoice2.layoutIfNeeded()
            btnChoice3.layoutIfNeeded()
        }
        
        select4View.isHidden = true
        select5View.isHidden = true
    }
    
    func selectHighSchool()
    {
        selectedTabIndex = 1
        
        btnHighSchool.backgroundColor = .OffWhite
        btnHighSchool.titleLabel?.font = UIFont.fontWithName(type: .bold, size: 16)
        btnHighSchool.setTitleColor(.puljaBlack, for: .normal)
        
        btnMiddleSchool.backgroundColor = .white
        btnMiddleSchool.titleLabel?.font = UIFont.fontWithName(type: .bold, size: 16)
        btnMiddleSchool.setTitleColor(.Foggy, for: .normal)
        
        resetChoice()
        
        UIView.performWithoutAnimation {
            btnChoice1.setTitle("1학년", for: .normal)
            btnChoice2.setTitle("2학년", for: .normal)
            btnChoice3.setTitle("3학년", for: .normal)
            btnChoice1.layoutIfNeeded()
            btnChoice2.layoutIfNeeded()
            btnChoice3.layoutIfNeeded()
        }
        
        
        
        select4View.isHidden = false
        select5View.isHidden = false
    }
    
    
    func setSelect1()
    {
        selectedChoiceIndex = 0
        
        setSelectChoice(button: btnChoice1)
        resetSingleChoice(button: btnChoice2)
        resetSingleChoice(button: btnChoice3)
        resetSingleChoice(button: btnChoice4)
        resetSingleChoice(button: btnChoice5)
        
        enableNext()
    }
    
    func setSelect2()
    {
        selectedChoiceIndex = 1
        setSelectChoice(button: btnChoice2)
        resetSingleChoice(button: btnChoice1)
        resetSingleChoice(button: btnChoice3)
        resetSingleChoice(button: btnChoice4)
        resetSingleChoice(button: btnChoice5)
        
        enableNext()
    }
    func setSelect3()
    {
        selectedChoiceIndex = 2
        setSelectChoice(button: btnChoice3)
        resetSingleChoice(button: btnChoice1)
        resetSingleChoice(button: btnChoice2)
        resetSingleChoice(button: btnChoice4)
        resetSingleChoice(button: btnChoice5)
        
        enableNext()
    }
    func setSelect4()
    {
        selectedChoiceIndex = 3
        setSelectChoice(button: btnChoice4)
        resetSingleChoice(button: btnChoice1)
        resetSingleChoice(button: btnChoice2)
        resetSingleChoice(button: btnChoice3)
        resetSingleChoice(button: btnChoice5)
        
        enableNext()
    }
    func setSelect5()
    {
        selectedChoiceIndex = 4
        setSelectChoice(button: btnChoice5)
        resetSingleChoice(button: btnChoice1)
        resetSingleChoice(button: btnChoice2)
        resetSingleChoice(button: btnChoice3)
        resetSingleChoice(button: btnChoice4)
        
        enableNext()
    }
    
    
    
    @IBAction func ibBack(_ sender: Any) {
//        self.dismiss(animated : true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnMiddleSchoolPressed(_ sender: Any) {
        if selectedTabIndex == 0 {
            return
        }
        selectMiddleSchool()
        
    }
    @IBAction func btnHighSchoolPressed(_ sender: Any) {
        
        if selectedTabIndex == 1 {
            return
        }
        selectHighSchool()
    }
    
    
    @IBAction func btnChoice1Pressed(_ sender: Any) {
        if selectedChoiceIndex == 0 {
            return
        }
        setSelect1()
        
    }
    @IBAction func btnChoice2Pressed(_ sender: Any) {
        if selectedChoiceIndex == 1 {
            return
        }
        setSelect2()
    }
    @IBAction func btnChoice3Pressed(_ sender: Any) {
        if selectedChoiceIndex == 2 {
            return
        }
        setSelect3()
    }
    @IBAction func btnChoice4Pressed(_ sender: Any) {
        if selectedChoiceIndex == 3 {
            return
        }
        setSelect4()
    }
    @IBAction func btnChoice5Pressed(_ sender: Any) {
        if selectedChoiceIndex == 4 {
            return
        }
        setSelect5()
    }
    

    
    func resetChoice()
    {
        selectedChoiceIndex = -1
        
        resetSingleChoice(button: btnChoice1)
        resetSingleChoice(button: btnChoice2)
        resetSingleChoice(button: btnChoice3)
        resetSingleChoice(button: btnChoice4)
        resetSingleChoice(button: btnChoice5)
        
        btnNext.setTitleColor(.Foggy, for: .normal)
        btnNext.setBackgroundColor(.Foggy20, for: .normal)
        btnNext.isEnabled = false
    }
    
    func setSelectChoice(button : UIButton)
    {
        button.setTitleColor(.OffPurple, for: .normal)
        button.layer.borderColor = UIColor.OffPurple.cgColor
        button.layer.borderWidth = 1.5
        button.titleLabel?.font = UIFont.fontWithName(type: .bold, size: 16)
    }
    
    func resetSingleChoice(button : UIButton)
    {
        button.titleLabel?.font = UIFont.fontWithName(type: .regular, size: 16)
        button.setTitleColor(.OffBlack, for: .normal)
        button.layer.borderColor = UIColor.Foggy.cgColor
        button.layer.borderWidth = 1
    }
    
    func enableNext()
    {
        btnNext.setTitleColor(.white, for: .normal)
        btnNext.setBackgroundColor(.OffPurple, for: .normal)
        btnNext.isEnabled = true
    }
    
    
    @IBAction func ibNext(_ sender: Any) {        
        var nzSchoolInfo = 0
        
        if selectedTabIndex == 0 {
            
            schoolType = "1"
            if selectedChoiceIndex == 0  {
                schoolNum = 1
                nzSchoolInfo = 1
            } else if selectedChoiceIndex == 1 {
                schoolNum = 2
                nzSchoolInfo = 2
            } else if selectedChoiceIndex == 2 {
                schoolNum = 3
                nzSchoolInfo = 3
            }
            
            let key = "\(self.userSeq ?? 0)"
            CommonUtil.shared.setSchoolInfo(info: [key : nzSchoolInfo])
//            self.navigationController?.pushViewController(vc, animated: false)

        } else if selectedTabIndex == 1 {
            
            schoolType = "2"
            if selectedChoiceIndex == 0  {
                schoolNum = 1
                nzSchoolInfo = 4
            } else if selectedChoiceIndex == 1 {
                schoolNum = 2
                nzSchoolInfo = 5
            } else if selectedChoiceIndex == 2 {
                schoolNum = 3
                nzSchoolInfo = 6
            } else if selectedChoiceIndex == 3 {
                schoolType = "3"
                schoolNum = 4
                nzSchoolInfo = 7
            } else if selectedChoiceIndex == 4 {
                schoolType = "3"
                schoolNum = 5
                nzSchoolInfo = 8
            }
            
            let key = "\(self.userSeq ?? 0)"
            CommonUtil.shared.setSchoolInfo(info: [key : nzSchoolInfo])
//            self.navigationController?.pushViewController(vc, animated: true)

        }
        
        goNext()

    }
    
    func goNext(){
        LoadingView.show()
      
        
        firstly {
            UserAPI.shared.myInfo()
        }.then { res in
            self.changeSchoolInfo(userData : res.data!)
        }.done { res in
            LoadingView.hide()
            if let bool = res.success, bool == true {
                self.goStudunt()
            }
        }.catch { e in
            LoadingView.hide()
        }
        
    }
        
    func changeSchoolInfo(userData : User) -> Promise<UserRes> {
        
        return  UserAPI.shared.changeSchoolInfo(schoolNum: self.schoolNum ?? 0, schoolType: self.schoolType ?? "", mockTestGrade: 0, user : userData)
    }
    
    func goStudunt(){

        guard var controllers = self.navigationController?.viewControllers else {
            return
            
        }
               
        let vc = R.storyboard.preparation.keywordViewController()!
        vc.cameFromOnboard = true
        vc.isModal = !isJoin
        self.navigationController?.pushViewController(vc, animated: true)
        

//        let vcDetail = R.storyboard.main.tabbarController()
//        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDel.window?.rootViewController = vcDetail
//
//        //코드 추가 09.25.22
//        guard var controllers = self.navigationController?.viewControllers else {
//            return
//        }
//
//        let secondVc = R.storyboard.preparation.keywordViewController()!
//        controllers.append(secondVc)
//
//        self.navigationController?.setViewControllers(controllers, animated: true)
//
//        let options: UIView.AnimationOptions = .transitionCrossDissolve
//        let duration: TimeInterval = 0.3
//        UIView.transition(with: appDel.window!, duration: duration, options: options, animations: {}, completion: { completed in
//            // maybe do something on completion here
//
//        })
       
   }
    
    
    func getUpdate() {
       //큐레이션 업데이트 알람
        if !UserDefaults.standard.bool(forKey: "hasProblemSeq") {
            //알러트 띄어야 함
            self.customAlert().done { res in }
            .catch { err in }
            .finally {}
        }
   }
    
    func customAlert() -> Promise<Bool> {
        return Promise { seal in
            
            let alert = R.storyboard.main.updateAlertViewController()!
            
            alert.callback = { check in
                seal.resolve(.fulfilled(check))
            }
            
            alert.modalPresentationStyle = .overCurrentContext
            present(alert, animated: false, completion: nil)

        }
    }
     
}
