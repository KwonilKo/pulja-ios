//
//  SnsLoginViewController.swift
//  pulja
//
//  Created by 김병헌 on 2022/08/31.
//

import UIKit
import KakaoSDKCommon
import KakaoSDKAuth
import KakaoSDKUser
import NaverThirdPartyLogin
import Alamofire
import AuthenticationServices
import PromiseKit
import AirBridge
import FlareLane
import ChannelIOFront
import CoreData


class SnsLoginViewController: PuljaBaseViewController , NaverThirdPartyLoginConnectionDelegate {
    
    var snsEmail = ""
    
    var snsType = ""
    
    let loginInstance = NaverThirdPartyLoginConnection.getSharedInstance()
    
    
    @IBOutlet weak var lbExistService: UILabel!
    
    @IBOutlet weak var lbEmail: UILabel!
    
    @IBOutlet weak var lbSnsType: UILabel!
    
    @IBOutlet weak var btNaver: UIButton!
    
    @IBOutlet weak var btKakao: UIButton!
    
    @IBOutlet weak var btApple: UIButton!
    
    @IBOutlet weak var btEmail: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btNaver.isHidden = true
        btKakao.isHidden = true
        btApple.isHidden = true
        btEmail.isHidden = true
        setServiceInfo()
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    func setServiceInfo()  {
        lbEmail.text = snsEmail
        
        if snsType == "a" {
            lbExistService.text = "Apple 계정이 있어요!"
            lbSnsType.text = "Apple"
            btApple.isHidden  = false
        } else if snsType == "n" {
            lbExistService.text = "네이버 계정이 있어요!"
            lbSnsType.text = "네이버"
            btNaver.isHidden  = false
        } else if snsType == "k" {
            lbExistService.text = "카카오 계정이 있어요!"
            lbSnsType.text = "카카오"
            btKakao.isHidden  = false
        } else if snsType == "" {
            lbExistService.text = "풀자 계정이 있어요!"
            lbSnsType.text = "풀자"
            btEmail.isHidden  = false
        }
    }
    
    func showSnsLoginFailedAlert(errorMessage: String, snsType: String) {
        
        //ab180 이벤트 보내기
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: "pulja2.0_snslogin_error",
                    customs:["errorMessage" : errorMessage, "snsType": snsType]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "category: pulja2.0_snslogin_error", separator: " ")
                print("[ABLog]", "errorMessage: \(errorMessage)", separator: " ")
                print("[ABLog]", "snsType: \(snsType)", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
        }
        
        CommonUtil().showHAlert(title: "로그인에 실패했어요", message: "다시 한번 시도 후에도 오류가\n지속될 경우 고객센터로 문의해주세요.", button: "확인", subButton: "문의하기", viewController: self) { isSend in
            if !isSend {
                self.initChennelTalk()
            }
        }
    }
    
    func initChennelTalk(){
        let profile = Profile()
            .set(name: "guest")
        
        let buttonOption = ChannelButtonOption.init(
            position: .right,
            xMargin: 16,
            yMargin: 23
        )
        
        let bootConfig = BootConfig.init(
            pluginKey: Const.CHANNELTALK_PLUGIN_KEY,
            memberId: "guest",
            //          memberHash: "",
            profile: profile,
            channelButtonOption: buttonOption,
            hidePopup: false,
            trackDefaultEvent: true,
            language: .korean
        )
        
        ChannelIO.boot(with: bootConfig) { (bootStatus, user) in
            if bootStatus == .success {
                // success
                ChannelIO.showMessenger()
            } else {
                // show failed reason from bootStatus
            }
        }
    }
    
    @objc func didBecomeActive(){
        LoadingView.hide()
    }
    
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func btnKakaoPressed(_ sender: Any) {
        
        if (UserApi.isKakaoTalkLoginAvailable()) {
            LoadingView.show()
            UserApi.shared.loginWithKakaoTalk {(oauthToken, error) in
                if let error = error {
                    print("kakaotalk login() failed -> \(error)")
                    // 알수없는오류
                    if (error.localizedDescription.contains("Unknown")) {
                        self.showSnsLoginFailedAlert(
                            errorMessage: error.localizedDescription,
                            snsType: "k"
                        )
                    }
                    LoadingView.hide()
                } else {
                    
                    print("loginWithKakaoTalk() success.")
                    
                    //do something
                    _ = oauthToken
                    
                    UserApi.shared.me() { (user, error) in
                        if let error = error {
                            print("kakaotalk me() failed -> \(error)")
                            LoadingView.hide()
                            // 알수없는오류
                            if (error.localizedDescription.contains("Unknown")) {
                                self.showSnsLoginFailedAlert(
                                    errorMessage: error.localizedDescription,
                                    snsType: "k"
                                )
                            }
                        } else {
                            LoadingView.hide()
                            print("me() success.")
                            let name = user?.kakaoAccount?.profile?.nickname ?? ""
                            let email = user?.kakaoAccount?.email ?? ""
                            let kakaoId = user?.id ?? 0
                            print(name)
                            print(email)
                            //do something
                            _ = user
                            
                            let snsUserReq = SnsUserReq(fcmToken: nil, marketAccept: nil, osType: "i", snsAppleCode: nil, snsEmail: email, snsId: String(kakaoId), snsNick: name, snsToken: oauthToken?.accessToken, snsType: "k", uuid: nil)
                            
                            self.snsEmailCheck(snsUserReq: snsUserReq)
                        }
                    }
                    
                    
                }
            }
        }else {
            LoadingView.show()
            UserApi.shared.loginWithKakaoAccount {(oauthToken, error) in
                if let error = error {
                    print("kakao login() failed -> \(error)")
                    // 알수없는오류
                    if (error.localizedDescription.contains("Unknown")) {
                        self.showSnsLoginFailedAlert(
                            errorMessage: error.localizedDescription,
                            snsType: "k"
                        )
                    }
                    LoadingView.hide()
                }
                else {
                    print("loginWithKakaoAccount() success.")
                    
                    //do something
                    _ = oauthToken
                    
                    UserApi.shared.me() { (user, error) in
                        if let error = error {
                            print("kakao me() failed -> \(error)")
                            // 알수없는오류
                            if (error.localizedDescription.contains("Unknown")) {
                                self.showSnsLoginFailedAlert(
                                    errorMessage: error.localizedDescription,
                                    snsType: "k"
                                )
                            }
                            LoadingView.hide()
                        } else {
                            LoadingView.hide()
                            print("me() success.")
                            let name = user?.kakaoAccount?.profile?.nickname ?? ""
                            let email = user?.kakaoAccount?.email ?? ""
                            let kakaoId = user?.id ?? 0
                            print(name)
                            print(email)
                            //do something
                            _ = user
                            
                            let snsUserReq = SnsUserReq(fcmToken: nil, marketAccept: nil, osType: "i", snsAppleCode: nil, snsEmail: email, snsId: String(kakaoId), snsNick: name, snsToken: oauthToken?.accessToken, snsType: "k", uuid: nil)
                            
                            self.snsEmailCheck(snsUserReq: snsUserReq)
                        }
                    }
                }
            }
        }
    }
    
    func snsEmailCheck(snsUserReq:SnsUserReq){
        LoadingView.show()
        SnsLoginAPI.shared.snsEmailCheck(snsUserReq: snsUserReq).done { chkRes in
            LoadingView.hide()
            if let checkEmailData = chkRes.data {
                
                let checkEmail = checkEmailData.snsEmail
                let checkServiceType = checkEmailData.snsType
                let checkSnsId = checkEmailData.snsId
                
                if checkEmail == nil { //없으면 가입.
                    
                    self.alert(title: nil, message: "로그인 할 수 없는 계정입니다.", button: "확인")
                    
                } else if snsUserReq.snsId == checkSnsId && snsUserReq.snsType == checkServiceType { //같으면 로그인
                    
                    self.puljaLogin(snsUserReq: snsUserReq)
                    
                } else if snsUserReq.snsEmail == checkEmail && snsUserReq.snsType != checkServiceType  { //다르면 다른 소셜 서비스 로그인 안내 패이지.
                    
                    self.alert(title: nil, message: "로그인 할 수 없는 계정입니다.", button: "확인")
                } else {
                    self.alert(title: nil, message: "로그인 할 수 없는 계정입니다.", button: "확인")
                }
            }else {
                self.alert(title: nil, message: "로그인 할 수 없는 계정입니다.", button: "확인")
            }
        }
    }
    
    @IBAction func btnNaverPressed(_ sender: Any) {
        LoadingView.show()
        loginInstance?.resetToken()
        if(loginInstance?.accessToken == nil) {
            loginInstance?.requestThirdPartyLogin()
        } else {
            if let isValidAccessToken = loginInstance?.isValidAccessTokenExpireTimeNow() {
                if !isValidAccessToken {
                    loginInstance?.requestAccessTokenWithRefreshToken()
                } else {
                    getInfo()
                }
            } else {
                loginInstance?.requestThirdPartyLogin()
            }
        }
    }
    
    
    @IBAction func btnApplePressed(_ sender: Any) {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            if let userID = UserDefaults.standard.string(forKey: "currentUserIdentifier") {
                appleIDProvider.getCredentialState(forUserID: userID) { (credentialState, error) in
                    switch credentialState {
                    case .authorized:
                        // The Apple ID credential is valid.
                        // 유저를 확인하여 바로 로그인한다.
                        break
                    case .revoked:
                        // The Apple ID credential is revoked.
                        UserDefaults.standard.removeObject(forKey: "currentUserIdentifier")
                        self.startSignInWithAppleFlow()
                        break
                    case .notFound:
                        // No credential was found, so show the sign-in UI.
                        break
                    default:
                        break
                    }
                }
            } else {
                self.startSignInWithAppleFlow()
            }
        }
        
    }
    
    @IBAction func emailLoginAction(_ sender: Any) {
        let vc = R.storyboard.main.loginViewController()!
        vc.modalTransitionStyle = .flipHorizontal
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // 로그인에 성공한 경우 호출
    func oauth20ConnectionDidFinishRequestACTokenWithAuthCode() {
        LoadingView.hide()
        print("Success login")
        getInfo()
    }
    
    // referesh token
    func oauth20ConnectionDidFinishRequestACTokenWithRefreshToken() {
        getInfo()
    }
    
    // 로그아웃
    func oauth20ConnectionDidFinishDeleteToken() {
        LoadingView.hide()
        print("log out")
    }
    
    // 모든 error
    func oauth20Connection(_ oauthConnection: NaverThirdPartyLoginConnection!, didFailWithError error: Error!) {
        LoadingView.hide()
        print("error = \(error.localizedDescription)")
        // 알수없는오류
        if (error.localizedDescription.contains("server_error") ||
            error.localizedDescription.contains("Internal Server Error")) {
            self.showSnsLoginFailedAlert(
                errorMessage: error.localizedDescription,
                snsType: "n"
            )
        }
    }
    
    
    // RESTful API, id가져오기
    func getInfo() {
        guard let isValidAccessToken = loginInstance?.isValidAccessTokenExpireTimeNow() else { return }
        
        if !isValidAccessToken {
            return
        }
        
        guard let tokenType = loginInstance?.tokenType else { return }
        guard let accessToken = loginInstance?.accessToken else { return }
        
        print("accessToken : \(accessToken)")
        
        
        
        LoadingView.show()
        SignAPI.shared.naverLogin(tokenType: tokenType, accessToken: accessToken).done { res in
            
            if let result = res.response
            {
                let naverUniqueId = result.id ?? ""
                let email = result.email ?? ""
                let name = result.name ?? ""
                let gender = result.gender ?? ""
                let age = result.age ?? ""
                let birthday = result.birthday ?? ""
                
                print(naverUniqueId)
                print(email)
                print(name)
                print(gender)
                print(age)
                print(birthday)
                let snsUserReq = SnsUserReq(fcmToken: nil, marketAccept: nil, osType: "i", snsAppleCode: nil, snsEmail: email, snsId: naverUniqueId, snsNick: name, snsToken: accessToken, snsType: "n", uuid: nil)
                
                self.snsEmailCheck(snsUserReq: snsUserReq)
            }
            
        }.catch { error in
            
            
        }.finally {
            LoadingView.hide()
        }
        
    }
    
    @available(iOS 13, *)
    func startSignInWithAppleFlow() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    
    func puljaLogin(snsUserReq:SnsUserReq) {
        
        LoadingView.show()
        
        var snsUser = snsUserReq
        
        var uuid = ""
        var fcmToken = ""
        
        if  let token = UserDefaults.standard.string(forKey: Const.UD_DIVICE_TOKEN) {
            fcmToken = token
            uuid = UIDevice.current.identifierForVendor!.uuidString
            
            snsUser.fcmToken = fcmToken
            snsUser.uuid = uuid
        }
        
        firstly {
            SnsLoginAPI.shared.snsLogin(snsUserReq: snsUser)
        }.then { res in
            when(fulfilled: self.setLoginUserInfo(signinRes: res), self.getHomeData())
        }.done { chatUserRes, homRes in
            
            if let cus = chatUserRes.data {
                var chat_ids:[String]? = []
                for chatUser in cus {
                    chat_ids?.append(chatUser.hashUserSeq!)
                }
                
                let jsonData = try? JSONEncoder().encode(cus)
                let jsonString = String.init(data: jsonData! , encoding: .utf8)
                
                
                let userDefault = UserDefaults.standard
                userDefault.setValue(chat_ids, forKey: Const.UD_CHAT_USER_IDS)
                userDefault.setValue(jsonString, forKey: Const.UD_CHAT_USER_JSON)
                
                userDefault.synchronize()
            } else {
                let userDefault = UserDefaults.standard
                userDefault.removeObject(forKey: Const.UD_CHAT_USER_IDS)
                userDefault.removeObject(forKey: Const.UD_CHAT_USER_JSON)
                userDefault.synchronize()
            }
            
            //PayType 추가.
            //p : paid user
            //f : free user
            //b : paid user before
            //c : coach
            if let userType = homRes.data?.appHomeView?.userType
            {
                let infos = try self.container.viewContext.fetch(MyInfo.fetchRequest())
                if infos.count > 0
                {
                    let myInfo = infos[0]
                    myInfo.setValue(userType, forKey: "payType")
                    try self.container.viewContext.save()
                }
                
            }
            
            
            if self.myInfo != nil, let id = self.myInfo?.hashUserSeq, let nick = self.myInfo?.username,  let userType = self.myInfo?.userType {
                LoadingView.hide()
                self.goMain(data:homRes.data)
            } else {
                LoadingView.hide()
            }
            
            
        }.catch { error in
            LoadingView.hide()
            
            let errtxt = error.localizedDescription
            
            var title:String? = nil
            var message:String? = errtxt
            var buttonText = "확인"
            
            var isDormant = false
            var userName:String? = nil
            
            if errtxt.contains("비밀번호")  {
                title = "다시 한번 확인해주세요."
                message = "해당하는 계정 정보가 없거나 비밀번호가 틀렸어요."
                buttonText = "다시 시도하기"
            } else if errtxt.contains("이메일") {
                title = "다시 한번 확인해주세요."
                message = "해당하는 계정 정보가 없거나 비밀번호가 틀렸어요."
                buttonText = "다시 시도하기"
            } else if errtxt.contains("탈퇴") {
                title = "다시 한번 확인해주세요."
                message = "탈퇴 등의 사유로 접속할 수 없어요.\n다른 계정을 사용해주세요."
                buttonText = "확인"
                
            } else {
                message = "알 수 없는 오류가 발생했습니다."
            }
            
            if(isDormant){
                
            } else {
                
                self.alert(title: title, message: message, button: buttonText).done { b in
                }
                
            }
        }
    }
    
    func getHomeData() -> Promise<HomeRes> {
        return  HomeAPI.shared.home()
    }
    
    func getDiagnosisInfo(signinRes: SigninRes) -> Promise<DiagnosisHistoryListRes>{
        
        if let data = signinRes.data , let userSeq = data.user?.userSeq {
            
            return StudyAPI.shared.diagnosis_test_info(userSeq: userSeq)
        }
        return StudyAPI.shared.diagnosis_test_info(userSeq: 0)
        
    }
    
    func setLoginUserInfo(signinRes: SigninRes) -> Promise<ChatUserRes> {
        
        if let data = signinRes.data {
            
            do {
                let infos = try self.container.viewContext.fetch(MyInfo.fetchRequest()) as! [MyInfo]
                for myinfo in infos {
                    self.container.viewContext.delete(myinfo)
                }
                
                let userEntity = NSEntityDescription.entity(forEntityName: "MyInfo", in: self.container.viewContext)
                let user = NSManagedObject(entity: userEntity!, insertInto: self.container.viewContext)
                user.setValue(data.user?.userSeq, forKey: "userSeq")
                user.setValue(data.user?.userId, forKey: "userId")
                user.setValue(data.user?.userType, forKey: "userType")
                user.setValue(data.user?.username, forKey: "username")
                user.setValue(data.user?.hashUserSeq, forKey: "hashUserSeq")
                
                user.setValue(data.user?.snsId, forKey: "snsId")
                user.setValue(data.user?.snsType, forKey: "snsType")
                user.setValue(data.user?.snsEmail, forKey: "snsEmail")
                user.setValue(data.user?.snsNick, forKey: "snsNick")
                
                
                try self.container.viewContext.save()
                
                let userSeq = data.user?.userSeq ?? 0
                FlareLane.setUserId(userId: "\(userSeq)")
                
                
                let newMyInfos = try self.container.viewContext.fetch(MyInfo.fetchRequest()) as! [MyInfo]
                self.myInfo = newMyInfos[0]
                
                if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
                    
                    let user = ABUser()
                    user.id = userId
                    user.email = userId
                    user.attributes = [
                        "userseq": userSeq as NSObject,
                    ]
                    
                    AirBridge.state().setUser(user)
                    
                    let event = ABInAppEvent()
                    event?.setCategory(ABCategory.signIn)
                    event?.send()
                }
            } catch {
                print(error.localizedDescription)
                
            }
            
            CommonUtil.shared.setAccessToken(token: data.accessToken!)
            CommonUtil.shared.setRefreshToken(token: data.refreshToken!)
            CommonUtil.shared.setHeader()
        }
        
        
        return ChatAPI.shared.chatUsers()
        
    }
    
    
    func goMain(data:Home?){
        // 문제풀이 userdefaults 초기화
        let userDefaultKey1 = "hasSolve"
        let userDefaultKey2 = "hasProblemSeq"
        
        UserDefaults.standard.removeObject(forKey: userDefaultKey1)
        UserDefaults.standard.removeObject(forKey: userDefaultKey2)
        
        if self.myInfo != nil, let userType = self.myInfo?.userType, let userSeq = self.myInfo?.userSeq {
            
            CurationAPI.shared.studyInfo(userSeq: Int(userSeq)).done { studyInfoRes in
                if let hasSolve = studyInfoRes.data?.hasSolve {
                    UserDefaults.standard.set(hasSolve, forKey: userDefaultKey1)
                    UserDefaults.standard.synchronize()
                }
                if let hasProblemSeq = studyInfoRes.data?.hasProblemSeq {
                    UserDefaults.standard.set(hasProblemSeq, forKey: userDefaultKey2)
                    UserDefaults.standard.synchronize()
                }
            }.catch { err in
                print(err)
            }.finally {
                if UserDefaults.standard.bool(forKey: userDefaultKey2) {
                    if userType == "C" {
                        self.goCoach(data: data)
                    } else {
                        self.goStudunt(data:data)
                    }
                } else {
                    let vc = R.storyboard.preparation.keywordViewController()!
                    vc.cameFromOnboard = true
                    vc.isModal = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    
    func goStudunt(data:Home?){
        
        let vcDetail = R.storyboard.main.tabbarController()
        
        if let vc = vcDetail?.viewControllers![0] as? HomeViewController {
            vc.homeInfo = data
        }
        
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDel.window?.rootViewController = vcDetail
        
        // A mask of options indicating how you want to perform the animations.
        let options: UIView.AnimationOptions = .transitionCrossDissolve
        
        // The duration of the transition animation, measured in seconds.
        let duration: TimeInterval = 0.3
        
        // Creates a transition animation.
        // Though `animations` is optional, the documentation tells us that it must not be nil. ¯\_(ツ)_/¯
        UIView.transition(with: appDel.window!, duration: duration, options: options, animations: {}, completion:
                            { completed in
            // maybe do something on completion here
        })
        
        //            if let d = data, let h = d.appHomeView, let userType = h.userType{
        //               if userType != "p" {
        //                   vcDetail?.selectedIndex = 2
        //                   vcDetail?.bottomNavBar.selectedItem = vcDetail?.bottomNavBar.items[2]
        //               }
        //           }
        
        
    }
    
    
    func goCoach(data:Home?) {
        let vcDetail = R.storyboard.main.coachTabbarController()
        
        if let vc = vcDetail?.viewControllers![0] as? HomeViewController {
            vc.homeInfo = data
        }
        
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDel.window?.rootViewController = vcDetail
        
        // A mask of options indicating how you want to perform the animations.
        let options: UIView.AnimationOptions = .transitionCrossDissolve
        
        // The duration of the transition animation, measured in seconds.
        let duration: TimeInterval = 0.3
        
        // Creates a transition animation.
        // Though `animations` is optional, the documentation tells us that it must not be nil. ¯\_(ツ)_/¯
        UIView.transition(with: appDel.window!, duration: duration, options: options, animations: {}, completion:
                            { completed in
            // maybe do something on completion here
        })
    }
    
    
}

extension SnsLoginViewController : ASAuthorizationControllerPresentationContextProviding, ASAuthorizationControllerDelegate
{
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    @available(iOS 13, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            guard let code = appleIDCredential.authorizationCode else { return }
            guard let token = appleIDCredential.identityToken else { return }
            
            let apple_id = appleIDCredential.user
            
            let apple_code = String(data: code, encoding: .utf8)
            let apple_id_token = String(data: token, encoding: .utf8)
            print(apple_id)
            print(apple_code)
            print(apple_id_token)
            var appleEmail = appleIDCredential.email ?? ""
            var appleFullName =  (appleIDCredential.fullName?.givenName ?? "") + " " + (appleIDCredential.fullName?.familyName ?? "")
            
            print(appleEmail)
            print(appleFullName)
            
            if let aId = UserDefaults.standard.string(forKey: "appleId"), aId ==  apple_id {
                
                if let ae = UserDefaults.standard.string(forKey: "appleEmail") {
                    appleEmail = ae
                }
                
                if let an = UserDefaults.standard.string(forKey: "appleFullName") {
                    appleFullName = an
                }
                
            } else if appleEmail != nil {
                
                let userDefault = UserDefaults.standard
                userDefault.setValue(apple_id, forKey: "appleId")
                userDefault.setValue(appleEmail, forKey: "appleEmail")
                userDefault.setValue(appleFullName, forKey: "appleFullName")
                userDefault.synchronize()
                
            }
            
            let snsUserReq = SnsUserReq(fcmToken: nil, marketAccept: nil, osType: "i", snsAppleCode: apple_code, snsEmail: appleEmail, snsId: apple_id, snsNick: appleFullName, snsToken: apple_id_token, snsType: "a", uuid: nil)
            
            self.snsEmailCheck(snsUserReq: snsUserReq)
            
        }
    }
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
        // print("Sign in with Apple errored: \(error)")
        
        guard let error = error as? ASAuthorizationError else {return}
             switch error.code {
             case .unknown:
                 print("Sign in with Apple errored: Unknow")
                 self.showSnsLoginFailedAlert(
                     errorMessage: error.localizedDescription,
                     snsType: "a"
                 )
                 break
             default:
                 print("Sign in with Apple errored: Default")
                 break
             }
    }
}
