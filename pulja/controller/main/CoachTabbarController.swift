//
//  CoachTabbarController.swift
//  pulja
//
//  Created by 김병헌 on 2022/03/08.
//

import UIKit
import MaterialComponents.MaterialBottomNavigation

class CoachTabbarController: UITabBarController , MDCBottomNavigationBarDelegate {
    
    let bottomNavBar = MDCBottomNavigationBar()
    
    var homeData:Home? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        initData()
        CommonUtil.coachTabbarController = self
        
       
    }

    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden( true, animated: animated )
    }

    //Initialize Bottom Bar
    init()
    {
        super.init(nibName: nil, bundle: nil)
        bottomNavigationInit()
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        bottomNavigationInit()
    }

    func bottomNavigationInit()
    {
        guard let arrControllers = self.viewControllers , arrControllers.count == 3 else { return }
        
        view.backgroundColor = .lightGray
        view.addSubview(bottomNavBar)

        // Always show bottom navigation bar item titles.
        bottomNavBar.titleVisibility = .always

        // Cluster and center the bottom navigation bar items.
        bottomNavBar.alignment = .centered

        let homeVC = arrControllers[0]
        let chatVC = arrControllers[1]
        let settingVC = arrControllers[2]
        
        
        // Add items to the bottom navigation bar.
        let tabBarItem1 = UITabBarItem(title: "투데이", image: UIImage(named: "iconOutlineHome"), tag: 0)
        let tabBarItem2 = UITabBarItem(title: "채팅", image: UIImage(named: "iconOutlineChat"), tag: 1)
        let tabBarItem3 = UITabBarItem(title: "설정", image: UIImage(named: "iconOutlineCog"), tag: 2)
        
        tabBarItem1.selectedImage = UIImage(named: "iconSolidHome")
        tabBarItem2.selectedImage = UIImage(named: "iconSolidChat")
        tabBarItem3.selectedImage = UIImage(named: "iconSolidCog")
        
        
        
        homeVC.tabBarItem = tabBarItem1
        chatVC.tabBarItem = tabBarItem2
        settingVC.tabBarItem = tabBarItem3

        
        bottomNavBar.items = [tabBarItem1, tabBarItem2, tabBarItem3]
        
        bottomNavBar.unselectedItemTintColor = UIColor.lightBlueGrey
        bottomNavBar.barTintColor = .white
        bottomNavBar.itemTitleFont = UIFont.boldSystemFont(ofSize: 12.0)

        // Select a bottom navigation bar item.
        bottomNavBar.selectedItem = tabBarItem1;
        bottomNavBar.delegate = self
    }


    func bottomNavigationBar(_ bottomNavigationBar: MDCBottomNavigationBar, didSelect item: UITabBarItem)
    {
        print("did select item \(item.tag)")
        
        self.selectedIndex = item.tag
        self.selectedViewController = self.viewControllers![item.tag]
        
        

       
        
    }

    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        layoutBottomNavBar()
    }

    #if swift(>=3.2)
    @available(iOS 11, *)
    override func viewSafeAreaInsetsDidChange()
    {
        super.viewSafeAreaInsetsDidChange()
        
        
        layoutBottomNavBar()
    }
    #endif

    // Setting Bottom Bar
    func layoutBottomNavBar()
    {
        let size = bottomNavBar.sizeThatFits(view.bounds.size)
            var bottomNavBarFrame = CGRect(
              x: 0,
              y: view.bounds.height - size.height,
              width: size.width,
              height: size.height)
            bottomNavBarFrame.size.height += view.safeAreaInsets.bottom
            bottomNavBarFrame.origin.y -= view.safeAreaInsets.bottom
            bottomNavBar.frame = bottomNavBarFrame
    }

    
    func initData() {
        
    }
}

