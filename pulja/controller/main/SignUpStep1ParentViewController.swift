//
//  SignUpStep1ParentViewController.swift
//  pulja
//
//  Created by 김병헌 on 2021/12/20.
//

import UIKit
import TTGSnackbar
import MaterialComponents.MaterialBottomSheet

class SignUpStep1ParentViewController: PuljaBaseViewController, MDCBottomSheetControllerDelegate {
    
    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var tfPhoneNumber: UITextField!
    @IBOutlet weak var btCheckParent: UIButton!
    @IBOutlet weak var btJoinAgree: UIButton!
    
    
    @IBOutlet weak var lbAgreeName: UILabel!
    
    var activeTextField : UITextField? = nil
    
    var isCheckInput = false
    
    var isCheckParent = false
    
    var signupReq:SignupReq? = nil
    
    var userPhone = ""
    
    var studentName = ""
    
    var fromSeven = false
    var selectedSchoolIndex = 0
    var selectedUnit1Index = 0
    
    //무료기능 선택 중단원
    var selectedUnitInfo : UnitInfo?
    var cameFromOnboard = false
    
    var parentCallback : (_ result : String) -> Void = { _ in}   // nil 취소. Y, N
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.tfUserName.setPadding(left: 12, right: 12)
        self.tfPhoneNumber.setPadding(left: 12, right: 12)
        
        
        self.tfUserName.addDoneButtonOnKeyboard()
        self.tfPhoneNumber.addDoneButtonOnKeyboard()
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SignUpStep1ParentViewController.backgroundTap))
        self.view.addGestureRecognizer(tapGestureRecognizer)
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpStep1ParentViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpStep1ParentViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.tfUserName.delegate = self
        self.tfPhoneNumber.delegate = self
        
        self.btJoinAgree.isEnabled = false
        
        lbAgreeName.text = "\(studentName) 학생의 회원가입에 동의합니다."
    }
    
    
    // MARK: - Input
    @objc func keyboardWillShow(notification: NSNotification) {
        
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            
            // if keyboard size is not available for some reason, dont do anything
            return
        }
        
        var shouldMoveViewUp = false
        
        // if active text field is not nil
        if let activeTextField = activeTextField {
            
            let bottomOfTextField = activeTextField.convert(activeTextField.bounds, to: self.view).maxY;
            let topOfKeyboard = self.view.frame.height - keyboardSize.height
            
            if bottomOfTextField > topOfKeyboard {
                shouldMoveViewUp = true
            }
        }
        
        if(shouldMoveViewUp) {
            self.view.frame.origin.y = 0 - keyboardSize.height
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    @objc func backgroundTap(_ sender: UITapGestureRecognizer) {
        // go through all of the textfield inside the view, and end editing thus resigning first responder
        // ie. it will trigger a keyboardWillHide notification
        self.view.endEditing(true)
    }
    
    
    @IBAction func nameChange(_ sender: UITextField) {
        if sender == tfUserName {
            if  let nameCount = tfUserName.text?.count, let phoneCount = tfPhoneNumber.text?.count {
                
                if nameCount > 0 && phoneCount > 0 {
                    isCheckInput = true
                } else {
                    isCheckInput = false
                    
                }
                
            }
            agreeCheck()
        }
        
    }
    
    
    @IBAction func phoneChange(_ sender: UITextField) {
        
        if sender == tfPhoneNumber {
            if  let nameCount = tfUserName.text?.count, let phoneCount = tfPhoneNumber.text?.count {
                
                if nameCount > 0 && phoneCount > 0 {
                    isCheckInput = true
                } else {
                    isCheckInput = false
                }
            }
            agreeCheck()
        }
        
    }
    
    
    
    @IBAction func checkParent(_ sender: Any) {
        
        isCheckParent = !isCheckParent
        
        if isCheckParent {
            
            btCheckParent.tintColor = UIColor.purpleishBlue
            
        } else {
            
            btCheckParent.tintColor = UIColor.lightBlueGrey
            
        }
        
        agreeCheck()
    }
    
    
    @IBAction func btBack(_ sender: Any) {
        //        self.navigationController?.popViewController(animated: true)
        alert(title: "그만두시겠어요?😭", message: "지금 돌아가면 입력했던 내용이\n모두 사라져요. 그래도 돌아가시겠어요?", button: "계속 가입하기", subButton: "그래도 돌아가기").done { b in
            if b == false {
                self.goSplash()
            }
        }
    }
    
    // MARK: - Validate
    func agreeCheck(){
        if isCheckParent && isCheckInput {
            btJoinAgree.setTitleColor(UIColor.white, for: .normal)
            btJoinAgree.backgroundColor = UIColor.puljaBlue
            btJoinAgree.isEnabled = true
            btJoinAgree.setTitle("보호자 전화번호로 인증하기", for: .normal)
        } else {
            btJoinAgree.setTitleColor(UIColor.puljaBlack, for: .normal)
            btJoinAgree.backgroundColor = UIColor.Foggy
            btJoinAgree.isEnabled = false
            btJoinAgree.setTitle("회원가입에 동의해주세요", for: .normal)
        }
    }
    
    
    func isPhone(candidate: String) -> Bool {
        let regex = "^01([0-9])([0-9]{3,4})([0-9]{4})$"
        return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: candidate)
    }
    
    @IBAction func agreeNext(_ sender: Any) {
        
        guard let phone = tfPhoneNumber.text  else {
            alert(title: "전화번호를 확인해주세요.", message: "전화번호를 올바른 형식으로\n입력했는지 확인해주세요.", button: "다시 시도하기")
            return
        }
        
        userPhone = phone
        
        if !isPhone(candidate: userPhone) {
            alert(title: "전화번호를 확인해주세요.", message: "전화번호를 올바른 형식으로\n입력했는지 확인해주세요.", button: "다시 시도하기")
            return
        }
        
        self.reqAuthCode(retryNoti: false)
        
        
        
    }
    
    
    func reqAuthCode(retryNoti: Bool){
        guard let userName = tfUserName.text else {
            return
        }
        
        let authCode = AuthCodeReq(cert: nil, email: nil, name: userName, phone: userPhone, service: "join", type: "P")
        
        SignAPI.shared.authCodeReq(authCode: authCode).done { res in
            if res.success ?? false {
                self.openAuthCode(isNew:true, timerNum:0, retryNoti: retryNoti)
            }
        }.catch { err in
            self.alert(title: nil, message: err.localizedDescription, button: "다시 시도하기").done { b in
                if b {
                    self.reqAuthCode(retryNoti: retryNoti)
                }
            }
        }
    }
    
    func openAuthCode(isNew: Bool, timerNum: Int, retryNoti: Bool ){
        guard let userName = tfUserName.text else {
            return
        }
        let vc = R.storyboard.main.authCodeViewController()!
        
        vc.authCodeReq = AuthCodeReq(cert: nil, email: nil, name: userName, phone: userPhone, service: "join", type: "P")
        vc.isNew = isNew
        if timerNum > 0 {
            vc.timerNum = timerNum
        }
        
        
        vc.authCallback = { resultCode in
            print("callback \(resultCode)")
            //            self.memoBottomSheet(idx)
            
            //타이머 종료 후 처리...
            if resultCode == "expire" {
                
                self.alert(title: "인증번호가 만료됐어요.", message: "3분이 지나 인증번호가 만료됐어요.\n인증번호를 다시 한번 받아주세요.", button: "인증번호 다시 받기").done { b in
                    print("authCallback: \(b)")
                    if b {
                        self.reqAuthCode(retryNoti:true)
                        //                            self.authCodeNoti()
                    }
                }
                
            } else if resultCode == "success" {
                
                
                self.goNextStep()
                
            } else if resultCode == "retry" {
                
                self.alert(title: "인증번호를 다시 보냈어요.", message: "받은 인증번호는 3분이 지나면 만료되니,\n그 뒤에는 인증번호를 다시 받아주세요.", button: "인증번호 입력하기").done { b in
                    print("authCallback: \(b)")
                    if b {
                        self.reqAuthCode(retryNoti:true)
                    }
                }
                
                
                
            } else if let sec = Int(resultCode) {
                
                self.alert(title: "인증번호를 확인해주세요.", message: "입력하신 번호가 발송된 번호와 다르거나,\n받은 지 3분이 지나 만료된 번호예요.", button: "인증번호 다시 받기", subButton: "다시 시도하기").done { b in
                    if b {
                        self.reqAuthCode(retryNoti:true)
                        //                        self.authCodeNoti()
                    } else {
                        self.openAuthCode(isNew: false, timerNum: sec, retryNoti:false)
                    }
                }
            }
        }
        
        // MDC 바텀 시트로 설정
        let bottomSheet = MDCBottomSheetController(contentViewController: vc)
        bottomSheet.mdc_bottomSheetPresentationController?.preferredSheetHeight = 353
        bottomSheet.delegate = self
        
        // 보여주기
        self.present(bottomSheet, animated: true, completion: nil)
    }
    
    
    func authCodeNoti(){
        
        let snackbar = TTGSnackbar(message: "인증번호를 다시 전송했어요.", duration: .short)
        snackbar.animationType = .slideFromTopBackToTop
        snackbar.show()
    }
    
    func goNextStep() {
        
        var startIndex = userPhone.index(userPhone.startIndex, offsetBy: 0)// 사용자지정 시작인덱스
        var endIndex = userPhone.index(userPhone.startIndex, offsetBy: 3)// 사용자지정 끝인덱스
        let p1_str = userPhone[startIndex ..< endIndex]
        
        
        startIndex = userPhone.index(userPhone.startIndex, offsetBy: 3)// 사용자지정 시작인덱스
        endIndex = userPhone.index(userPhone.startIndex, offsetBy: 7)// 사용자지정 끝인덱스
        let p2_str = userPhone[startIndex ..< endIndex]
        
        
        startIndex = userPhone.index(userPhone.startIndex, offsetBy: 7)// 사용자지정 시작인덱스
        endIndex = userPhone.index(userPhone.startIndex, offsetBy: 11)// 사용자지정 끝인덱스
        let p3_str = userPhone[startIndex ..< endIndex]
        
        
        signupReq?.parentAccept = "Y"
        signupReq?.parent_phone1 = String(p1_str)
        signupReq?.parent_phone2 = String(p2_str)
        signupReq?.parent_phone3 = String(p3_str)
        
        parentCallback("Y")
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func goSplash(){
        let onBoardVC = R.storyboard.main.weakCat4OnboardViewController()!
        let navigationController = UINavigationController(rootViewController: onBoardVC)
        navigationController.isNavigationBarHidden = true
        
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDel.window?.rootViewController = navigationController
    }
    
    
}


// MARK: - extension
extension SignUpStep1ParentViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
        
        
        
        if textField == self.tfUserName {
            self.tfUserName.layer.borderWidth = 1.0
            self.tfUserName.layer.borderColor = UIColor.Purple.cgColor
            self.tfPhoneNumber.layer.borderWidth = 0.5
            self.tfPhoneNumber.layer.borderColor = UIColor.lightBlueGrey.cgColor
        } else if textField == self.tfPhoneNumber {
            self.tfUserName.layer.borderWidth = 0.5
            self.tfUserName.layer.borderColor = UIColor.lightBlueGrey.cgColor
            self.tfPhoneNumber.layer.borderWidth = 1.0
            self.tfPhoneNumber.layer.borderColor = UIColor.Purple.cgColor
        } else {
            self.tfUserName.layer.borderWidth = 0.5
            self.tfUserName.layer.borderColor = UIColor.lightBlueGrey.cgColor
            self.tfPhoneNumber.layer.borderWidth = 0.5
            self.tfPhoneNumber.layer.borderColor = UIColor.lightBlueGrey.cgColor
        }
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeTextField = nil
    }
}
