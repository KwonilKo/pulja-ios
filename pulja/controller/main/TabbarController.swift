//
//  TabbarViewController.swift
//  pulja
//
//  Created by 김병헌 on 2021/11/24.
//

import UIKit
import MaterialComponents.MaterialBottomNavigation

class TabbarController: UITabBarController , MDCBottomNavigationBarDelegate {
    
    let bottomNavBar = MDCBottomNavigationBar()
    
    var homeData:Home? = nil
    
    /* recordEmptyViewController */
    var selectedUnit = ""
    var selectedKeywordURL: String?
    var selectedKeyword: String?
    var selectedKeywordName: String?
    var selectedKeywordList: [KeywordData]?
    var unit2SeqList: [Int]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        initData()
        CommonUtil.tabbarController = self
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden( true, animated: animated )
    }
    
    //Initialize Bottom Bar
    init()
    {
        super.init(nibName: nil, bundle: nil)
        bottomNavigationInit()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        bottomNavigationInit()
    }
    
    func bottomNavigationInit()
    {
        guard let arrControllers = self.viewControllers , arrControllers.count == 4 else { return }
        
        view.backgroundColor = .white
        view.addSubview(bottomNavBar)
        
        // Always show bottom navigation bar item titles.
        bottomNavBar.titleVisibility = .always
        
        // Cluster and center the bottom navigation bar items.
        bottomNavBar.alignment = .centered
        
        let preparationVC = arrControllers[0]
        let reviewVC = arrControllers[1]
        let recordVC = arrControllers[2]
        let settingVC = arrControllers[3]
        
        let tabBarItem1 = UITabBarItem(title: "홈", image: UIImage(named: "iconOutlineHome"), tag: 0)
        let tabBarItem2 = UITabBarItem(title: "복습", image: UIImage(named: "iconOutlineBookOpen"), tag: 1)
        let tabBarItem3 = UITabBarItem(title: "내 기록", image: UIImage(named: "iconOutlineRecord"), tag: 2)
        let tabBarItem4 = UITabBarItem(title: "설정", image: UIImage(named: "iconOutlineCog"), tag: 3)
        
        tabBarItem1.selectedImage = UIImage(named: "iconSolidHome")
        tabBarItem2.selectedImage = UIImage(named: "iconSolidBookOpen")
        tabBarItem3.selectedImage = UIImage(named: "iconSolidRecord")
        tabBarItem4.selectedImage = UIImage(named: "iconSolidCog")
        
        tabBarItem1.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 6);
        tabBarItem2.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 6);
        tabBarItem3.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 6);
        tabBarItem4.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 6);
        
        preparationVC.tabBarItem = tabBarItem1
        reviewVC.tabBarItem = tabBarItem2
        recordVC.tabBarItem = tabBarItem3
        settingVC.tabBarItem = tabBarItem4
        
        bottomNavBar.items = [tabBarItem1, tabBarItem2, tabBarItem3, tabBarItem4]
        
        bottomNavBar.unselectedItemTintColor = UIColor.lightBlueGrey
        bottomNavBar.barTintColor = .white
        bottomNavBar.itemTitleFont = UIFont.boldSystemFont(ofSize: 12.0)
        
        bottomNavBar.selectedItem = tabBarItem1
        bottomNavBar.delegate = self
        
        bottomNavBar.elevation = ShadowElevation(CGFloat(2))
        
    }
    
    
    func bottomNavigationBar(_ bottomNavigationBar: MDCBottomNavigationBar, didSelect item: UITabBarItem)
    {
        print("did select item \(item.tag)")
        
        self.selectedIndex = item.tag
        self.selectedViewController = self.viewControllers![item.tag]
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        layoutBottomNavBar()
    }
    
#if swift(>=3.2)
    @available(iOS 11, *)
    override func viewSafeAreaInsetsDidChange()
    {
        super.viewSafeAreaInsetsDidChange()
        
        
        layoutBottomNavBar()
    }
#endif
    
    // Setting Bottom Bar
    func layoutBottomNavBar()
    {
        let size = bottomNavBar.sizeThatFits(view.bounds.size)
        var bottomNavBarFrame = CGRect(
            x: 0,
            y: view.bounds.height - size.height,
            width: size.width,
            height: size.height)
        bottomNavBarFrame.size.height += view.safeAreaInsets.bottom
        bottomNavBarFrame.origin.y -= view.safeAreaInsets.bottom
        bottomNavBar.frame = bottomNavBarFrame
    }
    
    
    func initData() {
        
    }
}
