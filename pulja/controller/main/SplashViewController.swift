//
//  SplashViewController.swift
//  pulja
//
//  Created by 김병헌 on 2022/02/17.
//

import UIKit
import PromiseKit
import AirBridge

class SplashViewController: PuljaBaseViewController {
    
    var isUpdateCheck = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector:#selector(updateCheckForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        updateCheck()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func updateCheckForeground(){
        updateCheck()
    }
    
    func updateCheck(){
        
        CommonAPI.shared.version().done { res in
            var isForceUpdate = false
            
            if let ver = res.data {
                if let appVer = CheckUpdate.shared.getBundle(key: "CFBundleShortVersionString"), let minimum = ver.minimum {
                    
                    let versionCompare = appVer.compare(minimum, options: .numeric)
                    if versionCompare == .orderedSame {
                        print("same version")
                    } else if versionCompare == .orderedAscending {
                        // will execute the code here
                        print("ask user to update")
                        isForceUpdate = true
                    } else if versionCompare == .orderedDescending {
                        // execute if current > appStore
                        print("don't expect happen...")
                    }
                    
                }
                if isForceUpdate == true {
                    
                    let msg = ver.description
                    //이모지.
                    let transform = "Any-Hex/Java"
                    let output = msg!.mutableCopy() as! NSMutableString
                    CFStringTransform(output, nil, transform as NSString, true)
                    
                    //줄바꿈.
                    let message = output.replacingOccurrences(of: "\\n", with: "\n")
                    
                    self.alert(title: "업데이트 안내", message: message, button: "업데이트").done { b in
                        
                        // 문제풀이 userdefaults 초기화
                        UserDefaults.standard.removeObject(forKey: "hasSolve")
                        UserDefaults.standard.removeObject(forKey: "hasProblemSeq")
                        
                        self.openAppStore()
                        self.isUpdateCheck = true
                    }.catch { err in
                        print(err)
                    }
                } else {
                    self.runInit()
                }
            }
        }.catch { err in
            print(err)
        }
    }
    
    
    
    func openAppStore() {
        
        guard let url = URL(string: Const.MARKET_URL) else {
            return
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    
    func runInit(){
        
        let signinPromise :Promise<String> = checkSignIn()
        let homeDataPromise : Promise<HomeRes> = getHomeData()
        let loadingPromise:Promise<String> = Promise<String> { value in
            //processing
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                value.fulfill("Y")
                print("\(Date()) [1]：\(value)")
            }
        }
        
        firstly {
            when(fulfilled: signinPromise, loadingPromise, homeDataPromise)
        }.done { (res1, res2, res3) in
            
            if res1 == "Y" {
                
                //PayType 추가.
                //p : paid user
                //f : free user
                //b : paid user before
                //c : coach
                
                var userSeq = 0
                
                if let userType = res3.data?.appHomeView?.userType
                {
                    let infos = try self.container.viewContext.fetch(MyInfo.fetchRequest())
                    if infos.count > 0
                    {
                        let myInfo = infos[0]
                        myInfo.setValue(userType, forKey: "payType")
                        userSeq = Int(myInfo.userSeq ?? 0)
                        try self.container.viewContext.save()
                    }
                    
                }

                
                UserAPI.shared.myInfo().done { res in
                    
                    guard var user = res.data else {
                        self.goOnboard()
                        return
                    }
                    
                    if let schoolNum = user.schoolNum, schoolNum > 0 {
                        self.goMain(data:res3.data ?? nil)
                    } else {
                        let vc = R.storyboard.main.snsJoinStep1ViewController()!
                        vc.isJoin = false
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }.catch { e in
                    self.goOnboard()
                }
                
            } else {
                self.goOnboard()
            }
        }.catch { err in
            self.goOnboard()
        }
    }
    
    
    func getHomeData() -> Promise<HomeRes> {
        return  HomeAPI.shared.home()
    }
    
    
    func checkSignIn() -> Promise<String> {
        
        return Promise<String>() { seal in
            
            if(CommonUtil.shared.getAccessToken() != nil){
                
                CommonUtil.shared.setHeader()
                
                ChatAPI.shared.chatUsers().done { chatUserRes in
                    
//                    if let cus = chatUserRes.data {
//                        var chat_ids:[String]? = []
//                        for chatUser in cus {
//                            chat_ids?.append(chatUser.hashUserSeq!)
//                        }
//                        let jsonData = try? JSONEncoder().encode(cus)
//                        let jsonString = String.init(data: jsonData! , encoding: .utf8)
//                        let userDefault = UserDefaults.standard
//                        userDefault.setValue(chat_ids, forKey: Const.UD_CHAT_USER_IDS)
//                        userDefault.setValue(jsonString, forKey: Const.UD_CHAT_USER_JSON)
//                        userDefault.synchronize()
//                    } else {
//                        let userDefault = UserDefaults.standard
//                        userDefault.removeObject(forKey: Const.UD_CHAT_USER_IDS)
//                        userDefault.removeObject(forKey: Const.UD_CHAT_USER_JSON)
//                        userDefault.synchronize()
//                    }
                    if self.myInfo != nil, let id = self.myInfo?.hashUserSeq, let nick = self.myInfo?.username,  let userType = self.myInfo?.userType  {
                        
                        if let userId = self.myInfo?.userId, let userSeq = self.myInfo?.userSeq {
                            AirBridge.state()?.setUserID(userId)
                            AirBridge.state()?.setUserEmail(userId)
                            AirBridge.state()?.setUserAlias(["userseq": String(userSeq)])
                        }
                        
                        seal.fulfill("Y")
                    } else {
                        seal.fulfill("N")
                    }
                }.catch { err in
                    seal.fulfill("N")
                }
            } else {
                seal.fulfill("N")
            }
            
        }
        
    }
    
    func goOnboard(){
        let onBoardVC = R.storyboard.main.weakCat4OnboardViewController()!
        let navigationController = UINavigationController(rootViewController: onBoardVC)
        navigationController.isNavigationBarHidden = true
        
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDel.window?.rootViewController = navigationController
        
        // A mask of options indicating how you want to perform the animations.
        let options: UIView.AnimationOptions = .transitionCrossDissolve
        
        // The duration of the transition animation, measured in seconds.
        let duration: TimeInterval = 0.3
        
        // Creates a transition animation.
        // Though `animations` is optional, the documentation tells us that it must not be nil. ¯\_(ツ)_/¯
        UIView.transition(with: appDel.window!, duration: duration, options: options, animations: {}, completion:
                            { completed in
            // maybe do something on completion here
        })
    }
    
    func goMain(data:Home?){
        let userDefaultKey1 = "hasSolve"
        let userDefaultKey2 = "hasProblemSeq"
        
        if self.myInfo != nil, let userType = self.myInfo?.userType, let userSeq = self.myInfo?.userSeq {
            if UserDefaults.standard.bool(forKey: userDefaultKey1) || UserDefaults.standard.bool(forKey: userDefaultKey2) {
                if userType == "C" {
                    goCoach(data: data)
                } else {
                    goStudunt(data:data)
                }
            } else {
                CurationAPI.shared.studyInfo(userSeq: Int(userSeq)).done { studyInfoRes in
                    if let hasSolve = studyInfoRes.data?.hasSolve {
                        //print("splash hahSolve -> \(hasSolve)")
                        UserDefaults.standard.set(hasSolve, forKey: userDefaultKey1)
                        UserDefaults.standard.synchronize()
                    }
                    if let hasProblemSeq = studyInfoRes.data?.hasProblemSeq {
                        //print("splash hasProblemSeq -> \(hasProblemSeq)")
                        UserDefaults.standard.set(hasProblemSeq, forKey: userDefaultKey2)
                        UserDefaults.standard.synchronize()
                    }
                }.catch { err in
                    print(err)
                }.finally {
                    if UserDefaults.standard.bool(forKey: userDefaultKey2) {
                        if userType == "C" {
                            self.goCoach(data: data)
                        } else {
                            self.goStudunt(data:data)
                        }
                    } else {
                        let vc = R.storyboard.preparation.keywordViewController()!
                        vc.cameFromOnboard = true
                        vc.isModal = true
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }
    }
    
    func goCoach(data:Home?) {
        let vcDetail = R.storyboard.main.coachTabbarController()
        
        if let vc = vcDetail?.viewControllers![0] as? HomeViewController {
            vc.homeInfo = data
        }
        
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDel.window?.rootViewController = vcDetail
        
        // A mask of options indicating how you want to perform the animations.
        let options: UIView.AnimationOptions = .transitionCrossDissolve
        
        // The duration of the transition animation, measured in seconds.
        let duration: TimeInterval = 0.3
        
        // Creates a transition animation.
        // Though `animations` is optional, the documentation tells us that it must not be nil. ¯\_(ツ)_/¯
        UIView.transition(with: appDel.window!, duration: duration, options: options, animations: {}, completion:
                            { completed in
            // maybe do something on completion here
        })
    }
    
    func goStudunt(data:Home?){
        let vcDetail = R.storyboard.main.tabbarController()
        
        if let vc = vcDetail?.viewControllers![0] as? HomeViewController {
            vc.homeInfo = data
        }
        
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDel.window?.rootViewController = vcDetail
        
        // A mask of options indicating how you want to perform the animations.
        let options: UIView.AnimationOptions = .transitionCrossDissolve
        
        // The duration of the transition animation, measured in seconds.
        let duration: TimeInterval = 0.4
        
        // Creates a transition animation.
        // Though `animations` is optional, the documentation tells us that it must not be nil. ¯\_(ツ)_/¯
        UIView.transition(with: appDel.window!, duration: duration, options: options, animations: {}, completion:
                            { completed in
            // maybe do something on completion here
        })
    }
}
