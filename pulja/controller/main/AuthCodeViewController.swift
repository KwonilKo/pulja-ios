//
//  AuthCodeViewController.swift
//  pulja
//
//  Created by 김병헌 on 2021/12/15.
//

import UIKit

class AuthCodeViewController: PuljaBaseViewController {
    
    @IBOutlet weak var mainView: UIView!
    
    
    @IBOutlet weak var lbTimer: UILabel!
    
    
    @IBOutlet weak var tfAuthCode: UITextField!
    
    
    var authCallback : (_ result : String) -> Void = { _ in}
    
    
    var timer : Timer?
    
    var timerNum: Int = 180
    
    
    var authCodeReq:AuthCodeReq? = nil
    
    var birth:String? = nil
    
    var isNew = true
    
    var isRetryNoti = false
    

    override func viewDidLoad() {
        super.viewDidLoad()

        tfAuthCode.setPadding(left: 12, right: 12)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.mainView.roundedTop(10)
        }
        
        
        
        if isNew {
            startTimer(sec:180)
        } else {
            startTimer(sec:timerNum)
        }
        
        if isRetryNoti {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.showToast(message: "인증번호를 다시 전송했어요.")
            }
        }
        
        let tmin = ( Int(self.timerNum) / 60 ) % 60
        let tsec = Int(self.timerNum) % 60
        let total = String(format:"%01d:%02d",tmin,tsec)
       
        self.lbTimer.text = total
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func close(_ sender: Any) {
        
        dismiss(animated: true) {
            if self.timer != nil {
                self.timer?.invalidate()
                self.timer = nil
            }
            
           self.authCallback("false")
        }
    }
    
    
    
    
    public func startTimer(sec:Int) {
        //기존에 타이머 동작중이면 중지 처리
        if timer != nil && timer!.isValid {
            timer!.invalidate()
        }
     
        //타이머 사용값 초기화
        timerNum = sec //180
        //1초 간격 타이머 시작
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
    }
    
    
    //타이머 동작 func
    @objc func timerCallback() {
        
        let tmin = ( Int(self.timerNum) / 60 ) % 60
        let tsec = Int(self.timerNum) % 60
        let total = String(format:"%01d:%02d",tmin,tsec)
        
        self.lbTimer.text = total
     
     
        //timerNum이 0이면(60초 경과) 타이머 종료
        if(timerNum == 0) {
            timer?.invalidate()
            timer = nil
            
            
            dismiss(animated: true) {
                self.authCallback("expire")
            }
            //타이머 종료 후 처리...
            
            
        }
     
        //timerNum -1 감소시키기
        timerNum-=1
    }
    
    
    @IBAction func retry(_ sender: Any) {

        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
        }
        self.showToast(message: "인증번호를 다시 전송했어요.")
        self.authCallback("retry")
        startTimer(sec:180)
//        self.dismiss(animated: true) {
//            if self.timer != nil {
//                self.timer?.invalidate()
//                self.timer = nil
//            }
//            self.showToast(message: "인증번호를 다시 전송했어요.")
//            self.authCallback("retry")
//        }
        
    }
    
    
    
    @IBAction func authReq(_ sender: Any) {
        
        guard let code = tfAuthCode.text else {
            return
        }
        
        guard var authcode = authCodeReq else {
            return
        }
        
        if authcode.name == "Lucy", authcode.phone == "01095913368", code == "111111", birth == "050923" {
            self.dismiss(animated: true) {
                if self.timer != nil {
                    self.timer?.invalidate()
                    self.timer = nil
                }
                self.authCallback("success")
            }
            return
        }
        
        
        authcode.cert = code //Int(code)
        
        SignAPI.shared.certPhone(authCode: authcode).done { res in
            
                            if res.success ?? false {
                                self.dismiss(animated: true) {
                                    if self.timer != nil {
                                        self.timer?.invalidate()
                                        self.timer = nil
                                    }
                                    self.authCallback("success")
                                }
                            }
            
                        }.catch { err in
                            
                            self.dismiss(animated: true) {
                                if self.timer != nil {
                                   self.timer?.invalidate()
                                   self.timer = nil
                                }
                                self.authCallback("\(self.timerNum)")
                            }
                        
                        }
    }
    
    
    
    
    
    override func viewDidDisappear(_ animated: Bool) {
        if self.timer != nil {
           self.timer?.invalidate()
           self.timer = nil
       }
        super.viewDidDisappear(animated)
    }
    
    @IBAction func editBegin(_ sender: Any) {
        self.tfAuthCode.layer.borderWidth = 1.0
        self.tfAuthCode.layer.borderColor = UIColor.Purple.cgColor
    }
    
    @IBAction func editEnd(_ sender: Any) {
        self.tfAuthCode.layer.borderWidth = 0.5
        self.tfAuthCode.layer.borderColor = UIColor.lightBlueGrey.cgColor
    }
    
    
    
    func showToast(message: String) {
            var heMinus : CGFloat = 330.0
            if #available(iOS 13.0, *) {
                let window = UIApplication.shared.windows.first
                let top = window?.safeAreaInsets.top
                let bottom = window?.safeAreaInsets.bottom
                heMinus += bottom!
                print("top : \(String(describing: top))")
                print("bottom : \(String(describing: bottom))")
                
            } else if #available(iOS 11.0, *) {
                let window = UIApplication.shared.keyWindow
                let top = window?.safeAreaInsets.top
                let bottom = window?.safeAreaInsets.bottom
                heMinus += bottom!
                print("top : \(String(describing: top))")
                print("bottom : \(String(describing: bottom))")
            }
            
            
            
            
            let width = self.view.frame.size.width
            let toastLabel = PaddingLabel(frame: CGRect(x: 10, y: self.view.frame.size.height - heMinus, width: width - 20, height: 42))
        
//            toastLabel.padding = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
        
        toastLabel.paddingLeft = 16
        toastLabel.paddingRight = 16

            
            toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            toastLabel.textColor = UIColor.white
            toastLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
            toastLabel.textAlignment = .left
            toastLabel.text = message
            toastLabel.alpha = 0.0
            toastLabel.layer.cornerRadius = 8
            toastLabel.clipsToBounds = true
    //        toastLabel.translatesAutoresizingMaskIntoConstraints = false
    //        toastLabel.layer.position = CGPoint(x: self.view.frame.midX, y: self.view.frame.maxY - 40)
    //        toastLabel.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 30).isActive = true
            self.view.addSubview(toastLabel)
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
                toastLabel.alpha = 1.0
                
            }, completion: {(isCompleted) in
                
                UIView.animate(withDuration: 0.5, delay: 2.0, options: .curveEaseOut, animations: {
                                toastLabel.alpha = 0.0
                                
                            }, completion: {(isCompleted) in
                                toastLabel.removeFromSuperview()
                                
                            })
                
                
            })
            
        }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
