//
//  JoinPolicyViewController.swift
//  pulja
//
//  Created by 김병헌 on 2022/08/31.
//

import UIKit
import M13Checkbox

class JoinPolicyViewController: UIViewController {
    
    
    
    @IBOutlet weak var btCheckAll: M13Checkbox!
    
    @IBOutlet weak var btParent: UIButton!
    @IBOutlet weak var btPolicyCheck: UIButton!
    @IBOutlet weak var btPrivacyCheck: UIButton!
    @IBOutlet weak var btMaketingCheck: UIButton!

    
    @IBOutlet weak var btAgree: UIButton!
    
    var isCheckAll = false
    var isPolicy = false
    var isPrivacy = false
    var isMaketting = false
    var isParent = false
    
    var policyCallback : (_ result : String?) -> Void = { _ in}   // nil 취소. Y, N
    
    override func viewDidLoad() {
        super.viewDidLoad()


      
        btCheckAll.tintColor = UIColor.Foggy
      btPolicyCheck.tintColor = UIColor.Foggy
      btPrivacyCheck.tintColor = UIColor.Foggy
      btMaketingCheck.tintColor = UIColor.Foggy
      btParent.tintColor = UIColor.Foggy

        btCheckAll?.stateChangeAnimation = .bounce(.fill)
        btCheckAll?.tintColor = UIColor.OffPurple
        btCheckAll?.secondaryTintColor = UIColor.Foggy40
        btCheckAll?.checkmarkLineWidth = CGFloat(4)
        btCheckAll?.boxLineWidth = CGFloat(2)
        btCheckAll?.boxType = .square
        
    }
    
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func checkAll(_ sender: Any) {
            isCheckAll = !isCheckAll
            if isCheckAll {
                
                isPolicy = true
                isPrivacy = true
                isMaketting = true
                isParent = true

                
                btCheckAll.tintColor = UIColor.OffPurple
                btPolicyCheck.tintColor = UIColor.OffPurple
                btPrivacyCheck.tintColor = UIColor.OffPurple
                btMaketingCheck.tintColor = UIColor.OffPurple
                btParent.tintColor = UIColor.OffPurple
                
            } else {
                
                isPolicy = false
                isPrivacy = false
                isMaketting = false
                isParent = false
                
                btCheckAll.tintColor = UIColor.Foggy40
                btPolicyCheck.tintColor = UIColor.Foggy
                btPrivacyCheck.tintColor = UIColor.Foggy
                btMaketingCheck.tintColor = UIColor.Foggy
                btParent.tintColor = UIColor.Foggy
                
            }
            
            agreeCheck()
        }
    
    
    
    @IBAction func checkParent(_ sender: Any) {
        
        isParent = !isParent
                    
        if isParent {
            btParent.tintColor = UIColor.OffPurple
        } else {
            btParent.tintColor = UIColor.Foggy
        }
        allCheckState()
    }
    
        
        @IBAction func checkPolicy(_ sender: Any) {
            
            isPolicy = !isPolicy
            
            if isPolicy {
                btPolicyCheck.tintColor = UIColor.OffPurple
            } else {
                btPolicyCheck.tintColor = UIColor.Foggy
            }
            
            agreeCheck()
            allCheckState()
        }
        
        @IBAction func checkPrivacy(_ sender: Any) {
            
            isPrivacy = !isPrivacy
                   
            if isPrivacy {
                btPrivacyCheck.tintColor = UIColor.OffPurple
            } else {
                btPrivacyCheck.tintColor = UIColor.Foggy
            }
            agreeCheck()
            allCheckState()
        }
        
        @IBAction func checkMarket(_ sender: Any) {
            
            isMaketting = !isMaketting
            
            if isMaketting {
                btMaketingCheck.tintColor = UIColor.OffPurple
            } else {
               btMaketingCheck.tintColor = UIColor.Foggy
            }
            allCheckState()
            
        }
    
    func allCheckState(){
        
        
        
        if isMaketting && isPrivacy && isPolicy && isParent {
            isCheckAll = true
            btCheckAll.tintColor = UIColor.OffPurple
            btCheckAll.checkState = .checked
        } else {
            isCheckAll = false
            btCheckAll.tintColor = UIColor.Foggy40
            btCheckAll.checkState = .unchecked
        }
    }
    
    func agreeCheck(){
            if isPolicy && isPrivacy {
                btAgree.setTitleColor(UIColor.white, for: .normal)
                btAgree.backgroundColor = UIColor.OffPurple
    //            btAuthReq.isEnabled = true
            } else {
                btAgree.setTitleColor(UIColor.Foggy, for: .normal)
                btAgree.backgroundColor = UIColor.Foggy20
    //            btAuthReq.isEnabled = false
            }
        }
    
    
    @IBAction func goPolicy(_ sender: Any) {
            
        goWebView(url: "https://slow-process-b56.notion.site/In-Progress-9af3811274b44464af87d5ff1d40db80", title: "서비스 이용약관")
        
    }
    
    
    @IBAction func goPrivacy(_ sender: Any) {
        goWebView(url: "https://slow-process-b56.notion.site/In-Progress-cc2ac9057d2e4c5e92a51ae87cf00dd9", title: "개인정보처리방침")
    }
    
    
    @IBAction func goMaketting(_ sender: Any) {
        
        goWebView(url: "https://slow-process-b56.notion.site/In-Progress-936a7f99464b4b219ef959c748c57875", title: "마케팅 활용 및 광고성 정보 수신 동의")

    }
    
    func goWebView(url:String, title:String){
        
        let vc = R.storyboard.main.idpwdViewController()!
        vc.viewTitle = title
        vc.url = url
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true) {
        }
    }
    
    
    
    
    @IBAction func agreeAction(_ sender: Any) {
        
        if isPolicy && isPrivacy {
            
            let makettingYN =  isMaketting ? "Y" : "N"
            
            if isParent { //14세 이상
                policyCallback(makettingYN)
                
                self.navigationController?.popViewController(animated: true)
                
            } else { // 14세 이하 부모님 동의 화면.
                goValidParent()
            }
        }
    }
    
    
    
    
    func goValidParent(){
        let vc = R.storyboard.main.signUpStep1ParentViewController()!
//                  vc.signupReq = signupReq
//                  vc.studentName = userName
//                  vc.fromSeven = self.fromSeven
//                  vc.selectedSchoolIndex = self.selectedSchoolIndex
//                  vc.selectedUnit1Index = self.selectedUnit1Index
                  
                  //무료기능 선택한 중단원 그 다음 컨트롤러 전달
//                  vc.selectedUnitInfo = self.selectedUnitInfo
//                  vc.cameFromOnboard = self.cameFromOnboard
                  
                  
        vc.modalTransitionStyle = .flipHorizontal
        vc.parentCallback = { resultCode in
            
            if resultCode == "Y" {
                let makettingYN =  self.isMaketting ? "Y" : "N"
                self.policyCallback(makettingYN)
                self.navigationController?.popViewController(animated: true)
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    

}
