//
//  FinalAlertViewController.swift
//  pulja
//
//  Created by kwonilko on 2023/09/20.
//

import UIKit
import WebKit
import PromiseKit
import AirBridge
import M13Checkbox

class FinalAlertViewController: UIViewController, WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler {
    
    
    
    
    @IBOutlet var checkBox: M13Checkbox!
    
    
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let webViewController = sb.instantiateViewController(withIdentifier: "UpdateWebviewController") as! UpdateWebviewController
        // let webViewController = UpdateWebviewController()
        webViewController.modalPresentationStyle = .fullScreen
        
//        present(webViewController, animated: true, completion: nil)
//        self.dismiss(animated: false, completion: nil)
        
        
        self.present(webViewController, animated: false, completion: {
            self.dismiss(animated: false)
        })
//        self.dismiss(animated: false,
//                     completion: {
//            self.present(webViewController, animated: false, completion: nil)
//        })
    }
    
    var isCheckAll = false
    
    
    @IBOutlet var upperView: UIView!
    
    @IBOutlet var bottomView: UIView!
    
    
    @IBOutlet weak var webFrameView: UIView!
    
    @IBOutlet var outsideView: UIView!
    
    @IBOutlet weak var webView: WKWebView!
    
    //"https://bridge.edupanion.kr/pullza/request-data-transfer"
    let url_str = "https://bridge.edupanion.kr/pullza/request-data-transfer"
    
    
    var callback : (_ isSend : Bool) -> Void = { _ in}
    
    
    
    
    
    func loadWebViewContent() {
            let html = """
            <!DOCTYPE html>
            <html>
            <head>
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <!-- Other meta tags and head content -->
                <style>
                    /* Add your CSS styles here */
                </style>
            </head>
            <body>
                <!-- Your web content here -->
            </body>
            </html>
            """

            webView.loadHTMLString(html, baseURL: nil)
        }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkBox.boxType = .square
        checkBox.stateChangeAnimation = .bounce(.fill)
        checkBox.tintColor = UIColor(red: 198.0, green:201.0, blue: 211.0, alpha: 1.0)
        
        upperView.clipsToBounds = true
        upperView.layer.cornerRadius = 16.0
        upperView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        bottomView.clipsToBounds = true
        bottomView.layer.cornerRadius = 16.0
        bottomView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
//        self.webFrameView.layer.cornerRadius = 16.0
//        self.webFrameView.clipsToBounds = true
        
//        self.webView.scrollView.isScrollEnabled = false
//        self.webView.frame.size.height = 300.0
//        self.webView.clipsToBounds = true
        

        // Do any additional setup after loading the view.
        let contentController = WKUserContentController()

        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true


        let config = WKWebViewConfiguration()
        config.preferences = preferences
        config.userContentController = contentController
        
        // let contentController = webView.configuration.userContentController
        contentController.add(self, name: "buttonClick")
        
        self.webView = WKWebView(frame: .zero, configuration: config)
        self.webView?.frame = self.webFrameView.bounds
        self.webView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.webFrameView.addSubview(self.webView!)
        
        loadWebViewContent()
        
        
        let mytapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        self.outsideView.addGestureRecognizer(mytapGestureRecognizer)
        self.outsideView.isUserInteractionEnabled = true
        
        loadWebPage(url_str)
        
        webView?.navigationDelegate = self
        webView.uiDelegate = self
        
        
        
    }
    
    
    @IBAction func btnCheck(_ sender: Any) {
        
        isCheckAll = !isCheckAll
        
        if isCheckAll {
            checkBox.tintColor = UIColor(red: 255.0/256.0, green: 213.0/256.0, blue: 76.0/256.0, alpha: 1.0)
            checkBox.borderColor = UIColor(red: 255.0/256.0, green: 213.0/256.0, blue: 76.0/256.0, alpha: 1.0)
            
        } else {
            checkBox.tintColor = UIColor.white
            checkBox.borderColor = UIColor(red: 198.0/256.0, green: 201.0/256.0, blue: 211.0/256.0, alpha: 1.0)
        }
//        checkBox.setCheckState(.checked, animated: true)
        
    }
    
    
    @IBAction func exit(_ sender: Any) {
        
        //오늘 하루보지 않기 선택했으면 메모리에 저장
        if isCheckAll {
            let currentDate = Date()
            UserDefaults.standard.set(currentDate, forKey: "lastShownDate")
        }
        
        //안했으면 놔두기
        else {
            UserDefaults.standard.removeObject(forKey: "lastShownDate")
        }
        
        
        self.dismiss(animated: false)
    }
    
    private var authCookie: HTTPCookie? {
              let cookie = HTTPCookie(properties: [
               .domain: Const.COOKIE_DOMAIN,
                  .path: "/",
                  .name: "JWT-TOKEN",
                  .value: CommonUtil.shared.getAccessToken(),
              ])
              return cookie
          }
              
      private var refreshCookie: HTTPCookie? {
          let cookie = HTTPCookie(properties: [
              .domain: Const.COOKIE_DOMAIN,
              .path: "/",
              .name: "REFRESH-TOKEN",
              .value: CommonUtil.shared.getRefreshToken(),
          ])
          return cookie
      }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    private func loadWebPage(_ url: String) {
        guard let myUrl = URL(string: url) else {
            return
        }
        
        
        let urlRequest = URLRequest(url: myUrl)

        if let authCookie = authCookie, let refreshCookie = refreshCookie {
            self.webView?.configuration.websiteDataStore.httpCookieStore.setCookie(authCookie, completionHandler: {
                self.webView?.configuration.websiteDataStore.httpCookieStore.setCookie(refreshCookie, completionHandler: {
                    self.webView?.load(urlRequest)
                })
            })
        } else {
            self.webView?.load(urlRequest)
        }
                
        
//        let request = URLRequest(url: myUrl)
//        webView.load(request)
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer)
    {
             self.dismiss(animated: false, completion: nil)
    }
    
    
    @IBAction func testButtonAction(_ sender: Any) {
        
        self.dismiss(animated: false) {
                   self.callback(true)
        }
        
    }
    
    
    
}

