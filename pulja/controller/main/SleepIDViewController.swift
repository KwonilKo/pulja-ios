//
//  SleepIDViewController.swift
//  pulja
//
//  Created by 김병헌 on 2022/07/11.
//

import UIKit

class SleepIDViewController: UIViewController {
    
    var userName: String?
    var userId:String?
    var password:String?
    
    var restoreCallback : ((_ restore : Bool ) -> Void)?
    
    @IBOutlet weak var lbUserNameInfo: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        if let name = userName {
            lbUserNameInfo.text = "안녕하세요, \(name) 회원님."
        } else {
            lbUserNameInfo.text = "안녕하세요, 회원님."
        }
    }
    
    
    @IBAction func restoreUser(_ sender: Any) {
        if let id = userId, let pw = password {
            SignAPI.shared.restore(id: id, pw: pw).done { res in
                if let suc = res.success, suc == true {
                    self.restoreCallback?(true)
                    self.dismiss(animated: true)
                }
            }
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.restoreCallback?(false)
        self.dismiss(animated: true)
    }
    
}
