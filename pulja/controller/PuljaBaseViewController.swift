//
//  PuljaBaseViewController.swift
//  pulja
//
//  Created by 김병헌 on 2021/12/03.
//

import UIKit
import CoreData
import PromiseKit
import AirBridge

class PuljaBaseViewController: UIViewController {

    let delegateIdentifier = NSUUID().uuidString
    
    var container: NSPersistentContainer!
    
    var myInfo:MyInfo? = nil
    
    let chat_user_ids:[String]? = UserDefaults.standard.array(forKey: Const.UD_CHAT_USER_IDS) as? [String]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(" 🔸 \(className).viewDidLoad()")
        
        print(" 🔸 \(className): \(chat_user_ids ?? [])")
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.container = appDelegate.persistentContainer
        do {
            let myInfos = try self.container.viewContext.fetch(MyInfo.fetchRequest()) as! [MyInfo]
            if myInfos.count > 0
            {
                myInfo = myInfos[0]
                print("\(className) :: \(myInfo?.username ?? "username")")
            }
            
           
            
        } catch {
           print(error.localizedDescription)
        }
        
        serverCheck()
        
//        CheckUpdate.shared.showUpdate(withConfirmation: false)
    }
    
    deinit {
        print(" 🔹 \(className).deinit()")
    }
    
    
    func serverCheck(){
        
        //저장 된게 없으면 무시.
        if  let serverType = UserDefaults.standard.string(forKey: Const.UD_SERVER_TYPE) {
            if serverType != Const.serverType { // 서버 변경 시 로그아웃 처리.
                UserDefaults.standard.set(Const.serverType, forKey: Const.UD_SERVER_TYPE)
                UserDefaults.standard.synchronize()
                
                print("logout")
                CommonUtil.shared.logoutAction()
                
                CommonUtil.shared.showAlert(title: "알림", message: "로그아웃되었습니다.", button: "확인", viewController: UIViewController.currentViewController()!){
                    
                }
            }
        } else {
            UserDefaults.standard.set(Const.serverType, forKey: Const.UD_SERVER_TYPE)
            UserDefaults.standard.synchronize()
        }
        
    }
    
    
    
    func customFinalAlert() -> Promise<Bool> {
        
        return Promise { seal in
//            let btText:String = button ?? "확인"
//            let titleText:String = title ?? "알림"
//            let messageText:String = message ?? "오류가 발생하였습니다."
            
            
            let alert = R.storyboard.main.finalAlertViewController()!
            
            alert.callback = { check in
                seal.resolve(.fulfilled(check))
            }
            
//            alert.titleText = titleText
//            alert.messageText = messageText
//            alert.buttonText = btText
//            alert.isTwoButton = false
            alert.modalPresentationStyle = .overCurrentContext
            present(alert, animated: false, completion: nil)

        }
        
        
    }
    
    func customAlert() -> Promise<Bool> {
        return Promise { seal in
//            let btText:String = button ?? "확인"
//            let titleText:String = title ?? "알림"
//            let messageText:String = message ?? "오류가 발생하였습니다."
            
            
            let alert = R.storyboard.main.updateAlertViewController()!
            
            alert.callback = { check in
                seal.resolve(.fulfilled(check))
            }
            
//            alert.titleText = titleText
//            alert.messageText = messageText
//            alert.buttonText = btText
//            alert.isTwoButton = false
            alert.modalPresentationStyle = .overCurrentContext
            present(alert, animated: false, completion: nil)

        }
    }
    
    func checkAlertNew(userSeq: Int?, problemSeq: Int?, duration: Double?, questionId : Int?, userAnswer: String?, isFromCollection: Bool, delegate: PreparationMultipleChoiceViewController!) -> Promise <Bool> {
        return Promise { seal in
            
            let alert = R.storyboard.preparation.preparationCheckAnswerViewController()!
            alert.problemSeq = problemSeq
            alert.duration = duration
            alert.questionId = questionId
            alert.userAnswer = userAnswer
            alert.delegate = delegate
            alert.userSeq = userSeq
            alert.isFromCollection = isFromCollection
            alert.callback = { check in
                seal.resolve(.fulfilled(check))
            }
            
            alert.modalPresentationStyle = .overCurrentContext
            present(alert, animated: false, completion: nil)
        }
    }
    
    func checkAlert(imgName: String?, mainLbText: String?, subLbText :String? ) -> Promise <Bool> {
        return Promise { seal in
            let alert = R.storyboard.preparation.preparationCheckAnswerViewController()!
            alert.imgName = imgName
            alert.mainLbText = mainLbText
            alert.subLbText = subLbText
            
            alert.callback = { check in
                seal.resolve(.fulfilled(check))
            }
            
            alert.modalPresentationStyle = .overCurrentContext
            present(alert, animated: false, completion: nil)
        }
    }
    
    func alert(title:String?, message:String?, button:String?) -> Promise<Bool> {
        return Promise { seal in
            let btText:String = button ?? "확인"
            let titleText:String = title ?? "알림"
            let messageText:String = message ?? "오류가 발생하였습니다."
            let alert = R.storyboard.main.alertViewController()!
            
            alert.callback = { check in
                seal.resolve(.fulfilled(check))
            }
            
            alert.titleText = titleText
            alert.messageText = messageText
            alert.buttonText = btText
            alert.isTwoButton = false
            alert.modalPresentationStyle = .overCurrentContext
            present(alert, animated: false, completion: nil)
        }
    }
    
    func alert(title:String?, message:String?, button:String?, subButton:String?) -> Promise<Bool> {
        return Promise { seal in
            let btText:String = button ?? "확인"
            let subBtText:String = subButton ?? "취소"
            let titleText:String = title ?? "알림"
            let messageText:String = message ?? ""
            
            let alert = R.storyboard.main.alertViewController()!
            
            alert.callback = { check in
                seal.resolve(.fulfilled(check))
            }
            
            alert.titleText = titleText
            alert.messageText = messageText
            alert.buttonText = btText
            alert.subButtonText = subBtText
            alert.isTwoButton = true
            alert.modalPresentationStyle = .overCurrentContext
            present(alert, animated: false, completion: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func alertExit(title:String?, message:String?, button:String?, subButton:String?) -> Promise<Bool> {
        return Promise { seal in
            let btText:String = button ?? "확인"
            let subBtText:String = subButton ?? "취소"
            let titleText:String = title ?? "알림"
            let messageText:String = message ?? ""
            
            let alert = R.storyboard.main.alertViewController()!
            
            
            alert.callback = { check in
                seal.resolve(.fulfilled(check))
            }
            
            alert.titleText = titleText
            alert.messageText = messageText
            alert.buttonText = btText
            alert.subButtonText = subBtText
            alert.isTwoButton = true
            alert.isCancel = true
            alert.modalPresentationStyle = .overCurrentContext
            
            
            present(alert, animated: false, completion: nil)
        }
        
    }
}
