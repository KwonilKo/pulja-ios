//
//  AlertViewController.swift
//  pulja
//
//  Created by 김병헌 on 2021/12/08.
//

import UIKit

class AlertViewController: UIViewController {
    
    
    @IBOutlet weak var lbTitle: UILabel!
    
    
    @IBOutlet weak var lbMessage: UILabel!
    
    
    @IBOutlet weak var btConfirm: UIButton!
    
    @IBOutlet weak var btSubOrCancel: UIButton!
    
    @IBOutlet weak var alertView: UIView!
    
    var callback : (_ isSend : Bool) -> Void = { _ in}
    
    var messageText = ""
    var titleText = "알림"
    var buttonText = "확인"
    var subButtonText = "취소"
    
    var isTwoButton = false
    var isCancel = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lbTitle.text = titleText
        lbMessage.text = messageText
        btConfirm.setTitle(buttonText, for: .normal)
        btSubOrCancel.setTitle(subButtonText, for: .normal)
        
        if isTwoButton {
            btSubOrCancel.isHidden = false
        } else {
            btSubOrCancel.isHidden = true
        }
        
        if isCancel {
            btConfirm.backgroundColor = UIColor.red10
        }
    }
    
    
    
    @IBAction func confirm(_ sender: Any) {
        
        
        self.dismiss(animated: false) {
                   self.callback(true)
        }
    }
    
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: false) {
                   self.callback(false)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
