//
//  AlertHBtnViewController.swift
//  pulja
//
//  Created by 김병헌 on 2022/10/20.
//

import UIKit

class AlertHBtnViewController: UIViewController {

    
    @IBOutlet weak var lbTitle: UILabel!
    
    
    @IBOutlet weak var lbMessage: UILabel!
    
    
    @IBOutlet weak var btConfirm: UIButton!
    
    @IBOutlet weak var btSubOrCancel: UIButton!
    
    @IBOutlet weak var alertView: UIView!
    
    
    @IBOutlet weak var topSpace: NSLayoutConstraint!
    
    
    var callback : (_ isSend : Bool) -> Void = { _ in}
    
    var messageText = ""
    var titleText = "알림"
    var buttonText = "확인"
    var subButtonText = "취소"
    
    var isTwoButton = false
    var isCancel = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lbTitle.text = titleText
        
        if messageText != "" {
            lbMessage.isHidden = false
            topSpace.constant = 24
        } else {
            lbMessage.isHidden = true
            topSpace.constant = 36
        }
        
        lbMessage.text = messageText
        btConfirm.setTitle(buttonText, for: .normal)
        btSubOrCancel.setTitle(subButtonText, for: .normal)
        
        if isTwoButton {
            btSubOrCancel.isHidden = false
        } else {
            btSubOrCancel.isHidden = true
        }
        
        if isCancel {
            btConfirm.backgroundColor = UIColor.red10
        }
    }
    
    
    
    @IBAction func confirm(_ sender: Any) {
        
        
        self.dismiss(animated: false) {
                   self.callback(true)
        }
    }
    
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: false) {
                   self.callback(false)
        }
    }

}
