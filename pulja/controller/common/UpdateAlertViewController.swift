//
//  UpdateAlertViewController.swift
//  pulja
//
//  Created by 고권일 on 2022/08/31.
//

import UIKit

class UpdateAlertViewController: UIViewController {

    
    
    
    @IBOutlet var lbTitle: UILabel!
    
    
    @IBOutlet var lbMessage: UILabel!
    
    
    @IBOutlet var btConfirm: UIButton!
    
    
    
    @IBOutlet var btSubOrCancel: UIButton!
    
    
    @IBOutlet var alertView: UIView!
    
    var callback : (_ isSend : Bool) -> Void = { _ in}
    
    
    
    
    
    
    @IBOutlet var topConstraint: NSLayoutConstraint!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        btConfirm.setTitle("진단 보러가기", for:.normal)
//        topConstraint.constant = self.view.frame.size.height * 0.2
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func confirm(_ sender: Any) {
        
        self.dismiss(animated: false) {
                   self.callback(true)
        }
        
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: false) {
                   self.callback(false)
        }
    }
    
    
    
    
    
    

}
