//
//  ChangeUnitViewController.swift
//  pulja
//
//  Created by 김병헌 on 2022/10/26.
//

import UIKit

class ChangeUnitViewController: UIViewController {
    
    var callback : ((_ changeType : String) -> Void)?
    
    var keywords:[KeywordData] = []
    
    var recommendUnit : RecommendUnit? = nil
    var recentUnitYN = "N"
    var keyName = ""
    
    @IBOutlet weak var lbTitle: UILabel!
    
    
    @IBOutlet weak var lbUnit2Name: UILabel!
    
    
    @IBOutlet weak var lbSubjectAndUnit1name: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if recentUnitYN == "Y" {
            
            lbTitle.text = "최근 공부한 단원"
            
        } else {
            
            lbTitle.text = "추천 단원"
            
        }
        
        if let unitInfo = recommendUnit {
            
            lbUnit2Name.text = unitInfo.unit2Text
            
            if var subjectName = unitInfo.subjectName, var unit1Name = unitInfo.unit1Name {
               
                if let subjectSeq = unitInfo.subjectSeq, subjectSeq > 9 {
                    let arr = subjectName.components(separatedBy: "-")
                    let a1 = arr[0].substring(from: 1, to: 1)
                    let a2 = arr[1]
                    subjectName = "중등 \(a1)학년 \(a2)학기"
                } else {
                    subjectName = "고등\(subjectName)"
                }
                
                if let index = unit1Name.firstIndex(of: " ") {
                    unit1Name.insert(".", at: index)
                }
                
                
                lbSubjectAndUnit1name.text = "\(subjectName) \(unit1Name)"
            }
            
        }
    }
    
    
    @IBAction func closeAction(_ sender: Any) {
        
        dismiss(animated: true)
    }
    
    
    @IBAction func changeUnitAction(_ sender: Any) {
        self.callback!("change")
        dismiss(animated: true)
    }
    
    
    
    @IBAction func startAction(_ sender: Any) {
        
        self.callback!("start")
        dismiss(animated: true)
        
    }
    
}
