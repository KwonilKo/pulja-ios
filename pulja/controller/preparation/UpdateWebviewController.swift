//
//  UpdateWebviewController.swift
//  pulja
//
//  Created by HANBIT KIM on 2023/09/13.
//

import Foundation
import UIKit
import WebKit

class UpdateWebviewController: PuljaBaseViewController, WKNavigationDelegate, WKUIDelegate {
    
    
    
    @IBOutlet var webFrameView: UIView!
    
    
    
    @IBOutlet var webView: WKWebView!
    
    func setupView() {
        let webConfiguration = WKWebViewConfiguration()

        let customWebView = WKWebView(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height), configuration: webConfiguration).setUiDelegate(self)
        
        customWebView.contentMode = .scaleAspectFit
        customWebView.sizeToFit()
        customWebView.autoresizesSubviews = true
        
        self.webView = customWebView
        
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setupView()
        
//        webView = WKWebView(frame: view.bounds)
//        webView.navigationDelegate = self

//        webFrameView.addSubview(webView)
        
//        if let userSeq = self.myInfo?.userSeq {
//            let stringValue = String(userSeq)
//            let webViewDefaultCookies: [[HTTPCookiePropertyKey: Any]] = [
//                [
//                    .name: "userId",
//                    .value: stringValue,
//                    .path: "/",
//                    .domain: "bridge.edupanion.kr"
//                ]
//            ]
//
//            webView.setCookie(webViewDefaultCookies)
//        } else {
//            // self.myInfo 또는 self.myInfo?.userSeq가 nil인 경우의 처리
//        }
//
//        if let url = URL(string: "https://bridge.edupanion.kr/pullza/request-data-transfer/form") {
//            let request = URLRequest(url: url)
//            webView.load(request)
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let userSeq = self.myInfo?.userSeq {
            let stringValue = String(userSeq)
            let webViewDefaultCookies: [[HTTPCookiePropertyKey: Any]] = [
                [
                    .name: "userId",
                    .value: stringValue,
                    .path: "/",
                    .domain: "bridge.edupanion.kr"
                ]
            ]
            
            webView.setCookie(webViewDefaultCookies)
        } else {
            // self.myInfo 또는 self.myInfo?.userSeq가 nil인 경우의 처리
        }
        //https://bridge.edupanion.kr/pullza/request-data-transfer/form
        if let url = URL(string: "https://bridge.edupanion.kr/pullza/request-data-transfer/form") {
            let request = URLRequest(url: url)
            webView.load(request)
        }
    }
    // 웹뷰 관련 코드 및 WKNavigationDelegate 메서드를 구현할 수 있습니다.
    
    
    @IBAction func btnExit(_ sender: Any) {
        self.dismiss(animated: false)
    }
    
}
