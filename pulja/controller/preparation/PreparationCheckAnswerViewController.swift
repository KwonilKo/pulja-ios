//
//  PreparationCheckAnswerViewController.swift
//  pulja
//
//  Created by kwonilko on 2022/09/20.
//

import UIKit

class PreparationCheckAnswerViewController: UIViewController {
    
    @IBOutlet weak var checkImg: UIImageView!
    @IBOutlet weak var checkMainLb: UILabel!
    @IBOutlet weak var checkView: UIView!
    @IBOutlet weak var checkSubLb: UILabel!
    
    var imgName : String?
    var mainLbText : String?
    var subLbText : String?
    var isCorrect : String?
    var callback : (_ isSend: Bool) -> Void = { _ in}
    
    //@IBOutlet weak var bottomViewConstraint: NSLayoutConstraint!
    //문제 푸는 컨트롤러에서 받는정보
    var delegate : PreparationMultipleChoiceViewController!
    var problemSeq : Int?
    var duration : Double?
    var questionId : Int?
    var userAnswer : String?
    var userSeq : Int?
    var isFromCollection = false
    
    var startTime : DispatchTime?
    var endTime : DispatchTime?
    let threshold = 1.0
    var data : AIRecommendNext?
    
    var collectionData : CollectionExplain?
    
    //피드백 이모지
    let good = ["👍", "🎉", "👏", "🥳", "🙌"]
    let guess = ["🤔", "🥲", "🥺"]
    let bad = ["💪", "👊", "☺️"]
    
    //checkView height Constraint
    @IBOutlet weak var checkViewWidth: NSLayoutConstraint!
    @IBOutlet weak var checkViewHeight: NSLayoutConstraint!
    @IBOutlet weak var checkImgWidth: NSLayoutConstraint!
    @IBOutlet weak var checkImgHeight: NSLayoutConstraint!
    @IBOutlet weak var checkImgTop: NSLayoutConstraint!
    @IBOutlet weak var checkMainTop: NSLayoutConstraint!
    @IBOutlet weak var checkSubTop: NSLayoutConstraint!
    
    let scale = Double(240.0 / 216.0) //확대하는 스케일
    
    func setInit() {
        self.checkImg.layer.opacity = 0.0
        self.checkMainLb.layer.opacity = 0.0
        self.checkSubLb.layer.opacity = 0.0
        self.checkView.layer.opacity = 0.0
        self.checkSubLb.layer.opacity = 0.0
    }
    
    func setFinal(isCorrect : String) {
        self.checkImg.layer.opacity = 1.0
        self.checkMainLb.layer.opacity = 1.0
        self.checkSubLb.layer.opacity = 1.0
        self.checkView.layer.opacity = 1.0
        self.checkMainLb.layer.opacity = 1.0
        self.checkSubLb.layer.opacity = 1.0
        
        //뷰 조절
        self.checkViewHeight.constant *= scale
        self.checkViewWidth.constant *= scale
        
        //이미지 조절
        self.checkImgWidth.constant *= scale
        self.checkImgHeight.constant *= scale
        self.checkImgTop.constant *= scale
        
        if isCorrect == "Y" {
            self.checkImg.image = UIImage(named: "correct13dbig")
        } else {
            self.checkImg.image = UIImage(named: "wrong13dbig")
        }
        
        //main label 조절
        self.checkMainTop.constant *= scale
        self.checkMainLb.font = self.checkMainLb.font.withSize(18*scale)

        //sub label 조절
        self.checkSubTop.constant *= scale
        self.checkSubLb.font = self.checkSubLb.font.withSize(13*scale)
    }
    
    // MARK: - API
    private func postCollectionStudyStep() {
        let d = self.duration ?? 0
        let pSeq = self.problemSeq ?? 0
        let qId = self.questionId ?? 0
        let uA = self.userAnswer ?? ""
        //시간 측정
        LoadingView.showSolve()
        startTime = DispatchTime.now()
        
        if let uS = userSeq {
            CollectionAPI.shared.collectionStudyStep(duration: d, problemSeq: pSeq, questionId: qId, userAnswer: uA, userSeq: uS).done { res in
                if let suc = res.success, suc == true {
                    
                    guard let data = res.data else { return }
                            
                    if let explain = data.explain {
                        self.delegate?.isCorrect = (explain.isCorrect == "Y") ? true : false
                        self.collectionData = explain // 해설
                        self.delegate?.collectionExplain = explain // 해설
                        self.delegate?.answer = nil
                        self.delegate?.video = nil
                        self.delegate?.studyAllKeyword = false
                        self.delegate?.hasWeakVideo = false
                        self.delegate?.feedbackKeywordName = nil
                    }
                    
                    if let problem = data.problem {
                        self.delegate?.collectionProblem = problem // 다음문제
                    }
                    
                    let userDefaultsKey = "hasSolve"
                    if !UserDefaults.standard.bool(forKey: userDefaultsKey) {
                        UserDefaults.standard.set(true, forKey: userDefaultsKey)
                        UserDefaults.standard.synchronize()
                    }
                    
                    // colection banner
                    let bannerCheck = "isCollectionBanner_\(uS)"
                    if !UserDefaults.standard.bool(forKey: bannerCheck) {
                        UserDefaults.standard.set(true, forKey: bannerCheck)
                        UserDefaults.standard.synchronize()
                    }
                }
            }.catch { err in
                // print("그 다음 문제 받아오는데 문제 있으")
                LoadingView.hide()
                self.delegate?.errorMessage = err.localizedDescription
                self.dismiss(animated: false) {
                    self.callback(false)
                }
            }.finally {
                if let data = self.collectionData {
                    if let problemUrl = data.problemUrl {
                        if (problemUrl.contains("error_img.png")) {
                            LoadingView.hide()
                            self.dismiss(animated: false) {
                                self.callback(false)
                            }
                            return
                        }
                    }
                    
                    guard let isCorrect = data.isCorrect else { return }
                    guard let fA = data.feedBackTextA else { return }
                    guard let fB = data.feedBackTextB else { return }
                    
                    //끝나는 시간 측정
                    self.endTime = DispatchTime.now()
                    let nanoDuration = self.endTime!.uptimeNanoseconds - self.startTime!.uptimeNanoseconds
                    let temp = Double(nanoDuration) / 1_000_000_000

                    //1초 보다 작으면, 로티 계속 보여줌
                    if temp < self.threshold {
                        print("1초 보다 작음")
                        DispatchQueue.main.asyncAfter(deadline: .now() + temp) {
                            LoadingView.hide()
                            self.checkView.isHidden = false
                            self.showAnimationNew(d: d, isCorrect : isCorrect, fA: fA, fB: fB)
                        }
                    } else { //1초 보다 크면, 로티 끔
                        LoadingView.hide()
                        self.checkView.isHidden = false
                        self.showAnimationNew(d: d, isCorrect : isCorrect, fA: fA, fB: fB)
                    }
                }
            }
        }
    }
    
    private func postStudyStep() {
        let pSeq = self.problemSeq ?? 0
        let d = self.duration ?? 0
        let qId = self.questionId ?? 0
        let uA = self.userAnswer ?? ""
        
        //시간 측정
        LoadingView.showSolve()
        startTime = DispatchTime.now()
        
        CurationAPI.shared.studyStep(problemSeq: pSeq, duration: d, questionId: qId, userAnswer: uA).done {
            res in
            if let suc = res.success, suc == true {
                self.data = res.data
                guard let data = res.data else { return }
                guard let isCorrect = data.isCorrect else { return }
                self.delegate?.collectionExplain = nil
                self.delegate?.answer = data.answer //해설
                self.delegate?.video = data.video //유형 강의
                self.delegate?.question = data.question //그 다음 문제
                self.delegate?.studyAllKeyword = data.studyAllKeyword ?? false // 키워드 조건 문제 소진여부.
                
                self.delegate?.isCorrect = (isCorrect == "Y") ? true : false
                self.delegate?.hasWeakVideo = (self.delegate?.video == nil) ? false : true
                self.delegate?.feedbackKeywordName = data.keywordName
                
                let userDefaultsKey = "hasSolve"
                if !UserDefaults.standard.bool(forKey: userDefaultsKey) {
                    UserDefaults.standard.set(true, forKey: userDefaultsKey)
                    UserDefaults.standard.synchronize()
                }
            }
        }.catch { err in
            // print("그 다음 문제 받아오는데 문제 있으")
            LoadingView.hide()
            self.delegate?.errorMessage = err.localizedDescription
            self.dismiss(animated: false) {
                self.callback(false)
            }
        }.finally {
            
            guard let data = self.data else { return }
            
            guard let problemUrl = data.question?.problemUrl else { return }
            if (problemUrl.contains("error_img.png")) {
                LoadingView.hide()
                self.dismiss(animated: false) {
                    self.callback(false)
                }
                return
            }
            
            guard let isCorrect = data.isCorrect else { return }
            guard let fA = data.feedBackTextA else { return }
            guard let fB = data.feedBackTextB else { return }
            
            //끝나는 시간 측정
            self.endTime = DispatchTime.now()
            let nanoDuration = self.endTime!.uptimeNanoseconds - self.startTime!.uptimeNanoseconds
            let temp = Double(nanoDuration) / 1_000_000_000
            
            //1초 보다 작으면, 로티 계속 보여줌
            if temp < self.threshold {
                print("1초 보다 작음")
                DispatchQueue.main.asyncAfter(deadline: .now() + temp) {
                    LoadingView.hide()
                    self.checkView.isHidden = false
                    self.showAnimationNew(d: d, isCorrect : isCorrect, fA: fA, fB: fB)
                }
            } else { //1초 보다 크면, 로티 끔
                LoadingView.hide()
                self.checkView.isHidden = false
                self.showAnimationNew(d: d, isCorrect : isCorrect, fA: fA, fB: fB)
            }
        }
    }
    
    func showAnimationNew(d : Double, isCorrect : String, fA: String, fB: String) {
        //opacity 20%
        self.setInit()
        
        // 이미지 세팅
        if isCorrect == "Y" {
            self.checkImg.image = UIImage(named: "correct13dsmall")
        } else {
            self.checkImg.image = UIImage(named: "wrong13dsmall")
        }
        
        // 텍스트 세팅
        if d < 5 {
            self.checkMainLb.text = fA
        } else {
            if isCorrect == "Y" {
                self.checkMainLb.text = fA
            } else {
                self.checkMainLb.text = fA
            }
        }
        self.checkSubLb.text = fB
        
        // 여기서 가끔씩 채점결과 화면이 안 뜨는 현상 발생
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            
            UIView.animate(withDuration: 0.5, delay: 0
                           ,options: [.curveEaseOut]
                           ,animations: {
                
                self.setFinal(isCorrect : isCorrect)
                self.checkView.layoutIfNeeded()
                self.checkImg.layoutIfNeeded()
                self.checkMainLb.layoutIfNeeded()
                self.checkSubLb.layoutIfNeeded()
                
            }, completion: {_ in
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0)
                {
                    UIView.animate(withDuration: 0.5, delay: 0,
                                   animations: {
                        self.checkView.isHidden = true
                    }, completion: { _ in
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            self.dismiss(animated: false) {
                                self.callback(true)
                            }
                        }
                    })
                }
                
            })
        }
    }
    
    // MARK: - Override
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromCollection {
            postCollectionStudyStep()
        } else {
            postStudyStep()
        }
    }
}
