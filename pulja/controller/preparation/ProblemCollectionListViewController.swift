//
//  ProblemCollectionListViewController.swift
//  pulja
//
//  Created by HUN on 2023/01/10.
//

import UIKit
import Lottie

class ProblemCollectionListViewController: PuljaBaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - Action
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Animation Loading View
    private func showLoadingView(isShow: Bool) {
        if isShow {
            LoadingView.show()
        } else {
            LoadingView.hide()
        }
    }
    
    // MARK: - Dropdown View
    @IBOutlet weak var dropdownView: UIView!
    @IBOutlet weak var dropdownStackViewHeight: NSLayoutConstraint!
    @IBOutlet weak var dropdownStackView: UIStackView!
    @IBOutlet weak var dropdownButton: UIView!
    @IBOutlet weak var dropdownButtonLabel: UILabel!
    @IBOutlet weak var dropdownButtonImage: UIImageView!
    @IBOutlet weak var dropdownButton1: UIView!
    @IBOutlet weak var dropdownButton2: UIView!
    @IBOutlet weak var dropdownButtonLabel1: UILabel!
    @IBOutlet weak var dropdownButtonLabel2: UILabel!
    @IBOutlet weak var closeDropdownView: UIView!
    private var currentTabCount = 0
    private var isSelectedHighSchool = true
    private var subjectSeq = 0
    
    private func setDropdown() {
        dropdownView.isHidden = true
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.clickDropdown(_:)))
        dropdownButton.addGestureRecognizer(gesture)
        
        let gesture1 = UITapGestureRecognizer(target: self, action: #selector(self.clickDropdown1(_:)))
        dropdownButton1.addGestureRecognizer(gesture1)
        
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(self.clickDropdown2(_:)))
        self.dropdownButton2.addGestureRecognizer(gesture2)
        
        let gesture3 = UITapGestureRecognizer(target: self, action: #selector(self.closeDropdown(_:)))
        self.closeDropdownView.addGestureRecognizer(gesture3)
    }
    
    private func showDropdown() {
        dropdownButtonImage.image = UIImage(named: "iconSolidCheveronUp16")
        dropdownView.isHidden = false
        dropdownView.alpha = 0.0
        dropdownStackView.transform = CGAffineTransform(translationX: 0, y: -144)
        dropdownStackView.alpha = 0.0
        dropdownStackViewHeight.constant = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.dropdownView.alpha = 1.0
            self.dropdownStackView.transform = CGAffineTransform(translationX: 0, y: 0)
            self.dropdownStackView.alpha = 1.0
            self.dropdownStackViewHeight.constant = 144
        })
    }
    
    private func hideDropdown() {
        dropdownButtonImage.image = UIImage(named: "iconSolidCheveronDown16")
        dropdownStackView.transform = CGAffineTransform(translationX: 0, y: 0)
        UIView.animate(withDuration: 0.3, animations: {
            self.dropdownStackView.transform = CGAffineTransform(translationX: 0, y: -144)
            self.dropdownStackView.alpha = 0.0
            self.dropdownStackViewHeight.constant = 72
            self.dropdownView.alpha = 0.0
        }, completion: {(isCompleted) in
            self.dropdownView.isHidden = true
        })
    }
    
    @objc func clickDropdown(_ gesture: UITapGestureRecognizer) {
        if dropdownView.isHidden {
            showDropdown()
        } else {
            hideDropdown()
        }
    }
    
    @objc func clickDropdown1(_ gesture: UITapGestureRecognizer) { /* 중학수학 */
        if isSelectedHighSchool {
            isSelectedHighSchool = false
            highSchoolTabList[currentTabCount].isSelect = false
            middleSchoolTabList[0].isSelect = true
            currentTabCount = 0
            dropdownButtonLabel.text = "중학수학"
            dropdownButtonLabel1.textColor = UIColor.black
            dropdownButtonLabel2.textColor = UIColor.Foggy
            subjectSeq = 10
            cvTab.reloadData()
            getCollectionList()
            hideDropdown()
        }
    }
    
    @objc func clickDropdown2(_ gesture: UITapGestureRecognizer) { /* 고등수학 */
        if !isSelectedHighSchool {
            isSelectedHighSchool = true
            middleSchoolTabList[currentTabCount].isSelect = false
            highSchoolTabList[0].isSelect = true
            currentTabCount = 0
            dropdownButtonLabel.text = "고등수학"
            dropdownButtonLabel1.textColor = UIColor.Foggy
            dropdownButtonLabel2.textColor = UIColor.black
            subjectSeq = 1
            cvTab.reloadData()
            getCollectionList()
            hideDropdown()
        }
    }
    
    @objc func closeDropdown(_ gesture: UITapGestureRecognizer) {
        hideDropdown()
    }
    
    // MARK: - Tab Collection View
    @IBOutlet weak var header: UIView!
    @IBOutlet weak var cvTab: UICollectionView!
    private var highSchoolTabList: [SubjectTab] = [
        SubjectTab(name: "고등수학(상)", isSelect: false),
        SubjectTab(name: "고등수학(하)", isSelect: false),
        SubjectTab(name: "수학 I", isSelect: false),
        SubjectTab(name: "수학 II", isSelect: false)
    ]
    private var middleSchoolTabList: [SubjectTab] = [
        SubjectTab(name: "1학년 1학기", isSelect: false),
        SubjectTab(name: "1학년 2학기", isSelect: false),
        SubjectTab(name: "2학년 1학기", isSelect: false),
        SubjectTab(name: "2학년 2학기", isSelect: false),
        SubjectTab(name: "3학년 1학기", isSelect: false),
        SubjectTab(name: "3학년 2학기", isSelect: false),
    ]
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isSelectedHighSchool {
            return highSchoolTabList.count
        } else {
            return middleSchoolTabList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = cvTab.dequeueReusableCell(withReuseIdentifier: "ProblemSubjectTabViewCell", for: indexPath) as! ProblemSubjectTabViewCell
        if isSelectedHighSchool {
            cell.collectionTab = highSchoolTabList[indexPath.item]
        } else {
            cell.collectionTab = middleSchoolTabList[indexPath.item]
        }
        return cell
    }
    
    // Click
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if currentTabCount != indexPath.item {
            if isSelectedHighSchool {
                // 이전 선택
                highSchoolTabList[currentTabCount].isSelect = false
                
                // 새로 선택
                if highSchoolTabList[indexPath.item].isSelect == false {
                    highSchoolTabList[indexPath.item].isSelect = true
                }
                subjectSeq = indexPath.item + 1
            } else {
                // 이전 선택
                middleSchoolTabList[currentTabCount].isSelect = false
                
                // 새로 선택
                if middleSchoolTabList[indexPath.item].isSelect == false {
                    middleSchoolTabList[indexPath.item].isSelect = true
                }
                
                subjectSeq = indexPath.item + 10
            }
            
            currentTabCount = indexPath.item
            cvTab.reloadData()
            cvTab.scrollToItem(at: indexPath, at: .left, animated: true)
            getCollectionList()
        }
    }
//    // MARK: - Feedback View
    private func goWebView(url:String, title:String){
        let vc = R.storyboard.main.idpwdViewController()!
        vc.viewTitle = title
        vc.url = url
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true)
    }
    
    private var isBannerCheck1 = false
    private var isBannerCheck2 = false
    private func setFeedbackBanner() {
        
        if let userSeq = self.myInfo?.userSeq {
            // colection banner
            let bannerCheck1 = "isCollectionBanner_\(userSeq)"
            let bannerCheck2 = "isCollectionBannerChecked_\(userSeq)"
            
            isBannerCheck1 = UserDefaults.standard.bool(forKey: bannerCheck1)
            isBannerCheck2 = UserDefaults.standard.bool(forKey: bannerCheck2)
            
            if isBannerCheck1 {
                if !isBannerCheck2 {
                    unitTableView.register(ProblemCollectionHeaderView.nib(), forHeaderFooterViewReuseIdentifier: "ProblemCollectionHeaderView")
                }
            }
        }
    }
    
    // MARK: - Table View
    @IBOutlet weak var unitTableView: UITableView!
    private var framingList: [FramingViewData] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return framingList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = unitTableView.dequeueReusableCell(withIdentifier: "ProblemCollectionUnitViewCell", for: indexPath) as! ProblemCollectionUnitViewCell
        cell.selectionStyle = .none
        cell.framingName.text = framingList[indexPath.item].framingListData?.framingTypeTitle
        cell.unitName.text = ""
        
        if let isFirst = framingList[indexPath.item].isFirst {
            if isFirst {
                cell.unitName.text = framingList[indexPath.item].unit1Name
            }
        }
        
        if let dtoList = framingList[indexPath.item].framingListData?.collectionDTOList {
            cell.setCollectionList(list: dtoList)
        }
        
        cell.clickCallback = { dto in
            //print("dto -> \(dto)")
            let vc = R.storyboard.preparation.problemCollectionDetailViewController()!
            vc.delegate = self
            if let collectionSeq = dto.collectionSeq {
                vc.collectionSeq = collectionSeq
                vc.cameFrom = "pulja2.0_collection_total_next"
                //ab180 이벤트 보내기
                if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
                    if Const.isAirBridge {
                        CommonUtil.shared.ABEvent(
                            category: vc.cameFrom,
                            customs: ["userseq": userSeq, "user_id": userId, "collectionSeq" : vc.collectionSeq]
                        )
                    } else {
                        print("[ABLog]", "-----------------------------------------", separator: " ")
                        print("[ABLog]", "category: \(vc.cameFrom)", separator: " ")
                        print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                        print("[ABLog]", "user_id: \(userId)", separator: " ")
                        print("[ABLog]", "collectionSeq: \(vc.collectionSeq)", separator: " ")
                        print("[ABLog]", "-----------------------------------------", separator: " ")
                    }
                }
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var returnFloat = CGFloat(354 - 24 - 42) // 중단원 라벨(24) x, bottom bar(42) x
        
        if let isFirst = framingList[indexPath.item].isFirst {
            if isFirst {
                returnFloat = 354 - 42 // bottom bar(42) x
            }
        }
        
        if let isLast = framingList[indexPath.item].isLast {
            if isLast && indexPath.item < framingList.count - 1 {
                returnFloat = 354 - 24 + 40 //중단원 라벨(24) x, bottom margin(40) o
            }
        }
        
        return returnFloat
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard section == 0, let headerCell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ProblemCollectionHeaderView") as? ProblemCollectionHeaderView else { return nil }
        
        headerCell.clickCallback = { isSend in
            headerCell.isHidden = true
            if (UIDevice.current.userInterfaceIdiom == .pad) {
                tableView.sectionHeaderHeight = 24
            } else {
                tableView.sectionHeaderHeight = 20
            }
            
            if let userSeq = self.myInfo?.userSeq {
                let bannerCheck = "isCollectionBannerChecked_\(userSeq)"
                UserDefaults.standard.set(true, forKey: bannerCheck)
                UserDefaults.standard.synchronize()
                self.isBannerCheck2 = true
                if isSend {
                    self.goWebView(url: "https://i2c896t0amv.typeform.com/to/dty8FKBc", title: "문제 모음집 의견 보내기")
                }    
            }
            
            tableView.reloadData()
        }
        return headerCell
    }
    
    // MARK: - Override
    override func viewDidLoad() {
        super.viewDidLoad()
        cvTab.delegate = self
        cvTab.dataSource = self
        cvTab.register(ProblemSubjectTabViewCell.nib(), forCellWithReuseIdentifier: "ProblemSubjectTabViewCell")
        cvTab.reloadData()
        cvTab.isHidden = true
        header.isHidden = true
        
        unitTableView.delegate = self
        unitTableView.dataSource = self
        unitTableView.register(ProblemCollectionUnitViewCell.nib(), forCellReuseIdentifier: "ProblemCollectionUnitViewCell")
        
        setDropdown()
        getCollectionSubject()
        
        //ab180 이벤트 보내기
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: "pulja2.0_collection_total_enter",
                    customs: ["userseq": userSeq, "user_id": userId]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "category: pulja2.0_collection_detail_enter", separator: " ")
                print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                print("[ABLog]", "user_id: \(userId)", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
        }
    }
    
    var isUpdate = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        CommonUtil.tabbarController?.tabBar.isHidden = true
        
        if isUpdate {
            isUpdate = false
            getCollectionSubject()
        }
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    // MARK: - API
    private func getCollectionSubject() {
        if let userSeq = self.myInfo?.userSeq {
            CollectionAPI.shared.collectionSubject(userSeq: Int(userSeq)).done { res in
                if let dataList = res.data {
                    for data in dataList {
                        if let name = data.name {
                            let isSelected = data.selected ?? false
                            if name == "고등수학" {
                                if isSelected {
                                    self.isSelectedHighSchool = true
                                    self.dropdownButtonLabel1.textColor = UIColor.Foggy
                                    self.dropdownButtonLabel2.textColor = UIColor.black
                                }
                            } else {
                                if isSelected {
                                    self.isSelectedHighSchool = false
                                    self.dropdownButtonLabel1.textColor = UIColor.black
                                    self.dropdownButtonLabel2.textColor = UIColor.Foggy
                                }
                            }
                            if let subjets = data.subjects {
                                var subjectCount = 0
                                for subject in subjets {
                                    if let selected = subject.selected {
                                        if name == "고등수학" {
                                            self.highSchoolTabList[subjectCount].isSelect = selected
                                        } else {
                                            self.middleSchoolTabList[subjectCount].isSelect = selected
                                        }
                                        if selected {
                                            if let subjectSeq = subject.subjectSeq {
                                                self.subjectSeq = subjectSeq
                                            }
                                            self.currentTabCount = subjectCount
                                            
                                            if name == "고등수학" {
                                                self.dropdownButtonLabel.text = "고등수학"
                                            } else {
                                                self.dropdownButtonLabel.text = "중학수학"
                                            }
                                        }
                                    }
                                    subjectCount += 1
                                }
                            }
                        }
                    }
                    self.cvTab.reloadData()
                    self.cvTab.isHidden = false
                    self.header.isHidden = false
                }
            }.catch { err in
                //self.showLoadingView(isShow: false)
            }.finally {
                self.getCollectionList()
            }
        }
    }
    
    private func getCollectionList() {
        if let userSeq = self.myInfo?.userSeq {
            if let label = dropdownButtonLabel.text {
                self.showLoadingView(isShow: true)
                CollectionAPI.shared.collectionList(schoolTypeText: label, subjectSeq: subjectSeq, userSeq: Int(userSeq)).done { res in
                    if let data = res.data {
                        if let collections = data.unit1CollectionList {
                            self.framingList.removeAll()
                            var count = 0
                            for collection in collections {
                                if let framingList = collection.framingList {
                                    var framingCount = 0
                                    for framingListData in framingList {
                                        let fraiming = FramingViewData(
                                            unit1Seq: collection.unit1Seq,
                                            unit1Name: collection.unit1Name,
                                            unit1SName: collection.unit1SName,
                                            isFirst: framingCount == 0,
                                            isLast: framingCount == framingList.count - 1,
                                            framingListData: framingListData
                                        )
                                        self.framingList.insert(fraiming, at: count)
                                        framingCount += 1
                                        count += 1
                                    }
                                }
                            }
                            
                            self.unitTableView.setContentOffset(CGPointMake(0, 0), animated: false)
                            self.unitTableView.reloadData()
                            self.setFeedbackBanner()
                        }
                    }
                }.catch { err in
                    self.showLoadingView(isShow: false)
                }.finally {
                    self.showLoadingView(isShow: false)
                }
            }
        }
    }
}

// MARK: FramingData
struct FramingViewData {
    let unit1Seq : Int?
    let unit1Name : String?
    let unit1SName : String?
    let isFirst : Bool?
    let isLast : Bool?
    let framingListData: FramingList?
}
