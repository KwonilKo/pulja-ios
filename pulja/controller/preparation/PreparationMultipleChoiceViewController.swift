//
//  PreparationMultipleChoiceViewController.swift
//  pulja
//
//  Created by kwonilko on 2022/09/13.
//

import UIKit
import PanModal
import MaterialComponents.MaterialBottomSheet
import PencilKit
import AirBridge
import Kingfisher
import EasyTipView

class PreparationMultipleChoiceViewController: PuljaBaseViewController, PKCanvasViewDelegate, PKToolPickerObserver, UITextFieldDelegate, MDCBottomSheetControllerDelegate {
    
    @IBOutlet weak var btPencil: UIButton!
    @IBOutlet weak var pkCanvasView: PKCanvasView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var nAView: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var naBtn: UIButton!
    @IBOutlet weak var multipleStack: UIStackView!
    @IBOutlet var multipleOptions: [UIButton]!
    
    @IBOutlet weak var mStackLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var sBtnTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var naBtnLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var mStackTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var pencilToolView: UIView!
    var toolPicker : PKToolPicker!
    
    @IBOutlet weak var answerText: UITextField!
    @IBOutlet weak var naBtnImg: UIImageView!
    @IBOutlet weak var exitBtn: UIButton!
    @IBOutlet weak var answerView: UIView!
    @IBOutlet weak var problemLb: UILabel!
    @IBOutlet weak var unit2View: UIView!
    @IBOutlet weak var unit2Label: UILabel!
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var svProblemImage: UIScrollView!
    @IBOutlet weak var ivProblem: UIImageView!
    @IBOutlet weak var answerTextLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var answerTextTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var problemImageTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var problemImageLeadingConstraint: NSLayoutConstraint!
    
    //닫기 안내 tooltip
    var toolTipView: UIView?
    var cameFromOnboard = false
    var cameFrom = "" //문제 풀이 시작점.
    var multipleAnswerChoice : [Int]? = [] //객관식 - 정답 저장
    {
        didSet {
            if multipleAnswerChoice == [] && freeAnswerChoice == "" {
                deactivateAnswerButton()
            } else {
                if let cnt = multipleAnswerChoice?.count, cnt == self.answerCnt {
                    activateAnswerButton()
                } else if let cnt = multipleAnswerChoice?.count, cnt ==  1 , multipleAnswerChoice?[0] == 5 {
                    activateAnswerButton()
                } else {
                    deactivateAnswerButton()
                }
            }
            //            multipleAnswerChoice == [] ? deactivateAnswerButton() : activateAnswerButton()
        }
    }
    
    var freeAnswerChoice : String? = "" //주관식 - 정답 저장
    {
        didSet {
            if multipleAnswerChoice == [] && freeAnswerChoice == "" {
                deactivateAnswerButton()
            } else {
                activateAnswerButton()
            }
        }
    }
    var selectnaBtn = false
    var isCorrect = false // false -> 오답, true -> 정답
    var hasWeakVideo = false // false -> 취약유형 강의 보여주지 않음, true -> 취약유형 강의 보여줌
    var isMultipleAnswers = false // false -> 단수, true -> 복수
    var answerCnt = 1
    var choiceCnt = 5
    var isMultiple = false // false -> 주관식, true -> 객관식
    {
        didSet {
            if isMultiple {
                // 객관식 답 view 초기화
                resetMultipleNAButton()
            } else {
                // 주관식 답 view 초기화
                resetFreeNAButton()
            }
            self.answerView.isHidden = false
        }
    }
    
    var isFirst = true // true -> 최초 첫 문제, false -> 첫 문제 이후
    var questionId = -1
    var number = -1
    var problemSeq = -1
    var question: AIRecommendStep?
    var answer: AnswerVideo?
    var video: AIWeakVideo?
    var studyAllKeyword: Bool = false
    
    var startTime : DispatchTime?
    var endTime : DispatchTime?
    
    var errorMessage : String = ""
    var noUnit2Info : Bool = false
    
    var unit2Seqs:[Int] = []
    var keywordSeqs:[Int] = []
    var keywordName : String = ""
    
    var image : KFCrossPlatformImage?
    var duration : Double = 0
    var feedbackKeywordName:String?
    let oxImage = [R.image.iconSolidChoiceO(), R.image.iconSolidChoiceX()]
    var problemImageScale = 0.0
    var studyData : AIRecommendStep?
    
    // MARK: - Problem Collection
    var isLastCollection = false
    var isFromCollection = false
    var collectionSeq = 0
    private var collectionStudyData : CollectionStudyStart?
    var collectionProblem: CollectionProblem?
    var collectionExplain: CollectionExplain?
    
    // MARK: - Override
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        svProblemImage.minimumZoomScale = 1.0
        svProblemImage.maximumZoomScale = 3.0
        svProblemImage.delegate = self
        
        // add notification observers
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(backgroundEvent), name: UIApplication.didEnterBackgroundNotification, object: nil)
        
        if cameFromOnboard {
            cameFrom = "pulja2.0_onboard"
        }
    }
    
    @objc func backgroundEvent() {
        print("backgroundEvent - PreparationMultipleChoiceViewController")
        
        self.sendABFinishEvent()
    }
    
    @objc func didBecomeActive() {
        print("did become active")
        
        if /*let sT =*/ self.startTime != nil {
            
            //문제 푼 시간 측정
            endTime = DispatchTime.now()
            let nanoDuration = endTime!.uptimeNanoseconds - startTime!.uptimeNanoseconds
            //let temp = Double(nanoDuration) / 1_000_000_000
            let d = (Double(round(10 * Double(nanoDuration) / 1_000_000_000)/10))
            print("문제 푸는데 걸린 시간은 \(duration) 초 입니다")
            
            if d >= 900 { //15분 이상이면 리셋. 이하면 아무것도 할 필요없음.
                self.startTime = DispatchTime.now()
            }
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        //테블릿인지 아닌지 확인
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            self.ipadAnswerView(size : size)
        default:
            self.multipleStack.spacing = 13
            break
        }
        
        // 태블릿 가로 - 세로 모드에 따른 이미지 스케일 변경
        if UIDevice.current.orientation.isLandscape {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                self.svProblemImage.zoomScale = self.problemImageScale
                self.svProblemImage.setContentOffset(CGPoint(x: 0 ,y: 0), animated: false)
            }
        } else {
            self.svProblemImage.zoomScale = 1.0
            self.svProblemImage.setContentOffset(CGPoint(x: 0 ,y: 0), animated: false)
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.ivProblem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // 문제 모음집 상세화면으로 pop
        if isPop {
            self.navigationController?.popViewController(animated: true)
        } else {
            //exit 버튼 숨기기,보여주기
            checkFirstQuestion()
            
            answerView.isHidden = true
            
            CommonUtil.tabbarController?.bottomNavBar.isHidden = true
            CommonUtil.tabbarController?.tabBar.isHidden = true
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
            
            //주관식 뷰 세팅
            setAnswerText()
            
            // 문제모음집 마지막 문제인 경우 결과화면으로 이동
            if isLastCollection {
                moveToResult()
            } else {
                //객관식, 주관식 유무에 따라서 answerView 세팅
                initializeAnswerView()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        initPencil()
    }
    
    // MARK: - API
    private func initializeAnswerView() {
        if isFirst {
            //최초 첫 문제는 api 호출 - 중단원 정보 없는 경우
            hideInit()

            if isFromCollection {
                getCollectionStudyStart()
            } else {
                getStudyStart()
            }
        } else {
            if isFromCollection {
                postCollectionStudyStep()
            } else {
                postStudyStep()
            }
        }
        
        //잘 모르겠어요 선택은 처음에 무조건 false
        self.selectnaBtn = false
        
        //테블릿인지 아닌지 확인
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            self.ipadAnswerView(size: self.view.frame.size)
        default:
            self.multipleStack.spacing = 13
            self.multipleStack.layoutIfNeeded()
        }
    }
    
    private func getCollectionStudyStart() {
        let startCameFrom = setStartCameFrom()
        //print("!!!\(startCameFrom)")
        
        if let userSeq = self.myInfo?.userSeq {
            CollectionAPI.shared.collectionStudyStart(cameFrom: startCameFrom, collectionSeq: self.collectionSeq, userSeq: Int(userSeq)).done { res in
                if let suc = res.success, suc == true {
                    if let data = res.data {
                        self.collectionStudyData = data
                        if let problem = data.problem {
                            let pType = problem.problemType
                            let title = problem.title ?? ""
                            let aCnt = problem.answerCnt ?? 0
                            let qId = problem.questionId ?? 0
                            let number = problem.number ?? 0
                            let pSeq = data.problemSeq ?? 0
                            
                            self.choiceCnt = data.problem?.choiceCnt ?? 5
                            self.isMultiple = (pType == "cho") ? true : false
                            self.isMultipleAnswers = (aCnt > 1) ? true : false
                            self.answerCnt = aCnt
                            self.questionId = qId
                            self.number = number
                            self.problemSeq = pSeq
        
                            //n번 문제, 단원 이름
//                            guard let n = data.number else { return }
//                            guard let title = data.title else { return }
                            self.problemLb.text = "문제 \(String(number))"
                            self.unit2Label.text = self.processTitle(title: title)
        
                            //문제보여주기
                            self.viewprobImg()
                        }
                    }
                }
            }.catch { err in
                print(err.localizedDescription)
                let vc = R.storyboard.preparation.preparationErrorViewController()!
                vc.cameFromOnboard = self.cameFromOnboard
                vc.problemseqlb = "\(String(self.problemSeq))"
                vc.question_id = "\(String(self.questionId))"
                self.navigationController?.pushViewController(vc, animated: true)
            }.finally {
                guard let problemUrl = self.collectionStudyData?.problem?.problemUrl else { return }
                if (problemUrl.contains("error_img.png")) {
                    let vc = R.storyboard.preparation.preparationErrorViewController()!
                    vc.cameFromOnboard = self.cameFromOnboard
                    vc.problemseqlb = "\(String(self.problemSeq))"
                    vc.question_id = "\(String(self.questionId))"
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                    return
                }
                
                self.unhideInit()
            }
        }
    }
    
    private func postCollectionStudyStep() {
        // print("!!! -> postCollectionStudyStep()")
        guard let data = self.collectionProblem else { return }
        guard let pType = data.problemType else { return }
        guard let aCnt = data.answerCnt else { return }
        guard let qId = data.questionId else { return }
        guard let number = data.number else { return }
        
        self.choiceCnt = data.choiceCnt ?? 5
        self.isMultiple = (pType == "cho") ? true : false
        self.isMultipleAnswers = (aCnt > 1) ? true : false
        self.answerCnt = aCnt
        self.questionId = qId
        self.number = number
        self.isLastCollection = data.last ?? false
        //n번 문제, 단원 이름
        guard let n = data.number else { return }
        guard let title = data.title else { return }
        self.problemLb.text = "문제 \(String(n))"
        self.unit2Label.text = title.components(separatedBy: CharacterSet.decimalDigits).joined()
        
        //문제보여주기
        self.viewprobImg()
    }
    
    private func getStudyStart() {
        let startCameFrom = setStartCameFrom()
        //print("!!!\(startCameFrom)")
        
        let isContinuingStudy = startCameFrom == "02"
        //print("!!!\(isContinuingStudy)")
        
        CurationAPI.shared.studyStart(keywordSeqs:self.keywordSeqs, unit2Seqs:self.unit2Seqs, isContinuingStudy: isContinuingStudy, cameFrom: startCameFrom ).done{
            res in
            if let suc = res.success, suc == true {
                self.studyData = res.data
                guard let data = res.data else { return }
                guard let pType = data.problemType else { return }
                guard let aCnt = data.answerCnt else { return }
                guard let qId = data.questionId else { return }
                guard let number = data.number else { return }
                guard let problemSeq = data.problemSeq else { return }
                
                //                    self.isMultiple = false
                self.choiceCnt = data.choiceCnt ?? 5
                self.isMultiple = (pType == "cho") ? true : false
                self.isMultipleAnswers = (aCnt > 1) ? true : false
                self.answerCnt = aCnt
                self.questionId = qId
                //                        self.qLbQA.text = "문항번호: \(qId)" //풀자 2.0 콘텐츠 QA
                self.number = number
                self.problemSeq = problemSeq
                
                //n번 문제, 단원 이름
                guard let n = data.number else { return }
                guard let title = data.title else { return }
                self.problemLb.text = "문제 \(String(n))"
                //self.unit2Label.text = self.processTitle(title: title)
                //self.unit2Label.text = String(titleLabel.dropFirst())
                if title.contains("도함수") {
                    self.unit2Label.text = title.substring(from:3, to:title.count - 1)
                } else {
                    let labelText = title.components(separatedBy: CharacterSet.decimalDigits).joined()
                    self.unit2Label.text = String(labelText.dropFirst())
                }
                
                
                //문제보여주기
                self.viewprobImg()
                
                if data.studyAllKeyword ?? false {
                    CommonUtil().showHAlert(title: "선택한 키워드의 문제를\n모두 풀었어요", message: "같은 단원 추천 문제들로 더 풀어볼까요?", button: "이어 풀기", subButton: "나가기", viewController: self) { isSend in
                        if !isSend
                        {
                            //ab180 이벤트 보내기
                            if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
                                if Const.isAirBridge {
                                    CommonUtil.shared.ABEvent(
                                        category: "pulja2.0_study_noproblem",
                                        customs: ["userseq": userSeq, "user_id": userId, "number" : self.number, "keyword" : self.keywordName, "unit2List": self.unit2Seqs  ]
                                    )
                                } else {
                                    print("[ABLog]", "-----------------------------------------", separator: " ")
                                    print("[ABLog]", "category: pulja2.0_study_noproblem", separator: " ")
                                    print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                                    print("[ABLog]", "user_id: \(userId)", separator: " ")
                                    print("[ABLog]", "number: \(self.number)", separator: " ")
                                    print("[ABLog]", "keyword: \(self.keywordName)", separator: " ")
                                    print("[ABLog]", "unit2List: \(self.unit2Seqs)", separator: " ")
                                    print("[ABLog]", "-----------------------------------------", separator: " ")
                                }
                            }
                            self.exitPage()
                        }
                    }
                }
            }
        }.catch { err in
            print(err.localizedDescription)
            let vc = R.storyboard.preparation.preparationErrorViewController()!
            vc.cameFromOnboard = self.cameFromOnboard
            vc.problemseqlb = "\(String(self.problemSeq))"
            vc.question_id = "\(String(self.questionId))"
            self.navigationController?.pushViewController(vc, animated: true)
        }.finally {
            guard let problemUrl = self.studyData?.problemUrl else { return }
            if (problemUrl.contains("error_img.png")) {
                let vc = R.storyboard.preparation.preparationErrorViewController()!
                vc.cameFromOnboard = self.cameFromOnboard
                vc.problemseqlb = "\(String(self.problemSeq))"
                vc.question_id = "\(String(self.questionId))"
                self.navigationController?.pushViewController(vc, animated: true)
                
                return
            }
            
            self.unhideInit()
            
            //최초 문제인 경우, 문제 로딩 후 툴팁 보여주기
            self.showCloseToolTip()
            
            // 키워드 선택 최초 실행여부 저장.
            let userDefaultsKey = "hasProblemSeq"
            if !UserDefaults.standard.bool(forKey: userDefaultsKey) {
                UserDefaults.standard.set(true, forKey: userDefaultsKey)
                UserDefaults.standard.synchronize()
            }
        }
    }
    private func postStudyStep() {
        // 맞추거나/틀린 경우, 그 다음 문제를 question에서 꺼내서 뿌려줌
        guard let data = self.question else { return }
        guard let pType = data.problemType else { return }
        guard let aCnt = data.answerCnt else { return }
        guard let qId = data.questionId else { return }
        guard let number = data.number else { return }
        guard let problemSeq = data.problemSeq else { return }
        
        self.choiceCnt = data.choiceCnt ?? 5
        self.isMultiple = (pType == "cho") ? true : false
        self.isMultipleAnswers = (aCnt > 1) ? true : false
        self.answerCnt = aCnt
        
        self.questionId = qId
        //            self.qLbQA.text = "문항번호: \(qId)" //풀자 2.0 콘텐츠 QA
        self.number = number
        self.problemSeq = problemSeq
        
        //n번 문제, 단원 이름
        guard let n = data.number else { return }
        guard let title = data.title else { return }
        self.problemLb.text = "문제 \(String(n))"
        let titleLabel = title.components(separatedBy: CharacterSet.decimalDigits).joined()
        self.unit2Label.text = String(titleLabel.dropFirst())
        
        //문제보여주기
        self.viewprobImg()
        if data.studyAllKeyword ?? false {
            CommonUtil().showHAlert(title: "선택한 키워드의 문제를\n모두 풀었어요", message: "같은 단원 추천 문제들로 더 풀어볼까요?", button: "이어 풀기", subButton: "나가기", viewController: self) { isSend in
                if !isSend {
                    //ab180 이벤트 보내기
                    if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
                        if Const.isAirBridge {
                            CommonUtil.shared.ABEvent(
                                category: "pulja2.0_study_noproblem",
                                customs: ["userseq": userSeq, "user_id": userId, "number" : self.number, "keyword" : self.keywordName, "unit2List": self.unit2Seqs  ]
                            )
                        } else {
                            print("[ABLog]", "-----------------------------------------", separator: " ")
                            print("[ABLog]", "category: pulja2.0_study_noproblem", separator: " ")
                            print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                            print("[ABLog]", "user_id: \(userId)", separator: " ")
                            print("[ABLog]", "number: \(self.number)", separator: " ")
                            print("[ABLog]", "keyword: \(self.keywordName)", separator: " ")
                            print("[ABLog]", "unit2List: \(self.unit2Seqs)", separator: " ")
                            print("[ABLog]", "-----------------------------------------", separator: " ")
                        }
                    }
                    self.exitPage()
                } else {
                    if self.feedbackKeywordName != nil {
                        self.stisfactionOpen()
                    }
                }
            }
            
        } else {
            if self.feedbackKeywordName != nil {
                self.stisfactionOpen()
            }
        }
    }
    
    // MARK: - ToolTip
    func showCloseToolTip(){
        var preferences = EasyTipView.Preferences()
        preferences.drawing.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 12)!
        preferences.drawing.foregroundColor = UIColor.white
        preferences.drawing.backgroundColor = UIColor.black
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.top
        
        preferences.animating.dismissTransform = CGAffineTransform(translationX: 0, y: 15)
        preferences.animating.showInitialTransform = CGAffineTransform(translationX: 0, y: 15)
        preferences.animating.showInitialAlpha = 0
        preferences.animating.showDuration = 1
        preferences.animating.dismissDuration = 1
        
        let easyTipView = EasyTipView(text: "원하는 만큼 풀고, 결과 분석을 받아보세요", preferences: preferences)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            easyTipView.show(forView: self.exitBtn, withinSuperview: self.view)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            easyTipView.dismiss()
        }
    }
    
    // MARK: - 문제 풀이 컴포넌트
    func initializeAnswerText() {
        answerText.layer.borderColor = UIColor.Foggy.cgColor
        answerText.placeholder = "답안을 입력해주세요"
        answerText.text = ""
    }
    
    func checkFirstQuestion() {
        if UserDefaults.standard.bool(forKey: "first_problem_solve") {
            self.exitBtn.isHidden = false
        } else {
            self.exitBtn.isHidden = false // 해제. 221109
        }
    }
    
    func processUnit1(unit1: String) -> String {
        let replaced = unit1.map {
            $0.isNumber == true ? String("\($0).") : String($0) }.joined(separator: "")
        return replaced
    }
    
    func updateFirstQuestion() {
        if !UserDefaults.standard.bool(forKey: "first_problem_solve") {
            UserDefaults.standard.set(true, forKey: "first_problem_solve")
            UserDefaults.standard.synchronize()
        }
    }
    
    func setAnswerText() {
        answerText.keyboardType = .default
        answerText.addTarget(self, action: #selector(PreparationMultipleChoiceViewController.textFieldDidChange(_: )), for: .editingChanged)
        answerText.borderStyle = .none
        answerText.layer.borderWidth = 1.0
        answerText.layer.cornerRadius = 8.0
        answerText.addDoneButtonOnKeyboard()
        answerText.returnKeyType = .done
        answerText.setLeftPaddingPoints(16.0)
        answerText.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillHide(_ notification : Notification) {
        self.bottomConstraint.constant = 0
        answerText.layer.borderColor = UIColor.Foggy.cgColor
        bottomSheet?.mdc_bottomSheetPresentationController?.preferredSheetHeight = 400
    }
    
    @objc func keyboardWillShow(_ notification: Notification)
    {
        if let keyboardFrame: NSValue = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            //            self.bottomConstraint.constant = -1 * ( keyboardRectangle.height - 36 )
            self.bottomConstraint.constant = ( keyboardRectangle.height - 36 )
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
        
        answerText.layer.borderColor = UIColor.Purple.cgColor
        
        bottomSheet?.mdc_bottomSheetPresentationController?.preferredSheetHeight = 307
        bottomSheet?.mdc_bottomSheetPresentationController?.adjustHeightForSafeAreaInsets = true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.bottomConstraint.constant = 0
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //못 풀겠어요 를 선택한 경우, 해제
        if self.selectnaBtn {
            //못 풀겠어요 색깔 foggy로 변경
            naBtn.setTitleColor(UIColor.Foggy, for: .normal)
            naBtnImg.image = UIImage(named: "emoji-sad")
            nAView.backgroundColor = UIColor.white
            
            //버튼 비활성화
            deactivateAnswerButton()
            selectnaBtn = false
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        answerText.borderStyle = .none
        
        //정답 정보 저장
        freeAnswerChoice = textField.text!
        
        //textfield border color 변경
        if textField.text! == "" {
            answerText.layer.borderWidth = 1.0
            answerText.layer.cornerRadius = 8.0
            answerText.textColor = UIColor.Foggy
            answerText.layer.borderColor = UIColor.Foggy.cgColor
            deactivateAnswerButton() //09.29.22 코드 추가
        } else {
            answerText.layer.borderWidth = 1.0
            answerText.layer.cornerRadius = 8.0
            answerText.textColor = UIColor.puljaBlack
            answerText.layer.borderColor = UIColor.Purple.cgColor
            activateAnswerButton() //09.29.22 코드 추가
            
            //잘 모르겠어요 버튼 초기화 - 11.09 코드 추가
            naBtn.setTitleColor(UIColor.Foggy, for: .normal)
            naBtnImg.image = UIImage(named: "emoji-sad")
            nAView.backgroundColor = UIColor.white
            selectnaBtn = false
        }
    }
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        if let presentableController = viewControllerToPresent as? PanModalPresentable, let controller = presentableController as? UIViewController {
            controller.modalPresentationStyle = .custom
            controller.modalPresentationCapturesStatusBarAppearance = true
            controller.transitioningDelegate = PanModalPresentationDelegate.default
            super.present(controller, animated: flag, completion: completion)
            return
        }
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
    
    @IBAction func lbExit(_ sender: Any) {
        if UserDefaults.standard.bool(forKey: "hasSolve") {
            CommonUtil().showHAlert(title: "정말로 그만 푸시겠어요?", message: "", button: "그만 풀기", subButton: "계속 풀기", viewController: self) { isSend in
                if isSend
                {
                    //ab180 이벤트 보내기
                    self.sendABFinishEvent()
                    self.exitPage()
                }
            }
        } else {
            CommonUtil().showHAlert(title: "정말로 그만 푸시겠어요?", message: "한 문제도 풀지 않으면,\n추천 키워드를 받아 볼 수 없어요 😭", button: "그만 풀기", subButton: "계속 풀기", viewController: self) { isSend in
                if isSend
                {
                    //ab180 이벤트 보내기
                    self.sendABFinishEvent()
                    self.exitPage()
//                    if let vcDetail = R.storyboard.main.tabbarController() {
//                        self.navigationController?.setViewControllers([vcDetail], animated: true)
//                    }
                }
            }
        }
    }
    private var isPop = false
    @IBOutlet var rootView: UIView!
    func exitPage() {
        
        if cameFromOnboard {
            if let vcDetail = R.storyboard.main.tabbarController() {
                self.navigationController?.setViewControllers([vcDetail], animated: true)
            }
        } else {
            if self.number <= 1 {
                if isFromCollection {
                    self.navigationController?.popViewController(animated: true)
                } else {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            } else {
                
                if isFromCollection && isFirst {
                    self.navigationController?.popViewController(animated: true)
                } else {
                    moveToResult()
                }
            }
        }
    }
    
    private func moveToResult() {
        if isFromCollection {
            isPop = true
            rootView.isHidden = true
        }
        let vc = R.storyboard.preparation.aiResultViewController()!
        vc.cameFromOnboard = self.cameFromOnboard
        vc.problemSeq = self.problemSeq
        vc.keywordName = self.keywordName
        vc.unit2Seqs = self.unit2Seqs
        vc.isFromCollection = self.isFromCollection
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    var isLandscape: Bool {
        if #available(iOS 13.0, *) {
            return UIApplication.shared.windows
                .first?
                .windowScene?
                .interfaceOrientation
                .isLandscape ?? false
        } else {
            return UIApplication.shared.statusBarOrientation.isLandscape
        }
    }
    
    func viewprobImg() {
        let tempQ = self.questionId
        let qUrl = URL(string: "\(Const.PROBLEM_IMAGE_URL)\(tempQ).jpg")
        
        print("현재 문제 아이디: \(tempQ)")
        
        //문제 load
        LoadingView.show()
        self.svProblemImage.zoomScale = 1.0 // 확대 축소 초기화.
        self.problemImageScale = 1.0
        self.ivProblem.isHidden = true
        self.ivProblem.image = nil
        
        self.ivProblem.kf.indicatorType = .activity
        self.ivProblem.kf.setImage(with: qUrl, options: [.transition(.fade(0.2))],
                                   completionHandler: {
            result in
            switch result{
            case .success(let value):
                
                let image = value.image
                self.image = image
                
                self.ivProblem.image = image.aspectFitImage(inRect: self.ivProblem.frame)
                self.ivProblem.contentMode = .topLeft
                
                let height = self.ivProblem.intrinsicContentSize.height
                let width = self.ivProblem.intrinsicContentSize.width
                
                // 문제 scale 조정
                if (width * 2 < height) {
                    if (self.isLandscape) {
                        self.problemImageScale = 3.0
                    } else {
                        self.problemImageScale = 1.75
                    }
                }
                else if (width * 1.5 < height) {
                    if (self.isLandscape) {
                        self.problemImageScale = 2.0
                    } else {
                        self.problemImageScale = 1.25
                    }
                }
                else {
                    self.problemImageScale = 1.0
                }
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                    self.svProblemImage.zoomScale = self.problemImageScale
                    self.svProblemImage.setContentOffset(CGPoint(x: 0 ,y: 0), animated: false)
                    self.svProblemImage.flashScrollIndicators()
                    self.svProblemImage.isHidden = false
                    self.ivProblem.isHidden = false
                }
                LoadingView.hide()
                print("사진 불러오는데 성공함")
                
                //푸는 타임 측정
                self.startTime = DispatchTime.now()
                //print("startTime: \(self.startTime)")
                
            default:
                print("사진 불러오는데 실패함")
                return
            }
        })
    }
    
    func hideInit() {
        LoadingView.show()
        topView.isHidden = true
        lineView.isHidden = true
        svProblemImage.isHidden = true
        btPencil.isHidden = true
        self.answerView.isHidden = true
    }
    
    func unhideInit() {
        topView.isHidden = false
        lineView.isHidden = false
        svProblemImage.isHidden = false
        btPencil.isHidden = false
        self.answerView.isHidden = false
        LoadingView.hide()
    }
    
    func ipadAnswerView(size: CGSize) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            //1, 5번 leading , trailing constraint 조정
            self.mStackLeadingConstraint.constant = 128.0
            self.mStackTrailingConstraint.constant = 128.0
            self.multipleStack.spacing = 20 // pad 20
            self.multipleStack.layoutIfNeeded()
            
            //못 풀겠어요, 채점하기 leading, trailing constraint 조정
            self.naBtnLeadingConstraint.constant = 128.0
            self.sBtnTrailingConstraint.constant = 128.0
            
            //answerView 간격 조절
            self.answerTextLeadingConstraint.constant = 128.0
            self.answerTextTrailingConstraint.constant = 128.0
            
            // 사진 가격 조절
            self.problemImageLeadingConstraint.constant = 128.0
            self.problemImageTrailingConstraint.constant = 128.0
            
            //사진 조절
            guard let img = self.image else { return }
            self.ivProblem.image = img.aspectFitImage(inRect: self.ivProblem.frame)
            self.ivProblem.contentMode = .topLeft
        }
    }
    
    func processTitle(title: String) -> String {
        _ = ""
        if title.contains("도함수") {
            return title.substring(from:3, to:title.count - 1)
        } else {
            return title.components(separatedBy: CharacterSet.decimalDigits).joined()
        }
    }
    
    func activateMultipleBtn(idx: Int){
        multipleOptions[idx].borderWidth = 2
        multipleOptions[idx].borderColor = UIColor.Purple
        multipleOptions[idx].setTitleColor(UIColor.Purple, for: .normal)
        multipleOptions[idx].tintColor = .Purple
    }
    
    func deactivateMultipleBtn(idx: Int){
        multipleOptions[idx].borderWidth = 1
        multipleOptions[idx].borderColor = UIColor.Foggy
        multipleOptions[idx].setTitleColor(UIColor.grey, for: .normal)
        multipleOptions[idx].tintColor = .Foggy
    }
    
    func isButtonSelected(_ sender: UIButton) -> Bool {
        return sender.borderColor == UIColor.Purple
    }
    
    func resetFreeNAButton() {
        //객관식 보이기, 주관식 숨기기
        answerText.isHidden = false
        multipleStack.isHidden = true
        
        //주관식 리셋
        initializeAnswerText()
        
        //잘 모르겠어요 버튼 초기화
        naBtn.setTitleColor(UIColor.Foggy, for: .normal)
        naBtnImg.image = UIImage(named: "emoji-sad")
        nAView.backgroundColor = UIColor.white
        
        //업데이트
        multipleAnswerChoice = []
        freeAnswerChoice = ""
    }
    
    func resetMultipleNAButton() {
        //객관식 보이기, 주관식 숨기기
        answerText.isHidden = true
        multipleStack.isHidden = false
        
        if self.choiceCnt == 2 {
            for i in 0..<2 {
                multipleOptions[i].setTitle(nil, for: .normal)
                multipleOptions[i].setImage(oxImage[i], for: .normal)
                deactivateMultipleBtn(idx: i)
            }
            
            multipleOptions[2].isHidden = true
            multipleOptions[3].isHidden = true
            multipleOptions[4].isHidden = true
        } else {
            //객관식 버튼들 초기화
            for i in 0..<5 {
                let num = i + 1
                multipleOptions[i].setTitle("\(num)", for: .normal)
                multipleOptions[i].setImage(nil, for: .normal)
                multipleOptions[i].isHidden = false
                deactivateMultipleBtn(idx: i)
            }
        }
        
        //잘 모르겠어요 버튼 초기화
        naBtn.setTitleColor(UIColor.Foggy, for: .normal)
        naBtnImg.image = UIImage(named: "emoji-sad")
        nAView.backgroundColor = UIColor.white
        
        //업데이트
        multipleAnswerChoice = []
        freeAnswerChoice = ""
    }
    
    func sendAnswerToServer() -> String {
        //서버에 보내는 정답 variable
        var result = ""
        
        //객관식인 경우
        if self.multipleAnswerChoice != [] {
            for m in multipleAnswerChoice! {
                if m == 5 { //잘 모르겠어요
                    result += "⑥"
                    break
                } else {
                    switch m {
                    case 0:
                        result += "①"
                    case 1:
                        result += "②"
                    case 2:
                        result += "③"
                    case 3:
                        result += "④"
                    default:
                        result += "⑤"
                    }
                }
            }
        } else {  //주관식인 경우
            result = freeAnswerChoice!
        }
        
        return result
    }
    
    // MARK: - Pencil
    @IBAction func pencilClear(_ sender: Any) {
        pkCanvasView.drawing = PKDrawing()
    }
    
    @IBAction func pencilClose(_ sender: Any) {
        pkCanvasView.isHidden = true
        pencilToolView.isHidden = true
        btPencil.isHidden = false
        
        pkCanvasView.becomeFirstResponder()
        
        if #available(iOS 14.0, *) {
            toolPicker.setVisible(false, forFirstResponder: pkCanvasView)
        } else {
            guard view.window != nil else { return }
            toolPicker.setVisible(false, forFirstResponder: pkCanvasView)
        }
    }
    
    @IBAction func popupPencilView(_ sender: Any) {
        pkCanvasView.isHidden = false
        pencilToolView.isHidden = false
        btPencil.isHidden = true
        
        if #available(iOS 14.0, *) {
            toolPicker.setVisible(true, forFirstResponder: pkCanvasView)
        } else {
            guard view.window != nil else { return }
            toolPicker.setVisible(true, forFirstResponder: pkCanvasView)
        }
    }
    
    func initPencil() {
        // Set up the canvas view with the first drawing from the data model.
        pkCanvasView.delegate = self
        pkCanvasView.alwaysBounceVertical = false
        pkCanvasView.isOpaque = false
        // Set up the tool picker
        if #available(iOS 14.0, *) {
            toolPicker = PKToolPicker()
        } else {
            // Set up the tool picker, using the window of our parent because our view has not
            // been added to a window yet.
            guard let window = view.window else { return }
            toolPicker = PKToolPicker.shared(for:  window)
        }
        toolPicker.setVisible(false, forFirstResponder: pkCanvasView)
        toolPicker.addObserver(pkCanvasView)
        toolPicker.addObserver(self)
        //    updateLayout(for: toolPicker)
        pkCanvasView.becomeFirstResponder()
        if #available(iOS 14.0, *) {
            pkCanvasView.allowsFingerDrawing = true
        } else {
            pkCanvasView.allowsFingerDrawing = true
        }
    }
    
    @IBAction func touchnaButton(_ sender: UIButton) {
        if !selectnaBtn { //잘 모르겠어요 선택한 경우
            
            if self.isMultiple { //객관식인 경우
                //나머지 객관식 버튼들 초기화
                for i in 0..<5 {
                    deactivateMultipleBtn(idx: i)
                }
                //잘 모르겠어요 버튼 활성화
                naBtn.setTitleColor(UIColor.Purple, for: .normal)
                naBtnImg.image = UIImage(named: "emoji-sad-purple")
                nAView.backgroundColor = UIColor.OffWhite
                
                //업데이트
                multipleAnswerChoice = [5]
            } else { //주관식인 경우
                //잘 모르겠어요 버튼 활성화
                naBtn.setTitleColor(UIColor.Purple, for: .normal)
                naBtnImg.image = UIImage(named: "emoji-sad-purple")
                nAView.backgroundColor = UIColor.OffWhite
                
                //주관식 textfield초기화
                initializeAnswerText()
                
                //업데이트
                freeAnswerChoice = "잘 모르겠음."
            }
        } else { //잘 모르겠어요 선택해제 한 경우
            //잘 모르겠어요 버튼 초기화
            naBtn.setTitleColor(UIColor.Foggy, for: .normal)
            naBtnImg.image = UIImage(named: "emoji-sad")
            nAView.backgroundColor = UIColor.white
            
            //업데이트
            if self.isMultiple{
                multipleAnswerChoice = []
            } else {
                freeAnswerChoice = ""
            }
        }
        selectnaBtn.toggle()
    }
    
    
    @IBAction func touchButton(_ sender: UIButton) {
        //잘 모르겠어요를 선택한 경우, 초기화
        if multipleAnswerChoice == [5] {
            selectnaBtn.toggle()
            naBtn.setTitleColor(UIColor.Foggy, for: .normal)
            naBtnImg.image = UIImage(named: "emoji-sad")
            nAView.backgroundColor = UIColor.white
            
            multipleAnswerChoice = []
        }
        
        //방금 선택한 답 추출
        guard let selectedIdx = multipleOptions.firstIndex(of: sender) else { return }
        
        if !isMultipleAnswers { //객관식 - 단수
            //정답이 단수이면서, 답을 선택한 경우
            if multipleAnswerChoice != [] {
                let answerIdx = multipleAnswerChoice![0]
                //선택한 답 != 이전 선택했던 답
                if selectedIdx != answerIdx {
                    //이전에 선택했던 답 초기화
                    deactivateMultipleBtn(idx: answerIdx)
                    //선택한 답 색칠
                    activateMultipleBtn(idx: selectedIdx)
                    //업데이트
                    multipleAnswerChoice = [selectedIdx]
                } else {
                    //선택한 답, 이전 선택했던 답 초기화
                    deactivateMultipleBtn(idx: answerIdx)
                    //업데이트
                    multipleAnswerChoice = []
                }
            } else { //정답이 단수이면서, 답을 선택안 한 경우
                //선택한 답 색칠
                activateMultipleBtn(idx: selectedIdx)
                //업데이트
                multipleAnswerChoice = [selectedIdx]
            }
        } else { //객관식 - 복수
            //선택한 답이 이미 색칠해져 있는 경우
            if isButtonSelected(sender) {
                //이전에 선택했던 답 초기화
                deactivateMultipleBtn(idx: selectedIdx)
                guard let removeIdx = multipleAnswerChoice!.firstIndex(of: selectedIdx) else { return }
                //업데이트
                multipleAnswerChoice!.remove(at: removeIdx)
            } else {
                
                guard let choiceCount =  multipleAnswerChoice?.count, choiceCount < self.answerCnt else {
                    self.showToast(message: "정답은 \(self.answerCnt)개까지 선택할 수 있어요")
                    return
                }
                
                //선택한 답 색칠
                activateMultipleBtn(idx: selectedIdx)
                //업데이트
                multipleAnswerChoice! += [selectedIdx]
            }
        }
    }
    
    func deactivateAnswerButton() {
        submitBtn.isUserInteractionEnabled = false
        submitBtn.backgroundColor = UIColor.Foggy40
        submitBtn.setTitleColor(UIColor.Foggy, for: .normal)
    }
    
    func activateAnswerButton() {
        submitBtn.isUserInteractionEnabled = true
        submitBtn.backgroundColor = UIColor.Purple
        submitBtn.setTitleColor(.white, for: .normal)
    }
    
    func randomSetVariable() {
        let isCorrectRandom = [false]
        let hasWeakVideoRandom = [false, true]
        let isMultipleAnswersRandom = [false, true]
        
        self.isCorrect = isCorrectRandom.randomElement()!
        self.hasWeakVideo = hasWeakVideoRandom.randomElement()!
        self.isMultipleAnswers = isMultipleAnswersRandom.randomElement()!
    }
    
    @IBAction func submitAnswer(_ sender: Any) {
        //푸는 시간 nul 인 경우 무시
        guard self.startTime != nil else { return }
        
        //키보드 내림
        self.answerText.resignFirstResponder()
        
        //canvasview reset
        pkCanvasView.drawing = PKDrawing()
        
        //취약유형 강의 유무 variable 초기화
        self.hasWeakVideo = false
        
        //문제 푼 시간 측정
        endTime = DispatchTime.now()
        let nanoDuration = endTime!.uptimeNanoseconds - startTime!.uptimeNanoseconds
        let _ = Double(nanoDuration) / 1_000_000_000
        self.duration = (Double(round(10 * Double(nanoDuration) / 1_000_000_000)/10))
        print("문제 푸는데 걸린 시간은 \(duration) 초 입니다")
        
        // 서버에 정답 보내는 형태로 변환
        let answer = sendAnswerToServer()
        print("서버에 보낸 정답 정보는 \(answer) 입니다")
        isFirst = false
        
        //정답 정보 서버에 보내기 (최신꺼)
        let userSeq = self.myInfo?.userSeq ?? 0
        
        self.checkAlertNew(userSeq: Int(userSeq), problemSeq: self.problemSeq, duration: self.duration, questionId: self.questionId, userAnswer: answer, isFromCollection: self.isFromCollection, delegate: self).done{
            res in
            if res {
                //정오 상관없이 해설 보여줌.
                let vc = R.storyboard.preparation.preparationAnswerViewController()!
                vc.delegate = self
                vc.preparationAnswer = self.answer
                vc.collectionExplain = self.collectionExplain
                vc.isLastCollection = self.isLastCollection
                vc.cameFrom = self.cameFrom
                vc.unit2Seqs = self.unit2Seqs
                vc.keywordName = self.keywordName
                vc.number = self.number
                vc.problemSeq = self.problemSeq
                vc.questionId = self.questionId
                self.navigationController?.pushViewController(vc, animated: true)
                self.updateFirstQuestion()
            } else {
                let vc = R.storyboard.preparation.preparationErrorViewController()!
                vc.cameFromOnboard = self.cameFromOnboard
                vc.errorMessage = self.errorMessage
                vc.problemseqlb = "\(String(self.problemSeq))"
                vc.question_id = "\(String(self.questionId))"
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }.catch { err in
        }.finally {
        }
    }
    
    // MARK: - BottomSheet
    var bottomSheet: MDCBottomSheetController?
    func stisfactionOpen() {
        guard self.keywordSeqs.count > 0 else {
            return
        }

        let vc = R.storyboard.preparation.satisfactionViewController()!

        vc.problemSeq = self.problemSeq
        vc.keywordName = self.keywordName
        vc.keywordSeq = self.keywordSeqs[0]

        // MDC 바텀 시트로 설정
        bottomSheet = MDCBottomSheetController(contentViewController: vc)
        bottomSheet?.mdc_bottomSheetPresentationController?.preferredSheetHeight = 400
        // 아래로 드래그해도 안닫히게 하기
        bottomSheet?.dismissOnDraggingDownSheet = false
        bottomSheet?.dismissOnBackgroundTap = false
        bottomSheet?.delegate = self

        // 보여주기
        self.present(bottomSheet!, animated: true, completion: nil)
    }
    
    // MARK: - Toast
    func showToast(message: String) {
        var heMinus : CGFloat = 285.0
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.first
            let top = window?.safeAreaInsets.top
            let bottom = window?.safeAreaInsets.bottom
            heMinus += (bottom! - top!)
            
        } else if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let top = window?.safeAreaInsets.top
            let bottom = window?.safeAreaInsets.bottom
            heMinus += (bottom! - top!)
        }
        
        let width = self.view.frame.size.width
        let toastLabel = PaddingLabel(frame: CGRect(x: 16, y: self.view.frame.size.height - heMinus, width: width - 32, height: 44))
        toastLabel.paddingLeft = 16
        toastLabel.paddingRight = 16
        
        
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        toastLabel.textColor = UIColor.white
        toastLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        toastLabel.textAlignment = .left
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 8
        toastLabel.clipsToBounds = true
        
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 2.0, delay: 1.5, options: .curveEaseOut, animations: { toastLabel.alpha = 0.0 }, completion: {(isCompleted) in toastLabel.removeFromSuperview() })
    }
    
    // MARK: - AirBridge
    func setStartCameFrom() -> String {
        switch cameFrom {
        case "pulja2.0_onboard" :
            return "01"
        case "pulja2.0_today_continue_next" :
            return "02"
        case "pulja2.0_today_new_next" :
            return "03"
        case "pulja2.0_today_grade_next" :
            return "04"
        case "pulja2.0_today_solvedata_next" :
            return "05"
        case "pulja2.0_report_solvedata_next" :
            return "06"
        case "pulja2.0_today_nosolve_next" :
            return "07"
        case "pulja2.0_record_nosolve_next" :
            return "08"
        case "pulja2.0_collection_recent_next" :
            return "09"
        case "pulja2.0_collection_recommend_next" :
            return "10"
        case "pulja2.0_collection_total_next" :
            return "11"
        default :
            return "02"
        }
    }
    
    func sendABFinishEvent(){
        //ab180 이벤트 보내기
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: "pulja2.0_study_finish",
                    customs: ["userseq": userSeq, "user_id": userId, "number" : self.number, "keyword" : self.keywordName, "unit2List": self.unit2Seqs, "problemSeq": self.problemSeq, "cameFrom": self.cameFrom  ]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "category: pulja2.0_study_finish", separator: " ")
                print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                print("[ABLog]", "user_id: \(userId)", separator: " ")
                print("[ABLog]", "number: \(self.number)", separator: " ")
                print("[ABLog]", "keyword: \(self.keywordName)", separator: " ")
                print("[ABLog]", "unit2List: \(self.unit2Seqs)", separator: " ")
                print("[ABLog]", "problemSeq: \(self.problemSeq)", separator: " ")
                print("[ABLog]", "cameFrom: \(self.cameFrom)", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
        }
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension PreparationMultipleChoiceViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if isLandscape {
            // horizontal scrool disabled
            if svProblemImage.contentOffset.x != 0 {
                svProblemImage.contentOffset.x = 0
            }
            
            let imageWidth = ivProblem.intrinsicContentSize.width * svProblemImage.zoomScale
            let scrollWidth = svProblemImage.frame.width
            
            if imageWidth > scrollWidth {
                svProblemImage.setZoomScale(svProblemImage.zoomScale - 0.01, animated: false)
            }
        }
    }
}
