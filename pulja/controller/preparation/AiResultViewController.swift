//
//  AiResultViewController.swift
//  pulja
//
//  Created by 김병헌 on 2022/11/01.
//

import UIKit
import MaterialComponents.MaterialBottomSheet

class AiResultViewController: PuljaBaseViewController, MDCBottomSheetControllerDelegate {
    
    @IBOutlet weak var reportView: UIView!
    @IBOutlet weak var lbTopTitle1: UILabel!
    @IBOutlet weak var lbTopTitle2: UILabel!
    @IBOutlet weak var lbDuration: UILabel!
    @IBOutlet weak var kdcCount: KDCircularProgress!
    @IBOutlet weak var lbCorrectCount: UILabel!
    @IBOutlet weak var lbSolveCount: UILabel!
    @IBOutlet weak var btCorrecRate: UIButton!
    @IBOutlet weak var lbProposal1: UILabel!
    @IBOutlet weak var lbProposal2: UILabel!
    @IBOutlet weak var keywordView1: UIView!
    @IBOutlet weak var ivKeywordIcon1: UIImageView!
    @IBOutlet weak var lbKeword1: UILabel!
    @IBOutlet weak var keywordView2: UIView!
    @IBOutlet weak var ivKeywordIcon2: UIImageView!
    @IBOutlet weak var lbKeyword2: UILabel!
    
    @IBOutlet weak var keywordView3: UIView!
    @IBOutlet weak var ivKeywordIcon3: UIImageView!
    @IBOutlet weak var lbKeyword3: UILabel!
    
    @IBOutlet weak var keywordView4: UIView!
    @IBOutlet weak var ivKeywordIcon4: UIImageView!
    
    @IBOutlet weak var lbKeyword4: UILabel!
    @IBOutlet weak var btKeyword1: UIButton!
    @IBOutlet weak var btKeyword2: UIButton!
    @IBOutlet weak var btKeyword3: UIButton!
    @IBOutlet weak var btKeyword4: UIButton!
    
    @IBOutlet weak var kewordView: UIView!
    @IBOutlet weak var kewordViewHeight: NSLayoutConstraint!
    var problemSeq:Int?
    
    var recommendUnit : RecommendUnit? = nil
       
    var recentUnitYN = "N"
    
    
    var keywords:[KeywordData] = []
    
    var selectKeywords:[KeywordData] = []
    
    var keywordName : String = ""
    var unit2Seqs:[Int] = []

    var cameFromOnboard = false
    
    @IBOutlet var keywordArrViews: [UIView]!
    
    var isFromCollection = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reportView.isHidden = true
        if let problemSeq = self.problemSeq {
            
            LoadingView.show()
            
            if isFromCollection {
                kewordView.isHidden = true
                kewordViewHeight.constant = 0
                CollectionAPI.shared.collectionStudyResult(problemSeq: problemSeq).done { res in
                    if let data = res.data {
                        self.lbTopTitle1.text =  data.analysisText1
                        self.lbTopTitle2.text = data.analysisText2
                        if let totalDuration = data.totalDuration {
                            var min = Int(totalDuration / 60)
                            if min == 0 {
                                min = 1
                            }
                            self.lbDuration.text = "\(min)분 동안 풀었어요"
                        }
                        
                        if let studyCnt = data.studyCnt {
                            self.lbSolveCount.text = "/\(studyCnt)"
                        }
                        
                        if let correctCnt = data.correctCnt {
                            self.lbCorrectCount.text = "\(correctCnt)"
                        }
                        var correctRate = 0
                        if let studyCnt = data.studyCnt, studyCnt != 0, let correctCnt = data.correctCnt  {
                            correctRate = Int(100.0 * Double(correctCnt) / Double(studyCnt))
                            let correctAngle = correctRate * 360 / 100
                            self.kdcCount.animate(toAngle: Double(correctAngle), duration: 0.5, completion: nil)
                        }
                        
                        let attrs1 =  [NSAttributedString.Key.font : UIFont(name: "AppleSDGothicNeo-Regular", size: 14), NSAttributedString.Key.foregroundColor : UIColor.Purple]
                        let attrs2 = [NSAttributedString.Key.font : UIFont(name: "AppleSDGothicNeo-SemiBold", size: 14), NSAttributedString.Key.foregroundColor : UIColor.Purple]

                        let attributedString1 = NSMutableAttributedString(string:"정답률 ", attributes:attrs1)
                        let attributedString2 = NSMutableAttributedString(string:"\(correctRate)", attributes:attrs2)
                        let attributedString3 = NSMutableAttributedString(string:"%", attributes:attrs1)
                        attributedString1.append(attributedString2)
                        attributedString1.append(attributedString3)
                        
                        self.btCorrecRate.setAttributedTitle(attributedString1, for: .normal)
                        
                        self.reportView.isHidden = false
                    }
                }.catch { err in
                    LoadingView.hide()
                }.finally {
                    LoadingView.hide()
                }
            } else {
                CurationAPI.shared.ai3StudyResult(problemSeq: problemSeq).done { res in
                    if let data = res.data {
                        
                        self.lbTopTitle1.text =  data.analysisText1
                        self.lbTopTitle2.text = data.analysisText2
                        if let totalDuration = data.totalDuration {
                            var min = Int(totalDuration / 60)
                            if min == 0 {
                                min = 1
                            }
                            self.lbDuration.text = "\(min)분 동안 풀었어요"
                        }
                        
                        if let studyCnt = data.studyCnt {
                            self.lbSolveCount.text = "/\(studyCnt)"
                        }
                        
                        if let correctCnt = data.correctCnt {
                            self.lbCorrectCount.text = "\(correctCnt)"
                        }
                        var correctRate = 0
                        if let studyCnt = data.studyCnt, studyCnt != 0, let correctCnt = data.correctCnt  {
                            correctRate = Int(100.0 * Double(correctCnt) / Double(studyCnt))
                            let correctAngle = correctRate * 360 / 100
                            
                            self.kdcCount.animate(toAngle: Double(correctAngle), duration: 0.5, completion: nil);
                            
                        }
                        
                        let attrs1 =  [NSAttributedString.Key.font : UIFont(name: "AppleSDGothicNeo-Regular", size: 14), NSAttributedString.Key.foregroundColor : UIColor.Purple]
                        let attrs2 = [NSAttributedString.Key.font : UIFont(name: "AppleSDGothicNeo-SemiBold", size: 14), NSAttributedString.Key.foregroundColor : UIColor.Purple]

                        let attributedString1 = NSMutableAttributedString(string:"정답률 ", attributes:attrs1)
                        let attributedString2 = NSMutableAttributedString(string:"\(correctRate)", attributes:attrs2)
                        let attributedString3 = NSMutableAttributedString(string:"%", attributes:attrs1)
                        attributedString1.append(attributedString2)
                        attributedString1.append(attributedString3)
                        
                        
                        self.btCorrecRate.setAttributedTitle(attributedString1, for: .normal)
                        self.lbProposal1.text = data.proposalText1
                        self.lbProposal2.text = data.proposalText2
                        
                        self.reportView.isHidden = false
                        
                        if let keys = data.keywordList {
                            self.keywords = keys
                             self.keywordView1.isHidden = true
                             self.keywordView2.isHidden = true
                             self.keywordView3.isHidden = true
                             self.keywordView4.isHidden = true
                                                                                         
                            for row in 0..<keys.count {
                                if var displayName = keys[row].displayName {

                                    displayName = displayName.replacingOccurrences(of: "\\", with: "")
                                    displayName = displayName.replacingOccurrences(of: "n", with: "\n")

                                    var urlString = "https://pulja-contents.s3.ap-northeast-2.amazonaws.com/mobile_keyword/\(0)_result.png"
                                    if let keywordSeq = keys[row].keywordSeq {
                                        urlString = "https://pulja-contents.s3.ap-northeast-2.amazonaws.com/mobile_keyword/\(keywordSeq)_result.png"
                                    }
                                    
                                    let url = URL(string: urlString)

                                    if row == 0 {
                                        self.keywordView1.isHidden = false
                                        self.lbKeword1.text = displayName
                                        self.lbKeword1.setLineHeight(lineHeight: 1.2)
                                        self.btKeyword1.tag = row
                                        self.ivKeywordIcon1.kf.setImage(with: url)
                                    } else if row == 1 {
                                        self.keywordView2.isHidden = false
                                        self.lbKeyword2.text = displayName
                                        self.lbKeyword2.setLineHeight(lineHeight: 1.2)
                                        self.btKeyword2.tag = row
                                        self.ivKeywordIcon2.kf.setImage(with: url)
                                    } else if row == 2 {
                                        self.keywordView3.isHidden = false
                                        self.lbKeyword3.text = displayName
                                        self.lbKeyword3.setLineHeight(lineHeight: 1.2)
                                        self.btKeyword3.tag = row
                                        self.ivKeywordIcon3.kf.setImage(with: url)
                                    } else if row == 3 {
                                        self.keywordView4.isHidden = false
                                        self.lbKeyword4.text = displayName
                                        self.lbKeyword4.setLineHeight(lineHeight: 1.2)
                                        self.btKeyword4.tag = row
                                        self.ivKeywordIcon4.kf.setImage(with: url)
                                    }

                                }
                            }
                        }
                    }
                }.catch { err in
                    LoadingView.hide()
                }.finally {
                    LoadingView.hide()
                }
            }
        }
        
        CurationAPI.shared.recentUnit2().done { res in
            if let unitData =  res.data {
                self.recentUnitYN = "Y"
                self.recommendUnit = unitData
            } else {
                self.recentUnitYN = "N"
                CurationAPI.shared.recommendUnit2().done { recommendRes in
                    self.recommendUnit = recommendRes.data
                }
            }
            
        }
    }
    
    @IBAction func exitAction(_ sender: Any) {
        if isFromCollection {
            self.navigationController?.popViewController(animated: false)
        } else {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    
    @IBAction func explainAction(_ sender: Any) {
      
        //ab180 이벤트 보내기
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: "pulja2.0_report_review_next",
                    customs: ["userseq": userSeq, "user_id": userId, "keyword" : self.keywordName, "unit2List": self.unit2Seqs]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "category: pulja2.0_report_review_next", separator: " ")
                print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                print("[ABLog]", "user_id: \(userId)", separator: " ")
                print("[ABLog]", "keyword: \(self.keywordName)", separator: " ")
                print("[ABLog]", "unit2List: \(self.unit2Seqs)", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
        }
          
        if let resultVC = R.storyboard.review.problemResultViewController() {
            if let problemSeq = problemSeq {
                resultVC.problemSeq = problemSeq
                if isFromCollection {
                    resultVC.resultType = "n"
                }
            }
            self.navigationController?.pushViewController(resultVC, animated: true)
        }
    }
    
    
    @IBAction func keyword1Action(_ sender: UIButton) {
        //print("!!!1\(sender.tag)")
        DispatchQueue.main.async {
            self.setSelectKeyword(keyword: self.keywords[sender.tag], tagIdx: sender.tag)
        }
        
        changeUnit2()
    }
    
    
    @IBAction func keyword2Action(_ sender: UIButton) {
        //print("!!!2\(sender.tag)")
        DispatchQueue.main.async {
            self.setSelectKeyword(keyword: self.keywords[sender.tag], tagIdx: sender.tag)
        }
                
        changeUnit2()
    }
    

    @IBAction func keyword3Action(_ sender: UIButton) {
        //print("!!!3\(sender.tag)")
        DispatchQueue.main.async {
            self.setSelectKeyword(keyword: self.keywords[sender.tag], tagIdx: sender.tag)
        }
                
        changeUnit2()
    }
    
    @IBAction func keyword4Action(_ sender: UIButton) {
        //print("!!!4\(sender.tag)")
        DispatchQueue.main.async {
            self.setSelectKeyword(keyword: self.keywords[sender.tag], tagIdx: sender.tag)
        }
                
        changeUnit2()
    }
    
    func setSelectKeyword(keyword:KeywordData, tagIdx: Int){
        selectKeywords = []
        selectKeywords.append(keyword)
        
        //터치 이펙트 주기
        self.keywordArrViews[tagIdx].showAnimation {
            
        }
    }
    
    
    func changeUnit2() {

       let vc = R.storyboard.preparation.changeUnitViewController()!
           
       vc.keywords = self.selectKeywords
       vc.recentUnitYN = self.recentUnitYN
       vc.recommendUnit = self.recommendUnit
       vc.callback = { (type) in
           
           if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
               if let keyname = self.selectKeywords[0].name {
                   var u2SeqList:[Int] = []
                   if let unit2Seqs =  self.recommendUnit?.unit2SeqList {
                       u2SeqList = unit2Seqs
                   } else if let unit2Seq = self.recommendUnit?.unit2Seq {
                       u2SeqList.append(unit2Seq)
                   }
                   if Const.isAirBridge {
                       CommonUtil.shared.ABEvent(
                        category: "pulja2.0_report_solvedata_next",
                        customs: ["userseq": userSeq, "user_id": userId, "keyword" : keyname, "unit2List" : u2SeqList]
                       )
                   } else {
                       print("[ABLog]", "-----------------------------------------", separator: " ")
                       print("[ABLog]", "category: pulja2.0_report_solvedata_next", separator: " ")
                       print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                       print("[ABLog]", "user_id: \(userId)", separator: " ")
                       print("[ABLog]", "keyword: \(keyname)", separator: " ")
                       print("[ABLog]", "unit2List: \(u2SeqList)", separator: " ")
                       print("[ABLog]", "-----------------------------------------", separator: " ")
                   }
               }
           }
           
           if type == "change" {
               if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
                   if let keyname = self.selectKeywords[0].name {
                       
                       var unit2SeqList:[Int] = []
                       
                       if let unit2Seqs =  self.recommendUnit?.unit2SeqList {
                           unit2SeqList = unit2Seqs
                       } else if let unit2Seq = self.recommendUnit?.unit2Seq {
                           unit2SeqList.append(unit2Seq)
                       }
                       
                       if Const.isAirBridge {
                           CommonUtil.shared.ABEvent(
                            category: "pulja2.0_cat2modal_change_next",
                            customs: ["userseq": userSeq, "user_id": userId, "keyword" : keyname, "unit2List": unit2SeqList]
                           )
                       } else {
                           print("[ABLog]", "-----------------------------------------", separator: " ")
                           print("[ABLog]", "category: pulja2.0_cat2modal_change_next", separator: " ")
                           print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                           print("[ABLog]", "user_id: \(userId)", separator: " ")
                           print("[ABLog]", "keyword: \(keyname)", separator: " ")
                           print("[ABLog]", "unit2List: \(unit2SeqList)", separator: " ")
                           print("[ABLog]", "-----------------------------------------", separator: " ")
                       }
                   }
               }
               
               let vc = R.storyboard.preparation.preparationUnitSelectViewController()!
               if let keyname = self.selectKeywords[0].name { vc.keyName = keyname }
               vc.cameFrom = "pulja2.0_report_solvedata_next"
               vc.selectKeywordList = self.selectKeywords
               self.navigationController?.pushViewController(vc, animated: true)

           } else if type == "start" {
               
               var unit2SeqList:[Int] = []
               
               if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
                   if let keyname = self.selectKeywords[0].name {
                       
                       if let unit2Seqs =  self.recommendUnit?.unit2SeqList {
                           unit2SeqList = unit2Seqs
                       } else if let unit2Seq = self.recommendUnit?.unit2Seq {
                           unit2SeqList.append(unit2Seq)
                       }
                       
                       if Const.isAirBridge {
                           CommonUtil.shared.ABEvent(
                            category: "pulja2.0_cat2modal_next",
                            customs: ["userseq": userSeq, "user_id": userId, "keyword" : keyname, "unit2List": unit2SeqList]
                           )
                       } else {
                           print("[ABLog]", "-----------------------------------------", separator: " ")
                           print("[ABLog]", "category: pulja2.0_cat2modal_next", separator: " ")
                           print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                           print("[ABLog]", "user_id: \(userId)", separator: " ")
                           print("[ABLog]", "keyword: \(keyname)", separator: " ")
                           print("[ABLog]", "unit2List: \(unit2SeqList)", separator: " ")
                           print("[ABLog]", "-----------------------------------------", separator: " ")
                       }
                   }
               }
               
               
               var selectKeywordSeqs:[Int] = []
               
               for keyword in self.selectKeywords{
                   selectKeywordSeqs.append(keyword.keywordSeq!)
               }
               
               let vc = R.storyboard.preparation.preparationMultipleChoiceViewController()!
               vc.unit2Seqs = unit2SeqList
               vc.cameFrom = "pulja2.0_report_solvedata_next"
               vc.keywordSeqs = selectKeywordSeqs
               if let keyname = self.selectKeywords[0].name {
                   vc.keywordName = keyname
               }
               self.navigationController?.pushViewController(vc, animated: true)

           }
           
       }
       
       
              
       // MDC 바텀 시트로 설정
       let bottomSheet = MDCBottomSheetController(contentViewController: vc)
       bottomSheet.mdc_bottomSheetPresentationController?.preferredSheetHeight = 270
       bottomSheet.delegate = self

       // 보여주기
       self.present(bottomSheet, animated: true, completion: nil)
              
    }
    
    
    
}
