//
//  KeywordViewController.swift
//  pulja
//
//  Created by 김병헌 on 2022/10/24.
//

import UIKit
import Kingfisher
import MaterialComponents.MaterialBottomSheet


class KeywordViewController: PuljaBaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, MDCBottomSheetControllerDelegate  {
    @IBOutlet weak var cvKeyword: UICollectionView!
    @IBOutlet weak var btNext: UIButton!
    @IBOutlet weak var btSkip: UIButton!
    @IBOutlet weak var btBack: UIButton!
    
    var keywords:[KeywordData] = []
    
    var selectKeywords:[KeywordData] = []
    
    var selectUnit2Seqs:[Int] = []
    
    var recommendUnit : RecommendUnit? = nil
    
    var recentUnitYN = "N"
    
    var isModal = false
    
    var cameFromOnboard = false
    
    var cameFrom = ""
    
    override func viewWillAppear(_ animated: Bool) {
            
        super.viewWillAppear(animated)
        
        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        CommonUtil.tabbarController?.tabBar.isHidden = true
        
        if isModal {
            btBack.isHidden = true
        }
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UserDefaults.standard.bool(forKey: "ud_keyword_icon_cache") == false {
            KingfisherManager.shared.cache.clearMemoryCache()
            KingfisherManager.shared.cache.clearDiskCache()
            KingfisherManager.shared.cache.cleanExpiredDiskCache()
            
            UserDefaults.standard.set(true, forKey: "ud_keyword_icon_cache")
            UserDefaults.standard.synchronize()
            
        }

        countCheck()
        
        cvKeyword.delegate = self
        cvKeyword.dataSource = self
        
        self.cvKeyword.register(KeywordCollectionViewCell.nib(), forCellWithReuseIdentifier: "KeywordCollectionViewCell")
        self.cvKeyword.register(KeywordCollectionReusableView.nib(), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "KeywordCollectionReusableView")
        
        CommonAPI.shared.keyWordList().done { res in
            
            self.keywords = res.data!
            self.cvKeyword.reloadData()
            self.configure()
            self.getUpdate()
        }
        
        CurationAPI.shared.recentUnit2().done { res in
            if let unitData =  res.data {
                self.recentUnitYN = "Y"
                self.recommendUnit = unitData
            } else {
                self.recentUnitYN = "N"
                CurationAPI.shared.recommendUnit2().done { recommendRes in
                    self.recommendUnit = recommendRes.data
                }
            }
        }
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
    }
    

    func configure() {
        cvKeyword.collectionViewLayout = CollectionViewLeftAlignFlowLayout()
          if let flowLayout = cvKeyword?.collectionViewLayout as? UICollectionViewFlowLayout {
              flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
            }
    }
    
    
    @IBAction func closeAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//              return 1
//       }
       
   // 헤더 사이즈 설정
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
       let width: CGFloat = collectionView.frame.width
       let height: CGFloat = 138
       return CGSize(width: width, height: height)
   }
   
   func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
              
       switch kind {
           case UICollectionView.elementKindSectionHeader:
               let headerview = cvKeyword.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "KeywordCollectionReusableView", for: indexPath) as! KeywordCollectionReusableView
               return headerview
           default:
               let headerview = cvKeyword.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "KeywordCollectionReusableView", for: indexPath) as! KeywordCollectionReusableView
               return headerview

       }
   

    }
       
       
   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return keywords.count
   }
   
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell = cvKeyword.dequeueReusableCell(withReuseIdentifier: "KeywordCollectionViewCell", for: indexPath) as! KeywordCollectionViewCell
       
       cell.keyword = keywords[indexPath.item]
       
       return cell
   }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = cvKeyword.dequeueReusableCell(withReuseIdentifier: "KeywordCollectionViewCell", for: indexPath) as! KeywordCollectionViewCell
        
        if let isDisable =  keywords[indexPath.item].isDisable, isDisable {
            return
        }
        
        
        if  keywords[indexPath.item].isSelect == nil || keywords[indexPath.item].isSelect == false {
            
//            if countCheck() > 0 {
//                self.showToast(message: "키워드는 최대 1개까지 고를 수 있어요.")
//                return
//            }

            
            keywords[indexPath.item].isSelect = true
        } else {
            keywords[indexPath.item].isSelect = false
        }
        
        for row in 0..<keywords.count {
            if row !=  indexPath.item {
                keywords[row].isSelect = false
            }
        }
        
//        if countCheck() == 1 {
//            self.showToast(message: "키워드는 최대 1개까지 고를 수 있어요.")
//        }
        countCheck()
        disableCheck( keyword: keywords[indexPath.item])
        
        self.cvKeyword.reloadData()
    }
    
    
    
    @IBAction func nextAction(_ sender: Any) {
        guard countCheck() > 0  else {
            return
        }
        
        selectKeyword()

        let vc = R.storyboard.preparation.changeUnitViewController()!
            
        vc.keywords = self.selectKeywords
        if let keyname = self.selectKeywords[0].name { vc.keyName = keyname }
        vc.recentUnitYN = self.recentUnitYN
        vc.recommendUnit = self.recommendUnit
        
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            if let keyname = self.selectKeywords[0].name {
                if Const.isAirBridge {
                    CommonUtil.shared.ABEvent(
                        category: "pulja2.0_keyword_next",
                        customs: ["userseq": userSeq, "user_id": userId, "keyword" : keyname]
                    )
                } else {
                    print("[ABLog]", "-----------------------------------------", separator: " ")
                    print("[ABLog]", "category: pulja2.0_keyword_next", separator: " ")
                    print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                    print("[ABLog]", "user_id: \(userId)", separator: " ")
                    print("[ABLog]", "keyword: \(keyname)", separator: " ")
                    print("[ABLog]", "-----------------------------------------", separator: " ")
                }
            }
        }
        
        vc.callback = { (type) in
            
            if type == "change" {
                
                if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
                   if let keyname = self.selectKeywords[0].name {
                       
                       var unit2SeqList:[Int] = []
                       
                       if let unit2Seqs =  self.recommendUnit?.unit2SeqList {
                           unit2SeqList = unit2Seqs
                       }
                       else if let unit2Seq = self.recommendUnit?.unit2Seq {
                           unit2SeqList.append(unit2Seq)
                       }
                       
                       if Const.isAirBridge {
                           CommonUtil.shared.ABEvent(
                            category: "pulja2.0_cat2modal_change_next",
                            customs: ["userseq": userSeq, "user_id": userId, "keyword" : keyname, "unit2List": unit2SeqList]
                           )
                       } else {
                           print("[ABLog]", "-----------------------------------------", separator: " ")
                           print("[ABLog]", "pulja2.0_cat2modal_change_next", separator: " ")
                           print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                           print("[ABLog]", "user_id: \(userId)", separator: " ")
                           print("[ABLog]", "keyword: \(keyname)", separator: " ")
                           print("[ABLog]", "unit2List: \(unit2SeqList)", separator: " ")
                           print("[ABLog]", "-----------------------------------------", separator: " ")
                       }
                   }
               }
                
                let vc = R.storyboard.preparation.preparationUnitSelectViewController()!
                if let keyname = self.selectKeywords[0].name { vc.keyName = keyname }
                vc.cameFromOnboard = self.cameFromOnboard
                vc.cameFrom = self.cameFrom
                vc.selectKeywordList = self.selectKeywords
                vc.closeCallback = { (t) in
                    self.nextAction(self)
                }

                
                self.navigationController?.pushViewController(vc, animated: true)

            } else if type == "start" {
                
                var unit2SeqList:[Int] = []
                
                if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
                    if let keyname = self.selectKeywords[0].name {
                        
                        if let unit2Seqs =  self.recommendUnit?.unit2SeqList {
                            unit2SeqList = unit2Seqs
                        } else if let unit2Seq = self.recommendUnit?.unit2Seq {
                            unit2SeqList.append(unit2Seq)
                        }
                        
                        if Const.isAirBridge {
                            CommonUtil.shared.ABEvent(
                                category: "pulja2.0_cat2modal_next",
                                customs: ["userseq": userSeq, "user_id": userId, "keyword" : keyname, "unit2List": unit2SeqList]
                            )
                        } else {
                            print("[ABLog]", "-----------------------------------------", separator: " ")
                            print("[ABLog]", "category: pulja2.0_cat2modal_next", separator: " ")
                            print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                            print("[ABLog]", "user_id: \(userId)", separator: " ")
                            print("[ABLog]", "keyword: \(keyname)", separator: " ")
                            print("[ABLog]", "unit2List: \(unit2SeqList)", separator: " ")
                            print("[ABLog]", "-----------------------------------------", separator: " ")
                        }
                    }
                }
                
                var selectKeywordSeqs:[Int] = []
                
                for keyword in self.selectKeywords{
                    selectKeywordSeqs.append(keyword.keywordSeq!)
                }
                
                let vc = R.storyboard.preparation.preparationMultipleChoiceViewController()!
                vc.cameFromOnboard = self.cameFromOnboard
                vc.unit2Seqs = unit2SeqList
                vc.keywordSeqs = selectKeywordSeqs
                vc.cameFrom = self.cameFrom
                if let keyname = self.selectKeywords[0].name {
                    vc.keywordName = keyname
                }
                self.navigationController?.pushViewController(vc, animated: true)

            }
            
        }
        
        
               
        // MDC 바텀 시트로 설정
        let bottomSheet = MDCBottomSheetController(contentViewController: vc)
        bottomSheet.mdc_bottomSheetPresentationController?.preferredSheetHeight = 270
        bottomSheet.delegate = self

        // 보여주기
        self.present(bottomSheet, animated: true, completion: nil)
       
    }
    
    @IBAction func skipKeyword(_ sender: Any) {
        
        guard countCheck() == 0  else {
                   return
        }
        
        let vc = R.storyboard.preparation.changeUnitViewController()!
        
        vc.keyName = "키워드 선택 건너뛰기"
       vc.recentUnitYN = self.recentUnitYN
       vc.recommendUnit = self.recommendUnit
    
        //ab180 이벤트 보내기
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: "pulja2.0_keyword_next",
                    customs: ["userseq": userSeq, "user_id": userId, "keyword" : "키워드 선택 건너뛰기"]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "category: 0_keyword_next", separator: " ")
                print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                print("[ABLog]", "user_id: \(userId)", separator: " ")
                print("[ABLog]", "keyword: 키워드 선택 건너뛰기", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
        }
        
    
       vc.callback = { (type) in
           if type == "change" {
               
               if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
                   
                   var unit2SeqList:[Int] = []
                   if let unit2Seqs =  self.recommendUnit?.unit2SeqList {
                       unit2SeqList = unit2Seqs
                   } else if let unit2Seq = self.recommendUnit?.unit2Seq {
                       unit2SeqList.append(unit2Seq)
                   }
                   
                   if Const.isAirBridge {
                       CommonUtil.shared.ABEvent(
                        category: "pulja2.0_cat2modal_change_next",
                        customs: ["userseq": userSeq, "user_id": userId, "keyword" : "키워드 선택 건너뛰기", "unit2List": unit2SeqList]
                       )
                   } else {
                       print("[ABLog]", "-----------------------------------------", separator: " ")
                       print("[ABLog]", "category: pulja2.0_cat2modal_change_next", separator: " ")
                       print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                       print("[ABLog]", "user_id: \(userId)", separator: " ")
                       print("[ABLog]", "keyword: 키워드 선택 건너뛰기", separator: " ")
                       print("[ABLog]", "unit2List: \(unit2SeqList)", separator: " ")
                       print("[ABLog]", "-----------------------------------------", separator: " ")
                   }
               }
               
               let vc = R.storyboard.preparation.preparationUnitSelectViewController()!
               vc.keyName = "키워드 선택 건너뛰기"
               vc.cameFromOnboard = self.cameFromOnboard
               vc.cameFrom = self.cameFrom
               vc.selectKeywordList = self.selectKeywords
               vc.closeCallback = { (t) in
                   self.skipKeyword(self)
               }
               
               self.navigationController?.pushViewController(vc, animated: true)

           } else if type == "start" {
               
               var unit2SeqList:[Int] = []
               
               if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
                   
                   if let unit2Seqs =  self.recommendUnit?.unit2SeqList {
                       unit2SeqList = unit2Seqs
                   } else if let unit2Seq = self.recommendUnit?.unit2Seq {
                       unit2SeqList.append(unit2Seq)
                   }
                   
                   if Const.isAirBridge {
                       CommonUtil.shared.ABEvent(
                        category: "pulja2.0_cat2modal_next",
                        customs: ["userseq": userSeq, "user_id": userId, "keyword" : "키워드 선택 건너뛰기", "unit2List": unit2SeqList]
                       )
                   } else {
                       print("[ABLog]", "-----------------------------------------", separator: " ")
                       print("[ABLog]", "category: pulja2.0_cat2modal_next", separator: " ")
                       print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                       print("[ABLog]", "user_id: \(userId)", separator: " ")
                       print("[ABLog]", "keyword: 키워드 선택 건너뛰기", separator: " ")
                       print("[ABLog]", "unit2List: \(unit2SeqList)", separator: " ")
                       print("[ABLog]", "-----------------------------------------", separator: " ")
                   }
               }
               
               let vc = R.storyboard.preparation.preparationMultipleChoiceViewController()!
               vc.cameFromOnboard = self.cameFromOnboard
               vc.unit2Seqs = unit2SeqList
               vc.keywordName = "키워드 선택 건너뛰기"
               vc.cameFrom = self.cameFrom
               
               self.navigationController?.pushViewController(vc, animated: true)

           }
           
       }
       
       // MDC 바텀 시트로 설정
       let bottomSheet = MDCBottomSheetController(contentViewController: vc)
       bottomSheet.mdc_bottomSheetPresentationController?.preferredSheetHeight = 270
       bottomSheet.delegate = self

       // 보여주기
       self.present(bottomSheet, animated: true, completion: nil)
              
    }
    
    
    func selectKeyword(){
           selectKeywords = []
           for row in 0..<keywords.count {
              if keywords[row].isSelect ?? false {
                  selectKeywords.append(keywords[row])
              }
           }
           
       }
    
    func countCheck() -> Int {
        var count = 0
        for row in 0..<keywords.count {
           if keywords[row].isSelect ?? false {
               count = count + 1
           }
       }
        
        if count > 0 {
            btNext.setTitleColor(UIColor.white, for: .normal)
            btNext.backgroundColor = UIColor.Purple
            btSkip.setTitleColor(UIColor.Foggy, for: .normal)
            btSkip.setImageTintColor(UIColor.Foggy)
            btSkip.isUserInteractionEnabled = false
        } else {
           btNext.setTitleColor(UIColor.Foggy, for: .normal)
           btNext.backgroundColor = UIColor.Foggy40
            btSkip.setTitleColor(UIColor.Dust, for: .normal)
            btSkip.setImageTintColor(UIColor.Dust)
            btSkip.isUserInteractionEnabled = true
//            btSkip.isHidden = false
        }
        
        
        return count
    }
    
   
    
    func disableCheck(keyword:KeywordData){
        
        let key = keyword.keywordGroup?.substring(from: 0, to: 0)
        let num = keyword.keywordGroup?.substring(from: 1, to: 1)
        
        let isCancel = false
        
        for row in 0..<keywords.count {
            let checkKey =  keywords[row].keywordGroup?.substring(from: 0, to: 0)
            let checkNum = keywords[row].keywordGroup?.substring(from: 1, to: 1)
            if key == checkKey, num == checkNum, keyword.keywordSeq != keywords[row].keywordSeq {
                if keywords[row].isSelect ?? false {
                    return
                }
            }
        }

        
        for row in 0..<keywords.count {
            let checkKey =  keywords[row].keywordGroup?.substring(from: 0, to: 0)
            let checkNum = keywords[row].keywordGroup?.substring(from: 1, to: 1)
            if key == checkKey {
                if num != checkNum {
                    if keyword.keywordSeq != keywords[row].keywordSeq {
                        keywords[row].isDisable = keyword.isSelect
                    }
                }
            }
        }

    }
    
    func showToast(message: String) {
            var heMinus : CGFloat = 110.0
            if #available(iOS 13.0, *) {
                let window = UIApplication.shared.windows.first
                let top = window?.safeAreaInsets.top
                let bottom = window?.safeAreaInsets.bottom
                heMinus += bottom!
                
            } else if #available(iOS 11.0, *) {
                let window = UIApplication.shared.keyWindow
                let top = window?.safeAreaInsets.top
                let bottom = window?.safeAreaInsets.bottom
                heMinus += bottom!
            }
        
            let width = self.view.frame.size.width
            let toastLabel = PaddingLabel(frame: CGRect(x: 16, y: self.view.frame.size.height - heMinus, width: width - 32, height: 44))
            toastLabel.paddingLeft = 16
            toastLabel.paddingRight = 16

            
            toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.8)
            toastLabel.textColor = UIColor.white
            toastLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
            toastLabel.textAlignment = .left
            toastLabel.text = message
            toastLabel.alpha = 1.0
            toastLabel.layer.cornerRadius = 8
            toastLabel.clipsToBounds = true
        
            self.view.addSubview(toastLabel)
            UIView.animate(withDuration: 2.0, delay: 1.5, options: .curveEaseOut, animations: { toastLabel.alpha = 0.0 }, completion: {(isCompleted) in toastLabel.removeFromSuperview() })
            
        }
    
    
    func getUpdate() {
        //큐레이션 업데이트 알람
        if !UserDefaults.standard.bool(forKey: "hasProblemSeq") {
            //알러트 띄어야 함
            self.customAlert().done { res in }
            .catch { err in }
            .finally {}
        }
    }
}



class CollectionViewLeftAlignFlowLayout: UICollectionViewFlowLayout {
    let cellSpacing: CGFloat = 6
 
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        self.minimumLineSpacing = 10.0
        self.sectionInset = UIEdgeInsets(top: 12.0, left: 16.0, bottom: 0.0, right: 16.0)
        let attributes = super.layoutAttributesForElements(in: rect)
        
        
 
        var leftMargin = sectionInset.left
        var maxY: CGFloat = -1.0
        let count = attributes?.count
        
        var cnt = 0
        
        attributes?.forEach { layoutAttribute in
            
            cnt = cnt + 1
            if count == cnt {
                return
            }
            
            if layoutAttribute.frame.origin.y >= maxY {
                leftMargin = sectionInset.left
            }
            layoutAttribute.frame.origin.x = leftMargin
            leftMargin += layoutAttribute.frame.width + cellSpacing
            maxY = max(layoutAttribute.frame.maxY, maxY)
        }
        return attributes
    }
}
