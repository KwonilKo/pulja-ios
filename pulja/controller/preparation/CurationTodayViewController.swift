//
//  CurationTodayViewController.swift
//  pulja
//
//  Created by 김병헌 on 2022/11/04.
//

import UIKit
import WebKit
import TagListView
import MaterialComponents.MaterialBottomSheet

class CurationTodayViewController: PuljaBaseViewController, UIScrollViewDelegate, TagListViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, MDCBottomSheetControllerDelegate {
    
    @IBOutlet weak var svContents: UIScrollView!
    
    // MARK: - Top Contents
    @IBOutlet weak var statusBarView: UIView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var titleLineView: UIView!
    @IBOutlet weak var graView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var lbUnit2: UILabel!
    @IBOutlet weak var unit2TagListView: UIView!
    @IBOutlet weak var tlUnit2: TagListView!
    @IBOutlet weak var btOtherUnit2: UIButton!
    @IBOutlet weak var topCardView: UIView!
    @IBOutlet weak var lbEmptyKeyword: UILabel!
    @IBOutlet weak var ivKeywordIcon: UIImageView!
    @IBOutlet weak var lbKeywordDisplay: UILabel!
    
    // MARK: - Collection View
    @IBOutlet weak var recentCollectionView: UIView!
    @IBOutlet weak var lbRecentCollection: UILabel!
    @IBOutlet weak var cvRecentCollection: UICollectionView!
    
    @IBOutlet weak var recommendCollectionView: UIView!
    @IBOutlet weak var lbCollectionView: UILabel!
    @IBOutlet weak var cvRecommendCollectionView: UICollectionView!
    @IBAction func collectionListAction(_ sender: Any) {
        let vc = R.storyboard.preparation.problemCollectionListViewController()!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBOutlet weak var recommentView: UIView!
    @IBOutlet weak var lbRecommend: UILabel!
    @IBOutlet weak var cvRecommendUserKeywords: UICollectionView!
    
    @IBOutlet weak var favoriteView: UIView!
    @IBOutlet weak var lbFavorite: UILabel!
    @IBOutlet weak var cvFavoriteKeywords: UICollectionView!
    
    var isOtherUnit2 = false
    var keywordSeq = 0
    var displayName = ""
    var unit2Seqs:[Int] = []
    var keywordSeqs:[Int] = []
    var selectKeywords:[KeywordData] = [] //사용자 선택 키워드
    var userKeywordList:[KeywordData] = [] //내 추천 키워드 목록
    var gradeKeywordList:[KeywordData] = [] //또래 인기 키워드 목록
    var recommendUnit : RecommendUnit? = nil
    var recentUnitYN = "N"
    
    private var recentCollectionList: [CollectionDTOList] = []
    private var recommendCollectionList: [CollectionDTOList] = []
    
    var alertController: UIAlertController? = nil

    // MARK: - Override
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topCardView.isHidden = true
        
        recentCollectionView.isHidden = true
        cvRecentCollection.delegate = self
        cvRecentCollection.dataSource = self
        cvRecentCollection.register(ProblemCollectionViewCell.nib(), forCellWithReuseIdentifier: "ProblemCollectionViewCell")
        
        recommendCollectionView.isHidden = true
        cvRecommendCollectionView.delegate = self
        cvRecommendCollectionView.dataSource = self
        cvRecommendCollectionView.register(ProblemCollectionViewCell.nib(), forCellWithReuseIdentifier: "ProblemCollectionViewCell")
        
        recommentView.isHidden = true
        favoriteView.isHidden = true
        
        svContents.delegate = self
        
        tlUnit2.delegate = self
        tlUnit2.textFont = UIFont(name:"AppleSDGothicNeo-Regular", size:14)!
        DispatchQueue.main.async {
            self.graView.applyGradient(isVertical: true, colorArray: [UIColor.Paleblue, UIColor.white])
        }
        //        self.graView.setGradient(color1: UIColor.Paleblue, color2: UIColor.white)
        
        cvRecommendUserKeywords.delegate = self
        cvRecommendUserKeywords.dataSource = self
        
        cvFavoriteKeywords.delegate = self
        cvFavoriteKeywords.dataSource = self
        
        self.cvRecommendUserKeywords.register(RecommendKeywordCollectionViewCell.nib(), forCellWithReuseIdentifier: "RecommendKeywordCollectionViewCell")
        self.cvFavoriteKeywords.register(RecommendKeywordCollectionViewCell.nib(), forCellWithReuseIdentifier: "RecommendKeywordCollectionViewCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(CurationTodayViewController.rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
        
        //ab180 이벤트 보내기
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: "pulja2.0_today_enter",
                    customs: ["userseq": userSeq, "user_id": userId]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "category: pulja2.0_today_enter", separator: " ")
                print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                print("[ABLog]", "user_id: \(userId)", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
        }
        
        //        // 웹뷰 다이얼로그를 생성합니다.
        //        self.alertController = UIAlertController(title: "", message: nil, preferredStyle: .alert)
        //
        //        let screenWidth = UIScreen.main.bounds.size.width
        //
        //        let contentController = WKUserContentController()
        //        let configuration = WKWebViewConfiguration()
        //        contentController.add(self, name: "buttonClick") //⭐️ 메시지핸들러명: setPushToken
        //        configuration.userContentController = contentController
        //
        //        // 웹뷰를 생성하고 추가합니다.
        //        let webView = WKWebView(frame: CGRect(x: (screenWidth - 300) / -2 + 20, y: -250, width: 300, height: 550), configuration: configuration) // 적절한 크기로 조절하세요.
        //        if let url = URL(string: "https://bridge.edupanion.kr/pullza/request-data-transfer") {
        //            let request = URLRequest(url: url)
        //            webView.load(request)
        //        }
        //
        //        self.alertController!.view?.addSubview(webView)
        //        present(self.alertController!, animated: true, completion: nil)
        
        
//        // 09.20 한빛님 코드 확인
//        // UserDefaults에서 저장된 날짜 가져오기
//        if let savedDate = UserDefaults.standard.object(forKey: "lastShownDate") as? Date {
//            // 현재 날짜와 저장된 날짜 비교
//            let currentDate = Date()
//            let calendar = Calendar.current
//
//            if !calendar.isDate(currentDate, inSameDayAs: savedDate) {
//                // 현재 날짜와 저장된 날짜가 다르면 팝업을 표시
//                showPopup()
//            }
//        } else {
//            // 저장된 날짜가 없는 경우에도 팝업을 표시
//            showPopup()
//        }
        
        
    }
    
    
    // 한빛님 팝업 컨트롤러 등장
    func showPopup() {
            
//        // 한빛님 코드
//        let customDialogVC = CustomDialogViewController()
//        customDialogVC.modalPresentationStyle = .overCurrentContext
//        present(customDialogVC, animated: true, completion: nil)
        
        // 권일 코드 추가
        self.customFinalAlert()
        
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    @objc func rotated() {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            
        } else {
            print("Portrait")
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        DispatchQueue.main.async {
            self.graView.applyGradient(isVertical: true, colorArray: [UIColor.Paleblue, UIColor.white])
        }
        
        //테블릿인지 아닌지 확인
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.cvRecommendUserKeywords.reloadData()
                self.cvFavoriteKeywords.reloadData()
//                self.cvRecentCollection.reloadData()
//                self.cvCollectionView.reloadData()
            }
            break
            //               self.ipadAnswerView(size : size)
        default:
            break
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Do any additional setup after loading the view.
        CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        CommonUtil.tabbarController?.tabBar.isHidden = false
        
        // 문제풀이 기록이 없는경우
        if !UserDefaults.standard.bool(forKey: "hasSolve") {
            let vc = R.storyboard.record.recordEmptyViewNavController()!
            CommonUtil.tabbarController?.viewControllers?[2] = vc
        }
        
        // self.btOtherUnit2.isHidden = false
        // 키워드 선택 기존사용자도 한번은 떠야함
        // 학년 선택이동된 사용자는 키워드선택으로 이동됨으로 여기서 뜨지 않음
        if UserDefaults.standard.bool(forKey: "hasProblemSeq") {
            // scroll to top
            svContents.setContentOffset(CGPointMake(0, 0), animated: false)
            cvRecentCollection.setContentOffset(CGPointMake(0, 0), animated: false)
            cvRecommendCollectionView.setContentOffset(CGPointMake(0, 0), animated: false)
            LoadingView.show()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
                self.initData()
            }
        } else {
            // 아직 키워드를 선택하지 않은 유저
            let vc = R.storyboard.preparation.keywordViewController()!
            vc.isModal = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
        // 데이터 이관 팝엄
        // 09.20 한빛님 코드 확인
        // UserDefaults에서 저장된 날짜 가져오기
        if let savedDate = UserDefaults.standard.object(forKey: "lastShownDate") as? Date {
            // 현재 날짜와 저장된 날짜 비교
            let currentDate = Date()
            let calendar = Calendar.current
            
            if !calendar.isDate(currentDate, inSameDayAs: savedDate) {
                // 현재 날짜와 저장된 날짜가 다르면 팝업을 표시
                showPopup()
            }
        } else {
            // 저장된 날짜가 없는 경우에도 팝업을 표시
            DispatchQueue.main.async{
                self.showPopup()
            }
        }
        
    }
    
    func initData() {
        if let userSeq = self.myInfo?.userSeq {
            CollectionAPI.shared.collectionToday(userSeq: Int(userSeq)).done { res in
                if let data = res.data {
                    
                    self.lbUnit2.text = data.unit2Text
                    
                    if let unit2Tags = data.unit2NameList, unit2Tags.count > 0 {
                        self.tlUnit2.isHidden = false
                        self.isOtherUnit2 = true
                        self.tlUnit2.removeAllTags()
                        for unit2Name in unit2Tags {
                            self.tlUnit2.addTag(unit2Name)
                        }
                        self.btOtherUnit2.setTitle("외 \(unit2Tags.count)개", for: .normal)
                        self.btOtherUnit2.isHidden = false
                        self.btOtherUnit2.tag = 0
                        let image = R.image.iconSolidCheveronDown16()!.withRenderingMode(.alwaysTemplate)
                        self.btOtherUnit2.setImage(image,  for: .normal)
                        
                    } else {
                        self.unit2TagListView.isHidden = true
                        self.btOtherUnit2.isHidden = true
                        self.tlUnit2.isHidden = true
                    }
                    
                    if let unit2SeqList = data.unit2SeqList {
                        self.unit2Seqs = unit2SeqList
                    }
                    
                    if let keywordList = data.keywordList, keywordList.count > 0 {
                        self.selectKeywords = keywordList
                        self.ivKeywordIcon.isHidden = false
                        self.lbKeywordDisplay.isHidden = false
                        self.lbEmptyKeyword.isHidden = true
                        
                        var urlString = "https://pulja-contents.s3.ap-northeast-2.amazonaws.com/mobile_keyword/\(0)_result.png"
                        
                        if let keywordSeq = keywordList[0].keywordSeq  {
                            self.keywordSeq =  keywordSeq
                            self.keywordSeqs = []
                            self.keywordSeqs.append(keywordSeq)
                            
                            urlString = "https://pulja-contents.s3.ap-northeast-2.amazonaws.com/mobile_keyword/\(keywordSeq)_result.png"
                        }
                        
                        let url = URL(string: urlString)
                        self.ivKeywordIcon.kf.setImage(with: url)
                        self.displayName = keywordList[0].displayName ?? ""
                        self.displayName = self.displayName.replacingOccurrences(of: "\\", with: "")
                        self.displayName = self.displayName.replacingOccurrences(of: "n", with: "\n")
                        self.lbKeywordDisplay.text = self.displayName
                    } else {
                        self.ivKeywordIcon.isHidden = true
                        self.lbKeywordDisplay.isHidden = true
                        self.lbEmptyKeyword.isHidden = false
                        
                        //초기화
                        self.selectKeywords = []
                    }
                    
                    if let userKeywordList = data.userKeywordList {
                        self.userKeywordList = userKeywordList
                        self.cvRecommendUserKeywords.reloadData()
                        self.recommentView.isHidden = false
                    } else {
                        self.recommentView.isHidden = true
                    }
                    
                    if let gradeKeywordList = data.gradeKeywordList {
                        self.gradeKeywordList = gradeKeywordList
                        self.cvFavoriteKeywords.reloadData()
                        self.favoriteView.isHidden = false
                    } else {
                        self.favoriteView.isHidden = true
                    }
                    
                    self.lbRecommend.text = data.userKeyword
                    self.lbFavorite.text = data.gradeKeyword
                    self.topCardView.fadeIn()
                    
                    if let recentStudyCollectionList = data.recentStudyCollectionList, recentStudyCollectionList.isNotEmpty {
                        self.recentCollectionList = recentStudyCollectionList
                        self.cvRecentCollection.reloadData()
                        self.recentCollectionView.isHidden = false
                    }
                    
                    if let userRecommendCollectionList = data.userRecommendCollectionList, userRecommendCollectionList.isNotEmpty {
                        self.recommendCollectionList = userRecommendCollectionList
                        self.cvRecommendCollectionView.reloadData()
                        self.recommendCollectionView.isHidden = false
                    }
                } else {
                    self.newStudy(isModal: true)
                }
            }.catch { err in
                LoadingView.hide()
            }.finally {
                LoadingView.hide()
                
                CommonAPI.shared.schoolUpdateInfo(userSeq: Int(userSeq)).done { res in
                    if let data = res.data {
                        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
                        CommonUtil.tabbarController?.tabBar.isHidden = true
                        CommonUtil().showHAlert(
                            title: "학년이 자동 업데이트 되었어요.",
                            message: "\(data)으로 변경됐어요.\n잘못된 정보라면 설정에서 변경해주세요.",
                            button: "확인",
                            subButton: "변경하기",
                            viewController: self
                        ) { isSend in
                            
                            CommonUtil.tabbarController?.bottomNavBar.isHidden = false
                            CommonUtil.tabbarController?.tabBar.isHidden = false
                            
                            if !isSend
                            {   
                                let vc = R.storyboard.setting.settingUserInfoViewController()!
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                    }
                }.catch { err in

                }.finally {

                }
            }
            
            CurationAPI.shared.recentUnit2().done { res in
                if let unitData =  res.data {
                    self.recentUnitYN = "Y"
                    self.recommendUnit = unitData
                } else {
                    self.recentUnitYN = "N"
                    CurationAPI.shared.recommendUnit2().done { recommendRes in
                        self.recommendUnit = recommendRes.data
                    }
                    .catch { err in
                        
                    }.finally {
                        
                    }
                }
            }.catch { err in
                
            }.finally {
                
            }
        }
    }
    
    @IBAction func showOtherUnit2(_ sender: UIButton) {
        
        guard self.unit2Seqs.count > 1 else {
            return
        }
        
        if btOtherUnit2.tag == 0 {
            self.btOtherUnit2.tintColor = UIColor.grey
            self.btOtherUnit2.tag = 1
            //            self.unit2TagListView.isHidden = false
            let image = R.image.iconSolidCheveronUp16()!.withRenderingMode(.alwaysTemplate)
            self.btOtherUnit2.setImage(image,  for: .normal)
            
            UIView.animate(withDuration:  0.35,
                           delay: 0.0,
                           usingSpringWithDamping: 0.9,
                           initialSpringVelocity: 1,
                           options: [],
                           animations: {
                self.unit2TagListView.isHidden = false
                self.stackView.layoutIfNeeded()
                
            },
                           
                           completion: nil
            )
            
        } else {
            self.btOtherUnit2.tintColor = UIColor.grey
            self.btOtherUnit2.tag = 0
            //            self.unit2TagListView.isHidden = true
            let image = R.image.iconSolidCheveronDown16()!.withRenderingMode(.alwaysTemplate)
            self.btOtherUnit2.setImage(image,  for: .normal)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.unit2TagListView.isHidden = true
            }
            
            
            UIView.animate(withDuration:  0.35,
                           delay: 0.0,
                           usingSpringWithDamping: 1.0,
                           initialSpringVelocity: 1,
                           options: [],
                           animations: {
                self.unit2TagListView.isHidden = true
                self.stackView.layoutIfNeeded()
                
            },
                           completion: nil
            )
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView == svContents {
            // paging
            let offsetY = scrollView.contentOffset.y
            let percentage = (offsetY)/80
            
            
            
            self.statusBarView.backgroundColor = UIColor.white.withAlphaComponent(percentage)
            self.titleView.backgroundColor =  UIColor.white.withAlphaComponent(percentage)
            self.titleLineView.backgroundColor = UIColor.Foggy40.withAlphaComponent(percentage)
        }
        
    }
    
    
    @IBAction func continueStudyAction(_ sender: Any) {
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            var keyname = "키워드 선택 건너뛰기"
            if self.selectKeywords.count > 0 , let name = self.selectKeywords[0].name {
                keyname = name
            }
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: "pulja2.0_today_continue_next",
                    customs: ["userseq": userSeq, "user_id": userId, "keyword" : keyname, "unit2List": self.unit2Seqs]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "category: pulja2.0_today_continue_next", separator: " ")
                print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                print("[ABLog]", "user_id: \(userId)", separator: " ")
                print("[ABLog]", "keyword: \(keyname)", separator: " ")
                print("[ABLog]", "unit2List: \(self.unit2Seqs)", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
        }
        
        continueStudy()
    }
    
    
    @IBAction func newStudyAction(_ sender: Any) {
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            var keyname = "키워드 선택 건너뛰기"
            if self.selectKeywords.count > 0 , let name = self.selectKeywords[0].name {
                keyname = name
            }
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: "pulja2.0_today_new_next",
                    customs: ["userseq": userSeq, "user_id": userId, "keyword" : keyname, "unit2List": self.unit2Seqs]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "category: pulja2.0_today_new_next", separator: " ")
                print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                print("[ABLog]", "user_id: \(userId)", separator: " ")
                print("[ABLog]", "keyword: \(keyname)", separator: " ")
                print("[ABLog]", "unit2List: \(self.unit2Seqs)", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
        }
        
        newStudy(isModal: false)
    }
    
    func continueStudy(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
        {
            let vc = R.storyboard.preparation.preparationMultipleChoiceViewController()!
            vc.cameFromOnboard = false
            vc.unit2Seqs = self.unit2Seqs
            vc.keywordSeqs = self.keywordSeqs
            vc.cameFrom = "pulja2.0_today_continue_next"
            
            var keyname = "키워드 선택 건너뛰기"
            if self.selectKeywords.count > 0 , let name = self.selectKeywords[0].name {
                keyname = name
                
            }
            vc.keywordName = keyname
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func newStudy(isModal:Bool){
        
        guard (self.navigationController?.viewControllers) != nil else {
            return
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
        {
            
            let vc = R.storyboard.preparation.keywordViewController()!
            vc.isModal = isModal
            vc.cameFrom = "pulja2.0_today_new_next"
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == cvRecommendUserKeywords {
            return  self.userKeywordList.count
        } else if collectionView == cvFavoriteKeywords{
            return self.gradeKeywordList.count
        } else if collectionView == cvRecentCollection{
            return self.recentCollectionList.count
        } else if collectionView == cvRecommendCollectionView{
            return self.recommendCollectionList.count
        } else {
            return 0
        }
    }
    
    let sectionInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            cvFavoriteKeywords.isScrollEnabled = false
            cvRecommendUserKeywords.isScrollEnabled = false
            let bounds = UIScreen.main.bounds
            let width = bounds.size.width
            let height = 142
            var itemCount = 0
            
            if collectionView ==  cvFavoriteKeywords {
                itemCount = self.gradeKeywordList.count
            } else if collectionView == cvRecommendUserKeywords {
                itemCount = self.userKeywordList.count
            } else if collectionView == cvRecentCollection {
                itemCount = self.recentCollectionList.count
            } else if collectionView == cvRecommendCollectionView {
                itemCount = self.recommendCollectionList.count
            }
            
            if collectionView == cvFavoriteKeywords || collectionView == cvRecommendUserKeywords {
                let itemsPerRow: CGFloat = CGFloat(itemCount)
                let widthPadding = sectionInsets.left * (itemsPerRow)
                let itemsPerColumn: CGFloat = 1
                let heightPadding = sectionInsets.top * (itemsPerColumn + 1)
                let cellWidth = (width - widthPadding) / itemsPerRow
                let cellHeight = (CGFloat(height) - heightPadding) / itemsPerColumn
                
                return CGSize(width: cellWidth, height: cellHeight)
            } else {
                return CGSize(width: 186, height: 224)
            }
        } else {
            cvRecommendUserKeywords.isScrollEnabled = true
            cvFavoriteKeywords.isScrollEnabled = true
            if collectionView == cvFavoriteKeywords || collectionView == cvRecommendUserKeywords {
                return CGSize(width: 136, height: 142)
            } else {
                return CGSize(width: 186, height: 224)
            }
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == cvRecommendUserKeywords {
            
            let cell = cvRecommendUserKeywords.dequeueReusableCell(withReuseIdentifier: "RecommendKeywordCollectionViewCell", for: indexPath) as! RecommendKeywordCollectionViewCell
            
            cell.keyword = userKeywordList[indexPath.item]
            return cell
        } else if collectionView == cvFavoriteKeywords{
            
            let cell = cvFavoriteKeywords.dequeueReusableCell(withReuseIdentifier: "RecommendKeywordCollectionViewCell", for: indexPath) as! RecommendKeywordCollectionViewCell
            
            cell.keyword = gradeKeywordList[indexPath.item]
            return cell
        } else if collectionView == cvRecentCollection {
            let cell = cvRecentCollection.dequeueReusableCell(withReuseIdentifier: "ProblemCollectionViewCell", for: indexPath) as! ProblemCollectionViewCell
            cell.collection = recentCollectionList[indexPath.item]
            return cell
        } else {
            let cell = cvRecommendCollectionView.dequeueReusableCell(withReuseIdentifier: "ProblemCollectionViewCell", for: indexPath) as! ProblemCollectionViewCell
            cell.collection = recommendCollectionList[indexPath.item]
            return cell
        }
    }
    
    func showBounce(cell : UICollectionViewCell?) {
        cell?.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.1,
                       delay: 0,
                       options: .curveLinear,
                       animations: {
            cell?.transform = CGAffineTransform.init(scaleX: 0.94, y: 0.94)
        }) {  (done) in
            UIView.animate(withDuration: 0.1,
                           delay: 0,
                           options: .curveLinear,
                           animations: {
                cell?.transform = CGAffineTransform.init(scaleX: 1, y: 1)
            }) {_ in
                cell?.isUserInteractionEnabled = true
            }
        }
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == cvRecommendUserKeywords {
            let cell = cvRecommendUserKeywords.cellForItem(at: indexPath)
            showBounce(cell: cell)
            
            guard self.userKeywordList.count > indexPath.item  else {
                return
            }
            
            self.recommendOpen(recommendType:"user", keyword:self.userKeywordList[indexPath.item])
        } else if collectionView == cvFavoriteKeywords {
            let cell = cvFavoriteKeywords.cellForItem(at: indexPath)
            showBounce(cell: cell)
            
            guard self.gradeKeywordList.count > indexPath.item  else {
                return
            }
            
            self.recommendOpen(recommendType:"grade", keyword:self.gradeKeywordList[indexPath.item])
        } else if collectionView == cvRecentCollection {
            let vc = R.storyboard.preparation.problemCollectionDetailViewController()!
            vc.collectionSeq = recentCollectionList[indexPath.item].collectionSeq ?? 0
            vc.cameFrom = "pulja2.0_collection_recent_next"
            airBrindgeCollectionEvent(category: vc.cameFrom, collectionSeq: vc.collectionSeq)
            self.navigationController?.pushViewController(vc, animated: true)
        } else if collectionView == cvRecommendCollectionView {
            let vc = R.storyboard.preparation.problemCollectionDetailViewController()!
            vc.collectionSeq = recommendCollectionList[indexPath.item].collectionSeq ?? 0
            vc.cameFrom = "pulja2.0_collection_recommend_next"
            airBrindgeCollectionEvent(category: vc.cameFrom, collectionSeq: vc.collectionSeq)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    private func airBrindgeCollectionEvent(category: String, collectionSeq: Int) {
        //ab180 이벤트 보내기
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: category,
                    customs: ["userseq": userSeq, "user_id": userId, "collectionSeq" : collectionSeq]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "category: \(category)", separator: " ")
                print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                print("[ABLog]", "user_id: \(userId)", separator: " ")
                print("[ABLog]", "collectionSeq: \(collectionSeq)", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
        }
    }
    
    func recommendOpen(recommendType:String, keyword:KeywordData){
        let keywordName =  keyword.name ?? ""
        let category = recommendType == "user" ? "pulja2.0_today_solvedata_next" : "pulja2.0_today_grade_next"
        
        //ab180 이벤트 보내기
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            var unit2SeqList:[Int] = []
            if let unit2Seqs =  self.recommendUnit?.unit2SeqList {
                unit2SeqList = unit2Seqs
            } else if let unit2Seq = self.recommendUnit?.unit2Seq {
                unit2SeqList.append(unit2Seq)
            }
            
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: category,
                    customs: ["userseq": userSeq, "user_id": userId, "keyword" :keywordName, "unit2List": unit2SeqList]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "category: \(category)", separator: " ")
                print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                print("[ABLog]", "user_id: \(userId)", separator: " ")
                print("[ABLog]", "keyword: \(keywordName)", separator: " ")
                print("[ABLog]", "unit2List: \(unit2SeqList)", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
        }
        
        let recommandKeywords:[KeywordData] = [keyword]
        
        let vc = R.storyboard.preparation.changeUnitViewController()!
        vc.keywords = recommandKeywords
        vc.recentUnitYN = self.recentUnitYN
        vc.recommendUnit = self.recommendUnit
        vc.callback = { (type) in
            if type == "change" {
                if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
                    
                    var unit2SeqList:[Int] = []
                    
                    if let unit2Seqs =  self.recommendUnit?.unit2SeqList {
                        unit2SeqList = unit2Seqs
                    } else if let unit2Seq = self.recommendUnit?.unit2Seq {
                        unit2SeqList.append(unit2Seq)
                    }
                    
                    //ab180 이벤트 보내기
                    if Const.isAirBridge {
                        CommonUtil.shared.ABEvent(
                            category: "pulja2.0_cat2modal_change_next",
                            customs: ["userseq": userSeq, "user_id": userId, "keyword" : keywordName, "unit2List": unit2SeqList]
                        )
                    } else {
                        print("[ABLog]", "-----------------------------------------", separator: " ")
                        print("[ABLog]", "category: pulja2.0_cat2modal_change_next", separator: " ")
                        print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                        print("[ABLog]", "user_id: \(userId)", separator: " ")
                        print("[ABLog]", "keyword: \(keywordName)", separator: " ")
                        print("[ABLog]", "unit2List: \(unit2SeqList)", separator: " ")
                        print("[ABLog]", "-----------------------------------------", separator: " ")
                    }
                }
                
                let vc = R.storyboard.preparation.preparationUnitSelectViewController()!
                vc.selectKeywordList = recommandKeywords
                vc.keyName = keywordName
                vc.cameFrom = category
                
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else if type == "start" {
                
                var unit2SeqList:[Int] = []
                
                if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
                    
                    if let unit2Seqs =  self.recommendUnit?.unit2SeqList {
                        unit2SeqList = unit2Seqs
                    } else if let unit2Seq = self.recommendUnit?.unit2Seq {
                        unit2SeqList.append(unit2Seq)
                    }
                    
                    //ab180 이벤트 보내기
                    if Const.isAirBridge {
                        CommonUtil.shared.ABEvent(
                            category: "pulja2.0_cat2modal_next",
                            customs: ["userseq": userSeq, "user_id": userId, "keyword" : keywordName, "unit2List": unit2SeqList]
                        )
                    } else {
                        print("[ABLog]", "-----------------------------------------", separator: " ")
                        print("[ABLog]", "category: pulja2.0_cat2modal_next", separator: " ")
                        print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                        print("[ABLog]", "user_id: \(userId)", separator: " ")
                        print("[ABLog]", "keyword: \(keywordName)", separator: " ")
                        print("[ABLog]", "unit2List: \(unit2SeqList)", separator: " ")
                        print("[ABLog]", "-----------------------------------------", separator: " ")
                    }
                }
                
                
                var selectKeywordSeqs:[Int] = []
                
                for keyword in recommandKeywords{
                    selectKeywordSeqs.append(keyword.keywordSeq!)
                }
                
                let vc = R.storyboard.preparation.preparationMultipleChoiceViewController()!
                vc.cameFromOnboard = false
                vc.cameFrom = category
                vc.unit2Seqs = unit2SeqList
                vc.keywordSeqs = selectKeywordSeqs
                vc.keywordName = keywordName
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        // MDC 바텀 시트로 설정
        let bottomSheet = MDCBottomSheetController(contentViewController: vc)
        bottomSheet.mdc_bottomSheetPresentationController?.preferredSheetHeight = 270
        bottomSheet.delegate = self
        
        // 보여주기
        self.present(bottomSheet, animated: true, completion: nil)
        
    }
    
//    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
//        self.alertController?.dismiss(animated: false, completion: nil)
//        let webViewController = UpdateWebviewController()
//        navigationController?.pushViewController(webViewController, animated: true)
//    }
}

//extension UIWindow {
//
//    public var visibleViewController: UIViewController? {
//        return self.visibleViewControllerFrom(vc: self.rootViewController)
//    }
//
//    /**
//     # visibleViewControllerFrom
//     - Author: suni
//     - Date:
//     - Parameters:
//        - vc: rootViewController 혹은 UITapViewController
//     - Returns: UIViewController?
//     - Note: vc내에서 가장 최상위에 있는 뷰컨트롤러 반환
//    */
//    public func visibleViewControllerFrom(vc: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
//        if let nc = vc as? UINavigationController {
//            return self.visibleViewControllerFrom(vc: nc.visibleViewController)
//        } else if let tc = vc as? UITabBarController {
//            return self.visibleViewControllerFrom(vc: tc.selectedViewController)
//        } else {
//            if let pvc = vc?.presentedViewController {
//                return self.visibleViewControllerFrom(vc: pvc)
//            } else {
//                return vc
//            }
//        }
//    }
//}
