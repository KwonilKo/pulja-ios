//
//  CustomDialogViewController.swift
//  pulja
//
//  Created by HANBIT KIM on 2023/09/13.
//

import Foundation
import UIKit
import WebKit

class CustomDialogViewController: UIViewController {
    
//    var alertController: UIAlertController? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let contentController = WKUserContentController()
//        contentController.add(self, name: "buttonClick")
        
//        let configuration = WKWebViewConfiguration()
//        configuration.userContentController = contentController
        
        let centerX = view.bounds.midX
        let centerY = view.bounds.midY
        
        let verticalStackView = UIStackView(frame: CGRect(x: 0, y: 0, width: 300, height: 600))
        verticalStackView.axis = .vertical
        verticalStackView.alignment = .center
        verticalStackView.spacing = 10 // 뷰 사이의 간격 설정
        verticalStackView.center = CGPoint(x: centerX, y: centerY)
        verticalStackView.backgroundColor = UIColor.white


        let webView = WKWebView(frame: CGRect(x: 0, y: 0, width: 300, height: 530))
            .setUiDelegate(self)
            .setNavigationDelegate(self)
            .setAllowsBackForwardNavigationGestures(true)
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        if #available(iOS 14.0, *) {
            webView.configuration.defaultWebpagePreferences.allowsContentJavaScript = true
        } else {
            webView.configuration.preferences.javaScriptEnabled = true
        }
        
        verticalStackView.addSubview(webView)
        
        if let url = URL(string: "https://bridge.edupanion.kr/pullza/request-data-transfer") {
            let request = URLRequest(url: url)
            webView.load(request)
        }
        
        let contentController = webView.configuration.userContentController
        contentController.add(self, name: "buttonClick")
        
        let screenWidth = UIScreen.main.bounds.width
        
        let stackView = UIView(frame: CGRect(x: 0, y: 540, width: 300, height: 30))
//        stackView.axis = .horizontal
//        stackView.spacing = 10
//        stackView.backgroundColor = UIColor.white
//        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        // 웹뷰의 아래에 추가할 체크박스 및 라벨 생성
        let checkBox = UISwitch()
        checkBox.addTarget(self, action: #selector(checkBoxToggled(_:)), for: .valueChanged)

        let label = UILabel(frame: CGRect(x: 55, y: 0, width: 200, height: 30))
        label.text = "오늘 하루 보지 않기"
        
        stackView.addSubview(checkBox)
        stackView.addSubview(label)
        
        verticalStackView.addSubview(stackView)
        
        view.addSubview(verticalStackView)
        
//        verticalStackView.translatesAutoresizingMaskIntoConstraints = false
//
//        NSLayoutConstraint.activate([
//            verticalStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
//            verticalStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
//        ])
        

//        // 웹뷰 다이얼로그를 생성합니다.
//        self.alertController = UIAlertController(title: "", message: nil, preferredStyle: .alert)
//
//        let screenWidth = UIScreen.main.bounds.size.width
//
//        let contentController = WKUserContentController()
//        let configuration = WKWebViewConfiguration()
//        contentController.add(self, name: "buttonClick") //⭐️ 메시지핸들러명: setPushToken
//        configuration.userContentController = contentController
//
//        // 웹뷰를 생성하고 추가합니다.
//        let webView = WKWebView(frame: CGRect(x: (screenWidth - 300) / -2 + 20, y: -250, width: 300, height: 550), configuration: configuration) // 적절한 크기로 조절하세요.
//        if let url = URL(string: "https://bridge.edupanion.kr/pullza/request-data-transfer") {
//            let request = URLRequest(url: url)
//            webView.load(request)
//        }
    }
    
    @objc func checkBoxToggled(_ sender: UISwitch) {
        if sender.isOn {
            // 체크박스가 선택된 경우, 오늘 날짜를 UserDefaults에 저장
            let currentDate = Date()
            UserDefaults.standard.set(currentDate, forKey: "lastShownDate")
        } else {
            // 체크박스가 선택 해제된 경우, UserDefaults에서 날짜를 삭제
            UserDefaults.standard.removeObject(forKey: "lastShownDate")
        }
    }
}

extension CustomDialogViewController: WKScriptMessageHandler, WKNavigationDelegate, WKUIDelegate {

    func userContentController(_ userContentController: WKUserContentController,didReceive message: WKScriptMessage) {
        let webViewController = UpdateWebviewController()
        present(webViewController, animated: true, completion: nil)
        self.dismiss(animated: false, completion: nil)
    }
}

extension WKWebView {

    @discardableResult func setUiDelegate(_ delegate: WKUIDelegate) -> Self {
        
        self.uiDelegate = delegate
        return self
    }
    
    @discardableResult func setNavigationDelegate(_ delegate: WKNavigationDelegate) -> Self {
        
        self.navigationDelegate = delegate
        return self
    }
    
    @discardableResult func setAllowsBackForwardNavigationGestures(_ value: Bool) -> Self {
        
        self.allowsBackForwardNavigationGestures = value
        return self
    }
    
    @discardableResult func configureScrollView(_ configure: @escaping (UIScrollView) -> Void) -> Self {
        
        configure(self.scrollView)
        return self
    }
    
    @discardableResult func setCookie(_ cookies: [[HTTPCookiePropertyKey: Any]]) -> Self {
            
        if !cookies.isEmpty {
            cookies.forEach {
                if let httpCookie = HTTPCookie(properties: $0) {
                    self.configuration.websiteDataStore.httpCookieStore.setCookie(httpCookie)
                }
            }
        }
        
        self.configuration.websiteDataStore.httpCookieStore.getAllCookies { cookies in
            cookies.forEach { cookie in
                print(cookie.description)
                print(cookie.name)
                print(cookie.value)
                print(cookie.domain)
                print(cookie.path)
                print("---")
            }
        }
        
        
        return self
    }
    
//    if !self.cookies.isEmpty {
//        self.cookies.forEach {
//            if let httpCookie = HTTPCookie(properties: $0) {
//                webView.configuration.websiteDataStore.httpCookieStore.setCookie(httpCookie)
//            }
//        }
//    }
}
