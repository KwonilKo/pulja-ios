//
//  PreparationErrorViewController.swift
//  pulja
//
//  Created by kwonilko on 2022/08/11.
//

import UIKit

class PreparationErrorViewController: PuljaBaseViewController {

    var cameFromOnboard = false
    
    
    @IBOutlet weak var errLb: UILabel!
    
    var errorMessage : String?
    
    
    @IBOutlet weak var problemSeqLb: UILabel!
    
    
    @IBOutlet weak var userSeqLb: UILabel!
    
    var question_id: String?
    var problemseqlb : String?
    var userseqlb : String?
    
    
    override func viewDidAppear(_ animated: Bool){
        super.viewDidAppear(animated)
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        //ab180이벤트 보내기
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: "pulja2.0_error_enter",
                    customs:["userseq" : userSeq,
                             "user_id" : userId,
                             "problemSeq" : self.problemseqlb ?? "",
                             "question_id" : self.question_id ?? "",
                             "errorMessage" : self.errorMessage ?? ""]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "category: pulja2.0_error_enter", separator: " ")
                print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                print("[ABLog]", "user_id: \(userId)", separator: " ")
                print("[ABLog]", "problemSeq: \(self.problemseqlb ?? "")", separator: " ")
                print("[ABLog]", "question_id: \(self.question_id ?? "")", separator: " ")
                print("[ABLog]", "errorMessage: \(self.errorMessage ?? "")", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
        }
        
        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        CommonUtil.tabbarController?.tabBar.isHidden = true
        
        errLb.text = "푸링이가 길을 잃어\n해당 페이지를 찾을 수 없어요.\n홈으로 이동해서 다시 진행해주세요."
        
        //내부 테스트 용 
//        if errorMessage != nil {
//            errLb.text = errorMessage
//            problemSeqLb.text = "problemSeq: \(self.problemseqlb), question_id: \(self.question_id ?? "")"
//            if let userSeq = self.myInfo?.userSeq{
//                userSeqLb.text = String(userSeq)
//            }
//        } else {
//            errLb.text = "푸링이가 길을 잃어\n해당 페이지를 찾을 수 없어요.\n홈으로 이동해서 다시 진행해주세요."
//        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        
        
        super.viewWillDisappear(animated)
        
        CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        CommonUtil.tabbarController?.tabBar.isHidden = false
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        
    }
    
    
    @IBAction func goHome(_ sender: Any) {
        
        
        if self.cameFromOnboard {
            if let vcDetail = R.storyboard.main.tabbarController() {
                self.navigationController?.setViewControllers([vcDetail], animated: true)
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//                    guard let userType = self.myInfo?.userType else { return }
//                    if userType != "p" {
//                        vcDetail.selectedIndex = 2
//                        vcDetail.bottomNavBar.selectedItem = vcDetail.bottomNavBar.items[2]
//                    }
//                }
            }
        } else {
            self.navigationController?.popToRootViewController(animated: true)
        }
        
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
