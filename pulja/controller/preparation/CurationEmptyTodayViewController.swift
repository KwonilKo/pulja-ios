//
//  CurationEmptyTodayViewController.swift
//  pulja
//
//  Created by HUN on 2022/12/15.
//

import UIKit
import MaterialComponents.MaterialBottomSheet

class CurationTodayEmptyViewController: PuljaBaseViewController, UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, MDCBottomSheetControllerDelegate {
    
    //MARK: - Main Container
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var statusBarView: UIView!
    @IBOutlet weak var titleLineView: UIView!
    
    //MARK: - ScrollView Contents
    @IBOutlet weak var svContents: UIScrollView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var topCardView: UIView!
    @IBOutlet weak var recommendView: UIView!
    @IBOutlet weak var cvRecommendEmptyKeyword: UICollectionView!
    
    //MARK: -
    private var emptyKeywordList:[String] = [
        "각잡고\n풀 만한 문제",
        "유형 익히기 문제",
        "도전해\n볼 만한 문제",
        "수학 자신감을\n주는 문제"
    ]
    private var selectedUnit = ""
    private var selectedKeywordURL: String?
    private var selectedKeyword: String?
    private var selectedKeywordName: String?
    private var selectedKeywordList: [KeywordData]?
    private var unit2SeqList: [Int]?
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    //MARK: - Override
    override func viewDidLoad() {
        super.viewDidLoad()
        
        svContents.delegate = self
        
        DispatchQueue.main.async {
            self.gradientView.applyGradient(isVertical: true, colorArray: [UIColor.Paleblue, UIColor.white])
        }
        
        cvRecommendEmptyKeyword.delegate = self
        cvRecommendEmptyKeyword.dataSource = self
        cvRecommendEmptyKeyword.register(RecommendEmptyViewCell.nib(), forCellWithReuseIdentifier: "RecommendEmptyViewCell")
        cvRecommendEmptyKeyword.reloadData()
        
        //ab180 이벤트 보내기
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: "pulja2.0_today_nosolve_enter",
                    customs: ["userseq": userSeq, "user_id": userId]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "category: pulja2.0_today_nosolve_enter", separator: " ")
                print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                print("[ABLog]", "user_id: \(userId)", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(CurationTodayEmptyViewController.rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        // Background Gradient
        DispatchQueue.main.async {
            self.gradientView.applyGradient(isVertical: true, colorArray: [UIColor.Paleblue, UIColor.white])
        }
        
        // Check iPad
        if (UIDevice.current.userInterfaceIdiom == .pad) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.cvRecommendEmptyKeyword.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        CommonUtil.tabbarController?.tabBar.isHidden = false
        
        if UserDefaults.standard.bool(forKey: "hasSolve") {
            let vc1 = R.storyboard.preparation.curationViewController()!
            let vc2 = R.storyboard.review.reviewViewController()!
            let vc3 = R.storyboard.record.recordViewController()!
            let vc4 = R.storyboard.setting.settingViewController()!
            CommonUtil.tabbarController?.viewControllers = [vc1, vc2, vc3, vc4]
        } else {
            initData()
            
            // scroll to top
            svContents.setContentOffset(CGPointMake(0, 0), animated: false)
        }
    }
    
    @objc func rotated() {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            
        } else {
            print("Portrait")
        }
    }
    
    //MARK: - Functions
    func initData() {
        if let userSeq = self.myInfo?.userSeq {
            CollectionAPI.shared.collectionToday(userSeq: Int(userSeq)).done { res in
                if let data = res.data {
                    
                    // 선택한 키워드
                    if let keywordList = data.keywordList, keywordList.count > 0 {
                        self.selectedKeywordList = keywordList
                        self.selectedKeywordName = keywordList[0].name
                        self.selectedKeyword = keywordList[0].displayName
                        let keywordSeq = keywordList[0].keywordSeq ??  0
                        self.selectedKeywordURL = "https://pulja-contents.s3.ap-northeast-2.amazonaws.com/mobile_keyword/\(keywordSeq).png"
                    }
                    
                    // 선택 중단원
                    let unit2Text = data.unit2Text ?? ""
                    if let unit2Tags = data.unit2NameList, unit2Tags.count > 0 {
                        self.selectedUnit = "\(unit2Text) 외 \(unit2Tags.count)개"
                    } else {
                        self.selectedUnit = unit2Text
                    }
                    
                    self.unit2SeqList = data.unit2SeqList
                    
                    
                    // recordEmptyViewController에서 사용
                    if let tabBar = CommonUtil.tabbarController {
                        tabBar.selectedUnit = self.selectedUnit
                        tabBar.selectedKeywordURL = self.selectedKeywordURL
                        tabBar.selectedKeyword = self.selectedKeyword
                        tabBar.selectedKeywordName = self.selectedKeywordName
                        tabBar.selectedKeywordList = self.selectedKeywordList
                        tabBar.unit2SeqList = self.unit2SeqList
                    }
                }   
            }
            .catch { err in
                
            }.finally {
                
            }
        }
    }
    
    func showBottomSheet() {
        let vc = R.storyboard.preparation.changeEmptyUnitViewController()!
        vc.selectedUnit = self.selectedUnit
        vc.selectedKeyword = self.selectedKeywordName
        vc.selectedKeywordURL = self.selectedKeywordURL
        
        vc.callback = { (type) in
            if type == "change" {
                let vc = R.storyboard.preparation.keywordViewController()!
                vc.cameFrom = "pulja2.0_today_nosolve_next"
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if type == "start" {
                
                var selectKeywordSeqs:[Int] = []
                
                if self.selectedKeywordList?.count ?? 0 > 0 {
                    for keyword in self.selectedKeywordList! {
                        selectKeywordSeqs.append(keyword.keywordSeq!)
                    }
                }
                
                var unit2Seq:[Int] = []
                if self.unit2SeqList?.count ?? 0 > 0 {
                    for seq in self.unit2SeqList! {
                        unit2Seq.append(seq)
                    }
                }
                
                let keywordName = self.selectedKeywordName ?? ""
                
                let vc = R.storyboard.preparation.preparationMultipleChoiceViewController()!
                vc.cameFromOnboard = false
                vc.cameFrom = "pulja2.0_today_nosolve_next"
                vc.unit2Seqs = unit2Seq
                vc.keywordSeqs = selectKeywordSeqs
                vc.keywordName = keywordName

                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        // MDC 바텀 시트로 설정
        let bottomSheet = MDCBottomSheetController(contentViewController: vc)
        bottomSheet.mdc_bottomSheetPresentationController?.preferredSheetHeight = 340
        bottomSheet.delegate = self
        
        // 보여주기
        self.present(bottomSheet, animated: true, completion: nil)
    }
    
    /* Actions */
    @IBAction func clickProblemButton(_ sender: Any) {
        showBottomSheet()
    }
    
    /* Scrollview */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == svContents {
            // paging
            let offsetY = scrollView.contentOffset.y
            let percentage = (offsetY)/80
            
            self.statusBarView.backgroundColor = UIColor.white.withAlphaComponent(percentage)
            self.titleView.backgroundColor =  UIColor.white.withAlphaComponent(percentage)
            self.titleLineView.backgroundColor = UIColor.Foggy40.withAlphaComponent(percentage)
        }
    }
    
    /* Collection View */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = cvRecommendEmptyKeyword.dequeueReusableCell(withReuseIdentifier: "RecommendEmptyViewCell", for: indexPath) as! RecommendEmptyViewCell
        
        cell.keyword = emptyKeywordList[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.emptyKeywordList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 12
    }
    
    let sectionInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if UIDevice.current.userInterfaceIdiom == .pad {
            cvRecommendEmptyKeyword.isScrollEnabled = false
            let bounds = UIScreen.main.bounds
            
            let width = bounds.size.width
            
            let height = 142
            
            var itemCount = 0
            if collectionView == cvRecommendEmptyKeyword {
                itemCount = self.emptyKeywordList.count
            }
            let itemsPerRow: CGFloat = CGFloat(itemCount)
            let widthPadding = sectionInsets.left * (itemsPerRow)
            let itemsPerColumn: CGFloat = 1
            let heightPadding = sectionInsets.top * (itemsPerColumn + 1)
            let cellWidth = (width - widthPadding) / itemsPerRow
            let cellHeight = (CGFloat(height) - heightPadding) / itemsPerColumn
            
            return CGSize(width: cellWidth, height: cellHeight)
            
        } else {
            cvRecommendEmptyKeyword.isScrollEnabled = true
            return CGSize(width: 136, height: 142)
        }
    }
}
