//
//  PreparationUnitSelectViewController.swift
//  pulja
//
//  Created by HUN on 2023/01/25.
//

import UIKit

class PreparationUnitSelectViewController: PuljaBaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - Back Button
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Dropdown View
    @IBOutlet weak var dropdownView: UIView!
    @IBOutlet weak var dropdownStackViewHeight: NSLayoutConstraint!
    @IBOutlet weak var dropdownStackView: UIStackView!
    @IBOutlet weak var dropdownButton: UIView!
    @IBOutlet weak var dropdownButtonLabel: UILabel!
    @IBOutlet weak var dropdownButtonImage: UIImageView!
    @IBOutlet weak var dropdownButton1: UIView!
    @IBOutlet weak var dropdownButton2: UIView!
    @IBOutlet weak var dropdownButtonLabel1: UILabel!
    @IBOutlet weak var dropdownButtonLabel2: UILabel!
    @IBOutlet weak var closeDropdownView: UIView!
    private var selectedSchoolIndex = 0 // 0 -> 고등, 1 -> 중등
    private var selectedSubjectIndex = 0
    
    private func setHeaderView() {
        dropdownView.isHidden = true
        
        //중고등 구분
        if selectedSchoolIndex == 0 {
            dropdownButtonLabel1.textColor = UIColor.Foggy
            dropdownButtonLabel2.textColor = UIColor.black
            dropdownButtonLabel.text = "고등수학"
        } else {
            dropdownButtonLabel1.textColor = UIColor.black
            dropdownButtonLabel2.textColor = UIColor.Foggy
            dropdownButtonLabel.text = "중학수학"
        }
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.clickDropdown(_:)))
        dropdownButton.addGestureRecognizer(gesture)
        
        let gesture1 = UITapGestureRecognizer(target: self, action: #selector(self.clickDropdown1(_:)))
        dropdownButton1.addGestureRecognizer(gesture1)
        
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(self.clickDropdown2(_:)))
        self.dropdownButton2.addGestureRecognizer(gesture2)
        
        let gesture3 = UITapGestureRecognizer(target: self, action: #selector(self.closeDropdown(_:)))
        self.closeDropdownView.addGestureRecognizer(gesture3)
    }
    
    private func showDropdown() {
        dropdownButtonImage.image = UIImage(named: "iconSolidCheveronUp16")
        dropdownView.isHidden = false
        dropdownView.alpha = 0.0
        dropdownStackView.transform = CGAffineTransform(translationX: 0, y: -144)
        dropdownStackView.alpha = 0.0
        dropdownStackViewHeight.constant = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.dropdownView.alpha = 1.0
            self.dropdownStackView.transform = CGAffineTransform(translationX: 0, y: 0)
            self.dropdownStackView.alpha = 1.0
            self.dropdownStackViewHeight.constant = 144
        })
    }
    
    private func hideDropdown() {
        dropdownButtonImage.image = UIImage(named: "iconSolidCheveronDown16")
        dropdownStackView.transform = CGAffineTransform(translationX: 0, y: 0)
        UIView.animate(withDuration: 0.3, animations: {
            self.dropdownStackView.transform = CGAffineTransform(translationX: 0, y: -144)
            self.dropdownStackView.alpha = 0.0
            self.dropdownStackViewHeight.constant = 72
            self.dropdownView.alpha = 0.0
        }, completion: {(isCompleted) in
            self.dropdownView.isHidden = true
        })
    }
    
    @objc func clickDropdown(_ gesture: UITapGestureRecognizer) {
        if dropdownView.isHidden {
            showDropdown()
        } else {
            hideDropdown()
        }
    }
    
    @objc func clickDropdown1(_ gesture: UITapGestureRecognizer) { /* 중학수학 */
        if selectedSchoolIndex == 0 {
            selectedSchoolIndex = 1
            highSchoolTabList[selectedSubjectIndex].isSelect = false
            middleSchoolTabList[0].isSelect = true
            selectedSubjectIndex = 0
            dropdownButtonLabel.text = "중학수학"
            dropdownButtonLabel1.textColor = UIColor.black
            dropdownButtonLabel2.textColor = UIColor.Foggy
            cvTab.reloadData()
            tableViewReload(isScrollToSection: true)
            hideDropdown()
        }
    }
    
    @objc func clickDropdown2(_ gesture: UITapGestureRecognizer) { /* 고등수학 */
        if selectedSchoolIndex == 1 {
            selectedSchoolIndex = 0
            middleSchoolTabList[selectedSubjectIndex].isSelect = false
            highSchoolTabList[0].isSelect = true
            selectedSubjectIndex = 0
            dropdownButtonLabel.text = "고등수학"
            dropdownButtonLabel1.textColor = UIColor.Foggy
            dropdownButtonLabel2.textColor = UIColor.black
            cvTab.reloadData()
            tableViewReload(isScrollToSection: true)
            hideDropdown()
        }
    }
    
    @objc func closeDropdown(_ gesture: UITapGestureRecognizer) {
        hideDropdown()
    }
    
    // MARK: - Bottom View
    @IBOutlet weak var selectUnitListViewHeight: NSLayoutConstraint! // 120 or 0
    @IBOutlet weak var resetButtonWidth: NSLayoutConstraint! // 4 or startButtonWidth / 3
    @IBOutlet weak var startButtonLeading: NSLayoutConstraint!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var bottomView: UIView!
    private func setBottomButtonView() {
        //
        if selectUnitList.isNotEmpty {
            selectUnitListViewHeight.constant = 120
            startButtonLeading.constant = 12
            resetButtonWidth.constant = bottomView.frame.width / 3
            startButton.setTitle("\(selectUnitList.count)개 단원으로 시작", for: .normal)
            startButton.backgroundColor = UIColor.Purple
            startButton.setTitleColor(UIColor.white, for: .normal)
        } else {
            selectUnitListViewHeight.constant = 0
            startButtonLeading.constant = 0
            resetButtonWidth.constant = 0
            startButton.setTitle("단원을 선택해주세요", for: .normal)
            startButton.backgroundColor = UIColor.Foggy20
            startButton.setTitleColor(UIColor.Foggy, for: .normal)
        }
        
        cvSelectUnitList.reloadData()
    }
    
    @IBAction func resetButtonClick(_ sender: Any) {
        //ab180 이벤트 보내기
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            var kName = ""
            if self.keyName != "" {
                kName = self.keyName
            } else {
                kName = "키워드 선택 건너뛰기"
            }
            
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: "pulja2.0_cat2select_reset",
                    customs: ["userseq": userSeq, "user_id": userId, "keyword" : kName, "unit2List": self.selectUnitSeqs]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "category: pulja2.0_cat2select_reset", separator: " ")
                print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                print("[ABLog]", "user_id: \(userId)", separator: " ")
                print("[ABLog]", "keyword: \(kName)", separator: " ")
                print("[ABLog]", "unit2List: \(self.selectUnitSeqs)", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
        }
        
        selectUnitList.removeAll()
        resetUnitSelected(data: unitViewList)
        setBottomButtonView()
    }
    
    @IBAction func startButtonClick(_ sender: Any) {
        // keyword list
        selectKeywordSeqs = []
        if selectKeywordList.isNotEmpty {
            for keyword in selectKeywordList {
                if let keywordSeq = keyword.keywordSeq {
                    selectKeywordSeqs.append(keywordSeq)
                }
            }
        }
        
        // unit seq list
        selectUnitSeqs = []
        if selectUnitList.isNotEmpty {
            for unit in selectUnitList {
                selectUnitSeqs.append(unit.unit2Seq)
            }
        }
        
//        print("!!!\(selectKeywordSeqs)")
//        print("!!!\(selectUnitSeqs)")
//        print("!!!\(cameFromOnboard)")
//        print("!!!\(cameFrom)")
//        print("!!!\(keyName)")
        
        if selectUnitSeqs.isNotEmpty {
            let vc = R.storyboard.preparation.preparationMultipleChoiceViewController()!
            vc.cameFromOnboard = self.cameFromOnboard
            vc.cameFrom = self.cameFrom
            vc.unit2Seqs = selectUnitSeqs
            vc.keywordSeqs = selectKeywordSeqs


            //에어브릿지 이벤트 보내기
            if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
                if selectKeywordSeqs.count == 0 {
                    vc.keywordName = "키워드 선택 건너뛰기"
                } else {
                    vc.keywordName = self.keyName
                }

                //ab180 이벤트 보내기
                if Const.isAirBridge {
                    CommonUtil.shared.ABEvent(
                        category: "pulja2.0_cat2modal_next",
                        customs: ["userseq": userSeq, "user_id": userId, "keyword" : vc.keywordName, "unit2List": self.selectUnitSeqs]
                    )
                } else {
                    print("[ABLog]", "-----------------------------------------", separator: " ")
                    print("[ABLog]", "category: pulja2.0_cat2modal_next", separator: " ")
                    print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                    print("[ABLog]", "user_id: \(userId)", separator: " ")
                    print("[ABLog]", "keyword: \(vc.keywordName)", separator: " ")
                    print("[ABLog]", "unit2List: \(self.selectUnitSeqs)", separator: " ")
                    print("[ABLog]", "-----------------------------------------", separator: " ")
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // MARK: - Collection View : Tab, Selected Unit List
    @IBOutlet weak var header: UIView!
    @IBOutlet weak var cvTab: UICollectionView!
    private var highSchoolTabList: [SubjectTab] = [
        SubjectTab(name: "고등수학(상)", isSelect: false),
        SubjectTab(name: "고등수학(하)", isSelect: false),
        SubjectTab(name: "수학 I", isSelect: false),
        SubjectTab(name: "수학 II", isSelect: false)
    ]
    private var middleSchoolTabList: [SubjectTab] = [
        SubjectTab(name: "1학년 1학기", isSelect: false),
        SubjectTab(name: "1학년 2학기", isSelect: false),
        SubjectTab(name: "2학년 1학기", isSelect: false),
        SubjectTab(name: "2학년 2학기", isSelect: false),
        SubjectTab(name: "3학년 1학기", isSelect: false),
        SubjectTab(name: "3학년 2학기", isSelect: false),
    ]
    
    @IBOutlet weak var cvSelectUnitList: UICollectionView!
    private var selectUnitList: [Unit2ViewData] = []
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == cvTab {
            if selectedSchoolIndex == 0 {
                return highSchoolTabList.count
            } else {
                return middleSchoolTabList.count
            }
        } else {
            return selectUnitList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == cvTab {
            let cell = cvTab.dequeueReusableCell(withReuseIdentifier: "ProblemSubjectTabViewCell", for: indexPath) as! ProblemSubjectTabViewCell
            if selectedSchoolIndex == 0 {
                cell.collectionTab = highSchoolTabList[indexPath.item]
            } else {
                cell.collectionTab = middleSchoolTabList[indexPath.item]
            }
            return cell
        } else {
            let cell = cvSelectUnitList.dequeueReusableCell(withReuseIdentifier: "SelectUnitViewCell", for: indexPath) as! SelectUnitViewCell
            cell.unit = selectUnitList[indexPath.item]
            cell.deleteCallback = { isSend in
                
                let cellUnit = self.selectUnitList[indexPath.item]
                
                self.unitViewList[cellUnit.schoolIndex].subjects[cellUnit.subjectIndex].unit1s[cellUnit.unit1Index].unit2s[cellUnit.unit2Index].selected = false
                
                var allCheckCount = 0
                for unit2 in self.unitViewList[cellUnit.schoolIndex].subjects[cellUnit.subjectIndex].unit1s[cellUnit.unit1Index].unit2s {
                    if unit2.selected {
                        allCheckCount += 1
                    }
                }
                let unit2sCount = self.unitViewList[cellUnit.schoolIndex].subjects[cellUnit.subjectIndex].unit1s[cellUnit.unit1Index].unit2s.count
                
                self.unitViewList[cellUnit.schoolIndex].subjects[cellUnit.subjectIndex].unit1s[cellUnit.unit1Index].selected = unit2sCount == allCheckCount
                
                self.tableViewReload(isScrollToSection: false)
                self.selectUnitList.remove(at: indexPath.item)
                if self.selectUnitList.isEmpty {
                    self.resetUnitSelected(data: self.unitViewList)
                }
                self.setBottomButtonView()
            }
            return cell
        }
    }
    
    // Click
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == cvTab {
            if selectedSubjectIndex != indexPath.item {
                if selectedSchoolIndex == 0 {
                    // 이전 선택
                    highSchoolTabList[selectedSubjectIndex].isSelect = false
                    
                    // 새로 선택
                    if highSchoolTabList[indexPath.item].isSelect == false {
                        highSchoolTabList[indexPath.item].isSelect = true
                    }
                } else {
                    // 이전 선택
                    middleSchoolTabList[selectedSubjectIndex].isSelect = false
                    
                    // 새로 선택
                    if middleSchoolTabList[indexPath.item].isSelect == false {
                        middleSchoolTabList[indexPath.item].isSelect = true
                    }
                }
                
                selectedSubjectIndex = indexPath.item
                tableViewReload(isScrollToSection: true)
                cvTab.reloadData()
                cvTab.scrollToItem(at: indexPath, at: .left, animated: true)
            }
        }
    }
    
    // MARK: - Table View
    @IBOutlet weak var tvUnitList: UITableView!
    
    private var isCheckTableScroll = false
    private func tableViewReload(isScrollToSection: Bool) {
        if isScrollToSection {
            tvUnitList.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 24.0))
            tvUnitList.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 8.0))
            tvUnitList.setContentOffset(CGPointMake(0, 0), animated: false)
        }

        tvUnitList.reloadData()
        
        if isScrollToSection {
            isCheckTableScroll = false
            let unit1s = unitViewList[selectedSchoolIndex].subjects[selectedSubjectIndex].unit1s
            for unit1Index in 0..<unit1s.count {
                for unit2Index in 0..<unit1s[unit1Index].unit2s.count {
                    if unit1s[unit1Index].unit2s[unit2Index].selected {
                        if !isCheckTableScroll {
                            isCheckTableScroll = true
                            if unit1Index > 0 {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                                    let index = unit1Index - 1
                                    let count = unit1s[index].unit2s.count - 1
                                    let ip = IndexPath(item: count, section: index)
                                    self.tvUnitList.scrollToRow(at: ip, at: .top, animated: true)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int{
        return unitViewList[selectedSchoolIndex].subjects[selectedSubjectIndex].unit1s.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return unitViewList[selectedSchoolIndex].subjects[selectedSubjectIndex].unit1s[section].unit2s.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Unit2SelectTableViewCell", for: indexPath) as! Unit2SelectTableViewCell
        let section = indexPath.section
        let index = indexPath.row
        cell.selectionStyle = .none
        
        cell.unit1 = unitViewList[selectedSchoolIndex].subjects[selectedSubjectIndex].unit1s[section]
        cell.unit2 = unitViewList[selectedSchoolIndex].subjects[selectedSubjectIndex].unit1s[section].unit2s[index]
        
        cell.checkCallback = { isCheck in
            print("callback \(isCheck)")
            print("indexPath.section \(indexPath.section), indexPath.row \(indexPath.row)")
            
            if isCheck {
                self.selectUnitList.insert(cell.unit2, at: 0)
                self.cvSelectUnitList.setContentOffset(CGPointMake(0, 0), animated: false)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.cvSelectUnitList.flashScrollIndicators()
                }
                cell.cbUnit2.setCheckState(.checked, animated: false)
            } else {
                var index = 0
                for unit2 in self.selectUnitList {
                    if unit2.unit2Seq == cell.unit2.unit2Seq {
                        self.selectUnitList.remove(at: index)
                    }
                    index += 1
                }
                cell.cbUnit2.setCheckState(.unchecked, animated: false)
            }
            
            self.unitViewList[self.selectedSchoolIndex].subjects[self.selectedSubjectIndex].unit1s[indexPath.section].unit2s[indexPath.row].selected = isCheck
            
            var allCheckCount = 0
            for unit2 in self.unitViewList[self.selectedSchoolIndex].subjects[self.selectedSubjectIndex].unit1s[indexPath.section].unit2s {
                if unit2.selected {
                    allCheckCount += 1
                }
            }
            
            let unit2sCount = self.unitViewList[self.selectedSchoolIndex].subjects[self.selectedSubjectIndex].unit1s[indexPath.section].unit2s.count
            self.unitViewList[self.selectedSchoolIndex].subjects[self.selectedSubjectIndex].unit1s[indexPath.section].selected = unit2sCount == allCheckCount
            
            //print("!!!\(unit2sCount) , \(allCheckCount)")
            
            self.tvUnitList.reloadSections(IndexSet(indexPath.section...indexPath.section), with: .none)
            self.setBottomButtonView()
        }
        
        cell.checkAllCallback = { isCheck in
            self.unitViewList[self.selectedSchoolIndex].subjects[self.selectedSubjectIndex].unit1s[indexPath.section].selected = isCheck
            
            for unit1Index in 0..<cell.unit1.unit2s.count {
                self.unitViewList[self.selectedSchoolIndex].subjects[self.selectedSubjectIndex].unit1s[indexPath.section].unit2s[unit1Index].selected = isCheck
            }
            
            for unit2 in cell.unit1.unit2s {
                var index = 0
                for selectUnit2 in self.selectUnitList {
                    if self.selectUnitList.count > index && unit2.unit2Seq == selectUnit2.unit2Seq {
                        self.selectUnitList.remove(at: index)
                    }
                    index += 1
                }
                
                if isCheck {
                    self.selectUnitList.insert(unit2, at: 0)
                }
            }
            
            if isCheck {
                self.cvSelectUnitList.setContentOffset(CGPointMake(0, 0), animated: false)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.cvSelectUnitList.flashScrollIndicators()
                }
            }
            
            self.setBottomButtonView()
            self.tableViewReload(isScrollToSection: false)
        }
        
        if index == 0 {
            if let index = cell.unit1.name.firstIndex(of: " ") {
                cell.unit1.name.insert(".", at: index)
            }
            cell.unit1Name = cell.unit1.name
            cell.lbUnit1Name.text = cell.unit1.name
            cell.unit1View.isHidden = false
            cell.divider.isHidden = false
        } else {
            cell.unit1View.isHidden = true
            cell.divider.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cornerRadius: CGFloat = 12.0
        cell.backgroundColor = UIColor.clear

        let layer: CAShapeLayer = CAShapeLayer()
        let pathRef: CGMutablePath = CGMutablePath()
        //dx leading an trailing margins
        let bounds: CGRect = cell.bounds.insetBy(dx: 0, dy: 0)
        var addLine: Bool = false

        if indexPath.row == 0 && indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
            pathRef.__addRoundedRect(transform: nil, rect: bounds, cornerWidth: cornerRadius, cornerHeight: cornerRadius)
        } else if indexPath.row == 0 {
            pathRef.move(to: CGPoint(x: bounds.minX, y: bounds.maxY))
            pathRef.addArc(tangent1End: CGPoint(x: bounds.minX, y: bounds.minY),
                           tangent2End: CGPoint(x: bounds.midX, y: bounds.minY),
                           radius: cornerRadius)

            pathRef.addArc(tangent1End: CGPoint(x: bounds.maxX, y: bounds.minY),
                           tangent2End: CGPoint(x: bounds.maxX, y: bounds.midY),
                           radius: cornerRadius)
            pathRef.addLine(to: CGPoint(x: bounds.maxX, y: bounds.maxY))
            addLine = true
        } else if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
            pathRef.move(to: CGPoint(x: bounds.minX, y: bounds.minY))
            pathRef.addArc(tangent1End: CGPoint(x: bounds.minX, y: bounds.maxY),
                           tangent2End: CGPoint(x: bounds.midX, y: bounds.maxY),
                           radius: cornerRadius)

            pathRef.addArc(tangent1End: CGPoint(x: bounds.maxX, y: bounds.maxY),
                           tangent2End: CGPoint(x: bounds.maxX, y: bounds.midY),
                           radius: cornerRadius)
            pathRef.addLine(to: CGPoint(x: bounds.maxX, y: bounds.minY))
        }

        layer.path = pathRef
        layer.strokeColor = UIColor.Foggy40.cgColor
        layer.lineWidth = 1.5
        layer.fillColor = UIColor.white.cgColor
        layer.shadowColor = UIColor.white.cgColor

        if addLine == true {
            let lineLayer: CALayer = CALayer()
            let lineHeight: CGFloat = (1 / UIScreen.main.scale)
            lineLayer.frame = CGRect(x: bounds.minX, y: bounds.size.height - lineHeight, width: bounds.size.width, height: lineHeight)
            lineLayer.backgroundColor = UIColor.clear.cgColor
            layer.addSublayer(lineLayer)
        }

        let backgroundView: UIView = UIView(frame: bounds)
        backgroundView.layer.insertSublayer(layer, at: 0)
        backgroundView.backgroundColor = .white
        cell.backgroundView = backgroundView
    }
    
    //MARK: - Override
    var closeCallback : ((_ changeType : String) -> Void)?
    
    var selectKeywordList:[KeywordData] = []
    var cameFromOnboard = false
    var cameFrom = ""
    var keyName = ""
    
    private var selectUnitSeqs: [Int] = []
    private var selectKeywordSeqs: [Int] = []
    private var unitDataList:[UnitData]? = nil
    private var unitViewList:[UnitViewData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cvTab.delegate = self
        cvTab.dataSource = self
        cvTab.register(ProblemSubjectTabViewCell.nib(), forCellWithReuseIdentifier: "ProblemSubjectTabViewCell")
        cvTab.reloadData()
        cvTab.isHidden = true
        
        header.isHidden = true
        bottomView.isHidden = true
        
        getUnit2List()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        CommonUtil.tabbarController?.tabBar.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.tableViewReload(isScrollToSection: true)
        }
    }
    // MARK: - API
    private func getUnit2List() {
        LoadingView.show()
        CurationAPI.shared.unit2List().done { res in
            if let data = res.data {
                self.unitDataList = data
                
                self.setUnitDataList(data: data)
                self.setHeaderView()
                self.setBottomButtonView()

                self.cvTab.reloadData()
                self.cvTab.isHidden = false
                self.header.isHidden = false
                self.bottomView.isHidden = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.cvSelectUnitList.flashScrollIndicators()
                }
            }
       }.catch { err in
           LoadingView.hide()
       }.finally {
           LoadingView.hide()
       }
    }
    
    // MARK: - View Data
    private func setUnitDataList(data: [UnitData]) {
        // 중고등 구분
        if let selected = data[0].selected, selected {
            selectedSchoolIndex = 0
        } else {
            selectedSchoolIndex = 1
        }
        var subjectSelectCheck = false
        // Unit View Data
        for schoolIndex in 0..<data.count {
            var unitViewData = UnitViewData()
            var subjectViewDataList: [SubjectViewData] = []
            if let subjects = data[schoolIndex].subjects {
                for subjectIndex in 0..<subjects.count {
                    
                    if let selected = subjects[subjectIndex].selected, selected, !subjectSelectCheck {
                        // 초기 진입시 셋팅
                        subjectSelectCheck = true
                        selectedSubjectIndex = subjectIndex
                        if selectedSchoolIndex == 0 {
                            self.highSchoolTabList[subjectIndex].isSelect = selected
                        } else {
                            self.middleSchoolTabList[subjectIndex].isSelect = selected
                        }
                    }
                    
                    var subjectViewData = SubjectViewData()
                    var unit1ViewDataList: [Unit1ViewData] = []
                    
                    if let unit1s = subjects[subjectIndex].unit1s {
                        for unit1Index in 0..<unit1s.count {
                            var unit1ViewData = Unit1ViewData()
                            var unit2ViewDataList: [Unit2ViewData] = []
                            var unit1SelectAllCheck = true
                            
                            if let unit2s = unit1s[unit1Index].unit2s {
                                for unit2Index in 0..<unit2s.count {
                                    let unit2ViewData = Unit2ViewData(
                                        schoolIndex: schoolIndex,
                                        subjectIndex: subjectIndex,
                                        unit1Index: unit1Index,
                                        unit2Index: unit2Index,
                                        unit2Seq: unit2s[unit2Index].unit2Seq ?? 0,
                                        name: unit2s[unit2Index].sname ?? "",
                                        selected: unit2s[unit2Index].selected ?? false
                                    )
                                    unit2ViewDataList.append(unit2ViewData)
                                    
                                    if let selected = unit2s[unit2Index].selected {
                                        if selected {
                                            selectUnitList.append(unit2ViewData)
                                        } else {
                                            unit1SelectAllCheck = false
                                        }
                                    }
                                }
                            }
                            unit1ViewData.unit1Seq = unit1s[unit1Index].unit1Seq ?? 0
                            unit1ViewData.name = unit1s[unit1Index].name ?? ""
                            unit1ViewData.selected = unit1SelectAllCheck
                            unit1ViewData.unit2s = unit2ViewDataList
                            unit1ViewDataList.append(unit1ViewData)
                        }
                    }
                    subjectViewData.subjectSeq = subjects[subjectIndex].subjectSeq ?? 0
                    subjectViewData.name = subjects[subjectIndex].name ?? ""
                    subjectViewData.selected = subjects[subjectIndex].selected ?? false
                    subjectViewData.unit1s = unit1ViewDataList
                    subjectViewDataList.append(subjectViewData)
                }
            }
            unitViewData.name = data[schoolIndex].name ?? ""
            unitViewData.selected = data[schoolIndex].selected ?? false
            unitViewData.subjects = subjectViewDataList
            unitViewList.append(unitViewData)
        }
        
        cvSelectUnitList.delegate = self
        cvSelectUnitList.dataSource = self
        cvSelectUnitList.register(SelectUnitViewCell.nib(), forCellWithReuseIdentifier: "SelectUnitViewCell")
        cvSelectUnitList.reloadData()
        cvSelectUnitList.collectionViewLayout = SelectUnitListViewLeftAlignFlowLayout()
        if let flowLayout = self.cvSelectUnitList?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        }
        
        tvUnitList.delegate = self
        tvUnitList.dataSource = self
        tvUnitList.separatorStyle = .none
        tvUnitList.register(Unit2SelectTableViewCell.nib(), forCellReuseIdentifier: "Unit2SelectTableViewCell")
        
        tableViewReload(isScrollToSection: true)
    }
    
    private func resetUnitSelected(data: [UnitViewData]) {
        for unit in data {
            for subject in unit.subjects {
                for unit1 in subject.unit1s {
                    for unit2 in unit1.unit2s {
                        unitViewList[unit2.schoolIndex].subjects[unit2.subjectIndex].unit1s[unit2.unit1Index].selected = false
                        unitViewList[unit2.schoolIndex].subjects[unit2.subjectIndex].unit1s[unit2.unit1Index].unit2s[unit2.unit2Index].selected = false
                    }
                }
            }
        }
        tableViewReload(isScrollToSection: false)
    }
}

class SelectUnitListViewLeftAlignFlowLayout: UICollectionViewFlowLayout {
    let cellSpacing: CGFloat = 10
 
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        self.minimumLineSpacing = 10.0
        self.sectionInset = UIEdgeInsets(top: 16.0, left: 16.0, bottom: 12.0, right: 16.0)
        let attributes = super.layoutAttributesForElements(in: rect)
 
        var leftMargin = sectionInset.left
        var maxY: CGFloat = -1.0
        attributes?.forEach { layoutAttribute in
            if layoutAttribute.frame.origin.y >= maxY {
                leftMargin = sectionInset.left
            }
            layoutAttribute.frame.origin.x = leftMargin
            leftMargin += layoutAttribute.frame.width + cellSpacing
            maxY = max(layoutAttribute.frame.maxY, maxY)
        }
        return attributes
    }
}

// MARK: - UnitData
struct UnitViewData {
    var name : String = ""
    var subjects : [SubjectViewData] = []
    var selected : Bool = false
}

struct SubjectViewData {
    var name : String = ""
    var subjectSeq : Int = 0
    var unit1s : [Unit1ViewData] = []
    var selected : Bool = false
}

struct Unit1ViewData {
    var name : String = ""
    var unit1Seq : Int = 0
    var unit2s : [Unit2ViewData] = []
    var selected : Bool = false
}

struct Unit2ViewData {
    var schoolIndex : Int = 0
    var subjectIndex : Int = 0
    var unit1Index : Int = 0
    var unit2Index : Int = 0
    var unit2Seq : Int = 0
    var name : String = ""
    var selected : Bool = false
}
