//
//  SatisfactionViewController.swift
//  pulja
//
//  Created by 김병헌 on 2022/11/04.
//

import UIKit
import Kingfisher

class SatisfactionViewController: PuljaBaseViewController, UITextViewDelegate {
    
    var callback: (_ isSend: Bool) -> Void = { _ in }
    
    @IBOutlet weak var ivKeywordIcon: UIImageView!
    
    @IBOutlet weak var lbKeywordDesc1: UILabel!
    
    
    @IBOutlet weak var ivBadFace: UIImageView!
    
    @IBOutlet weak var lbBad: UILabel!
    
    
    @IBOutlet weak var ivNormalFace: UIImageView!
    
    @IBOutlet weak var lbNormal: UILabel!
    
    @IBOutlet weak var ivGoodFace: UIImageView!
    
    @IBOutlet weak var lbGood: UILabel!
    
    @IBOutlet weak var tvReason: UITextView!
    
    
    @IBOutlet weak var btSubmit: UIButton!
    
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var lbDesc2: UILabel!
    
    @IBOutlet weak var sheetView: UIView!
    
    @IBOutlet weak var topMargin: NSLayoutConstraint!
    
    
    let textViewPlaceHolder = "그렇게 생각한 이유를 들려주세요. 더 나은 추천이 가능해져요. (선택 사항)"
    
    var characterCount = 0
    
    var problemSeq = 0
    var keywordName = ""
    var feedbackMeasure  = 0
    var feedbackText = ""
    var keywordSeq = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.sheetView.roundedTop(10.0)
        }
        
        //done button 추가
        let toolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
        let flexSpace = UIBarButtonItem(barButtonSystemItem:.flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "완료", style: .done, target: self, action: #selector(dismissMyKeyboard))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        self.tvReason.inputAccessoryView = toolbar
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

//        lbKeywordDesc1.font = UIFont.init(name: "AppleSDGothicNeo-SemiBold", size: 20)
        let attrs1 =  [NSAttributedString.Key.font : UIFont(name: "AppleSDGothicNeo-SemiBold", size: 20), NSAttributedString.Key.foregroundColor : UIColor.Purple]
        let attrs2 = [NSAttributedString.Key.font : UIFont(name: "AppleSDGothicNeo-SemiBold", size: 20), NSAttributedString.Key.foregroundColor : UIColor.puljaBlack]

        let attributedString1 = NSMutableAttributedString(string:keywordName, attributes:attrs1)
        let attributedString2 = NSMutableAttributedString(string:" 문제가", attributes:attrs2)

        attributedString1.append(attributedString2)
        
        self.lbKeywordDesc1.attributedText = attributedString1
        
        
        if keywordSeq > 0 {
            let url = "https://pulja-contents.s3.ap-northeast-2.amazonaws.com/mobile_keyword/\(keywordSeq).png"
            self.downloadImage(with: url) { image in
                self.ivKeywordIcon.image = image?.withRenderingMode(.alwaysTemplate)
                self.ivKeywordIcon.tintColor = .Purple
            }
        }
        
        
        tvReason.delegate = self
              
        tvReason.textColor = .Foggy
        tvReason.text = textViewPlaceHolder
        tvReason.textContainerInset = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16);
        
        
        CurationAPI.shared.keywordFeedBackShow(problemSeq: self.problemSeq).done { res in
            print("res -> \(res)")
        }.catch { error in
            print(error.localizedDescription)
        }
    }
    
    @objc func dismissMyKeyboard() {
            view.endEditing(true)
        }
    
//    @objc func keyboardWillHide(_ notification : Notification) {
//        self.bottomConstraint.constant = 0
//    }
//
//    @objc func keyboardWillShow(_ notification: Notification)
//    {
//        if let keyboardFrame: NSValue = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
//          let keyboardRectangle = keyboardFrame.cgRectValue
////            self.bottomConstraint.constant = -1 * ( keyboardRectangle.height - 36 )
//            self.bottomConstraint.constant = ( keyboardRectangle.height - 36 )
//          UIView.animate(withDuration: 0.3) {
//            self.view.layoutIfNeeded()
//          }
//         }
//   }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == textViewPlaceHolder {
            textView.text = nil
            textView.textColor = .puljaBlack
            
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            textView.text = textViewPlaceHolder
            textView.textColor = .Foggy
        }
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let inputString = text.trimmingCharacters(in: .whitespacesAndNewlines)
        guard let oldString = textView.text, let newRange = Range(range, in: oldString) else { return true }
        let newString = oldString.replacingCharacters(in: newRange, with: inputString).trimmingCharacters(in: .whitespacesAndNewlines)

        let characterCount = newString.count
        
        guard characterCount <= 500 else {
            self.showToast(message: "최대 500자까지 작성이 가능합니다.")
            return false
            
        }
        updateCountLabel(characterCount: characterCount)

        return true
    }
    
    private func updateCountLabel(characterCount: Int) {
        self.characterCount = characterCount
//         remainCountLabel.text = "\(characterCount)"
        if characterCount > 0 {
//            remainCountLabel.textColor = .puljaBlack
        } else {
//            remainCountLabel.textColor = .grey
        }
        
        
//        submitEnable()
     }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
                 
         guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
//             self.topMargin.constant = 92
             // if keyboard size is not available for some reason, dont do anything

            return
         }
        
        
//        if let keyboardFrame: NSValue = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
//          let keyboardRectangle = keyboardFrame.cgRectValue
////            self.bottomConstraint.constant = -1 * ( keyboardRectangle.height - 36 )
//            self.bottomConstraint.constant = ( keyboardRectangle.height - 36 )
//          UIView.animate(withDuration: 0.3) {
//            self.view.layoutIfNeeded()
//          }
//         }
        
        tvReason.layer.borderColor = UIColor.OffPurple.cgColor
        
        topView.isHidden = true
        lbDesc2.isHidden = true
     }

     @objc func keyboardWillHide(notification: NSNotification) {
         self.view.frame.origin.y = 0
//         self.topMargin.constant = 0
         tvReason.layer.borderColor = UIColor.Foggy.cgColor
         
         topView.isHidden = false
         lbDesc2.isHidden = false

     }
    
    func submitEnable(){
        if feedbackMeasure > 0 {
            btSubmit.setTitleColor(UIColor.white, for: .normal)
            btSubmit.backgroundColor = UIColor.OffPurple
        } else {
            btSubmit.setTitleColor(UIColor.Foggy, for: .normal)
            btSubmit.backgroundColor = UIColor.Foggy40
        }

    }
    
    
    @IBAction func badAction(_ sender: Any) {
        feedbackMeasure = 1
        setMeasure()
    }
    
    
    @IBAction func normalAction(_ sender: Any) {
        feedbackMeasure = 2
        setMeasure()
    }
    
    
    @IBAction func goodAction(_ sender: Any) {
        feedbackMeasure = 3
        setMeasure()
    }
    
    func setMeasure() {
        
        ivBadFace.image = UIImage(named: "iconFaceBadOff")
        ivNormalFace.image = UIImage(named: "iconFaceNormalOff")
        ivGoodFace.image = UIImage(named: "iconFaceGoodOff")
        
        lbBad.textColor = UIColor.grey
        lbNormal.textColor = UIColor.grey
        lbGood.textColor = UIColor.grey
        
        if feedbackMeasure == 1 {
            ivBadFace.image = UIImage(named: "iconFaceBadOn")
            lbBad.textColor = UIColor.Purple

            
        } else if feedbackMeasure == 2 {
            ivNormalFace.image = UIImage(named: "iconFaceNormalOn")
            lbNormal.textColor = UIColor.Purple
            
        } else if feedbackMeasure == 3 {
            ivGoodFace.image = UIImage(named: "iconFaceGoodOn")
            lbGood.textColor = UIColor.Purple
        }
        
        submitEnable()
    }
    
    @IBAction func submitAction(_ sender: Any) {
        if feedbackMeasure > 0 {
            var contents = ""
            
            if self.tvReason.text != self.textViewPlaceHolder {
                contents = tvReason.text
            }
            
            LoadingView.show()
            
            CurationAPI.shared.keywordFeedBack(feedbackMeasure: self.feedbackMeasure, problemSeq: self.problemSeq, feedbackText: contents).done { res in
                self.dismiss(animated: true) {
                    self.callback(true)
                }
            }.catch { error in
                print(error.localizedDescription)
            }.finally {
                LoadingView.hide()
            }
        }
    }
    
    
    func showToast(message: String) {
        
        var heMinus : CGFloat = 120.0
        
        
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.first
            let top = window?.safeAreaInsets.top
            let bottom = window?.safeAreaInsets.bottom
            heMinus += bottom!
            
        } else if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let top = window?.safeAreaInsets.top
            let bottom = window?.safeAreaInsets.bottom
            heMinus += bottom!
        }
        
        let width = self.view.frame.size.width
        let toastLabel = PaddingLabel(frame: CGRect(x: 16, y: self.view.frame.size.height - heMinus, width: width - 32, height: 44))
        toastLabel.paddingLeft = 16
        toastLabel.paddingRight = 16
        
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        toastLabel.textColor = UIColor.white
        toastLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        toastLabel.textAlignment = .left
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 8
        toastLabel.clipsToBounds = true
        
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 2.0, delay: 1.5, options: .curveEaseOut, animations: { toastLabel.alpha = 0.0 }, completion: {(isCompleted) in toastLabel.removeFromSuperview() })
        
        
    }
    
    func downloadImage(with urlString : String , imageCompletionHandler: @escaping (UIImage?) -> Void){
                
            guard let url = URL.init(string: urlString) else {
                return  imageCompletionHandler(nil)
            }
            let resource = ImageResource(downloadURL: url)
            KingfisherManager.shared.retrieveImage(with: resource, progressBlock: nil) { result in
                switch result {
                case .success(let value):
                    imageCompletionHandler(value.image)
                case .failure:
                    imageCompletionHandler(nil)
                }
            }
        }

}
