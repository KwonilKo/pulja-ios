//
//  SubjectPickerViewController.swift
//  pulja
//
//  Created by 김병헌 on 2022/10/26.
//

import UIKit

class SubjectPickerViewController: UIViewController,
                                   UIPickerViewDelegate, UIPickerViewDataSource {
   let totalCount = 6
   let pickerViewCnt = 1
   
   var callback : ((_ subjectName : String?, _ subjectSeq : Int?) -> Void)?
   
    var unitDatas:[UnitData]? = nil
    var selectMHIndex = 0
    var selectSubjectIndex = 0
    var selectSubjectName = ""
   @IBOutlet weak var pickerView: UIPickerView!
   
   
   
   override func viewDidLoad() {
       super.viewDidLoad()

       pickerView.selectRow(selectSubjectIndex, inComponent: 0, animated: true)
   }
   
   @IBAction func sGradeSubmit(_ sender: Any) {
     
       callback?(selectSubjectName, selectSubjectIndex)
       dismiss(animated: true)
       
   }
    
   
   @IBAction func close(_ sender: Any) {
       dismiss(animated: true) { }
   }
   
   
   func numberOfComponents(in pickerView: UIPickerView) -> Int {
       return pickerViewCnt
   }
   
   func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       return unitDatas?[selectMHIndex].subjects?.count ?? 0
   }
   
   func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       if var subjectName = unitDatas?[selectMHIndex].subjects?[row].name {
           if selectMHIndex == 0 {
               subjectName = "고등\(subjectName)"
           } else {
               let arr = subjectName.components(separatedBy: "-")
               let a1 = arr[0].substring(from: 1, to: 1)
               let a2 = arr[1]
               subjectName = "\(a1)학년 \(a2)학기"
           }
           return subjectName
       }
       return ""
   }
    
   func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
       var subjectName = ""
       
       if var name = unitDatas?[selectMHIndex].subjects?[row].name {
           if selectMHIndex == 0 {
               subjectName = "고등\(name)"
           } else {
               let arr = name.components(separatedBy: "-")
               let a1 = arr[0].substring(from: 1, to: 1)
               let a2 = arr[1]
               subjectName = "\(a1)학년 \(a2)학기"
           }
       }
       
       return NSAttributedString(string: subjectName, attributes: [.foregroundColor:UIColor.puljaBlack])
   }

   func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       selectSubjectIndex = row
   }
   
   

}
