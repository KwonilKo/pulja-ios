//
//  ChangeEmptyUnitViewController.swift
//  pulja
//
//  Created by HUN on 2022/12/15.
//

import UIKit
import Kingfisher

class ChangeEmptyUnitViewController: UIViewController {
    
    /* UI Variables */
    @IBOutlet weak var ivKeyword: UIImageView!
    @IBOutlet weak var lbImageKeyword: UILabel!
    @IBOutlet weak var lbKeyword: UILabel!
    @IBOutlet weak var lbUnit: UILabel!
    
    /* Callback Functions */
    var callback : ((_ changeType : String) -> Void)?
    
    /* Variables */
    var selectedUnit = ""
    var selectedKeywordURL: String?
    var selectedKeyword: String?
    
    /* Override */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 키워드 정보
        if let url = self.selectedKeywordURL {
            self.lbKeyword.text = ""
            self.lbImageKeyword.text = self.selectedKeyword ?? ""
            
            self.downloadImage(with: url) { image in
                self.ivKeyword.image = image?.withRenderingMode(.alwaysTemplate)
                self.ivKeyword.tintColor = .black
            }
        }
        
        // 단원명
        self.lbUnit.text = selectedUnit
    }
    
    /* Fuction */
    func downloadImage(with urlString : String , imageCompletionHandler: @escaping (UIImage?) -> Void){
            
        guard let url = URL.init(string: urlString) else {
            return  imageCompletionHandler(nil)
        }
        let resource = ImageResource(downloadURL: url)
        KingfisherManager.shared.retrieveImage(with: resource, progressBlock: nil) { result in
            switch result {
            case .success(let value):
                imageCompletionHandler(value.image)
            case .failure:
                imageCompletionHandler(nil)
            }
        }
    }
    
    /* Action */
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func changeUnitAction(_ sender: Any) {
        self.callback!("change")
        dismiss(animated: true)
    }
    
    @IBAction func startAction(_ sender: Any) {
        self.callback!("start")
        dismiss(animated: true)
    }
}
