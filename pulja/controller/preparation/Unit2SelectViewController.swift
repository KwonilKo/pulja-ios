//
//  Unit2SelectViewController.swift
//  pulja
//
//  Created by 김병헌 on 2022/10/26.
//

import UIKit
import MaterialComponents.MaterialBottomSheet

class Unit2SelectViewController: PuljaBaseViewController, UITableViewDataSource, UITableViewDelegate, MDCBottomSheetControllerDelegate {
    
    
    
    @IBOutlet weak var tvUnitList: UITableView!
    
    
    @IBOutlet weak var btStartStudy: UIButton!
    
    var closeCallback : ((_ changeType : String) -> Void)?
    
    var selectKeywords:[KeywordData] = []
    
    var recentUnitYN = "N"
    var recommendUnit : RecommendUnit? = nil

    
    var unitDatas:[UnitData]? = nil
    
    var selectSubjectIndex = 0
    
    var selectMHIndex = 0
    
    var selectUnitSeqs: [Int] = []
    var selectKeywordSeqs: [Int] = []
    
    var selectedUnit2Infos: [Unit2s] = []
    
    var cameFromOnboard = false
    var cameFrom = ""
    var keyName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tvUnitList.delegate = self
        tvUnitList.dataSource = self
        self.tvUnitList.register(Unit2SelectTableViewCell.nib(), forCellReuseIdentifier: "Unit2SelectTableViewCell")
        self.tvUnitList.register(Unit2SelectHeaderView.nib(), forHeaderFooterViewReuseIdentifier: "Unit2SelectHeaderView")

        LoadingView.show()
        CurationAPI.shared.unit2List().done { res in
                   
            self.unitDatas = res.data!
            
            self.setList()
            self.setSelectUnit2Seqs()
            
            self.tvUnitList.reloadData()
            self.setButtonInfo()
            LoadingView.hide()
       }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        CommonUtil.tabbarController?.tabBar.isHidden = true
        
        
    }
    
    func setList(){
        if let unitInfo = recommendUnit {
            //중고등 구분
          if let subjectSeq =  unitInfo.subjectSeq, subjectSeq  > 9 {
              selectMHIndex = 1
          } else {
              selectMHIndex = 0
          }

            
            if let unit2Seq = unitInfo.unit2Seq {
                
                //과목 세팅.
                if let data =  self.unitDatas, let subjects = data[selectMHIndex].subjects {
                    let idx = subjects.firstIndex(where: {$0.subjectSeq == unitInfo.subjectSeq})
                    selectSubjectIndex = idx ?? 0
                }
            }
        }
    }
    
    func setSelectKeywordSeqs(){
        selectKeywordSeqs = []
        if selectKeywords != nil {
            for keyword in selectKeywords{
                selectKeywordSeqs.append(keyword.keywordSeq!)
            }
        }
    }
    
    func setSelectUnit2Seqs(){
        if let data =  self.unitDatas {
            selectUnitSeqs = []
            selectedUnit2Infos = []
            for row in 0..<data.count {
                if let subjects = data[row].subjects {
                    for si in 0..<subjects.count {
                        if let unit1s = subjects[si].unit1s {
                            for u1 in 0..<unit1s.count {
                                if let unit2s = unit1s[u1].unit2s {
                                    for u2 in 0..<unit2s.count {
                                        if unit2s[u2].selected ?? false {
                                            selectUnitSeqs.append(unit2s[u2].unit2Seq!)
                                            selectedUnit2Infos.append(unit2s[u2])
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func resetUnit2Seqs(){
       if let data =  self.unitDatas {
           selectUnitSeqs = []
           selectedUnit2Infos = []
           for row in 0..<data.count {
               if let subjects = data[row].subjects {
                   for si in 0..<subjects.count {
                       if let unit1s = subjects[si].unit1s {
                           for u1 in 0..<unit1s.count {
                               if let unit2s = unit1s[u1].unit2s {
                                   for u2 in 0..<unit2s.count {
                                       if unit2s[u2].selected ?? false {
                                           self.unitDatas![row].subjects![si].unit1s![u1].unit2s![u2].selected = false
                                       }
                                   }
                               }
                           }
                       }
                   }
               }
           }
       }
    
        setButtonInfo()
    }

    
    
    @IBAction func backAction(_ sender: Any) {
        
//        self.navigationController?.popViewController(animated: true)
        self.navigationController?.popViewController(animated: true, completion: {
            self.closeCallback!("close")
            
        })
    }
    
    
    @IBAction func startStudyAction(_ sender: Any) {
        
        setSelectKeywordSeqs()
        setSelectUnit2Seqs()
        
        if selectedUnit2Infos.count > 0 {
            let vc = R.storyboard.preparation.preparationMultipleChoiceViewController()!
            vc.cameFromOnboard = self.cameFromOnboard
            vc.cameFrom = self.cameFrom
             vc.unit2Seqs = selectUnitSeqs
             vc.keywordSeqs = selectKeywordSeqs
            
            
            //에어브릿지 이벤트 보내기
            if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
                if selectKeywordSeqs.count == 0 {
                    vc.keywordName = "키워드 선택 건너뛰기"
                } else {
                    vc.keywordName = self.keyName
                }
                
                //ab180 이벤트 보내기
                if Const.isAirBridge {
                    CommonUtil.shared.ABEvent(
                        category: "pulja2.0_cat2modal_next",
                        customs: ["userseq": userSeq, "user_id": userId, "keyword" : vc.keywordName, "unit2List": self.selectUnitSeqs]
                    )
                } else {
                    print("[ABLog]", "-----------------------------------------", separator: " ")
                    print("[ABLog]", "category: pulja2.0_cat2modal_next", separator: " ")
                    print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                    print("[ABLog]", "user_id: \(userId)", separator: " ")
                    print("[ABLog]", "keyword: \(vc.keywordName)", separator: " ")
                    print("[ABLog]", "unit2List: \(self.selectUnitSeqs)", separator: " ")
                    print("[ABLog]", "-----------------------------------------", separator: " ")
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)

//            guard var controllers = self.navigationController?.viewControllers else {
//                 return
//              }
//            let secondVc = R.storyboard.preparation.preparationMultipleChoiceViewController()!
//            secondVc.unit2Seqs = selectUnitSeqs
//            secondVc.keywordSeqs = selectKeywordSeqs
//
////                  secondVc.noUnit2Info = true
//            controllers.append(secondVc)
//
//
//            self.navigationController?.setViewControllers(controllers, animated: false)
        }
    }
    
    func setButtonInfo(){
        if selectedUnit2Infos.count > 0 {
            btStartStudy.setTitle("\(selectedUnit2Infos.count)개의 단원 공부 시작하기", for: .normal)
            btStartStudy.setTitleColor(UIColor.white, for: .normal)
            btStartStudy.backgroundColor = UIColor.Purple
        } else {
            btStartStudy.setTitle("단원을 선택해주세요", for: .normal)
            btStartStudy.setTitleColor(UIColor.Foggy, for: .normal)
            btStartStudy.backgroundColor = UIColor.Foggy40
        }
    }
    
    
    @objc func subjectPicker(sender: UIButton){
            
          
        let vc = R.storyboard.preparation.subjectPickerViewController()!
        vc.selectMHIndex = self.selectMHIndex
        vc.selectSubjectIndex = self.selectSubjectIndex
        vc.unitDatas = self.unitDatas
        
        vc.callback = { (name, index) in
            if let i = index {
                self.selectSubjectIndex = i
                self.tvUnitList.reloadData()
            }
        }
            
        // MDC 바텀 시트로 설정
        let bottomSheet = MDCBottomSheetController(contentViewController: vc)
        bottomSheet.mdc_bottomSheetPresentationController?.preferredSheetHeight = 270
        bottomSheet.delegate = self

        // 보여주기
        self.present(bottomSheet, animated: true, completion: nil)
    }
    
    
    @objc func selectHigh(sender: UIButton){
        selectMHIndex = 0
        selectSubjectIndex = 0
        self.tvUnitList.reloadData()
    }
    
    
    @objc func selectMiddle(sender: UIButton){
        selectMHIndex = 1
        selectSubjectIndex = 0
        self.tvUnitList.reloadData()
    }
    
    
    @objc func resetUnitSelect(sender: UIButton){
        
        //ab180 이벤트 보내기
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            var kName = ""
            if self.keyName != "" {
                kName = self.keyName
            } else {
                kName = "키워드 선택 건너뛰기"
            }
            
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: "pulja2.0_cat2select_reset",
                    customs: ["userseq": userSeq, "user_id": userId, "keyword" : kName, "unit2List": self.selectUnitSeqs]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "category: pulja2.0_cat2select_reset", separator: " ")
                print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                print("[ABLog]", "user_id: \(userId)", separator: " ")
                print("[ABLog]", "keyword: \(kName)", separator: " ")
                print("[ABLog]", "unit2List: \(self.selectUnitSeqs)", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
        }
        
        self.resetUnit2Seqs()
        self.tvUnitList.reloadData()
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int{
        
        var cnt = 0
                      
      if let data =  self.unitDatas {
          cnt = data[selectMHIndex].subjects?[selectSubjectIndex].unit1s?.count ?? 0
      }
      
      return cnt
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard section == 0, let headerCell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "Unit2SelectHeaderView") as? Unit2SelectHeaderView else { return nil }
      
        if let data =  self.unitDatas, var subjectName = data[selectMHIndex].subjects?[selectSubjectIndex].name {
            if selectMHIndex == 0 {
                subjectName = "고등\(subjectName)"
            } else {
                let arr = subjectName.components(separatedBy: "-")
                let a1 = arr[0].substring(from: 1, to: 1)
                let a2 = arr[1]
                subjectName = "\(a1)학년 \(a2)학기"
            }
            
            headerCell.btSubject.setTitle(subjectName, for: .normal)
        }
        
        if selectMHIndex == 0 {
            
            headerCell.btHigh.borderColor = UIColor.Purple
            headerCell.btHigh.setTitleColor(UIColor.Purple, for: .normal)
            headerCell.btHigh.backgroundColor = R.color.purple10()
            
            headerCell.btMiddle.borderColor = UIColor.Foggy
            headerCell.btMiddle.setTitleColor(UIColor.Foggy, for: .normal)
            headerCell.btMiddle.backgroundColor = UIColor.white

        } else {
            
            headerCell.btHigh.borderColor = UIColor.Foggy
            headerCell.btHigh.setTitleColor(UIColor.Foggy, for: .normal)
            headerCell.btHigh.backgroundColor = UIColor.white

            
            headerCell.btMiddle.borderColor = UIColor.Purple
            headerCell.btMiddle.setTitleColor(UIColor.Purple, for: .normal)
            headerCell.btMiddle.backgroundColor = R.color.purple10()
        }


        headerCell.btSubject.addTarget(self, action: #selector(subjectPicker(sender:)), for: .touchUpInside)
        headerCell.btHigh.addTarget(self, action: #selector(selectHigh(sender:)), for: .touchUpInside)
        headerCell.btMiddle.addTarget(self, action: #selector(selectMiddle(sender:)), for: .touchUpInside)
        headerCell.btReset.addTarget(self, action: #selector(resetUnitSelect(sender:)), for: .touchUpInside)
        
        
        return headerCell
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return section == 0 ? 220 : 0
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let data =  self.unitDatas, let cnt = data[selectMHIndex].subjects?[selectSubjectIndex].unit1s?[section].unit2s?.count else {
            return 0
        }
        return cnt
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = indexPath.section
        let index = indexPath.row

//        let cell:Unit2SelectTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Unit2SelectTableViewCell", for: indexPath) as! Unit2SelectTableViewCell
//
//
//        let  checkState = !cell.cbUnit2.
//
//        cell.cbUnit2.checkState = checkState
                   
        print("indexPath.section \(indexPath.section), indexPath.row \(indexPath.row)")
                   
        guard let data = self.unitDatas, let unit1 = data[self.selectMHIndex].subjects?[self.selectSubjectIndex].unit1s?[section], let unit2 = unit1.unit2s?[index]  else {
            return
            
        }
                   
        let checkState = !(self.unitDatas![self.selectMHIndex].subjects![self.selectSubjectIndex].unit1s![section].unit2s![index].selected ?? false)
        
        self.unitDatas![self.selectMHIndex].subjects![self.selectSubjectIndex].unit1s![section].unit2s![index].selected = checkState
        
        print("checkState \(checkState)")

       
       if checkState {
           
           self.selectedUnit2Infos.append(unit2)
           
       } else {
           guard let idx = self.selectedUnit2Infos.firstIndex(where: { $0.unit2Seq == unit2.unit2Seq }) else {
               return
           }
           self.selectedUnit2Infos.remove(at: idx)
           
       }
       self.setButtonInfo()
        
        self.tvUnitList.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        let index = indexPath.row
        
        let cell:Unit2SelectTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Unit2SelectTableViewCell", for: indexPath) as! Unit2SelectTableViewCell
        cell.selectionStyle = .none
        if let data =   self.unitDatas, let unit1 = data[selectMHIndex].subjects?[selectSubjectIndex].unit1s?[section] {
            
            if index == 0 {
                if var unit1Name = unit1.name {
                    if let index = unit1Name.firstIndex(of: " ") {
                        unit1Name.insert(".", at: index)
                    }
                    cell.unit1Name = unit1Name
                    cell.lbUnit1Name.text = unit1Name
                }
                cell.unit1View.isHidden = false
            } else {
                cell.unit1View.isHidden = true
            }
            
            if let unit2 = unit1.unit2s?[index] {
                //cell.unit2 = unit2
            }
            
        }

        
        cell.checkCallback = { isCheck in
            print("callback \(isCheck)")
            print("indexPath.section \(indexPath.section), indexPath.row \(indexPath.row)")
            
            guard let data = self.unitDatas, let unit1 = data[self.selectMHIndex].subjects?[self.selectSubjectIndex].unit1s?[section], let unit2 = unit1.unit2s?[index]  else {
                return
            }
            
            self.unitDatas![self.selectMHIndex].subjects![self.selectSubjectIndex].unit1s![section].unit2s![index].selected = isCheck
            
            if isCheck {
                
                self.selectedUnit2Infos.append(unit2)
                
            } else {
               guard let idx = self.selectedUnit2Infos.firstIndex(where: { $0.unit2Seq == unit2.unit2Seq }) else {
                    return
               }
               self.selectedUnit2Infos.remove(at: idx)
            }
            self.setButtonInfo()
            
//            self.tvUnitList.reloadData()
        }
        
//        if indexPath.row == 0 && indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
//            cell.leftView.isHidden = true
//            cell.rightView.isHidden = true
//            cell.bottomView.isHidden = false
//       } else if indexPath.row == 0 {
//           cell.leftView.isHidden = true
//           cell.rightView.isHidden = true
//           cell.bottomView.isHidden = false
//       } else if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
//           cell.leftView.isHidden = true
//           cell.rightView.isHidden = true
//           cell.bottomView.isHidden = true
//       } else {
//           cell.leftView.isHidden = false
//           cell.rightView.isHidden = false
//           cell.bottomView.isHidden = false
//       }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cornerRadius: CGFloat = 10.0
               cell.backgroundColor = UIColor.clear
               let layer: CAShapeLayer = CAShapeLayer()
               let pathRef: CGMutablePath = CGMutablePath()
               //dx leading an trailing margins
               let bounds: CGRect = cell.bounds.insetBy(dx: 0, dy: 0)
               var addLine: Bool = false
               
               if indexPath.row == 0 && indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
                   pathRef.__addRoundedRect(transform: nil, rect: bounds, cornerWidth: cornerRadius, cornerHeight: cornerRadius)
               } else if indexPath.row == 0 {
                   pathRef.move(to: CGPoint(x: bounds.minX, y: bounds.maxY))
                   pathRef.addArc(tangent1End: CGPoint(x: bounds.minX, y: bounds.minY),
                                  tangent2End: CGPoint(x: bounds.midX, y: bounds.minY),
                                  radius: cornerRadius)
                   
                   pathRef.addArc(tangent1End: CGPoint(x: bounds.maxX, y: bounds.minY),
                                  tangent2End: CGPoint(x: bounds.maxX, y: bounds.midY),
                                  radius: cornerRadius)
                   pathRef.addLine(to: CGPoint(x: bounds.maxX, y: bounds.maxY))
                   addLine = true
               } else if indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
                   pathRef.move(to: CGPoint(x: bounds.minX, y: bounds.minY))
                   pathRef.addArc(tangent1End: CGPoint(x: bounds.minX, y: bounds.maxY),
                                  tangent2End: CGPoint(x: bounds.midX, y: bounds.maxY),
                                  radius: cornerRadius)
                   
                   pathRef.addArc(tangent1End: CGPoint(x: bounds.maxX, y: bounds.maxY),
                                  tangent2End: CGPoint(x: bounds.maxX, y: bounds.midY),
                                  radius: cornerRadius)
                   pathRef.addLine(to: CGPoint(x: bounds.maxX, y: bounds.minY))
               } else {
//                   pathRef.addRect(bounds)
//                   addLine = true
                   
               }
               
               layer.path = pathRef
               layer.strokeColor = UIColor.Foggy.cgColor
               layer.lineWidth = 1.2
               layer.fillColor = UIColor.white.cgColor
               
               if addLine == true {
                   let lineLayer: CALayer = CALayer()
                   let lineHeight: CGFloat = (1 / UIScreen.main.scale)
                   lineLayer.frame = CGRect(x: bounds.minX, y: bounds.size.height - lineHeight, width: bounds.size.width, height: lineHeight)
                   lineLayer.backgroundColor = UIColor.clear.cgColor
                   layer.addSublayer(lineLayer)
               }
               
               let backgroundView: UIView = UIView(frame: bounds)
               backgroundView.layer.insertSublayer(layer, at: 0)
               backgroundView.backgroundColor = .white
               cell.backgroundView = backgroundView

    }
    
    
    
   

}
