//
//  ProblemCollectionDetailViewController.swift
//  pulja
//
//  Created by HUN on 2023/01/09.
//

import UIKit
import Kingfisher

class ProblemCollectionDetailViewController: PuljaBaseViewController, UIScrollViewDelegate {
    // MARK: - Pad
    @IBOutlet weak var descriptionTop: NSLayoutConstraint! // 100
    @IBOutlet weak var titleTop: NSLayoutConstraint! // 120
    @IBOutlet weak var stackViewBottom: NSLayoutConstraint! // 200
    private func setPadConstants() {
        if isPad {
            if UIDevice.current.orientation.isPortrait {
                //descriptionTop.constant = 32
                titleTop.constant = 120
                stackViewBottom.constant = 200
            } else {
                //descriptionTop.constant = 32
                titleTop.constant = 60
                stackViewBottom.constant = 60
            }
        }
    }
    
    // MARK: - BeforeStudy
    @IBOutlet weak var lbEmptyUnitName: UILabel!
    @IBOutlet weak var lbEmptyFramingTitle: UILabel!
    @IBOutlet weak var emptyStudyStatusBadge: UIView!
    @IBOutlet weak var lbEmptyStudyStatus: UILabel!
    @IBOutlet weak var lbEmptyDescription: UILabel!
    @IBOutlet weak var lbEmptyTotalQuestion: UILabel!
    @IBOutlet weak var lbEmptyStudyDays: UILabel!
    @IBOutlet weak var lbEmptyDifficulty: UILabel!
    
    private func setBeforeStudyData(data: CollectionDetail) {
        lbEmptyUnitName.text = data.unitName
        lbEmptyFramingTitle.text = data.framingTitle
        emptyStudyStatusBadge.backgroundColor = UIColor.Foggy20
        lbEmptyStudyStatus.text = data.studyStatusText
        lbEmptyStudyStatus.textColor = UIColor.Dust
        
        if isPad {
            lbEmptyDescription.text = data.framingDescTab
        } else {
            lbEmptyDescription.text = data.framingDescMob
        }
        
        lbEmptyTotalQuestion.text = "\(data.totalQuestionCnt ?? 0)개"
        lbEmptyStudyDays.text = "\(data.studyDays ?? 0)일"
        lbEmptyDifficulty.text = data.difficultyText
        
        nextButtonDivider.isHidden = false
        nextButton.isHidden = false
        nextButton.setTitle("시작하기", for: .normal)
    }
    
    // MARK: - Started Study
    @IBOutlet weak var lbUnitName: UILabel!
    @IBOutlet weak var lbFramingTitle: UILabel!
    @IBOutlet weak var studyStatusBadge: UIView!
    @IBOutlet weak var lbStudyStatus: UILabel!
    @IBOutlet weak var lbTotalQuestion: UILabel!
    @IBOutlet weak var lbStudyDays: UILabel!
    @IBOutlet weak var lbDifficulty: UILabel!
    
    @IBOutlet weak var lbProgress1: UILabel!
    @IBOutlet weak var lbProgress2: UILabel!
    @IBOutlet weak var lbProgress3: UILabel!
    @IBOutlet weak var progress: UIView!
    @IBOutlet weak var progressFill: UIView!
    @IBOutlet weak var progressWidth: NSLayoutConstraint!
    
    @IBOutlet weak var lbCorrectRate1: UILabel!
    @IBOutlet weak var lbCorrectRate2: UILabel!
    @IBOutlet weak var lbCorrectRate3: UILabel!
    
    @IBOutlet weak var nextButtonHeight: NSLayoutConstraint!
    private func setStartStudyData(data: CollectionDetail) {
        lbUnitName.text = data.unitName
        lbFramingTitle.text = data.framingTitle
        
        let totalCnt = data.totalQuestionCnt ?? 0
        let studyCnt = data.userStudyQuestionCnt ?? 0
        let correctCnt = data.userCorrectQuestionCnt ?? 0
        
        //self.problemSeq = data.collectionSeq ?? 0
        lbProgress2.text = "\(studyCnt)문제"
        lbProgress3.text = "\(studyCnt)/\(totalCnt)"
        lbTotalQuestion.text = "\(totalCnt)개"
        lbStudyDays.text = "\(data.studyDays ?? 0)일"
        lbDifficulty.text = data.difficultyText
        lbStudyStatus.text = data.studyStatusText ?? ""
        
        if studyCnt == 0 {
            lbProgress1.text = "0%"
            progressWidth.constant = 0
            nextButtonDivider.isHidden = false
            nextButton.isHidden = false
        } else if totalCnt <= studyCnt {
            lbProgress1.text = "100%"
            lbProgress2.text = "\(totalCnt)문제"
            lbProgress3.text = "\(totalCnt)/\(totalCnt)"
            progressWidth.constant = progress.frame.width
            progressFill.roundCorners(cornerRadius: 10, maskedCorners: [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner])
            
            studyStatusBadge.backgroundColor = UIColor.Green10
            lbStudyStatus.textColor = UIColor.Green
            
            nextButtonDivider.isHidden = true
            nextButton.isHidden = true
            nextButtonHeight.constant = 0
        } else {
            let rate = Double(100) / Double(totalCnt) * Double(studyCnt)
            let per = Int(rate.rounded())
            lbProgress1.text = "\(per)%"
            
            let width = progress.frame.width
            let constant = CGFloat(width / 100 * CGFloat(per)) + 8
            let checkWidth = width - 8
            if checkWidth > constant {
                progressWidth.constant = constant
            } else {
                progressWidth.constant = checkWidth
            }
            
            progressFill.roundCorners(cornerRadius: 10, maskedCorners: [.layerMinXMinYCorner, .layerMinXMaxYCorner])
            
            studyStatusBadge.backgroundColor = UIColor.Purple10
            lbStudyStatus.textColor = UIColor.Purple
            
            nextButtonDivider.isHidden = false
            nextButton.isHidden = false
            nextButton.setTitle("이어 풀기", for: .normal)
        }
        let rate = Double(100) / Double(studyCnt) * Double(correctCnt)
        lbCorrectRate1.text = "\(Int(rate.rounded()))%"
        lbCorrectRate2.text = "\(studyCnt)문제 중 "
        lbCorrectRate3.text = "\(correctCnt)문제"
    }
    
    // MARK: - Override
    var cameFrom = ""
    var collectionSeq = 0
    var problemSeq:Int?
    @IBOutlet weak var svCollection: UIScrollView!
    @IBOutlet weak var emptyCollectionView: UIView!
    @IBOutlet weak var nextButtonDivider: UIView!
    @IBOutlet weak var nextButton: UIButton!
    
    private var isPad = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        svCollection.delegate = self
        svCollection.isHidden = true
        emptyCollectionView.isHidden = true
        nextButtonDivider.isHidden = true
        nextButton.isHidden = true
        isPad = UIDevice.current.userInterfaceIdiom == .pad
        
        //ab180 이벤트 보내기
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: "pulja2.0_collection_detail_enter",
                    customs: ["userseq": userSeq, "user_id": userId, "collectionSeq": self.collectionSeq]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "category: pulja2.0_collection_detail_enter", separator: " ")
                print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                print("[ABLog]", "user_id: \(userId)", separator: " ")
                print("[ABLog]", "collectionSeq: \(self.collectionSeq)", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        setPadConstants()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        CommonUtil.tabbarController?.tabBar.isHidden = true
        
        LoadingView.show()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
            self.getCollectionDetail()
            self.setPadConstants()
        }
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    // MARK: - API
    private func getCollectionDetail() {
        if let userSeq = self.myInfo?.userSeq {
            CollectionAPI.shared.collectionDetail(collectionSeq: collectionSeq, userSeq: Int(userSeq)).done { res in
                if let data = res.data {
                    let notStarted = data.studyStatusText == "학습 전"
                    self.svCollection.isHidden = notStarted
                    self.emptyCollectionView.isHidden = !notStarted
                    if let collectionSeq = data.collectionSeq {
                        self.collectionSeq = collectionSeq
                    }
                    
                    self.problemSeq = data.problemSeq
                    
                    if notStarted {
                        self.setBeforeStudyData(data: data)
                    } else {
                        self.setStartStudyData(data: data)
                    }
                }
            }.catch { err in
                LoadingView.hide()
            }.finally {
                LoadingView.hide()
            }
        }
    }
    
    // MARK: - Action
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    var delegate : ProblemCollectionListViewController?
    @IBAction func nextButtonAction(_ sender: Any) {
        if collectionSeq == 0 { return }
        
        delegate?.isUpdate = true
        let vc = R.storyboard.preparation.preparationMultipleChoiceViewController()!
        vc.cameFrom = self.cameFrom
        vc.isFromCollection = true
        vc.collectionSeq = collectionSeq
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func problemListAction(_ sender: Any) {
        if let resultVC = R.storyboard.review.problemResultViewController() {
            
            //ab180 이벤트 보내기
            if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
                if Const.isAirBridge {
                    CommonUtil.shared.ABEvent(
                        category: "pulja2.0_collection_detail_review_next",
                        customs: ["userseq": userSeq, "user_id": userId, "collectionSeq": self.collectionSeq]
                    )
                } else {
                    print("[ABLog]", "-----------------------------------------", separator: " ")
                    print("[ABLog]", "category: pulja2.0_collection_detail_review_next", separator: " ")
                    print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                    print("[ABLog]", "user_id: \(userId)", separator: " ")
                    print("[ABLog]", "collectionSeq: \(self.collectionSeq)", separator: " ")
                    print("[ABLog]", "-----------------------------------------", separator: " ")
                }
            }
            
            if let problemSeq = self.problemSeq {
                resultVC.problemSeq = problemSeq
            }
            self.navigationController?.pushViewController(resultVC, animated: true)
        }
    }
}

extension UIView {
    func roundCorners(cornerRadius: CGFloat, maskedCorners: CACornerMask) {
        clipsToBounds = true
        layer.cornerRadius = cornerRadius
        layer.maskedCorners = CACornerMask(arrayLiteral: maskedCorners)
    }
}
