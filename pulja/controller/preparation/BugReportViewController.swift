//
//  BugReportViewController.swift
//  pulja
//
//  Created by 김병헌 on 2022/10/04.
//

import UIKit

class BugReportViewController: PuljaBaseViewController, UIPickerViewDelegate , UIPickerViewDataSource, UITextViewDelegate {
    
    var callback: (_ isSend: Bool) -> Void = { _ in }
    
    var questionId = -1
    var movieCode:String? = nil
    var reportType:String? = "p"
    
    var videoErrorType = ["강의 내용에 오류가 있어요.", "원활한 영상 재생에 문제가 있어요.", "기타" ]
    var problemErrorType = ["문제 자체가 잘못된 것 같아요.","정답에 오류가 있어요.", "해설에 오류가 있어요.", "기타" ]
    var pickerData:[String] = []
    
    var pickerView = UIPickerView()
    var pickerToolbar : UIToolbar = UIToolbar()
    var selectErrorType = ""
    var characterCount = 0
    var selectRow = 0
    
    @IBOutlet weak var lbErrorTitle: UILabel!
    @IBOutlet weak var btSelectError: ButtonLTextRImage!
    
    
    @IBOutlet weak var lbReportTitle: UILabel!
    @IBOutlet weak var tvReportContents: UITextView!
    
    @IBOutlet weak var remainCountLabel: UILabel!
    
    @IBOutlet weak var submitHeightSpace: NSLayoutConstraint!
    
    @IBOutlet weak var btSubmit: UIButton!
    
    
    @IBOutlet weak var topHeightSpace: NSLayoutConstraint!
    var textViewPlaceHolder = "ex) 주관식 답이 입력이 안돼요"
    var defaultSpace:CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if reportType == "p" {
            pickerData = problemErrorType
            lbErrorTitle.text = "어떤 오류가 있나요?"
            textViewPlaceHolder = "ex) 주관식 답이 입력이 안돼요"
        } else {
            pickerData = videoErrorType
            lbErrorTitle.text = "강의 영상에 어떤 오류가 있나요?"
            textViewPlaceHolder = "ex) 영상이 재생되다가 중간에 끊겨요. "
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

        
        tvReportContents.delegate = self
        
        tvReportContents.textColor = .Foggy
        tvReportContents.text = textViewPlaceHolder
        tvReportContents.textContainerInset = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16);
        
        defaultSpace = submitHeightSpace.constant
    }
    

    @IBAction func goBack(_ sender: Any) {
        dismiss(animated: true)
//        self.navigationController?.popViewController(animated: true)
    }
    

    
    @IBAction func selectTypeAction(_ sender: Any) {
        
        pickerView = UIPickerView.init()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = UIColor.white
        pickerView.setValue(UIColor.black, forKey: "textColor")
        pickerView.autoresizingMask = .flexibleWidth
        pickerView.contentMode = .center
        pickerView.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 220, width: UIScreen.main.bounds.size.width, height: 220)
           self.view.addSubview(pickerView)
                   
        pickerToolbar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 220, width: UIScreen.main.bounds.size.width, height: 50))
        pickerToolbar.sizeToFit()
        pickerToolbar.barStyle = .default
        pickerToolbar.isTranslucent = true
        pickerToolbar.items = [UIBarButtonItem.init(title: "확인", style: .done, target: self, action: #selector(onPickDone))]
           self.view.addSubview(pickerToolbar)
        
        
        pickerView.selectRow(selectRow, inComponent: 0, animated: false)
        self.view.endEditing(true)
    }
    
    
    
    // 피커뷰 > 확인 클릭
     @objc func onPickDone() {
         
         if selectErrorType == "" {
             selectErrorType = pickerData[selectRow]
         }
         
         btSelectError.setTitle(selectErrorType, for: .normal)
         btSelectError.setTitleColor(UIColor.Dust, for: .normal)

         pickerToolbar.removeFromSuperview()
         pickerView.removeFromSuperview()
         submitEnable()
     }
     
     // 피커뷰 > 취소 클릭
     @objc func onPickCancel() {
        pickerToolbar.removeFromSuperview()
        pickerView.removeFromSuperview()
//         selectErrorType = ""
         submitEnable()
     }
  



     // 피커뷰의 구성요소(컬럼) 수
     func numberOfComponents(in pickerView: UIPickerView) -> Int {
         return 1    // 구성요소(컬럼)로 지역만 있으므로 1을 리턴
     }
     
     // 구성요소(컬럼)의 행수
     func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
         return pickerData.count
     }
  
     // 피커뷰에 보여줄 값 전달
     func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
         return pickerData[row]
     }
     
     // 피커뷰에서 선택시 호출
     func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
         selectErrorType = pickerData[row]
         selectRow = row
         submitEnable()
     }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == textViewPlaceHolder {
            textView.text = nil
            textView.textColor = .puljaBlack
            
        }
        onPickCancel()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            textView.text = textViewPlaceHolder
            textView.textColor = .Foggy
            
            updateCountLabel(characterCount: 0)
        }
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let inputString = text.trimmingCharacters(in: .whitespacesAndNewlines)
        guard let oldString = textView.text, let newRange = Range(range, in: oldString) else { return true }
        let newString = oldString.replacingCharacters(in: newRange, with: inputString).trimmingCharacters(in: .whitespacesAndNewlines)

        let characterCount = newString.count
        guard characterCount <= 200 else { return false }
        updateCountLabel(characterCount: characterCount)

        return true
    }
    
    private func updateCountLabel(characterCount: Int) {
        self.characterCount = characterCount
         remainCountLabel.text = "\(characterCount)"
        if characterCount > 0 {
            remainCountLabel.textColor = .puljaBlack
        } else {
            remainCountLabel.textColor = .grey
        }
        
        
        submitEnable()
     }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
                 
         guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
             
             // if keyboard size is not available for some reason, dont do anything
            return
         }
         
         
        self.view.frame.origin.y = 0 - keyboardSize.height
        submitHeightSpace.constant = CGFloat(39)
        submitHeightSpace.priority = UILayoutPriority(1000)
        
        topHeightSpace.constant = defaultSpace - CGFloat(39) + CGFloat(20)
        topHeightSpace.priority = UILayoutPriority(250)
        
        tvReportContents.layer.borderColor = UIColor.OffPurple.cgColor
     }

     @objc func keyboardWillHide(notification: NSNotification) {
         self.view.frame.origin.y = 0
         submitHeightSpace.constant = defaultSpace
         submitHeightSpace.priority = UILayoutPriority(250)

         topHeightSpace.constant = CGFloat(20)
         topHeightSpace.priority = UILayoutPriority(1000)
         
         tvReportContents.layer.borderColor = UIColor.Foggy.cgColor
     }
    
    func submitEnable(){
        if selectErrorType != "" && characterCount > 0 {
            btSubmit.setTitleColor(UIColor.white, for: .normal)
            btSubmit.backgroundColor = UIColor.OffPurple
        } else {
            btSubmit.setTitleColor(UIColor.Foggy, for: .normal)
            btSubmit.backgroundColor = UIColor.Foggy20
        }

    }
    
    
    @IBAction func submitAction(_ sender: Any) {
        if selectErrorType != "" && characterCount > 0 {
            
            guard let userSeq = self.myInfo?.userSeq else { return }
            
            if let contents = tvReportContents.text {
                LoadingView.show()
                let modelAndOs = "\(UIDevice.modelName), OS \(getDeviceOsVersion())"
                let req = ErrorReportReq(errorType: self.selectErrorType, questionId: self.questionId, movieCode: self.movieCode, reportContent: contents, reportDevice: UIDevice.modelName, userSeq: Int(userSeq))
                
                CommonAPI.shared.errorReport(errorReportReq: req, reportType: self.reportType!).done { res in
                    self.dismiss(animated: true) {
                        self.callback(true)
                    }
                }.catch { error in
                    print(error.localizedDescription)
                }.finally {
                    LoadingView.hide()
                }

            }
        }
    }
    
    // MARK: - [디바이스 소프트웨어 OS 버전 확인]
    func getDeviceOsVersion() -> String {
        let returnData = String(describing: UIDevice.current.systemVersion)
        return returnData
    }
        
    
}

public extension UIDevice {

    /*
     2022. 09. 22 Kimjiwook
     위키의 애플 기기들 모델 정리
     https://www.theiphonewiki.com/wiki/Models
     iOS 지원 지원 기기목록
     https://support.apple.com/ko-kr/guide/iphone/iphe3fa5df43/16.0/ios/16.0
     */
    // 정보 가져온 URL
    // https://stackoverflow.com/questions/26028918/how-to-determine-the-current-iphone-device-model
    @objc static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            switch identifier {
                /*
                 1. iPod
                 */
            case "iPod1,1":                                       return "iPod touch"
            case "iPod2,1":                                       return "iPod touch (2nd generation)"
            case "iPod3,1":                                       return "iPod touch (3rd generation)"
            case "iPod4,1":                                       return "iPod touch (4th generation)"
            case "iPod5,1":                                       return "iPod touch (5th generation)"
            case "iPod7,1":                                       return "iPod touch (6th generation)"
            case "iPod9,1":                                       return "iPod touch (7th generation)"
                
                /*
                 2. iPhone
                 */
            case "iPhone1,1":                                     return "iPhone"
            case "iPhone1,2":                                     return "iPhone 3G"
            case "iPhone2,1":                                     return "iPhone 3GS"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":           return "iPhone 4"
            case "iPhone4,1":                                     return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                        return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                        return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                        return "iPhone 5s"
                
            case "iPhone7,2":                                     return "iPhone 6"
            case "iPhone7,1":                                     return "iPhone 6 Plus"
                // iOS 12 아래는 문서도 이제 남아있지 않내요
                // ---- iOS 12 -> iOS 13 올라가면서 에서 위에는 지원종료 --
                
                // iOS 15 까진 지원 (iPhone 6s ~ 7 지원종료)
            case "iPhone8,1":                                     return "iPhone 6s"
            case "iPhone8,2":                                     return "iPhone 6s Plus"
                
            case "iPhone8,4":                                     return "iPhone SE (1st generation)"
                
            case "iPhone9,1", "iPhone9,3":                        return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                        return "iPhone 7 Plus"
                
                // iOS 16 까진 지원중.. iPhone 8이상
                
            case "iPhone10,1", "iPhone10,4":                      return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                      return "iPhone 8 Plus"
                
            case "iPhone10,3", "iPhone10,6":                      return "iPhone X"
            
            case "iPhone11,8":                                    return "iPhone XR"
            case "iPhone11,2":                                    return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                      return "iPhone XS Max"
                
            case "iPhone12,1":                                    return "iPhone 11"
            case "iPhone12,3":                                    return "iPhone 11 Pro"
            case "iPhone12,5":                                    return "iPhone 11 Pro Max"
                
            case "iPhone12,8":                                    return "iPhone SE (2nd generation)"
                
            case "iPhone13,1":                                    return "iPhone 12 mini"
            case "iPhone13,2":                                    return "iPhone 12"
            case "iPhone13,3":                                    return "iPhone 12 Pro"
            case "iPhone13,4":                                    return "iPhone 12 Pro Max"
                
            case "iPhone14,4":                                    return "iPhone 13 mini"
            case "iPhone14,5":                                    return "iPhone 13"
            case "iPhone14,2":                                    return "iPhone 13 Pro"
            case "iPhone14,3":                                    return "iPhone 13 Pro Max"
                
            case "iPhone14,6":                                    return "iPhone SE (3rd generation)"
                
            case "iPhone14,7":                                    return "iPhone 14"
            case "iPhone14,8":                                    return "iPhone 14 Plus"
            case "iPhone15,2":                                    return "iPhone 14 Pro"
            case "iPhone15,3":                                    return "iPhone 14 Pro Max"
                /*
                 3. iPad
                 */
                // iPad
            case "iPad1,1":                                       return "iPad"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":      return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":                 return "iPad (3rd generation)"
            case "iPad3,4", "iPad3,5", "iPad3,6":                 return "iPad (4th generation)"
            case "iPad6,11", "iPad6,12":                          return "iPad (5th generation)"
            case "iPad7,5", "iPad7,6":                            return "iPad (6th generation)"
            case "iPad7,11", "iPad7,12":                          return "iPad (7th generation)"
            case "iPad11,6", "iPad11,7":                          return "iPad (8th generation)"
            case "iPad12,1", "iPad12,2":                          return "iPad (9th generation)"
                // iPad Air
            case "iPad4,1", "iPad4,2", "iPad4,3":                 return "iPad Air"
            case "iPad5,3", "iPad5,4":                            return "iPad Air 2"
            case "iPad11,3", "iPad11,4":                          return "iPad Air (3rd generation)"
            case "iPad13,1", "iPad13,2":                          return "iPad Air (4th generation)"
            case "iPad13,16", "iPad13,17":                        return "iPad Air (5th generation)"
                // iPad Mini
            case "iPad2,5", "iPad2,6", "iPad2,7":                 return "iPad mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":                 return "iPad mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":                 return "iPad mini 3"
            case "iPad5,1", "iPad5,2":                            return "iPad mini 4"
            case "iPad11,1", "iPad11,2":                          return "iPad mini (5th generation)"
            case "iPad14,1", "iPad14,2":                          return "iPad mini (6th generation)"
                // iPad Pro
            case "iPad6,7", "iPad6,8":                            return "iPad Pro (12.9-inch)"
            case "iPad6,3", "iPad6,4":                            return "iPad Pro (9.7-inch)"
            
            case "iPad7,1", "iPad7,2":                            return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad7,3", "iPad7,4":                            return "iPad Pro (10.5-inch)"

            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":      return "iPad Pro (11-inch)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":      return "iPad Pro (12.9-inch) (3rd generation)"
                
            case "iPad8,9", "iPad8,10":                           return "iPad Pro (11-inch) (2nd generation)"
            case "iPad8,11", "iPad8,12":                          return "iPad Pro (12.9-inch) (4th generation)"
                                
            case "iPad13,4", "iPad13,5", "iPad13,6", "iPad13,7":  return "iPad Pro (11-inch) (3rd generation)"
            case "iPad13,8", "iPad13,9", "iPad13,10", "iPad13,11":return "iPad Pro (12.9-inch) (5th generation)"
            
                /*
                 #. 예외
                 */
            case "i386", "x86_64", "arm64":                       return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                              return identifier
            }
        }
        
        return mapToDevice(identifier: identifier)
    }()
    
    
    
}
