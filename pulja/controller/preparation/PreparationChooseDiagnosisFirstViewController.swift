//
//  preparationChooseDiagnosisFirstViewController.swift
//  pulja
//
//  Created by kwonilko on 2022/07/27.
//

import UIKit
import OrderedCollections

class PreparationChooseDiagnosisFirstViewController: UIViewController {

    
    @IBOutlet weak var tfSearchText: UILabel!
    var units: [UnitInfo] = []
//    var subjects : [SubjectInfo] = []
    
    var processedUnits : OrderedDictionary<String, [UnitInfo]> = [:]
    
    @IBOutlet weak var tfSearchView: UIView!
    
    
    
    @IBOutlet weak var nextButton: DesignableButton!
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        CommonUtil.tabbarController?.tabBar.isHidden = true
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        CommonUtil.tabbarController?.bottomNavBar.isHidden = false
        CommonUtil.tabbarController?.tabBar.isHidden = false
        super.viewWillDisappear(animated)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.searchUnit(_:)))
        self.tfSearchView.addGestureRecognizer(gesture)
        getUnitInfo()
        
    }
    
    //과목 - 중단원 - 대단원 정보를 전부 받아서 다음 컨트롤러에 전달
    func getUnitInfo() {
        
        CommonAPI.shared.unitList(unit2Keyword: "").done {
            res in
            self.units = res.data!
            
        }.catch {
            err in print(err.localizedDescription)
        }.finally {
            
            // { 과목명 : [ unitInfo] } 로 정렬
            let reference = ["수학(상)" : "고등수학(상)", "수학(하)":
                                "고등수학(하)", "수학 I":"수학 I", "수학 II": "수학 II"]
            for idx in 0..<self.units.count {
                guard let subName = reference[self.units[idx].subjectName!] else { return }
                if self.processedUnits.keys.contains(subName) {
                    self.processedUnits[subName]! += [self.units[idx]]
                } else {
                    self.processedUnits[subName] = []
                    self.processedUnits[subName]! = [self.units[idx]]
                }
            }
        }
        
        
    }
    
    func activateNextButton() {
        nextButton.isEnabled = true
        nextButton.setBackgroundColor(.Purple, for: .normal)
        nextButton.setTitleColor(.white, for: .normal)
        
    }
    
    func deactivateNextButton() {
        nextButton.isEnabled = false
        nextButton.setBackgroundColor(.Foggy, for: .normal)
        nextButton.setTitleColor(.Dust, for: .normal)
        
    }
    
    
    @IBAction func lbNext(_ sender: Any) {
        
        let vc = R.storyboard.preparation.preparationDiagStartViewController()!
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    
    @objc func searchUnit(_ sender:UIGestureRecognizer) {
//        let vc = R.storyboard.preparation.preparationUnitSelectionViewController()!
////        vc.selectedUnitInfoArr = self.selectedUnitInfoArr
////        vc.units = self.units
////        vc.subjects = self.subjects
//               
//        vc.selectUnitInfoCallback = { result in
//            
//            //탭바 숨김
//            CommonUtil.tabbarController?.bottomNavBar.isHidden = true
//            CommonUtil.tabbarController?.tabBar.isHidden = true
//            
//            self.tfSearchText.text = result
//            self.tfSearchText.textColor = UIColor.puljaBlack
//            self.tfSearchText.font = UIFont.systemFont(ofSize: 16.0, weight: .bold)
//            self.activateNextButton()
//
//        }
//        vc.processedUnits = self.processedUnits
//        present(vc, animated: true, completion: nil)
////        presentPanModal(vc)
    }
    
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func lbBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
    

}
