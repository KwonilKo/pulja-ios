//
//  PreparationAnswerViewController.swift
//  pulja
//
//  Created by kwonilko on 2022/09/16.
//

import UIKit
import Tabman
import Pageboy
import PencilKit
import WebKit
import AirBridge
import Kingfisher

class PreparationAnswerViewController: PuljaBaseViewController, WKUIDelegate, WKScriptMessageHandler, WKNavigationDelegate, PKToolPickerObserver, PKCanvasViewDelegate
{
    var window: UIWindow?

    @IBOutlet weak var questionText: UIButton!
    @IBOutlet weak var lineview: UIView!
    @IBOutlet weak var answerText: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var answerProgressView: UIView!
    @IBOutlet weak var questionProgressView: UIView!
    @IBOutlet weak var tabView: UIView!
    
    
    @IBOutlet weak var questionContents: UIView!
    @IBOutlet weak var answerContents: UIView!
    var toolPicker : PKToolPicker!
    
    @IBOutlet weak var pencilToolView: UIView!
    @IBOutlet weak var btPencilClear: UIButton!
    @IBOutlet weak var btPencilClose: UIButton!
    @IBOutlet weak var btPencilUndo: UIButton!
    @IBOutlet weak var btPencilRedo: UIButton!
    @IBOutlet weak var categoryLb: UILabel!
    @IBOutlet weak var pkCanvasView: PKCanvasView!
    @IBOutlet weak var explainView: UIView!
    @IBOutlet weak var explainVideoHeight: NSLayoutConstraint!

    @IBOutlet weak var wvExplain: WKWebView!
    @IBOutlet weak var explainScrollView: UIScrollView!
    @IBOutlet weak var ivExplain: UIImageView!
    @IBOutlet weak var topview: UIView!
    @IBOutlet weak var nextBtnBG: UIView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var nextBtnBgHeight: NSLayoutConstraint! // default 74
    @IBOutlet weak var nextBtnHeight: NSLayoutConstraint! // default 50
    @IBOutlet weak var wrongIcon: UIImageView!
    @IBOutlet weak var realAnswerLb: UILabel!
    
    @IBOutlet weak var ivProblem: UIImageView!
    @IBOutlet weak var wrongCorrectView: UIView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var wrongCorrectLb: UILabel!
    @IBOutlet weak var wrongCorrectDefaultLb: UILabel!
    @IBOutlet weak var wrongCorrectDefaultWidth: NSLayoutConstraint! // 81
    @IBOutlet weak var btPencil: UIButton!
    @IBOutlet weak var questionScrollView: UIScrollView!
    @IBOutlet weak var btBookMark: UIButton!
    @IBOutlet weak var explainEmptyView: UIView!
    
    func bottomButtonHidden(isHidden: Bool) {
        if isHidden {
            self.nextBtn.isHidden = true
            self.nextBtnBG.isHidden = true
            self.nextBtnHeight.constant = 0
            self.nextBtnBgHeight.constant = 0
        } else {
            self.nextBtn.isHidden = false
            self.nextBtnBG.isHidden = false
            self.nextBtnHeight.constant = 50
            self.nextBtnBgHeight.constant = 74
        }
    }
    
    var dispatchWorkItem : DispatchWorkItem?
    var cameFromAISolve = true // true -> 학습 중 틀렸을 때 해설화면 나오는 경우, false -> 복습
    var cameFrom = ""
    var answerLb = UILabel()
    
    var unit2Seqs:[Int] = []
    var keywordName : String = ""
    var number = -1
    var problemSeq = -1
    
    // MARK: - Collection
    var collectionExplain : CollectionExplain?
    var isLastCollection = false
    
    // MARK: - Review
    var reviewQuestion  : ReviewQuestion?
    var reqParam : AnswerInfoReq?
    var answerInfo : AnswerInfo?
    var heroId : String?
    var detailBookMarkCallback : (_ result : Int?) -> Void = { _ in }
    
    var problemImg : KFCrossPlatformImage?
    var answerImg : KFCrossPlatformImage?
    
    var currentTab = 0
    
    var seeQuestion = true // true -> 문제보기가 default
    {
        didSet {
            if self.cameFromAISolve //학습
            {
                wrongIcon.image = UIImage(named: "wrongIcon")
                wrongCorrectView.backgroundColor = UIColor.Red
                wrongCorrectLb.textColor = UIColor.red10
                wrongCorrectLb.font = UIFont.systemFont(ofSize: 14.0, weight: .semibold)
                wrongCorrectDefaultLb.textColor = UIColor.red10
                
                if seeQuestion {
                    
                    //questionScrollView.isHidden = false
                    questionContents.isHidden = false
                    answerContents.isHidden = true
                    btPencil.isHidden = false
                    
                    var isCorrect = "N"
                    
                    if let answer = self.preparationAnswer {
                        isCorrect = answer.isCorrect ?? "N"
                    } else if let explain = self.collectionExplain {
                        isCorrect = explain.isCorrect ?? "N"
                        //explain.
                    }
                    
                    if isCorrect == "Y" {
                        wrongIcon.image = UIImage(named: "correctIcon")
                        wrongCorrectView.backgroundColor = UIColor.green10
                        wrongCorrectLb.textColor = UIColor.Green
                        wrongCorrectLb.text = "잘 맞췄어요! 한번 더 복습해볼까요?"
                        
                        wrongCorrectDefaultWidth.constant = 0
                        wrongCorrectDefaultLb.text = ""
                        
                    }  else {
                        wrongCorrectLb.text = "앗, 틀렸네요. 다시 한 번 풀어볼까요?"
                        wrongCorrectDefaultWidth.constant = 0
                        wrongCorrectDefaultLb.text = ""
                    }
                    
                    self.answerLb.isHidden = true
                    
                } else {
                    
                    //questionScrollView.isHidden = true
                    questionContents.isHidden = true
                    answerContents.isHidden = false
                    btPencil.isHidden = true
                    
                    //let isCorrect = self.answer?.isCorrect ?? "N"
                    var isCorrect = "N"
                    
                    if let answer = self.preparationAnswer {
                        isCorrect = answer.isCorrect ?? "N"
                    } else if let explain = self.collectionExplain {
                        isCorrect = explain.isCorrect ?? "N"
                    }
                    
                    if isCorrect == "Y" {
                        wrongIcon.image = UIImage(named: "correctIcon")
                        wrongCorrectView.backgroundColor = UIColor.green10
                        wrongCorrectLb.textColor = UIColor.Green
                        wrongCorrectLb.text = "잘 맞췄어요! 한번 더 복습해볼까요?"
                        
                        wrongCorrectDefaultWidth.constant = 0
                        wrongCorrectDefaultLb.text = ""
                        
                    }  else {
                        if self.userAnswer == "⑥" {
                            wrongCorrectDefaultWidth.constant = 81
                            wrongCorrectDefaultLb.text = "내가 선택한 답"
                            wrongCorrectLb.font = UIFont.systemFont(ofSize: 14.0, weight: .regular)
                            wrongCorrectLb.text = "  잘 모르겠음."
                        } else {
                            adjustFont(a: self.userAnswer)
                        }
                    }
                    
                }
                
            } else { //복습
                wrongIcon.image = UIImage(named: "wrongIcon")
                wrongCorrectView.backgroundColor = UIColor.Red
                wrongCorrectDefaultLb.textColor = UIColor.red10
                wrongCorrectLb.textColor = UIColor.red10
                wrongCorrectLb.font = UIFont.systemFont(ofSize: 14.0, weight: .semibold)
                
                if seeQuestion {
                    //questionScrollView.isHidden = false
                    questionContents.isHidden = false
                    answerContents.isHidden = true
                    btPencil.isHidden = false
                    
                    let isCorrect = self.answerInfo?.isCorrect ?? "N"
                    if isCorrect == "Y" {
                        wrongIcon.image = UIImage(named: "correctIcon")
                        wrongCorrectView.backgroundColor = UIColor.green10
                        wrongCorrectLb.textColor = UIColor.Green
                        wrongCorrectLb.text = "잘 맞췄어요! 한번 더 복습해볼까요?"
                        
                        wrongCorrectDefaultWidth.constant = 0
                        wrongCorrectDefaultLb.text = ""
                    } else {
                        wrongCorrectLb.text = "앗, 틀렸네요. 다시 한 번 풀어볼까요?"
                        wrongCorrectDefaultWidth.constant = 0
                        wrongCorrectDefaultLb.text = ""
                        
                        self.answerLb.isHidden = true
                    }
                    
                } else {
                    //questionScrollView.isHidden = true
                    questionContents.isHidden = true
                    answerContents.isHidden = false
                    btPencil.isHidden = true
                    
                    //정/오에 따른 텍스트 분기
                    let isCorrect = self.answerInfo?.isCorrect ?? "N"
                    if isCorrect == "Y" {
                        wrongIcon.image = UIImage(named: "correctIcon")
                        wrongCorrectView.backgroundColor = UIColor.green10
                        wrongCorrectLb.textColor = UIColor.Green
                        wrongCorrectLb.text = "잘 맞췄어요! 한번 더 복습해볼까요?"
                        
                        wrongCorrectDefaultWidth.constant = 0
                        wrongCorrectDefaultLb.text = ""
                    } else {
                        if self.answerInfo?.userAnswer == "⑥" {
                            wrongCorrectDefaultWidth.constant = 81
                            wrongCorrectDefaultLb.text = "내가 선택한 답"
                            wrongCorrectLb.font = UIFont.systemFont(ofSize: 14.0, weight: .regular)
                            wrongCorrectLb.text = "  잘 모르겠음."
                        } else {
                            adjustFont(a: self.answerInfo?.userAnswer ?? "")
                        }
                    }
                }
            }
        }
    }
    var updateLayout: (() -> Void)?
    
    // MARK: - Preparation Answer
    var preparationAnswer : AnswerVideo?
    var questionId = -1
    var userAnswer = ""
    var realAnswer = ""
    var movieUrl = ""
    var movieCode = ""
    var answerOrignHeight: CGFloat = 0
    var isBookMarkChange = false
    {
        didSet {
            if isBookMarkChange {
                
            } else {
                
            }
        }
    }
    
    var delegate : PreparationMultipleChoiceViewController?
    var blockUrl: String? = ""
    var bookMarkSeq : Int?
    var choiceCount: Int? = 0
    
    func adjustFont(a: String) {
        //print("!!! \(a)")
        wrongCorrectDefaultWidth.constant = 81
        wrongCorrectDefaultLb.text = "내가 선택한 답"
        wrongCorrectLb.font = UIFont.systemFont(ofSize: 14.0, weight: .regular)
        
        let ref = ["①","②","③","④","⑤"]
        var check = false
        for r in ref {
            if (a.contains(r)) {
                // O/X 문제 답 표시.
                var answer = a
                if self.choiceCount == 2 {
                    if a == "①" {
                        answer = "ⓞ"
                    } else {
                        answer = "ⓧ"
                    }
                }
                wrongCorrectLb.font = UIFont.systemFont(ofSize: 18.0, weight: .regular)
                wrongCorrectLb.text = "  \(answer)"
                check = true
                break
            }
        }
        
        if (!check) {
            wrongCorrectLb.text = "  \(a)"
        }
    }
    
    func adjustFontReal(a: String) {
        realAnswerLb.font = UIFont.systemFont(ofSize: 14.0, weight: .regular)
        let ref = ["①","②","③","④","⑤"]
        var check = false
        for r in ref {
            if (a.contains(r)) {
                realAnswerLb.font = UIFont.systemFont(ofSize: 18.0, weight: .regular)
                // O/X 문제 답 표시.
                if self.choiceCount == 2 {
                    var ox = "ⓧ"
                    if a == "①" {
                        ox = "ⓞ"
                    }
                    realAnswerLb.text = ox
                } else {
                    realAnswerLb.text = a
                }
                check = true
                break
            }
        }
        
        if (!check) {
            realAnswerLb.text = a
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        questionScrollView.minimumZoomScale = 1.0
        questionScrollView.maximumZoomScale = 3.0
        questionScrollView.delegate = self
        
        explainScrollView.minimumZoomScale = 1.0
        explainScrollView.maximumZoomScale = 3.0
        explainScrollView.delegate = self
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func sendABFinishEvent(){
        //ab180 이벤트 보내기
        if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId {
            if Const.isAirBridge {
                CommonUtil.shared.ABEvent(
                    category: "pulja2.0_study_finish",
                    customs: ["userseq": userSeq, "user_id": userId, "number" : self.number, "keyword" : self.keywordName, "unit2List": self.unit2Seqs, "problemSeq": self.problemSeq, "cameFrom": self.cameFrom]
                )
            } else {
                print("[ABLog]", "-----------------------------------------", separator: " ")
                print("[ABLog]", "pulja2.0_study_finish", separator: " ")
                print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                print("[ABLog]", "user_id: \(userId)", separator: " ")
                print("[ABLog]", "number: \(self.number)", separator: " ")
                print("[ABLog]", "keyword: \(self.keywordName)", separator: " ")
                print("[ABLog]", "unit2List: \(self.unit2Seqs)", separator: " ")
                print("[ABLog]", "problemSeq: \(self.problemSeq)", separator: " ")
                print("[ABLog]", "cameFrom: \(self.cameFrom)", separator: " ")
                print("[ABLog]", "-----------------------------------------", separator: " ")
            }
        }
    }
    
    func processCat4(weakCat4 : String) -> String {
        if let i = weakCat4.firstIndex(of: "]") {
            let index: Int = weakCat4.distance(from: weakCat4.startIndex, to: i)
            let processedCat4 = weakCat4.substring(from: index+1, to: weakCat4.count-1)
            return processedCat4
        }
        return ""
    }
    
    func initData() {
        if let reviewQuestion = reviewQuestion {
            reqParam = AnswerInfoReq(problemType: reviewQuestion.problemType, questionId: reviewQuestion.questionId, solveSeq: reviewQuestion.solveSeq, userSeq: "")
            
            CommonAPI.shared.answerInfo(answerInfoReq: reqParam!).done { res in
                guard let data = res.data  else {
                    self.dismiss(animated: true)
                    return
                }
                
                self.answerInfo = data
                self.choiceCount = self.answerInfo?.choiceCount
                if self.currentTab == 0 {
                    self.seeQuestion = true
                } else {
                    self.seeQuestion = false
                }
                
                //문제 세팅
                self.questionId = (self.answerInfo?.questionId)!
                self.movieUrl = self.answerInfo?.movieURL ?? ""
                self.movieCode = self.answerInfo?.movieCode ?? ""
                
                let problemUrl = URL(string: "\(Const.PROBLEM_IMAGE_URL)\(self.questionId).jpg")
                self.viewProblemImg(isSkeleton: true, problemUrl: problemUrl)
                
                let explainUrl = URL(string:"\(Const.EXPLAIN_IMAGE_URL)\(self.questionId).jpg")
                self.showAnswerVideo(explainUrl: explainUrl)
            }
        }
    }
    
    func viewInit() {
        if (reviewQuestion?.bookMarkSeq) != nil {
            self.isBookMarkChange = true
            self.btBookMark.setImage(UIImage(named: "yellowstar"), for: .normal)
        } else {
            self.isBookMarkChange = false
            self.btBookMark.setImage(UIImage(named: "star"), for: .normal)
        }
        
        bookMarkSeq = reviewQuestion?.bookMarkSeq
        questionContents.isHidden = false
        answerContents.isHidden = true
        self.categoryLb.text = processCat4(weakCat4: self.reviewQuestion?.categoryName ?? "")
        
        btPencil.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        initPencil()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        //테블릿인지 아닌지 확인
        switch UIDevice.current.userInterfaceIdiom {
            case .pad:
            self.ipadAnswerView(size : size)
            default:
                break
        }

        if UIDevice.current.orientation.isLandscape {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                self.questionScrollView.zoomScale = self.problemImageScale
                self.questionScrollView.setContentOffset(CGPoint(x: 0 ,y: 0), animated: false)
                
                self.explainScrollView.zoomScale = self.solutionImageScale
                self.explainScrollView.setContentOffset(CGPoint(x: 0 ,y: 0), animated: false)
            }
        } else {
            self.questionScrollView.zoomScale = 1.0
            self.questionScrollView.setContentOffset(CGPoint(x: 0 ,y: 0), animated: false)
            
            self.explainScrollView.zoomScale = 1.0
            self.explainScrollView.setContentOffset(CGPoint(x: 0 ,y: 0), animated: false)
        }
    }
    
    var problemImageScale = 0.0
    var solutionImageScale = 0.0
    var isLandscape: Bool {
        if #available(iOS 13.0, *) {
            return UIApplication.shared.windows
                .first?
                .windowScene?
                .interfaceOrientation
                .isLandscape ?? false
        } else {
            return UIApplication.shared.statusBarOrientation.isLandscape
        }
    }
    
    
    func setIvExplain() {
       
    }
    
    func ipadAnswerView(size : CGSize) {
        questionScrollView.zoomScale = 1.0
        explainScrollView.zoomScale = 1.0
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            
            //사진 조절
            guard let pImg = self.problemImg else { return }
            self.ivProblem.image = pImg.aspectFitImage(inRect: self.ivProblem.frame)
            self.ivProblem.contentMode = .topLeft
            
            guard let aImg = self.answerImg else { return }
            self.ivExplain.image = aImg.aspectFitImage(inRect: self.ivExplain.frame)
            self.ivExplain.contentMode = .topLeft
        }
    }
    
    func viewProblemImg(isSkeleton: Bool, problemUrl: URL?) {
        //print("현재 문제 아이디: \(tempQ)")
        if isSkeleton {
            LoadingView.show()
        }
        
        self.questionScrollView.zoomScale = 1.0 // 확대 축소 초기화.
        self.ivProblem.isHidden = true
        //문제 load
        self.ivProblem.kf.indicatorType = .activity
        self.ivProblem.kf.setImage(with: problemUrl, options: [.transition(.fade(0.2))],
                                   completionHandler: {
            result in
            switch result{
            case .success(let value):
                
                let image = value.image
                self.problemImg = image
                
                self.ivProblem.image = image.aspectFitImage(inRect: self.ivProblem.frame)
                self.ivProblem.contentMode = .topLeft
                
                let height = self.ivProblem.intrinsicContentSize.height
                let width = self.ivProblem.intrinsicContentSize.width
                
                // 문제 scale 조정
                if (width * 2 < height) {
                    if (self.isLandscape) {
                        self.problemImageScale = 3.0
                    } else {
                        self.problemImageScale = 1.75
                    }
                }
                else if (width * 1.5 < height) {
                    if (self.isLandscape) {
                        self.problemImageScale = 2.0
                    } else {
                        self.problemImageScale = 1.25
                    }
                }
                else {
                    self.problemImageScale = 1.0
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                    self.questionScrollView.zoomScale = self.problemImageScale
                    self.questionScrollView.setContentOffset(CGPoint(x: 0 ,y: 0), animated: true)
                    self.questionScrollView.isHidden = false
                    self.questionScrollView.flashScrollIndicators()
                    self.ivProblem.isHidden = false
                    
                    if isSkeleton {
                        LoadingView.hide()
                    }
                    print("문제 사진 불러오는데 성공함 height:\(self.ivProblem.frame.height)")
                }
            default:
                if isSkeleton {
                    LoadingView.hide()
                }
                print("문제 사진 불러오는데 실패함")
                return
            }
        })
    }
    
    func showAnswerVideo(explainUrl: URL?) {
        //해설 영상
        self.initExplainWebview(movieUrl: self.movieUrl, movieCode: self.movieCode)
        
        //정답 풀이
        //let explainUrl = URL(string:"\(Const.EXPLAIN_IMAGE_URL)\(self.questionId).jpg")
        
        LoadingView.show()
        self.explainScrollView.zoomScale = 1.0 // 확대 축소 초기화.
        self.solutionImageScale = 1.0
        self.ivExplain.isHidden = true
        self.explainEmptyView.isHidden = true
        
        self.ivExplain.kf.indicatorType = .activity
        self.ivExplain.kf.setImage(with: explainUrl, options: [.transition(.fade(0.2))],
                                   completionHandler: {
            result in
            switch result{
            case .success(let value):

                let image = value.image
                self.answerImg = image

                self.ivExplain.image = image.aspectFitImage(inRect: self.ivExplain.frame)
                self.ivExplain.contentMode = .topLeft
                
                let height = self.ivExplain.intrinsicContentSize.height
                let width = self.ivExplain.intrinsicContentSize.width
                
                // 문제 scale 조정
                if (width * 2 < height) {
                    if (self.isLandscape) {
                        self.solutionImageScale = 3.0
                    } else {
                        self.solutionImageScale = 1.75
                    }
                }
                else if (width * 1.5 < height) {
                    if (self.isLandscape) {
                        self.solutionImageScale = 2.0
                    } else {
                        self.solutionImageScale = 1.25
                    }
                }
                else {
                    self.solutionImageScale = 1.0
                }
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                    self.explainScrollView.zoomScale = self.solutionImageScale
                    self.explainScrollView.setContentOffset(CGPoint(x: 0 ,y: 0), animated: false)
                    self.explainScrollView.isHidden = false
                    self.explainScrollView.flashScrollIndicators()
                    self.ivExplain.isHidden = false
                }
                
                LoadingView.hide()
                print("해설 사진 불러오는데 성공함 height:\(height)")
                
            default:
                LoadingView.hide()
                print("해설 사진 불러오는데 실패함")
                return
            }
        })
        
        if self.cameFromAISolve {
            adjustFontReal(a: self.realAnswer)
            
            wrongIcon.image = UIImage(named: "wrongIcon")
            wrongCorrectView.backgroundColor = UIColor.Red
            wrongCorrectDefaultLb.textColor = UIColor.red10

            wrongCorrectLb.textColor = UIColor.red10
            wrongCorrectLb.font = UIFont.systemFont(ofSize: 14.0, weight: .semibold)
            
            var isCorrect = "N"
            
            if let answer = self.preparationAnswer {
                isCorrect = answer.isCorrect ?? "N"
            } else if let explain = self.collectionExplain {
                isCorrect = explain.isCorrect ?? "N"
            }
            
            if isCorrect == "Y" {
                wrongIcon.image = UIImage(named: "correctIcon")
                wrongCorrectView.backgroundColor = UIColor.green10
                wrongCorrectLb.textColor = UIColor.Green
                wrongCorrectLb.text = "잘 맞췄어요! 한번 더 복습해볼까요?"
                
                wrongCorrectDefaultWidth.constant = 0
                wrongCorrectDefaultLb.text = ""
            }  else {
                wrongIcon.image = UIImage(named: "wrongIcon")
                if self.userAnswer == "⑥" {
                    wrongCorrectDefaultWidth.constant = 81
                    wrongCorrectDefaultLb.text = "내가 선택한 답"
                    wrongCorrectLb.font = UIFont.systemFont(ofSize: 14.0, weight: .regular)
                    wrongCorrectLb.text = "  잘 모르겠음."
                } else {
                    adjustFont(a: self.userAnswer)
                }
            }
        } else {
            adjustFontReal(a: self.answerInfo?.answerTxt ?? "")
        }
    }
    
    func showData() {
        if let answer = self.preparationAnswer {
            //유형 이름 뿌려주기
            let cLb = answer.categoryName ?? ""
            if let firstIdx = cLb.index(of: "]") {
                let i = firstIdx.encodedOffset
                self.categoryLb.text = cLb.substring(from: i+1, to: cLb.count - 1)
            }
            
            //문제 보기 정보 뿌려주기
            self.questionId = answer.questionId ?? 0
            self.userAnswer = answer.userAnswer ?? ""
            self.realAnswer = answer.answer ?? ""
            self.choiceCount = answer.choiceCnt ?? 0
            
            //정답 및 해설 보기 정보 뿌려주기
            self.movieUrl = answer.movieUrl ?? ""
            self.movieCode = answer.movieCode ?? ""
            let explainUrl = URL(string:"\(Const.EXPLAIN_IMAGE_URL)\(self.questionId).jpg")
            showAnswerVideo(explainUrl: explainUrl)
            
            //북마크 정보 처리 (학습 -> nil, 복습 -> 저장된 정보 사용)
            bookMarkSeq = (cameFromAISolve == true) ? nil : reviewQuestion?.bookMarkSeq
            
            // 문제
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                let problemUrl = URL(string: "\(Const.PROBLEM_IMAGE_URL)\(self.questionId).jpg")
                self.viewProblemImg(isSkeleton: false, problemUrl: problemUrl)
            }
        } else if let explain = self.collectionExplain {
            // MARK: - Collection 해설
            
            if isLastCollection {
                nextBtn.setTitle("풀이 결과 보러가기", for: .normal)
            }
            
            //유형 이름 뿌려주기
            let cLb = explain.categoryName ?? ""
            if let firstIdx = cLb.index(of: "]") {
                let i = firstIdx.encodedOffset
                self.categoryLb.text = cLb.substring(from: i+1, to: cLb.count - 1)
            }

            //문제 보기 정보 뿌려주기
            //self.questionId = explain.questionId ?? 0
            self.userAnswer = explain.userAnswer ?? ""
            self.realAnswer = explain.answer ?? ""
            self.choiceCount = explain.choiceCnt ?? 0
            
            //정답 및 해설 보기 정보 뿌려주기
            //self.movieUrl = explain.movieUrl ?? ""
            //self.movieCode = explain.movieCode ?? ""
            let explainUrl = URL(string: explain.explainUrl ?? "")
            showAnswerVideo(explainUrl: explainUrl)
            
            bookMarkSeq = nil
            
            // 문제
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                let problemUrl = URL(string: explain.problemUrl ?? "")
                self.viewProblemImg(isSkeleton: false, problemUrl: problemUrl)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CommonUtil.tabbarController?.bottomNavBar.isHidden = true
        CommonUtil.tabbarController?.tabBar.isHidden = true
        
        if self.cameFromAISolve {
            seeQuestion = false
            
            questionText.setTitleColor(UIColor.Foggy, for: .normal)
            questionProgressView.backgroundColor = UIColor.white
            
            answerText.setTitleColor(UIColor.OffPurple, for: .normal)
            answerProgressView.backgroundColor = UIColor.OffPurple
            
            //백버튼 숨기기, 바텀버튼 표시하기
            self.backBtn.isHidden = true
            answerContents.isHidden = false
            questionContents.isHidden = true
            bottomButtonHidden(isHidden: false)
            
            self.categoryLb.translatesAutoresizingMaskIntoConstraints = false
            self.categoryLb.leadingAnchor.constraint(equalTo: self.topview.leadingAnchor, constant: 12.0).isActive = true
            
            showData()
            
            
        } else {
            //ab180 이벤트 보내기
            if let userSeq = self.myInfo?.userSeq, let userId = self.myInfo?.userId, let c4name = self.reviewQuestion?.categoryName {
                if Const.isAirBridge {
                    CommonUtil.shared.ABEvent(
                        category: "pulja2.0_review_detail_enter",
                        customs: ["userseq": userSeq, "user_id": userId, "categoryName": c4name]
                    )
                } else {
                    print("[ABLog]", "-----------------------------------------", separator: " ")
                    print("[ABLog]", "category: pulja2.0_review_detail_enter", separator: " ")
                    print("[ABLog]", "userseq: \(userSeq)", separator: " ")
                    print("[ABLog]", "user_id: \(userId)", separator: " ")
                    print("[ABLog]", "categoryName: \(c4name)", separator: " ")
                    print("[ABLog]", "-----------------------------------------", separator: " ")
                }
            }
            
            //백버튼 보여주기, 바텀버튼 가리기
            self.backBtn.isHidden = false
            bottomButtonHidden(isHidden: true)
            
            viewInit()
            initData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !self.cameFromAISolve {
            CommonUtil.tabbarController?.bottomNavBar.isHidden = false
            CommonUtil.tabbarController?.tabBar.isHidden = false
        }
        
    }
    
    @IBAction func pencilClear(_ sender: Any) {
        pkCanvasView.drawing = PKDrawing()
    }
    
    @IBAction func goBack(_ sender: Any) {
        detailBookMarkCallback (self.bookMarkSeq)
        dismiss(animated: true)
    }
    
    
    
    @IBAction func popupPencilView(_ sender: Any) {
        pkCanvasView.isHidden = false
        pencilToolView.isHidden = false
        btPencil.isHidden = true
        
        if #available(iOS 14.0, *) {
            toolPicker.setVisible(true, forFirstResponder: pkCanvasView)
        } else {
            guard let _ = view.window else { return }
            toolPicker.setVisible(true, forFirstResponder: pkCanvasView)
        }
    }
    
    @IBAction func setBookMark(_ sender: Any) {
        //북마크 안 눌러 있는 경우
        if !isBookMarkChange {
            self.btBookMark.setImage(UIImage(named: "yellowstar"), for: .normal)

        } else {
            self.btBookMark.setImage(UIImage(named:"star"), for: .normal)

        }

        //북마크 api 호출
        var solveseq: Int?
        var ptype: String?
        if let ce = collectionExplain {
            solveseq = ce.solveSeq
            ptype = "d"
        } else {
            solveseq = (cameFromAISolve == true) ? self.preparationAnswer?.solveSeq : self.reviewQuestion?.solveSeq
            ptype = (cameFromAISolve == true) ? "d" : self.reviewQuestion?.problemType
        }
        let req : BookMarkReq = BookMarkReq(bookMarkSeq: self.bookMarkSeq
                                            , solveSeq: solveseq
                                            , userSeq: nil
                                            , problemType: ptype)
        ReviewAPI.shared.bookMark(bookMarkReq: req).done {
            res in
            
            //북마크 등록하는 경우, 북마크 bookMarkSeq 저장함.
            if !self.isBookMarkChange {
                self.bookMarkSeq = res.data?.bookMarkSeq
                self.showToast(message: "보관함에 저장되었어요.")
            } else { //북마크 해제하는 경우
                self.bookMarkSeq = nil
                self.showToast(message: "보관함에서 삭제되었어요.")
            }
            
        }.catch { err in

        }.finally {
            self.isBookMarkChange.toggle()
        }
    }
    
    
    @IBAction func goBugReport(_ sender: Any) {
        let vc = R.storyboard.preparation.bugReportViewController()!
        vc.questionId = self.questionId
        vc.reportType = "p"  // 문제 관련 신고.
        vc.callback = { b in
            if b {
                self.showToast(message: "제보가 접수되었어요. 빠르게 검토 후 수정할게요.")
            }
        }

        let heroid = "report_\(self.questionId)"
        self.present(vc, animated: true, completion: nil)
        //print("!!!->\(self.questionId)")
    }
    
    @IBAction func pencilClose(_ sender: Any) {
        pkCanvasView.isHidden = true
        pencilToolView.isHidden = true
        btPencil.isHidden = false
        
        pkCanvasView.becomeFirstResponder()
        
        if #available(iOS 14.0, *) {
          toolPicker.setVisible(false, forFirstResponder: pkCanvasView)
        } else {
          guard let _ = view.window else { return }
          toolPicker.setVisible(false, forFirstResponder: pkCanvasView)
        }
    }
    
    @IBAction func nextProbBtn(_ sender: Any) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.popViewController(animated: false)
    }
    
    
    @IBAction func seeQuestion(_ sender: Any) {
        currentTab = 0
        seeQuestion = true
        
        questionText.setTitleColor(UIColor.OffPurple, for: .normal)
        questionProgressView.backgroundColor = UIColor.OffPurple
        
        answerText.setTitleColor(UIColor.Foggy, for: .normal)
        answerProgressView.backgroundColor = UIColor.white
        
    }
    
    
    @IBAction func seeAnswer(_ sender: Any) {
        currentTab = 1
        seeQuestion = false
        
        questionText.setTitleColor(UIColor.Foggy, for: .normal)
        questionProgressView.backgroundColor = UIColor.white
        
        answerText.setTitleColor(UIColor.OffPurple, for: .normal)
        answerProgressView.backgroundColor = UIColor.OffPurple
    }
    
    
    func initPencil() {
    // Set up the canvas view with the first drawing from the data model.
        pkCanvasView.delegate = self
        pkCanvasView.alwaysBounceVertical = false
        pkCanvasView.isOpaque = false
        // Set up the tool picker
        if #available(iOS 14.0, *) {
          toolPicker = PKToolPicker()
        } else {
          // Set up the tool picker, using the window of our parent because our view has not
          // been added to a window yet.
          guard let window = view.window else { return }
          toolPicker = PKToolPicker.shared(for:  window)
        }
        toolPicker.setVisible(false, forFirstResponder: pkCanvasView)
        toolPicker.addObserver(pkCanvasView)
        toolPicker.addObserver(self)
    //    updateLayout(for: toolPicker)
        pkCanvasView.becomeFirstResponder()
        if #available(iOS 14.0, *) {
          pkCanvasView.allowsFingerDrawing = true
        } else {
          pkCanvasView.allowsFingerDrawing = true
        }
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
    
    private var authCookie: HTTPCookie? {
             let cookie = HTTPCookie(properties: [
              .domain: Const.COOKIE_DOMAIN,
                 .path: "/",
                 .name: "JWT-TOKEN",
                 .value: CommonUtil.shared.getAccessToken(),
             ])
             return cookie
         }
             
     private var refreshCookie: HTTPCookie? {
         let cookie = HTTPCookie(properties: [
             .domain: Const.COOKIE_DOMAIN,
             .path: "/",
             .name: "REFRESH-TOKEN",
             .value: CommonUtil.shared.getRefreshToken(),
         ])
         return cookie
     }
    
    func initExplainWebview(movieUrl:String?, movieCode:String?) {
        //실제 배포때 쓸 코드 (영상 보이면 안됨)
        guard let bUrl = URL(string: self.blockUrl!) else {
            explainVideoHeight.constant = 0
            lineView.isHidden = true
            return

        }
        
        let contentController = WKUserContentController()
        AirBridge.webInterface()?.inject(to: contentController, withWebToken: Const.AB_WEB_TOKEN)
        
        let script =
            "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport');" +
                "meta.setAttribute('content', 'width=device-width, initial-scale = 1.0, maximum-scale=5.0, user-scalable = yes');" +
        "document.getElementsByTagName('head')[0].appendChild(meta);"
        let userScript = WKUserScript(source: script,
                                      injectionTime: WKUserScriptInjectionTime.atDocumentEnd,
                                      forMainFrameOnly: true)
        contentController.addUserScript(userScript)
        
        // Bridge 등록
        contentController.add(self, name: "currentDuration")
        contentController.add(self, name: "pause")
       
        
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        
        let config = WKWebViewConfiguration()
        config.preferences = preferences
        config.userContentController = contentController
        
        self.wvExplain = WKWebView(frame: .zero, configuration: config)
        self.wvExplain?.frame = self.explainView.bounds
        self.wvExplain?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.explainView.addSubview(self.wvExplain!)

        
        let urlRequest = URLRequest(url: bUrl)
        
        
        if let authCookie = authCookie, let refreshCookie = refreshCookie {
            self.wvExplain?.configuration.websiteDataStore.httpCookieStore.setCookie(authCookie, completionHandler: {
                self.wvExplain?.configuration.websiteDataStore.httpCookieStore.setCookie(refreshCookie, completionHandler: {
                    self.wvExplain?.load(urlRequest)
                })
            })
        }
        
        self.wvExplain.uiDelegate = self
        self.wvExplain?.navigationDelegate = self
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        switch message.name {
            case "pause":
                print("pause")
            case "currentDuration":
                
                guard let currentDuration = message.body as? Int else {
                    return
                }
                print("currentDuration : \(message.body)")
                
            default:
                break
            }
    }

    
    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        if scrollView == self.questionScrollView
        {
            return self.ivProblem
        }
        else
        {
            return self.ivExplain
        }
    }
    
    func showToast(message: String) {
        
        var heMinus : CGFloat = 120.0
        
        if self.cameFromAISolve {
            heMinus = 120.0
        }else {
            heMinus = 60.0
        }
        
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.first
            let top = window?.safeAreaInsets.top
            let bottom = window?.safeAreaInsets.bottom
            heMinus += bottom!
            
        } else if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let top = window?.safeAreaInsets.top
            let bottom = window?.safeAreaInsets.bottom
            heMinus += bottom!
        }
    
        let width = self.view.frame.size.width
        let toastLabel = PaddingLabel(frame: CGRect(x: 16, y: self.view.frame.size.height - heMinus, width: width - 32, height: 44))
        toastLabel.paddingLeft = 16
        toastLabel.paddingRight = 16
        
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        toastLabel.textColor = UIColor.white
        toastLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        toastLabel.textAlignment = .left
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 8
        toastLabel.clipsToBounds = true
    
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 2.0, delay: 1.5, options: .curveEaseOut, animations: { toastLabel.alpha = 0.0 }, completion: {(isCompleted) in toastLabel.removeFromSuperview() })
        
    }
    

}

extension PreparationAnswerViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if isLandscape {
            if questionScrollView.contentOffset.x != 0 {
                questionScrollView.contentOffset.x = 0
            }
            
            let problemWidth = ivProblem.intrinsicContentSize.width * questionScrollView.zoomScale
            let problemScrollViewWith = questionScrollView.frame.width
            
            if problemWidth > problemScrollViewWith {
                questionScrollView.setZoomScale(questionScrollView.zoomScale - 0.01, animated: false)
            }
            
            
            if explainScrollView.contentOffset.x != 0 {
                explainScrollView.contentOffset.x = 0
            }
            
            let explainWidth = ivExplain.intrinsicContentSize.width * explainScrollView.zoomScale
            let explainScrollViewWith = explainScrollView.frame.width
            
            if explainWidth > explainScrollViewWith {
                explainScrollView.setZoomScale(explainScrollView.zoomScale - 0.01, animated: false)
            }
        }
    }
}
