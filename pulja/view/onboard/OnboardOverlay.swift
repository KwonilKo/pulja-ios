//
//  OnboardOverlay.swift
//  pulja
//
//  Created by 김병헌 on 2022/02/17.
//

import UIKit
import SwiftyOnboard


class OnboardOverlay: SwiftyOnboardOverlay {
    
    
    @IBOutlet weak var pcDot: UIPageControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "OnboardOverlay", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
}
