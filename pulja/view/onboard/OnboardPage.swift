//
//  OnboardPage.swift
//  pulja
//
//  Created by 김병헌 on 2022/02/17.
//

import UIKit
import SwiftyOnboard

class OnboardPage: SwiftyOnboardPage {
    
    
    @IBOutlet weak var lbTitle: UILabel!
    
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    class func instanceFromNib() -> UIView {
       return UINib(nibName: "OnboardPage", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
   }

    
}
