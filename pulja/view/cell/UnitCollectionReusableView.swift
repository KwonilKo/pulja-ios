//
//  UnitCollectionReusableView.swift
//  pulja
//
//  Created by 김병헌 on 2022/01/14.
//

import UIKit

class UnitCollectionReusableView: UICollectionReusableView {
    
    
    @IBOutlet weak var lbUnitTitle: UILabel!
    
    
    @IBOutlet weak var btExpand: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    static func nib() -> UINib {
          return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
      }
}

