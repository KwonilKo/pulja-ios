//
//  RecommendEmptyViewCell.swift
//  pulja
//
//  Created by HUN on 2022/12/15.
//

import UIKit
import Kingfisher

class RecommendEmptyViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lbKeyword: UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    static func nib() -> UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    var keyword: String! {
        didSet {
            
            let style = NSMutableParagraphStyle()
            style.alignment = NSTextAlignment.center
            style.lineHeightMultiple = 1.13
            let richText = NSMutableAttributedString(string: keyword, attributes: [ NSAttributedString.Key.paragraphStyle: style ])
            self.lbKeyword.attributedText = richText
        }
    }
}
