//
//  ProblemTableViewCell.swift
//  pulja
//
//  Created by 김병헌 on 2021/12/30.
//

import UIKit
import Kingfisher

class ProblemTableViewCell: UITableViewCell {

    //@IBOutlet weak var gradientViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var lbTitle: UILabel!
       
    @IBOutlet weak var btFavorite: UIButton!
   
    @IBOutlet weak var lbDate: UILabel!
   
    @IBOutlet weak var lbCorrect: UILabel!
    
    @IBOutlet weak var ivProblem: UIImageView!
    
    @IBOutlet weak var gradientView: UIView!
    
    @IBOutlet weak var btDetail: UIButton!
    
    @IBOutlet weak var btDetail2: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        btProblem.layer.addBorder([.top], color: UIColor.lightBlueGrey, width: 0.3)
//        btExplain.layer.addBorder([.top, .left], color: UIColor.lightBlueGrey, width: 0.3)
//        let startColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
//        let endColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.88)

//        //테블릿인지 아닌지 확인
//        switch UIDevice.current.userInterfaceIdiom {
//            case .pad:
//            let height = self.frame.size.height
//            self.gradientViewTopConstraint.constant = self.frame.size.height - 107.0
//            default:
//                break
//        }

//        self.gradientView.applyGradient(isVertical: true, colorArray: [startColor, endColor])
    }
    
    static func nib() -> UINib {
            return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
        }
    
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func processCat4(weakCat4 : String) -> String {
        if let i = weakCat4.firstIndex(of: "]") {
            let index: Int = weakCat4.distance(from: weakCat4.startIndex, to: i)
            let processedCat4 = weakCat4.substring(from: index+1, to: weakCat4.count-1)
            return processedCat4
        }
        return ""
    }
    
    var question : ReviewQuestion! {
        didSet {
            lbTitle.text = processCat4(weakCat4: question.categoryName ?? "")
            if question.bookMarkSeq == nil {
                btFavorite.setImage(R.image.iconOutlineStar(), for: .normal)
            } else {
                btFavorite.setImage(R.image.iconSolidStar(), for: .normal)
            }
            if let date = question.regdate?.toFormatString( .dotdate) {
                lbDate.text = date
            }

            
            if question.isCorrect == "Y" {
                lbCorrect.text = "O"
                lbCorrect.textColor = R.color.puljaBlue()
            } else {
                lbCorrect.text = "X"
                lbCorrect.textColor = R.color.reddishOrange()
            }
            

            let url = URL(string:"\(Const.PROBLEM_IMAGE_URL)\(question.questionId ?? 0).jpg")
            ivProblem.kf.indicatorType = .activity
            
            // Cropping
//            print("!!!\(UIScreen.main.bounds.width)")
//            let width = UIScreen.main.bounds.width
//            let height = UIScreen.main.bounds.width * 0.6
            let processor = CroppingImageProcessor(size: CGSize(width: 700, height: 500), anchor: CGPoint(x: 0, y: 0))
            ivProblem.kf.setImage(with: url,  options: [.transition(.fade(0.2)), .processor(processor)])
            //ivProblem.contentMode = .left
//            ivProblem.clipsToBounds = true
//            ivProblem.image = ivProblem.image?.resizeTopAlignedToFill(containerSize: ivProblem.frame.size)
            
        }
    }
    

}
