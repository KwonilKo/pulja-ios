//
//  Unit2SelectHeaderView.swift
//  pulja
//
//  Created by 김병헌 on 2022/10/26.
//

import UIKit
import EasyTipView

class Unit2SelectHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var btMiddle: UIButton!
    
    
    @IBOutlet weak var btHigh: UIButton!
    
    
    @IBOutlet weak var btSubject: UIButton!
    
    
    @IBOutlet weak var btReset: UIButton!
    
    var selectMHIndex = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        showCloseToolTip()
    }
    
    func showCloseToolTip(){
        var preferences = EasyTipView.Preferences()
        preferences.drawing.font = UIFont(name: "AppleSDGothicNeo-Regular", size: 12)!
        preferences.drawing.foregroundColor = UIColor.white
        preferences.drawing.backgroundColor = UIColor.black
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.bottom
        preferences.positioning.contentInsets = UIEdgeInsets(top: 6.0, left: 8.0, bottom: 6.0, right: 8.0)
      
        preferences.animating.dismissTransform = CGAffineTransform(translationX: 0, y: -15)
        preferences.animating.showInitialTransform = CGAffineTransform(translationX: 0, y: -15)
        preferences.animating.showInitialAlpha = 0
        preferences.animating.showDuration = 1
        preferences.animating.dismissDuration = 1

        let easyTipView = EasyTipView(text: "원하는 과목을 선택할 수 있어요", preferences: preferences)

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            easyTipView.show(forView: self.btSubject, withinSuperview: self.contentView)
        }
      
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            easyTipView.dismiss()
        }
    }
    static func nib() -> UINib {
       return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
   }
    

}
