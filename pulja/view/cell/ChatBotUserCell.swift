//
//  ChatBotUserCell.swift
//  pulja
//
//  Created by samdols on 2022/02/02.
//

import UIKit

class ChatBotUserCell: UITableViewCell {

    static let cellHeight : CGFloat = 88
    
    @IBOutlet weak var lbTime: UILabel!
    @IBOutlet weak var lbMessage: UILabel!
    
    static func nib() -> UINib {
            return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
        }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
