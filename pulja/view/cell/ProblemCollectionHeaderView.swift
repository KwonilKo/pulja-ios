//
//  ProblemCollectionHeaderViewCell.swift
//  pulja
//
//  Created by HUN on 2023/02/02.
//

import UIKit

class ProblemCollectionHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var feedbackView: UIView!
    @IBOutlet weak var feedbackViewHeight: NSLayoutConstraint! // mobile: 194, pad: 217 -> 0
//    @IBOutlet weak var feedbackViewTop: NSLayoutConstraint! // mobile: 20, pad: 24 -> 0
//    @IBOutlet weak var feedbackViewLeading: NSLayoutConstraint! // mobile: 16, pad: 20
//    @IBOutlet weak var feedbackViewTrailing: NSLayoutConstraint! // mobile: 16, pad: 20
    @IBOutlet weak var feedbackLabel: UILabel! // mobile: 16, pad: 18
    @IBOutlet weak var feedbackButton: UIButton! // mobile: 14, pad: 16
    @IBOutlet weak var feedbackButtonHeight: NSLayoutConstraint! // mobile: 36, pad: 44
    @IBOutlet weak var feedbackImageHeight: NSLayoutConstraint! // mobile: 67, pad: 80
    @IBOutlet weak var feedbackImageWidth: NSLayoutConstraint! // mobile: 67, pad: 80
    @IBOutlet weak var feedbackImageTop: NSLayoutConstraint! // mobile: 20, pad: 24
    @IBOutlet weak var feedbackButtonTop: NSLayoutConstraint! // mobile: 14, pad: 18
    @IBOutlet weak var feedbackViewLabelTop: NSLayoutConstraint! // mobile: 12, pad: 16
 
    
    var clickCallback : (_ isSend : Bool) -> Void = { _ in}
    
    // MARK: - Feedback View
    @IBAction func clickFeedbackClose(_ sender: Any) {
        clickCallback(false)
    }
    
    @IBAction func clickSendButton(_ sender: Any) {
        clickCallback(true)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if (UIDevice.current.userInterfaceIdiom == .pad) {
            feedbackViewHeight.constant = 229
            feedbackLabel.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
            feedbackButton.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .bold)
            feedbackImageHeight.constant = 80
            feedbackImageWidth.constant = 80
            feedbackImageTop.constant = 24
            feedbackButtonTop.constant = 18
            feedbackViewLabelTop.constant = 16
            feedbackButtonHeight.constant = 44
        }
    }
    
    static func nib() -> UINib {
       return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
}
