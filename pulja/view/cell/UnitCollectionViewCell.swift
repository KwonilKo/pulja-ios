//
//  UnitCollectionViewCell.swift
//  pulja
//
//  Created by 김병헌 on 2022/01/04.
//

import UIKit
import M13Checkbox

class UnitCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var lbUnit2: UILabel!
    
    @IBOutlet weak var lbUnit1: UILabel!
    
    
    @IBOutlet weak var checkbox: M13Checkbox!
    
    
    var checkCallback : (_ isSend : Bool) -> Void = { _ in}
    
    var searchText:String? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        initCheckbox()
    }
    
    func initCheckbox(){
        
        checkbox?.stateChangeAnimation = .bounce(.fill)
        checkbox?.tintColor = UIColor.purpleishBlue
        checkbox?.secondaryTintColor = UIColor.lightBlueGrey
        checkbox?.checkmarkLineWidth = CGFloat(4)
        checkbox?.boxLineWidth = CGFloat(1)

    }
    
    
    static func nib() -> UINib {
       return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
   }
    
    
    
    @IBAction func checkboxValueChanged(_ sender: M13Checkbox) {
        switch sender.checkState {
        case .unchecked:
            self.checkCallback(false)
            break
        case .checked:
            self.checkCallback(true)
            break
        case .mixed:
            break
        }
    }
    
    
    var unitInfo : UnitInfo! {
        
        didSet {
//            if let sText = searchText {
//                lbUnit2.text = unitInfo.unit2SName
//
//                // NSMutableAttributedString Type으로 바꾼 text를 저장
//                let attributedStr = NSMutableAttributedString(string: lbUnit2.text!)
//
//
//                // text의 range 중에서 "Bonus"라는 글자는 UIColor를 blue로 변경
//                attributedStr.addAttribute(.foregroundColor, value: UIColor.blue, range: (lbUnit2.text! as NSString).range(of: sText))
//                attributedStr.addAttribute(.font, value: UIFont.systemFont(ofSize: 14.0, weight: .bold), range: (lbUnit2.text! as NSString).range(of: sText))
//                // 설정이 적용된 text를 label의 attributedText에 저장
//                lbUnit2.attributedText = attributedStr
//            } else {
//                lbUnit2.text = unitInfo.unit2SName
//            }
            lbUnit2.text = unitInfo.unit2SName
            lbUnit1.text = unitInfo.unit1SName
        }
    }
    

}
