//
//  TodayCoachJTACalendarViewCell.swift
//  pulja
//
//  Created by 고권일 on 2022/03/07.
//

import UIKit
import JTAppleCalendar

class TodayCoachJTACalendarViewCell: JTACDayCell {
    
    
    @IBOutlet var lbDate: UILabel!

    @IBOutlet var bgView: UIView!
    
    @IBOutlet var time1: UILabel!
    
    @IBOutlet var time2: UILabel!
    
    @IBOutlet var time3: UILabel!
    
}
