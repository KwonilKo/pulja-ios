//
//  ProblemCollectionTabViewCell.swift
//  pulja
//
//  Created by HUN on 2023/01/10.
//

import UIKit
import Kingfisher

class ProblemSubjectTabViewCell: UICollectionViewCell {
    
    @IBOutlet weak var tabLabel: UILabel!
    @IBOutlet weak var tabBottom: UIView!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    static func nib() -> UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    var collectionTab: SubjectTab! {
        didSet {
            tabLabel.text = collectionTab.name
            
            if collectionTab.isSelect {
                self.tabBottom.isHidden = false
                self.tabLabel.textColor = UIColor.Purple
            } else {
                self.tabBottom.isHidden = true
                self.tabLabel.textColor = UIColor.OffBlack
            }
        }
    }

}

struct SubjectTab {
    let name : String
    var isSelect : Bool
    
    init(
        name: String = "",
        isSelect: Bool = false
    ) {
        self.name = name
        self.isSelect = isSelect
    }
}
