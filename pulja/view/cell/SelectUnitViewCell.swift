//
//  SelectUnitViewCell.swift
//  pulja
//
//  Created by HUN on 2023/01/25.
//

import UIKit

class SelectUnitViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lbUnit: UILabel!
    
    @IBOutlet weak var deleteButton: UIView!
    
    var deleteCallback : (_ isSend : Bool) -> Void = { _ in}
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    static func nib() -> UINib {
       return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
   }

    var unit: Unit2ViewData! {
        didSet {
            lbUnit.text = unit.name
            
            let gesture = UITapGestureRecognizer(target: self, action: #selector(self.deleteButton(_:)))
            self.deleteButton.addGestureRecognizer(gesture)
        }
    }
    
    
    @objc func deleteButton(_ gesture: UITapGestureRecognizer) {
        deleteCallback(true)
    }
}
