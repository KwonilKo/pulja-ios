//
//  Unit2SelectTableViewCell.swift
//  pulja
//
//  Created by 김병헌 on 2022/10/26.
//

import UIKit
import M13Checkbox

class Unit2SelectTableViewCell: UITableViewCell {
    
    @IBOutlet weak var rootView: UIView!
    @IBOutlet weak var unit1View: UIView!
    @IBOutlet weak var lbUnit1Name: UILabel!
    @IBOutlet weak var lbUnit2Name: UILabel!
    @IBOutlet weak var cbUnit2: M13Checkbox!
    @IBOutlet weak var unit2CheckView: UIView!
    @IBOutlet weak var divider: UIView!
    @IBOutlet weak var unit2ViewTop: NSLayoutConstraint!
    //@IBOutlet weak var cbTop: NSLayoutConstraint!
    @IBOutlet weak var cbUnitAll: M13Checkbox!
    @IBOutlet weak var unitAllCheckView: UIView!
    
    var checkAllCallback : (_ isSend : Bool) -> Void = { _ in}
    var checkCallback : (_ isSend : Bool) -> Void = { _ in}
    var unit1Name : String?
    var unit2 : Unit2ViewData! {
        didSet {
            lbUnit2Name.text = unit2.name
            
            if unit2.selected {
                //cbUnit2.checkState = .checked
                cbUnit2.setCheckState(.checked, animated: false)
            } else {
                //cbUnit2.checkState = .unchecked
                cbUnit2.setCheckState(.unchecked, animated: false)
            }
            
            let gesture = UITapGestureRecognizer(target: self, action: #selector(self.clickCheckbox(_:)))
            self.unit2CheckView.addGestureRecognizer(gesture)
        }
    }
    
    var unit1 : Unit1ViewData! {
        didSet {
            if unit1.selected {
                //cbUnitAll.checkState = .checked
                cbUnitAll.setCheckState(.checked, animated: false)
            } else {
                //cbUnitAll.checkState = .unchecked
                cbUnitAll.setCheckState(.unchecked, animated: false)
            }
            
            let gesture2 = UITapGestureRecognizer(target: self, action: #selector(self.clickAllCheckbox(_:)))
            self.unitAllCheckView.addGestureRecognizer(gesture2)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initCheckbox()
    }
    
    func initCheckbox(){
        cbUnit2?.stateChangeAnimation = .bounce(.fill)
        cbUnit2?.tintColor = UIColor.purpleishBlue
        cbUnit2?.secondaryTintColor = UIColor.Foggy40
        cbUnit2?.checkmarkLineWidth = CGFloat(3)
        cbUnit2?.boxLineWidth = CGFloat(1)
        cbUnit2?.boxType = .square
        
        cbUnitAll?.stateChangeAnimation = .bounce(.fill)
        cbUnitAll?.tintColor = UIColor.purpleishBlue
        cbUnitAll?.secondaryTintColor = UIColor.Foggy40
        cbUnitAll?.checkmarkLineWidth = CGFloat(3)
        cbUnitAll?.boxLineWidth = CGFloat(1)
        cbUnitAll?.boxType = .square
    }
    
    @objc func clickCheckbox(_ gesture: UITapGestureRecognizer) {
        if unit2.selected {
            unit2.selected = false
            checkCallback(false)
        } else {
            unit2.selected = true
            checkCallback(true)
        }
    }
    
    @objc func clickAllCheckbox(_ gesture: UITapGestureRecognizer) {
        if unit1.selected {
            unit1.selected = false
            checkAllCallback(false)
        } else {
            unit1.selected = true
            checkAllCallback(true)
        }
    }
    
    static func nib() -> UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
