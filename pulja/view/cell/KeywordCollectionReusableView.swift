//
//  KeywordCollectionReusableView.swift
//  pulja
//
//  Created by 김병헌 on 2022/10/25.
//

import UIKit

class KeywordCollectionReusableView: UICollectionReusableView {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    static func nib() -> UINib {
       return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
   }
    
}
