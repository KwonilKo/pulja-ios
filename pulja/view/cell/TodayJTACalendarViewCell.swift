//
//  TodayJTACalendarViewCell.swift
//  pulja
//
//  Created by 김병헌 on 2022/02/14.
//

import UIKit
import JTAppleCalendar

class TodayJTACalendarViewCell: JTACDayCell {
    
    @IBOutlet var lbDate: UILabel!
    
    @IBOutlet var lbDay: UILabel!
}
