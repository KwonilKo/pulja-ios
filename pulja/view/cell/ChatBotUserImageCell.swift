//
//  ChatBotUserImageCell.swift
//  pulja
//
//  Created by samdols on 2022/02/02.
//

import UIKit

class ChatBotUserImageCell: UITableViewCell {

    static let cellHeight : CGFloat = 175
    
    @IBOutlet weak var studyImageView: UIImageView!
    @IBOutlet weak var lbTime: UILabel!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    static func nib() -> UINib {
            return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
