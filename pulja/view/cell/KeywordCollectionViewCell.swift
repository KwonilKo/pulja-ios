//
//  KeywordCollectionViewCell.swift
//  pulja
//
//  Created by 김병헌 on 2022/10/24.
//

import UIKit
import Kingfisher

class KeywordCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var ivKeyword: UIImageView!
    
    @IBOutlet weak var lbKeyword: UILabel!
    
    
    @IBOutlet weak var keywordView: UIView!
    
    
    
    var isSelectedBool : Bool = false {
        didSet {
            if self.isSelectedBool {
                self.keywordView.backgroundColor = UIColor.white
                self.keywordView.borderColor = UIColor.Purple
                self.lbKeyword.textColor = UIColor.Purple
                self.ivKeyword.tintColor = UIColor.Purple
            } else {
                self.keywordView.backgroundColor = UIColor.Purple.withAlphaComponent(0.1)
                self.keywordView.borderColor = UIColor.Foggy
                self.lbKeyword.textColor = UIColor.OffBlack
                self.ivKeyword.tintColor = UIColor.OffBlack
            }
        }
    }
    
    var keyword : KeywordData! {
               
               didSet {
                   lbKeyword.text = keyword.name
                   if let keywordSeq = keyword.keywordSeq {
                       let url = "https://pulja-contents.s3.ap-northeast-2.amazonaws.com/mobile_keyword/\(keywordSeq).png"
                       self.downloadImage(with: url) { image in
                           self.ivKeyword.image = image?.withRenderingMode(.alwaysTemplate)
                           self.ivKeyword.tintColor = .OffBlack
                       }

                   }
                   
                   if let isSelect = keyword.isSelect, isSelect {
                       self.keywordView.backgroundColor = UIColor.Purple.withAlphaComponent(0.1)
                       self.keywordView.borderColor = UIColor.Purple
                       self.lbKeyword.textColor = UIColor.Purple
                       self.ivKeyword.tintColor = UIColor.Purple
                   } else {
                       
                       if let isDisable = keyword.isDisable, isDisable {
                           self.keywordView.backgroundColor = UIColor.Foggy20
                           self.keywordView.borderColor = UIColor.Foggy
                           self.lbKeyword.textColor = UIColor.Foggy
                           self.ivKeyword.tintColor = UIColor.Foggy
                           
                       } else {
                           self.keywordView.backgroundColor = UIColor.white
                           self.keywordView.borderColor = UIColor.Foggy
                           self.lbKeyword.textColor = UIColor.OffBlack
                           self.ivKeyword.tintColor = UIColor.OffBlack

                       }
                       
                   }
                   
                   
                   
               }
           }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    static func nib() -> UINib {
       return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
   }
    
    
   
    
    
    func downloadImage(with urlString : String , imageCompletionHandler: @escaping (UIImage?) -> Void){
            
        guard let url = URL.init(string: urlString) else {
            return  imageCompletionHandler(nil)
        }
        let resource = ImageResource(downloadURL: url)
        KingfisherManager.shared.retrieveImage(with: resource, progressBlock: nil) { result in
            switch result {
            case .success(let value):
                imageCompletionHandler(value.image)
            case .failure:
                imageCompletionHandler(nil)
            }
        }
    }

}
