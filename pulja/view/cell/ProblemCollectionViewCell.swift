//
//  ProblemCollectionViewCell.swift
//  pulja
//
//  Created by HUN on 2023/01/09.
//

import UIKit
import Kingfisher

class ProblemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var rootView: UIView!
    @IBOutlet weak var lbLevel: UILabel!
    @IBOutlet weak var lbUnit: UILabel!
    @IBOutlet weak var lbFrame: UILabel!
    @IBOutlet weak var lbProgress1: UILabel!
    @IBOutlet weak var lbProgress2: UILabel!
    @IBOutlet weak var progress: UIView!
    @IBOutlet weak var progressFill: UIView!
    @IBOutlet weak var progressFillWidth: NSLayoutConstraint!
    @IBOutlet weak var ivFrameType: UIImageView!
    @IBOutlet weak var levelView: UIView!
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    static func nib() -> UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    var collection: CollectionDTOList! {
        didSet {
            lbUnit.text = collection.collectionUnitName ?? ""
            lbFrame.text = collection.collectionFramingTitle ?? ""
            lbLevel.text = "난이도 \(collection.difficultyText ?? "")"
            
            let questionCnt = collection.questionCnt ?? 0
            let userStudyQuestionCnt = collection.userStudyQuestionCnt ?? 0
            lbProgress1.text = "총 \(questionCnt)문제"
            lbProgress2.textColor = UIColor.grey
            
            if userStudyQuestionCnt == 0 {
                lbProgress2.text = "0%"
                lbProgress2.textColor = UIColor.Foggy
                progressFillWidth.constant = 0
            } else if questionCnt <= userStudyQuestionCnt {
                lbProgress2.text = "100%"
                progressFillWidth.constant = progress.frame.width
                progressFill.roundCorners(cornerRadius: 3, maskedCorners: [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner])
            } else {
                let rate = Double(100) / Double(questionCnt) * Double(userStudyQuestionCnt)
                let per = Int(rate.rounded())
                lbProgress2.text = "\(per)%"
                let width = progress.frame.width
                let constant = CGFloat(width / 100 * CGFloat(per)) + 2
                let checkWidth = width - 2
                if checkWidth > constant {
                    progressFillWidth.constant = constant
                } else {
                    progressFillWidth.constant = checkWidth
                }
                
                progressFill.roundCorners(cornerRadius: 3, maskedCorners: [.layerMinXMinYCorner, .layerMinXMaxYCorner])
            }
            
            let collectionFramingNumber = collection.collectionFramingNumber ?? 0
                
            switch collectionFramingNumber {
            case 1:
                levelView.borderColor = UIColor.OffBlue
                lbLevel.textColor = UIColor.OffBlue
                rootView.backgroundColor = UIColor.PastelBlue
                ivFrameType.image = UIImage(named : "iconFramingType1")
            case 2:
                levelView.borderColor = UIColor.OffOrange
                lbLevel.textColor = UIColor.OffOrange
                rootView.backgroundColor = UIColor.PastelOrange
                ivFrameType.image = UIImage(named : "iconFramingType2")
            case 3:
                levelView.borderColor = UIColor.OffRed
                lbLevel.textColor = UIColor.OffRed
                rootView.backgroundColor = UIColor.PastelRed
                ivFrameType.image = UIImage(named : "iconFramingType3")
            default:
                levelView.borderColor = UIColor.OffGreen
                lbLevel.textColor = UIColor.OffGreen
                rootView.backgroundColor = UIColor.PastelGreen
                ivFrameType.image = UIImage(named : "iconFramingType4")
            }
        }
    }
}
