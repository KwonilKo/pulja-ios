//
//  ProblemCollectionUnitViewCell.swift
//  pulja
//
//  Created by HUN on 2023/01/11.
//

import UIKit
import Kingfisher

class ProblemCollectionUnitViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var unitName: UILabel!
    @IBOutlet weak var framingName: UILabel!
    
    private var collectionList: [CollectionDTOList] = []
    
    func setCollectionList(list: [CollectionDTOList]) {
        collectionList.removeAll()
        collectionList = list
        collectionView?.setContentOffset(CGPointMake(0, 0), animated: false)
        collectionView.reloadData()
    }
    
    var clickCallback : (_ dto : CollectionDTOList) -> Void = { _ in}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setInit()
    }
    
    static func nib() -> UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    private func setInit() {
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.register(ProblemCollectionViewCell.nib(), forCellWithReuseIdentifier: "ProblemCollectionViewCell")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProblemCollectionViewCell", for: indexPath) as! ProblemCollectionViewCell
        if collectionList.isNotEmpty {
            if collectionList.count > indexPath.item {
                cell.collection = collectionList[indexPath.item]
            }
        }
        return cell
    }
    
    // Click
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionList.isNotEmpty {
            if collectionList.count > indexPath.item {
                clickCallback(collectionList[indexPath.item])
            }
        }
    }
}
