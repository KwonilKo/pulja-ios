//
//  LoadingTableViewCell.swift
//  pulja
//
//  Created by 김병헌 on 2022/01/18.
//

import UIKit

class LoadingTableViewCell: UITableViewCell {

    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static func nib() -> UINib {
                return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
            }
    func start() {
        activityIndicatorView.startAnimating()
    }
    
}
