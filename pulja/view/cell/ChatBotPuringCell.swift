//
//  ChatBotPuringCell.swift
//  pulja
//
//  Created by samdols on 2022/02/02.
//

import UIKit

class ChatBotPuringCell: UITableViewCell {

    
    @IBOutlet weak var topMarginHeightConstraint: NSLayoutConstraint!
    static let leadingConstant : CGFloat = 67
    static let tailConstant : CGFloat = 27
    static let timeWidth : CGFloat = 50
    static let timeLeadingConstant : CGFloat = 9
    static let sumWidth : CGFloat = leadingConstant + tailConstant + timeWidth + timeLeadingConstant
    static let sumWidthNotime : CGFloat = leadingConstant + tailConstant +  timeLeadingConstant
    
    static let messageLineHeight : CGFloat = 20
    static let messageMargin : CGFloat = 5
    static let cellMargin : CGFloat = 8
    static let puringNameHeight : CGFloat = 24
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileBgView: DesignableView!
    
    
    @IBOutlet weak var puringNameView: UIView!
    @IBOutlet weak var lbMessage: UILabel!
    @IBOutlet weak var lbTime: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    static func nib() -> UINib {
            return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
        }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
