//
//  RecommendKeywordCollectionViewCell.swift
//  pulja
//
//  Created by 김병헌 on 2022/11/07.
//

import UIKit
import Kingfisher

class RecommendKeywordCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var ivKeyword: UIImageView!
    
    @IBOutlet weak var lbKeyword: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    static func nib() -> UINib {
         return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
     }
    
    
    var keyword : KeywordData! {
        
        didSet {
            
            if let keywordSeq = keyword.keywordSeq {
                let urlString = "https://pulja-contents.s3.ap-northeast-2.amazonaws.com/mobile_keyword/\(keywordSeq)_result.png"
                let url = URL(string: urlString)
                self.ivKeyword.kf.setImage(with: url)
            }
            
            if var displayName = keyword.displayName {
                
                
                
                displayName = displayName.replacingOccurrences(of: "\\", with: "")
                displayName = displayName.replacingOccurrences(of: "n", with: "\n")

                let style = NSMutableParagraphStyle()
                style.alignment = NSTextAlignment.center
                style.lineHeightMultiple = 1.13
                let richText = NSMutableAttributedString(string: displayName,
                                                         attributes: [ NSAttributedString.Key.paragraphStyle: style ])

                
//                self.lbKeyword.text = displayName
                self.lbKeyword.attributedText = richText
            }
        }
        
    }
    
   
    
}
