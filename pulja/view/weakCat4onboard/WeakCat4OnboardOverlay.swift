//
//  WeakCat4OnboardOverlay.swift
//  pulja
//
//  Created by kwonilko on 2022/08/03.
//

import UIKit
import SwiftyOnboard


class WeakCat4OnboardOverlay: SwiftyOnboardOverlay {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
//    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var dotTopHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var dotBottomHeight: NSLayoutConstraint!
    
    
    
    
    
    @IBOutlet weak var lbTitle: UILabel!
    
    @IBOutlet weak var pcDot: UIPageControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "WeakCat4OnboardOverlay", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    

}
