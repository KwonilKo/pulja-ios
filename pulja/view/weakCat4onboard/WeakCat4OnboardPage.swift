//
//  WeakCat4OnboardPage.swift
//  pulja
//
//  Created by kwonilko on 2022/08/03.
//

import UIKit
import SwiftyOnboard



class WeakCat4OnboardPage: SwiftyOnboardPage {

    
    
    @IBOutlet weak var image: UIImageView!
//
    
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    class func instanceFromNib() -> UIView {
       return UINib(nibName: "WeakCat4OnboardPage", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
   }
    
    
}
