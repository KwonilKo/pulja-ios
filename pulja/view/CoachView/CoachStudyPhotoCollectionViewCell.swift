//
//  CoachStudyPhotoCollectionViewCell.swift
//  pulja
//
//  Created by kwonilko on 2022/03/22.
//

import UIKit

class CoachStudyPhotoCollectionViewCell: UICollectionViewCell
{
    
    
    @IBOutlet var imageView: UIImageView!
    
    @IBOutlet var dateLabel: UILabel!
    
    
    @IBOutlet var gradientView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

        let startColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        let endColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.60)
          
        self.gradientView.applyGradient(isVertical: true, colorArray: [startColor, endColor])
    }
    
    static func nib() -> UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
}
