//
//  CoachHomeView.swift
//  pulja
//
//  Created by kwonilko on 2022/03/20.
//

import UIKit

class CoachHomeView: UIView {
    
    
    
    
    @IBOutlet var lbStudent: UILabel!
    
    
    
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
    }
        
    override init(frame: CGRect) {
            super.init(frame: frame)
    }
    
    private func loadXib() {
            let identifier = String(describing: type(of: self))
            let nibs = Bundle.main.loadNibNamed(identifier, owner: self, options: nil)
            
            guard let customView = nibs?.first as? UIView else { return }
            customView.frame = self.bounds
            self.addSubview(customView)
        }
    
    
    
    
    
}
