//
//  StudyCurriculumHeaderView.swift
//  pulja
//
//  Created by 고권일 on 2022/01/10.
//

import UIKit

class StudyCurriculumHeaderView: UITableViewHeaderFooterView {

    var isBigCircle: Bool?
    var sectionIdx: Int?
    var sectionIdxForDelayed: Int?
    var totalSection : Int?
    
    @IBOutlet weak var BigCircleView: UIView!
    
    
    @IBOutlet weak var lastStick: UIView!
    
    @IBOutlet weak var BigDate: UILabel!
    
    @IBOutlet weak var circleView: UIView!
    
    
    @IBOutlet weak var lbDate: UILabel!
    var lbdate : String?
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var isCoachingTest : Bool = false
    
    
    override func awakeFromNib() {
        print("헤더 bool: \(isBigCircle), lb: \(lbdate)")
        if let bool = isBigCircle, let lb = lbdate {
            if bool {   //오늘의 학습 처리
                BigCircleView.isHidden = false
                BigCircleView.layer.borderWidth = 1.0
                BigCircleView.layer.masksToBounds = false
                BigCircleView.layer.borderColor = UIColor.white.cgColor
                BigCircleView.layer.cornerRadius = BigCircleView.frame.size.width / 2
                BigCircleView.clipsToBounds = true
                
                BigDate.text = (isCoachingTest == true) ? lb.substring(from: 4, to : 5) : lb.substring(from: 6, to : 7)
//                BigDate.text = lb.substring(from: 6, to : 7)
                lastStick.isHidden = true
                
            } else if let sI = sectionIdx, let tS = totalSection {
                if sI == tS - 1 {   //이전 학습 처리
                    print("sI: \(sI), tS: \(tS)")
                    BigCircleView.isHidden = false
                    BigCircleView.layer.borderWidth = 1.0
                    BigCircleView.layer.masksToBounds = false
                    BigCircleView.layer.borderColor = UIColor.white.cgColor
                    BigCircleView.layer.cornerRadius = BigCircleView.frame.size.width / 2
                    BigCircleView.clipsToBounds = true
                    BigDate.text = lb.substring(from: 6, to : 7)
                    lastStick.isHidden = true
                } else {
                    BigCircleView.isHidden = true
                }
            } else if let sI = sectionIdxForDelayed, let tS = totalSection {
                if sI == tS - 1 {   //밀린 학습 처리
                    print("sI: \(sI), tS: \(tS)")
                    BigCircleView.isHidden = true
                    lastStick.isHidden = true
                } else {
                    BigCircleView.isHidden = true
                    lastStick.isHidden = false
                }
            }
            else {
                BigCircleView.isHidden = true
            }
            //공통적으로 필요한 부분
            circleView.layer.borderWidth = 1.0
            circleView.layer.masksToBounds = false
            circleView.layer.borderColor = UIColor.white.cgColor
            circleView.layer.cornerRadius = circleView.frame.size.width / 2
            circleView.clipsToBounds = true
            
            
            
            let y = Int(lb.substring(from : 0, to : 3))
            let m = Int(lb.substring(from : 4, to : 5))
            let d = Int(lb.substring(from : 6, to : 7))
            print("\(y)\(m)\(d)")
            print("\(lb)")
            
            lbDate.text = self.weekday(year: y!, month: m!, dayy: d!)
//            lbDate.text = (isCoachingTest == true) ? lb : self.weekday(year: y!, month: m!, dayy: d!)
//            lbDate.text = self.weekday(year: y!, month: m!, dayy: d!)
//            lbDate.text = "\(lb.substring(from : 0, to : 3))년 \(lb.substring(from: 4, to: 5))월 \(lb.substring(from: 6, to : 7))일"
//
        } else {
            BigCircleView.isHidden = true
        }
    
    }
    
    
    
    /// 특정 연도, 월, 일에 대한 요일을 구하는 메소드
    ///
    /// - Parameter year: 연도
    /// - Parameter month: 월
    /// - Parameter day: 일
    ///
    /// - Returns: 요일에 해당하는 문자열 Ex) Sun, Sunday, S
    /// - note: day에 대한 안정성을 보장하지 않음
    ///         Ex) 2019-02-40을 넣으면 2월1일로부터 40일이 되는날(3월12일)의 요일이 반환됨
    func weekday(year: Int, month: Int, dayy: Int) -> String? {
        let calendar = Calendar(identifier: .gregorian)
        
        guard let targetDate: Date = {
            let comps = DateComponents(calendar:calendar, year: year, month: month, day: dayy)
            return comps.date
            }() else { return nil }
        
        let day = Calendar.current.component(.weekday, from: targetDate) - 1
        let detailedDay:String? = Calendar.current.weekdaySymbols[day]

        print("\(month)월 \(dayy)일 \(detailedDay!)")
        return "\(month)월 \(dayy)일 \(detailedDay!)"

    
    }

}
