//
//  StudyCurriculumFooterView.swift
//  pulja
//
//  Created by 고권일 on 2022/01/13.
//

import UIKit
import Collections

class StudyCurriculumFooterView: UITableViewHeaderFooterView {

    
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var lastStickView: UIView!
    @IBOutlet weak var cellButton: UIButton!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    //밀린학습 커리큘럼 보는데 필요한 variable
    var isOpen = true
    var ListStudyDaysCurr : OrderedDictionary<String, OrderedDictionary<String, [CurriculumUnit]>>? = nil
    var ListStudyDaysExpand : OrderedDictionary<String, OrderedDictionary<String, Bool>>? = nil
    var secDate : String = ""
    var delegate : CustomFooterViewDelegate?
    
    
    //오늘의 커리큘럼 보는데 필요한 variable
    var todayCurriculum : [CurriculumUnit]?
    var todayDate : String = ""
    var todayIsOpen : Bool?
    
    var sectionIdx: Int?
    var totalSection : Int?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if footerView != nil {
            let gesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender: )))
            footerView.addGestureRecognizer(gesture)
        }
            
        if let sI = sectionIdx, let tS = totalSection {
            if sI == tS - 1 {
                lastStickView.isHidden = true
            } else {
                lastStickView.isHidden = false
            }
        }
        
    }
    
    
    @IBAction func buttonClick(_ sender: Any) {
        print("버튼 클릭함")
        //        var open = [String : Bool]()
        print(delegate)
        if self.footerView != nil {
            UIView.animate(withDuration: 0.1, animations: {
                self.footerView.backgroundColor =
                UIColor.puljaBlack.withAlphaComponent(0.1)
            }, completion: {
                finished in
                self.footerView.backgroundColor = UIColor.OffWhite
                self.delegate?.didTouchFooter(self.secDate, self.ListStudyDaysExpand!)
            })
        }
        
//        delegate?.didTouchFooter(secDate, ListStudyDaysExpand!)
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        
        if self.footerView != nil {
            UIView.animate(withDuration: 0.1, animations: {
                self.footerView.backgroundColor =
                UIColor.puljaBlack.withAlphaComponent(0.1)
            }, completion: {
                finished in
                self.footerView.backgroundColor = UIColor.OffWhite
                self.delegate?.didTouchFooter(self.secDate, self.ListStudyDaysExpand!)
            })
        }
        
//        delegate?.didTouchFooter(secDate, ListStudyDaysExpand!)
    }
    
}


protocol CustomFooterViewDelegate : AnyObject {
    func didTouchFooter(_ secDate: String, _ ListStudyDaysExpand : OrderedDictionary<String, OrderedDictionary<String, Bool>> )
}
