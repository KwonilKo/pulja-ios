//
//  StudyCurriculumMyView.swift
//  pulja
//
//  Created by 고권일 on 2022/01/18.
//

import UIKit

class StudyCurriculumMyView: UIView {

    @IBOutlet weak var customView: UIView!
    
    @IBOutlet weak var ImgCurr: UIImageView!
    var imageName = ""
    
    @IBOutlet weak var lbUnit: UILabel!
    var subjectname = ""
    var unit1name = ""
    
    @IBOutlet weak var lbLecProb: UILabel!
    var complete = false
    var lecnum = 0
    var probnum = 0
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "StudyCurriculumMyView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
//        initialize()

    }

    override init(frame: CGRect) {
        super.init(frame: frame)
//        initialize()
    }
    
//    func initialize(cu: CurriculumSimpleInfo, imagename: String) {
////        guard let myView = loadViewFromNib(nib: "StudyCurriculumMyView") else { return }
////        var image : UIImage = UIImage(named: imageName)!
////        ImgCurr = UIImageView(image: image)
////        lbUnit.text = "\(subjectname) | \(unit1name)"
////        if complete {
////            lbLecProb.text = "학습 완료"
////        } else {
////            lbLecProb.text = "강의 \(lecnum)강, 문제 \(probnum)개 미학습"
////        }
////        customView.frame = self.bounds
////        addSubview(customView)
//
//        var image : UIImage = UIImage(named: imagename)!
//        ImgCurr = UIImageView(image: image)
//        lbUnit.text = "\(cu.subjectName!) | \(cu.unit1Name!)"
//        if complete {
//            lbLecProb.text = "학습 완료"
//        } else {
//            lbLecProb.text = "강의 \(cu.restVideoCnt!)강, 문제 \(cu.restProblemCnt!)개 미학습"
//        }
//
//        print("여기: \(lbLecProb.text), \(lbUnit.text), \(imageName)")
//
//
//
////        var image : UIImage = UIImage(named: imageName)!
////        ImgCurr = UIImageView(image: image)
////        lbUnit.text = "\(subjectname) | \(unit1name)"
////        if complete {
////            lbLecProb.text = "학습 완료"
////        } else {
////            lbLecProb.text = "강의 \(lecnum)강, 문제 \(probnum)개 미학습"
////        }
////
////        print("여기: \(lbLecProb.text), \(lbUnit.text), \(imageName)")
//
//
//    }

}

//extension UIView {
//    func loadViewFromNib(nib: String) -> UIView? {
//        let bundle = Bundle(for: type(of: self))
//        let nib = UINib(nibName: nib, bundle: bundle)
//        return nib.instantiate(withOwner: self, options: nil).first as? UIView
//    }
//}

