//
//  StudyCurriculumTableViewCell.swift
//  pulja
//
//  Created by 고권일 on 2022/01/08.
//

import UIKit

class StudyCurriculumTableViewCell: UITableViewCell {

    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lbCurriculum: UILabel!
    var currUnitName : String?
    
    @IBOutlet weak var lbStatus: UILabel!
    var studyStatusName : String?
    
    @IBOutlet weak var lbVideo: UIView!
    
    
    @IBOutlet weak var VideoLabelView: UIView!
    @IBOutlet weak var VideoImg: UIImageView!
    
    @IBOutlet weak var lbNumVideo: UILabel!
    var videoCnt : Int?
    var cunitSeq : Int?
    var currSeq : Int?
    var userSeq : Int?
    
    @IBOutlet weak var lbProblems: UIView!
    
    
    @IBOutlet weak var ProblemImg: UIImageView!
    
    @IBOutlet weak var ProblemLabelView: UIView!
    
    @IBOutlet weak var lbNumProblem: UILabel!
    var problemCnt : Int?
    var isCoachingTest: Bool?
    var connectFromCoach: Bool = false 
    
    
    @IBOutlet weak var lastStick: UIView!
    
    @IBOutlet weak var btArrow: UIButton!
    
    var sectionIdx: Int?
    var totalSection : Int?
    
    var authCallback : ((_ currUnitName : String, _ studyStatusName : String,
                         _ videoCnt : Int, _ problemCnt: Int ) -> Void)?
    
    
    var delegateController : PuljaBaseViewController?
    
    var studyDate : String?

    var coachingTest : CoachingTest?
    var indexPath: IndexPath?
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ProblemLabelView.layer.cornerRadius = ProblemLabelView.layer.bounds.width / 2
        VideoLabelView.layer.cornerRadius = VideoLabelView.layer.bounds.width / 2
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender: )))
        mainView.addGestureRecognizer(gesture)
        
        
//        selectedBackgroundView = {
//            let view = UIView.init()
//            view.backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
//            return view
//        }()
        
        //코칭테스트인지 판별
        if (studyStatusName != nil && currUnitName != nil && videoCnt != nil && problemCnt != nil)  {
            
            if let sI = sectionIdx, let tS = totalSection {
                if sI == tS - 1 {
                    lastStick.isHidden = true
                }
                else {
                    lastStick.isHidden = false
                }
            }
            
            lbCurriculum.text = currUnitName
            lbStatus.text = studyStatusName
            lbNumProblem.text = "\(problemCnt!)"
            lbNumVideo.text = "\(videoCnt!)"
            
        
            // 영상, 문제 개수 유무에 따른 처리작업
            if (videoCnt! > 0 && problemCnt! == 0) {
                lbProblems.isHidden = true
                VideoLabelView.backgroundColor = UIColor.Purple
                VideoImg.backgroundColor = UIColor.Purple
                VideoImg.image = UIImage(named: "StudyCamera")
                VideoLabelView.borderColor = UIColor.Purple
                lbNumVideo.text = "\(videoCnt!)"
                
            } else if (videoCnt! == 0 && problemCnt! > 0) {
                lbProblems.isHidden = true
                VideoLabelView.backgroundColor = UIColor.Green
                VideoImg.backgroundColor = UIColor.Green
                VideoImg.image = UIImage(named: "StudyPaper")
                VideoLabelView.borderColor = UIColor.Green
                
                lbNumVideo.text = "\(problemCnt!)"
            } else if (videoCnt! == 0 && problemCnt! == 0) {
                lbProblems.isHidden = true
                lbVideo.isHidden = true
            } else {
                lbVideo.isHidden = false
                VideoLabelView.backgroundColor = UIColor.Purple
                VideoImg.backgroundColor = UIColor.Purple
                VideoImg.image = UIImage(named: "StudyCamera")
                VideoLabelView.borderColor = UIColor.Purple
                lbNumVideo.text = "\(videoCnt!)"
                
                lbProblems.isHidden = false
                ProblemLabelView.backgroundColor = UIColor.Green
                ProblemImg.backgroundColor = UIColor.Green
                ProblemImg.image = UIImage(named: "StudyPaper")
                ProblemLabelView.borderColor = UIColor.Green
                
            }
        
        }
        
        if (studyStatusName == "학습 중" || studyStatusName == "학습 전") {
            mainView.backgroundColor = UIColor.PalePink
            lbStatus.textColor = UIColor.OffRed

        } else {
            mainView.backgroundColor = UIColor.OffWhite
            lbStatus.textColor = UIColor.Foggy
        }
        
    }
    
    
    func handleTouchAction() {
        if self.connectFromCoach == false {
            if let status = self.studyStatusName, status == "학습 완료" {
                if let iscoach = self.isCoachingTest, iscoach == true {
                    LoadingView.show()
                    StudyAPI.shared.coachingTestResult(studyDay: self.studyDate!, userSeq: self.userSeq!).done {
                        res in
                        if let suc = res.success, suc == true {
                            print("풀이 결과 리스펀스 성공함")
                            let solveResult = res.data!
                            let thisWeekStudy = solveResult.thisWeekStudy!
                            let lastWeekStudy = solveResult.lastWeekStudy!
                            let nowStudy = solveResult.nowStudy!
                            
                            let vc = R.storyboard.study.studySolveResultViewController()!
                            vc.thisWeekStudy = thisWeekStudy
                            vc.lastWeekStudy = lastWeekStudy
                            vc.nowStudy = nowStudy
                            vc.studyDay = self.studyDate
                            self.delegateController?.navigationController?.pushViewController(vc, animated: true)
                        }
                    }.catch { error in
                        print(error.localizedDescription)
                    }.finally {
                        LoadingView.hide()
                    }
                
                } else {
                    
                    //학습 완료이지만, 동영상이 있는 경우, 동영상 화면 보내주어야 함
                    if let videoc = self.videoCnt, videoc > 0   {
                        let vc1 = R.storyboard.study.studyVideoViewController()!
                        vc1.userSeq = userSeq
                        vc1.cunitSeq = cunitSeq
                        vc1.problemCnt = problemCnt
                        vc1.currSeq = currSeq
                        vc1.videoCnt = videoCnt
                        vc1.studyDate = self.studyDate
                        vc1.celldelegateViewController = self.delegateController
                        vc1.boolCompleteVideoProb = true
                        self.delegateController?.navigationController?.pushViewController(vc1, animated: true)
                    } else {
                        let vc = R.storyboard.study.studySolveResultViewController()!
                        LoadingView.show()
                        StudyAPI.shared.curriculumStudyResult(cunitSeq: self.cunitSeq!, currSeq: self.currSeq!, studyDate: self.studyDate!, userSeq: self.userSeq!).done {
                            res in
                            if let suc = res.success, suc == true {
                                print("풀이 결과 리스펀스 성공함")
                                guard let solveResult = res.data else { return }
                                let thisWeekStudy = solveResult.thisWeekStudy!
                                let lastWeekStudy = solveResult.lastWeekStudy!
                                let nowStudy = solveResult.nowStudy!
                                
                                let vc = R.storyboard.study.studySolveResultViewController()!
                                vc.thisWeekStudy = thisWeekStudy
                                vc.lastWeekStudy = lastWeekStudy
                                vc.nowStudy = nowStudy
                                vc.currSeq = self.currSeq
                                vc.cunitSeq = self.cunitSeq
                                self.delegateController?.navigationController?.pushViewController(vc, animated: true)
                            }
                        }.catch { error in
                            
                        }.finally {
                            LoadingView.hide()
                        }
                    }
                }
            } else {
                if videoCnt! > 0 {
                    let vc = R.storyboard.study.studyVideoViewController()!
                    vc.userSeq = userSeq
                    vc.cunitSeq = cunitSeq
                    vc.problemCnt = problemCnt
                    vc.currSeq = currSeq
                    vc.videoCnt = videoCnt
                    vc.studyDate = self.studyDate
                    vc.celldelegateViewController = self.delegateController
        //            self.delegateController?.present(vc, animated: true)
                    self.delegateController?.navigationController?.pushViewController(vc, animated: true)
                } else {
                    let vc = R.storyboard.study.multipleChoiceViewController()!
                    if let iscoach = self.isCoachingTest, iscoach == true {
                        vc.isCoachingTest = true
                    }
                    
                    vc.problemCnt = problemCnt
                    vc.videoCnt = videoCnt
                    vc.userSeq = userSeq
                    vc.currSeq = currSeq
                    vc.cunitSeq = cunitSeq
                    vc.userResponseList = []
    //                vc.trackStatus = 0
                    vc.studyDate = self.studyDate
                    vc.celldelegateViewController = self.delegateController
        //            self.delegateController?.present(vc, animated: true)
                    print("코칭테스트: \(vc.celldelegateViewController!)")
                    self.delegateController?.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        else {
            //코칭에서 접속했지만 학습 완료 인 경우
            if let status = self.studyStatusName, status == "학습 완료" {
                //학습 완료이지만, 동영상이 있는 경우, 동영상 화면 보내주어야 함
                if let videoc = self.videoCnt, videoc > 0   {
                    let vc1 = R.storyboard.study.studyVideoViewController()!
                    vc1.userSeq = userSeq
                    vc1.cunitSeq = cunitSeq
                    vc1.problemCnt = problemCnt
                    vc1.currSeq = currSeq
                    vc1.videoCnt = videoCnt
                    vc1.studyDate = self.studyDate
                    vc1.celldelegateViewController = self.delegateController
                    vc1.boolCompleteVideoProb = true
                    vc1.connectFromCoach = true
                    self.delegateController?.navigationController?.pushViewController(vc1, animated: true)
                } else {
                    let vc = R.storyboard.study.studySolveResultViewController()!
                    LoadingView.show()
                    StudyAPI.shared.curriculumStudyResult(cunitSeq: self.cunitSeq!, currSeq: self.currSeq!, studyDate: self.studyDate!, userSeq: self.userSeq!).done {
                        res in
                        if let suc = res.success, suc == true {
                            print("풀이 결과 리스펀스 성공함")
                            guard let solveResult = res.data else { return }
                            let thisWeekStudy = solveResult.thisWeekStudy!
                            let lastWeekStudy = solveResult.lastWeekStudy!
                            let nowStudy = solveResult.nowStudy!
                            
                            let vc = R.storyboard.study.studySolveResultViewController()!
                            vc.userSeq = self.userSeq!
                            vc.thisWeekStudy = thisWeekStudy
                            vc.lastWeekStudy = lastWeekStudy
                            vc.nowStudy = nowStudy
                            vc.currSeq = self.currSeq
                            vc.cunitSeq = self.cunitSeq
                            vc.connectFromCoach = true 
                            self.delegateController?.navigationController?.pushViewController(vc, animated: true)
                        }
                    }.catch { error in
                        
                    }.finally {
                        LoadingView.hide()
                    }
                }
            
            }
        
        
        }
        
    }
            
    
    
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
//        self.selectedBackgroundView?.backgroundColor = UIColor.puljaBlack
//        self.handleTouchAction()
        
        guard let status = self.studyStatusName else { return }

        if self.connectFromCoach, status != "학습 완료" {
            return
        }

        let nextViewColor = (status == "학습 완료") ? UIColor.Foggy.withAlphaComponent(0.6) : UIColor.PalePinkTouched
        let resetViewColor = (status == "학습 완료") ? UIColor.OffWhite : UIColor.PalePink
        
        
        
        
        UIView.animate(withDuration: 0.1, animations: {
            self.mainView.backgroundColor = nextViewColor
        }, completion:
            { finished in
            self.mainView.backgroundColor = resetViewColor
            self.handleTouchAction()
        })
//        self.handleTouchAction()
    
    }
    
    
    static func nib() -> UINib {
            return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
//        self.selectedBackgroundView?.backgroundColor = UIColor.puljaBlack
    }
    
    @IBAction func cellExpandClicked(_ sender: Any) {
        guard let status = self.studyStatusName else { return }
        let nextViewColor = (status == "학습 완료") ? UIColor.Foggy.withAlphaComponent(0.6) : UIColor.PalePinkTouched
        let resetViewColor = (status == "학습 완료") ? UIColor.OffWhite : UIColor.PalePink
        
        UIView.animate(withDuration: 0.1, animations: {
            self.mainView.backgroundColor = nextViewColor
        }, completion:
            { finished in
            self.mainView.backgroundColor = resetViewColor
            self.handleTouchAction()
        })
//        self.handleTouchAction()
            
            
    }
        
    
    
}
