//
//  StudyCurriculumMultipleTableViewCell.swift
//  pulja
//
//  Created by 고권일 on 2022/01/11.
//

import UIKit
import Collections

class StudyCurriculumMultipleTableViewCell: UITableViewCell {

    @IBOutlet weak var lbCurriculum: UILabel!
    
    
    @IBOutlet weak var ProblemLabelView: UIView!
    
    @IBOutlet weak var ProblemImg: UIImageView!
    
    @IBOutlet weak var lbStatus: UILabel!
    
    
    @IBOutlet weak var lbNumVideo: UILabel!
    @IBOutlet weak var lbNumProblem: UILabel!
    
    @IBOutlet weak var cellButton: UIButton!
    
    
    @IBOutlet weak var VideoImg: UIImageView!
    @IBOutlet weak var VideoLabelView: UIView!
    
    
    @IBOutlet weak var tableCellMainView: UIView!
    
    
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var thirdView: UIView!

    var currUnitName : String?
    var studyStatusName : String?
    var problemCnt : Int? 
    var videoCnt : Int?
    var cunitSeq : Int?
    var currSeq : Int?
    var userSeq : Int?
    var studyDate : String?
    
    @IBOutlet weak var lbProblems: UIView!
    
    @IBOutlet weak var lbVideo: UIView!
    
    
    
    @IBOutlet weak var lastStsick: UIView!
    var sectionIdx: Int?
    var totalSection : Int?
    
    var secDate : String = ""
    var rowStatus : String = ""
    var ListStudyDaysCurr : OrderedDictionary<String, OrderedDictionary<String, [CurriculumUnit]>>? = nil
    var ListStudyDaysExpand : OrderedDictionary<String, OrderedDictionary<String, Bool>>? = nil
    
    var delegate : CustomTableViewDelegate?
    var delegateController : PuljaBaseViewController?
    var indexPath: IndexPath?
    var connectFromCoach: Bool = false 
    
    var isOpened: Bool = false {
        didSet {
            isOpened ? self.cellButton.setImage(UIImage(named : "iconSolidCheveronRight"), for : .normal)
            : self.cellButton.setImage(UIImage(named: "iconSolidCheveronDown"), for : .normal)
            
            if self.connectFromCoach, self.isOpened, let status = self.studyStatusName, status != "학습 완료" {
                self.cellButton.isHidden = true
            } else {
                self.cellButton.isHidden = false
            }
         
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender: )))
        thirdView.addGestureRecognizer(gesture)
        
//        // cell 선택된 경우 색깔
//        selectedBackgroundView = {
//            let view = UIView.init()
//            view.backgroundColor = UIColor.puljaBlack.withAlphaComponent(0.1)
//            return view
//        }()
        
        
        if self.ListStudyDaysExpand != nil {
            
            if let sI = sectionIdx, let tS = totalSection {
                
                if sI == tS - 1 {
                    lastStsick.isHidden = true 
                } else {
                    lastStsick.isHidden = false
                }
            }
            
            lbCurriculum.text = currUnitName
            lbStatus.text = studyStatusName
            
            lbNumProblem.text = "\(problemCnt!)"
            lbNumVideo.text = "\(videoCnt!)"
            
            //학습 전,중,완료에 따른 색깔 처리
            if (studyStatusName == "학습 중" || studyStatusName == "학습 전") {
                firstView.backgroundColor = UIColor.PalePink
                secondView.backgroundColor =  UIColor.PalePink
                thirdView.backgroundColor = UIColor.PalePink
                lbStatus.textColor = UIColor.OffRed

            } else {
                firstView.backgroundColor = UIColor.OffWhite
                secondView.backgroundColor = UIColor.OffWhite
                thirdView.backgroundColor = UIColor.OffWhite
                lbStatus.textColor = UIColor.Foggy
            }
            
            if let openStatus = self.ListStudyDaysExpand![secDate]![rowStatus], openStatus == true {
                print("셀 date: \(secDate), 학습상태: \(rowStatus), 오픈여부: \(isOpened)")
                firstView.isHidden = true
                secondView.isHidden = true
                
                isOpened = true
            } else {
                firstView.isHidden = false
                secondView.isHidden = false
                isOpened = false
            }
            
        } else {
            firstView.isHidden = false
            secondView.isHidden = false
            isOpened = false
        }
        
        if (videoCnt != nil && problemCnt != nil)  {
            lbNumProblem.text = "\(problemCnt!)"
            lbNumVideo.text = "\(videoCnt!)"
            
        
            // 영상, 문제 개수 유무에 따른 처리작업
            if (videoCnt! > 0 && problemCnt! == 0) {
                lbProblems.isHidden = true
                VideoLabelView.backgroundColor = UIColor.Purple
                VideoImg.backgroundColor = UIColor.Purple
                VideoImg.image = UIImage(named: "StudyCamera")
                VideoLabelView.borderColor = UIColor.Purple
                lbNumVideo.text = "\(videoCnt!)"
                
            } else if (videoCnt! == 0 && problemCnt! > 0) {
                lbProblems.isHidden = true
                VideoLabelView.backgroundColor = UIColor.Green
                VideoImg.backgroundColor = UIColor.Green
                VideoImg.image = UIImage(named: "StudyPaper")
                VideoLabelView.borderColor = UIColor.Green
                
                lbNumVideo.text = "\(problemCnt!)"
            } else if (videoCnt! == 0 && problemCnt! == 0) {
                lbProblems.isHidden = true
                lbVideo.isHidden = true
            } else {
                lbVideo.isHidden = false
                VideoLabelView.backgroundColor = UIColor.Purple
                VideoImg.backgroundColor = UIColor.Purple
                VideoImg.image = UIImage(named: "StudyCamera")
                VideoLabelView.borderColor = UIColor.Purple
                lbNumVideo.text = "\(videoCnt!)"
                
                lbProblems.isHidden = false
                ProblemLabelView.backgroundColor = UIColor.Green
                ProblemImg.backgroundColor = UIColor.Green
                ProblemImg.image = UIImage(named: "StudyPaper")
                ProblemLabelView.borderColor = UIColor.Green
                
            }
        
        }
        
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        
        guard let status = self.studyStatusName else { return }
        let nextViewColor = (status == "학습 완료") ? UIColor.Foggy.withAlphaComponent(0.6) : UIColor.PalePinkTouched
        let resetViewColor = (status == "학습 완료") ? UIColor.OffWhite : UIColor.PalePink
        self.handleTouchAction()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
//        self.selectedBackgroundView?.backgroundColor = UIColor.puljaBlack
    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 10))
//    }
    
    static func nib() -> UINib {
            return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    
    @IBAction func cellExpandClicked(_ sender: Any) {
        print("멀티뷰셀에서 익스팬드 누름")
        
        guard let status = self.studyStatusName else { return }
        if connectFromCoach, isOpened, let status = self.studyStatusName, status != "학습 완료" {
            return
        }
        
        
        let nextViewColor = (status == "학습 완료") ? UIColor.Foggy.withAlphaComponent(0.6) : UIColor.PalePinkTouched
        let resetViewColor = (status == "학습 완료") ? UIColor.OffWhite : UIColor.PalePink
    
        self.handleTouchAction()
    }
            
    func handleTouchAction(){
        
        guard let status = self.studyStatusName else { return }
        let nextViewColor = (status == "학습 완료") ? UIColor.Foggy.withAlphaComponent(0.6) : UIColor.PalePinkTouched
        let resetViewColor = (status == "학습 완료") ? UIColor.OffWhite : UIColor.PalePink
        
        //셀 펼치기
        if isOpened == false {
            
            //겹쳐져 있는 화면 두개 감추어주기
            UIView.animate(withDuration: 0.1, animations: {
//                self.firstView.backgroundColor = nextViewColor
//                self.secondView.backgroundColor = nextViewColor
                self.thirdView.backgroundColor = nextViewColor
                
                self.firstView.isHidden = true
                self.secondView.isHidden = true
            }, completion: {finished in
                
//                //두개 감추기
//                self.firstView.isHidden = true
//                self.secondView.isHidden = true
//                self.thirdView.backgroundColor = resetViewColor
                
                //ListDaysStudyCurr에 저장되어 있는거 뿌려주기
                let eachCurr = self.ListStudyDaysCurr![self.secDate]![self.rowStatus]
                let eachCurrlen = eachCurr!.count
                let remainCurr = eachCurr![1..<eachCurrlen]
                self.delegate?.didTouchCell(self.secDate, self.rowStatus, self.ListStudyDaysExpand!, self.indexPath)
                self.isOpened.toggle()
            })
        
            
//            firstView.isHidden = true
//            secondView.isHidden = true
//            thirdView.backgroundColor = UIColor.PalePinkTouched
//
//            //ListDaysStudyCurr에 저장되어 있는거 뿌려주기
//            let eachCurr = ListStudyDaysCurr![self.secDate]![self.rowStatus]
//            let eachCurrlen = eachCurr!.count
//            let remainCurr = eachCurr![1..<eachCurrlen]
//
//
//            print("cell상태 : \(self.secDate), \(self.rowStatus), \(ListStudyDaysExpand![self.secDate]![self.rowStatus])")
//            delegate?.didTouchCell(secDate, rowStatus, ListStudyDaysExpand!, self.indexPath)
//            isOpened.toggle()
        }
        
        //셀 닫기 (문제 또는 강의 푸는 화면 이동)
        else {
            
            if self.connectFromCoach == true, let status = self.studyStatusName, status != "학습 완료" {
                return
            }
            
            UIView.animate(withDuration: 0.1, animations: {
                self.thirdView.backgroundColor = nextViewColor
            }, completion: { finished in
                self.thirdView.backgroundColor = resetViewColor
                if self.connectFromCoach == false {
                    if let status = self.studyStatusName, status == "학습 완료" {
                        //학습 완료이지만, 동영상이 있는 경우, 동영상 화면 보내주어야 함
                        if let videoc = self.videoCnt, videoc > 0 {
                            let vc1 = R.storyboard.study.studyVideoViewController()!
                            vc1.userSeq = self.userSeq
                            vc1.cunitSeq = self.cunitSeq
                            vc1.problemCnt = self.problemCnt
                            vc1.currSeq = self.currSeq
                            vc1.videoCnt = self.videoCnt
                            vc1.studyDate = self.studyDate
                            vc1.celldelegateViewController = self.delegateController
                            vc1.boolCompleteVideoProb = true
                            self.delegateController?.navigationController?.pushViewController(vc1, animated: true)
                        } else {
                            let vc = R.storyboard.study.studySolveResultViewController()!
                            LoadingView.show()
                            StudyAPI.shared.curriculumStudyResult(cunitSeq: self.cunitSeq!, currSeq: self.currSeq!, studyDate: self.studyDate!, userSeq: self.userSeq!).done {
                                res in
                                if let suc = res.success, suc == true {
                                    print("풀이 결과 리스펀스 성공함")
                                    guard let solveResult = res.data else { return }
                                    let thisWeekStudy = solveResult.thisWeekStudy!
                                    let lastWeekStudy = solveResult.lastWeekStudy!
                                    let nowStudy = solveResult.nowStudy!
                                    
                                    let vc = R.storyboard.study.studySolveResultViewController()!
                                    vc.thisWeekStudy = thisWeekStudy
                                    vc.lastWeekStudy = lastWeekStudy
                                    vc.nowStudy = nowStudy
                                    vc.currSeq = self.currSeq
                                    vc.cunitSeq = self.cunitSeq
                                    self.delegateController?.navigationController?.pushViewController(vc, animated: true)
                                }
                            }.catch { error in
                                
                            }.finally {
                                LoadingView.hide()
                            }
                        }
                    } else {
                        //강의, 문제 같이 있는 경우 강의 화면으로 이동
                        if self.videoCnt! > 0 {
                            print("영상 보여주어야 함 \(self.isOpened)")
                            let vc = R.storyboard.study.studyVideoViewController()!
                            vc.userSeq = self.userSeq
                            vc.cunitSeq = self.cunitSeq
                            vc.problemCnt = self.problemCnt
                            vc.currSeq = self.currSeq
                            vc.videoCnt = self.videoCnt
                            vc.studyDate = self.studyDate
                            vc.celldelegateViewController = self.delegateController
                            self.delegateController?.navigationController?.pushViewController(vc, animated: true)
                            //                self.delegateController?.navigationController?.pushViewController(vc, animated: false)
                        } else {
                            print("문제 보여주어야 함 \(self.isOpened)")
                            let vc = R.storyboard.study.multipleChoiceViewController()!
                            vc.problemCnt = self.problemCnt
                            vc.videoCnt = self.videoCnt
                            vc.userSeq = self.userSeq
                            vc.currSeq = self.currSeq
                            vc.cunitSeq = self.cunitSeq
                            vc.userResponseList = []
        //                    vc.trackStatus = 0
                            vc.studyDate = self.studyDate
                            vc.celldelegateViewController = self.delegateController
            //                self.delegateController?.present(vc, animated: true)
                            self.delegateController?.navigationController?.pushViewController(vc, animated: true)
                        }
                        
                    }
                } else {
                    //코칭에서 접속했지만 학습 완료 인 경우
                    if let status = self.studyStatusName, status == "학습 완료" {
                        
                        //코치가 접속한 경우 학습 완료이지만, 동영상이 있는 경우, 동영상 화면 보내주어야 함
                        if let videoc = self.videoCnt, videoc > 0   {
                            let vc1 = R.storyboard.study.studyVideoViewController()!
                            vc1.userSeq = self.userSeq
                            vc1.cunitSeq = self.cunitSeq
                            vc1.problemCnt = self.problemCnt
                            vc1.currSeq = self.currSeq
                            vc1.videoCnt = self.videoCnt
                            vc1.studyDate = self.self.studyDate
                            vc1.celldelegateViewController = self.delegateController
                            vc1.boolCompleteVideoProb = true
                            vc1.connectFromCoach = true
                            self.delegateController?.navigationController?.pushViewController(vc1, animated: true)
                        } else {
                            let vc = R.storyboard.study.studySolveResultViewController()!
                            LoadingView.show()
                            StudyAPI.shared.curriculumStudyResult(cunitSeq: self.cunitSeq!, currSeq: self.currSeq!, studyDate: self.studyDate!, userSeq: self.userSeq!).done {
                                res in
                                if let suc = res.success, suc == true {
                                    print("풀이 결과 리스펀스 성공함")
                                    guard let solveResult = res.data else { return }
                                    let thisWeekStudy = solveResult.thisWeekStudy!
                                    let lastWeekStudy = solveResult.lastWeekStudy!
                                    let nowStudy = solveResult.nowStudy!
                                    
                                    let vc = R.storyboard.study.studySolveResultViewController()!
                                    vc.userSeq = self.userSeq!
                                    vc.thisWeekStudy = thisWeekStudy
                                    vc.lastWeekStudy = lastWeekStudy
                                    vc.nowStudy = nowStudy
                                    vc.currSeq = self.currSeq
                                    vc.cunitSeq = self.cunitSeq
                                    vc.connectFromCoach = true
                                    self.delegateController?.navigationController?.pushViewController(vc, animated: true)
                                }
                            }.catch { error in
                                
                            }.finally {
                                LoadingView.hide()
                            }
                        }
                    }
                }
            })
        
        
        }
    
        
        
    }
   
}




protocol CustomTableViewDelegate : AnyObject {
    func didTouchCell(_ secDate: String, _ rowStatus: String,
                      _ ListStudyDaysExpand : OrderedDictionary<String, OrderedDictionary<String, Bool>>,
                      _ indexPath: IndexPath?)
}
