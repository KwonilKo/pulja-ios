//
//  StudyVideoTableViewCell.swift
//  pulja
//
//  Created by 고권일 on 2022/02/04.
//

import UIKit

class StudyVideoTableViewCell: UITableViewCell {

    @IBOutlet var lbTitle: UILabel!
    
    @IBOutlet var lbVideoTIme: UILabel!

    
    @IBOutlet weak var progressView: UIProgressView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static func nib() -> UINib {
            return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    var video : StudyVideo! {
            didSet {
                
                self.lbTitle.text = video.title
                
                
                if let totalDuration = video.videoDuration, let maxDuration = video.maxDuration {
                    let q = Int(totalDuration/60)
                               
                    let r = totalDuration - q * 60
                    self.lbVideoTIme.text = "\(q)분 \(r)초"
                    
                    let p : Float = Float(maxDuration)/Float(totalDuration)
                    self.progressView.setProgress(p, animated: true)

                } else {
                    self.lbVideoTIme.text = ""
                    self.progressView.setProgress(0, animated: true)
                }
                

            }
        }
    var weakcat4video : WeakCategoryVideo! {
            didSet {
                
                self.lbTitle.text = weakcat4video.title
                
                
                if let totalDuration = weakcat4video.videoDuration, let maxDuration = weakcat4video.maxDuration {
                    let q = Int(totalDuration/60)
                               
                    let r = totalDuration - q * 60
                    self.lbVideoTIme.text = "\(q)분 \(r)초"
                    
                    let p : Float = Float(maxDuration)/Float(totalDuration)
                    self.progressView.setProgress(p, animated: true)

                } else {
                    self.lbVideoTIme.text = ""
                    self.progressView.setProgress(0, animated: true)
                }
                

            }
        }
    
        
    
}
