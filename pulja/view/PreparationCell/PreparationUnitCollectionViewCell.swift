//
//  preparationUnitCollectionReusableView.swift
//  pulja
//
//  Created by kwonilko on 2022/07/28.
//

import UIKit

class PreparationUnitCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var unitView: UIView!
    
    @IBOutlet weak var lbUnit2: UILabel!
    
    
    @IBOutlet weak var lbUnit1: UILabel!
    var recInfo: Unit2Recommends? {
        didSet {
            lbUnit2.text = recInfo!.unit2Sname
            lbUnit1.text = processUnit1(unit1: (recInfo?.unit1Name!)!)
        }
    }
    
    var isSelectedBool : Bool = false {
        didSet {
            if self.isSelectedBool {

            } else {
                self.unitView.borderWidth = 1
                self.unitView.cornerRadius = 8
                self.unitView.borderColor = UIColor(red: 0.8, green: 0.808, blue: 0.815, alpha: 0.4)
            }
        }
    }
    var selectedGradeNumber : Int = 1
    var selectedUnitIdx : Int = 0
    
    var identityGradeNumber : Int = -1
    var identityUnitIdx : Int = -1


    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.isSelectedBool = false
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.isSelectedBool = false
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.selectUnit(_:)))
        self.unitView.addGestureRecognizer(gesture)
        
    }
    
    @objc func selectUnit(_ sender:UIGestureRecognizer) {
        isSelectedBool.toggle()

    }
    
    static func nib() -> UINib {
       return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
   }
    
    
    var unitInfo : UnitInfo! {
        
        didSet {
            lbUnit2.text = unitInfo.unit2SName
            lbUnit1.text = processUnit1(unit1: unitInfo.unit1Name!)
//            lbUnit1.text = "\(unitInfo.unit1Name!)"
//            lbUnit1.text = "\(unitInfo.unit1Seq!). \(unitInfo.unit1SName!)"
        }
    }
    
    
    func processUnit1(unit1: String) -> String {
        let replaced = unit1.map {
            $0.isNumber == true ? String("\($0).") : String($0) }.joined(separator: "")
        return replaced
    }

}
