//
//  PreparationTodayShareView.swift
//  pulja
//
//  Created by kwonilko on 2022/09/23.
//

import UIKit

class PreparationTodayShareView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBOutlet weak var sviewWidth: NSLayoutConstraint!
    
    @IBOutlet weak var sviewBig: UIView!
    var sview : String?
    
    @IBOutlet weak var sViewSubLb: UILabel!
    var sviewsublb = ""
    
    @IBOutlet weak var shareViewMainLb1: UILabel!
    var shareviewmainlb1 = ""
    
    
    @IBOutlet weak var shareViewMainLb2: UILabel!
    var shareviewmainlb2 = ""
    
    @IBOutlet weak var shareViewImg: UIImageView!
    var shareviewimg = ""
    
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        loadView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadView()
    }
    
    
    private func loadView() {
        
        let view = Bundle.main.loadNibNamed("PreparationTodayShareView",
                                       owner: self,
                                       options: nil)?.first as! UIView
        view.frame = bounds
        addSubview(view)
        
//        self.sViewSubLb.text = sviewsublb
//        self.shareViewMainLb1.text = shareviewmainlb1
//        self.shareViewMainLb2.text = shareviewmainlb2
//        self.shareViewImg.image = UIImage(named : shareviewimg)
        
        
        
    }
    
    func setUp() {
        self.sViewSubLb.text = sviewsublb
//        self.sviewBig.layoutIfNeeded()
//        self.sViewSubLb.layoutIfNeeded()
        self.shareViewMainLb1.text = shareviewmainlb1
        self.shareViewMainLb2.text = shareviewmainlb2
        self.shareViewImg.image = UIImage(named : shareviewimg)
        
        self.sviewBig.layoutIfNeeded()
        self.sViewSubLb.layoutIfNeeded()
        
    }
    
    
    
    
    
    
    
    

}
