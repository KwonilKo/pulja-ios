//
//  PreparationThisWeekView.swift
//  pulja
//
//  Created by kwonilko on 2022/09/19.
//

import UIKit

class PreparationThisWeekView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBOutlet weak var sLeadingConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var tStackView: UIStackView!
    @IBOutlet weak var sTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var mStackView: UIStackView!
    
    @IBOutlet weak var monBadgeImg: UIImageView!
    
    
    @IBOutlet weak var tueBadgeImg: UIImageView!
    
    
    @IBOutlet weak var wedBadgeImg: UIImageView!
    
    
    @IBOutlet weak var thurBadgeImg: UIImageView!
    
    
    
    @IBOutlet weak var friBadgeImg: UIImageView!
    
    
    @IBOutlet weak var satBadgeImg: UIImageView!
    
    
    @IBOutlet weak var sunBadgeImg: UIImageView!
    
    
    @IBOutlet weak var monProbView: UIView!
    
    @IBOutlet weak var monProbLb: UILabel!
    
    
    @IBOutlet weak var tueProbView: UIView!
    
    @IBOutlet weak var tueProbLb: UILabel!
    
    
    @IBOutlet weak var wedProbView: UIView!
    
    @IBOutlet weak var wedProbLb: UILabel!
    
    
    @IBOutlet weak var thurProbLb: UILabel!
    
    @IBOutlet weak var thurProbView: UIView!
    
    
    @IBOutlet weak var friProbLb: UILabel!
    
    @IBOutlet weak var friProbView: UIView!
    
    
    @IBOutlet weak var satProbLb: UILabel!
    @IBOutlet weak var satProbView: UIView!
    
    
    @IBOutlet var badgeImage: [UIImageView]!
    
    
    
    @IBOutlet var probView: [UIView]!
    
    
   
    @IBOutlet var probLb: [UILabel]!
    
    @IBOutlet weak var sunProbLb: UIView!
    
    
    @IBOutlet weak var sunProbView: UIView!
    
    
    @IBOutlet weak var firstViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var secondViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var thirdViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var fifthViewHeight: NSLayoutConstraint!
    @IBOutlet weak var forthViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var sixthViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var seventhViewHeight: NSLayoutConstraint!
    
    
    
    var thisWeekStudyCntArrayChange : [Int] = [0,0,0,0,0,0,0]
    var thisWeekStudyCntArray : [Int] = []
    {
        didSet {
            if thisWeekStudyCntArray != []
            {
                processToday()
            }
        }
    }
    let referenceDay : [String : Int] = ["월요일" : 0, "화요일" : 1, "수요일" : 2, "목요일" : 3, "금요일" : 4, "토요일" : 5, "일요일" : 6]
    

    var heightConstraints : [NSLayoutConstraint] = []
    let refHeight : CGFloat = 124.0
    
    
    func processToday() {
        
        //테블릿인지 아닌지 확인
        switch UIDevice.current.userInterfaceIdiom {
            case .pad:
            self.makeipad()
            default:
                break
        }
        
        //오늘 날짜 확인하기
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHHmmss" // 24 시간 대 설정
        dateFormatter.locale = Locale(identifier: "ko_kr") // 한국 시간 지정
        dateFormatter.timeZone = TimeZone(abbreviation: "KST") // 한국 시간대 지정
        let dateString = dateFormatter.string(from: now)
        let year = dateString.substring(from: 0, to: 3)
        let month = dateString.substring(from: 4, to: 5)
        let day = dateString.substring(from: 6, to: 7)
        
        let todayWeekday = weekday(year: Int(year)!, month: Int(month)!, day: Int(day)!)
        let todayWeekdayIdx = referenceDay[todayWeekday!]
        
        var maxCnt = thisWeekStudyCntArray.max()!
        if maxCnt == 0 { maxCnt = 1 }
                
        //thisWeekStudyCntArray 변형한거 저장하고, 그려주기
        for (i, j) in thisWeekStudyCntArray.enumerated() {
            thisWeekStudyCntArrayChange[i] = (i <= todayWeekdayIdx!) ? j : -1
            let temp = thisWeekStudyCntArrayChange[i]
            
            //변형한 형태로 뱃지, 그래프 그려주기
            if temp > 0 {
                badgeImage[i].image = UIImage(named: "badge36")
                
                heightConstraints[i].constant = refHeight * Double(temp) / Double(maxCnt)
                probLb[i].text = "\(String(temp))"
                
                //왼쪽, 오른쪽 상단 모서리만 둥글게 만들기
                probView[i].clipsToBounds = false
                probView[i].layer.cornerRadius = 5
                probView[i].layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
                
            } else {
                if temp == 0 {
                    badgeImage[i].image = UIImage(named: "badge36Grey")
                    
                } else {
                    badgeImage[i].image = UIImage(named: "badge36Empty")
                }
                
                probView[i].isHidden = true
                probLb[i].isHidden = true
            }
        }
        
        
        
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
            loadView()
        }
        
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadView()
    }
    
    
    private func loadView() {
        
        
        let view = Bundle.main.loadNibNamed("PreparationThisWeekView",
                                       owner: self,
                                       options: nil)?.first as! UIView
        view.frame = bounds
        addSubview(view)
        
        self.heightConstraints = [firstViewHeight, secondViewHeight,thirdViewHeight,forthViewHeight,fifthViewHeight,sixthViewHeight,seventhViewHeight]
        
//        avgRoundView.layer.borderWidth = 1.0
//        avgRoundView.layer.masksToBounds = false
//        avgRoundView.layer.borderColor = UIColor.white.cgColor
//        avgRoundView.layer.cornerRadius = avgRoundView.frame.size.width / 2
//        avgRoundView.clipsToBounds = true
        
        
        
    }
    
    
    
    /// 특정 연도, 월, 일에 대한 요일을 구하는 메소드
    ///
    /// - Parameter year: 연도
    /// - Parameter month: 월
    /// - Parameter day: 일
    ///
    /// - Returns: 요일에 해당하는 문자열 Ex) Sun, Sunday, S
    /// - note: day에 대한 안정성을 보장하지 않음
    ///         Ex) 2019-02-40을 넣으면 2월1일로부터 40일이 되는날(3월12일)의 요일이 반환됨
    func weekday(year: Int, month: Int, day: Int) -> String? {
        let calendar = Calendar(identifier: .gregorian)
        
        guard let targetDate: Date = {
            let comps = DateComponents(calendar:calendar, year: year, month: month, day: day)
            return comps.date
            }() else { return nil }
        
        let day = Calendar.current.component(.weekday, from: targetDate) - 1
        let detailedDay:String? = Calendar.current.weekdaySymbols[day]

        print("\(month)월 \(day)일 \(detailedDay!)")
        return "\(detailedDay!)"

    
    }
    
    
    func makeipad() {
//        sLeadingConstraint.constant =
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
        {
            let s = CGFloat((self.frame.size.width - 32.0  - 25.0 * 7)/6.0)
            self.mStackView.spacing = s
            
            let p = CGFloat((self.frame.size.width - 32.0  - 36.0 * 7)/6.0)
            self.tStackView.spacing = p
        }
    
    }
    
    
    
    
    

}
