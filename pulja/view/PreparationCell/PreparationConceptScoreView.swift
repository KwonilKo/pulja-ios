//
//  PreparationConceptScoreView.swift
//  pulja
//
//  Created by kwonilko on 2022/07/30.
//

import UIKit
import PromiseKit

class PreparationConceptScoreView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBOutlet weak var avgRoundViewLeadingSpace: NSLayoutConstraint!
    
    @IBOutlet weak var myScoreWidth: NSLayoutConstraint!
    
    @IBOutlet weak var roundViewLeadingConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var lbTime: UILabel!
    
    @IBOutlet weak var lbConceptScore: UILabel!
    
    
    @IBOutlet weak var lbConceptScoreExplain: UILabel!
    
    
    @IBOutlet weak var lbPercent: UIView!
    

    @IBOutlet weak var lbPercenText: UILabel!
    
    
    var conceptQuotientDto : ConceptQuotientDto?
    
    
    var flag : Bool = true {
        didSet {
            if flag { //자습 - 오늘의 공부 탭
                myScoreImage.isHidden = false
                totalProgressView.isHidden = false
                startScore.isHidden = false
                peerScore.isHidden = false
                endScore.isHidden = false
                avgRoundView.isHidden = false
                avgSquareView.isHidden = false
                
                if isWeakCat4Finished == 0 {
                    // 개념지수 아예 없는 경우 (처음 생성한 경우)
                    lbTime.isHidden = true
                    lbConceptScore.text = "? 점"
                    lbConceptScore.textColor = UIColor.Foggy
                    lbConceptScoreExplain.text = "아직 나의 개념지수를 알 수 없어요."
                    lbConceptScoreExplain.textColor = UIColor.Dust
                    myScoreProgressView.isHidden = true
                    peerScore.isHidden = true
                    avgRoundView.isHidden = true
                    avgSquareView.isHidden = true
                    lbPercent.isHidden = true
                    myScoreImage.isHidden = true
                    startScore.textColor = UIColor.Foggy
                    endScore.textColor = UIColor.Foggy
                    
                } else  {
                    //개념지수가 기존에 있지만 업데이트 되는 경우
                    lbTime.isHidden = false
//                    lbConceptScore.text = "59 점"
                    lbConceptScore.textColor = UIColor.puljaBlack
//                    lbConceptScoreExplain.text = "직전 대비 12점 내려갔어요 😞"
                    lbConceptScoreExplain.textColor = UIColor.puljaBlack
                    myScoreProgressView.isHidden = false
                    peerScore.isHidden = false
                    avgRoundView.isHidden = false
                    avgSquareView.isHidden = false
                    lbPercent.isHidden = false
                    myScoreImage.isHidden = false 
                    startScore.textColor = UIColor.grey
                    endScore.textColor = UIColor.grey
                    
                    fillConceptSectionWithProgressView()
                }
                
            } else { // 진단 후 리포트, 내 리포트 탭
                myScoreImage.isHidden = true
                totalProgressView.isHidden = true
                startScore.isHidden = true
                peerScore.isHidden = true
                endScore.isHidden = true
                avgRoundView.isHidden = true
                avgSquareView.isHidden = true
                
                fillConceptSectionWithoutProgressView()
            }
        }
    }
    
    @IBOutlet weak var myScoreProgressView: UIView!
    @IBOutlet weak var myScoreImage: UIImageView!
    
    @IBOutlet weak var avgRoundView: UIView!
    
    @IBOutlet weak var totalProgressView: UIView!
    
    @IBOutlet weak var avgSquareView: UIView!
    
    @IBOutlet weak var startScore: UILabel!
    
    
    @IBOutlet weak var peerScore: UILabel!
    
    
    @IBOutlet weak var endScore: UILabel!
    
    
    var isWeakCat4Finished : Int = 0
    
    
    override init(frame: CGRect) {
            super.init(frame: frame)
            loadView()
        }
        
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadView()
    }
    
    
    func fillCommonConceptSection() {
        
        guard let cQuotientDto = self.conceptQuotientDto else { return }
        let mdate = (cQuotientDto.moddate ?? "")
        let tConceptQuotient = (cQuotientDto.totalConceptQuotient ?? 0)
        let percent = (cQuotientDto.percent ?? 0)
        
        lbTime.text = "\(mdate)"
        lbConceptScore.text = "\(tConceptQuotient)점"
        lbPercenText.text = "상위 \(percent)%"
        
        
        if cQuotientDto.conceptQuotientDiff == nil { //처음 진단 본 후, conceptQuotientDiff 가 nil 인 경우
            lbConceptScoreExplain.text = "축하해요! 첫 개념지수를 받았어요 🎉"
        } else {
            guard let diff = cQuotientDto.conceptQuotientDiff else { return }
            if diff == 0 {
                lbConceptScoreExplain.text = "직전 점수와 동일해요 😶"
            } else if diff == 9999 {
                lbConceptScoreExplain.text = "📣 AI 업데이트로 개념지수에 변동이 있을 수 있어요."
            } else if diff < 0 {
                lbConceptScoreExplain.text = "직전 대비 \(abs(diff))점 내려갔어요 😞"
            } else {
                lbConceptScoreExplain.text = "직전 대비 \(abs(diff))점 올랐어요 👍"
            }
        }
    }
    
    func fillConceptSectionWithoutProgressView() {
        
        fillCommonConceptSection()
    
    }
    
    func fillConceptSectionWithProgressView() {
        
        fillCommonConceptSection()
        
        guard let cQuotientDto = self.conceptQuotientDto else { return }
        //내 점수 이미지, 프로그레스 뷰 영역
        let ratio = Double(cQuotientDto.totalConceptQuotient!) / Double(100)
        myScoreWidth.constant = totalProgressView.frame.size.width * ratio
        
        //평균, 가운데 영역
        let peerratio = Double(cQuotientDto.peerAverage!) / Double(100)
        avgRoundViewLeadingSpace.constant = totalProgressView.frame.size.width * peerratio
    }
    
    
    private func loadView() {
        
        
        let view = Bundle.main.loadNibNamed("PreparationConceptScoreView",
                                       owner: self,
                                       options: nil)?.first as! UIView
        view.frame = bounds
        addSubview(view)
        
        avgRoundView.layer.borderWidth = 1.0
        avgRoundView.layer.masksToBounds = false
        avgRoundView.layer.borderColor = UIColor.white.cgColor
        avgRoundView.layer.cornerRadius = avgRoundView.frame.size.width / 2
        avgRoundView.clipsToBounds = true
    }
    
    

}


