//
//  PreparationWeakCat4View.swift
//  pulja
//
//  Created by kwonilko on 2022/07/30.
//

import UIKit

class PreparationWeakCat4View: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBOutlet weak var firstLb: UILabel!
    
    @IBOutlet weak var secondLb: UILabel!
    
    @IBOutlet weak var problemCnt: UILabel!
    
    @IBOutlet weak var videoCnt: UILabel!
    
    
    @IBOutlet weak var weakCat41: UILabel!
    
    @IBOutlet weak var weakCat43: UILabel!
    @IBOutlet weak var weakCat42: UILabel!
    
    @IBOutlet weak var completeBadgeView: UIView!
    var weakCategory : WeakCategory?
    var problemSeq = 0
    var pCnt = 0
    var vCnt = 0
    var isAIRecommend = true
    var delegateController : UIViewController?
    
    
    @IBOutlet weak var problemVideoView: UIView!
    var cameFromOnboard = false
    
    var isWeakCat4Finished = 2 { // 1: 미학습 취약 유형 상태값 보여줌, 2: 학습완료된 취약 유형 상태값 보여줌
        didSet {
            if isWeakCat4Finished == 1 {
                completeBadgeView.isHidden = true
                problemVideoView.isHidden = false
            } else {
                completeBadgeView.isHidden = false
                problemVideoView.isHidden = true
            }
            fillWeakCat4View()
        }
    }
    
    func processUnit1(unit1: String) -> String {
        let replaced = unit1.map {
            $0.isNumber == true ? String("\($0).") : String($0) }.joined(separator: "")
        return replaced
    }
    
    func processCat4(weakCat4 : String) -> String {
        if var i = weakCat4.firstIndex(of: " ") {
            i = weakCat4.index(i, offsetBy: 1)
            return weakCat4.substring(from: i)
        }
        return ""
    }
    
    func fillWeakCat4View() {
        guard let pSet = self.weakCategory else { return }
        
        
        //학습 완료가 아닌 경우, 문제/비디오 수 보여주어야 함
        if self.isWeakCat4Finished != 0
        {
            if self.isWeakCat4Finished == 1 {
                self.problemCnt.text = String(pSet.problemCount!)
                self.videoCnt.text = String(pSet.videoCount!)
            }
            
            var temp = ""
            if pSet.subject! == "수학(상)" || pSet.subject! == "수학(하)" {
                temp = "고등"
            }
            temp += pSet.subject!
            firstLb.text = "\(temp) | \(processUnit1(unit1: pSet.unit1Sname!))"
            secondLb.text = "\(pSet.unit2Sname!)"
            
            if pSet.weakCategories!.count == 0 {
                weakCat41.text = ""
                weakCat42.text = ""
                weakCat43.text = ""
            } else if pSet.weakCategories!.count == 1 {
                weakCat41.text = "\(processCat4(weakCat4: pSet.weakCategories![0].category4Name!))"
                weakCat42.text = ""
                weakCat43.text = ""
            } else if pSet.weakCategories!.count == 2 {
                weakCat41.text = "\(processCat4(weakCat4: pSet.weakCategories![0].category4Name!))"
                weakCat42.text = "\(processCat4(weakCat4: pSet.weakCategories![1].category4Name!))"
                weakCat43.text = ""
            } else {
                weakCat41.text = "\(processCat4(weakCat4: pSet.weakCategories![0].category4Name!))"
                weakCat42.text = "\(processCat4(weakCat4: pSet.weakCategories![1].category4Name!))"
                weakCat43.text = "\(processCat4(weakCat4: pSet.weakCategories![2].category4Name!))"
            }
        }
    
        
        
    }
    override init(frame: CGRect) {
            super.init(frame: frame)
            loadView()
        }
        
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadView()
    }
    
    private func loadView() {
        let view = Bundle.main.loadNibNamed("PreparationWeakCat4View",
                                       owner: self,
                                       options: nil)?.first as! UIView
        view.frame = bounds
        addSubview(view)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.selectWeakCat4View(_:)))
        view.addGestureRecognizer(gesture)
    }
    
    @objc func selectWeakCat4View(_ sender:UIGestureRecognizer) {
        
    }
}
