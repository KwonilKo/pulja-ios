//
//  PuljaToolTipView.swift
//  pulja
//
//  Created by samdols on 2022/07/30.
//

import UIKit

class PuljaToolTipView: NibView {

    var callback : (_ view : PuljaToolTipView ) -> Void = {_ in }
    @IBOutlet weak var lbDesc: UILabel!
    

    @IBAction func btnTouchPressed(_ sender: Any) {
        self.callback(self)
    }
}
