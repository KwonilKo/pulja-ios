//
//  PuljaShareToolTipView.swift
//  pulja
//
//  Created by kwonilko on 2022/09/24.
//

import UIKit

class PuljaShareToolTipView: NibView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var callback : (_ view: PuljaShareToolTipView) -> Void = {_ in}
    
    
    @IBOutlet weak var lbDesc: UILabel!
    
    
    @IBAction func btnTouchPressed(_ sender: Any) {
        
        
        self.callback(self)
    }
    
    
}
