//
//  UITextField.swift
//  pulja
//
//  Created by 김병헌 on 2021/12/14.
//

import Foundation
import UIKit
import AnyFormatKit

extension UITextField {
    
    func addLeftImage(image:UIImage, left: CGFloat? = nil) {
        let leftimage = UIImageView(frame: CGRect(x: left ?? 0, y: 0, width: image.size.width, height: image.size.height))
                leftimage.image = image
                self.leftView = leftimage
                self.leftViewMode = .always
            }
    
    func setPadding(left: CGFloat? = nil, right: CGFloat? = nil){
        if let left = left {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: left, height: self.frame.size.height))
            self.leftView = paddingView
            self.leftViewMode = .always
            
        }
        
        if let right = right {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: right, height: self.frame.size.height))
            self.rightView = paddingView
            self.rightViewMode = .always
            
        }
        
    }
    
    func disableAutoFill() {
           if #available(iOS 12, *) {
               textContentType = .oneTimeCode
           } else {
               textContentType = .init(rawValue: "")
           }
       }
    
    
    
    // Runtime key
    private struct AssociatedKeys {
        // Maximum length key
        static var maxlength: UInt8 = 0
        // Temporary string key
        static var tempString: UInt8 = 0
    }

    // Limit the maximum input length of the textfiled
    @IBInspectable var maxLength: Int {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.maxlength) as? Int ?? 0
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.maxlength, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            addTarget(self, action: #selector(handleEditingChanged(textField:)), for: .editingChanged)
        }
    }

    // Temporary string
    private var tempString: String? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.tempString) as? String
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.tempString, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    // When the text changes, process the amount of text in the input
    // box so that its length is within the controllable range.
    @objc private func handleEditingChanged(textField: UITextField) {

        // Special processing for the Chinese input method
        guard markedTextRange == nil else { return }

        if textField.text?.count == maxLength {

            // Set lastQualifiedString where text length == maximum length
            tempString = textField.text
        } else if textField.text?.count ?? 0 < maxLength {

            // Clear lastQualifiedString when text length > maxlength
            tempString = nil
        }

        // Keep the current text range in arcgives
        let archivesEditRange: UITextRange?

        if textField.text?.count ?? 0 > maxLength {

            // If text length > maximum length, remove last range and to move to -1 postion.
            let position = textField.position(from: safeTextPosition(selectedTextRange?.start), offset: -1) ?? textField.endOfDocument
            archivesEditRange = textField.textRange(from: safeTextPosition(position), to: safeTextPosition(position))
        } else {

            // Just set current select text range
            archivesEditRange = selectedTextRange
        }

        // Main handle string maximum length
        textField.text = tempString ?? String((textField.text ?? "").prefix(maxLength))

        // Last configuration edit text range
        textField.selectedTextRange = archivesEditRange
    }

    // Get safe textPosition
    private func safeTextPosition(_ optionlTextPosition: UITextPosition?) -> UITextPosition {

        /* beginningOfDocument -> The end of the the text document. */
        return optionlTextPosition ?? endOfDocument
    }
    
    func addDoneButtonOnKeyboard() {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        doneToolbar.barTintColor = .puljaPaleGrey
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        // MARK: 버튼의 이름과 색상을 바꾸고 싶다면 아래 title과 tintColor를 바꿔보세요!
        let done: UIBarButtonItem = UIBarButtonItem(title: "완료", style: .done, target: self, action: #selector(self.doneButtonAction))
        done.tintColor = .black
        // ==============================================================
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
        
    }
    
    // MARK: 완료 버튼을 눌렀을 때 실행돼야 할 내용을 아래에 구현해주시면 됩니다(현재는 키보드가 사라지도록 하는 메소드만 있는 상태입니다)
    @objc func doneButtonAction() {
        self.resignFirstResponder()
    }
    
    
    func formatPhoneNumber(range: NSRange, string: String) {
          guard let text = self.text else {
              return
          }
          let characterSet = CharacterSet(charactersIn: string)
          if CharacterSet.decimalDigits.isSuperset(of: characterSet) == false {
              return
          }

          let newLength = text.count + string.count - range.length
          let formatter: DefaultTextInputFormatter
          let onlyPhoneNumber = text.filter { $0.isNumber }

          let currentText: String
          if newLength < 13 {
              if text.count == 13, string.isEmpty { // crash 방지
                  formatter = DefaultTextInputFormatter(textPattern: "###-####-####")
              } else {
                  formatter = DefaultTextInputFormatter(textPattern: "###-###-####")
              }
          } else {
              formatter = DefaultTextInputFormatter(textPattern: "###-####-####")
          }

          currentText = formatter.format(onlyPhoneNumber) ?? ""
          let result = formatter.formatInput(currentText: currentText, range: range, replacementString: string)
          if text.count == 13, string.isEmpty {
              self.text = DefaultTextInputFormatter(textPattern: "###-###-####").format(result.formattedText.filter { $0.isNumber })
          } else {
              self.text = result.formattedText
          }

        
          let position: UITextPosition
        
          if self.text?.substring(from: result.caretBeginOffset - 1, to: result.caretBeginOffset - 1) == "-" {
              position = self.position(from: self.beginningOfDocument, offset: result.caretBeginOffset + 1)!
          } else {
              position = self.position(from: self.beginningOfDocument, offset: result.caretBeginOffset)!
          }

          self.selectedTextRange = self.textRange(from: position, to: position)
      }
    
}

extension UITextView {
    func addDoneButtonOnKeyboard() {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        // MARK: 버튼의 이름과 색상을 바꾸고 싶다면 아래 title과 tintColor를 바꿔보세요!
        let done: UIBarButtonItem = UIBarButtonItem(title: "완료", style: .done, target: self, action: #selector(self.doneButtonAction))
        done.tintColor = .systemBlue
        // ==============================================================
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction() {
        self.resignFirstResponder()
    }
    
    func adjustUITextViewHeight() {
           self.translatesAutoresizingMaskIntoConstraints = true
           self.sizeToFit()
           self.isScrollEnabled = false
       }
}


@IBDesignable
class DesignableUITextField: UITextField {
    
    // Provides left padding for images
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += leftPadding
        return textRect
    }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0
    
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    
    func updateView() {
        if let image = leftImage {
            leftViewMode = UITextField.ViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 14, height: 14))
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = color
            
            let iconContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 14))
               iconContainerView.addSubview(imageView)
            leftView = iconContainerView
//            leftView = imageView
        } else {
            leftViewMode = UITextField.ViewMode.never
            leftView = nil
        }
        
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: color])
    }
}
