//
//  LoadingView.swift
//  pulja
//
//  Created by 김병헌 on 2021/12/27.
//

import Foundation


import UIKit
import Lottie

class LoadingView {
    private static let sharedInstance = LoadingView()
    
    private var backgroundView: UIView?
//    private var popupView: UIImageView?
    private var loadingLabel: UILabel?
    private var animationView: LottieAnimationView?
    
    
    class func showSkeleton() {
//        guard let tempWindow = UIApplication.shared.keyWindow else { return }
//        print("tempWindow: \(tempWindow)")
//        let backgroundView = UIView(frame: CGRect.init(x: 0, y: 0, width: tempWindow.frame.maxX, height: tempWindow.frame.maxY))
//            
//            
//            
//    //        let popupView = UIImageView(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100))
//    //        popupView.animationImages = LoadingView.getAnimationImageArray()
//    //        popupView.animationDuration = 0.8
//    //        popupView.animationRepeatCount = 0
//            
//            let animationView = LottieAnimationView(name: "loading2")
//            animationView.frame = CGRect.init(x: 0, y: 0, width: tempWindow.frame.maxX/2, height: tempWindow.frame.maxY/2)
//            
//    //        animationView.center = self.view.center // 애니메이션뷰의 위치설정
//    //        animationView.contentMode = .scaleAspectFill // 애니메이션뷰의 콘텐트모드 설정
//            
//            
//            
//            let loadingLabel = UILabel(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100))
//            loadingLabel.text = "Loading ..."
//            loadingLabel.font = UIFont.boldSystemFont(ofSize: 20)
//            loadingLabel.textColor = .black
//            loadingLabel.isHidden = true
//            
//            if let window = UIApplication.shared.keyWindow {
//                window.addSubview(backgroundView)
//    //            window.addSubview(popupView)
//                window.addSubview(animationView)
//                window.addSubview(loadingLabel)
//                
//                backgroundView.frame = CGRect(x: 0, y: 0, width: window.frame.maxX, height: window.frame.maxY)
//                backgroundView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.0)
//                
//    //            popupView.center = window.center
//    //            popupView.startAnimating()
//                
//                animationView.center = window.center
//                animationView.contentMode = .scaleAspectFill
//                animationView.loopMode = .loop
//                animationView.play()
//                
//    //            loadingLabel.layer.position = CGPoint(x: window.frame.midX, y: popupView.frame.maxY + 10)
//                loadingLabel.layer.position = CGPoint(x: window.frame.midX, y: animationView.frame.maxY + 10)
//                
//                sharedInstance.backgroundView?.removeFromSuperview()
//    //            sharedInstance.popupView?.removeFromSuperview()
//                sharedInstance.animationView?.removeFromSuperview()
//                sharedInstance.loadingLabel?.removeFromSuperview()
//                sharedInstance.backgroundView = backgroundView
//    //            sharedInstance.popupView = popupView
//                sharedInstance.animationView = animationView
//                sharedInstance.loadingLabel = loadingLabel
//            }
        }
    
    class func diagshow1(message:String) {
        let backgroundView = UIView(frame: CGRect.init(x: 0, y: 0, width: 180, height: 120))
        
//        let popupView = UIImageView(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100))
//        popupView.animationImages = LoadingView.getAnimationImageArray()
//        popupView.animationDuration = 0.8
//        popupView.animationRepeatCount = 0
        
        let animationView = LottieAnimationView(name: "diagloding1")
        animationView.frame = CGRect.init(x: 0, y: 0, width: 180, height: 120)
        
//        animationView.center = self.view.center // 애니메이션뷰의 위치설정
//        animationView.contentMode = .scaleAspectFill // 애니메이션뷰의 콘텐트모드 설정
        
        
        
        let loadingLabel = UILabel(frame: CGRect.init(x: 0, y: 0, width: 150, height: 120))
        loadingLabel.text = message
        loadingLabel.font = UIFont.boldSystemFont(ofSize: 14)
        loadingLabel.textColor = .black
        loadingLabel.isHidden = false
        
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(backgroundView)
//            window.addSubview(popupView)
            window.addSubview(animationView)
            window.addSubview(loadingLabel)
            
            backgroundView.frame = CGRect(x: 0, y: 0, width: window.frame.maxX, height: window.frame.maxY)
            backgroundView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.0)
            
//            popupView.center = window.center
//            popupView.startAnimating()
            
            animationView.center = window.center
            animationView.contentMode = .scaleAspectFill
            animationView.loopMode = .loop
            animationView.play()
            
//            loadingLabel.layer.position = CGPoint(x: window.frame.midX, y: popupView.frame.maxY + 10)
            loadingLabel.layer.position = CGPoint(x: window.frame.midX, y: animationView.frame.maxY + 10)
            
            sharedInstance.backgroundView?.removeFromSuperview()
//            sharedInstance.popupView?.removeFromSuperview()
            sharedInstance.animationView?.removeFromSuperview()
            sharedInstance.loadingLabel?.removeFromSuperview()
            sharedInstance.backgroundView = backgroundView
//            sharedInstance.popupView = popupView
            sharedInstance.animationView = animationView
            sharedInstance.loadingLabel = loadingLabel
        }
    }
    
    class func diagshow2() {
        let backgroundView = UIView(frame: CGRect.init(x: 0, y: 0, width: 130, height: 100))
        
//        let popupView = UIImageView(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100))
//        popupView.animationImages = LoadingView.getAnimationImageArray()
//        popupView.animationDuration = 0.8
//        popupView.animationRepeatCount = 0
        
        let animationView = LottieAnimationView(name: "diagloding2")
        animationView.frame = CGRect.init(x: 0, y: 0, width: 130, height: 100)
        
//        animationView.center = self.view.center // 애니메이션뷰의 위치설정
//        animationView.contentMode = .scaleAspectFill // 애니메이션뷰의 콘텐트모드 설정
        
        
        
        let loadingLabel = UILabel(frame: CGRect.init(x: 0, y: 0, width: 220, height: 100))
//        let loadingLabel = UILabel()
        loadingLabel.text = "푸링이가 취약점을 분석하는 중이에요."
        loadingLabel.font = UIFont.boldSystemFont(ofSize: 14)
        loadingLabel.textColor = .black
        loadingLabel.isHidden = false
        
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(backgroundView)
//            window.addSubview(popupView)
            window.addSubview(animationView)
            window.addSubview(loadingLabel)
            
            backgroundView.frame = CGRect(x: 0, y: 0, width: window.frame.maxX, height: window.frame.maxY)
            backgroundView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.0)
            
//            popupView.center = window.center
//            popupView.startAnimating()
            
            animationView.center = window.center
            animationView.contentMode = .scaleAspectFill
            animationView.loopMode = .loop
            animationView.play()
            
//            loadingLabel.layer.position = CGPoint(x: window.frame.midX, y: popupView.frame.maxY + 10)
//            loadingLabel.center = window.center
//            loadingLabel.topAnchor.constraint(equalTo: animationView.bottomAnchor, constant: 50).isActive = true
            loadingLabel.layer.position = CGPoint(x: window.frame.midX, y: animationView.frame.maxY + 20)
            
            sharedInstance.backgroundView?.removeFromSuperview()
//            sharedInstance.popupView?.removeFromSuperview()
            sharedInstance.animationView?.removeFromSuperview()
            sharedInstance.loadingLabel?.removeFromSuperview()
            sharedInstance.backgroundView = backgroundView
//            sharedInstance.popupView = popupView
            sharedInstance.animationView = animationView
            sharedInstance.loadingLabel = loadingLabel
        }
    }
    
    class func diagshow3() {
        let backgroundView = UIView(frame: CGRect.init(x: 0, y: 0, width: 120, height: 120))
        
//        let popupView = UIImageView(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100))
//        popupView.animationImages = LoadingView.getAnimationImageArray()
//        popupView.animationDuration = 0.8
//        popupView.animationRepeatCount = 0
        
        let animationView = LottieAnimationView(name: "diagloding3")
        animationView.frame = CGRect.init(x: 0, y: 0, width: 120, height: 120)
        
//        animationView.center = self.view.center // 애니메이션뷰의 위치설정
//        animationView.contentMode = .scaleAspectFill // 애니메이션뷰의 콘텐트모드 설정
        
        
        
        let loadingLabel = UILabel(frame: CGRect.init(x: 0, y: 0, width: 185, height: 20))
//        let loadingLabel = UILabel()
        loadingLabel.text = "진단평가 리포트로 이동합니다!"
        loadingLabel.font = UIFont.boldSystemFont(ofSize: 14)
        loadingLabel.textColor = .black
        loadingLabel.isHidden = false
        
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(backgroundView)
//            window.addSubview(popupView)
            window.addSubview(animationView)
            window.addSubview(loadingLabel)
            
            backgroundView.frame = CGRect(x: 0, y: 0, width: window.frame.maxX, height: window.frame.maxY + 30)
            backgroundView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.0)
            
//            popupView.center = window.center
//            popupView.startAnimating()
            
            animationView.center = window.center
            animationView.contentMode = .scaleAspectFill
            animationView.loopMode = .loop
            animationView.play()
            
//            loadingLabel.layer.position = CGPoint(x: window.frame.midX, y: popupView.frame.maxY + 10)
            loadingLabel.layer.position = CGPoint(x: window.frame.midX, y: animationView.frame.maxY + 10)
            
            sharedInstance.backgroundView?.removeFromSuperview()
//            sharedInstance.popupView?.removeFromSuperview()
            sharedInstance.animationView?.removeFromSuperview()
            sharedInstance.loadingLabel?.removeFromSuperview()
            sharedInstance.backgroundView = backgroundView
//            sharedInstance.popupView = popupView
            sharedInstance.animationView = animationView
            sharedInstance.loadingLabel = loadingLabel
        }
    }
    
    
    class func showSolve() {
        let backgroundView = UIView(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100))
        
//        let popupView = UIImageView(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100))
//        popupView.animationImages = LoadingView.getAnimationImageArray()
//        popupView.animationDuration = 0.8
//        popupView.animationRepeatCount = 0
        
        let animationView = LottieAnimationView(name: "newloading")
        animationView.frame = CGRect.init(x: 0, y: 0, width: 64, height: 64)
        
//        animationView.center = self.view.center // 애니메이션뷰의 위치설정
//        animationView.contentMode = .scaleAspectFill // 애니메이션뷰의 콘텐트모드 설정
        
        
        
        let loadingLabel = UILabel(frame: CGRect.init(x: 0, y: 0, width: 64, height: 64))
        loadingLabel.text = "Loading ..."
        loadingLabel.font = UIFont.boldSystemFont(ofSize: 20)
        loadingLabel.textColor = .black
        loadingLabel.isHidden = true
        
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(backgroundView)
//            window.addSubview(popupView)
            window.addSubview(animationView)
            window.addSubview(loadingLabel)
            
            backgroundView.frame = CGRect(x: 0, y: 0, width: window.frame.maxX, height: window.frame.maxY)
            backgroundView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.0)
            
//            popupView.center = window.center
//            popupView.startAnimating()
            
            animationView.center = window.center
            animationView.contentMode = .scaleAspectFill
            animationView.loopMode = .loop
            animationView.play()
            
//            loadingLabel.layer.position = CGPoint(x: window.frame.midX, y: popupView.frame.maxY + 10)
            loadingLabel.layer.position = CGPoint(x: window.frame.midX, y: animationView.frame.maxY + 10)
            
            sharedInstance.backgroundView?.removeFromSuperview()
//            sharedInstance.popupView?.removeFromSuperview()
            sharedInstance.animationView?.removeFromSuperview()
            sharedInstance.loadingLabel?.removeFromSuperview()
            sharedInstance.backgroundView = backgroundView
//            sharedInstance.popupView = popupView
            sharedInstance.animationView = animationView
            sharedInstance.loadingLabel = loadingLabel
        }
        
    }
    
    
    class func show() {
        let backgroundView = UIView(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100))
        
        
        
//        let popupView = UIImageView(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100))
//        popupView.animationImages = LoadingView.getAnimationImageArray()
//        popupView.animationDuration = 0.8
//        popupView.animationRepeatCount = 0
        
        let animationView = LottieAnimationView(name: "lf20_3msg70cf")
        animationView.frame = CGRect.init(x: 0, y: 0, width: 100, height: 100)
        
//        animationView.center = self.view.center // 애니메이션뷰의 위치설정
//        animationView.contentMode = .scaleAspectFill // 애니메이션뷰의 콘텐트모드 설정
        
        
        
        let loadingLabel = UILabel(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100))
        loadingLabel.text = "Loading ..."
        loadingLabel.font = UIFont.boldSystemFont(ofSize: 20)
        loadingLabel.textColor = .black
        loadingLabel.isHidden = true
        
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(backgroundView)
//            window.addSubview(popupView)
            window.addSubview(animationView)
            window.addSubview(loadingLabel)
            
            backgroundView.frame = CGRect(x: 0, y: 0, width: window.frame.maxX, height: window.frame.maxY)
            backgroundView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.0)
            
//            popupView.center = window.center
//            popupView.startAnimating()
            
            animationView.center = window.center
            animationView.contentMode = .scaleAspectFill
            animationView.loopMode = .loop
            animationView.play()
            
//            loadingLabel.layer.position = CGPoint(x: window.frame.midX, y: popupView.frame.maxY + 10)
            loadingLabel.layer.position = CGPoint(x: window.frame.midX, y: animationView.frame.maxY + 10)
            
            sharedInstance.backgroundView?.removeFromSuperview()
//            sharedInstance.popupView?.removeFromSuperview()
            sharedInstance.animationView?.removeFromSuperview()
            sharedInstance.loadingLabel?.removeFromSuperview()
            sharedInstance.backgroundView = backgroundView
//            sharedInstance.popupView = popupView
            sharedInstance.animationView = animationView
            sharedInstance.loadingLabel = loadingLabel
        }
    }
    
    class func show(message : String) {
        let backgroundView = UIView(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100))

        
        let animationView = LottieAnimationView(name: "lf20_3msg70cf")
        animationView.frame = CGRect.init(x: 0, y: 0, width: 100, height: 100)
        
        let loadingLabel = UILabel(frame: CGRect.init(x: 0, y: 0, width: 375, height: 100))
        loadingLabel.text = message
        loadingLabel.font = UIFont.boldSystemFont(ofSize: 14)
        loadingLabel.textColor = .OffWhite
        loadingLabel.textAlignment = .center
        loadingLabel.isHidden = false
        
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(backgroundView)
            window.addSubview(animationView)
            window.addSubview(loadingLabel)
            
            backgroundView.frame = CGRect(x: 0, y: 0, width: window.frame.maxX, height: window.frame.maxY)
            backgroundView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.0)
            
            animationView.center = window.center
            animationView.contentMode = .scaleAspectFill
            animationView.loopMode = .loop
            animationView.play()
            
            loadingLabel.layer.position = CGPoint(x: window.frame.midX, y: animationView.frame.maxY + 10)
            
            sharedInstance.backgroundView?.removeFromSuperview()
            sharedInstance.animationView?.removeFromSuperview()
            sharedInstance.loadingLabel?.removeFromSuperview()
            sharedInstance.backgroundView = backgroundView
            sharedInstance.animationView = animationView
            sharedInstance.loadingLabel = loadingLabel
        }
    }
    
    class func hide() {
        if //let popupView = sharedInstance.popupView,
            let animationView = sharedInstance.animationView,
            let loadingLabel = sharedInstance.loadingLabel,
            let backgroundView = sharedInstance.backgroundView {
            
//            popupView.stopAnimating()
            animationView.stop()
            backgroundView.removeFromSuperview()
//            popupView.removeFromSuperview()
            animationView.removeFromSuperview()
            loadingLabel.removeFromSuperview()
            
        }
    }

    private class func getAnimationImageArray() -> [UIImage] {
        var animationArray: [UIImage] = []
        animationArray.append(UIImage(named: "img_default_profile_image_1")!)
        animationArray.append(UIImage(named: "img_default_profile_image_2")!)
        animationArray.append(UIImage(named: "img_default_profile_image_3")!)
        animationArray.append(UIImage(named: "img_default_profile_image_4")!)
        return animationArray
    }
}
