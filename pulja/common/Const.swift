//
//  Const.swift
//  pulja
//
//  Created by jeonghoonlee on 2021/11/04.
//

import Foundation
import UIKit
struct Const {
    
    enum Keys {
        static let COOKIE_DOMAIN = "COOKIE_DOMAIN"
        static let SERVER_TYPE = "SERVER_TYPE"
        static let IS_AIRBRIDGE = "IS_AIRBRIDGE"
        static let SCHEME = "SCHEME"
        static let HOST = "HOST"
        static let PORT = "PORT"
    }
    
    // MARK: - Plist
    private static let infoDictionary: [String: Any] = {
        guard let dict = Bundle.main.infoDictionary else {
            fatalError("Plist file not found")
        }
        return dict
    }()

    static let serverType: String = {
        let value = Const.infoDictionary[Keys.SERVER_TYPE]
        return value as? String ?? "product"
    }()
    
    static var isProd : Bool {
        if serverType == "product" {
            return true
        } else {
            return false
        }
    }
    
    static let COOKIE_DOMAIN : String = {
        let value = Const.infoDictionary[Keys.COOKIE_DOMAIN]
        return value as? String ?? ".pulja.co.kr"
    }()
    
    enum EnumServerType : String {
        case product = "product" //운영서버
        case staging = "staging" // lmsDev
        case dev = "dev" // 사내 개발
    }
    
    static let SCHEME: String = {
        let value = Const.infoDictionary[Keys.SCHEME]
        return value as? String ?? "https"
    }()
    
    static let HOST: String = {
        let value = Const.infoDictionary[Keys.HOST]
        return value as? String ?? "www.pulja.co.kr"
    }()
    
    static let PORT: String = {
        let value = Const.infoDictionary[Keys.PORT]
        return value as? String ?? ""
    }()

    static let DOMAIN: String = {
        if serverType == EnumServerType.dev.rawValue {
            return "\(SCHEME)://\(HOST):\(PORT)"
        } else {
            return "\(SCHEME)://\(HOST)"
        }
    }()
    

    static let API_PATH : String  = Const.DOMAIN +  "/m/v1/"
    
    static let isAirBridge: Bool = {
        guard let isAirBridge = Const.infoDictionary[Keys.IS_AIRBRIDGE] as? String else {
            return false
        }
        if isAirBridge == "YES" {
            return true
        } else {
            return false
        }
    }()

    static let MARKET_URL = "https://apps.apple.com/us/app/%ED%92%80%EC%9E%90-%EA%B0%9C%EB%85%90%EC%9B%90%EB%A6%AC-%EC%98%A8%EB%9D%BC%EC%9D%B8-%EC%84%9C%EB%B9%84%EC%8A%A4/id1589353333"
    static let CHANNELTALK_PLUGIN_KEY = "77e29843-e342-46b1-bc18-7c679893018b"
    static let FL_PROJECT_ID = "9fbe7631-2ff9-4044-aef8-4833584708a9"
    static let DEFAULT_COMPRESSION_QUALITY : CGFloat = 0.15
    static let OS : String = "I"
    static let marketUrl : String = ""
    static let AB_WEB_TOKEN = "46c991a866f44ce4873754957f86db80"
    static let AB_APP_TOKEN = "42afc412ac6c44078c7c9333844a22a3"
    static let AB_APP_NAME = "pulja"
    static let UD_CHAT_USER_IDS = "UD_CHAT_USER_IDS"
    static let UD_DIVICE_TOKEN = "UD_DIVICE_TOKEN"
    static let UD_CHAT_USER_JSON = "UD_CHAT_USER_JSON"
    static let UD_SERVER_TYPE = "UI_SERVER_TYPE"
    static let PROBLEM_IMAGE_URL = "https://pulja-contents.s3.ap-northeast-2.amazonaws.com/mobile_problem/"
    static let EXPLAIN_IMAGE_URL = "https://pulja-contents.s3.ap-northeast-2.amazonaws.com/mobile_explain/"
    static let FREE_TRIAL_URL = "https://www.pulja.co.kr/free_trial"
    static let SUBSCRIPTION_URL = "https://www.pulja.co.kr/subscription_landing"
    static let MAX_TOOL_TIP_WIDTH : CGFloat = 174.0
    enum ChatBotListType : String {
        case recent = "R"
        case next = "N"
        case previous = "P"
    }

    enum ChatBotMessageType : String {
        case studyTime = "study_time"
        case studyConfirm = "study_confirm"
        case studyPushNow = "study_push1"
        case studyPush1Hour = "study_push2"
        case studyPush3Hour = "study_push3"
    }

    enum UserType : String {
        case coach = "C"
        case student = "S"
    }

    enum PayType : String {
        case paid = "p" //유료 사용자
        case bought = "b" //구매이력이 있는 사용자. 현재는 무료
        case free = "f" //아무것도 하지 않은 무료 사용자
        case week = "w" //7일체험 신청한 사람
    }
}
