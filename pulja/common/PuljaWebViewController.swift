//
//  PuljaWebViewController.swift
//  pulja
//
//  Created by jeonghoonlee on 2022/03/10.
//

import UIKit
import WebKit
import AirBridge

class PuljaWebViewController: UIViewController, WKUIDelegate, WKNavigationDelegate {

    @IBOutlet weak var containerView: UIView!
    var webView: WKWebView!
    var urlString : String?
    
    override func loadView() {
        super.loadView()
        
        let contentController = WKUserContentController()
        AirBridge.webInterface()?.inject(to: contentController, withWebToken: Const.AB_WEB_TOKEN)
        
        let script =
            "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport');" +
                "meta.setAttribute('content', 'width=device-width, initial-scale = 1.0, user-scalable = no');" +
        "document.getElementsByTagName('head')[0].appendChild(meta);"
        let userScript = WKUserScript(source: script,
                                      injectionTime: WKUserScriptInjectionTime.atDocumentEnd,
                                      forMainFrameOnly: true)
        contentController.addUserScript(userScript)
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        
        let config = WKWebViewConfiguration()
        config.preferences = preferences
        config.userContentController = contentController
        
        self.webView = WKWebView(frame: .zero, configuration: config)
        self.webView?.frame = self.containerView.bounds
        self.webView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.containerView.addSubview(self.webView!)
        
        self.webView?.navigationDelegate = self
        self.webView?.uiDelegate = self
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        if let _url = self.urlString, let url = URL(string: _url)
        {
            let urlRequest = URLRequest(url: url)

            if let authCookie = authCookie, let refreshCookie = refreshCookie {
                self.webView?.configuration.websiteDataStore.httpCookieStore.setCookie(authCookie, completionHandler: {
                    self.webView?.configuration.websiteDataStore.httpCookieStore.setCookie(refreshCookie, completionHandler: {
                        self.webView?.load(urlRequest)
                    })
                })
            }
            
        }
        // Do any additional setup after loading the view.
    }
    
    private var authCookie: HTTPCookie? {
          let cookie = HTTPCookie(properties: [
           .domain: Const.COOKIE_DOMAIN,
              .path: "/",
              .name: "JWT-TOKEN",
              .value: CommonUtil.shared.getAccessToken(),
          ])
          return cookie
      }
          
  private var refreshCookie: HTTPCookie? {
      let cookie = HTTPCookie(properties: [
          .domain: Const.COOKIE_DOMAIN,
          .path: "/",
          .name: "REFRESH-TOKEN",
          .value: CommonUtil.shared.getRefreshToken(),
      ])
      return cookie
  }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnExitPressed(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
}

extension PuljaWebViewController: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        switch message.name {
            
        default:
            break
        }
    }
}
