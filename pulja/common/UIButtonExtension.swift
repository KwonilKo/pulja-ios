//
//  UIButtonExtension.swift
//  pulja
//
//  Created by 김병헌 on 2022/02/14.
//

import Foundation
import UIKit

extension UIButton {
    func setImageTintColor(_ color: UIColor) {
            let tintedImage = self.imageView?.image?.withRenderingMode(.alwaysTemplate)
            self.setImage(tintedImage, for: .normal)
            self.tintColor = color
        }
    
    func setBGColor(_ color: UIColor, for state: UIControl.State) {
           UIGraphicsBeginImageContext(CGSize(width: 1.0, height: 1.0))
           guard let context = UIGraphicsGetCurrentContext() else { return }
           context.setFillColor(color.cgColor)
           context.fill(CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0))

           let backgroundImage = UIGraphicsGetImageFromCurrentImageContext()
           UIGraphicsEndImageContext()

           setBackgroundImage(backgroundImage, for: state)
       }
    
    
}
