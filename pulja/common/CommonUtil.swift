//
//  CommonUtil.swift
//

import Foundation
import UIKit
import AVKit
import CoreTelephony
import Alamofire
import CoreData
import UserNotifications
import AirBridge

public class CommonUtil {
    
    static var shared = CommonUtil()
    
    private let strAccept = "*/*"
    private let strContetType = "application/json" //"application/x-www-form-urlencoded; charset=utf-8"
    private let strMultipartContetType = "multipart/form-data"
    private var headers:HTTPHeaders! = nil
    private var mheaders:HTTPHeaders! = nil
    
    static var tabbarController : TabbarController?
    static var coachTabbarController : CoachTabbarController?
    
    static var hasDiagnosis = false
    public static var userInfoRemote:NSDictionary? // = ["":""]
    static let statusDesc = [
        400 : "Bad Request",
        401 : "Token Expired",
        403 : "Forbidden",
        404 : "Not Found",
        405 : "Method Not Allowed",
        409 : "Conflict",
        500 : "Internal Server Error",
        503 : "Service Unavailable",
        998 : "Token Expired , refresh token not exist",
        999 : "Token Expired , access token fetch failed"
    ]
    
  
    public init() {}
    
    func setHeader() {
        if let token = getAccessToken() {
            print("=======================================")
            print(String(format:"=================token[%@]==============",token))
            print("=======================================")
            let authorization = "" + token
            self.headers = [
                "Authorization": authorization,
                "Accept": "*/*",
                "Content-Type":self.strContetType
            ]
            
            self.mheaders = [
                "Authorization": authorization,
                "Accept": self.strAccept,
                "Accept-Encoding":"gzip, deflate",
                "Content-Type":self.strMultipartContetType,
                "Connection":"Keep-Alive"
            ]
        } else {
            self.headers = [
                //"WWW-Authenticate" : "Basic realm=\"api\"",
                "Accept": self.strAccept,
                "Content-Type":self.strContetType,
                "Connection":"Keep-Alive"
            ]
            
            self.mheaders = [
                "Accept": self.strAccept,
                "Content-Type":self.strContetType,
                "Connection":"Keep-Alive"
            ]
        }

    }
        
    func setNoAuthHeader() {
        self.headers = [
            //"WWW-Authenticate" : "Basic realm=\"api\"",
            "Accept": self.strAccept,
            "Content-Type":self.strContetType,
            "Connection":"Keep-Alive"
        ]
        
        self.mheaders = [
            "Accept": self.strAccept,
            "Content-Type":self.strContetType,
            "Connection":"Keep-Alive"
        ]
    }
    
    func getNoAuthHeader() -> HTTPHeaders {
       let httpHeader : HTTPHeaders = [
           "Accept": self.strAccept,
           "Content-Type":self.strContetType,
           "Connection":"Keep-Alive"
       ]
       return httpHeader
   }
    
   func getHeaders()-> HTTPHeaders? {
       setHeader()
//       if self.headers == nil {
//           setHeader()
//       }
       
       return self.headers
   }
   
   func getMutipartHeaders()-> HTTPHeaders? {
       setHeader()
//       if self.mheaders == nil {
//          setHeader()
//       }
       return self.mheaders
   }
    
    func setDeviceToken(token:Data) {
        UserDefaults.standard.set(token, forKey:"DEVICE_TOKEN")
        UserDefaults.standard.synchronize()
    }
    
    func getDeviceToken() -> Data? {
        if  let token = UserDefaults.standard.data(forKey: "DEVICE_TOKEN") {
            return token
        } else {
            return nil
        }
    }
    
    
    
    func setAccessToken(token:String) {
        UserDefaults.standard.set(token, forKey:"ACCESS_TOKEN")
        UserDefaults.standard.synchronize()
    }
    
    func removeAccessToken() {
        UserDefaults.standard.removeObject(forKey: "ACCESS_TOKEN")
        UserDefaults.standard.synchronize()
    }
    
    func getAccessToken() -> String? {
        if  let token = UserDefaults.standard.string(forKey: "ACCESS_TOKEN") {
            return token
        } else {
            return nil
        }
    }
    
    
    func removeRefreshToken() {
        UserDefaults.standard.removeObject(forKey: "REFRESH_TOKEN")
        UserDefaults.standard.synchronize()
    }
    
    func setRefreshToken(token:String) {
        UserDefaults.standard.set(token, forKey:"REFRESH_TOKEN")
        UserDefaults.standard.synchronize()
    }
    
    func getRefreshToken() -> String? {
        if  let token = UserDefaults.standard.string(forKey: "REFRESH_TOKEN") {
            return token
        } else {
            return nil
        }
    }
    
    func getSchoolInfo() -> Dictionary<String,Int>? {
        if  let info = UserDefaults.standard.object(forKey:"SCHOOL_INFO") as? Dictionary<String,Int> {
            return info
        } else {
            return nil
        }
    }
    
    func setSchoolInfo(info:Dictionary<String,Int>) {
        UserDefaults.standard.set(info, forKey: "SCHOOL_INFO")
        UserDefaults.standard.synchronize()
    }
    
    
    
    func saveMyInfo(info : User) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(info) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: "userSelfInfo")
        }
    }
    
    func loadMyInfo() -> User? {
        let defaults = UserDefaults.standard
        if let saveInfo = defaults.object(forKey: "userSelfInfo") as? Data {
            
            let decoder = JSONDecoder()
            
            if let loadedInfo = try? decoder.decode(User.self, from: saveInfo) {
                return loadedInfo
            } else {
                return nil
            }
        }
        
        return nil
    }
       

    static func getCarrierName() -> String {
        let info: CTTelephonyNetworkInfo = CTTelephonyNetworkInfo()
        guard let carrier: CTCarrier = info.subscriberCellularProvider else {
          return ""
        }
        
        return carrier.carrierName ?? ""
    }
    

    static func getDeviceIdentifier() -> String {

        var systemInfo = utsname()

        uname(&systemInfo)

        let machineMirror = Mirror(reflecting: systemInfo.machine)

        let identifier = machineMirror.children.reduce("") { identifier, element in

            guard let value = element.value as? Int8, value != 0 else { return identifier }

            return identifier + String(UnicodeScalar(UInt8(value)))

        }

        return identifier

    }
    

    static func getAppVersion() -> String {
        
        guard let dictionary = Bundle.main.infoDictionary,
              let version = dictionary["CFBundleShortVersionString"] as? String,
              let build = dictionary["CFBundleVersion"] as? String else {return ".."}
        
        let versionAndBuild: String = "vserion: \(version), build: \(build)"
        return versionAndBuild

    }
    
    static func getAppName() -> String {
        guard let dictionary = Bundle.main.infoDictionary else {
            return ""
        }
        if let version : String = dictionary["CFBundleName"] as? String {
            return version
        } else {
            return ""
        }
    }
    
    func getCurrentTime() -> String {
        let date = Date()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHHmmss"
        
        let result = dateFormatter.string(from: date)
        return result
    }
    
    

    func setStudyTime(datetime:String) {
        
        //yyyyMMddHHmmss
        UserDefaults.standard.set(datetime, forKey:"PULJA_STUDY_DATETIME")
        
    }
    
    func getStudyTime() -> String? {
        if  let datetime = UserDefaults.standard.string(forKey: "PULJA_STUDY_DATETIME") {
            return datetime
        } else {
            return nil
        }
    }
    
    func removeStudyTime() {
        UserDefaults.standard.removeObject(forKey: "PULJA_STUDY_DATETIME")
        UserDefaults.standard.synchronize()
    }
    
    func setChatbotLastSeq(lastSeq:Int) {
        

        UserDefaults.standard.set(lastSeq, forKey:"PULJA_CHATBOT_LASTSEQ")
        
    }
    
    func getChatbotLastSeq() -> Int {
        let lastSeq = UserDefaults.standard.integer(forKey: "PULJA_CHATBOT_LASTSEQ")
        return lastSeq
    }
    
    
    func calculateMaxLines(width:CGFloat , font:UIFont, text:String) -> Int {
        let maxSize = CGSize(width: width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        let lines = Int(textSize.height/charSize)
        return lines
    }
    
    func caluateWidth(height:CGFloat , font:UIFont, text:String) -> CGFloat {
        let maxSize = CGSize(width: CGFloat(Float.infinity), height: height)
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return textSize.width
    }
    
    func showAlert(title:String?, message:String?, button:String?,  viewController:UIViewController, callback : @escaping () -> Void = {})
    {
        let btText:String = button ?? "확인"
        let titleText:String = title ?? "알림"
        let messageText:String = message ?? "오류가 발생하였습니다."
        
        let alert = R.storyboard.main.alertViewController()!
        
        alert.callback = { check in
            
            if check
            {
                callback()
            }
        }
        
        alert.titleText = titleText
        alert.messageText = messageText
        alert.buttonText = btText
        alert.isTwoButton = false
        alert.modalPresentationStyle = .overCurrentContext
        viewController.present(alert, animated: false, completion: nil)
        
    }
    
    func showAlert(title:String?, message:String?, button:String?, subButton:String?, viewController:UIViewController, callback : @escaping () -> Void = {})
    {
        let btText:String = button ?? "확인"
        let subBtText:String = subButton ?? "취소"
        let titleText:String = title ?? "알림"
        let messageText:String = message ?? ""
        
        let alert = R.storyboard.main.alertViewController()!
        
        alert.callback = { check in
            
            if check
            {
                callback()
            }
        }
        
        alert.titleText = titleText
        alert.messageText = messageText
        alert.buttonText = btText
        alert.subButtonText = subBtText
        alert.isTwoButton = true
        alert.modalPresentationStyle = .overCurrentContext
        viewController.present(alert, animated: false, completion: nil)
        
    }
    
    func showAlert(title:String?, message:String?, button:String?, subButton:String?, viewController:UIViewController, callback : @escaping (_ isSend : Bool) -> Void)
    {
        let btText:String = button ?? "확인"
        let subBtText:String = subButton ?? "취소"
        let titleText:String = title ?? "알림"
        let messageText:String = message ?? ""
        
        let alert = R.storyboard.main.alertViewController()!
        
        alert.callback = { check in
            
            callback(check)
            
        }
        
        alert.titleText = titleText
        alert.messageText = messageText
        alert.buttonText = btText
        alert.subButtonText = subBtText
        alert.isTwoButton = true
        alert.modalPresentationStyle = .overCurrentContext
        viewController.present(alert, animated: false, completion: nil)
        
    }
    
    
    func showHAlert(title:String?, message:String?, button:String?, subButton:String?, viewController:UIViewController, callback : @escaping (_ isSend : Bool) -> Void)
        {
            let btText:String = button ?? "확인"
            let subBtText:String = subButton ?? "취소"
            let titleText:String = title ?? "알림"
            let messageText:String = message ?? ""
            
            var alert = R.storyboard.main.alertHBtnViewController()!
            var tableAlert = R.storyboard.main.alertHBtnTabletViewController()!
            //테블릿인지 아닌지 확인
            if UIDevice.current.userInterfaceIdiom == .pad {
                alert.callback = { check in
                   callback(check)
                }
               
               alert.titleText = titleText
               alert.messageText = messageText
               alert.buttonText = btText
               alert.subButtonText = subBtText
               alert.isTwoButton = true
               alert.modalPresentationStyle = .overCurrentContext
               viewController.present(alert, animated: false, completion: nil)
                
            } else {
                
                alert.callback = { check in
                    callback(check)
                }
                
                alert.titleText = titleText
                alert.messageText = messageText
                alert.buttonText = btText
                alert.subButtonText = subBtText
                alert.isTwoButton = true
                alert.modalPresentationStyle = .overCurrentContext
                viewController.present(alert, animated: false, completion: nil)
            }
          
            
        }
    
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
    // 시간 분에서 시간만 리턴.
    func convertDurationHour(duration:Int) -> String {
        
        let MIN:Int = 60
        let HOUR:Int = MIN * 60
        var h, m, s:Int
            
        s =  Int(duration % MIN)
        m =  Int(duration) >= HOUR ?  Int((duration % HOUR) / MIN) : Int(duration / MIN)
        h = Int(duration / HOUR)
    
        var dur:String = ""
        if ( h > 0 ) {
            dur = "\(h)";
        } else {
            dur = "";
        }
        
        return dur
    }
    
    // 시간 분에서 분만 리턴
    func convertDurationMin(duration:Int) -> String {
    
        let MIN:Int = 60
        let HOUR:Int = MIN * 60
        var h, m, s:Int
            
        s =  Int(duration % MIN)
        m =  Int(duration) >= HOUR ?  Int((duration % HOUR) / MIN) : Int(duration / MIN)
        h = Int(duration / HOUR)
    
        var dur:String = ""
        if ( duration == 0) {
            dur = "0";
        } else {
            dur = "\(m)";
        }
        return dur
    }

    
    func convertDurationString(duration:Int) -> String {
    
        let MIN:Int = 60
        let HOUR:Int = MIN * 60
        var h, m, s:Int
            
        s =  Int(duration % MIN)
        m =  Int(duration) >= HOUR ?  Int((duration % HOUR) / MIN) : Int(duration / MIN)
        h = Int(duration / HOUR)
    
        var dur:String = ""
        if ( duration == 0) {
            dur = "0분";
        } else if ( h == 0 ) {
            dur = "\(m)분";
        } else {
            dur = "\(h )시간 \(m)분";
        }
            
        return dur

    }
    
    
    func cancelPendingLocalPush()
    {
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
    }
    func saveLocalPush(date : Date)
    {
        let calendar = NSCalendar(identifier: .gregorian)!;
        let fireComponents = calendar.components( [NSCalendar.Unit.day, NSCalendar.Unit.month, NSCalendar.Unit.year, NSCalendar.Unit.hour, NSCalendar.Unit.minute, NSCalendar.Unit.second], from:date);

        let content = UNMutableNotificationContent()
        let identifier = "study_time"
        content.title = "학습 시작 알림"
        content.body = "약속한 학습 시간이 지났어요. 시작 인증사진을 보내주세요! 👀"
        content.sound = .default
//        content.categoryIdentifier = "image-message"
//
//        if let imageURL = Bundle.main.url(forResource: "pushImage", withExtension: "png")
//        {
//
//            let attach = try! UNNotificationAttachment(identifier: "pushImage", url: imageURL, options: .none)
//            content.attachments = [attach]
//
//        }
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: fireComponents, repeats: false)
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    func checkNewBotMessage(callback : @escaping (_ hasNew : Bool) -> Void = { _ in})
    {
        var retValue = false
        var myInfo:MyInfo? = nil
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let container = appDelegate.persistentContainer
        
        do {
            let myInfos = try container.viewContext.fetch(MyInfo.fetchRequest()) //as! [MyInfo]
            if myInfos.count > 0
            {
                myInfo = myInfos[0]
            }
        } catch {
            print(error.localizedDescription)
            callback(retValue)
            return
        }
        
        guard let userSeq = myInfo?.userSeq else {
            callback(retValue)
            return
        }
        
        //챗봇 메세지 체크
        ChatBotAPI.shared.messages(userSeq: userSeq, type: Const.ChatBotListType.recent.rawValue).done { res in
            
            if let list = res.data , list.count > 0
            {
                let data = list.last!
                let lastSeq = data.messageSeq ?? 0
                let prevLastSeq = CommonUtil.shared.getChatbotLastSeq()
                if lastSeq > prevLastSeq
                {
                    retValue = true
                }
            
            }
            
            callback(retValue)
            
        }.catch {error in
            
            callback(retValue)
        }.finally {
            
        }
        
    }
    //0 : no message
    //1 : chatting message
    //2 : chatbot message
    //3 : both
    func checkNewMessage(callback : @escaping (_ hasNew : Int) -> Void = { _ in})
    {
        var retValue = 0
        var myInfo:MyInfo? = nil
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let container = appDelegate.persistentContainer
        
        do {
            let myInfos = try container.viewContext.fetch(MyInfo.fetchRequest()) //as! [MyInfo]
            if myInfos.count > 0
            {
                myInfo = myInfos[0]
            }
        } catch {
            print(error.localizedDescription)
            callback(retValue)
            return
        }
        
        guard let userSeq = myInfo?.userSeq else {
            callback(retValue)
            return
        }
    }
    
    
    func logout(_ notice : Bool = true)
    {
        print("logout")
        
        if notice
        {
            self.showAlert(title: "알림", message: "로그아웃 하시겠습니까?", button: "확인", subButton: "취소", viewController: UIViewController.currentViewController()!) {
                self.logoutAction()
            }
        }
        else
        {
            self.logoutAction()
        }

    }
    
    func logoutAction()
    {
        let event = ABInAppEvent()
        event?.setCategory(ABCategory.signOut)
        event?.send()

        AirBridge.state().setUser(ABUser())
        
        
        //remove access token
        CommonUtil.shared.removeAccessToken()
        
        //remove refresh token
        CommonUtil.shared.removeRefreshToken()
        
        
        //remove study time
        CommonUtil.shared.removeStudyTime()
        
        CommonUtil.hasDiagnosis = false
        CommonUtil.tabbarController = nil
        CommonUtil.userInfoRemote = nil
        //go onboard
        
        let onBoardVC = R.storyboard.main.weakCat4OnboardViewController()!
        let navigationController = UINavigationController(rootViewController: onBoardVC)
        navigationController.isNavigationBarHidden = true
        
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDel.window?.rootViewController = navigationController
    }
    
    func openCustomURLScheme(_ customURLScheme: String) -> Bool {
        let customURL = URL(string: customURLScheme)!
        if UIApplication.shared.canOpenURL(customURL) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(customURL)
            } else {
                UIApplication.shared.openURL(customURL)
            }
            return true
        }
        
        return false
    }
    
    
    func goStudy(){
        

        
//        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
//        if let vcDetail = R.storyboard?.instantiateViewController(withIdentifier: "tabbarController") as? TabbarController {
//            if let vc = vcDetail?.viewControllers![1] as? StudyNavigationViewController {
//
//                let navigationController = StudyNavigationViewController()
//                vcDetail?.viewControllers![1] = navigationController
//            }
//        }
        
        
//        if let vcDetail = R.storyboard.main.tabbarController() {
//            if let vc = vcDetail.viewControllers![1] as? StudyNavigationViewController {
//
//                let navigationController = StudyNavigationViewController()
//                vcDetail.viewControllers![1] = navigationController
//            }
//        }
        
        
        if let vcDetail = R.storyboard.main.tabbarController() {
                    
            let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDel.window?.rootViewController = vcDetail
            vcDetail.selectedIndex = 1
            vcDetail.bottomNavBar.selectedItem = vcDetail.bottomNavBar.items[1]

            // A mask of options indicating how you want to perform the animations.
            let options: UIView.AnimationOptions = .transitionCrossDissolve

            // The duration of the transition animation, measured in seconds.
            let duration: TimeInterval = 0.3

            // Creates a transition animation.
            // Though `animations` is optional, the documentation tells us that it must not be nil. ¯\_(ツ)_/¯
            UIView.transition(with: appDel.window!, duration: duration, options: options, animations: {}, completion:
            { completed in
                // maybe do something on completion here
            })
            
        }
    
    }
    
    
    func ABEvent(category:String, customs:[String:Any]?){
        
        if !Const.isProd {
            print("ABEvent category:\(category), customs:\(customs)")
        }
        
        
        let event = ABInAppEvent()

        event?.setCategory(category)
        event?.setCustoms(customs)
        event?.send()
    }
    
    
    func addShareToolTip(frame : CGRect , desc: String, parent : UIView, font : UIFont, lineHeight : CGFloat) -> UIView
    {
        let labelLRPadding : CGFloat = 8
        let labelTBPadding : CGFloat = 8
        
        let viewTopMargin  : CGFloat = 35
        let triangleMargin : CGFloat = 135
        
        let triangleWidth  : CGFloat = 17
        
        
        let textWidth = CommonUtil.shared.caluateWidth(height: lineHeight, font: font, text: desc)
        let textLine = CommonUtil.shared.calculateMaxLines(width: (textWidth > Const.MAX_TOOL_TIP_WIDTH) ? Const.MAX_TOOL_TIP_WIDTH : textWidth, font: font, text: desc)
        
        let balloonWidth :  CGFloat = ((textWidth > Const.MAX_TOOL_TIP_WIDTH) ? Const.MAX_TOOL_TIP_WIDTH : textWidth ) + labelLRPadding * 2
        let ballonHeight : CGFloat = viewTopMargin + labelTBPadding * 2 + CGFloat(textLine) * lineHeight
        
//        let x : CGFloat = frame.origin.x + frame.size.width / 2  - triangleMargin - (triangleWidth / 2)
//        let y : CGFloat = frame.origin.y + frame.size.height + 1
        
        let x : CGFloat = frame.origin.x + frame.size.width / 2  - triangleMargin + (triangleWidth / 2) + 6
        let y : CGFloat = frame.origin.y - frame.size.height - 20
        
        
        
        let balloonView = PuljaShareToolTipView(frame: CGRect(x: x, y: y, width: balloonWidth, height: ballonHeight ))
        parent.addSubview(balloonView)
        balloonView.lbDesc.text = desc
        balloonView.lbDesc.setMinimumLineHeight(minimumLineHeight: lineHeight, alignment: .left)
        balloonView.callback = { balloon in
 
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3, animations: {
 
                    balloonView.alpha = 0
                    
                }, completion: {finished in
                    
                    balloon.removeFromSuperview()
                })
            }
                
            
        }
        
        let leadingConstraint = NSLayoutConstraint(item: balloonView, attribute: .leading, relatedBy: .equal, toItem: parent, attribute: .leading, multiplier: 1, constant: x)
        let topConstraint = NSLayoutConstraint(item: balloonView, attribute: .top, relatedBy: .equal, toItem: parent, attribute: .top, multiplier: 1, constant: y)
        let widthConstraint = NSLayoutConstraint(item: balloonView, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: balloonWidth)
        let heightConstraint = NSLayoutConstraint(item: balloonView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: ballonHeight)
      
        parent.addConstraints([leadingConstraint, topConstraint,widthConstraint, heightConstraint])
        
        return balloonView
        
        
    }
    
    func addTooTip(frame : CGRect , desc: String, parent : UIView, font : UIFont, lineHeight : CGFloat) -> UIView
    {

        let labelLRPadding : CGFloat = 8
        let labelTBPadding : CGFloat = 8
        let viewTopMargin  : CGFloat = 9
        let triangleMargin : CGFloat = 33
        let triangleWidth  : CGFloat = 17
        
        
        let textWidth = CommonUtil.shared.caluateWidth(height: lineHeight, font: font, text: desc)
        let textLine = CommonUtil.shared.calculateMaxLines(width: (textWidth > Const.MAX_TOOL_TIP_WIDTH) ? Const.MAX_TOOL_TIP_WIDTH : textWidth, font: font, text: desc)
        
        let balloonWidth :  CGFloat = ((textWidth > Const.MAX_TOOL_TIP_WIDTH) ? Const.MAX_TOOL_TIP_WIDTH : textWidth ) + labelLRPadding * 2
        let ballonHeight : CGFloat = viewTopMargin + labelTBPadding * 2 + CGFloat(textLine) * lineHeight
        
        let x : CGFloat = frame.origin.x + frame.size.width / 2  - triangleMargin - (triangleWidth / 2)
        let y : CGFloat = frame.origin.y + frame.size.height + 1
        
        let balloonView = PuljaToolTipView(frame: CGRect(x: x, y: y, width: balloonWidth, height: ballonHeight ))
        parent.addSubview(balloonView)
        balloonView.lbDesc.text = desc
        balloonView.lbDesc.setMinimumLineHeight(minimumLineHeight: lineHeight, alignment: .left)
        balloonView.callback = { balloon in
 
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3, animations: {
 
                    balloonView.alpha = 0
                    
                }, completion: {finished in
                    
                    balloon.removeFromSuperview()
                })
            }
                
            
        }
        
        let leadingConstraint = NSLayoutConstraint(item: balloonView, attribute: .leading, relatedBy: .equal, toItem: parent, attribute: .leading, multiplier: 1, constant: x)
        let topConstraint = NSLayoutConstraint(item: balloonView, attribute: .top, relatedBy: .equal, toItem: parent, attribute: .top, multiplier: 1, constant: y)
        let widthConstraint = NSLayoutConstraint(item: balloonView, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: balloonWidth)
        let heightConstraint = NSLayoutConstraint(item: balloonView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: ballonHeight)
      
        parent.addConstraints([leadingConstraint, topConstraint,widthConstraint, heightConstraint])
        
        return balloonView
        
    }
    
    
    
    func addCloseTooTip(frame : CGRect , desc: String, parent : UIView, font : UIFont, lineHeight : CGFloat) -> UIView
        {

            let labelLRPadding : CGFloat = 8
            let labelTBPadding : CGFloat = 8
            let viewTopMargin  : CGFloat = 9
            let triangleMargin : CGFloat = 33
            let triangleWidth  : CGFloat = 17
            
            
            let textWidth = CommonUtil.shared.caluateWidth(height: lineHeight, font: font, text: desc)
            let textLine = CommonUtil.shared.calculateMaxLines(width: (textWidth > Const.MAX_TOOL_TIP_WIDTH) ? Const.MAX_TOOL_TIP_WIDTH : textWidth, font: font, text: desc)
            
            let balloonWidth :  CGFloat = ((textWidth > Const.MAX_TOOL_TIP_WIDTH) ? Const.MAX_TOOL_TIP_WIDTH : textWidth ) + labelLRPadding * 2
            let ballonHeight : CGFloat = viewTopMargin + labelTBPadding * 2 + CGFloat(textLine) * lineHeight
            
            let x : CGFloat = frame.origin.x + frame.size.width / 2  - triangleMargin - (triangleWidth / 2)
            let y : CGFloat = frame.origin.y + frame.size.height + 1
            
            let balloonView = PuljaToolTipView(frame: CGRect(x: x, y: y, width: balloonWidth, height: ballonHeight ))
            parent.addSubview(balloonView)
            balloonView.lbDesc.text = desc
            balloonView.lbDesc.setMinimumLineHeight(minimumLineHeight: lineHeight, alignment: .left)
            balloonView.callback = { balloon in
     
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.3, animations: {
     
                        balloonView.alpha = 0
                        
                    }, completion: {finished in
                        
                        balloon.removeFromSuperview()
                    })
                }
                    
                
            }
            
            let leadingConstraint = NSLayoutConstraint(item: balloonView, attribute: .leading, relatedBy: .equal, toItem: parent, attribute: .leading, multiplier: 1, constant: x)
            let topConstraint = NSLayoutConstraint(item: balloonView, attribute: .top, relatedBy: .equal, toItem: parent, attribute: .top, multiplier: 1, constant: y)
            let widthConstraint = NSLayoutConstraint(item: balloonView, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: balloonWidth)
            let heightConstraint = NSLayoutConstraint(item: balloonView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: ballonHeight)
          
            parent.addConstraints([leadingConstraint, topConstraint,widthConstraint, heightConstraint])
            
            return balloonView
            
        }
    
}


