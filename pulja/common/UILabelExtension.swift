//
//  UILabelExtension.swift
//  pulja
//
//  Created by samdols on 2022/02/08.
//

import Foundation
import UIKit


extension UILabel {
    
    func setMargins(_ margin: CGFloat = 10) {
        if let textString = self.text {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.firstLineHeadIndent = margin
            paragraphStyle.headIndent = margin
            paragraphStyle.tailIndent = -margin
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
            attributedText = attributedString
        }
    }

    
    func setMinimumLineHeight(minimumLineHeight : CGFloat , alignment : NSTextAlignment = NSTextAlignment.left)
    {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 0.5
        paragraphStyle.minimumLineHeight = minimumLineHeight
        paragraphStyle.alignment = alignment
        paragraphStyle.lineBreakMode = .byTruncatingTail
        
        let attrubutedString = NSMutableAttributedString(string: text ?? "" )
        attrubutedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrubutedString.length))
        self.attributedText = attrubutedString;
    }
    
    func setLineHeight(lineHeight: CGFloat) {
           let paragraphStyle = NSMutableParagraphStyle()
           paragraphStyle.lineSpacing = 1.0
           paragraphStyle.lineHeightMultiple = lineHeight
           paragraphStyle.alignment = self.textAlignment

           let attrString = NSMutableAttributedString()
           if (self.attributedText != nil) {
               attrString.append( self.attributedText!)
           } else {
               attrString.append( NSMutableAttributedString(string: self.text!))
               attrString.addAttribute(NSAttributedString.Key.font, value: self.font, range: NSMakeRange(0, attrString.length))
           }
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attrString.length))
           self.attributedText = attrString
       }
    
}
