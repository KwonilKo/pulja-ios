//
//  UIColor+Additions.swift
//  풀자 앱 iOS
//
//  Generated on Zeplin. (2021. 11. 24.).
//  Copyright (c) 2021 __MyCompanyName__. All rights reserved.
//

import UIKit
import UIGradient

extension UIColor {

  @nonobjc class var puljaLightblue: UIColor {
    return UIColor(red: 145.0 / 255.0, green: 167.0 / 255.0, blue: 188.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var puljaRealblack: UIColor {
    return UIColor(white: 0.0, alpha: 1.0)
  }

  @nonobjc class var puljaRed: UIColor {
    return UIColor(red: 237.0 / 255.0, green: 72.0 / 255.0, blue: 29.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var puljaBlueblack: UIColor {
    return UIColor(red: 35.0 / 255.0, green: 41.0 / 255.0, blue: 48.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var puljaBlue: UIColor {
    return UIColor(red: 85.0 / 255.0, green: 62.0 / 255.0, blue: 1.0, alpha: 1.0)
  }

  @nonobjc class var lightBlueGrey: UIColor {
    return UIColor(red: 197.0 / 255.0, green: 197.0 / 255.0, blue: 216.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var puljaBlack: UIColor {
    return UIColor(white: 41.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var puljaPaleGrey: UIColor {
    return UIColor(red: 240.0 / 255.0, green: 239.0 / 255.0, blue: 249.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var pastelBlue: UIColor {
    return UIColor(red: 166.0 / 255.0, green: 183.0 / 255.0, blue: 1.0, alpha: 1.0)
  }

  @nonobjc class var purpleishBlue: UIColor {
    return UIColor(red: 85.0 / 255.0, green: 62.0 / 255.0, blue: 1.0, alpha: 1.0)
  }

  @nonobjc class var paleGrey: UIColor {
    return UIColor(red: 245.0 / 255.0, green: 247.0 / 255.0, blue: 250.0 / 255.0, alpha: 1.0)
  }
    

    @nonobjc class var reddishOrange: UIColor {
    
        return UIColor(red: 237.0 / 255.0, green: 72.0 / 255.0, blue: 29.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var indigoBlue: UIColor {
      return UIColor(red: 48.0 / 255.0, green: 23.0 / 255.0, blue: 140.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var paleSalmon: UIColor {
        return UIColor(red: 255.0 / 255.0, green: 164.0 / 255.0, blue: 164.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var Green: UIColor {
        return UIColor(red: 0.0 / 255.0, green: 201.0 / 255.0, blue: 151.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var Green10: UIColor {
        return UIColor(red: 0.0 / 255.0, green: 201.0 / 255.0, blue: 151.0 / 255.0, alpha: 0.1)
    }
    
    @nonobjc class var OffWhite: UIColor {
        return UIColor(red: 245.0 / 255.0, green: 247.0 / 255.0, blue: 250.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var OffBlack: UIColor {
        return UIColor(red: 82.0 / 255.0, green: 82.0 / 255.0, blue: 83.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var OffRed: UIColor {
        return UIColor(red: 255.0 / 255.0, green: 129.0 / 255.0, blue: 77.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var OffBlue: UIColor {
        return UIColor(red: 119.0 / 255.0, green: 157.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var OffOrange: UIColor {
        return UIColor(red: 255.0 / 255.0, green: 177.0 / 255.0, blue: 56.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var OffGreen: UIColor {
        return UIColor(red: 133.0 / 255.0, green: 193.0 / 255.0, blue: 2.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var Purple: UIColor {
        return UIColor(red: 99.0 / 255.0, green: 78.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var Purple10: UIColor {
        return UIColor(red: 99.0 / 255.0, green: 78.0 / 255.0, blue: 255.0 / 255.0, alpha: 0.1)
    }
    @nonobjc class var PalePink: UIColor {
        return UIColor(red: 252.0 / 255.0, green: 237.0 / 255.0, blue: 237.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var Foggy: UIColor {
        return UIColor(red: 204.0 / 255.0, green: 206.0 / 255.0, blue: 208.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var Foggy20: UIColor {
        return UIColor(red: 245.0 / 255.0, green: 246.0 / 255.0, blue: 245.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var Foggy40: UIColor {
        return UIColor(red: 235.0 / 255.0, green: 235.0 / 255.0, blue: 236.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var OffPurple: UIColor {
        return UIColor(red: 99.0 / 255.0, green: 78.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var PastelBlue: UIColor {
        return UIColor(red: 248.0 / 255.0, green: 251.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var PastelGreen: UIColor {
        return UIColor(red: 249.0 / 255.0, green: 252.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var PastelOrange: UIColor {
        return UIColor(red: 255.0 / 255.0, green: 253.0 / 255.0, blue: 243.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var PastelRed: UIColor {
        return UIColor(red: 255.0 / 255.0, green: 249.0 / 255.0, blue: 249.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var grey: UIColor {
        return UIColor(red: 163.0 / 255.0, green: 165.0 / 255.0, blue: 166.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var grey80: UIColor {
        return UIColor(red: 181.0 / 255.0, green: 183.0 / 255.0, blue: 184.0 / 255.0, alpha: 0.8)
    }
    @nonobjc class var grey20: UIColor {
        return UIColor(red: 204.0 / 255.0, green: 206.0 / 255.0, blue: 208.0 / 255.0, alpha: 0.2)
    }
    @nonobjc class var DiagnosisBackground: UIColor {
        return UIColor(red: 58.0 / 255.0, green: 60.0 / 255.0, blue: 79.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var Offlilac: UIColor {
        return UIColor(red: 187.0 / 255.0, green: 178.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var Paleblue: UIColor {
        return UIColor(red: 187.0 / 255.0, green: 205.0 / 255.0, blue: 251.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var Red: UIColor {
        return UIColor(red: 255.0 / 255.0, green: 105.0 / 255.0, blue: 87.0 / 255.0, alpha: 0.1)
    }
    
    @nonobjc class var red10: UIColor {
        return UIColor(red: 255.0 / 255.0, green: 105.0 / 255.0, blue: 87.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var green10: UIColor {
        return UIColor(red: 0.0 / 255.0, green: 201.0 / 255.0, blue: 151.0 / 255.0, alpha: 0.1)
    }
    
    
    @nonobjc class var paledPurple: UIColor {
        return UIColor(red: 221.0 / 255.0, green: 216.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var Dust: UIColor {
        return UIColor(red: 123.0 / 255.0, green: 123.0 / 255.0, blue: 125.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var PalePinkTouched: UIColor {
        return UIColor(red: 252.0 / 255.0, green: 221.0 / 255.0, blue: 213.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var submitButtonPressed: UIColor {
        return UIColor(red: 68.0 / 255.0, green: 46.0 / 255.0, blue: 227.0 / 255.0, alpha: 1.0)
    }
    
    
    static func fromGradient(_ gradient: GradientLayer, frame: CGRect, cornerRadius: CGFloat = 0) -> UIColor? {
        guard let image = UIImage.fromGradient(gradient, frame: frame, cornerRadius: cornerRadius) else { return nil }
        return UIColor(patternImage: image)
    }
    
    static func fromGradientWithDirection(_ direction: GradientDirection, frame: CGRect, colors: [UIColor], cornerRadius: CGFloat = 0, locations: [Double]? = nil) -> UIColor? {
        let gradient = GradientLayer(direction: direction, colors: colors, cornerRadius: cornerRadius, locations: locations)
        return UIColor.fromGradient(gradient, frame: frame)
    }
    
    static func hex(_ hex: String, alpha: CGFloat = 1.0) -> UIColor {
        guard let hex = Int(hex, radix: 16) else { return UIColor.clear }
        return UIColor(red: ((CGFloat)((hex & 0xFF0000) >> 16)) / 255.0,
                       green: ((CGFloat)((hex & 0x00FF00) >> 8)) / 255.0,
                       blue: ((CGFloat)((hex & 0x0000FF) >> 0)) / 255.0,
                       alpha: alpha)
    }

}
