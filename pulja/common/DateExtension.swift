//
//  DateExtension.swift
//  pulja
//
//  Created by 김병헌 on 2022/01/19.
//


import Foundation
import UIKit

enum DateFormatType: String {
    /// Time
    case time = "HH:mm:ss"
    
    /// Date with hours
    case dateWithTime = "yyyy-MM-dd HH:mm:ss"
    
    case dateWithTimeMS = "yyyy-MM-dd HH:mm:ss.S"
    
    case dateRequest = "yyyy-MM-dd HH:mm"
    
    /// Date
    case date = "yyyy-MM-dd"
    case date3mon = "MMM. dd. yyyy"
    case lounge = "MMM dd, yyyy HH:mm"
    case announcement = "MMM dd, yyyy"
    case calendar = "MMMM yyyy"
    case birthChartTime = "h:mma"
    case mainTab = "M/d"
    case mainTab2 = "EEE (M/d)"
    case dotdate = "yyyy.MM.dd"
    case slashdate = "yyyy/MM/dd"
    case noDotdate = "yyyyMMdd"
    case spaceDate = "yyyy MM dd"
    
    case koYMD = "yyyy년 MM월 dd일"
    
    
    case timemin = "HH:mm"
    
    case parkingfee = "YY.MM.dd HH:mm"
    
    case handleDetail = "MM/dd HH:mm"
    
    case handleDetail2 = "yy/MM/dd HH:mm"
    
    case notiStart = "MM.dd HH:mm"
    
    case notiEnd = "HHmm"
    
    case filename  = "yyyyMMddHHmmss"
    
    case timeLine = "MM.dd"
    
    case onlyYear = "yyyy"
    
    case lastnotify = "MM.dd. HH:mm:ss"
    
    case full = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    
    case hour = "HH"
    
    case mm = "mm"
    case ss = "ss"
    
    case push = "yyyy-MM-dd'T'HH:mm:ss"
    
    case mmdd = "MM-dd"
    
    case chatbotToday = "a h:mm"
    case chatbotMD = "MM월 dd일"
    case chatbotNotToday = "yyyy년 MM월 dd일 a h시 mm분"
    case studyTime  = "yyyyMMddHHmm"
    
    case chatMDE = "MM월 dd일 eeee"
    
    case studyTimeToday = "오늘 학습 시간: a h:mm"
    case studyTimeTommorow = "내일 학습 시간: a h:mm"
    case studyTimeAnother = "학습 시간 : yyyy년 MM월 dd일 a h시 mm분"
    
    
}


extension Date {
    
    var age: Int { Calendar.current.dateComponents([.year], from: self, to: Date()).year! }
    
    /// Convert Date to String
    func toString(_ formatType: DateFormatType = .date) -> String {
        let dateFormatter = DateFormatter()
//        dateFormatter.locale = Locale(identifier:"ko_KR")
//        dateFormatter.timeZone = TimeZone(abbreviation: "KST")
        
        dateFormatter.dateFormat = formatType.rawValue // Your New Date format as per requirement change it own
        
        let newDate: String = dateFormatter.string(from: self) // pass Date here
        //print(newDate) // New formatted Date string
        
        return newDate
    }
    func toStringKO(_ formatType: DateFormatType = .date) -> String {
           let dateFormatter = DateFormatter()
           dateFormatter.locale = Locale(identifier:"ko")
   //        dateFormatter.timeZone = TimeZone(abbreviation: "KST")
           
           dateFormatter.dateFormat = formatType.rawValue // Your New Date format as per requirement change it own
           
           let newDate: String = dateFormatter.string(from: self) // pass Date here
           //print(newDate) // New formatted Date string
           
           return newDate
       }
    
    
    
    func toString(_ formatType: DateFormatType = .date ,  timezone : TimeZone) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current
        dateFormatter.timeZone = timezone
        
        dateFormatter.dateFormat = formatType.rawValue // Your New Date format as per requirement change it own
        
        let newDate: String = dateFormatter.string(from: self) // pass Date here
        //print(newDate) // New formatted Date string
        return newDate
    }
    
    
    func secondsFromBeginningOfTheDay() -> Int {
        let calendar = Calendar.current
        // omitting fractions of seconds for simplicity
        let dateComponents = calendar.dateComponents([.hour, .minute, .second], from: self)
        
        let dateSeconds = dateComponents.hour! * 3600 + dateComponents.minute! * 60 + dateComponents.second!
        
        return dateSeconds
    }

    
    //음수면 이전 날짜
    //양수면 이후 날짜
    func getSomeDayDate(day: Double) -> Date {
 
        let resultDate: Date
        
        if day > 0 {
            resultDate = Date(timeInterval: 86400 * day, since: self as Date)
        } else {
            resultDate = Date(timeInterval: -86400 * fabs(day), since: self as Date)
        }
        
        return resultDate
    }
    
    func compareNow() -> TimeInterval {
        
        let current = Date()
        let interval = self.timeIntervalSince(current)
        return interval
        
    }
    
    func compareNowToText() -> String {
           
           let current = Date()
           let diff = current.timeIntervalSince(self)
           let text:String
           
           if diff < 11
           {
              text = "조금 전"
           }
           else if diff < 60
           {
               text = String(format: "%d초 전", Int(diff))
           }
           else if diff < 60*60
           {
               let min  = Int(floor(diff / 60.0))
               text = String(format: "%d분 전", min)
            //text = String(min) + ( ( min > 1 ) ? " minutes ago" : " minute ago" )
           }
           else  //if diff < 60*60*24
           {
               let hour  = Int(floor(diff / (60*60)))
               text = String(format: "%d시간 전", hour)
//               text = String(hour) + ( ( hour > 1 ) ? " hours ago" : " hour ago" )
           }
//           else
//           {
//               let day  = Int(floor(diff / (60*60*24)))
//               text = String(day) + ( ( day > 1 ) ? " days ago" : " day ago" )
//           }
           
           
           return text
           
       }
    
    
    
    func getChattingTime() -> String {
           
           let current = Date()
           let diff = self.timeIntervalSince(current) * -1
           let text:String
           
           if diff < 0
           {
              text = " "
           }
           else if diff < 60
           {
                text =  "• 1m"
           }
           else if diff < 60*60
           {
               let min  = Int(floor(diff / 60.0))
               text = String(format: "• %dm", min)
           }
           else if diff < 60*60*24
           {
               let hour  = Int(floor(diff / (60*60)))
                text = String(format: "• %dh", hour)
           }
           else if diff < 60*60*24*7
           {
               let day  = Int(floor(diff / (60*60*24)))
            
               text = String(format: "• %dd", day)
               
           }
           else
           {
                let week  = Int(floor(diff / (60*60*24*7)))
                text = String(format: "• %dw", week)
           }
           
           
           return text
           
       }
    
    // 학습일시가 오늘인지 내일인지
    //1 : today
    //2 : tommorow
    //3 : anotherDay
    func isToday() -> Int
    {
        let firstTimeDate = self.toString(.noDotdate).toDate(.noDotdate)
        let nowDate = Date().toString(.noDotdate).toDate(.noDotdate)
        
        let timeDiff = firstTimeDate.timeIntervalSince(nowDate)

        if timeDiff == 0
        {
            return 1
        }
        else if timeDiff == 60*60*24
        {
            return 2
        }
        
        return 3
    }
    
    var daysInMonth:Int{
           let calendar = Calendar.current
           
           let dateComponents = DateComponents(year: calendar.component(.year, from: self), month: calendar.component(.month, from: self))
           let date = calendar.date(from: dateComponents)!
           
           let range = calendar.range(of: .day, in: .month, for: date)!
           let numDays = range.count
           
           return numDays
       }
    
    func getFormattedDate(format: String) -> String {
           let dateformat = DateFormatter()
           dateformat.dateFormat = format
           return dateformat.string(from: self)
       }
    
    
    
}

extension String {
    /// Convert String to Date
    func toDate() -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormatType.date.rawValue // Your date format
        let serverDate: Date = dateFormatter.date(from: self)! // according to date format your date string
        return serverDate
    }

    func toDate(_ formatType: DateFormatType = .date) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatType.rawValue // Your date format
        if let serverDate: Date = dateFormatter.date(from: self)
        {
            return serverDate
        }
        else //방어코드
        {
            return Date()
        }
        
    }

    func toPushDate(_ formatType: DateFormatType = .date) -> Date? {
        let dateFormatter = DateFormatter()
//        let timeZone = TimeZone.current
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = formatType.rawValue // Your date format
        if let serverDate: Date = dateFormatter.date(from: self)
        {
            return serverDate
        }
        return nil
        
    }

    
    func toDateNullable(_ formatType: DateFormatType = .date) -> Date? {
        let dateFormatter = DateFormatter()
//        dateFormatter.locale = Locale(identifier:"ko_KR")
//        dateFormatter.timeZone = TimeZone(abbreviation: "KST")
        dateFormatter.dateFormat = formatType.rawValue // Your date format
        if let serverDate: Date = dateFormatter.date(from: self)  // according to date format your date string
        {
            return serverDate
        }
        else
        {
            return nil
        }
    }
    
    func toDate(_ formatType: DateFormatType = .date,  timezone : TimeZone) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current
        dateFormatter.timeZone = timezone
        dateFormatter.dateFormat = formatType.rawValue // Your date format
        let serverDate: Date = dateFormatter.date(from: self)! // according to date format your date string
        return serverDate
    }
    
    /// Diviation calculation
    func toSeconds(_ formatType: DateFormatType = .date) -> Int {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatType.rawValue
        
        let date: Date = dateFormatter.date(from: self)!
        let calendar = Calendar.current
        let comp = calendar.dateComponents([.hour, .minute, .second], from: date)
        let hour = comp.hour
        let minute = comp.minute
        let sec = comp.second
        
        let totalSeconds = ((hour! * 60) * 60) + (minute! * 60) + sec!
        
        return totalSeconds
    }
    
    //현재 시간과의 비교한 값 리턴은 초단위.
    func compareCurrentTime() -> Int {
        
        let current = Date()
        let startAtDate = self.toDate(DateFormatType.dateWithTime)
        let interval = startAtDate.timeIntervalSince(current)
        return Int(interval)
        
    }

    
    //현재 날짜와 비교한 값 리턴은 초단위.
    //리워드 등 날짜 선택 시 미래가 선택이 안되게.
    func compareCurrentDay() -> Int {
        
        let current = Date()
        let dayCurrent = current.toString(DateFormatType.dotdate).toDate(DateFormatType.dotdate)
        
        let startAtDate = self.toDate(DateFormatType.dotdate)
        let interval = startAtDate.timeIntervalSince(dayCurrent)
        return Int(interval)
        
    }
    
    
    func toFormatString(_ formatType: DateFormatType = .dotdate) -> String {
        let defaultType: DateFormatType = .dateWithTimeMS
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = defaultType.rawValue

        if let date = inputFormatter.date(from: self) {
            return date.toString(formatType)
        } else {
            return self
        }
        
        
    }
    
    
    //알림에서 현재시간하고 비교.
    func compareNow(_ dateFormatType : DateFormatType) -> String {
        
        let current = Date()
        let dayCurrent = current.toString(DateFormatType.dateWithTime).toDate(DateFormatType.dateWithTime)
        
        let startAtDate = self.toDate(dateFormatType)
        let diff = dayCurrent.timeIntervalSince(startAtDate)
        let text:String
        
        if diff < 60
        {
            text = "지금"
        }
        else if diff < 60*60
        {
            let min  = Int(floor(diff / 60.0))
            text = String(min) + "분 전"
        }
        else if diff < 60*60*24
        {
            let hour  = Int(floor(diff / (60*60)))
            text = String(hour) + "시간 전"
        }
        else
        {
            let day  = Int(floor(diff / (60*60*24)))
            text = String(day) + "일 전"
        }
        
        
        return text
        
    }
    
    
    
        
        
        
    

}
