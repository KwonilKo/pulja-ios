//
//  UIColor+Tabman.swift
//  Example iOS
//
//  Created by Merrick Sapsford on 04/10/2020.
//

import UIKit

extension UIColor {
    
    class var tabmanPrimary: UIColor {
//        UIColor(red: 0.56, green: 0.18, blue: 0.89, alpha: 1.00)
        UIColor.puljaBlack
    }
    
    class var tabmanSecondary: UIColor {
//        UIColor(red: 136.0/255.0, green: 136.0/255.0, blue: 136.0/255.0, alpha: 1.00)
        UIColor.lightBlueGrey
    }
    
    class var tabmanForeground: UIColor {
        if #available(iOS 13, *) {
            return UIColor { (traitCollection) -> UIColor in
                switch traitCollection.userInterfaceStyle {
                case .dark:
                    return UIColor.puljaBlack
                default:
                    return UIColor.puljaBlack                }
            }
        } else {
            return UIColor.puljaBlack
        }
    }
}
