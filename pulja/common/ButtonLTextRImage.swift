//
//  ButtonLTextRImage.swift
//  pulja
//
//  Created by 김병헌 on 2022/10/04.
//

import UIKit

class ButtonLTextRImage: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func layoutSubviews() {
            super.layoutSubviews()
            if imageView != nil {
                imageEdgeInsets = UIEdgeInsets(top: 5, left: (bounds.width - 35), bottom: 5, right: 5)
                titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            }
        }

}
