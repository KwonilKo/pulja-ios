
import Foundation
import UIKit

@IBDesignable
class DesignableView: UIView {
    
    var gradLayer : CAGradientLayer?
}

@IBDesignable
class DesignableButton: UIButton {
    
    var gradLayer : CAGradientLayer?
    var disabledColorHandle: UInt8 = 0
    var highlightedColorHandle: UInt8 = 0
    var selectedColorHandle: UInt8 = 0
    
    
//    @IBInspectable var localisedKey: String? {
//        didSet {
//            guard let key = localisedKey else { return }
//            UIView.performWithoutAnimation {
//                setTitle(key.localized, for: .normal)
//                layoutIfNeeded()
//            }
//        }
//    }
    
    private func image(withColor color: UIColor) -> UIImage? {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()

        context?.setFillColor(color.cgColor)
        context?.fill(rect)

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return image
    }
    
//    func setBackgroundColor(_ color: UIColor, for state: UIControl.State) {
//
//        let _image = image(withColor: color)
//        self.imageView?.layer.cornerRadius = self.layer.cornerRadius
//        self.imageView?.layer.masksToBounds = true
//        self.setBackgroundImage( _image, for: state)
//    }

    @IBInspectable
    var disabledColor: UIColor? {
        get {
            if let color = objc_getAssociatedObject(self, &disabledColorHandle) as? UIColor {
                return color
            }
            return nil
        }
        set {
            if let color = newValue {
                self.setBackgroundColor(color, for: .disabled)
                
                objc_setAssociatedObject(self, &disabledColorHandle, color, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            } else {
                self.setBackgroundImage(nil, for: .disabled)
                objc_setAssociatedObject(self, &disabledColorHandle, nil, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }

    @IBInspectable
    var highlightedColor: UIColor? {
        get {
            if let color = objc_getAssociatedObject(self, &highlightedColorHandle) as? UIColor {
                return color
            }
            return nil
        }
        set {
            if let color = newValue {
                self.setBackgroundColor(color, for: .highlighted)
                objc_setAssociatedObject(self, &highlightedColorHandle, color, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            } else {
                self.setBackgroundImage(nil, for: .highlighted)
                objc_setAssociatedObject(self, &highlightedColorHandle, nil, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }

    @IBInspectable
    var selectedColor: UIColor? {
        get {
            if let color = objc_getAssociatedObject(self, &selectedColorHandle) as? UIColor {
                return color
            }
            return nil
        }
        set {
            if let color = newValue {
                self.setBackgroundColor(color, for: .selected)
                objc_setAssociatedObject(self, &selectedColorHandle, color, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            } else {
                self.setBackgroundImage(nil, for: .selected)
                objc_setAssociatedObject(self, &selectedColorHandle, nil, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    func setBackgroundColor(_ color: UIColor, for state: UIControl.State) {

        UIGraphicsBeginImageContext(CGSize(width: self.frame.width ,  height: self.frame.height))
        guard let context = UIGraphicsGetCurrentContext() else { return }

        let rect = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        let clipPath: CGPath = UIBezierPath(roundedRect: rect, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 8, height: 8)).cgPath
//        let clipPath: CGPath = UIBezierPath(roundedRect: rect, cornerRadius: 8).cgPath

        context.addPath(clipPath)
        context.setFillColor(color.cgColor)
        context.closePath()
        
//        context.setStrokeColor(color.cgColor)
//        context.strokePath()
        
        context.fillPath()


        let backgroundImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        self.setBackgroundImage(backgroundImage, for: state)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
      self.setup()
    }
    
    override init(frame: CGRect) {
      super.init(frame: frame)
      self.setup()
    }
    
    deinit {
        self.removeTarget(self, action: #selector(sendLog(btn:)) , for: .touchUpInside)
    }
}

@IBDesignable
class DesignableLabel: UILabel {
        @IBInspectable var topInset: CGFloat = 5.0
        @IBInspectable var bottomInset: CGFloat = 5.0
        @IBInspectable var leftInset: CGFloat = 7.0
        @IBInspectable var rightInset: CGFloat = 7.0

        override func drawText(in rect: CGRect) {
            let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
            super.drawText(in: rect.inset(by: insets))
        }

        override var intrinsicContentSize: CGSize {
            let size = super.intrinsicContentSize
            return CGSize(width: size.width + leftInset + rightInset,
                          height: size.height + topInset + bottomInset)
        }

        override var bounds: CGRect {
            didSet {
                // ensures this works within stack views if multi-line
                preferredMaxLayoutWidth = bounds.width - (leftInset + rightInset)
            }
        }
}


@IBDesignable
class PaddingLabel: UILabel {
    var textEdgeInsets = UIEdgeInsets.zero {
        didSet { invalidateIntrinsicContentSize() }
    }
    
    open override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        let insetRect = bounds.inset(by: textEdgeInsets)
        let textRect = super.textRect(forBounds: insetRect, limitedToNumberOfLines: numberOfLines)
        let invertedInsets = UIEdgeInsets(top: -textEdgeInsets.top, left: -textEdgeInsets.left, bottom: -textEdgeInsets.bottom, right: -textEdgeInsets.right)
        return textRect.inset(by: invertedInsets)
    }
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: textEdgeInsets))
    }
    
    @IBInspectable
    var paddingLeft: CGFloat {
        set { textEdgeInsets.left = newValue }
        get { return textEdgeInsets.left }
    }
    
    @IBInspectable
    var paddingRight: CGFloat {
        set { textEdgeInsets.right = newValue }
        get { return textEdgeInsets.right }
    }
    
    @IBInspectable
    var paddingTop: CGFloat {
        set { textEdgeInsets.top = newValue }
        get { return textEdgeInsets.top }
    }
    
    @IBInspectable
    var paddingBottom: CGFloat {
        set { textEdgeInsets.bottom = newValue }
        get { return textEdgeInsets.bottom }
    }
}


@IBDesignable
class DesignableImageView: UILabel {
}

extension UIView {
    

    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    

}

extension DesignableView
{
    func applyGradient(colours: [UIColor],
                       startPoint:CGPoint,
                       endPoint:CGPoint,
                       cornerRadius : CGFloat) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = startPoint
        gradient.endPoint = endPoint
        if cornerRadius > 0
        {
            gradient.cornerRadius = cornerRadius
        }
        self.gradLayer = gradient
        self.layer.insertSublayer(gradient, at: 0)
        
    }
    
    func removeGradient()
    {
        self.gradLayer?.removeFromSuperlayer()
        self.gradLayer = nil
    }
    
    func addDashedBorder() {
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.lightGray.cgColor
        shapeLayer.lineWidth = 2
        // passing an array with the values [2,3] sets a dash pattern that alternates between a 2-user-space-unit-long painted segment and a 3-user-space-unit-long unpainted segment
        shapeLayer.lineDashPattern = [2,3]
        
        let path = CGMutablePath()
        path.addLines(between: [CGPoint(x: 0, y: 0),
                                CGPoint(x: self.frame.width, y: 0)])
        shapeLayer.path = path
        layer.addSublayer(shapeLayer)
    }
}
extension DesignableButton
{

    func setup() {
        self.addTarget(self, action: #selector(sendLog(btn:)) , for: .touchUpInside)
    }
    
    @objc func sendLog(btn: DesignableButton) {
      print("Send Log")
    }
    
    func applyGradientButton(colours: [UIColor], cornerRadius : CGFloat) -> Void {
        
        
        self.gradLayer?.removeFromSuperlayer()
        
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = CGPoint(x: 0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1, y: 0.5)
        if cornerRadius > 0
        {
            gradient.cornerRadius = cornerRadius
        }
        
        self.gradLayer = gradient
        self.layer.insertSublayer(gradient, at: 0)
        
    }
    
    func applyRoundshadow(
        color: UIColor = .black,
        alpha: Float = 0.5,
        x: CGFloat = 0,
        y: CGFloat = 2,
        blur: CGFloat = 4,
        spread: CGFloat = 0,
        cornerRadius: CGFloat = 0)
    {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = alpha
        self.layer.shadowOffset = CGSize(width: x, height: y)
        self.layer.shadowRadius = blur / 2.0
        self.layer.masksToBounds =  false
        if spread == 0 {
            self.layer.shadowPath = nil
        } else {
            let dx = -spread
            let rect = self.layer.bounds.insetBy(dx: dx, dy: dx)
            self.layer.shadowPath = UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius).cgPath
        }

        
    }
    func removeGradient()
    {
        self.gradLayer?.removeFromSuperlayer()
        self.gradLayer = nil
    }
}
