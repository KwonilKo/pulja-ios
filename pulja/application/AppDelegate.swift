//
//  AppDelegate.swift
//  pulja
//
//  Created by jeonghoonlee on 2021/10/07.
//

import UIKit
import CoreData
import Firebase
import UserNotifications
import ChannelIOFront
import FlareLane
import AirBridge
import AppTrackingTransparency
import KakaoSDKCommon
import KakaoSDKAuth
import NaverThirdPartyLogin
@main
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    //    , SBDChannelDelegate {
    
    var window: UIWindow?
    
    //    send bird
    var receivedPushChannelUrl: String?
    var pushReceivedGroupChannel: String?
    
    
    var appApplication:UIApplication?
    var appLaunchOptions:[UIApplication.LaunchOptionsKey: Any]?
    var isAppLaunchOptionsCheck = false
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if !Const.isProd {
            AirBridge.setLogLevel(.LOG_ALL)
        }
        
        AirBridge.setting()?.trackingAuthorizeTimeout = 120 * 1000
        AirBridge.setting()?.isRestartTrackingAuthorizeTimeout = false
        
        // User 정보 Hash 화 해제
        AirBridge.setIsUserInfoHashed(false)
        AirBridge.getInstance(Const.AB_APP_TOKEN, appName:Const.AB_APP_NAME, withLaunchOptions:launchOptions)
        
        AirBridge.deeplink()?.setDeeplinkCallback({ deeplink in
            // 딥링크로 앱이 열리는 경우 작동할 코드
            // Airbridge 를 통한 Deeplink = YOUR_SCHEME://...
            NSLog("DeeplinkCallback : %@", deeplink)
        })
        
        appApplication = application
        appLaunchOptions = launchOptions
        
        FirebaseApp.configure()
        
        self.registerForRemoteNotification()
        
        ChannelIO.initialize(application)
        KakaoSDK.initSDK(appKey: "daaaeb0d8854dbaea3e22d9f6cd0a8a9")
        let instance = NaverThirdPartyLoginConnection.getSharedInstance()
        instance?.isNaverAppOauthEnable = true //네이버앱 로그인 설정
        instance?.isInAppOauthEnable = true //사파리 로그인 설정
        
        instance?.serviceUrlScheme = "pulja" //URL Scheme
        instance?.consumerKey = "hk4icSY38GICwlUrCiQT" //클라이언트 아이디
        instance?.consumerSecret = "G7ctRMkSQ8" //시크릿 아이디
        instance?.appName = "풀자" //앱이름
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        if let _launchOptions = launchOptions {
            if let userInfoRemote = _launchOptions[UIApplication.LaunchOptionsKey.remoteNotification] as? NSDictionary{
                CommonUtil.userInfoRemote = userInfoRemote
            }
        }
        
        return true
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func application(
        _ app: UIApplication,
        open url: URL,
        options: [UIApplication.OpenURLOptionsKey : Any] = [:]
    ) -> Bool {
        
        if (AuthApi.isKakaoTalkLoginUrl(url)) {
            return AuthController.handleOpenUrl(url: url)
        }
        else if url.absoluteString.contains("pulja://thirdPartyLoginResult")
        {
            NaverThirdPartyLoginConnection.getSharedInstance()?.application(app, open: url, options: options)
            return true
        }
        else
        {
            AirBridge.deeplink()?.handleURLSchemeDeeplink(url)
            return true
        }
    }
    
    func application(_ application: UIApplication,
                     continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool
    {
        if let webpageURL = userActivity.webpageURL {
            AirBridge.deeplink()?.handleUniversalLink(webpageURL)
        }
        
        return true
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "pulja")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func registerForRemoteNotification() {
        
#if targetEnvironment(simulator)
        
        guard Double(UIDevice.current.systemVersion)! >= 10.0 else { return }
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.requestAuthorization(options: [.alert]) { _, _ in }
        
#else
        
        guard let systemVersion = Double(UIDevice.current.systemVersion), systemVersion >= 10.0 else {
            guard UIApplication.shared.responds(to: #selector(UIApplication.registerUserNotificationSettings(_:))) else { return }
            let notificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(notificationSettings)
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
            return
        }
        
        
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.requestAuthorization(options: [.alert, .sound]) { granted, error in
            guard granted else { return }
            UNUserNotificationCenter.current().getNotificationSettings { settings in
                guard settings.authorizationStatus == .authorized else { return }
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
        
#endif
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let deviceTokenString = deviceToken.map { String(format: "%02x", $0) }.joined()
        //print("devicetoken : \(deviceTokenString)")
        
        AirBridge.registerPushToken(deviceToken)
        CommonUtil.shared.setDeviceToken(token: deviceToken)
        
        
        let userDefault = UserDefaults.standard
        userDefault.setValue(deviceTokenString, forKey: Const.UD_DIVICE_TOKEN)
        userDefault.synchronize()
        
        //            SBDMain.registerDevicePushToken(deviceToken, unique: false) { status, error in
        //
        //                switch status {
        //                case .success:
        //                    print("APNS Token is registered.")
        //                case .pending:
        //                    print("Push registration is pending.")
        //                case .error:
        //                    print("APNS registration failed with error: \(String(describing: error ?? nil))")
        //
        //                @unknown default:
        //                    print("Push registration: unknown default")
        //                    assertionFailure()
        //                }
        //
        //                print("FlareLane.initWithLaunchOptions")
        //                FlareLane.initWithLaunchOptions(self.appLaunchOptions, projectId: Const.FL_PROJECT_ID)
        //
        //
        //                //FlareLane init을 하면 delegate를 가로채기 때문에 AppDelegate에 관련 함수가 호출되지 않으므로
        //                //대리자(delegate)를 AppDelegate로 다시 지정해줘야한다.
        //                //2022.03.06 이정훈
        //                //do not remove next line
        //                UNUserNotificationCenter.current().delegate = self
        //
        //            }
        
        FlareLane.initWithLaunchOptions(self.appLaunchOptions, projectId: Const.FL_PROJECT_ID)
        
        //FlareLane init을 하면 delegate를 가로채기 때문에 AppDelegate에 관련 함수가 호출되지 않으므로
        //대리자(delegate)를 AppDelegate로 다시 지정해줘야한다.
        //2022.03.06 이정훈
        //do not remove next line
        UNUserNotificationCenter.current().delegate = self
        
        
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to get token, error: \(error)")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if let userInfo = response.notification.request.content.userInfo as? [String: Any],
           let isFlareLane = userInfo["isFlareLane"] as? Int ,
           let url = userInfo["url"] as? String,
           isFlareLane > 0
        {
            
            var target = ""
            var txproblemSeq = "0"
            var txpCnt = "0"
            let components = URLComponents(string: url)
            let items = components?.queryItems ?? []
            for item in items
            {
                if item.name == "target"
                {
                    target = item.value ?? ""
                }
                else if item.name == "problemSeq"
                {
                    txproblemSeq = item.value ?? "0"
                } else if item.name == "pCnt"
                {
                    txpCnt = item.value ?? "0"
                }
            }
            let problemSeq = Int(txproblemSeq) ?? 0
            let pCnt = Int(txpCnt) ?? 0
            
            //로그인 상태에서만 이동.
            if let _ = CommonUtil.shared.getAccessToken(),  target == "preparation" , problemSeq > 0
            {
                do {
                    let myInfos = try self.persistentContainer.viewContext.fetch(MyInfo.fetchRequest()) as! [MyInfo]
                    if myInfos.count > 0
                    {
                        let myInfo = myInfos[0]
                        let userSeq = myInfo.userSeq
                        
                        
                        if let vcDetail = R.storyboard.main.tabbarController() {
                            
                            let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDel.window?.rootViewController = vcDetail
                            vcDetail.selectedIndex = 0
                            vcDetail.bottomNavBar.selectedItem = vcDetail.bottomNavBar.items[0]
                            
                            // A mask of options indicating how you want to perform the animations.
                            let options: UIView.AnimationOptions = .transitionCrossDissolve
                            
                            // The duration of the transition animation, measured in seconds.
                            let duration: TimeInterval = 0.3
                            
                            // Creates a transition animation.
                            // Though `animations` is optional, the documentation tells us that it must not be nil. ¯\_(ツ)_/¯
                            UIView.transition(with: appDel.window!, duration: duration, options: options, animations: {}, completion:
                                                { completed in
                                // maybe do something on completion here
                            })
                            
                        }
                        
                    }
                } catch {
                    
                }
                completionHandler()
                return
            }
            
        }
        
        
        return // 채팅 막음.
    }
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        completionHandler()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        requestTrackingAuthorization()
    }
    
    // 추적 접근 요청 메서드
    private func requestTrackingAuthorization() {
        if #available(iOS 14, *) {
            if ATTrackingManager.trackingAuthorizationStatus == .notDetermined {
                ATTrackingManager.requestTrackingAuthorization(completionHandler: { _ in })
            }
        }
    }
    
}
