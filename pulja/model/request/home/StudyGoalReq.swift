//
//  StudyGoalReq.swift
//  pulja
//
//  Created by 김병헌 on 2022/02/08.
//

import Foundation


struct StudyGoalReq : Codable {
        let problemCnt : Int?
        let userSeq : Int?
}
