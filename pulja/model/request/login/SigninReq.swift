//
//  LoginInput.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on November 19, 2021

import Foundation

struct SigninReq : Codable {

        let fcmToken : String?
        let osType : String?
        let password : String?
        let userId : String?
        let uuid : String?


}
