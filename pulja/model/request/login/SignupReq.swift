//
//  SignupReq.swift
//  pulja
//
//  Created by 김병헌 on 2021/12/21.
//

import Foundation


struct SignupReq : Codable {

    var addr1 : String?
    var addr2 : String?
    var birth_day : String?
    var birth_month : String?
    var birth_year : String?
    var email_com : String?
    var email_id : String?
    var fcmToken : String?
    var gender : String?
    var marketAccept : String?
    var marketing_yn : String?
    var osType : String?
    var parentAccept : String?
    var parent_phone1 : String?
    var parent_phone2 : String?
    var parent_phone3 : String?
    var password : String?
    var phone1 : String?
    var phone2 : String?
    var phone3 : String?
    var post_code : String?
    var recommender_id : String?
    var reserveSeq : String?
    var school_grade : String?
    var school_id : String?
    var school_level : String?
    var school_name : String?
    var studentAccept : String?
    var userId : String?
    var user_type : String?
    var username : String?
    var uuid : String?


}
