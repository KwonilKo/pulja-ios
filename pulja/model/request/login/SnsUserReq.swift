//
//  SnsUserReq.swift
//  pulja
//
//  Created by 김병헌 on 2022/08/30.
//

import Foundation


struct SnsUserReq : Codable {

    var fcmToken : String?
    var marketAccept : String?
    let osType : String?
    let snsAppleCode : String?
    let snsEmail : String?
    let snsId : String?
    let snsNick : String?
    var snsToken : String?
    let snsType : String?
    var uuid : String?

}
