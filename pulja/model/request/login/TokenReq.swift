//
//  TokenReq.swift
//  pulja
//
//  Created by 김병헌 on 2021/11/22.
//

import Foundation

struct TokenReq : Codable {
    let accessToken : String
    let refreshToken : String
    let userSeq : Int
}
