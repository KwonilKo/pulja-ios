//
//  AuthCodeReq.swift
//  pulja
//
//  Created by 김병헌 on 2021/12/15.
//

import Foundation

struct AuthCodeReq : Codable {

    var cert : String?
    var email : String?
    var name : String?
    var phone : String?
    var service : String?
    var type : String?


}
