//
//  FcmTokenReq.swift
//  pulja
//
//  Created by 김병헌 on 2022/03/07.
//

import Foundation

struct FcmTokenReq : Codable {
    let fcmToken : String
    let osType : String
    let uuid : String
}
