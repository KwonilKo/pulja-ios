//
//  ErrorReportReq.swift
//  pulja
//
//  Created by 김병헌 on 2022/10/05.
//

import Foundation

struct ErrorReportReq : Codable {
    
    var errorType : String?
    var questionId : Int?
    var movieCode : String?
    var reportContent : String?
    var reportDevice : String?
    var userSeq : Int?
    
}
