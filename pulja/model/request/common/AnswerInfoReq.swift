//
//  AnswerInfoReq.swift
//  pulja
//
//  Created by 김병헌 on 2022/01/28.
//

import Foundation

struct AnswerInfoReq : Codable {
    
    var problemType : String?
    var questionId : Int?
    var solveSeq : Int?
    var userSeq : String?
    
}
