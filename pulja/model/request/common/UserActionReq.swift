//
//  UserActionReq.swift
//  pulja
//
//  Created by 김병헌 on 2022/04/08.
//

import Foundation

struct UserActionReq : Codable {
    
    var actionType : String?
    var cunitSeq : Int?
    var currSeq : Int?
    var movieCode : String?
    var question_id : Int?
    var userAnswer : String?
    var userSeq : Int?
    var vlecSeq : Int?
    
}
