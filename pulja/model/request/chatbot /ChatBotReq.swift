//
//  ChatBotReq.swift
//  pulja
//
//  Created by samdols on 2022/02/02.
//

import Foundation
struct ChatBotReq : Codable {
    var customData : String?
    
    /*
     study_time : 학습시간 설정 및 변경
     study_confirm : 학습 인증
     study_push1 : 학습 시작 시간 도달 시
     study_push2 : 학습 시작 시간 1시간 경과시
     study_push3 : 학습 시작 시간 3시간 경과 시
    */
    var customType : String?
    var lastSeq : String?
    var message : String?
    var messageType : String?
    var userSeq : String?
    
}
