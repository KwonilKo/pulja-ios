//
//  BookMarkReq.swift
//  pulja
//
//  Created by 김병헌 on 2022/01/20.
//

import Foundation


struct BookMarkReq : Codable {
    var bookMarkSeq : Int?
    var solveSeq : Int?
    var userSeq : Int?
    var problemType : String?
}
