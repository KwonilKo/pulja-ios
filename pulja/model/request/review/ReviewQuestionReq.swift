//
//  ReviewQuestionReq.swift
//  pulja
//
//  Created by 김병헌 on 2022/01/17.
//

import Foundation


struct ReviewQuestionReq : Codable {
    var endDate : String?
    var isCorrect : String?
    var page : Int?
    var startDate : String?
    var unit2Seqs : String?
    var userSeq : String?
}
