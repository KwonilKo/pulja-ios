//
//  review.swift
//  pulja
//
//  Created by 김병헌 on 2022/01/17.
//

import Foundation

struct ReviewQuestion : Codable {
    let isCorrect : String?
    let unit1Name : String?
    let subjectName : String?
    let categoryName : String?
    let unit2Name : String?
    let problemType : String?
    var bookMarkSeq : Int?
    let solveSeq : Int?
    let questionId : Int?
    let userSeq : Int?
    let movieCode : String?
    let regdate : String?

    enum CodingKeys: String, CodingKey {

        case isCorrect = "isCorrect"
        case unit1Name = "unit1Name"
        case subjectName = "subjectName"
        case categoryName = "categoryName"
        case unit2Name = "unit2Name"
        case problemType = "problemType"
        case bookMarkSeq = "bookMarkSeq"
        case solveSeq = "solveSeq"
        case questionId = "questionId"
        case userSeq = "userSeq"
        case movieCode = "movieCode"
        case regdate = "regdate"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isCorrect = try values.decodeIfPresent(String.self, forKey: .isCorrect)
        unit1Name = try values.decodeIfPresent(String.self, forKey: .unit1Name)
        subjectName = try values.decodeIfPresent(String.self, forKey: .subjectName)
        categoryName = try values.decodeIfPresent(String.self, forKey: .categoryName)
        unit2Name = try values.decodeIfPresent(String.self, forKey: .unit2Name)
        problemType = try values.decodeIfPresent(String.self, forKey: .problemType)
        bookMarkSeq = try values.decodeIfPresent(Int.self, forKey: .bookMarkSeq)
        solveSeq = try values.decodeIfPresent(Int.self, forKey: .solveSeq)
        questionId = try values.decodeIfPresent(Int.self, forKey: .questionId)
        userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
        movieCode = try values.decodeIfPresent(String.self, forKey: .movieCode)
        regdate = try values.decodeIfPresent(String.self, forKey: .regdate)
    }

}
