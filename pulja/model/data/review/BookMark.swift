//
//  BookMark.swift
//  pulja
//
//  Created by 김병헌 on 2022/01/20.
//

import Foundation


struct BookMark : Codable {
    let bookMarkSeq : Int?
    let solveSeq : Int?
    let userSeq : Int?
    let problemType : String?

    enum CodingKeys: String, CodingKey {

        case bookMarkSeq = "bookMarkSeq"
        case solveSeq = "solveSeq"
        case userSeq = "userSeq"
        case problemType = "problemType"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        bookMarkSeq = try values.decodeIfPresent(Int.self, forKey: .bookMarkSeq)
        solveSeq = try values.decodeIfPresent(Int.self, forKey: .solveSeq)
        userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
        problemType = try values.decodeIfPresent(String.self, forKey: .problemType)
    }

}
