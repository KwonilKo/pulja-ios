//
//  User.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on November 19, 2021

import Foundation

struct User : Codable {

        let addressMain : String?
        let addressSub : String?
        var birthday : String?
        let coachDesc : String?
        let coachProfile : String?
        let email : String?
        let gender : String?
        let logindate : String?
        let marketAccept : String?
        let mbEndDate : String?
        let mbStartDate : String?
        let memberStatus : String?
        let memberType : String?
        let memo : String?
        let moddate : String?
        let parentAccept : String?
        var parentsPhone : String?
        let paymentCheck : PaymentCheck?
        var phonenumber : String?
        let postCode : String?
        let profileUrl : String?
        let regdate : String?
        let repid : String?
        let resume : String?
        var schoolId : String?
        var schoolName : String?
        var schoolNum : Int?
        var schoolType : String?
        let studentAccept : String?
        let userId : String?
        let username : String?
        let userSeq : Int?
        let userType : String?
    let hashUserSeq : String?
    let  snsId : String?
    let  snsType : String?
    let  snsEmail : String?
    let  snsNick : String?
    var mockTestGrade: Int?

        enum CodingKeys: String, CodingKey {
                case addressMain = "addressMain"
                case addressSub = "addressSub"
                case birthday = "birthday"
                case coachDesc = "coachDesc"
                case coachProfile = "coachProfile"
                case email = "email"
                case gender = "gender"
                case logindate = "logindate"
                case marketAccept = "marketAccept"
                case mbEndDate = "mbEndDate"
                case mbStartDate = "mbStartDate"
                case memberStatus = "memberStatus"
                case memberType = "memberType"
                case memo = "memo"
                case moddate = "moddate"
                case parentAccept = "parentAccept"
                case parentsPhone = "parentsPhone"
                case paymentCheck = "paymentCheck"
                case phonenumber = "phonenumber"
                case postCode = "postCode"
                case profileUrl = "profileUrl"
                case regdate = "regdate"
                case repid = "repid"
                case resume = "resume"
                case schoolId = "schoolId"
                case schoolName = "schoolName"
                case schoolNum = "schoolNum"
                case schoolType = "schoolType"
                case studentAccept = "studentAccept"
                case userId = "userId"
                case username = "username"
                case userSeq = "userSeq"
                case userType = "userType"
                case hashUserSeq = "hashUserSeq"
            case snsId = "snsId"
            case snsType = "snsType"
            case snsEmail = "snsEmail"
            case snsNick = "snsNick"
            case mockTestGrade = "mockTestGrade"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                addressMain = try values.decodeIfPresent(String.self, forKey: .addressMain)
                addressSub = try values.decodeIfPresent(String.self, forKey: .addressSub)
                birthday = try values.decodeIfPresent(String.self, forKey: .birthday)
                coachDesc = try values.decodeIfPresent(String.self, forKey: .coachDesc)
                coachProfile = try values.decodeIfPresent(String.self, forKey: .coachProfile)
                email = try values.decodeIfPresent(String.self, forKey: .email)
                gender = try values.decodeIfPresent(String.self, forKey: .gender)
                logindate = try values.decodeIfPresent(String.self, forKey: .logindate)
                marketAccept = try values.decodeIfPresent(String.self, forKey: .marketAccept)
                mbEndDate = try values.decodeIfPresent(String.self, forKey: .mbEndDate)
                mbStartDate = try values.decodeIfPresent(String.self, forKey: .mbStartDate)
                memberStatus = try values.decodeIfPresent(String.self, forKey: .memberStatus)
                memberType = try values.decodeIfPresent(String.self, forKey: .memberType)
                memo = try values.decodeIfPresent(String.self, forKey: .memo)
                moddate = try values.decodeIfPresent(String.self, forKey: .moddate)
                parentAccept = try values.decodeIfPresent(String.self, forKey: .parentAccept)
                parentsPhone = try values.decodeIfPresent(String.self, forKey: .parentsPhone)
                paymentCheck = try PaymentCheck(from: decoder)
                phonenumber = try values.decodeIfPresent(String.self, forKey: .phonenumber)
                postCode = try values.decodeIfPresent(String.self, forKey: .postCode)
                profileUrl = try values.decodeIfPresent(String.self, forKey: .profileUrl)
                regdate = try values.decodeIfPresent(String.self, forKey: .regdate)
                repid = try values.decodeIfPresent(String.self, forKey: .repid)
                resume = try values.decodeIfPresent(String.self, forKey: .resume)
                schoolId = try values.decodeIfPresent(String.self, forKey: .schoolId)
                schoolName = try values.decodeIfPresent(String.self, forKey: .schoolName)
                schoolNum = try values.decodeIfPresent(Int.self, forKey: .schoolNum)
                schoolType = try values.decodeIfPresent(String.self, forKey: .schoolType)
                studentAccept = try values.decodeIfPresent(String.self, forKey: .studentAccept)
                userId = try values.decodeIfPresent(String.self, forKey: .userId)
                username = try values.decodeIfPresent(String.self, forKey: .username)
                userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
                userType = try values.decodeIfPresent(String.self, forKey: .userType)
            hashUserSeq =  try values.decodeIfPresent(String.self, forKey: .hashUserSeq)
            snsId =  try values.decodeIfPresent(String.self, forKey: .snsId)
            snsType =  try values.decodeIfPresent(String.self, forKey: .snsType)
            snsEmail =  try values.decodeIfPresent(String.self, forKey: .snsEmail)
            snsNick =  try values.decodeIfPresent(String.self, forKey: .snsNick)
            mockTestGrade =  try values.decodeIfPresent(Int.self, forKey: .mockTestGrade)
        }

}
