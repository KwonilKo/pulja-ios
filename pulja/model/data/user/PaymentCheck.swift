//
//  PaymentCheck.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on November 19, 2021

import Foundation

struct PaymentCheck : Codable {

    let expPayment : Bool?
    let payment : Bool?
    let paymentExpDay : Int?
    let userSeq : Int?
    let haveBuyHistory: Bool?

    enum CodingKeys: String, CodingKey {
        case expPayment = "expPayment"
        case payment = "payment"
        case paymentExpDay = "paymentExpDay"
        case userSeq = "userSeq"
        case haveBuyHistory = "haveBuyHistory"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        expPayment = try values.decodeIfPresent(Bool.self, forKey: .expPayment)
        payment = try values.decodeIfPresent(Bool.self, forKey: .payment)
        paymentExpDay = try values.decodeIfPresent(Int.self, forKey: .paymentExpDay)
        userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
        haveBuyHistory = try values.decodeIfPresent(Bool.self, forKey: .haveBuyHistory)
    }

}
