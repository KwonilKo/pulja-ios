//
//  SnsUserInfo.swift
//  pulja
//
//  Created by 김병헌 on 2022/08/30.
//

import Foundation

struct SnsUserInfo : Codable {
    let fcmToken : String?
    let marketAccept : String?
    let osType : String?
    let snsAppleCode : String?
    let snsEmail : String?
    let snsId : String?
    let snsNick : String?
    let snsToken : String?
    let snsType : String?
    let uuid : String?

    enum CodingKeys: String, CodingKey {

        case fcmToken = "fcmToken"
        case marketAccept = "marketAccept"
        case osType = "osType"
        case snsAppleCode = "snsAppleCode"
        case snsEmail = "snsEmail"
        case snsId = "snsId"
        case snsNick = "snsNick"
        case snsToken = "snsToken"
        case snsType = "snsType"
        case uuid = "uuid"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        fcmToken = try values.decodeIfPresent(String.self, forKey: .fcmToken)
        marketAccept = try values.decodeIfPresent(String.self, forKey: .marketAccept)
        osType = try values.decodeIfPresent(String.self, forKey: .osType)
        snsAppleCode = try values.decodeIfPresent(String.self, forKey: .snsAppleCode)
        snsEmail = try values.decodeIfPresent(String.self, forKey: .snsEmail)
        snsId = try values.decodeIfPresent(String.self, forKey: .snsId)
        snsNick = try values.decodeIfPresent(String.self, forKey: .snsNick)
        snsToken = try values.decodeIfPresent(String.self, forKey: .snsToken)
        snsType = try values.decodeIfPresent(String.self, forKey: .snsType)
        uuid = try values.decodeIfPresent(String.self, forKey: .uuid)
    }

}
