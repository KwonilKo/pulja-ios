//
//  ChatUser.swift
//  pulja
//
//  Created by 김병헌 on 2021/12/03.
//

import Foundation
struct ChatUser : Codable {
    let userSeq : Int?
    let userId : String?
    let email : String?
    let username : String?
    let phonenumber : String?
    let gender : String?
    let profileUrl : String?
    let hashUserSeq : String?

    enum CodingKeys: String, CodingKey {

        case userSeq = "userSeq"
        case userId = "userId"
        case email = "email"
        case username = "username"
        case phonenumber = "phonenumber"
        case gender = "gender"
        case profileUrl = "profileUrl"
        case hashUserSeq = "hashUserSeq"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
        userId = try values.decodeIfPresent(String.self, forKey: .userId)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        phonenumber = try values.decodeIfPresent(String.self, forKey: .phonenumber)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        profileUrl = try values.decodeIfPresent(String.self, forKey: .profileUrl)
        hashUserSeq = try values.decodeIfPresent(String.self, forKey: .hashUserSeq)
    }

}
