//
//  UnitInfo.swift
//  pulja
//
//  Created by 김병헌 on 2022/01/20.
//

import Foundation


struct UnitInfo : Codable, Equatable {
    
    let subjectSeq : Int?
    let unit1Name : String?
    let subjectName : String?
    let unit2Name : String?
    let unit1SName : String?
    let unit2SName : String?
    let unit1Seq : Int?
    let unit2Seq : Int?
    let classSeq : Int?

    enum CodingKeys: String, CodingKey {

        case subjectSeq = "subjectSeq"
        case unit1Name = "unit1Name"
        case subjectName = "subjectName"
        case unit2Name = "unit2Name"
        case unit1SName = "unit1SName"
        case unit2SName = "unit2SName"
        case unit1Seq = "unit1Seq"
        case unit2Seq = "unit2Seq"
        case classSeq = "classSeq"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        subjectSeq = try values.decodeIfPresent(Int.self, forKey: .subjectSeq)
        unit1Name = try values.decodeIfPresent(String.self, forKey: .unit1Name)
        subjectName = try values.decodeIfPresent(String.self, forKey: .subjectName)
        unit2Name = try values.decodeIfPresent(String.self, forKey: .unit2Name)
        unit1SName = try values.decodeIfPresent(String.self, forKey: .unit1SName)
        unit2SName = try values.decodeIfPresent(String.self, forKey: .unit2SName)
        unit1Seq = try values.decodeIfPresent(Int.self, forKey: .unit1Seq)
        unit2Seq = try values.decodeIfPresent(Int.self, forKey: .unit2Seq)
        classSeq = try values.decodeIfPresent(Int.self, forKey: .classSeq)
    }

}
