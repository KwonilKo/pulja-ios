//
//  SchoolUpdateInfo.swift
//  pulja
//
//  Created by HUN on 2023/02/01.
//

import Foundation

struct SchoolUpdateInfo : Codable {
    let statusCode : Int?
    let message : String?
    let data : String?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(String.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}
