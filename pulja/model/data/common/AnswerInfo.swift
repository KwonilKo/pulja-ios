//
//  AnswerInfo.swift
//  pulja
//
//  Created by 김병헌 on 2022/01/28.
//
import Foundation

struct AnswerInfo : Codable {
    let userSeq : Int?
    let currSeq : Int?
    let cunitSeq : Int?
    let questionId : Int?
    let solveSeq : Int?
    let problemType : String?
    let isCorrect : String?
    let userAnswer : String?
    let movieCode : String?
    let movieURL : String?
    let answerTxt : String?
    let explanationHtml : String?
    var choiceCount : Int?

    enum CodingKeys: String, CodingKey {

        case userSeq = "userSeq"
        case currSeq = "currSeq"
        case cunitSeq = "cunitSeq"
        case questionId = "questionId"
        case solveSeq = "solveSeq"
        case problemType = "problemType"
        case isCorrect = "isCorrect"
        case userAnswer = "userAnswer"
        case movieCode = "movieCode"
        case movieURL = "movieURL"
        case answerTxt = "answerTxt"
        case explanationHtml = "explanationHtml"
        case choiceCount = "choiceCount"
    }

    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
        currSeq = try values.decodeIfPresent(Int.self, forKey: .currSeq)
        cunitSeq = try values.decodeIfPresent(Int.self, forKey: .cunitSeq)
        questionId = try values.decodeIfPresent(Int.self, forKey: .questionId)
        solveSeq = try values.decodeIfPresent(Int.self, forKey: .solveSeq)
        problemType = try values.decodeIfPresent(String.self, forKey: .problemType)
        isCorrect = try values.decodeIfPresent(String.self, forKey: .isCorrect)
        userAnswer = try values.decodeIfPresent(String.self, forKey: .userAnswer)
        movieCode = try values.decodeIfPresent(String.self, forKey: .movieCode)
        movieURL = try values.decodeIfPresent(String.self, forKey: .movieURL)
        answerTxt = try values.decodeIfPresent(String.self, forKey: .answerTxt)
        explanationHtml = try values.decodeIfPresent(String.self, forKey: .explanationHtml)
        choiceCount = try values.decodeIfPresent(Int.self, forKey: .choiceCount)
        
    }

}
