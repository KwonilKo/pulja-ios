//
//  SubjectInfo.swift
//  pulja
//
//  Created by 김병헌 on 2022/01/21.
//

import Foundation

struct SubjectInfo : Codable {
    
    let subjectSeq : Int?
    let name : String?
    let classSeq : Int?

    enum CodingKeys: String, CodingKey {

        case subjectSeq = "subjectSeq"
        case name = "name"
        case classSeq = "classSeq"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        subjectSeq = try values.decodeIfPresent(Int.self, forKey: .subjectSeq)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        classSeq = try values.decodeIfPresent(Int.self, forKey: .classSeq)
    }

}


struct SubjectNameInfo : Codable {
    
    let subjectSeq : Int?
    let subjectName : String?
    let classSeq : Int?

    enum CodingKeys: String, CodingKey {

        case subjectSeq = "subjectSeq"
        case subjectName = "subjectName"
        case classSeq = "classSeq"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        subjectSeq = try values.decodeIfPresent(Int.self, forKey: .subjectSeq)
        subjectName = try values.decodeIfPresent(String.self, forKey: .subjectName)
        classSeq = try values.decodeIfPresent(Int.self, forKey: .classSeq)
    }

}


