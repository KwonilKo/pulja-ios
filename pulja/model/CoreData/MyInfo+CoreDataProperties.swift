//
//  User+CoreDataProperties.swift
//  
//
//  Created by 김병헌 on 2021/12/03.
//
//

import Foundation
import CoreData


extension MyInfo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MyInfo> {
        return NSFetchRequest<MyInfo>(entityName: "MyInfo")
    }

    @NSManaged public var userId: String?
    @NSManaged public var userType: String?
    @NSManaged public var userSeq: Int64
    @NSManaged public var username: String?
    @NSManaged public var hashUserSeq: String?
    @NSManaged public var payType: String?
    @NSManaged public var snsId: String?
    @NSManaged public var snsType: String?
    @NSManaged public var snsEmail: String?
    @NSManaged public var snsNick: String?
    
}
