//
//  StudyRes.swift
//  pulja
//
//  Created by jeonghoonlee on 2021/12/27.
//

import Foundation

struct DiagnosisHistoryListRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : [DiagnosisHistory]?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([DiagnosisHistory].self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct DiagnosisHistory : Codable {
    let recentTestDate : String?
    let diagnosisSeq : Int?
    
    enum CodingKeys: String, CodingKey {

        case recentTestDate = "recentTestDate"
        case diagnosisSeq = "diagnosisSeq"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        recentTestDate = try values.decodeIfPresent(String.self, forKey: .recentTestDate)
        diagnosisSeq = try values.decodeIfPresent(Int.self, forKey: .diagnosisSeq)
    }

}


struct CurriculumListRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : [CurriculumSimpleInfo]?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([CurriculumSimpleInfo].self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

//커리큘럼 중 오늘 할 커리큘럼.
struct CurriculumDetailRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : CurriculumDetail?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(CurriculumDetail.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct CurriculumNotDoneListRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : [CurriculumUnit]?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([CurriculumUnit].self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct CurriculumPrevListRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : [CurriculumUnit]?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([CurriculumUnit].self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

//학습하기 메인화면
struct CurriculumMain : Codable {
    let statusCode : Int?
    let message : String?
    let data : CurriculumSimpleMain?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(CurriculumSimpleMain.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}
struct CurriculumSimpleMain : Codable {
    let hasSevenFree : String?
    let isValidPay : String?
    let hasBuyHistory : String?
    let hasCurriculum : String?
    let curriculumList : [CurriculumSimpleInfo]?
    let restOfCurriculum : Int?
    let studyAlarm : String?
    let coachingRoomUrl : String?
    
    
    enum CodingKeys: String, CodingKey {

        case hasSevenFree = "hasSevenFree"
        case isValidPay = "isValidPay"
        case hasBuyHistory = "hasBuyHistory"
        case hasCurriculum = "hasCurriculum"
        case curriculumList = "curriculumList"
        case restOfCurriculum = "restOfCurriculum"
        case studyAlarm = "studyAlarm"
        case coachingRoomUrl = "coachingRoomUrl"
        
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        hasSevenFree = try values.decodeIfPresent(String.self, forKey: .hasSevenFree)
        isValidPay = try values.decodeIfPresent(String.self, forKey: .isValidPay)
        hasBuyHistory = try values.decodeIfPresent(String.self, forKey: .hasBuyHistory)
        hasCurriculum = try values.decodeIfPresent(String.self, forKey: .hasCurriculum)
        curriculumList = try values.decodeIfPresent([CurriculumSimpleInfo].self, forKey: .curriculumList)
        restOfCurriculum = try values.decodeIfPresent(Int.self, forKey: .restOfCurriculum)
        studyAlarm = try values.decodeIfPresent(String.self, forKey: .studyAlarm)
        coachingRoomUrl = try values.decodeIfPresent(String.self, forKey: .coachingRoomUrl)
       
    }

}
struct CurriculumSimpleInfo : Codable {
    let restVideoCnt : Int?
    let unit1Name : String?
    let subjectName : String?
    let restProblemCnt : Int?
    let isEndCurriculum : Int?
    let currSeq : Int?
    
    
    enum CodingKeys: String, CodingKey {

        case restVideoCnt = "restVideoCnt"
        case unit1Name = "unit1Name"
        case subjectName = "subjectName"
        case restProblemCnt = "restProblemCnt"
        case isEndCurriculum = "isEndCurriculum"
        case currSeq = "currSeq"
        
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        restVideoCnt = try values.decodeIfPresent(Int.self, forKey: .restVideoCnt)
        unit1Name = try values.decodeIfPresent(String.self, forKey: .unit1Name)
        subjectName = try values.decodeIfPresent(String.self, forKey: .subjectName)
        restProblemCnt = try values.decodeIfPresent(Int.self, forKey: .restProblemCnt)
        isEndCurriculum = try values.decodeIfPresent(Int.self, forKey: .isEndCurriculum)
        currSeq = try values.decodeIfPresent(Int.self, forKey: .currSeq)
       
    }
    
   

}







struct CurriculumDetail : Codable {
    let notDoneStudyAmount : NotDoneStudyAmounts?
    let todayCurriculumUnit : [CurriculumUnit]?
    let curriculumInfo : CurriculumInfo?

    enum CodingKeys: String, CodingKey {

        case notDoneStudyAmount = "notDoneStudyAmount"
        case todayCurriculumUnit = "todayCurriculumUnit"
        case curriculumInfo = "curriculumInfo"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        notDoneStudyAmount = try values.decodeIfPresent(NotDoneStudyAmounts.self, forKey: .notDoneStudyAmount)
        todayCurriculumUnit = try values.decodeIfPresent([CurriculumUnit].self, forKey: .todayCurriculumUnit)
        curriculumInfo = try values.decodeIfPresent(CurriculumInfo.self, forKey: .curriculumInfo)
    }

}

struct CurriculumInfo : Codable {
    let currSeq : Int?
    let subjectName : String?
    let unit1Name : String?
    let restVideoCnt : Int?
    let isEndCurriculum : Int?
    let restProblemCnt : Int?
    let gradeName : String?
    let level : Int?
    let currTypeName : String?
    
    enum CodingKeys: String, CodingKey {

        case currSeq = "currSeq"
        case subjectName = "subjectName"
        case unit1Name = "unit1Name"
        case restVideoCnt = "restVideoCnt"
        case isEndCurriculum = "isEndCurriculum"
        case restProblemCnt = "restProblemCnt"
        case gradeName = "gradeName"
        case level = "level"
        case currTypeName = "currTypeName"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        currSeq = try values.decodeIfPresent(Int.self, forKey: .currSeq)
        subjectName = try values.decodeIfPresent(String.self, forKey: .subjectName)
        unit1Name = try values.decodeIfPresent(String.self, forKey: .unit1Name)
        restVideoCnt = try values.decodeIfPresent(Int.self, forKey: .restVideoCnt)
        isEndCurriculum = try values.decodeIfPresent(Int.self, forKey: .isEndCurriculum)
        restProblemCnt = try values.decodeIfPresent(Int.self, forKey: .restProblemCnt)
        gradeName = try values.decodeIfPresent(String.self, forKey: .gradeName)
        level = try values.decodeIfPresent(Int.self, forKey: .level)
        currTypeName = try values.decodeIfPresent(String.self, forKey: .currTypeName)
    }

}

struct CurriculumUnit : Codable {
    let cunitSeq : Int?
    let currSeq : Int?
    let cuOrder : Int?
    let studyDay : String?
    let isUnit1Test : String?
    let currUnitName : String?
    let studyStatus : String?
    let studyStatusName : String?
    let videoCnt : Int?
    let problemCnt : Int?

    enum CodingKeys: String, CodingKey {

        case cunitSeq = "cunitSeq"
        case currSeq = "currSeq"
        case cuOrder = "cuOrder"
        case studyDay = "studyDay"
        case isUnit1Test = "isUnit1Test"
        case currUnitName = "currUnitName"
        case studyStatus = "studyStatus"
        case studyStatusName = "studyStatusName"
        case videoCnt = "videoCnt"
        case problemCnt = "problemCnt"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        cunitSeq = try values.decodeIfPresent(Int.self, forKey: .cunitSeq)
        currSeq = try values.decodeIfPresent(Int.self, forKey: .currSeq)
        cuOrder = try values.decodeIfPresent(Int.self, forKey: .cuOrder)
        studyDay = try values.decodeIfPresent(String.self, forKey: .studyDay)
        isUnit1Test = try values.decodeIfPresent(String.self, forKey: .isUnit1Test)
        currUnitName = try values.decodeIfPresent(String.self, forKey: .currUnitName)
        studyStatus = try values.decodeIfPresent(String.self, forKey: .studyStatus)
        studyStatusName = try values.decodeIfPresent(String.self, forKey: .studyStatusName)
        videoCnt = try values.decodeIfPresent(Int.self, forKey: .videoCnt)
        problemCnt = try values.decodeIfPresent(Int.self, forKey: .problemCnt)
    }

}

struct NotDoneStudyAmounts : Codable {
    let videoCnt : Int?
    let problemCnt : Int?

    enum CodingKeys: String, CodingKey {

        case videoCnt = "videoCnt"
        case problemCnt = "problemCnt"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        videoCnt = try values.decodeIfPresent(Int.self, forKey: .videoCnt)
        problemCnt = try values.decodeIfPresent(Int.self, forKey: .problemCnt)
    }

}


struct StudyStepRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : StudyStep?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(StudyStep.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct StudyStep : Codable {
    let answerCnt: Int?
    let cunitSeq : Int?
    let currSeq : Int?
    let title : String?
    let questionId : Int?
    let problemUrl : String?
    let problemType : String?
    let number : Int?
    let totalCnt : Int?
    let last : Bool?
    let userAnswer: String?

    enum CodingKeys: String, CodingKey {
        case answerCnt = "answerCnt"
        case cunitSeq = "cunitSeq"
        case currSeq = "currSeq"
        case title = "title"
        case questionId = "questionId"
        case problemUrl = "problemUrl"
        case problemType = "problemType"
        case number = "number"
        case totalCnt = "totalCnt"
        case last = "last"
        case userAnswer = "userAnswer"
       
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        answerCnt = try values.decodeIfPresent(Int.self, forKey: .answerCnt)
        cunitSeq = try values.decodeIfPresent(Int.self, forKey: .cunitSeq)
        currSeq = try values.decodeIfPresent(Int.self, forKey: .currSeq)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        questionId = try values.decodeIfPresent(Int.self, forKey: .questionId)
        problemUrl = try values.decodeIfPresent(String.self, forKey: .problemUrl)
        problemType = try values.decodeIfPresent(String.self, forKey: .problemType)
        number = try values.decodeIfPresent(Int.self, forKey: .number)
        totalCnt = try values.decodeIfPresent(Int.self, forKey: .totalCnt)
        last = try values.decodeIfPresent(Bool.self, forKey: .last)
        userAnswer = try values.decodeIfPresent(String.self, forKey: .userAnswer)
        
    }

}



struct StudyVideoRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : [StudyVideo]?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([StudyVideo].self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct StudyVideo : Codable {
    let cateName : String?
    let cunitSeq : Int?
    var currentDuration : Int?
    var maxDuration : Int?
    let isNextStudy : String?
    let title : String?
    let videoDuration : Int?
    let videoKey : String?
    let videoUrl : String?
    let vlecSeq : Int?
    let vorder : Int?

    enum CodingKeys: String, CodingKey {

        case cateName = "cateName"
        case cunitSeq = "cunitSeq"
        case currentDuration = "currentDuration"
        case maxDuration = "maxDuration"
        case isNextStudy = "isNextStudy"
        case title = "title"
        case videoDuration = "videoDuration"
        case videoKey = "videoKey"
        case videoUrl = "videoUrl"
        case vlecSeq = "vlecSeq"
        case vorder = "vorder"
       
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        cateName = try values.decodeIfPresent(String.self, forKey: .cateName)
        cunitSeq = try values.decodeIfPresent(Int.self, forKey: .cunitSeq)
        currentDuration = try values.decodeIfPresent(Int.self, forKey: .currentDuration)
        maxDuration = try values.decodeIfPresent(Int.self, forKey: .maxDuration)
        isNextStudy = try values.decodeIfPresent(String.self, forKey: .isNextStudy)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        videoDuration = try values.decodeIfPresent(Int.self, forKey: .videoDuration)
        videoKey = try values.decodeIfPresent(String.self, forKey: .videoKey)
        videoUrl = try values.decodeIfPresent(String.self, forKey: .videoUrl)
        vlecSeq = try values.decodeIfPresent(Int.self, forKey: .vlecSeq)
        vorder = try values.decodeIfPresent(Int.self, forKey: .vorder)
    }

}

struct CoachingTestDetailRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : [CoachingTestDetail]?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([CoachingTestDetail].self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}
struct CoachingTestDetail : Codable {
    let studyDay : String?
    let studyStatus : String?
    let problemCount : Int?
    
    enum CodingKeys: String, CodingKey {

        case studyDay = "studyDay"
        case studyStatus = "studyStatus"
        case problemCount = "problemCount"
       
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        studyDay = try values.decodeIfPresent(String.self, forKey: .studyDay)
        studyStatus = try values.decodeIfPresent(String.self, forKey: .studyStatus)
        problemCount = try values.decodeIfPresent(Int.self, forKey: .problemCount)
        
    }

}

struct CurriculumStudyRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : CurriculumStudy?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(CurriculumStudy.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct CurriculumStudy : Codable {
    let thisWeekStudy : ThisWeekStudy?
    let lastWeekStudy : LastWeekStudy?
    let nowStudy : NowStudy?
    
    enum CodingKeys: String, CodingKey {

        case thisWeekStudy = "thisWeekStudy"
        case lastWeekStudy = "lastWeekStudy"
        case nowStudy = "nowStudy"
       
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        thisWeekStudy = try values.decodeIfPresent(ThisWeekStudy.self, forKey: .thisWeekStudy)
        lastWeekStudy = try values.decodeIfPresent(LastWeekStudy.self, forKey: .lastWeekStudy)
        nowStudy = try values.decodeIfPresent(NowStudy.self, forKey: .nowStudy)
        
    }

}

struct ThisWeekStudy : Codable {
    let problemCnt : Int?
    let correctCnt : Int?
    enum CodingKeys: String, CodingKey {
        case problemCnt = "problemCnt"
        case correctCnt = "correctCnt"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        problemCnt = try values.decodeIfPresent(Int.self, forKey: .problemCnt)
        correctCnt = try values.decodeIfPresent(Int.self, forKey: .correctCnt)
    }
}

struct LastWeekStudy : Codable {
    let problemCnt : Int?
    let correctCnt : Int?
    
    enum CodingKeys: String, CodingKey {
        case problemCnt = "problemCnt"
        case correctCnt = "correctCnt"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        problemCnt = try values.decodeIfPresent(Int.self, forKey: .problemCnt)
        correctCnt = try values.decodeIfPresent(Int.self, forKey: .correctCnt)
    }

}


struct NowStudy : Codable {
    let problemCnt : Int?
    let studyTime : Int?
    let cunitSeq : Int?
    let correctCnt : Int?
    
    enum CodingKeys: String, CodingKey {
        case problemCnt = "problemCnt"
        case studyTime = "studyTime"
        case cunitSeq = "cunitSeq"
        case correctCnt = "correctCnt"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        problemCnt = try values.decodeIfPresent(Int.self, forKey: .problemCnt)
        studyTime = try values.decodeIfPresent(Int.self, forKey: .studyTime)
        cunitSeq = try values.decodeIfPresent(Int.self, forKey: .cunitSeq)
        correctCnt = try values.decodeIfPresent(Int.self, forKey: .correctCnt)
    }

}


struct DiagnosisSurveryRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : DiagnosisSurvey?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(DiagnosisSurvey.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}


struct DiagnosisSurvey : Codable {
    let pollSeq : Int?
    let userSeq : Int?
    let pollUserid : String?
    let schoolGrade : String?
    let recentScore : Int?
    let recentGrade : Int?
    let goalScore : Int?
    let goalGrade : Int?
    let status: String?
    let testSeq: Int?
    let diagnosisSeq: Int?
    let regdate: String?
    
    enum CodingKeys: String, CodingKey {
        case pollSeq = "pollSeq"
        case userSeq = "userSeq"
        case pollUserid = "pollUserid"
        case schoolGrade = "schoolGrade"
        case recentScore = "recentScore"
        case recentGrade = "recentGrade"
        case goalScore = "goalScore"
        case goalGrade = "goalGrade"
        case status = "status"
        case testSeq = "testSeq"
        case diagnosisSeq = "diagnosisSeq"
        case regdate = "regdate"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        pollSeq = try values.decodeIfPresent(Int.self, forKey: .pollSeq)
        userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
        pollUserid = try values.decodeIfPresent(String.self, forKey: .pollUserid)
        schoolGrade = try values.decodeIfPresent(String.self, forKey: .schoolGrade)
        recentScore = try values.decodeIfPresent(Int.self, forKey: .recentScore)
        recentGrade = try values.decodeIfPresent(Int.self, forKey: .recentGrade)
        goalScore = try values.decodeIfPresent(Int.self, forKey: .goalScore)
        goalGrade = try values.decodeIfPresent(Int.self, forKey: .goalGrade)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        testSeq = try values.decodeIfPresent(Int.self, forKey: .testSeq)
        diagnosisSeq = try values.decodeIfPresent(Int.self, forKey: .diagnosisSeq)
        regdate = try values.decodeIfPresent(String.self, forKey: .regdate)
    }

}

struct DiagnosisProbRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : DiagnosisProb?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(DiagnosisProb.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}


struct DiagnosisProb : Codable {
    let diagnosisInput : DiagnosisInput?
    let problem : DiagnosisProblem?
    let isEnd : String?
    
    
    enum CodingKeys: String, CodingKey {
        case diagnosisInput = "diagnosisInput"
        case problem = "problem"
        case isEnd = "isEnd"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        diagnosisInput = try values.decodeIfPresent(DiagnosisInput.self, forKey: .diagnosisInput)
        problem = try values.decodeIfPresent(DiagnosisProblem.self, forKey: .problem)
        isEnd = try values.decodeIfPresent(String.self, forKey: .isEnd)
    }
}


struct DiagnosisInput : Codable {
    let diagnosisSeq : Int?
    let nodeSeq : Int?
    let stepCount : Int?
    let testSeq : Int?
    let number : Int?
    
    
    enum CodingKeys: String, CodingKey {
        case diagnosisSeq = "diagnosisSeq"
        case nodeSeq = "nodeSeq"
        case stepCount = "stepCount"
        case testSeq = "testSeq"
        case number = "number"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        diagnosisSeq = try values.decodeIfPresent(Int.self, forKey: .diagnosisSeq)
        nodeSeq = try values.decodeIfPresent(Int.self, forKey: .nodeSeq)
        stepCount = try values.decodeIfPresent(Int.self, forKey: .stepCount)
        testSeq = try values.decodeIfPresent(Int.self, forKey: .testSeq)
        number = try values.decodeIfPresent(Int.self, forKey: .number)
    }
}

struct DiagnosisProblem : Codable {
    let questionId : Int?
    let problemUrl : String?
    let problemType : String?
    let answerCnt : Int?
    
    enum CodingKeys: String, CodingKey {
        case questionId = "questionId"
        case problemUrl = "problemUrl"
        case problemType = "problemType"
        case answerCnt = "answerCnt"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        questionId = try values.decodeIfPresent(Int.self, forKey: .questionId)
        problemUrl = try values.decodeIfPresent(String.self, forKey: .problemUrl)
        problemType = try values.decodeIfPresent(String.self, forKey: .problemType)
        answerCnt = try values.decodeIfPresent(Int.self, forKey: .answerCnt)
       
    }
}


struct Diagnosis : Codable {
    let statusCode : Int?
    let message : String?
    let data : DiagnosisProb?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(DiagnosisProb.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}



struct DiagnosisFinishRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : DiagnosisFinish?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(DiagnosisFinish.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct DiagnosisFinish : Codable {
    let diagnosisName : String?
    let totalDuration : Double?
    let targetAverageDuration : Double?
    
    let conceptListGroup : [DiagnosisContent]?
    
    let currGrade : Int?
    let targetGrade : Int?
    let targetAvgConceptPoint : Double?
    let averageDuration : Double?
    let targetTotalDuration : Double?
    let diagnosisSeq : Int?
    
    let targetConceptListGroup: [DiagnosisContent]?
    
    let avgConceptPoint : Double?
    let testSeq : Int?
    let correctRate : Double?
    let recommendCurriculums : [RecCurriculum]?
    let testDate : String?
    
    enum CodingKeys: String, CodingKey {
        case diagnosisName = "diagnosisName"
        case totalDuration = "totalDuration"
        case targetAverageDuration = "targetAverageDuration"
        case conceptListGroup = "conceptListGroup"
        
        case currGrade = "currGrade"
        case targetGrade = "targetGrade"
        case targetAvgConceptPoint = "targetAvgConceptPoint"
        case averageDuration = "averageDuration"
        case targetTotalDuration = "targetTotalDuration"
        case diagnosisSeq = "diagnosisSeq"
        
        case targetConceptListGroup = "targetConceptListGroup"
        case avgConceptPoint = "avgConceptPoint"
        case testSeq = "testSeq"
        case correctRate = "correctRate"
        case recommendCurriculums = "recommendCurriculums"
        case testDate = "testDate"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        diagnosisName = try values.decodeIfPresent(String.self, forKey: .diagnosisName)
        totalDuration = try values.decodeIfPresent(Double.self, forKey: .totalDuration)
        targetAverageDuration = try values.decodeIfPresent(Double.self, forKey: .targetAverageDuration)
        conceptListGroup = try values.decodeIfPresent([DiagnosisContent].self, forKey: .conceptListGroup)
        
        currGrade = try values.decodeIfPresent(Int.self, forKey: .currGrade)
        targetGrade = try values.decodeIfPresent(Int.self, forKey: .targetGrade)
        
        targetAvgConceptPoint = try values.decodeIfPresent(Double.self, forKey: .targetAvgConceptPoint)
        averageDuration = try values.decodeIfPresent(Double.self, forKey: .averageDuration)
        targetTotalDuration = try values.decodeIfPresent(Double.self, forKey: .targetTotalDuration)
        diagnosisSeq = try values.decodeIfPresent(Int.self, forKey: .diagnosisSeq)
        
        targetConceptListGroup = try values.decodeIfPresent([DiagnosisContent].self, forKey: .targetConceptListGroup)
        avgConceptPoint = try values.decodeIfPresent(Double.self, forKey: .avgConceptPoint)
        testSeq = try values.decodeIfPresent(Int.self, forKey: .testSeq)
        correctRate = try values.decodeIfPresent(Double.self, forKey: .correctRate)
        recommendCurriculums = try values.decodeIfPresent([RecCurriculum].self, forKey: .recommendCurriculums)
        testDate = try values.decodeIfPresent(String.self, forKey: .testDate)
    }

}


struct DiagnosisContent : Codable {
    let unit1name : String?
    let diagnosisUserConceptIndexList : [DiagnosisUserConceptIndexList]?
    
    enum CodingKeys: String, CodingKey {
        case unit1name = "unit1name"
        case diagnosisUserConceptIndexList = "diagnosisUserConceptIndexList"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        unit1name = try values.decodeIfPresent(String.self, forKey: .unit1name)
        diagnosisUserConceptIndexList = try values.decodeIfPresent([DiagnosisUserConceptIndexList].self, forKey: .diagnosisUserConceptIndexList)
       
    }
}

struct DiagnosisUserConceptIndexList : Codable {
    let conceptSeq : Int?
    let score : Double?
    let name : String?
    let unit1name : String?
    let unit1Seq : Int?
    let unit2Seq : Int?
    
    enum CodingKeys: String, CodingKey {
        case conceptSeq = "conceptSeq"
        case score = "score"
        case name = "name"
        case unit1name = "unit1name"
        case unit1Seq = "unit1Seq"
        case unit2Seq = "unit2Seq"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        conceptSeq = try values.decodeIfPresent(Int.self, forKey: .conceptSeq)
        score = try values.decodeIfPresent(Double.self, forKey: .score)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        unit1name = try values.decodeIfPresent(String.self, forKey: .unit1name)
        unit1Seq = try values.decodeIfPresent(Int.self, forKey: .unit1Seq)
        unit2Seq = try values.decodeIfPresent(Int.self, forKey: .unit2Seq)
       
    }
}

struct RecCurriculum : Codable {
    let diagnosisSeq : Int?
    let currSeq : Int?
    let grade : Double?
    let currName : String?
    let recommendDesc : String?
    
    enum CodingKeys: String, CodingKey {
        case diagnosisSeq = "diagnosisSeq"
        case currSeq = "currSeq"
        case grade = "grade"
        case currName = "currName"
        case recommendDesc = "recommendDesc"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        diagnosisSeq = try values.decodeIfPresent(Int.self, forKey: .diagnosisSeq)
        currSeq = try values.decodeIfPresent(Int.self, forKey: .currSeq)
        grade = try values.decodeIfPresent(Double.self, forKey: .grade)
        currName = try values.decodeIfPresent(String.self, forKey: .currName)
        recommendDesc = try values.decodeIfPresent(String.self, forKey: .recommendDesc)
    }
}

struct CoachingTestRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : CoachingTest?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(CoachingTest.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct CoachingTest : Codable {
    let addSeq : Int?
    let studyDay : String?
    let title : String?
    let questionId : Int?
    let problemUrl : String?
    let problemType : String?
    let answerCnt : Int?
    let number : Int?
    let totalCnt : Int?
    let userAnswer : String?
    let last: Bool?
    
    enum CodingKeys: String, CodingKey {
        case addSeq = "addSeq"
        case studyDay = "studyDay"
        case title = "title"
        case questionId = "questionId"
        case problemUrl = "problemUrl"
        case problemType = "problemType"
        case answerCnt = "answerCnt"
        case number = "number"
        case totalCnt = "totalCnt"
        case userAnswer = "userAnswer"
        case last = "last"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        addSeq = try values.decodeIfPresent(Int.self, forKey: .addSeq)
        studyDay = try values.decodeIfPresent(String.self, forKey: .studyDay)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        questionId = try values.decodeIfPresent(Int.self, forKey: .questionId)
        problemUrl = try values.decodeIfPresent(String.self, forKey: .problemUrl)
        problemType = try values.decodeIfPresent(String.self, forKey: .problemType)
        answerCnt = try values.decodeIfPresent(Int.self, forKey: .answerCnt)
        number = try values.decodeIfPresent(Int.self, forKey: .number)
        totalCnt = try values.decodeIfPresent(Int.self, forKey: .totalCnt)
        userAnswer = try values.decodeIfPresent(String.self, forKey: .userAnswer)
        last = try values.decodeIfPresent(Bool.self, forKey: .last)
    }
}

struct WeeklyReportRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : WeeklyReport?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(WeeklyReport.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }
}

struct WeeklyReport : Codable {
    let seq : Int?
    let userSeq : Int?
    let actionType : String?
    let actionData : String?
    let regdate : String?
    
    enum CodingKeys: String, CodingKey {
        case seq = "seq"
        case userSeq = "userSeq"
        case actionType = "actionType"
        case actionData = "actionData"
        case regdate = "regdate"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        seq = try values.decodeIfPresent(Int.self, forKey: .seq)
        userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
        actionType = try values.decodeIfPresent(String.self, forKey: .actionType)
        actionData = try values.decodeIfPresent(String.self, forKey: .actionData)
        regdate = try values.decodeIfPresent(String.self, forKey: .regdate)
    }
}

