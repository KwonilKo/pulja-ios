//
//  QuestionsRes.swift
//  pulja
//
//  Created by 김병헌 on 2022/02/19.
//

import Foundation

struct QuestionsRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : [ReviewQuestion]?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([ReviewQuestion].self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct Question : Codable {
    let movieUrl : String?
    let categoryName : String?
    let unit2Name : String?
    let subjectName : String?
    let explainUrl : String?
    let problemUrl : String?
    let userSeq : Int?
    let unit1Name : String?
    let questionId : Int?
    let bookMarkSeq : String?
    let problemType : String?
    let solveSeq : Int?
    let movieCode : String?
    let regdate : String?
    let isCorrect : String?

    enum CodingKeys: String, CodingKey {

        case movieUrl = "movieUrl"
        case categoryName = "categoryName"
        case unit2Name = "unit2Name"
        case subjectName = "subjectName"
        case explainUrl = "explainUrl"
        case problemUrl = "problemUrl"
        case userSeq = "userSeq"
        case unit1Name = "unit1Name"
        case questionId = "questionId"
        case bookMarkSeq = "bookMarkSeq"
        case problemType = "problemType"
        case solveSeq = "solveSeq"
        case movieCode = "movieCode"
        case regdate = "regdate"
        case isCorrect = "isCorrect"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        movieUrl = try values.decodeIfPresent(String.self, forKey: .movieUrl)
        categoryName = try values.decodeIfPresent(String.self, forKey: .categoryName)
        unit2Name = try values.decodeIfPresent(String.self, forKey: .unit2Name)
        subjectName = try values.decodeIfPresent(String.self, forKey: .subjectName)
        explainUrl = try values.decodeIfPresent(String.self, forKey: .explainUrl)
        problemUrl = try values.decodeIfPresent(String.self, forKey: .problemUrl)
        userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
        unit1Name = try values.decodeIfPresent(String.self, forKey: .unit1Name)
        questionId = try values.decodeIfPresent(Int.self, forKey: .questionId)
        bookMarkSeq = try values.decodeIfPresent(String.self, forKey: .bookMarkSeq)
        problemType = try values.decodeIfPresent(String.self, forKey: .problemType)
        solveSeq = try values.decodeIfPresent(Int.self, forKey: .solveSeq)
        movieCode = try values.decodeIfPresent(String.self, forKey: .movieCode)
        regdate = try values.decodeIfPresent(String.self, forKey: .regdate)
        isCorrect = try values.decodeIfPresent(String.self, forKey: .isCorrect)
    }

}
