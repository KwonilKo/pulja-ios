//
//  ChatBotRes.swift
//  pulja
//
//  Created by samdols on 2022/01/30.
//

import Foundation
struct MessageRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : [ChatBotData]?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([ChatBotData].self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}
struct StudyTimeRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : Bool?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(Bool.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct ChatBotData : Codable {
    let messageSeq : Int?
    let messageType : String?
    let message : String?
    let customType : String?
    let customData : String?
    let userSeq : Int?
    let regdate : String?
    let user : Bool?

    enum CodingKeys: String, CodingKey {
 
        case messageSeq = "messageSeq"
        case messageType = "messageType"
        case message = "message"
        case customType = "customType"
        case customData = "customData"
        case userSeq = "userSeq"
        case regdate = "regdate"
        case user = "user"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        messageSeq = try values.decodeIfPresent(Int.self, forKey: .messageSeq)
        messageType = try values.decodeIfPresent(String.self, forKey: .messageType)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        customType = try values.decodeIfPresent(String.self, forKey: .customType)
        customData = try values.decodeIfPresent(String.self, forKey: .customData)
        userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
        regdate = try values.decodeIfPresent(String.self, forKey: .regdate)
        user = try values.decodeIfPresent(Bool.self, forKey: .user)
    }

}
