//
//  SignupRes.swift
//  pulja
//
//  Created by 김병헌 on 2021/12/24.
//

import Foundation

struct SignupRes  : Codable {

        let data : User?
        let message : String?
        let statusCode : Int?
        let success : Bool?

        enum CodingKeys: String, CodingKey {
                case data = "data"
                case message = "message"
                case statusCode = "statusCode"
                case success = "success"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                data = try values.decodeIfPresent(User.self, forKey: .data)
                message = try values.decodeIfPresent(String.self, forKey: .message)
                statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
                success = try values.decodeIfPresent(Bool.self, forKey: .success)
        }

}
