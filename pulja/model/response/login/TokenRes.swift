//
//  TokenRes.swift
//  pulja
//
//  Created by 김병헌 on 2021/11/22.
//

import Foundation

struct TokenRes   : Codable {

        let data : Token?
        let message : String?
        let statusCode : Int?
        let success : Bool?

        enum CodingKeys: String, CodingKey {
                case data = "data"
                case message = "message"
                case statusCode = "statusCode"
                case success = "success"
        }
    
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            data = try values.decodeIfPresent(Token.self, forKey: .data)
            message = try values.decodeIfPresent(String.self, forKey: .message)
            statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
            success = try values.decodeIfPresent(Bool.self, forKey: .success)
        }

}


struct Token : Codable {

        let accessToken : String?
        let refreshToken : String?
        let userSeq : Int?

        enum CodingKeys: String, CodingKey {
                case accessToken = "accessToken"
                case refreshToken = "refreshToken"
                case userSeq = "userSeq"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                accessToken = try values.decodeIfPresent(String.self, forKey: .accessToken)
                refreshToken = try values.decodeIfPresent(String.self, forKey: .refreshToken)
                userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
        }

}
