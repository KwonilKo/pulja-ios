//
//  SigninRes.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on November 19, 2021

import Foundation

struct SigninRes  : Codable {

        let data : Signin?
        let message : String?
        let statusCode : Int?
        let success : Bool?

        enum CodingKeys: String, CodingKey {
                case data = "data"
                case message = "message"
                case statusCode = "statusCode"
                case success = "success"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                data = try values.decodeIfPresent(Signin.self, forKey: .data)
                message = try values.decodeIfPresent(String.self, forKey: .message)
                statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
                success = try values.decodeIfPresent(Bool.self, forKey: .success)
        }

}


struct Signin : Codable {

        let accessToken : String?
        let refreshToken : String?
        let user : User?

        enum CodingKeys: String, CodingKey {
                case accessToken = "accessToken"
                case refreshToken = "refreshToken"
                case user = "user"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                accessToken = try values.decodeIfPresent(String.self, forKey: .accessToken)
                refreshToken = try values.decodeIfPresent(String.self, forKey: .refreshToken)
                user = try values.decodeIfPresent(User.self, forKey: .user)
        }

}


struct response_naver_login: Codable {
 
    let resultcode: String?
    let message: String?
    let response: NaverLoginResponse?
    
    enum CodingKeys: String, CodingKey {
            case resultcode = "resultcode"
            case message = "message"
            case response = "response"
    }

    init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
        resultcode = try values.decodeIfPresent(String.self, forKey: .resultcode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        response = try values.decodeIfPresent(NaverLoginResponse.self, forKey: .response)
    }
    
}

struct NaverLoginResponse: Codable {
  
  let id: String?
  /// 사용자 별명
  let nickname: String?
  /// 사용자 이름
  let name: String?
  /// 사용자 메일 주소
  let email: String?
  /// F: 여성 M: 남성 U: 확인불가
  let gender: String?
  /// 사용자 연령대
  let age: String?
  /// 사용자 생일(MM-DD 형식)
  let birthday: String?
  /// 사용자 프로필 사진 URL
  let profile_image: String?
    
    enum CodingKeys: String, CodingKey {
            case id = "id"
            case nickname = "nickname"
            case name = "name"
        case email = "email"
        case gender = "gender"
        case age = "age"
        case birthday = "birthday"
        case profile_image = "profile_image"
        
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        nickname = try values.decodeIfPresent(String.self, forKey: .nickname)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        age = try values.decodeIfPresent(String.self, forKey: .age)
        birthday = try values.decodeIfPresent(String.self, forKey: .birthday)
        profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
    }
    
}
