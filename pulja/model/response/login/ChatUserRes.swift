//
//  ChatUserRes.swift
//  pulja
//
//  Created by 김병헌 on 2021/12/03.
//

import Foundation

struct ChatUserRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : [ChatUser]?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([ChatUser].self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}
