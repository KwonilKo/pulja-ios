//
//  ResCommon.swift
//  pulja
//
//  Created by 김병헌 on 2021/11/22.
//

import Foundation
struct ResCommon : Codable {
    
    let statusCode : Int?
    let message : String?
    let success : Bool?
    
    enum CodingKeys: String, CodingKey {
        
    
        case statusCode = "statusCode"
        case message = "message"
        case success = "success"
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    
    }
    
}
