//
//  StudyInfoRes.swift
//  pulja
//
//  Created by HUN on 2022/12/16.
//

import Foundation

struct StudyInfoRes: Codable {
    let statusCode : Int?
    let message : String?
    let data : StudyInfo?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(StudyInfo.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }
}

struct StudyInfo: Codable {
    let hasProblemSeq: Bool?
    let hasSolve: Bool?
    
    enum CodingKeys: String, CodingKey {
        case hasProblemSeq = "hasProblemSeq"
        case hasSolve = "hasSolve"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        hasProblemSeq = try values.decodeIfPresent(Bool.self, forKey: .hasProblemSeq)
        hasSolve = try values.decodeIfPresent(Bool.self, forKey: .hasSolve)
    }

}
