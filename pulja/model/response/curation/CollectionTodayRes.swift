//
//  CurationTodayRes.swift
//  pulja
//
//  Created by 김병헌 on 2022/11/05.
//

import Foundation


struct CollectionTodayRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : CollectionToday?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(CollectionToday.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}


struct CollectionToday : Codable {
    let unit2Text : String?
    let unit2NameList : [String]?
    let unit2SeqList : [Int]?
    let keywordList : [KeywordData]?
    let userKeyword : String?
    let userKeywordList : [KeywordData]?
    let gradeKeyword : String?
    let gradeKeywordList : [KeywordData]?
    let recentStudyCollectionList : [CollectionDTOList]?
    let userRecommendCollectionList : [CollectionDTOList]?

    enum CodingKeys: String, CodingKey {

        case unit2Text = "unit2Text"
        case unit2NameList = "unit2NameList"
        case unit2SeqList = "unit2SeqList"
        case keywordList = "keywordList"
        case userKeyword = "userKeyword"
        case userKeywordList = "userKeywordList"
        case gradeKeyword = "gradeKeyword"
        case gradeKeywordList = "gradeKeywordList"
        case recentStudyCollectionList = "recentStudyCollectionList"
        case userRecommendCollectionList = "userRecommendCollectionList"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        unit2Text = try values.decodeIfPresent(String.self, forKey: .unit2Text)
        unit2NameList = try values.decodeIfPresent([String].self, forKey: .unit2NameList)
        unit2SeqList = try values.decodeIfPresent([Int].self, forKey: .unit2SeqList)
        keywordList = try values.decodeIfPresent([KeywordData].self, forKey: .keywordList)
        userKeyword = try values.decodeIfPresent(String.self, forKey: .userKeyword)
        userKeywordList = try values.decodeIfPresent([KeywordData].self, forKey: .userKeywordList)
        gradeKeyword = try values.decodeIfPresent(String.self, forKey: .gradeKeyword)
        gradeKeywordList = try values.decodeIfPresent([KeywordData].self, forKey: .gradeKeywordList)
        recentStudyCollectionList = try values.decodeIfPresent([CollectionDTOList].self, forKey: .recentStudyCollectionList)
        userRecommendCollectionList = try values.decodeIfPresent([CollectionDTOList].self, forKey: .userRecommendCollectionList)
    }

}
