//
//  StudyHistoryRes.swift
//  pulja
//
//  Created by HUN on 2023/01/04.
//

import Foundation

struct StudyHistoryRes: Codable {
    let statusCode : Int?
    let message : String?
    let data : StudyHistory?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(StudyHistory.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }
}

struct StudyHistory: Codable {
    let todaySolveCnt: Int?
    let todaySolveDuration: Int?
    let yesterdaySolveCnt: Int?
    let yesterdaySolveDuration: Int?
    let weekSolveCnt: Int?
    let weekSolveCntList: [Int]?
    let lastWeekCorrectRate: Int?
    let thisWeekCorrectRate: Int?
    
    enum CodingKeys: String, CodingKey {
        case todaySolveCnt = "todaySolveCnt"
        case todaySolveDuration = "todaySolveDuration"
        case yesterdaySolveCnt = "yesterdaySolveCnt"
        case yesterdaySolveDuration = "yesterdaySolveDuration"
        case weekSolveCnt = "weekSolveCnt"
        case weekSolveCntList = "weekSolveCntList"
        case lastWeekCorrectRate = "lastWeekCorrectRate"
        case thisWeekCorrectRate = "thisWeekCorrectRate"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        todaySolveCnt = try values.decodeIfPresent(Int.self, forKey: .todaySolveCnt)
        todaySolveDuration = try values.decodeIfPresent(Int.self, forKey: .todaySolveDuration)
        yesterdaySolveCnt = try values.decodeIfPresent(Int.self, forKey: .yesterdaySolveCnt)
        yesterdaySolveDuration = try values.decodeIfPresent(Int.self, forKey: .yesterdaySolveDuration)
        weekSolveCnt = try values.decodeIfPresent(Int.self, forKey: .weekSolveCnt)
        weekSolveCntList = try values.decodeIfPresent([Int].self, forKey: .weekSolveCntList)
        lastWeekCorrectRate = try values.decodeIfPresent(Int.self, forKey: .lastWeekCorrectRate)
        thisWeekCorrectRate = try values.decodeIfPresent(Int.self, forKey: .thisWeekCorrectRate)
    }
}
