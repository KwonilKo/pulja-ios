//
//  AIStudyResultRes.swift
//  pulja
//
//  Created by 김병헌 on 2022/11/01.
//

import Foundation


struct AIStudyResultRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : AIStudyResult?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(AIStudyResult.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}


struct AIStudyResult : Codable {
    let correctCnt : Int?
    let studyCnt : Int?
    let totalDuration : Int?
    let analysisText1 : String?
    let analysisText2 : String?
    let proposalText1 : String?
    let proposalText2 : String?
    let keywordList : [KeywordData]?

    enum CodingKeys: String, CodingKey {

        case correctCnt = "correctCnt"
        case studyCnt = "studyCnt"
        case totalDuration = "totalDuration"
        case analysisText1 = "analysisText1"
        case analysisText2 = "analysisText2"
        case proposalText1 = "proposalText1"
        case proposalText2 = "proposalText2"
        case keywordList = "keywordList"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        correctCnt = try values.decodeIfPresent(Int.self, forKey: .correctCnt)
        studyCnt = try values.decodeIfPresent(Int.self, forKey: .studyCnt)
        totalDuration = try values.decodeIfPresent(Int.self, forKey: .totalDuration)
        analysisText1 = try values.decodeIfPresent(String.self, forKey: .analysisText1)
        analysisText2 = try values.decodeIfPresent(String.self, forKey: .analysisText2)
        proposalText1 = try values.decodeIfPresent(String.self, forKey: .proposalText1)
        proposalText2 = try values.decodeIfPresent(String.self, forKey: .proposalText2)
        keywordList = try values.decodeIfPresent([KeywordData].self, forKey: .keywordList)
    }

}
