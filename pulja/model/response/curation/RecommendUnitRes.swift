//
//  RecommendUnitRes.swift
//  pulja
//
//  Created by 김병헌 on 2022/10/26.
//

import Foundation


struct RecommendUnitRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : RecommendUnit?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(RecommendUnit.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct RecommendUnit : Codable {
    let classSeq : Int?
    let subjectName : String?
    let subjectSeq : Int?
    let unit1Name : String?
    let unit1SName : String?
    let unit1Seq : Int?
    let unit2Name : String?
    let unit2SName : String?
    let unit2Seq : Int?
    let unit2Text: String?
    let unit2SeqList : [Int]?
    

    enum CodingKeys: String, CodingKey {

        case classSeq = "classSeq"
        case subjectName = "subjectName"
        case subjectSeq = "subjectSeq"
        case unit1Name = "unit1Name"
        case unit1SName = "unit1SName"
        case unit1Seq = "unit1Seq"
        case unit2Name = "unit2Name"
        case unit2SName = "unit2SName"
        case unit2Seq = "unit2Seq"
        case unit2Text = "unit2Text"
        case unit2SeqList = "unit2SeqList"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        classSeq = try values.decodeIfPresent(Int.self, forKey: .classSeq)
        subjectName = try values.decodeIfPresent(String.self, forKey: .subjectName)
        subjectSeq = try values.decodeIfPresent(Int.self, forKey: .subjectSeq)
        unit1Name = try values.decodeIfPresent(String.self, forKey: .unit1Name)
        unit1SName = try values.decodeIfPresent(String.self, forKey: .unit1SName)
        unit1Seq = try values.decodeIfPresent(Int.self, forKey: .unit1Seq)
        unit2Name = try values.decodeIfPresent(String.self, forKey: .unit2Name)
        unit2SName = try values.decodeIfPresent(String.self, forKey: .unit2SName)
        unit2Seq = try values.decodeIfPresent(Int.self, forKey: .unit2Seq)
        unit2Text = try values.decodeIfPresent(String.self, forKey: .unit2Text)
        unit2SeqList = try values.decodeIfPresent([Int].self, forKey: .unit2SeqList)
    }

}
