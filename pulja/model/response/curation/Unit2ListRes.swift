//
//  Unit2ListRes.swift
//  pulja
//
//  Created by 김병헌 on 2022/10/26.
//

import Foundation

struct Unit2ListRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : [UnitData]?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([UnitData].self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct UnitData : Codable {
    let name : String?
    var subjects : [Subjects]?
    var selected : Bool?

    enum CodingKeys: String, CodingKey {

        case name = "name"
        case subjects = "subjects"
        case selected = "selected"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        subjects = try values.decodeIfPresent([Subjects].self, forKey: .subjects)
        selected = try values.decodeIfPresent(Bool.self, forKey: .selected)
    }

}


struct Subjects : Codable {
    let subjectSeq : Int?
    let classSeq : Int?
    let name : String?
    var unit1s : [Unit1s]?
    var selected : Bool?

    enum CodingKeys: String, CodingKey {

        case subjectSeq = "subjectSeq"
        case classSeq = "classSeq"
        case name = "name"
        case unit1s = "unit1s"
        case selected = "selected"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        subjectSeq = try values.decodeIfPresent(Int.self, forKey: .subjectSeq)
        classSeq = try values.decodeIfPresent(Int.self, forKey: .classSeq)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        unit1s = try values.decodeIfPresent([Unit1s].self, forKey: .unit1s)
        selected = try values.decodeIfPresent(Bool.self, forKey: .selected)
    }

}


struct Unit1s : Codable {
    let unit1Seq : Int?
    let subjectSeq : Int?
    let name : String?
    var unit2s : [Unit2s]?

    enum CodingKeys: String, CodingKey {

        case unit1Seq = "unit1Seq"
        case subjectSeq = "subjectSeq"
        case name = "name"
        case unit2s = "unit2s"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        unit1Seq = try values.decodeIfPresent(Int.self, forKey: .unit1Seq)
        subjectSeq = try values.decodeIfPresent(Int.self, forKey: .subjectSeq)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        unit2s = try values.decodeIfPresent([Unit2s].self, forKey: .unit2s)
    }

}

struct Unit2s : Codable {
    let unit2Seq : Int?
    let unit1Seq : Int?
    let name : String?
    let sname : String?
    var selected : Bool?

    enum CodingKeys: String, CodingKey {

        case unit2Seq = "unit2Seq"
        case unit1Seq = "unit1Seq"
        case name = "name"
        case sname = "sname"
        case selected = "selected"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        unit2Seq = try values.decodeIfPresent(Int.self, forKey: .unit2Seq)
        unit1Seq = try values.decodeIfPresent(Int.self, forKey: .unit1Seq)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        sname = try values.decodeIfPresent(String.self, forKey: .sname)
        selected = try values.decodeIfPresent(Bool.self, forKey: .selected)
    }

}
