//
//  CollectionDetailRes.swift
//  pulja
//
//  Created by HUN on 2023/01/11.
//

import Foundation

struct CollectionDetailRes: Codable {
    let statusCode : Int?
    let message : String?
    let data : CollectionDetail?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(CollectionDetail.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }
}

struct CollectionDetail: Codable {
    let collectionSeq : Int?
    let difficultyText : String?
    let framingDescMob : String?
    let framingDescTab : String?
    let framingTitle : String?
    let studyDays : Int?
    let studyStatus : Int?
    let studyStatusText : String?
    let totalQuestionCnt : Int?
    let unitName : String?
    let userCompleteDate : String?
    let userCorrectQuestionCnt : Int?
    let userStudyDays : Int?
    let userStudyQuestionCnt : Int?
    let problemSeq : Int?
    
    enum CodingKeys: String, CodingKey {
        case collectionSeq = "collectionSeq"
        case difficultyText = "difficultyText"
        case framingDescMob = "framingDescMob"
        case framingDescTab = "framingDescTab"
        case framingTitle = "framingTitle"
        case studyDays = "studyDays"
        case studyStatus = "studyStatus"
        case studyStatusText = "studyStatusText"
        case totalQuestionCnt = "totalQuestionCnt"
        case unitName = "unitName"
        case userCompleteDate = "userCompleteDate"
        case userCorrectQuestionCnt = "userCorrectQuestionCnt"
        case userStudyDays = "userStudyDays"
        case userStudyQuestionCnt = "userStudyQuestionCnt"
        case problemSeq = "problemSeq"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        collectionSeq = try values.decodeIfPresent(Int.self, forKey: .collectionSeq)
        difficultyText = try values.decodeIfPresent(String.self, forKey: .difficultyText)
        framingDescMob = try values.decodeIfPresent(String.self, forKey: .framingDescMob)
        framingDescTab = try values.decodeIfPresent(String.self, forKey: .framingDescTab)
        framingTitle = try values.decodeIfPresent(String.self, forKey: .framingTitle)
        studyDays = try values.decodeIfPresent(Int.self, forKey: .studyDays)
        studyStatus = try values.decodeIfPresent(Int.self, forKey: .studyStatus)
        studyStatusText = try values.decodeIfPresent(String.self, forKey: .studyStatusText)
        totalQuestionCnt = try values.decodeIfPresent(Int.self, forKey: .totalQuestionCnt)
        unitName = try values.decodeIfPresent(String.self, forKey: .unitName)
        userCompleteDate = try values.decodeIfPresent(String.self, forKey: .userCompleteDate)
        userCorrectQuestionCnt = try values.decodeIfPresent(Int.self, forKey: .userCorrectQuestionCnt)
        userStudyDays = try values.decodeIfPresent(Int.self, forKey: .userStudyDays)
        userStudyQuestionCnt = try values.decodeIfPresent(Int.self, forKey: .userStudyQuestionCnt)
        problemSeq = try values.decodeIfPresent(Int.self, forKey: .problemSeq)
    }
}
