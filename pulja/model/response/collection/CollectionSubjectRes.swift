//
//  CollectionSubjectRes.swift
//  pulja
//
//  Created by HUN on 2023/01/12.
//

import Foundation
struct CollectionSubjectRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : [SubjectList]?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([SubjectList].self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct SubjectList : Codable {
    let name : String?
    let subjects : [Subject]?
    let selected : Bool?

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case subjects = "subjects"
        case selected = "selected"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        subjects = try values.decodeIfPresent([Subject].self, forKey: .subjects)
        selected = try values.decodeIfPresent(Bool.self, forKey: .selected)
    }

}

struct Subject : Codable {
    let subjectSeq : Int?
    let classSeq : Int?
    let name : String?
    let unit1s : String?
    let selected : Bool?

    enum CodingKeys: String, CodingKey {

        case subjectSeq = "subjectSeq"
        case classSeq = "classSeq"
        case name = "name"
        case unit1s = "unit1s"
        case selected = "selected"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        subjectSeq = try values.decodeIfPresent(Int.self, forKey: .subjectSeq)
        classSeq = try values.decodeIfPresent(Int.self, forKey: .classSeq)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        unit1s = try values.decodeIfPresent(String.self, forKey: .unit1s)
        selected = try values.decodeIfPresent(Bool.self, forKey: .selected)
    }

}
