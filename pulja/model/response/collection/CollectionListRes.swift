//
//  CollectionListRes.swift
//  pulja
//
//  Created by HUN on 2023/01/11.
//

import Foundation

struct CollectionListRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : CollectionListData?
    let success : Bool?

    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(CollectionListData.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct CollectionListData : Codable {
    let selectedSchoolType : String?
    let selectedSubjectSeq : Int?
    let unit1CollectionList : [Unit1CollectionList]?

    enum CodingKeys: String, CodingKey {

        case selectedSchoolType = "selectedSchoolType"
        case selectedSubjectSeq = "selectedSubjectSeq"
        case unit1CollectionList = "unit1CollectionList"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        selectedSchoolType = try values.decodeIfPresent(String.self, forKey: .selectedSchoolType)
        selectedSubjectSeq = try values.decodeIfPresent(Int.self, forKey: .selectedSubjectSeq)
        unit1CollectionList = try values.decodeIfPresent([Unit1CollectionList].self, forKey: .unit1CollectionList)
    }
}

struct Unit1CollectionList : Codable {
    let unit1Seq : Int?
    let unit1Name : String?
    let unit1SName : String?
    let framingList : [FramingList]?

    enum CodingKeys: String, CodingKey {

        case unit1Seq = "unit1Seq"
        case unit1Name = "unit1Name"
        case unit1SName = "unit1SName"
        case framingList = "framingList"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        unit1Seq = try values.decodeIfPresent(Int.self, forKey: .unit1Seq)
        unit1Name = try values.decodeIfPresent(String.self, forKey: .unit1Name)
        unit1SName = try values.decodeIfPresent(String.self, forKey: .unit1SName)
        framingList = try values.decodeIfPresent([FramingList].self, forKey: .framingList)
    }
}

struct FramingList : Codable {
    let framingType : Int?
    let framingTypeTitle : String?
    let collectionDTOList : [CollectionDTOList]?

    enum CodingKeys: String, CodingKey {

        case framingType = "framingType"
        case framingTypeTitle = "framingTypeTitle"
        case collectionDTOList = "collectionDTOList"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        framingType = try values.decodeIfPresent(Int.self, forKey: .framingType)
        framingTypeTitle = try values.decodeIfPresent(String.self, forKey: .framingTypeTitle)
        collectionDTOList = try values.decodeIfPresent([CollectionDTOList].self, forKey: .collectionDTOList)
    }
}

struct CollectionDTOList : Codable {
    let collectionSeq : Int?
    let collectionUnitName : String?
    let collectionFramingTitle : String?
    let difficultyText : String?
    let questionCnt : Int?
    let rcmdStudyDays : Int?
    let userStudyQuestionCnt : Int?
    let collectionFramingNumber : Int?
    
    enum CodingKeys: String, CodingKey {

        case collectionSeq = "collectionSeq"
        case collectionUnitName = "collectionUnitName"
        case collectionFramingTitle = "collectionFramingTitle"
        case difficultyText = "difficultyText"
        case questionCnt = "questionCnt"
        case rcmdStudyDays = "rcmdStudyDays"
        case userStudyQuestionCnt = "userStudyQuestionCnt"
        case collectionFramingNumber = "collectionFramingNumber"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        collectionSeq = try values.decodeIfPresent(Int.self, forKey: .collectionSeq)
        collectionUnitName = try values.decodeIfPresent(String.self, forKey: .collectionUnitName)
        collectionFramingTitle = try values.decodeIfPresent(String.self, forKey: .collectionFramingTitle)
        difficultyText = try values.decodeIfPresent(String.self, forKey: .difficultyText)
        questionCnt = try values.decodeIfPresent(Int.self, forKey: .questionCnt)
        rcmdStudyDays = try values.decodeIfPresent(Int.self, forKey: .rcmdStudyDays)
        userStudyQuestionCnt = try values.decodeIfPresent(Int.self, forKey: .userStudyQuestionCnt)
        collectionFramingNumber = try values.decodeIfPresent(Int.self, forKey: .collectionFramingNumber)
    }
}
