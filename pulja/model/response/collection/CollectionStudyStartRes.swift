//
//  CollectionStudyStartRes.swift
//  pulja
//
//  Created by HUN on 2023/01/13.
//

import Foundation
struct CollectionStudyStartRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : CollectionStudyStart?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(CollectionStudyStart.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }
}


struct CollectionStudyStart : Codable {
    let collectionSeq : Int?
    let explain : CollectionExplain?
    let problem : CollectionProblem?
    let problemSeq : Int?

    enum CodingKeys: String, CodingKey {

        case collectionSeq = "collectionSeq"
        case explain = "explain"
        case problem = "problem"
        case problemSeq = "problemSeq"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        collectionSeq = try values.decodeIfPresent(Int.self, forKey: .collectionSeq)
        explain = try values.decodeIfPresent(CollectionExplain.self, forKey: .explain)
        problem = try values.decodeIfPresent(CollectionProblem.self, forKey: .problem)
        problemSeq = try values.decodeIfPresent(Int.self, forKey: .problemSeq)
    }

}

struct CollectionProblem : Codable {
    let title : String?
    let questionId : Int?
    let problemUrl : String?
    let problemType : String?
    let answerCnt : Int?
    let choiceCnt : Int?
    let number : Int?
    let totalCnt : Int?
    let last : Bool?

    enum CodingKeys: String, CodingKey {

        case title = "title"
        case questionId = "questionId"
        case problemUrl = "problemUrl"
        case problemType = "problemType"
        case answerCnt = "answerCnt"
        case choiceCnt = "choiceCnt"
        case number = "number"
        case totalCnt = "totalCnt"
        case last = "last"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        questionId = try values.decodeIfPresent(Int.self, forKey: .questionId)
        problemUrl = try values.decodeIfPresent(String.self, forKey: .problemUrl)
        problemType = try values.decodeIfPresent(String.self, forKey: .problemType)
        answerCnt = try values.decodeIfPresent(Int.self, forKey: .answerCnt)
        choiceCnt = try values.decodeIfPresent(Int.self, forKey: .choiceCnt)
        number = try values.decodeIfPresent(Int.self, forKey: .number)
        totalCnt = try values.decodeIfPresent(Int.self, forKey: .totalCnt)
        last = try values.decodeIfPresent(Bool.self, forKey: .last)
    }

}

struct CollectionExplain : Codable {
    let isCorrect : String?
    let feedBackTextA : String?
    let feedBackTextB : String?
    let problemType : String?
    let categoryName : String?
    let solveSeq : Int?
    let userAnswer : String?
    let answer : String?
    let problemUrl : String?
    let explainUrl : String?
    let choiceCnt : Int?

    enum CodingKeys: String, CodingKey {

        case isCorrect = "isCorrect"
        case feedBackTextA = "feedBackTextA"
        case feedBackTextB = "feedBackTextB"
        case problemType = "problemType"
        case categoryName = "categoryName"
        case solveSeq = "solveSeq"
        case userAnswer = "userAnswer"
        case answer = "answer"
        case problemUrl = "problemUrl"
        case explainUrl = "explainUrl"
        case choiceCnt = "choiceCnt"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isCorrect = try values.decodeIfPresent(String.self, forKey: .isCorrect)
        feedBackTextA = try values.decodeIfPresent(String.self, forKey: .feedBackTextA)
        feedBackTextB = try values.decodeIfPresent(String.self, forKey: .feedBackTextB)
        problemType = try values.decodeIfPresent(String.self, forKey: .problemType)
        categoryName = try values.decodeIfPresent(String.self, forKey: .categoryName)
        solveSeq = try values.decodeIfPresent(Int.self, forKey: .solveSeq)
        userAnswer = try values.decodeIfPresent(String.self, forKey: .userAnswer)
        answer = try values.decodeIfPresent(String.self, forKey: .answer)
        problemUrl = try values.decodeIfPresent(String.self, forKey: .problemUrl)
        explainUrl = try values.decodeIfPresent(String.self, forKey: .explainUrl)
        choiceCnt = try values.decodeIfPresent(Int.self, forKey: .choiceCnt)
    }

}
