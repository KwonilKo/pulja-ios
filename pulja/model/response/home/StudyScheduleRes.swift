//
//  StudyScheduleRes.swift
//  pulja
//
//  Created by 김병헌 on 2022/02/08.
//

import Foundation


struct StudyScheduleRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : [StudySchedule]?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([StudySchedule].self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct StudySchedule : Codable {
    let userSeq : Int?
    let studyDay : String?
    let weekInfo : String?
    let coachingSchedule : String?
    let videoSchedule : String?
    let problemSchedule : String?
    let studyComplete : Bool?
    let problemComplete : Bool?
    let coachingComplete : Bool?
    let videoComplete : Bool?

    enum CodingKeys: String, CodingKey {

        case userSeq = "userSeq"
        case studyDay = "studyDay"
        case weekInfo = "weekInfo"
        case coachingSchedule = "coachingSchedule"
        case videoSchedule = "videoSchedule"
        case problemSchedule = "problemSchedule"
        case studyComplete = "studyComplete"
        case problemComplete = "problemComplete"
        case coachingComplete = "coachingComplete"
        case videoComplete = "videoComplete"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
        studyDay = try values.decodeIfPresent(String.self, forKey: .studyDay)
        weekInfo = try values.decodeIfPresent(String.self, forKey: .weekInfo)
        coachingSchedule = try values.decodeIfPresent(String.self, forKey: .coachingSchedule)
        videoSchedule = try values.decodeIfPresent(String.self, forKey: .videoSchedule)
        problemSchedule = try values.decodeIfPresent(String.self, forKey: .problemSchedule)
        studyComplete = try values.decodeIfPresent(Bool.self, forKey: .studyComplete)
        problemComplete = try values.decodeIfPresent(Bool.self, forKey: .problemComplete)
        coachingComplete = try values.decodeIfPresent(Bool.self, forKey: .coachingComplete)
        videoComplete = try values.decodeIfPresent(Bool.self, forKey: .videoComplete)
    }

}
