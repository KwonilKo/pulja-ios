//
//  StudyGoalRes.swift
//  pulja
//
//  Created by 김병헌 on 2022/02/08.
//

import Foundation

struct StudyGoalRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : Int?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(Int.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}
