//
//  StudentsRes.swift
//  pulja
//
//  Created by kwonilko on 2022/03/19.
//

import Foundation


struct StudentsScheduleRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : [StudentsSchedule]?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([StudentsSchedule].self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}



struct StudentsSchedule : Codable {
    let userSeq : Int?
    let username : String?
    let restPaymentDay : Int?
    let restCoachingCnt : Int?
    let curriculumTitle : String?
    let nextCoaching : String?
    let todayStudyList : [StudentsTodayStudy]?
    let weekStudy : String?

    enum CodingKeys: String, CodingKey {

        case userSeq = "userSeq"
        case username = "username"
        case restPaymentDay = "restPaymentDay"
        case restCoachingCnt = "restCoachingCnt"
        case curriculumTitle = "curriculumTitle"
        case nextCoaching = "nextCoaching"
        case todayStudyList  = "todayStudyList"
        case weekStudy = "weekStudy"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        restPaymentDay = try values.decodeIfPresent(Int.self, forKey: .restPaymentDay)
        restCoachingCnt = try values.decodeIfPresent(Int.self, forKey: .restCoachingCnt)
        curriculumTitle = try values.decodeIfPresent(String.self, forKey: .curriculumTitle)
        nextCoaching = try values.decodeIfPresent(String.self, forKey: .nextCoaching)
        todayStudyList = try values.decodeIfPresent([StudentsTodayStudy].self, forKey: .todayStudyList)
        weekStudy = try values.decodeIfPresent(String.self, forKey: .weekStudy)
    }

}

struct StudentsTodayStudy : Codable {
    let confirmTodayStudy : Bool?
    let studyTime : String?
    let studyUrl : String?
    
    enum CodingKeys: String, CodingKey {

        case confirmTodayStudy = "confirmTodayStudy"
        case studyTime = "studyTime"
        case studyUrl = "studyUrl"
       
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        confirmTodayStudy = try values.decodeIfPresent(Bool.self, forKey: .confirmTodayStudy)
        studyTime = try values.decodeIfPresent(String.self, forKey: .studyTime)
        studyUrl = try values.decodeIfPresent(String.self, forKey: .studyUrl)
        
    }
}

