//
//  HomeRes.swift
//  pulja
//
//  Created by 김병헌 on 2022/02/08.
//

import Foundation

struct HomeRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : Home?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(Home.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}


struct Home : Codable {
    let appHomeView : AppHomeView?
    let todayStudyScheduleText : String?
    let weekInfo : String?
    let thisWeekStudyTime : Int?
    let thisWeekStudyTimeText : String?
    let thisWeekStudyVideo : Int?
    let thisWeekTotalVideo : Int?
    let thisWeekStudyProblem : Int?
    let thisWeekTotalProblem : Int?
    let lastWeekStudyTime : Int?
    let lastWeekStudyTimeText : String?
    let incompleteVideo : Int?
    let incompleteProblem : Int?
    let weakCateStudyAmount : WeakCateStudyAmount?

    enum CodingKeys: String, CodingKey {

        case appHomeView = "appHomeView"
        case todayStudyScheduleText = "todayStudyScheduleText"
        case weekInfo = "weekInfo"
        case thisWeekStudyTime = "thisWeekStudyTime"
        case thisWeekStudyTimeText = "thisWeekStudyTimeText"
        case thisWeekStudyVideo = "thisWeekStudyVideo"
        case thisWeekTotalVideo = "thisWeekTotalVideo"
        case thisWeekStudyProblem = "thisWeekStudyProblem"
        case thisWeekTotalProblem = "thisWeekTotalProblem"
        case lastWeekStudyTime = "lastWeekStudyTime"
        case lastWeekStudyTimeText = "lastWeekStudyTimeText"
        case incompleteVideo = "incompleteVideo"
        case incompleteProblem = "incompleteProblem"
        case weakCateStudyAmount = "weakCateStudyAmount"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        appHomeView = try values.decodeIfPresent(AppHomeView.self, forKey: .appHomeView)
        todayStudyScheduleText = try values.decodeIfPresent(String.self, forKey: .todayStudyScheduleText)
        weekInfo = try values.decodeIfPresent(String.self, forKey: .weekInfo)
        thisWeekStudyTime = try values.decodeIfPresent(Int.self, forKey: .thisWeekStudyTime)
        thisWeekStudyTimeText = try values.decodeIfPresent(String.self, forKey: .thisWeekStudyTimeText)
        thisWeekStudyVideo = try values.decodeIfPresent(Int.self, forKey: .thisWeekStudyVideo)
        thisWeekTotalVideo = try values.decodeIfPresent(Int.self, forKey: .thisWeekTotalVideo)
        thisWeekStudyProblem = try values.decodeIfPresent(Int.self, forKey: .thisWeekStudyProblem)
        thisWeekTotalProblem = try values.decodeIfPresent(Int.self, forKey: .thisWeekTotalProblem)
        lastWeekStudyTime = try values.decodeIfPresent(Int.self, forKey: .lastWeekStudyTime)
        lastWeekStudyTimeText = try values.decodeIfPresent(String.self, forKey: .lastWeekStudyTimeText)
        incompleteVideo = try values.decodeIfPresent(Int.self, forKey: .incompleteVideo)
        incompleteProblem = try values.decodeIfPresent(Int.self, forKey: .incompleteProblem)
        weakCateStudyAmount = try values.decodeIfPresent(WeakCateStudyAmount.self, forKey: .weakCateStudyAmount)
    }

}

struct AppHomeView : Codable {
    let userSeq : Int?
    let userType : String?
    let viewText : String?
    let viewColor : String?
    let viewImage : String?
    let hashUserSeq : String?

    enum CodingKeys: String, CodingKey {

        case userSeq = "userSeq"
        case userType = "userType"
        case viewText = "viewText"
        case viewColor = "viewColor"
        case viewImage = "viewImage"
        case hashUserSeq = "hashUserSeq"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
        userType = try values.decodeIfPresent(String.self, forKey: .userType)
        viewText = try values.decodeIfPresent(String.self, forKey: .viewText)
        viewColor = try values.decodeIfPresent(String.self, forKey: .viewColor)
        viewImage = try values.decodeIfPresent(String.self, forKey: .viewImage)
        hashUserSeq = try values.decodeIfPresent(String.self, forKey: .hashUserSeq)
    }

}

struct WeakCateStudyAmount :  Codable {
    let userSeq : Int?
    let thisWeekStudyTime : Int?
    let thisWeekStudyTimeText : String?
    let lastWeekStudyTime : Int?
    let lastWeekStudyTimeText : String?
    let weakCategoryCount : Int?
    let completeCategoryCount : Int?
    let correctRate : Int?
    let completeCateStudy : Bool?
    let problemSeq : Int?

    enum CodingKeys: String, CodingKey {

        case userSeq = "userSeq"
        case thisWeekStudyTime = "thisWeekStudyTime"
        case thisWeekStudyTimeText = "thisWeekStudyTimeText"
        case lastWeekStudyTime = "lastWeekStudyTime"
        case lastWeekStudyTimeText = "lastWeekStudyTimeText"
        case weakCategoryCount = "weakCategoryCount"
        case completeCategoryCount = "completeCategoryCount"
        case correctRate = "correctRate"
        case completeCateStudy = "completeCateStudy"
        case problemSeq = "problemSeq"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
        thisWeekStudyTime = try values.decodeIfPresent(Int.self, forKey: .thisWeekStudyTime)
        thisWeekStudyTimeText = try values.decodeIfPresent(String.self, forKey: .thisWeekStudyTimeText)
        lastWeekStudyTime = try values.decodeIfPresent(Int.self, forKey: .lastWeekStudyTime)
        lastWeekStudyTimeText = try values.decodeIfPresent(String.self, forKey: .lastWeekStudyTimeText)
        weakCategoryCount = try values.decodeIfPresent(Int.self, forKey: .weakCategoryCount)
        completeCategoryCount = try values.decodeIfPresent(Int.self, forKey: .completeCategoryCount)
        correctRate = try values.decodeIfPresent(Int.self, forKey: .correctRate)
        completeCateStudy = try values.decodeIfPresent(Bool.self, forKey: .completeCateStudy)
        problemSeq = try values.decodeIfPresent(Int.self, forKey: .problemSeq)
    }

}
