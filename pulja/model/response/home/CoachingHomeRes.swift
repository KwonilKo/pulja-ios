//
//  CoachingHomeRes.swift
//  pulja
//
//  Created by kwonilko on 2022/03/19.
//

import Foundation


struct CoachingHomeRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : [CoachingHome]?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([CoachingHome].self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}


struct CoachingHome : Codable {
    let scheduleDate : String?
    let scheduleList : [Schedule]?
   

    enum CodingKeys: String, CodingKey {

        case scheduleDate = "scheduleDate"
        case scheduleList = "scheduleList"
       
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        scheduleDate = try values.decodeIfPresent(String.self, forKey: .scheduleDate)
        scheduleList = try values.decodeIfPresent([Schedule].self, forKey: .scheduleList)
        
    }

}

struct Schedule : Codable {
    let confSeq : Int?
    let username : String?
    let status : String?
    let statusText : String?
    let cdate : String?
    let ctime : String?
   
    

    enum CodingKeys: String, CodingKey {

        case confSeq = "confSeq"
        case username = "username"
        case status = "status"
        case statusText = "statusText"
        case cdate = "cdate"
        case ctime = "ctime"
       
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        confSeq = try values.decodeIfPresent(Int.self, forKey: .confSeq)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        statusText = try values.decodeIfPresent(String.self, forKey: .statusText)
        cdate = try values.decodeIfPresent(String.self, forKey: .cdate)
        ctime = try values.decodeIfPresent(String.self, forKey: .ctime)
        
    }

}

