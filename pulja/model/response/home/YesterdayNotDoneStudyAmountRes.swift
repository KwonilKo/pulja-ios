//
//  YesterdayNotDoneStudyAmountRes.swift
//  pulja
//
//  Created by 김병헌 on 2022/02/08.
//

import Foundation



struct YesterdayNotDoneStudyAmountRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : YesterdayNotDoneStudyAmount?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(YesterdayNotDoneStudyAmount.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}


struct YesterdayNotDoneStudyAmount : Codable {
    let userSeq : Int?
    let startDate : String?
    let endDate : String?
    let videoStudyCnt : Int?
    let videoTotalCnt : Int?
    let problemStudyCnt : Int?
    let problemTotalCnt : Int?
    let problemStudyCorrectCnt : Int?
    let studyTime : Int?

    enum CodingKeys: String, CodingKey {

        case userSeq = "userSeq"
        case startDate = "startDate"
        case endDate = "endDate"
        case videoStudyCnt = "videoStudyCnt"
        case videoTotalCnt = "videoTotalCnt"
        case problemStudyCnt = "problemStudyCnt"
        case problemTotalCnt = "problemTotalCnt"
        case problemStudyCorrectCnt = "problemStudyCorrectCnt"
        case studyTime = "studyTime"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
        startDate = try values.decodeIfPresent(String.self, forKey: .startDate)
        endDate = try values.decodeIfPresent(String.self, forKey: .endDate)
        videoStudyCnt = try values.decodeIfPresent(Int.self, forKey: .videoStudyCnt)
        videoTotalCnt = try values.decodeIfPresent(Int.self, forKey: .videoTotalCnt)
        problemStudyCnt = try values.decodeIfPresent(Int.self, forKey: .problemStudyCnt)
        problemTotalCnt = try values.decodeIfPresent(Int.self, forKey: .problemTotalCnt)
        problemStudyCorrectCnt = try values.decodeIfPresent(Int.self, forKey: .problemStudyCorrectCnt)
        studyTime = try values.decodeIfPresent(Int.self, forKey: .studyTime)
    }

}

