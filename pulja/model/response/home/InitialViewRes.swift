//
//  InitialViewRes.swift
//  pulja
//
//  Created by 고권일 on 2022/02/14.
//

import Foundation

struct InitialViewRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : InitialView?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(InitialView.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}


struct InitialView : Codable {
    let userSeq : Int?
    let userType : String?
    let viewText : String?
    let viewColor : String?
    let viewImage : String?
   
    enum CodingKeys: String, CodingKey {

        case userSeq = "userSeq"
        case userType = "userType"
        case viewText = "viewText"
        case viewColor = "viewColor"
        case viewImage = "viewImage"
       
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
        userType = try values.decodeIfPresent(String.self, forKey: .userType)
        viewText = try values.decodeIfPresent(String.self, forKey: .viewText)
        viewColor = try values.decodeIfPresent(String.self, forKey: .viewColor)
        viewImage = try values.decodeIfPresent(String.self, forKey: .viewImage)
    }

}
