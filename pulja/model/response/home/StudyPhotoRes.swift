//
//  StudyPhotoRes.swift
//  pulja
//
//  Created by kwonilko on 2022/03/23.
//

import Foundation

struct StudyPhotoRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : StudyPhoto?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(StudyPhoto.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct StudyPhoto : Codable {
    let content : [StudyPhotoContent]?
    let totalPages : Int?
    let totalElements : Int?
    let last : Bool?
    let size : Int?
    let number : Int?
    let numberOfElements : Int?
    let sort: Sort?
    let first : Bool?
    let empty: Bool?

    enum CodingKeys: String, CodingKey {

        case content = "content"
        case totalPages = "totalPages"
        case totalElements = "totalElements"
        case last = "last"
        case size = "size"
        case number = "number"
        case numberOfElements = "numberOfElements"
        case sort = "sort"
        case first = "first"
        case empty = "empty"
        
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        content = try values.decodeIfPresent([StudyPhotoContent].self, forKey: .content)
        totalPages = try values.decodeIfPresent(Int.self, forKey: .totalPages)
        totalElements = try values.decodeIfPresent(Int.self, forKey: .totalElements)
        last = try values.decodeIfPresent(Bool.self, forKey: .last)
        size = try values.decodeIfPresent(Int.self, forKey: .size)
        number = try values.decodeIfPresent(Int.self, forKey: .number)
        numberOfElements = try values.decodeIfPresent(Int.self, forKey: .numberOfElements)
        sort = try values.decodeIfPresent(Sort.self, forKey: .sort)
        first = try values.decodeIfPresent(Bool.self, forKey: .first)
        empty = try values.decodeIfPresent(Bool.self, forKey: .empty)
    }

}

struct StudyPhotoContent : Codable {
    let startTime : String?
    let studySeq : Int?
    let username : String?
    let userSeq : Int?
    let photo : String?
    

    enum CodingKeys: String, CodingKey {

        case startTime = "startTime"
        case studySeq = "studySeq"
        case username = "username"
        case userSeq = "userSeq"
        case photo = "photo"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        startTime = try values.decodeIfPresent(String.self, forKey: .startTime)
        studySeq = try values.decodeIfPresent(Int.self, forKey: .studySeq)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
        photo = try values.decodeIfPresent(String.self, forKey: .photo)
    }

}
