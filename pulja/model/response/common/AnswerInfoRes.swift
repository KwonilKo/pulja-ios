//
//  AnswerRes.swift
//  pulja
//
//  Created by 김병헌 on 2022/01/28.
//

import Foundation

struct AnswerInfoRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : AnswerInfo?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(AnswerInfo.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}
