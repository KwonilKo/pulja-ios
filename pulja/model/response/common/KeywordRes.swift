//
//  KeywordRes.swift
//  pulja
//
//  Created by 김병헌 on 2022/10/24.
//

import Foundation



struct KeywordRes : Codable {
    
    let statusCode : Int?
    let message : String?
    let data : [KeywordData]?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([KeywordData].self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}


struct KeywordData : Codable {
    let keywordSeq : Int?
    let keywordGroup : String?
    let name : String?
    let displayName : String?
    let keywordOrder : Int?
    let recentChoice : Bool?
    var isSelect : Bool?
    var isDisable : Bool?
    

    enum CodingKeys: String, CodingKey {

        case keywordSeq = "keywordSeq"
        case keywordGroup = "keywordGroup"
        case name = "name"
        case displayName = "displayName"
        case keywordOrder = "keywordOrder"
        case recentChoice = "recentChoice"
        case isSelect = "isSelect"
        case isDisable = "isDisable"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        keywordSeq = try values.decodeIfPresent(Int.self, forKey: .keywordSeq)
        keywordGroup = try values.decodeIfPresent(String.self, forKey: .keywordGroup)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        displayName = try values.decodeIfPresent(String.self, forKey: .displayName)
        keywordOrder = try values.decodeIfPresent(Int.self, forKey: .keywordOrder)
        recentChoice = try values.decodeIfPresent(Bool.self, forKey: .recentChoice)
        isSelect = try values.decodeIfPresent(Bool.self, forKey: .isSelect)
        isDisable = try values.decodeIfPresent(Bool.self, forKey: .isDisable)
    }

}
