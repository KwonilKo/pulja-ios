//
//  VersionRes.swift
//  pulja
//
//  Created by 김병헌 on 2022/03/03.
//

import Foundation

struct VersionRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : Version?
    let success : Bool?

    enum CodingKeys: String, CodingKey {
        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(Version.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct Version : Codable {
    
    let osType : String?
    let minimum : String?
    let current : String?
    let description : String?

    enum CodingKeys: String, CodingKey {
        case osType = "osType"
        case minimum = "minimum"
        case current = "current"
        case description = "description"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        osType = try values.decodeIfPresent(String.self, forKey: .osType)
        minimum = try values.decodeIfPresent(String.self, forKey: .minimum)
        current = try values.decodeIfPresent(String.self, forKey: .current)
        description = try values.decodeIfPresent(String.self, forKey: .description)
    }

}
