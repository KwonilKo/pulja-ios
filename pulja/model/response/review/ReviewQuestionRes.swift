//
//  ReviewQuestionRes.swift
//  pulja
//
//  Created by 김병헌 on 2022/01/17.
//

import Foundation

struct ReviewQuestionRes : Codable {
    
    let statusCode : Int?
    let message : String?
    let data : ReviewQuestionData?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(ReviewQuestionData.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}


struct ReviewQuestionData : Codable {
    let content : [ReviewQuestion]?
    let pageable : Pageable?
    let totalElements : Int?
    let last : Bool?
    let totalPages : Int?
    let size : Int?
    let number : Int?
    let sort : Sort?
    let numberOfElements : Int?
    let first : Bool?
    let empty : Bool?

    enum CodingKeys: String, CodingKey {

        case content = "content"
        case pageable = "pageable"
        case totalElements = "totalElements"
        case last = "last"
        case totalPages = "totalPages"
        case size = "size"
        case number = "number"
        case sort = "sort"
        case numberOfElements = "numberOfElements"
        case first = "first"
        case empty = "empty"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        content = try values.decodeIfPresent([ReviewQuestion].self, forKey: .content)
        pageable = try values.decodeIfPresent(Pageable.self, forKey: .pageable)
        totalElements = try values.decodeIfPresent(Int.self, forKey: .totalElements)
        last = try values.decodeIfPresent(Bool.self, forKey: .last)
        totalPages = try values.decodeIfPresent(Int.self, forKey: .totalPages)
        size = try values.decodeIfPresent(Int.self, forKey: .size)
        number = try values.decodeIfPresent(Int.self, forKey: .number)
        sort = try values.decodeIfPresent(Sort.self, forKey: .sort)
        numberOfElements = try values.decodeIfPresent(Int.self, forKey: .numberOfElements)
        first = try values.decodeIfPresent(Bool.self, forKey: .first)
        empty = try values.decodeIfPresent(Bool.self, forKey: .empty)
    }

}
