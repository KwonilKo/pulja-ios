//
//  DropoutCheckValid.swift
//  pulja
//
//  Created by 김병헌 on 2022/06/30.
//

import Foundation


struct DropoutCheckValidRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : DropoutCheckValid?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(DropoutCheckValid.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}


struct DropoutCheckValid : Codable {
    let dropoutPossible : Bool?
        let dropoutUserType : String?
        let coachName : String?

        enum CodingKeys: String, CodingKey {

            case dropoutPossible = "dropoutPossible"
            case dropoutUserType = "dropoutUserType"
            case coachName = "coachName"
        }

        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            dropoutPossible = try values.decodeIfPresent(Bool.self, forKey: .dropoutPossible)
            dropoutUserType = try values.decodeIfPresent(String.self, forKey: .dropoutUserType)
            coachName = try values.decodeIfPresent(String.self, forKey: .coachName)
        }

}
