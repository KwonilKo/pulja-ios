//
//  UserRes.swift
//  pulja
//
//  Created by 고권일 on 2021/12/31.

import Foundation

struct UserRes  : Codable {

        let data : User?
        let message : String?
        let statusCode : Int?
        let success : Bool?

        enum CodingKeys: String, CodingKey {
                case data = "data"
                case message = "message"
                case statusCode = "statusCode"
                case success = "success"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                data = try values.decodeIfPresent(User.self, forKey: .data)
                message = try values.decodeIfPresent(String.self, forKey: .message)
                statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
                success = try values.decodeIfPresent(Bool.self, forKey: .success)
        }

}


struct UserNotificationRes : Codable {
    let data : NotificationRes?
    let message : String?
    let statusCode : Int?
    let success : Bool?

    enum CodingKeys: String, CodingKey {
            case data = "data"
            case message = "message"
            case statusCode = "statusCode"
            case success = "success"
    }

    init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            data = try values.decodeIfPresent(NotificationRes.self, forKey: .data)
            message = try values.decodeIfPresent(String.self, forKey: .message)
            statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
            success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }
}

struct NotificationRes : Codable {

        let userSeq : Int?
        let notice : String?
        let chatting : String?
        let study : String?
        let marketing : String?

        enum CodingKeys: String, CodingKey {
            case userSeq = "userSeq"
            case notice = "notice"
            case chatting = "chatting"
            case study = "study"
            case marketing = "marketing"
        }
    
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
            notice = try values.decodeIfPresent(String.self, forKey: .notice)
            chatting = try values.decodeIfPresent(String.self, forKey: .chatting)
            study = try values.decodeIfPresent(String.self, forKey: .study)
            marketing = try values.decodeIfPresent(String.self, forKey: .marketing)
        }

}


