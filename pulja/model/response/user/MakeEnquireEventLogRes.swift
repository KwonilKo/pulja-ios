//
//  MakeEnquireEventLog.swift
//  pulja
//
//  Created by 김병헌 on 2022/06/30.
//

import Foundation

struct MakeEnquireEventLogRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : MakeEnquireEventLog?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(MakeEnquireEventLog.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}


struct MakeEnquireEventLog : Codable {
    let seq : Int?
        let userSeq : Int?
        let eventCode : String?
        let method : String?
        let regdate : String?

        enum CodingKeys: String, CodingKey {

            case seq = "seq"
            case userSeq = "userSeq"
            case eventCode = "eventCode"
            case method = "method"
            case regdate = "regdate"
        }

        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            seq = try values.decodeIfPresent(Int.self, forKey: .seq)
            userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
            eventCode = try values.decodeIfPresent(String.self, forKey: .eventCode)
            method = try values.decodeIfPresent(String.self, forKey: .method)
            regdate = try values.decodeIfPresent(String.self, forKey: .regdate)
        }

}
