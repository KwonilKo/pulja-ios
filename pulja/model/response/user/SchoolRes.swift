//
//  SchoolRes.swift
//  pulja
//
//  Created by 고권일 on 2022/01/04.
//

import Foundation

//
//public struct OptionalObject<Base: Decodable>: Decodable {
//    public let value: Base?
//
//    public init(from decoder: Decoder) throws {
//        do {
//            let container = try decoder.singleValueContainer()
//            self.value = try container.decode(Base.self)
//        } catch {
//            self.value = nil
//        }
//    }
//}

struct SchoolRes  : Codable {

        let data : [School]?
        let message : String?
        let statusCode : Int?
        let success : Bool?

        enum CodingKeys: String, CodingKey {
                case data = "data"
                case message = "message"
                case statusCode = "statusCode"
                case success = "success"
        }
    
//        init(from decoder: Decoder) throws {
//            let values = try decoder.container(keyedBy: CodingKeys.self)
//            let nullableData = try values.decode([OptionalObject<School>].self, forKey: .data) // <1>
//            data = nullableData.compactMap { $0.value } // <2>
//            message = try values.decodeIfPresent(String.self, forKey: .message)
//            statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
//            success = try values.decodeIfPresent(Bool.self, forKey: .success)
//        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)

                data = try values.decodeIfPresent([School].self, forKey: .data)
                message = try values.decodeIfPresent(String.self, forKey: .message)
                statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
                success = try values.decodeIfPresent(Bool.self, forKey: .success)
        }

}

struct School : Codable {

        let schulCode : String?
        let schulKndScCode : Int?
        let schulNm : String?
        let schulRdnma : String?

        enum CodingKeys: String, CodingKey {
                case schulCode = "schulCode"
                case schulKndScCode = "schulKndScCode"
                case schulNm = "schulNm"
                case schulRdnma = "schulRdnma"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                schulCode = try values.decodeIfPresent(String.self, forKey: .schulCode)
                schulKndScCode = try values.decodeIfPresent(Int.self, forKey: .schulKndScCode)
                schulNm = try values.decodeIfPresent(String.self, forKey: .schulNm)
                schulRdnma = try values.decodeIfPresent(String.self, forKey: .schulRdnma)
        }

}
