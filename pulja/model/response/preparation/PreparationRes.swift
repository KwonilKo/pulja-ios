//
//  PreparationRes.swift
//  pulja
//
//  Created by kwonilko on 2022/08/02.
//

import Foundation


struct SelfStudyRes : Codable {
    
    let statusCode : Int?
    let message : String?
    let data : SelfStudy?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(SelfStudy.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}


struct SelfStudy : Codable {
    let imageType : String?
    let lastStudyUnit2Name : String?
    let lastStudyUnit2Seq : Int?
    let mainText : String?
    let subText : String?
    let badgeText : String?
    let todayStudyCnt: Int?
    let totalStudyCnt: Int?

    enum CodingKeys: String, CodingKey {
        case imageType = "imageType"
        case lastStudyUnit2Name = "lastStudyUnit2Name"
        case lastStudyUnit2Seq = "lastStudyUnit2Seq"
        case mainText = "mainText"
        case subText = "subText"
        case badgeText = "badgeText"
        case todayStudyCnt = "todayStudyCnt"
        case totalStudyCnt = "totalStudyCnt"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        imageType = try values.decodeIfPresent(String.self, forKey: .imageType)
        lastStudyUnit2Name = try values.decodeIfPresent(String.self, forKey: .lastStudyUnit2Name)
        lastStudyUnit2Seq = try values.decodeIfPresent(Int.self, forKey: .lastStudyUnit2Seq)
        mainText = try values.decodeIfPresent(String.self, forKey: .mainText)
        subText = try values.decodeIfPresent(String.self, forKey: .subText)
        badgeText = try values.decodeIfPresent(String.self, forKey: .badgeText)
        todayStudyCnt = try values.decodeIfPresent(Int.self, forKey: .todayStudyCnt)
        totalStudyCnt = try values.decodeIfPresent(Int.self, forKey: .totalStudyCnt)

    }

}




struct AIStudyRes : Codable {
    
    let statusCode : Int?
    let message : String?
    let data : AIStudy?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(AIStudy.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct AIStudy : Codable {
    let correctCnt : Int?
    let studyCnt : Int?
    let totalDuration : Int?
    let goalStudyCnt : Int?
    let thisWeekStudyCntArray: [Int]?

    enum CodingKeys: String, CodingKey {
        case correctCnt = "correctCnt"
        case studyCnt = "studyCnt"
        case totalDuration = "totalDuration"
        case goalStudyCnt = "goalStudyCnt"
        case thisWeekStudyCntArray = "thisWeekStudyCntArray"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        correctCnt = try values.decodeIfPresent(Int.self, forKey: .correctCnt)
        studyCnt = try values.decodeIfPresent(Int.self, forKey: .studyCnt)
        totalDuration = try values.decodeIfPresent(Int.self, forKey: .totalDuration)
        goalStudyCnt = try values.decodeIfPresent(Int.self, forKey: .goalStudyCnt)
        thisWeekStudyCntArray = try values.decodeIfPresent([Int].self, forKey: .thisWeekStudyCntArray)

    }

}





struct WeakVideoReactionRes : Codable {
    
    let statusCode : Int?
    let message : String?
    let data : Int?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(Int.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}




struct AIRecommendNextRes : Codable {
    
    let statusCode : Int?
    let message : String?
    let data : AIRecommendNext?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(AIRecommendNext.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct AIRecommendNext : Codable {
    let answer : AnswerVideo?
    let feedBackText : String?
    let feedBackTextA : String?
    let feedBackTextB : String?
    let isCorrect : String?
    let question: AIRecommendStep?
    let video: AIWeakVideo?
    let studyAllKeyword : Bool?
    let keywordName: String?
//    let choiceCnt: Int?

    

    enum CodingKeys: String, CodingKey {
        case answer = "answer"
        case feedBackText = "feedBackText"
        case feedBackTextA = "feedBackTextA"
        case feedBackTextB = "feedBackTextB"
        case isCorrect = "isCorrect"
        case question = "question"
        case video = "video"
        case studyAllKeyword = "studyAllKeyword"
        case keywordName = "keywordName"
//        case choiceCnt = "choiceCnt"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        answer = try values.decodeIfPresent(AnswerVideo.self, forKey: .answer)
        feedBackText = try values.decodeIfPresent(String.self, forKey: .feedBackText)
        feedBackTextA = try values.decodeIfPresent(String.self, forKey: .feedBackTextA)
        feedBackTextB = try values.decodeIfPresent(String.self, forKey: .feedBackTextB)
        isCorrect = try values.decodeIfPresent(String.self, forKey: .isCorrect)
        question = try values.decodeIfPresent(AIRecommendStep.self, forKey: .question)
        video = try values.decodeIfPresent(AIWeakVideo.self, forKey: .video)
        studyAllKeyword = try values.decodeIfPresent(Bool.self, forKey: .studyAllKeyword)
        keywordName = try values.decodeIfPresent(String.self, forKey: .keywordName)
//        choiceCnt = try values.decodeIfPresent(Int.self, forKey: .choiceCnt)

    }

}

struct AnswerVideo : Codable {
    let answer : String?
    let solveSeq : Int?
    let categoryName : String?
    let explainUrl : String?
    let isCorrect : String?
    let movieCode: String?
    let movieUrl: String?
    let problemSeq : Int?
    let problemType : String?
    let problemUrl : String?
    let questionId: Int?
    let userAnswer: String?
    let userSeq: Int?
    let vlecSeq: Int?
    let choiceCnt: Int?

    enum CodingKeys: String, CodingKey {
        case answer = "answer"
        case solveSeq = "solveSeq"
        case categoryName = "categoryName"
        case explainUrl = "explainUrl"
        case isCorrect = "isCorrect"
        case movieCode = "movieCode"
        case movieUrl = "movieUrl"
        case problemSeq = "problemSeq"
        case problemType = "problemType"
        case problemUrl = "problemUrl"
        case questionId = "questionId"
        case userAnswer = "userAnswer"
        case userSeq = "userSeq"
        case vlecSeq = "vlecSeq"
        case choiceCnt = "choiceCnt"
        
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        answer = try values.decodeIfPresent(String.self, forKey: .answer)
        solveSeq = try values.decodeIfPresent(Int.self, forKey: .solveSeq)
        categoryName = try values.decodeIfPresent(String.self, forKey: .categoryName)
        explainUrl = try values.decodeIfPresent(String.self, forKey: .explainUrl)
        isCorrect = try values.decodeIfPresent(String.self, forKey: .isCorrect)
        movieCode = try values.decodeIfPresent(String.self, forKey: .movieCode)
        movieUrl = try values.decodeIfPresent(String.self, forKey: .movieUrl)
        problemSeq = try values.decodeIfPresent(Int.self, forKey: .problemSeq)
        problemType = try values.decodeIfPresent(String.self, forKey: .problemType)
        problemUrl = try values.decodeIfPresent(String.self, forKey: .problemUrl)
        questionId = try values.decodeIfPresent(Int.self, forKey: .questionId)
        userAnswer = try values.decodeIfPresent(String.self, forKey: .userAnswer)
        userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
        vlecSeq = try values.decodeIfPresent(Int.self, forKey: .vlecSeq)
        choiceCnt = try values.decodeIfPresent(Int.self, forKey: .choiceCnt)

    }

}

struct AIWeakVideo : Codable {
    let cateName : String?
    let currentDuration : Int?
    let goodReactionCnt : Int?
    let isNextStudy: String?
    let maxDuration: Int?
    let problemSeq : Int?
    let title : String?
    let videoDuration : Int?
    let videoKey: String?
    let videoUrl: String?
    let vlecSeq: Int?
    let vorder: Int?

    enum CodingKeys: String, CodingKey {
        case cateName = "cateName"
        case currentDuration = "currentDuration"
        case goodReactionCnt = "goodReactionCnt"
        case isNextStudy = "isNextStudy"
        case maxDuration = "maxDuration"
        case problemSeq = "problemSeq"
        case title = "title"
        case videoDuration = "videoDuration"
        case videoKey = "videoKey"
        case videoUrl = "videoUrl"
        case vlecSeq = "vlecSeq"
        case vorder = "vorder"
        
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        cateName = try values.decodeIfPresent(String.self, forKey: .cateName)
        currentDuration = try values.decodeIfPresent(Int.self, forKey: .currentDuration)
        goodReactionCnt = try values.decodeIfPresent(Int.self, forKey: .goodReactionCnt)
        isNextStudy = try values.decodeIfPresent(String.self, forKey: .isNextStudy)
        maxDuration = try values.decodeIfPresent(Int.self, forKey: .maxDuration)
        problemSeq = try values.decodeIfPresent(Int.self, forKey: .problemSeq)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        videoDuration = try values.decodeIfPresent(Int.self, forKey: .videoDuration)
        videoKey = try values.decodeIfPresent(String.self, forKey: .videoKey)
        videoUrl = try values.decodeIfPresent(String.self, forKey: .videoUrl)
        vlecSeq = try values.decodeIfPresent(Int.self, forKey: .vlecSeq)
        vorder = try values.decodeIfPresent(Int.self, forKey: .vorder)

    }

}







struct WeakCategoryStudyInfoRes : Codable {
    
    let statusCode : Int?
    let message : String?
    let data : WeakStudyInfo?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(WeakStudyInfo.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}



struct WeakCategoryStudyVideoRes : Codable {
    
    let statusCode : Int?
    let message : String?
    let data : [WeakCategoryVideo]?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent([WeakCategoryVideo].self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}
struct WeakCategoryVideo : Codable {
    let cateName : String?
    let problemSeq : Int?
    let vlecSeq : Int?
    let currentDuration : Int?
    let maxDuration : Int?
    let videoDuration : Int?
    let videoKey : String?
    let videoUrl : String?
    let title : String?
    let isNextStudy : String?
    let vorder : Int?

    enum CodingKeys: String, CodingKey {

        case cateName = "cateName"
        case problemSeq = "problemSeq"
        case vlecSeq = "vlecSeq"
        case currentDuration = "currentDuration"
        case maxDuration = "maxDuration"
        case videoDuration = "videoDuration"
        case videoKey = "videoKey"
        case videoUrl = "videoUrl"
        case title = "title"
        case isNextStudy = "isNextStudy"
        case vorder = "vorder"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        cateName = try values.decodeIfPresent(String.self, forKey: .cateName)
        problemSeq = try values.decodeIfPresent(Int.self, forKey: .problemSeq)
        vlecSeq = try values.decodeIfPresent(Int.self, forKey: .vlecSeq)
        currentDuration = try values.decodeIfPresent(Int.self, forKey: .currentDuration)
        maxDuration = try values.decodeIfPresent(Int.self, forKey: .maxDuration)
        videoDuration = try values.decodeIfPresent(Int.self, forKey: .videoDuration)
        videoKey = try values.decodeIfPresent(String.self, forKey: .videoKey)
        videoUrl = try values.decodeIfPresent(String.self, forKey: .videoUrl)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        isNextStudy = try values.decodeIfPresent(String.self, forKey: .isNextStudy)
        vorder = try values.decodeIfPresent(Int.self, forKey: .vorder)
    }

}






struct AIDiagnosisFinishRes : Codable {
    
    let statusCode : Int?
    let message : String?
    let data : AIDiagnosisFinish?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(AIDiagnosisFinish.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}


struct AIDiagnosisFinish : Codable {
    let weakCategory : WeakCategory?
    let diagnosis : AIDiagnosis?

    enum CodingKeys: String, CodingKey {

        case weakCategory = "weakCategory"
        case diagnosis = "diagnosis"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        weakCategory = try values.decodeIfPresent(WeakCategory.self, forKey: .weakCategory)
        diagnosis = try values.decodeIfPresent(AIDiagnosis.self, forKey: .diagnosis)
    }

}

struct AIDiagnosis : Codable {
    let duration : Int?
    let conceptQuotient : ConceptQuotientDto?
    let correctRate : Int?

    enum CodingKeys: String, CodingKey {

        case duration = "duration"
        case conceptQuotient = "conceptQuotient"
        case correctRate = "correctRate"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        duration = try values.decodeIfPresent(Int.self, forKey: .duration)
        conceptQuotient = try values.decodeIfPresent(ConceptQuotientDto.self, forKey: .conceptQuotient)
        correctRate = try values.decodeIfPresent(Int.self, forKey: .correctRate)
    }

}


struct WeakStudyInfo : Codable {
    let problemSeq : Int?
    let userSeq : Int?
    let unit2Seq : Int?
    let status : String?
    let type : String?
    let problemCount : Int?
    let videoCount : Int?
    let duration : Int?
    let problemResult : Int?
    let videoResult : Int?
    let regdate : String?
    let userWeakCategoryList : [UserWeakCategoryList]?
    let unit2 : Unit2?

    enum CodingKeys: String, CodingKey {

        case problemSeq = "problemSeq"
        case userSeq = "userSeq"
        case unit2Seq = "unit2Seq"
        case status = "status"
        case type = "type"
        case problemCount = "problemCount"
        case videoCount = "videoCount"
        case duration = "duration"
        case problemResult = "problemResult"
        case videoResult = "videoResult"
        case regdate = "regdate"
        case userWeakCategoryList = "userWeakCategoryList"
        case unit2 = "unit2"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        problemSeq = try values.decodeIfPresent(Int.self, forKey: .problemSeq)
        userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
        unit2Seq = try values.decodeIfPresent(Int.self, forKey: .unit2Seq)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        problemCount = try values.decodeIfPresent(Int.self, forKey: .problemCount)
        videoCount = try values.decodeIfPresent(Int.self, forKey: .videoCount)
        duration = try values.decodeIfPresent(Int.self, forKey: .duration)
        problemResult = try values.decodeIfPresent(Int.self, forKey: .problemResult)
        videoResult = try values.decodeIfPresent(Int.self, forKey: .videoResult)
        regdate = try values.decodeIfPresent(String.self, forKey: .regdate)
        userWeakCategoryList = try values.decodeIfPresent([UserWeakCategoryList].self, forKey: .userWeakCategoryList)
        unit2 = try values.decodeIfPresent(Unit2.self, forKey: .unit2)
    }

}





struct WeakStudy : Codable {
    let problemSeq : Int?
    let userSeq : Int?
    let unit2Seq : Int?
    let status : String?
    let type : String?
    let problemCount : Int?
    let videoCount : Int?
    let duration : Int?
    let regdate : String?
    let userWeakCategoryList : [UserWeakCategoryList]?
    let unit2 : Unit2?

    enum CodingKeys: String, CodingKey {

        case problemSeq = "problemSeq"
        case userSeq = "userSeq"
        case unit2Seq = "unit2Seq"
        case status = "status"
        case type = "type"
        case problemCount = "problemCount"
        case videoCount = "videoCount"
        case duration = "duration"
        case regdate = "regdate"
        case userWeakCategoryList = "userWeakCategoryList"
        case unit2 = "unit2"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        problemSeq = try values.decodeIfPresent(Int.self, forKey: .problemSeq)
        userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
        unit2Seq = try values.decodeIfPresent(Int.self, forKey: .unit2Seq)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        problemCount = try values.decodeIfPresent(Int.self, forKey: .problemCount)
        videoCount = try values.decodeIfPresent(Int.self, forKey: .videoCount)
        duration = try values.decodeIfPresent(Int.self, forKey: .duration)
        regdate = try values.decodeIfPresent(String.self, forKey: .regdate)
        userWeakCategoryList = try values.decodeIfPresent([UserWeakCategoryList].self, forKey: .userWeakCategoryList)
        unit2 = try values.decodeIfPresent(Unit2.self, forKey: .unit2)
    }

}



struct Unit2 : Codable {
    let unit2Seq : Int?
    let unit1Seq : Int?
    let name : String?
    let sname : String?
    let regdate : String?
    

    enum CodingKeys: String, CodingKey {

        case unit2Seq = "unit2Seq"
        case unit1Seq = "unit1Seq"
        case name = "name"
        case sname = "sname"
        case regdate = "regdate"
//        case hibernateLazyInitializer = "hibernateLazyInitializer"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        unit2Seq = try values.decodeIfPresent(Int.self, forKey: .unit2Seq)
        unit1Seq = try values.decodeIfPresent(Int.self, forKey: .unit1Seq)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        sname = try values.decodeIfPresent(String.self, forKey: .sname)
        regdate = try values.decodeIfPresent(String.self, forKey: .regdate)
        
    }

}

//struct HibernateLazyInitializer : Codable {
//
//    enum CodingKeys: String, CodingKey {
//
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//    }
//
//}





struct UserWeakCategoryList : Codable {
    let problemSeq : Int?
    let conceptCode : String?
    let userSeq : Int?
    let unit2Seq : Int?
    let unit2name : String?
    let category4 : String?
    let regdate : String?

    enum CodingKeys: String, CodingKey {

        case problemSeq = "problemSeq"
        case conceptCode = "conceptCode"
        case userSeq = "userSeq"
        case unit2Seq = "unit2Seq"
        case unit2name = "unit2name"
        case category4 = "category4"
        case regdate = "regdate"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        problemSeq = try values.decodeIfPresent(Int.self, forKey: .problemSeq)
        conceptCode = try values.decodeIfPresent(String.self, forKey: .conceptCode)
        userSeq = try values.decodeIfPresent(Int.self, forKey: .userSeq)
        unit2Seq = try values.decodeIfPresent(Int.self, forKey: .unit2Seq)
        unit2name = try values.decodeIfPresent(String.self, forKey: .unit2name)
        category4 = try values.decodeIfPresent(String.self, forKey: .category4)
        regdate = try values.decodeIfPresent(String.self, forKey: .regdate)
    }

}






struct AIRecommendStepRes : Codable {
    
    let statusCode : Int?
    let message : String?
    let data : AIRecommendStep?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(AIRecommendStep.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}


struct AIRecommendStep : Codable {
    
    let answerCnt: Int?
    let topText : String?
    let problemSeq : Int?
    let title : String?
    let questionId : Int?
    let problemUrl : String?
    let problemType : String?
    let number : Int?
    let totalCnt : Int?
    let last : Bool?
    let userAnswer: String?
    let keywordList:[String]?
    let studyAllKeyword: Bool?
    let choiceCnt: Int?

    enum CodingKeys: String, CodingKey {
        case answerCnt = "answerCnt"
        case topText = "topText"
        case problemSeq = "problemSeq"
        case title = "title"
        case questionId = "questionId"
        case problemUrl = "problemUrl"
        case problemType = "problemType"
        case number = "number"
        case totalCnt = "totalCnt"
        case last = "last"
        case userAnswer = "userAnswer"
        case keywordList = "keywordList"
        case studyAllKeyword = "studyAllKeyword"
        case choiceCnt = "choiceCnt"
       
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        answerCnt = try values.decodeIfPresent(Int.self, forKey: .answerCnt)
        topText = try values.decodeIfPresent(String.self, forKey: .topText)
        problemSeq = try values.decodeIfPresent(Int.self, forKey: .problemSeq)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        questionId = try values.decodeIfPresent(Int.self, forKey: .questionId)
        problemUrl = try values.decodeIfPresent(String.self, forKey: .problemUrl)
        problemType = try values.decodeIfPresent(String.self, forKey: .problemType)
        number = try values.decodeIfPresent(Int.self, forKey: .number)
        totalCnt = try values.decodeIfPresent(Int.self, forKey: .totalCnt)
        last = try values.decodeIfPresent(Bool.self, forKey: .last)
        userAnswer = try values.decodeIfPresent(String.self, forKey: .userAnswer)
        keywordList = try values.decodeIfPresent([String].self, forKey: .keywordList)
        studyAllKeyword = try values.decodeIfPresent(Bool.self, forKey: .studyAllKeyword)
        choiceCnt = try values.decodeIfPresent(Int.self, forKey: .choiceCnt)
    }

}



struct DiagnosisStep1Res : Codable {
    
    let statusCode : Int?
    let message : String?
    let data : DiagnosisStep1?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(DiagnosisStep1.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct DiagnosisStep1 : Codable {
    let problemSeq : Int?
    let problemCount : Int?

    enum CodingKeys: String, CodingKey {

        case problemSeq = "problemSeq"
        case problemCount = "problemCount"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        problemSeq = try values.decodeIfPresent(Int.self, forKey: .problemSeq)
        problemCount = try values.decodeIfPresent(Int.self, forKey: .problemCount)
    }

}




struct TodayStudyRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : TodayStudy?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(TodayStudy.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct TodayStudy : Codable {
    let conceptQuotient : ConceptQuotientDto?
    let weakCategory : WeakCategory?
    let unit2Recommends : [Unit2Recommends]?

    enum CodingKeys: String, CodingKey {

        case conceptQuotient = "conceptQuotient"
        case weakCategory = "weakCategory"
        case unit2Recommends = "unit2Recommends"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        conceptQuotient = try values.decodeIfPresent(ConceptQuotientDto.self, forKey: .conceptQuotient)
        weakCategory = try values.decodeIfPresent(WeakCategory.self, forKey: .weakCategory)
        unit2Recommends = try values.decodeIfPresent([Unit2Recommends].self, forKey: .unit2Recommends)
    }

}

struct Unit2Recommends : Codable {
    let subjectSeq : Int?
    let subjectName : String?
    let unit1Seq : Int?
    let unit1Name : String?
    let unit2Seq : Int?
    let unit2Sname : String?

    enum CodingKeys: String, CodingKey {

        case subjectSeq = "subjectSeq"
        case subjectName = "subjectName"
        case unit1Seq = "unit1Seq"
        case unit1Name = "unit1Name"
        case unit2Seq = "unit2Seq"
        case unit2Sname = "unit2Sname"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        subjectSeq = try values.decodeIfPresent(Int.self, forKey: .subjectSeq)
        subjectName = try values.decodeIfPresent(String.self, forKey: .subjectName)
        unit1Seq = try values.decodeIfPresent(Int.self, forKey: .unit1Seq)
        unit1Name = try values.decodeIfPresent(String.self, forKey: .unit1Name)
        unit2Seq = try values.decodeIfPresent(Int.self, forKey: .unit2Seq)
        unit2Sname = try values.decodeIfPresent(String.self, forKey: .unit2Sname)
    }

}


struct WeakCategory : Codable {
    let isWeakCete4Finished : Int?
    let problemSeq : Int?
    let unit2Sname : String?
    let subject : String?
    let unit1Seq : Int?
    let unit1Sname : String?
    let videoCount : Int?
    let problemCount : Int?
    let weakCategories : [WeakCategories]?

    enum CodingKeys: String, CodingKey {

        case isWeakCete4Finished = "isWeakCete4Finished"
        case problemSeq = "problemSeq"
        case unit2Sname = "unit2Sname"
        case subject = "subject"
        case unit1Seq = "unit1Seq"
        case unit1Sname = "unit1Sname"
        case videoCount = "videoCount"
        case problemCount = "problemCount"
        case weakCategories = "weakCategories"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isWeakCete4Finished = try values.decodeIfPresent(Int.self, forKey: .isWeakCete4Finished)
        problemSeq = try values.decodeIfPresent(Int.self, forKey: .problemSeq)
        unit2Sname = try values.decodeIfPresent(String.self, forKey: .unit2Sname)
        subject = try values.decodeIfPresent(String.self, forKey: .subject)
        unit1Seq = try values.decodeIfPresent(Int.self, forKey: .unit1Seq)
        unit1Sname = try values.decodeIfPresent(String.self, forKey: .unit1Sname)
        videoCount = try values.decodeIfPresent(Int.self, forKey: .videoCount)
        problemCount = try values.decodeIfPresent(Int.self, forKey: .problemCount)
        weakCategories = try values.decodeIfPresent([WeakCategories].self, forKey: .weakCategories)
    }

}




struct MyReportRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : MyReportData?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(MyReportData.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}


struct MyReportData : Codable {
    let subjectReports : [SubjectNameInfo]?

    enum CodingKeys: String, CodingKey {
        case subjectReports = "subjectReports"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        subjectReports = try values.decodeIfPresent([SubjectNameInfo].self, forKey: .subjectReports)
    
    }

}

struct MyReportDetailRes : Codable {
    let statusCode : Int?
    let message : String?
    let data : MyReportDetailData?
    let success : Bool?

    enum CodingKeys: String, CodingKey {

        case statusCode = "statusCode"
        case message = "message"
        case data = "data"
        case success = "success"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(MyReportDetailData.self, forKey: .data)
        success = try values.decodeIfPresent(Bool.self, forKey: .success)
    }

}

struct MyReportDetailData : Codable {
    let conceptQuotientDto : ConceptQuotientDto?
    let unit1List : [Unit1List]?

    enum CodingKeys: String, CodingKey {

        case conceptQuotientDto = "conceptQuotientDto"
        case unit1List = "unit1List"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        conceptQuotientDto = try values.decodeIfPresent(ConceptQuotientDto.self, forKey: .conceptQuotientDto)
        unit1List = try values.decodeIfPresent([Unit1List].self, forKey: .unit1List)
    }

}

struct ConceptQuotientDto : Codable {
    let moddate : String?
    let percent : Int?
    let totalConceptQuotient : Int?
    let conceptQuotientDiff : Int?
    let peerAverage : Double?

    enum CodingKeys: String, CodingKey {

        case moddate = "moddate"
        case percent = "percent"
        case totalConceptQuotient = "totalConceptQuotient"
        case conceptQuotientDiff = "conceptQuotientDiff"
        case peerAverage = "peerAverage"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        moddate = try values.decodeIfPresent(String.self, forKey: .moddate)
        percent = try values.decodeIfPresent(Int.self, forKey: .percent)
        totalConceptQuotient = try values.decodeIfPresent(Int.self, forKey: .totalConceptQuotient)
        conceptQuotientDiff = try values.decodeIfPresent(Int.self, forKey: .conceptQuotientDiff)
        peerAverage = try values.decodeIfPresent(Double.self, forKey: .peerAverage)
    }

}



struct Unit1List : Codable {
    let unit1Seq : Int?
    let name : String?
    let weakCategory : [WeakCategory]?

    enum CodingKeys: String, CodingKey {

        case unit1Seq = "unit1Seq"
        case name = "name"
        case weakCategory = "weakCategory"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        unit1Seq = try values.decodeIfPresent(Int.self, forKey: .unit1Seq)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        weakCategory = try values.decodeIfPresent([WeakCategory].self, forKey: .weakCategory)
    }

}


//struct ProblemSet : Codable {
//    let isWeakCete4Finished : Int?
//    let unit2Sname : String?
//    let subject : String?
//    let unit1Seq : Int?
//    let unit1Sname : String?
//    let videoCount : Int?
//    let problemCount : Int?
//    let weakCategories : [WeakCategories]?
//
//    enum CodingKeys: String, CodingKey {
//
//        case isWeakCete4Finished = "isWeakCete4Finished"
//        case unit2Sname = "unit2Sname"
//        case subject = "subject"
//        case unit1Seq = "unit1Seq"
//        case unit1Sname = "unit1Sname"
//        case videoCount = "videoCount"
//        case problemCount = "problemCount"
//        case weakCategories = "weakCategories"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        isWeakCete4Finished = try values.decodeIfPresent(Int.self, forKey: .isWeakCete4Finished)
//        unit2Sname = try values.decodeIfPresent(String.self, forKey: .unit2Sname)
//        subject = try values.decodeIfPresent(String.self, forKey: .subject)
//        unit1Seq = try values.decodeIfPresent(Int.self, forKey: .unit1Seq)
//        unit1Sname = try values.decodeIfPresent(String.self, forKey: .unit1Sname)
//        videoCount = try values.decodeIfPresent(Int.self, forKey: .videoCount)
//        problemCount = try values.decodeIfPresent(Int.self, forKey: .problemCount)
//        weakCategories = try values.decodeIfPresent([WeakCategories].self, forKey: .weakCategories)
//    }
//
//}


struct WeakCategories : Codable {
    let f_concept_code : String?
    let category4Name : String?

    enum CodingKeys: String, CodingKey {

        case f_concept_code = "f_concept_code"
        case category4Name = "category4Name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        f_concept_code = try values.decodeIfPresent(String.self, forKey: .f_concept_code)
        category4Name = try values.decodeIfPresent(String.self, forKey: .category4Name)
    }

}


