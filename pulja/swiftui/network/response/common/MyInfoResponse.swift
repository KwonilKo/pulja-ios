//
//  MyInfoResponse.swift
//  pulja
//
//  Created by HUN on 2023/01/05.
//

import Foundation

struct MyInfoResponse: Decodable {
    let data: MyInfoObject
}

struct MyInfoObject: Hashable, Decodable {
    let userSeq : Int
    let notice : String
    let chatting : String
    let study : String
    let marketing : String
}
