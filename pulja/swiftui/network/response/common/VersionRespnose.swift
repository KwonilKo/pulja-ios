//
//  VersionInfoRes.swift
//  pulja
//
//  Created by HUN on 2023/01/05.
//

import Foundation

struct VersionResponse: Decodable {
    let data: VersionObject
}

struct VersionObject: Hashable, Decodable {
    let osType: String
    let minimum: String
    let current: String
    let description: String
}
