//
//  NaverLoginResponse.swift
//  pulja
//
//  Created by HUN on 2023/01/05.
//

import Foundation

struct NaverLoginRes: Decodable {
    let data: NaverLoginObject
}

struct NaverLoginObject: Hashable, Decodable {
    let id: String?
    let nickname: String?
    let name: String?
    let email: String?
    let gender: String? // F: 여성 M: 남성 U: 확인불가
    let age: String?
    let birthday: String?
    let profile_image: String?
}
