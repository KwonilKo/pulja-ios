//
//  AuthNetwork.swift
//  pulja
//
//  Created by HUN on 2023/01/05.
//

import Foundation
import Combine

class AuthNetwork {
    private let session: URLSession
    let api = AuthREST()
    
    init(session: URLSession = .shared) {
        self.session = session
    }
    
    func getNaverLogin(tokenType: String, accessToken: String) -> AnyPublisher<NaverLoginObject, URLError> {
        guard let url = api.getNaverLogin().url else {
            return Fail(error: URLError(.badURL)).eraseToAnyPublisher()
        }
        
        let token = "\(tokenType) \(accessToken)"
        
        var request = URLRequest(url: url)
        request.setValue("Authorization", forHTTPHeaderField: String(token))
        
        return session.dataTaskPublisher(for: request)
            .tryMap { data, response in
                guard let httpResponse = response as? HTTPURLResponse else {
                    throw URLError(.unknown)
                }
                
                switch httpResponse.statusCode {
                case 200..<300:
                    print("getNaverLogin() -> success")
                    return data
                case 400..<500:
                    print("getNaverLogin() -> \(String(describing: NetworkManager.statusDesc[httpResponse.statusCode]))")
                    throw URLError(.clientCertificateRejected)
                case 500..<599:
                    print("getNaverLogin() -> \(String(describing: NetworkManager.statusDesc[httpResponse.statusCode]))")
                    throw URLError(.badServerResponse)
                default:
                    throw URLError(.unknown)
                }
            }
            .decode(type: NaverLoginRes.self, decoder: JSONDecoder())
            .map { $0.data }
            .mapError { $0 as? URLError ?? URLError(.unknown) }
            .eraseToAnyPublisher()
    }
}

// MARK: Auth Rest API
struct AuthREST {
    
    func getNaverLogin() -> URLComponents {
        var components = URLComponents()
        
        components.scheme = "https"
        components.host = "openapi.naver.com"
        components.path = "/v1/nid/me"
        
        return components
    }
}
