//
//  CommonNetwork.swift
//  pulja
//
//  Created by HUN on 2023/01/05.
//

import Foundation
import Combine

class CommonNetwork {
    private let session: URLSession
    let api = CommonREST()
    
    init(session: URLSession = .shared) {
        self.session = session
    }
    
    func getVersion() -> AnyPublisher<VersionObject, URLError> {
        guard let url = api.getVersion().url else {
            return Fail(error: URLError(.badURL)).eraseToAnyPublisher()
        }
        
        var request = URLRequest(url: url)
        request.setValue("*/*", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Keep-Alive", forHTTPHeaderField: "Connection")
        
        return session.dataTaskPublisher(for: request)
            .tryMap { data, response in
                guard let httpResponse = response as? HTTPURLResponse else {
                    throw URLError(.unknown)
                }
                
                switch httpResponse.statusCode {
                case 200..<300:
                    print("getVersion() -> success")
                    return data
                case 400..<500:
                    print("getVersion() -> \(String(describing: NetworkManager.statusDesc[httpResponse.statusCode]))")
                    throw URLError(.clientCertificateRejected)
                case 500..<599:
                    print("getVersion() -> \(String(describing: NetworkManager.statusDesc[httpResponse.statusCode]))")
                    throw URLError(.badServerResponse)
                default:
                    throw URLError(.unknown)
                }
            }
            .decode(type: VersionResponse.self, decoder: JSONDecoder())
            .map { $0.data }
            .mapError { $0 as? URLError ?? URLError(.unknown) }
            .eraseToAnyPublisher()
    }
    
    func getMyInfo() -> AnyPublisher<MyInfoObject, URLError> {
        guard let url = api.getMyInfo().url else {
            return Fail(error: URLError(.badURL)).eraseToAnyPublisher()
        }
        
        var request = URLRequest(url: url)
        
        if let token = UserDefaultsManager().getAccessToken() {
            print("=======================================")
            print("token[\(token)]")
            print("=======================================")
            request.setValue("Authorization", forHTTPHeaderField: String(token))
        } else {
            request.setValue("Keep-Alive", forHTTPHeaderField: "Connection")
        }
        
        request.setValue("*/*", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        return session.dataTaskPublisher(for: request)
            .tryMap { data, response in
                guard let httpResponse = response as? HTTPURLResponse else {
                    throw URLError(.unknown)
                }
                
                switch httpResponse.statusCode {
                case 200..<300:
                    print("getMyInfo() -> success")
                    return data
                case 400..<500:
                    print("getMyInfo() -> \(String(describing: NetworkManager.statusDesc[httpResponse.statusCode]))")
                    throw URLError(.clientCertificateRejected)
                case 500..<599:
                    print("getMyInfo() -> \(String(describing: NetworkManager.statusDesc[httpResponse.statusCode]))")
                    throw URLError(.badServerResponse)
                default:
                    throw URLError(.unknown)
                }
            }
            .decode(type: MyInfoResponse.self, decoder: JSONDecoder())
            .map { $0.data }
            .mapError { $0 as? URLError ?? URLError(.unknown) }
            .eraseToAnyPublisher()
    }
}

// MARK: Common Rest API
struct CommonREST {
    func getVersion() -> URLComponents {
        var components = URLComponents()
        
        components.scheme = Const.SCHEME
        components.host = Const.HOST

        if Const.PORT != "" {
            components.port = Int(Const.PORT)
        }
        
        components.path = "/m/v1/app/version"
        
        components.queryItems = [
            URLQueryItem(name: "osType", value: "i")
        ]
        
        return components
    }
    
    func getMyInfo() -> URLComponents {
        var components = URLComponents()
        
        components.scheme = Const.SCHEME
        components.host = Const.HOST

        if Const.PORT != "" {
            components.port = Int(Const.PORT)
        }
        
        components.path = "/m/v1/user/myInfo"
        
        return components
    }
}
