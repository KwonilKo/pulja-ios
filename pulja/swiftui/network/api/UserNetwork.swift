//
//  UserNetwork.swift
//  pulja
//
//  Created by HUN on 2023/01/05.
//

import Foundation
import Combine

class UserNetwork {
    private let session: URLSession
    let api = UserREST()
    
    init(session: URLSession = .shared) {
        self.session = session
    }
}

// MARK: User REST API
struct UserREST {
    
}
