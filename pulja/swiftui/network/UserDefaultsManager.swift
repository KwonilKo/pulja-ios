//
//  UserDefaultsManager.swift
//  pulja
//
//  Created by HUN on 2023/01/05.
//

import Foundation

public class UserDefaultsManager {
    
    func setAccessToken(token:String) {
        UserDefaults.standard.set(token, forKey:"ACCESS_TOKEN")
        UserDefaults.standard.synchronize()
    }
    
    func removeAccessToken() {
        UserDefaults.standard.removeObject(forKey: "ACCESS_TOKEN")
        UserDefaults.standard.synchronize()
    }
    
    func getAccessToken() -> String? {
        if  let token = UserDefaults.standard.string(forKey: "ACCESS_TOKEN") {
            return token
        } else {
            return nil
        }
    }
    
    func removeRefreshToken() {
        UserDefaults.standard.removeObject(forKey: "REFRESH_TOKEN")
        UserDefaults.standard.synchronize()
    }
    
    func setRefreshToken(token:String) {
        UserDefaults.standard.set(token, forKey:"REFRESH_TOKEN")
        UserDefaults.standard.synchronize()
    }
    
    func getRefreshToken() -> String? {
        if  let token = UserDefaults.standard.string(forKey: "REFRESH_TOKEN") {
            return token
        } else {
            return nil
        }
    }
}
