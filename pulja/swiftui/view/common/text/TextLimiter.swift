//
//  TextLimiter.swift
//  pulja
//
//  Created by HUN on 2023/01/05.
//

import SwiftUI

class TextLimiter: ObservableObject {
    private let limit: Int
    private let isTypeNumer: Bool
    
    init(limit: Int, isTypeNumer: Bool) {
        self.limit = limit
        self.isTypeNumer = isTypeNumer
    }
    
    @Published var hasReachedLimit = false
    
    @Published var value = "" {
        didSet {
            if value.count > self.limit {
                value = String(value.prefix(self.limit))
                self.hasReachedLimit = true
            } else {
                self.hasReachedLimit = false
                // TextLimiter 의 타입이 Number이고 입력된 값이 숫자가 아닌 경우 제거
                if isTypeNumer && !value.allSatisfy({ $0.isNumber }) {
                    value = value.trimmingCharacters(
                            in: CharacterSet(
                                charactersIn: "0123456789"
                            )
                            .inverted
                        )
                }
            }
        }
    }
}
