//
//  FontStyle.swift
//  pulja
//
//  Created by HUN on 2023/01/05.
//

import Foundation
import SwiftUI

enum FontStyle: String, Identifiable, Decodable {
    case extraTitle
    case extraTitleSB
    case extraTitleBold
    case largeTitle
    case largeTitleSB
    case largeTitleBold
    case mediumTitle
    case mediumTitleSB
    case mediumTitleBold
    case title
    case titleSB
    case titleBold
    case title2
    case titleSB2
    case titleBold2
    case headlineLarge
    case headlineLargeSB
    case headlineLargeBold
    case headline
    case headlineSB
    case headlineBold
    case body
    case bodySB
    case bodyBold
    
    var id: String {
        return self.rawValue
    }
    
    var font: String {
        return "Apple SD Gothic Neo"
    }
    
    var size: CGFloat {
        switch self {
        case .extraTitle, .extraTitleSB, .extraTitleBold:
            return 42
        case .largeTitle, .largeTitleSB, .largeTitleBold:
            return 32
        case .mediumTitle, .mediumTitleSB, .mediumTitleBold:
            return 24
        case .title, .titleSB, .titleBold:
            return 22
        case .title2, .titleSB2, .titleBold2:
            return 20
        case .headlineLarge, .headlineLargeSB, .headlineLargeBold:
            return 18
        case .headline, .headlineSB, .headlineBold:
            return 16
        case .body, .bodySB, .bodyBold:
            return 14
        }
    }
    
    var weight: Font.Weight {
        switch self {
        case .extraTitleBold, .largeTitleBold, .mediumTitleBold, .titleBold, .titleBold2, .headlineLargeBold, .headlineBold, .bodyBold:
            return .bold
        case .extraTitleSB, .largeTitleSB, .mediumTitleSB, .titleSB, .titleSB2, .headlineLargeSB, .headlineSB, .bodySB:
            return .semibold
        case .extraTitle, .largeTitle, .mediumTitle, .title, .title2, .headlineLarge, .headline, .body:
            return .regular
        }
    }
}
