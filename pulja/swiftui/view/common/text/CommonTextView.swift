//
//  CommonTextView.swift
//  pulja
//
//  Created by HUN on 2023/01/05.
//

import SwiftUI

struct CommonTextView: View {
    // Not Null
    var style: FontStyle
    var text: String
    
    // Optional
    var color: Color = .black
    var isUnderLine: Bool = false
    var lineSpacing: CGFloat = 6
    
    var body: some View {
        Text(text)
            .foregroundColor(color)
            .font(.custom(style.font, size: style.size).weight(style.weight))
            .underline(isUnderLine)
            .multilineTextAlignment(.leading)
            .lineSpacing(lineSpacing)
    }
}

struct CustomTextView_Previews: PreviewProvider {
    static var previews: some View {
        CommonTextView(style: FontStyle.mediumTitleBold, text: "TEST")
    }
}
