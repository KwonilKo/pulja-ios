//
//  SolidButton.swift
//  pulja
//
//  Created by HUN on 2023/01/05.
//

import SwiftUI

struct SolidButton: View {
    @Binding var isEnabled: Bool
    var label = "다음으로"
    
    let clickCallback: (() -> ())?
    
    var body: some View {
        Button(action: {
            if (isEnabled) {
                clickCallback?()
            }
        }) {
            CommonTextView(
                style: FontStyle.headlineSB,
                text: self.label,
                color: self.isEnabled ? .white : Color("Foggy")
            )
            .frame(maxWidth: .infinity, minHeight: 50)
        }
        .background(self.isEnabled ? Color("Purple") : Color("Foggy20"))
        .cornerRadius(8)
    }
}

struct SolidButton_Previews: PreviewProvider {
    static var previews: some View {
        SolidButton(isEnabled: .constant(true), clickCallback: nil)
    }
}
