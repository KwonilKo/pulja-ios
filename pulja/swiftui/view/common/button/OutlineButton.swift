//
//  OutlineButton.swift
//  pulja
//
//  Created by HUN on 2023/01/05.
//

import SwiftUI

struct OutlineButton: View {
    @Binding var selectedCount: Int
    var label = "1학년"
    var count = 0
    
    let clickCallback: ((Int)->())?
    
    var body: some View {
        Button(action: {
            if self.selectedCount != count {
                self.selectedCount = count
                clickCallback?(self.count)
            }
        }) {
            CommonTextView(
                style: FontStyle.headline,
                text: self.label,
                color: self.selectedCount == count ? Color("Purple") : Color("Dust")
            )
            .frame(maxWidth: .infinity, minHeight: 50)
        }
        .overlay(
            RoundedRectangle(cornerRadius: 8)
                .stroke(self.selectedCount == count ? Color("Purple") : Color("Foggy"), lineWidth: 1)
                .background(self.selectedCount == count ? Color("Purple10").cornerRadius(8) : Color("White").cornerRadius(8))
        )
    }
}

struct OutlineButton_Previews: PreviewProvider {
    static var previews: some View {
        OutlineButton(selectedCount: .constant(0), clickCallback: nil)
    }
}
