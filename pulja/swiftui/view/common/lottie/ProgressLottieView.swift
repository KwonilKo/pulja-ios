//
//  LottieView.swift
//  pulja
//
//  Created by HUN on 2023/01/06.
//

import Lottie
import SwiftUI
import UIKit

struct ProgressLottieView: UIViewRepresentable {
    //typealias UIViewType = UIView
    var filename: String
    var width: CGFloat
    var height: CGFloat
    
    @Binding var isLoading: Bool
    
    let animationView = LottieAnimationView()
    
    func makeUIView(context: UIViewRepresentableContext<ProgressLottieView>) -> UIView {
        let view = UIView(frame: .zero)
        
        if let window = UIWindow.key {
            view.frame = CGRect(x: 0, y: 0, width: window.frame.maxX, height: window.frame.maxY)
            view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.0)
            
            animationView.animation = LottieAnimation.named(filename)
            animationView.frame = CGRect.init(x: 0, y: 0, width: width, height: height)
            if (width == 0 && height == 0) {
                animationView.frame = CGRect(x: 0, y: 0, width: window.frame.maxX/2, height: window.frame.maxY/2)
            }
            animationView.center = window.center
            animationView.contentMode = .scaleAspectFit
            animationView.loopMode = .loop
            animationView.play()
            
            //print(animationView)
            
            window.addSubview(view)
            window.addSubview(animationView)
        }
        
        return view
    }
    
    func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<ProgressLottieView>) {
        if !isLoading {
            context.coordinator.parent.animationView.pause()
            context.coordinator.parent.animationView.isHidden = true
            uiView.isHidden = true
        }
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    class Coordinator: NSObject {
        var parent: ProgressLottieView

        init(_ parent: ProgressLottieView) {
            self.parent = parent
        }
    }
}

extension UIWindow {
    static var key: UIWindow? {
        if #available(iOS 13, *) {
            return UIApplication.shared.windows.first { $0.isKeyWindow }
        } else {
            return UIApplication.shared.keyWindow
        }
    }
}
