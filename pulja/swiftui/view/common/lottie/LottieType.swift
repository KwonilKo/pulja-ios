//
//  LottieType.swift
//  pulja
//
//  Created by HUN on 2023/01/06.
//

import Foundation

enum LottieType: String, Identifiable, Decodable {
    case diagLoading1
    case diagLoading2
    case diagLoading3
    case problemLoading
    case commonLoading
    case prevProblemLoading
    case onBoard
    
    var id: String {
        return self.rawValue
    }
    
    var fileName: String {
        switch self {
        case .diagLoading1:
            return "diagloding1"
        case .diagLoading2:
            return "diagloding2"
        case .diagLoading3:
            return "diagloding3"
        case .problemLoading:
            return "newloading"
        case .commonLoading:
            return "lf20_3msg70cf"
        case .prevProblemLoading:
            return "loading2"
        case .onBoard:
            return "onboard"
        }
    }
    
    var width: CGFloat {
        switch self {
        case .diagLoading1:
            return 180
        case .diagLoading2:
            return 130
        case .diagLoading3:
            return 120
        case .problemLoading:
            return 64
        case .commonLoading:
            return 100
        case .prevProblemLoading:
            return 0
        case .onBoard:
            return 355
        }
    }
    
    var height: CGFloat {
        switch self {
        case .diagLoading1:
            return 120
        case .diagLoading2:
            return 100
        case .diagLoading3:
            return 120
        case .problemLoading:
            return 64
        case .commonLoading:
            return 100
        case .prevProblemLoading:
            return 0
        case .onBoard:
            return 211
        }
    }
}
