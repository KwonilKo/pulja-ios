//
//  ProgressView.swift
//  pulja
//
//  Created by HUN on 2023/01/06.
//

import SwiftUI

struct ProgressView: View {
    @Binding var isLoading: Bool
    var type: LottieType
    
    var body: some View {
        ZStack {
            ProgressLottieView(
                filename: type.fileName,
                width: type.width,
                height: type.height,
                isLoading: $isLoading
            )
        }.show(isVisible: isLoading)
    }
}

struct ProgressView_Previews: PreviewProvider {
    static var previews: some View {
        ProgressView(isLoading: .constant(true), type: LottieType.commonLoading)
    }
}
