//
//  CommonPopupView.swift
//  pulja
//
//  Created by HUN on 2023/01/05.
//

import SwiftUI

struct CommonPopupView: View {
    var title: String
    var description: String
    var leftText: String
    var rightText: String
    
    let leftClickCallback: (()->())?
    let rightClickCallback: (()->())?
    
    var body: some View {
        // 아이패드
        if UIDevice.current.userInterfaceIdiom == .pad {
            VStack{
                VStack(spacing: 16){
                    CommonTextView(
                        style: FontStyle.titleSB2,
                        text: title,
                        color: .black,
                        lineSpacing: 8
                    )
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.top, 12)
                    
                    CommonTextView(
                        style: FontStyle.headline,
                        text: description,
                        color: Color("Dust"),
                        lineSpacing: 8
                    )
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.bottom, 12)
                }
                .frame(maxWidth: .infinity, minHeight: 112)
                .padding(.horizontal, 12)
                
                
                HStack(spacing: 16){
                    if leftText != "" {
                        Button(action: {
                           leftClickCallback?()
                        }) {
                            CommonTextView(
                                style: FontStyle.headlineSB,
                                text: leftText,
                                color: Color("Dust")
                            )
                            .frame(maxWidth: .infinity, minHeight: 50)
                        }
                        .background(Color("OffWhite"))
                        .cornerRadius(8)
                    }
                    
                    if rightText != "" {
                        Button(action: {
                            rightClickCallback?()
                        }) {
                            CommonTextView(
                                style: FontStyle.headlineSB,
                                text: rightText,
                                color: .white
                            )
                            .frame(maxWidth: .infinity, minHeight: 50)
                        }
                        .background(Color("Purple"))
                        .cornerRadius(8)
                    }
                }
                .padding(.top, 14)
            }
            .padding(.horizontal, 20)
            .padding(.vertical, 20)
            .frame(maxWidth: 360, minHeight: 220)
            .background(
                RoundedRectangle(cornerRadius: 12)
                    .stroke(Color.clear, lineWidth: 0)
                    .background(Color.white.cornerRadius(12))
                    .shadow(color: Color("Foggy"), radius: 12)
            )
        } else {
            VStack{
                VStack(spacing: 16){
                    CommonTextView(
                        style: FontStyle.headlineLargeSB,
                        text: title,
                        color: .black,
                        lineSpacing: 6
                    )
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.top, 8)
                    
                    CommonTextView(
                        style: FontStyle.body,
                        text: description,
                        color: Color("Dust"),
                        lineSpacing: 6
                    )
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.bottom, 8)
                    
                }
                .frame(maxWidth: .infinity, minHeight: 80)
                .padding(.horizontal, 8)
                
                HStack(spacing: 10){
                    if leftText != "" {
                        Button(action: {
                            leftClickCallback?()
                        }) {
                            CommonTextView(
                                style: FontStyle.headlineSB,
                                text: leftText,
                                color: Color("Dust")
                            )
                            .frame(maxWidth: .infinity, minHeight: 44)
                        }
                        .background(Color("OffWhite"))
                        .cornerRadius(8)
                    }
                    
                    if rightText != "" {
                        Button(action: {
                            rightClickCallback?()
                        }) {
                            CommonTextView(
                                style: FontStyle.headlineSB,
                                text: rightText,
                                color: .white
                            )
                            .frame(maxWidth: .infinity, minHeight: 44)
                        }
                        .background(Color("Purple"))
                        .cornerRadius(8)
                    }
                }
                .padding(.top, 8)
            }
            .padding(.horizontal, 16)
            .padding(.vertical, 16)
            .frame(maxWidth: 288, minHeight: 180)
            .background(
                RoundedRectangle(cornerRadius: 12)
                    .stroke(Color.clear, lineWidth: 0)
                    .background(Color.white.cornerRadius(12))
                    .shadow(color: Color("Foggy"), radius: 12)
            )
        }
    }
}

struct CommonPopupView_Previews: PreviewProvider {
    static var previews: some View {
        CommonPopupView(
            title: "타이틀",
            description: "테스트 테스트 테스트 테스트 테스트",
            leftText: "취소",
            rightText: "확인",
            leftClickCallback: nil,
            rightClickCallback: nil
        )
    }
}
