//
//  OnBoardView.swift
//  pulja
//
//  Created by HUN on 2023/01/05.
//

import SwiftUI

struct OnBoardView: View {
    @ObservedObject var viewModel = OnBoardViewModel()

    @State private var isNext = false
    @State private var isPresented = false
    
    var body: some View {
        ZStack {
            VStack {
                
                HStack {
                    Image("signatureDefault")
                        .frame(width: 64, height: 28)
                        .padding(EdgeInsets(top: 20, leading: 16, bottom: 20, trailing: 0))
                    Spacer()
                }
                
                
                Spacer()
                VStack(alignment: .center, spacing: 20) {
                    LottieView(
                        jsonName: LottieType.onBoard.fileName,
                        width: LottieType.onBoard.width,
                        height: LottieType.onBoard.height,
                        speed: 0.8
                    ).frame(width: 355, height: 211)
                    
                    VStack(alignment: .center, spacing: 4) {
                        CommonTextView(
                            style: FontStyle.titleBold,
                            text: "나만을 위한"
                        )
                        CommonTextView(
                            style: FontStyle.titleBold,
                            text: "수학문제 스트리밍"
                        )
                    }
                }
                Spacer()
                
                HStack(alignment: .center, spacing: 20) {
                    Button(action: {
                        print("카카오")
                        viewModel.loginWithKakao()
                    }) {
                        Image("iconKakao")
                    }
                    
                    Button(action: {
                        print("네이버")
                        viewModel.loginWithNaver()
                    }) {
                        Image("iconNaver")
                    }
                    
                    Button(action: {
                        print("애플")
                        viewModel.loginWithApple()
                    }) {
                        Image("iconApple")
                    }
                }
                
                CommonTextView(
                    style: FontStyle.body,
                    text: "이메일로 로그인하기",
                    color: Color("Dust"),
                    isUnderLine: true
                )
                .padding(EdgeInsets(top: 32, leading: 0, bottom: 50, trailing: 0))
                .onTapGesture {
                    isPresented = true
                }
            }
            ProgressView(
                isLoading: $viewModel.isLoadingView,
                type:LottieType.commonLoading
            )
            
            NavigationLink(
                destination: OnBoardAgreementView().navigationBarBackButtonHidden(true),
                isActive: $isNext
            ) {}
        }
    }
    
    class Coordniator { }
    
    func makeCoordinator() -> Coordniator {
        return Coordniator()
    }
}

struct OnBoardView_Previews: PreviewProvider {
    static var previews: some View {
        OnBoardView()
    }
}
