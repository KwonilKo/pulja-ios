//
//  OnBoardSchoolView.swift
//  pulja
//
//  Created by HUN on 2023/01/06.
//

import SwiftUI

struct OnBoardSchoolView: View {
    @Environment(\.presentationMode) var presentationMode

    @State private var isButtonEnabled = false
    @State private var isNextButtonClick = false
    @State private var isHighSchool = false
    @State private var selectedCount = 0
    
    var body: some View {
        VStack() {
            VStack {
                Spacer()
                    .frame(height: 27)
                CommonTextView(
                    style: FontStyle.headlineLargeBold,
                    text: "학년 선택"
                )
                Spacer()
                    .frame(height: 19)
                
                Rectangle().fill(Color("BorderBasic02")).frame(height: 1)
            }
            
            HStack {
                VStack(alignment: .leading) {
                    CommonTextView(
                        style: FontStyle.mediumTitle,
                        text: "반가워요!"
                    )
                    Spacer()
                        .frame(height: 8)
                    CommonTextView(
                        style: FontStyle.mediumTitleBold,
                        text: "학년을 선택해주세요"
                    )
                    Spacer()
                        .frame(height: 12)
                    CommonTextView(
                        style: FontStyle.headline,
                        text: "지금 시기에 알맞은 문제를 추천할게요.",
                        color: Color("grey")
                    )
                }
                
                Spacer()
            }
            .padding(EdgeInsets(top: 30, leading: 16, bottom: 0, trailing: 0))

            SchoolTabView(
                selectedCount: $selectedCount,
                tabCallback: schoolTabCallback,
                schoolCallback: selectedSchoolCallback
            )
            .padding(EdgeInsets(top: 36, leading: 16 , bottom: 0, trailing: 16))
            
            Spacer()
                .frame(minHeight: 0)
            Spacer()
                .frame(minHeight: 0)
            Spacer()
                .frame(minHeight: 0)
            
            SolidButton(
                isEnabled: $isButtonEnabled,
                label: "다음으로",
                clickCallback: nextClickCallback
            )
            .padding(EdgeInsets(top: 8, leading: 16 , bottom: 8, trailing: 16))
        }
    }
    
    func schoolTabCallback(isHighSchool: Bool) {
        self.isHighSchool = isHighSchool
        self.selectedCount = 0
        self.isButtonEnabled = false
    }
    
    func selectedSchoolCallback(selectedCount: Int) {
        self.selectedCount = selectedCount
        self.isButtonEnabled = true
    }
    
    func nextClickCallback() {
        //
        self.isNextButtonClick = true
    }
    
}

struct OnBoardSchoolView_Previews: PreviewProvider {
    static var previews: some View {
        OnBoardSchoolView()
    }
}
