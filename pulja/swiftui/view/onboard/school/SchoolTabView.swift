//
//  SchoolTabView.swift
//  pulja
//
//  Created by HUN on 2023/01/06.
//

import SwiftUI

struct SchoolTabView: View {
    @Binding var selectedCount: Int
    
    @State private var isHighSchool = false
    
    
    let tabCallback: ((Bool)->())?
    let schoolCallback: ((Int)->())?
    
    var body: some View {
        VStack(spacing: 20) {
            HStack {
                Button(action: {
                    self.isHighSchool = false
                    self.tabCallback?(false)
                }) {
                    CommonTextView(
                        style: FontStyle.headlineBold,
                        text: "중학교",
                        color: !self.isHighSchool ? .white : Color("Foggy")
                    )
                    .frame(maxWidth: .infinity, minHeight: 52)
                }
                .background(!self.isHighSchool ? Color("Purple") : Color("Foggy20"))
                .cornerRadius(30)
                
                Button(action: {
                    self.isHighSchool = true
                    self.tabCallback?(true)
                }) {
                    CommonTextView(
                        style: FontStyle.headlineBold,
                        text: "고등학교",
                        color: self.isHighSchool ? .white : Color("Foggy")
                    )
                    .frame(maxWidth: .infinity, minHeight: 52)
                }
                .background(self.isHighSchool ? Color("Purple") : Color("Foggy20"))
                .cornerRadius(30)
            }
            
            VStack(spacing: 10) {
                OutlineButton(
                    selectedCount: $selectedCount,
                    label: "1학년",
                    count: 1,
                    clickCallback: schoolSelected
                )
                
                OutlineButton(
                    selectedCount: $selectedCount,
                    label: "2학년",
                    count: 2,
                    clickCallback: schoolSelected
                )
                
                OutlineButton(
                    selectedCount: $selectedCount,
                    label: "3학년",
                    count: 3,
                    clickCallback: schoolSelected
                )
                
                OutlineButton(
                    selectedCount: $selectedCount,
                    label: "검정고시",
                    count: 4,
                    clickCallback: schoolSelected
                )
                .show(isVisible: isHighSchool)
                
                OutlineButton(
                    selectedCount: $selectedCount,
                    label: "N수생",
                    count: 5,
                    clickCallback: schoolSelected
                )
                .show(isVisible: isHighSchool)
            }
        }
    }
    
    func schoolSelected(count: Int) {
        schoolCallback?(count)
    }
}

struct SchoolTabView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolTabView(selectedCount: .constant(0),tabCallback: nil, schoolCallback: nil)
    }
}
