//
//  CheckBoxField.swift
//  pulja
//
//  Created by HUN on 2023/01/06.
//

import SwiftUI

struct CheckboxField: View {
    @Binding var isChecked: Bool
    
    let type: Agreement
    let checkCallback: ((Agreement, Bool)->())?
    let moveButtonCallback: ((Agreement)->())?
    
    var body: some View {
        HStack {
            Image(systemName: self.isChecked ? "checkmark.square.fill" : "square")
                            .resizable()
                            .frame(width: 20, height: 20)
                            .foregroundColor(self.isChecked ? Color("OffPurple") : Color("Foggy40"))
                            .onTapGesture {
                                self.isChecked = !self.isChecked
                                self.checkCallback?(self.type, self.isChecked)
                            }
            
            
            CommonTextView(
                style: FontStyle.body,
                text: type.label
            )
            .padding(EdgeInsets(top: 0, leading: 4 , bottom: 0, trailing: 0))
            Spacer()
            
            if (type != Agreement.checked1) {
                Image("iconSolidChevron24")
                    .frame(width: 20, height: 20)
                    .onTapGesture {
                        self.moveButtonCallback?(self.type)
                    }
            }
        }
    }
}

enum Agreement: String {
    case checked1
    case checked2
    case checked3
    case checked4
    
    var id: String {
        return self.rawValue
    }
    
    var label: String {
        switch self {
        case .checked1:
            return "만 14세 이상입니다"
        case .checked2:
            return "[필수] 회원가입 약관에 동의"
        case .checked3:
            return "[필수] 개인정보 수집 ·이용 ·제공 동의"
        case .checked4:
            return "[선택] 마케팅 활용에 동의"
        }
    }
}

func checkboxSelected(id: String, isMarked: Bool) {
    print("\(id) is marked: \(isMarked)")
}

struct CheckboxField_Previews: PreviewProvider {
    static var previews: some View {
        CheckboxField(
            isChecked: .constant(false),
            type: Agreement.checked4,
            checkCallback: nil,
            moveButtonCallback: nil
        )
    }
}
