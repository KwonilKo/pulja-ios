//
//  AgreementView.swift
//  pulja
//
//  Created by HUN on 2023/01/06.
//

import SwiftUI

struct AgreementView: View {
    @State private var isAllChecked = false
    @State private var isChecked1 = false
    @State private var isChecked2 = false
    @State private var isChecked3 = false
    @State private var isChecked4 = false
    
    let checkAllCallback: ((Bool)->())?
    let checkStatusCallback: ((Bool, Bool)->())?
    let moveButtonCallback: ((Agreement)->())?
    
    var body: some View {
        VStack {
            HStack {
                Image(systemName: self.isAllChecked ? "checkmark.square.fill" : "square")
                                .resizable()
                                .frame(width: 20, height: 20)
                                .foregroundColor(self.isAllChecked ? Color("OffPurple") : Color("Foggy40"))
                                .onTapGesture {
                                    self.isAllChecked = !self.isAllChecked
                                    self.isChecked1 = self.isAllChecked
                                    self.isChecked2 = self.isAllChecked
                                    self.isChecked3 = self.isAllChecked
                                    self.isChecked4 = self.isAllChecked
                                    self.checkAllCallback?(isAllChecked)
                                }
                
                CommonTextView(
                    style: FontStyle.headlineBold,
                    text: "전체(필수, 선택) 약관에 동의합니다"
                )
                .padding(EdgeInsets(top: 0, leading: 4 , bottom: 0, trailing: 0))
                Spacer()
            }
            
            Rectangle()
                .fill(Color("DividerGrey"))
                .frame(height: 1)
                .padding(EdgeInsets(top: 6, leading: 0 , bottom: 0, trailing: 0))
            
            CheckboxField(
                isChecked: $isChecked1,
                type: Agreement.checked1,
                checkCallback: checkBoxSelected,
                moveButtonCallback: moveButtonSelected
            )
            .padding(EdgeInsets(top: 20, leading: 0 , bottom: 0, trailing: 0))
            CheckboxField(
                isChecked: $isChecked2,
                type: Agreement.checked2,
                checkCallback: checkBoxSelected,
                moveButtonCallback: moveButtonSelected
            )
            .padding(EdgeInsets(top: 28, leading: 0 , bottom: 0, trailing: 0))
            CheckboxField(
                isChecked: $isChecked3,
                type: Agreement.checked3,
                checkCallback: checkBoxSelected,
                moveButtonCallback: moveButtonSelected
            )
            .padding(EdgeInsets(top: 28, leading: 0 , bottom: 0, trailing: 0))
            CheckboxField(
                isChecked: $isChecked4,
                type: Agreement.checked4,
                checkCallback: checkBoxSelected,
                moveButtonCallback: moveButtonSelected
            )
            .padding(EdgeInsets(top: 28, leading: 0 , bottom: 0, trailing: 0))
        }
        .padding(EdgeInsets(top: 20, leading: 16 , bottom: 20, trailing: 16))
        .overlay(
            RoundedRectangle(cornerRadius: 12)
                .stroke(Color("Foggy"), lineWidth: 1)
        )
    }
    
    func checkBoxSelected(type: Agreement, isChecked: Bool) {
        print("\(type.id) is cheked: \(isChecked)")
        
        if (self.isChecked1 && self.isChecked2 && self.isChecked3 && self.isChecked4) {
            self.isAllChecked = true
            self.checkAllCallback?(isAllChecked)
        } else {
            self.isAllChecked = false
            self.checkAllCallback?(isAllChecked)
            
            if (self.isChecked2 && self.isChecked3) {
                self.checkStatusCallback?(self.isChecked1, self.isChecked4)
            }
        }
    }
    
    func moveButtonSelected(type: Agreement) {
        self.moveButtonCallback?(type)
    }
    
}

struct AgreementView_Previews: PreviewProvider {
    static var previews: some View {
        AgreementView(checkAllCallback: nil, checkStatusCallback: nil, moveButtonCallback: nil)
    }
}
