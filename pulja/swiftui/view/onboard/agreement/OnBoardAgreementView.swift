//
//  OnBoardAgreementView.swift
//  pulja
//
//  Created by HUN on 2023/01/06.
//

import SwiftUI

struct OnBoardAgreementView: View {
    @Environment(\.presentationMode) var presentationMode
    
    @State private var isButtonEnabled = false
    @State private var isNextButtonClick = false
    @State private var is14 = false
    @State private var isMarketing = false
    
    var body: some View {
        VStack {
            
            VStack {
                Spacer()
                    .frame(height: 27)
                
                ZStack {
                    HStack {
                        Image("iconSolidCheveronLeft24")
                            .onTapGesture {
                                // Back
                                presentationMode.wrappedValue.dismiss()
                            }
                            .padding(EdgeInsets(top: 0, leading: 12 , bottom: 0, trailing: 0))
                        Spacer()
                    }
                    
                    CommonTextView(
                        style: FontStyle.headlineLargeBold,
                        text: "약관 동의"
                    )
                }
                Spacer()
                    .frame(height: 19)
                
                Rectangle().fill(Color("BorderBasic02")).frame(height: 1)
            }
            
            
            HStack {
                VStack(alignment: .leading, spacing: 4) {
                    CommonTextView(
                        style: FontStyle.mediumTitle,
                        text: "회원가입을 위해"
                    )
                    
                    CommonTextView(
                        style: FontStyle.mediumTitleBold,
                        text: "약관에 동의해 주세요"
                    )
                }
                
                Spacer()
            }
            .padding(EdgeInsets(top: 24, leading: 20 , bottom: 0, trailing: 0))
            
            Spacer()
                .frame(minHeight: 0, maxHeight: 80)
            
            AgreementView(
                checkAllCallback: checkAllCallback,
                checkStatusCallback: checkStatusCallback,
                moveButtonCallback: moveButtonCallback
            )
            .padding(EdgeInsets(top: 0, leading: 16 , bottom: 0, trailing: 16))
            
            Spacer()
                .frame(minHeight: 0)
            Spacer()
                .frame(minHeight: 0)
            Spacer()
                .frame(minHeight: 0)
            
            SolidButton(
                isEnabled: $isButtonEnabled,
                label: "다음으로",
                clickCallback: nextClickCallback
            )
            .padding(16)
        }
        
        NavigationLink(
            destination: OnBoardSchoolView().navigationBarBackButtonHidden(true),
            isActive: $isNextButtonClick
        ) {

        }
    }
    
    func checkAllCallback(isAllChecked: Bool) {
        self.isButtonEnabled = isAllChecked
        self.is14 = isAllChecked
        self.isMarketing = isAllChecked
    }
    
    func checkStatusCallback(is14: Bool, isMarketing: Bool) {
        self.isButtonEnabled = true
        self.is14 = is14
        self.isMarketing = isMarketing
    }
    
    func nextClickCallback() {
        self.isNextButtonClick = true
        if (self.is14 && self.isMarketing) {
            print("is All Chekced Next")
        } else {
            print("next")
            print("is14 -> \(is14) / isMarketing: \(isMarketing)")
        }
    }
    
    func moveButtonCallback(type: Agreement) {
        print("\(type.id)")
    }
}

struct OnBoardAgreementView_Previews: PreviewProvider {
    static var previews: some View {
        OnBoardAgreementView()
    }
}
