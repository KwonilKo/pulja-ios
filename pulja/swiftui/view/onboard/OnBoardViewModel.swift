//
//  OnBoardViewModel.swift
//  pulja
//
//  Created by HUN on 2023/01/06.
//

import Foundation
import Combine
import KakaoSDKCommon
import KakaoSDKAuth
import KakaoSDKUser
import NaverThirdPartyLogin
import AuthenticationServices

class OnBoardViewModel: NSObject, ObservableObject, NaverThirdPartyLoginConnectionDelegate {
    // MARK: - MVVM
    @Published var isMoveToHome = false
    @Published var isLoadingView = false
    private var cancellables = Set<AnyCancellable>()
    
    init(UserNetwork: UserNetwork = UserNetwork()) {
        
    }
    
    // MARK: - Kakao Login API
    func loginWithKakao() {
        if (UserApi.isKakaoTalkLoginAvailable()) {
            UserApi.shared.loginWithKakaoTalk {(oauthToken, error) in
                if let error = error {
                    print("kakaotalk login() failed -> \(error)")
                } else {
                    print("loginWithKakaoTalk() success.")
                    
                    //do something
                    //_ = oauthToken
                    
                    self.isLoadingView = true
                    UserApi.shared.me() { (user, error) in
                        if let error = error {
                            self.isLoadingView = false
                            print("kakaotalk me() failed -> \(error)")
                        } else {
                            self.isLoadingView = false
                            print("kakaotalk me() success.")
                            let name = user?.kakaoAccount?.profile?.nickname ?? ""
                            let email = user?.kakaoAccount?.email ?? ""
                            //let kakaoId = user?.id ?? 0
                            print("kakaotalk me() -> \(name)")
                            print("kakaotalk me() -> \(email)")
                            //do something
                            _ = user
                            
                            //                                let snsUserReq = SnsUserReq(fcmToken: nil, marketAccept: nil, osType: "i", snsAppleCode: nil, snsEmail: email, snsId: String(kakaoId), snsNick: name, snsToken: oauthToken?.accessToken, snsType: "k", uuid: nil)
                            //
                            //                                self.snsEmailCheck(snsUserReq: snsUserReq)
                        }
                    }
                }
            }
        } else {
            UserApi.shared.loginWithKakaoAccount {(oauthToken, error) in
                if let error = error {
                    print("kakaoAccount login() failed -> \(error)")
                } else {
                    print("loginWithKakaoAccount() success.")
                    
                    //do something
                    //_ = oauthToken
                    
                    self.isLoadingView = true
                    UserApi.shared.me() { (user, error) in
                        if let error = error {
                            self.isLoadingView = false
                            print("kakaoAccount me() failed -> \(error)")
                        } else {
                            self.isLoadingView = false
                            print("kakaoAccount me() success.")
                            let name = user?.kakaoAccount?.profile?.nickname ?? ""
                            let email = user?.kakaoAccount?.email ?? ""
                            // let kakaoId = user?.id ?? 0
                            print("kakaoAccount me() -> \(name)")
                            print("kakaoAccount me() -> \(email)")
                            //do something
                            //_ = user
                            
                            //                                                       let snsUserReq = SnsUserReq(fcmToken: nil, marketAccept: nil, osType: "i", snsAppleCode: nil, snsEmail: email, snsId: String(kakaoId), snsNick: name, snsToken: oauthToken?.accessToken, snsType: "k", uuid: nil)
                            //
                            //                                                       self.snsEmailCheck(snsUserReq: snsUserReq)
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Naver Login API
    private let loginInstance = NaverThirdPartyLoginConnection.getSharedInstance()
    func loginWithNaver() {
        self.isLoadingView = true
        self.loginInstance?.delegate = self
        self.loginInstance?.resetToken()
        if(self.loginInstance?.accessToken == nil) {
            print("naver 1")
            self.loginInstance?.requestThirdPartyLogin()
        } else {
            if let isValidAccessToken = self.loginInstance?.isValidAccessTokenExpireTimeNow() {
                if !isValidAccessToken {
                    print("naver 2")
                    self.loginInstance?.requestAccessTokenWithRefreshToken()
                } else {
                    print("naver 3")
                    getNaverUserInfo()
                }
            } else {
                print("naver 4")
                self.loginInstance?.requestThirdPartyLogin()
            }
        }
    }
    
    // naver login success
    func oauth20ConnectionDidFinishRequestACTokenWithAuthCode() {
        print("Success login")
        getNaverUserInfo()
    }
    
    // naver referesh token
    func oauth20ConnectionDidFinishRequestACTokenWithRefreshToken() {
        getNaverUserInfo()
    }
    
    // naver logout
    func oauth20ConnectionDidFinishDeleteToken() {
        self.isLoadingView = false
        print("log out")
    }
    
    // naver error
    func oauth20Connection(_ oauthConnection: NaverThirdPartyLoginConnection!, didFailWithError error: Error!) {
        self.isLoadingView = false
        print("error = \(error.localizedDescription)")
    }
    
    // naver RESTful API
    func getNaverUserInfo() {
        guard let isValidAccessToken = loginInstance?.isValidAccessTokenExpireTimeNow() else { return }
        
        if !isValidAccessToken {
            return
        }
        
        guard let tokenType = loginInstance?.tokenType else { return }
        guard let accessToken = loginInstance?.accessToken else { return }
        
        print("naver getInfo() -> accessToken : \(accessToken)")
        
        self.isLoadingView = true
        AuthNetwork().getNaverLogin(tokenType: tokenType, accessToken: accessToken)
            .receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { [weak self] in
                    guard case .failure(let error) = $0 else { return }
                    print("getNaverLogin() failure -> \(error)")
                    // 알수없는오류
                    if (error.localizedDescription.contains("server_error") || error.localizedDescription.contains("Internal Server Error")) {
//                        self.showSnsLoginFailedAlert(
//                            errorMessage: error.localizedDescription,
//                            snsType: "n"
//                        )
                    }
                    self?.isLoadingView = false
                },
                receiveValue: { [weak self] naverLogin in
                    print("getMyInfo() value: \(naverLogin)")
                    self?.isLoadingView = false
                    
                    let naverUniqueId = naverLogin.id ?? ""
                    let email = naverLogin.email ?? ""
                    let name = naverLogin.name ?? ""
                    let gender = naverLogin.gender ?? ""
                    let age = naverLogin.age ?? ""
                    let birthday = naverLogin.birthday ?? ""
                    
                    print(naverUniqueId)
                    print(email)
                    print(name)
                    print(gender)
                    print(age)
                    print(birthday)
                    
//                    let snsUserReq = SnsUserReq(fcmToken: nil, marketAccept: nil, osType: "i", snsAppleCode: nil, snsEmail: email, snsId: naverUniqueId, snsNick: name, snsToken: accessToken, snsType: "n", uuid: nil)
//
//                    self.snsEmailCheck(snsUserReq: snsUserReq)
                }
            )
            .store(in: &cancellables)
    }
    
    // MARK: - Apple Login API
    func loginWithApple() {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            if let userID = UserDefaults.standard.string(forKey: "currentUserIdentifier") {
                appleIDProvider.getCredentialState(forUserID: userID) { (credentialState, error) in
                    switch credentialState {
                    case .authorized:
                        // The Apple ID credential is valid.
                        // 유저를 확인하여 바로 로그인한다.
                        break
                    case .revoked:
                        // The Apple ID credential is revoked.
                        UserDefaults.standard.removeObject(forKey: "currentUserIdentifier")
                        self.startSignInWithAppleFlow()
                        break
                    case .notFound:
                        // No credential was found, so show the sign-in UI.
                        break
                    default:
                        break
                    }
                }
            } else {
                self.startSignInWithAppleFlow()
            }
        }
    }
    
    @available(iOS 13, *)
        func startSignInWithAppleFlow() {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            //authorizationController.delegate = self
            //authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
     }
}
