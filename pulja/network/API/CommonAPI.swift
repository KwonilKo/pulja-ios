//
//  CommonAPI.swift
//  pulja
//
//  Created by 김병헌 on 2021/11/22.
//

import Foundation
import Alamofire
import PromiseKit


class CommonAPI {
    
    static let shared = CommonAPI()
    
    func retryRequest<T : Codable>(request : URLRequest , type : T.Type) -> Promise<Any> {
         
        return AF.request(request).responseCodableWithType(type)
    }
    
    
    
    
    func unitList(unit2Keyword : String) -> Promise<UnitRes> {
        
        let _path: String = "/m/v1/unit2List"
            
        let _query: Parameters = [
            "unit2Keyword" : unit2Keyword
        ]
        
        let _request = AF.request(Const.DOMAIN + _path,
                                             method:.get,
                                             parameters: _query,
                                             encoding: URLEncoding.queryString,
                                             headers: CommonUtil.shared.getHeaders())
            
        _request.validate()
            
        return _request.responseCodable()
    }
    
    func subjectList() -> Promise<SubjectRes> {
            
        let _path: String = "/m/v1/subjectList"
                
        let _query: Parameters = [:]
            
        let _request = AF.request(Const.DOMAIN + _path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                                  headers: CommonUtil.shared.getHeaders()).validate()
        _request.validate()
        return _request.responseCodable()
    }
    
    
    func recentStudyUnit2() -> Promise<UnitRes> {
        
        let _path: String = "/m/v1/recentStudyUnit2"
            
        let _query: Parameters = [:]
        
        let _request = AF.request(Const.DOMAIN + _path,
                                             method:.get,
                                             parameters: _query,
                                             encoding: URLEncoding.queryString,
                                             headers: CommonUtil.shared.getHeaders())
            
        _request.validate()
            
        return _request.responseCodable()

        
    }
    
    
    
    func answerInfo(answerInfoReq: AnswerInfoReq) -> Promise<AnswerInfoRes> {
            
        let _path: String = "/m/v1/answerInfo"
        
        let query:Parameters = answerInfoReq.dictionary!
        
        let _request = AF.request(Const.DOMAIN + _path,
                                  method: .get,
                                  parameters: query,
                                  encoding: URLEncoding.queryString,
                                  headers: CommonUtil.shared.getHeaders())
               
        _request.validate()
        return _request.responseCodable()
    }
    
    
    func version() -> Promise<VersionRes> {
                
        let _path: String = "/m/v1/app/version"
            
        let _query: Parameters = ["osType":"i"]
            
        let _request = AF.request(Const.DOMAIN + _path,
                                      method: .get,
                                      parameters: _query,
                                      encoding: URLEncoding.queryString,
                                      headers: CommonUtil.shared.getHeaders())
                   
        _request.validate()
        return _request.responseCodable()
    }
    
    func userAction(userActionReq: UserActionReq) -> Promise<ResCommon> {
            let _path : String = "/m/v1/userAction"
            
            return AF.request(Const.DOMAIN + _path
                              , method: .post
                              , parameters: userActionReq
                              , encoder: JSONParameterEncoder.default
                              , headers: CommonUtil.shared.getHeaders())
            .validate()
            .responseCodable()
        }
    
    
    func errorReport(errorReportReq: ErrorReportReq, reportType: String) -> Promise<ResCommon> {
        
        var _path : String = "/m/v2/ai/errorReport/problem"
        
        if reportType == "v" {
            _path = "/m/v2/ai/errorReport/video"
        } else {
            _path = "/m/v2/ai/errorReport/problem"
        }
        
                
        return AF.request(Const.DOMAIN + _path
                          , method: .post
                          , parameters: errorReportReq
                          , encoder: JSONParameterEncoder.default
                          , headers: CommonUtil.shared.getHeaders())
        .validate()
        .responseCodable()
    }
    
    
    
    func keyWordList() -> Promise<KeywordRes> {
        
        let _path: String = "/m/v2/keywordList"
            
        let _query: Parameters =  [:]
        
        let _request = AF.request(Const.DOMAIN + _path,
                                             method:.get,
                                             parameters: _query,
                                             encoding: URLEncoding.queryString,
                                             headers: CommonUtil.shared.getHeaders())
            
        _request.validate()
            
        return _request.responseCodable()
    }
  
    func schoolUpdateInfo(userSeq:Int) -> Promise<SchoolUpdateInfo> {
        
        let _path: String = "/m/v1/school/updateInfo"
            
        let _query: Parameters =  ["userSeq":userSeq]
        
        let _request = AF.request(Const.DOMAIN + _path,
                                             method:.get,
                                             parameters: _query,
                                             encoding: URLEncoding.queryString,
                                             headers: CommonUtil.shared.getHeaders())
            
        _request.validate()
            
        return _request.responseCodable()
    }
}
