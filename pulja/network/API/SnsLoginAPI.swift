//
//  SnsLoginAPI.swift
//  pulja
//
//  Created by 김병헌 on 2022/08/30.
//

import Foundation
import Alamofire
import PromiseKit

public class SnsLoginAPI {
    
    static let shared = SnsLoginAPI()
    
    /* */
    func snsEmailCheck(snsUserReq:SnsUserReq) -> Promise<SnsEmailCheckRes> {
        
        let _path: String = "/m/v2/snsEmailCheck"
           
           
        let _body = snsUserReq
           
           
        let _header: HTTPHeaders! = [
           "Accept": "application/json",
           "Content-Type":"application/json; charset=utf-8"
        ]

        let _request = AF.request(Const.DOMAIN + _path,
                                  method:.post,
                                  parameters: _body,
                                  encoder: JSONParameterEncoder.default,
                                  headers: _header)
        _request.validate()
        return _request.responseCodable()
    }
    
    func snsJoin(snsUserReq : SnsUserReq ) -> Promise<SigninRes> {
        
        let _path: String = "/m/v2/snsJoin"
           
        let body  = snsUserReq
           
        return AF.request(Const.DOMAIN + _path,
                          method:.post,
                          parameters: body,
                          encoder: JSONParameterEncoder.default,
                          headers: CommonUtil.shared.getHeaders()).responseCodable()
    }
    
    
    func snsLogin(snsUserReq : SnsUserReq) -> Promise<SigninRes>{
        
        let _path: String = "/m/v2/snsLogin"
        
        let body  = snsUserReq
        
        return AF.request(Const.DOMAIN + _path,
                                  method:.post,
                                  parameters: body,
                                  encoder: JSONParameterEncoder.default,
                                  headers: CommonUtil.shared.getHeaders()).responseCodable()
    }
    
   
    
}
