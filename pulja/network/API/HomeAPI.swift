//
//  HomeAPI.swift
//  pulja
//
//  Created by 김병헌 on 2022/02/08.
//

import Foundation


import Alamofire
import PromiseKit


public class HomeAPI {
    
    static let shared = HomeAPI()
    
    func home() -> Promise<HomeRes> {
        
        let _path: String = "/m/v1/home"

        let _request = AF.request(Const.DOMAIN + _path,
                                  method:.get,
                                  parameters: nil,
                                  encoding: URLEncoding.queryString,
                                  headers: CommonUtil.shared.getHeaders())
        _request.validate()
        return _request.responseCodable()
    }
    
    func notDoneStudyAmount(startDate:String, endDate:String) -> Promise<NotDoneStudyAmountRes> {
        
        let _path: String = "/m/v1/notDoneStudyAmount"
        
        let _query: Parameters = [
            "startDate" : startDate,
            "endDate" : endDate
        ]

        let _request = AF.request(Const.DOMAIN + _path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                                  headers: CommonUtil.shared.getHeaders())
        _request.validate()
        return _request.responseCodable()

    }
    
    func studyAmount(startDate:String, endDate:String) -> Promise<StudyAmountRes> {
        
        let _path: String = "/m/v1/studyAmount"
        
        let _query: Parameters = [
            "startDate" : startDate,
            "endDate" : endDate
        ]

        let _request = AF.request(Const.DOMAIN + _path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                                  headers: CommonUtil.shared.getHeaders())
        _request.validate()
        return _request.responseCodable()
    }
    
    func studyGoal() -> Promise<StudyGoalRes> {
        
        let _path: String = "/m/v1/studyGoal"
        
        let _query: Parameters = [:]

        let _request = AF.request(Const.DOMAIN + _path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                                  headers: CommonUtil.shared.getHeaders())
        _request.validate()
        return _request.responseCodable()
    }
    
    
//    func studyGoal(studyGoal:StudyGoalReq) -> Promise<StudyGoalRes> {
//        
//        var _path: String = "/m/v1/studyGoal"
//        
//        let _request = AF.request(Const.DOMAIN + _path,
//                                  method:.post,
//                                  parameters: StudyGoalReq,
//                                  encoder: JSONParameterEncoder.default,
//                                  headers: CommonUtil.shared.getHeaders())
//        _request.validate()
//        return _request.responseCodable()
//    }
    
    func studySchedule(startDate:String, endDate:String) -> Promise<StudyScheduleRes> {
        
        let _path: String = "/m/v1/studySchedule"
        
        let _query: Parameters = [
            "startDate" : startDate,
            "endDate" : endDate
        ]

        let _request = AF.request(Const.DOMAIN + _path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                                  headers: CommonUtil.shared.getHeaders())
        _request.validate()
        return _request.responseCodable()
    }
    
    
    func todayStudyAmount() -> Promise<TodayStudyAmountRes> {
        
        let _path: String = "/m/v1/todayStudyAmount"
        
        let _query: Parameters = [:]

        let _request = AF.request(Const.DOMAIN + _path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                                  headers: CommonUtil.shared.getHeaders())
        _request.validate()
        return _request.responseCodable()
    }
    
    
    func weekStudyAmount() -> Promise<WeekStudyAmountRes> {
        
        let _path: String = "/m/v1/weekStudyAmount"
        
        let _query: Parameters = [:]

        let _request = AF.request(Const.DOMAIN + _path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                                  headers: CommonUtil.shared.getHeaders())
        _request.validate()
        return _request.responseCodable()
    }
    
    func weekStudySchedule() -> Promise<WeekStudyScheduleRes> {
        
        let _path: String = "/m/v1/weekStudySchedule"
        
        let _query: Parameters = [:]

        let _request = AF.request(Const.DOMAIN + _path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                                  headers: CommonUtil.shared.getHeaders())
        _request.validate()
        return _request.responseCodable()
    }
    
    func yesterdayNotDoneStudyAmount() -> Promise<YesterdayNotDoneStudyAmountRes> {
        
        let _path: String = "/m/v1/yesterdayNotDoneStudyAmount"
        
        let _query: Parameters = [:]

        let _request = AF.request(Const.DOMAIN + _path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                                  headers: CommonUtil.shared.getHeaders())
        _request.validate()
        return _request.responseCodable()
    }
    
    
    func initialView() -> Promise<InitialViewRes> {
        
        let _path: String = "/m/v1/initialView"
        
        let _query: Parameters = [:]

        let _request = AF.request(Const.DOMAIN + _path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                                  headers: CommonUtil.shared.getHeaders())
        _request.validate()
        return _request.responseCodable()
    }
    
    
}
