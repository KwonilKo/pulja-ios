//
//  StudyAPI.swift
//  pulja
//
//  Created by jeonghoonlee on 2021/12/27.
//

import Foundation
import Alamofire
import PromiseKit

public class StudyAPI {
    
    static let shared = StudyAPI()

    //진단평가 응시 정보.
    func diagnosis_test_info(userSeq:Int) -> Promise<DiagnosisHistoryListRes> {

        let _path: String = Const.DOMAIN + "/m/v1/diagnosis_test_info"
        print("경로, \(_path)")


        let _query: Parameters = [
            "userSeq" : userSeq
        ]
        
        let response = AF.request(_path,
                          method:.get,
                          parameters: _query,
                          encoding: URLEncoding.queryString,
                          headers:
                            CommonUtil.shared.getHeaders()).response {
            (res) in
        }
        response.validate()
        return response.responseCodable()

    }
    
    func myCurriculumList(userSeq:Int) -> Promise<CurriculumListRes> {

        let _path: String = Const.DOMAIN + "/m/v1/myCurriculumList"
        print("경로, \(_path)")


        let _query: Parameters = [
            "userSeq" : userSeq
        ]
        
        let response = AF.request(_path,
                          method:.get,
                          parameters: _query,
                          encoding: URLEncoding.queryString,
                          headers:
                            CommonUtil.shared.getHeaders()).response {
            (res) in
        }
        response.validate()
        return response.responseCodable()

    }
    
    func myCurriculumDetail(userSeq:Int, currSeq:Int) -> Promise<CurriculumDetailRes> {

        let _path: String = Const.DOMAIN + "/m/v1/myCurriculumDetail"
        print("경로, \(_path)")

        let _query: Parameters = [
            "userSeq" : userSeq,
            "currSeq" : currSeq
        ]
        
        return AF.request(_path,
                          method:.get,
                          parameters: _query,
                          encoding: URLEncoding.queryString,
                          headers:
                            CommonUtil.shared.getHeaders()).validate().responseCodable()

    }
    
    func myCurriculumNotDoneList(currSeq:Int, userSeq:Int) -> Promise<CurriculumNotDoneListRes> {

        let _path: String = Const.DOMAIN + "/m/v1/myCurriculumNotDoneList"


        let _query: Parameters = [
            "currSeq" : currSeq,
            "userSeq" : userSeq
        ]
        
        return AF.request(_path,
                          method:.get,
                          parameters: _query,
                          encoding: URLEncoding.queryString,
                          headers:
                            CommonUtil.shared.getHeaders()).validate().responseCodable()

    }
    
    func studyMain(userSeq:Int) -> Promise<CurriculumMain> {

        let _path: String = Const.DOMAIN + "/m/v1/studyMain"

        print("스터디메인")
        print(_path)
        let _query: Parameters = [
            "userSeq" : userSeq
        ]
        
        let response = AF.request(_path,
                          method:.get,
                          parameters: _query,
                          encoding: URLEncoding.queryString,
                          headers:
                            CommonUtil.shared.getHeaders()).response {
            (res) in
            print("스터디메인스터디메인")
        }
        response.validate()
        print("벨레디에트 끝남")
        return response.responseCodable()

    }
    
    
    
    func myCurriculumUnitList(userSeq:Int, currSeq:Int) -> Promise<CurriculumPrevListRes> {

        let _path: String = Const.DOMAIN + "/m/v1/myCurriculumUnitList"


        let _query: Parameters = [
            "userSeq" : userSeq,
            "currSeq" : currSeq
        ]
        
        return AF.request(_path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                          headers:
                            CommonUtil.shared.getHeaders()).validate().responseCodable()

    }
    
    func curriculumStudyStep(cunitSeq:Int, currSeq:Int, number: Int, userSeq: Int) -> Promise<StudyStepRes> {

        let _path: String = Const.DOMAIN + "/m/v1/curriculumStudyStep"


        let _query: Parameters = [
            "cunitSeq" : cunitSeq,
            "currSeq" : currSeq,
            "number" : number,
            "userSeq" : userSeq
        ]
        
        return AF.request(_path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                          headers:
                            CommonUtil.shared.getHeaders()).validate().responseCodable()

    }
    
    func curriculumStudyStepInitial(cunitSeq:Int, currSeq:Int, userSeq: Int) -> Promise<StudyStepRes> {

        let _path: String = Const.DOMAIN + "/m/v1/curriculumStudyStep"


        let _query: Parameters = [
            "cunitSeq" : cunitSeq,
            "currSeq" : currSeq,
            "userSeq" : userSeq
        ]
        
        return AF.request(_path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                          headers:
                            CommonUtil.shared.getHeaders()).validate().responseCodable()

    }
    
    
    func curriculumStudyVideo(cunitSeq:Int, userSeq: Int) -> Promise<StudyVideoRes> {

        let _path: String = Const.DOMAIN + "/m/v1/curriculumStudyVideo"


        let _query: Parameters = [
            "cunitSeq" : cunitSeq,
            "userSeq" : userSeq
        ]
        
        return AF.request(_path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                          headers:
                            CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    func curriculumStudyAnswer(cunitSeq:Int, currSeq: Int, duration: Double, questionId: Int, userAnswer: String, userSeq: Int) -> Promise<StudyStepRes> {

        let _path: String = "/m/v1/curriculumStudyAnswer"
        let _body : Parameters = ["cunitSeq" : cunitSeq, "currSeq" : currSeq, "duration": duration, "questionId" : questionId, "userAnswer" : userAnswer, "userSeq" : userSeq]
        print("포스트 path: \(_path)")
        return AF.request(Const.DOMAIN + _path, method: .post, parameters: _body,
                          encoding: JSONEncoding.default, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    func coachingTestDetail(userSeq : Int) -> Promise<CoachingTestDetailRes> {
        let _path: String = Const.DOMAIN + "/m/v1/coachingTestDetail"

        let _query: Parameters = [
            "userSeq" : userSeq
        ]
        
        return AF.request(_path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                          headers:
                            CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    func coachingTestStepPrev(number: Int, studyDay: String, userSeq : Int) -> Promise<CoachingTestRes> {
        let _path: String = Const.DOMAIN + "/m/v1/coachingTestStep"

        let _query: Parameters = [
            "number" : number,
            "studyDay" : studyDay,
            "userSeq" : userSeq
        ]
        
        return AF.request(_path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                          headers:
                            CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    
    func coachingTestStep(studyDay: String, userSeq : Int) -> Promise<CoachingTestRes> {
        let _path: String = Const.DOMAIN + "/m/v1/coachingTestStep"

        let _query: Parameters = [
            "studyDay" : studyDay,
            "userSeq" : userSeq
        ]
        
        return AF.request(_path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                          headers:
                            CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    func coachingTestAnswer(addSeq:Int, duration: Double, number: Int, questionId: Int, studyDay: String, userAnswer: String, userSeq: Int) -> Promise<CoachingTestRes> {

        let _path: String = "/m/v1/coachingTestAnswer"
        let _body : Parameters = ["addSeq" : addSeq, "duration" : duration, "number": number, "questionId" : questionId, "studyDay" : studyDay, "userAnswer": userAnswer, "userSeq" : userSeq]
        print("포스트 path: \(_path)")
        return AF.request(Const.DOMAIN + _path, method: .post, parameters: _body,
                          encoding: JSONEncoding.default, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    func coachingTestResult(studyDay : String, userSeq : Int) -> Promise<CurriculumStudyRes> {
        let _path: String = Const.DOMAIN + "/m/v1/coachingTestResult"

        let _query: Parameters = [
            "studyDay" : studyDay,
            "userSeq" : userSeq
        ]
        
        return AF.request(_path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                          headers:
                            CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    
    func curriculumStudyResult(cunitSeq : Int, currSeq : Int, studyDate: String, userSeq : Int) -> Promise<CurriculumStudyRes> {
        let _path: String = Const.DOMAIN + "/m/v1/curriculumStudyResult"

        let _query: Parameters = [
            "cunitSeq" : cunitSeq,
            "currSeq" : currSeq,
            "studyDate" : studyDate,
            "userSeq" : userSeq
        ]
        
        return AF.request(_path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                          headers:
                            CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    func diagnosisStep1(diagnosisSeq:Int, goalGrade: Int, schoolGrade: String, userSeq: Int) -> Promise<DiagnosisSurveryRes> {

        let _path: String = "/m/v1/diagnosisStep1"
        let _body : Parameters = ["diagnosisSeq" : diagnosisSeq, "goalGrade" : goalGrade, "schoolGrade": schoolGrade, "userSeq" : userSeq]
        print("포스트 path: \(_path)")
        return AF.request(Const.DOMAIN + _path, method: .post, parameters: _body,
                          encoding: JSONEncoding.default, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    
    func diagnosisProgress(diagnosisSeq : Int, nodeSeq : Int, number: Int, stepCount : Int, testSeq : Int, answer: String, duration: Double, questionId: Int) -> Promise<DiagnosisProbRes> {
        let _path: String = "/m/v1/diagnosisProgress"

        let _body: Parameters = [
            "diagnosisSeq" : diagnosisSeq,
            "nodeSeq" : nodeSeq,
            "number" : number,
            "stepCount" : stepCount,
            "testSeq" : testSeq,
            "userAnswer" : [
                "answer" : answer,
                "duration" : duration,
                "questionId" : questionId
            ]
        ]
        
        return AF.request(Const.DOMAIN + _path, method: .post, parameters: _body,
                          encoding: JSONEncoding.default, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    
    func diagnosisStart(diagnosisSeq : Int, nodeSeq : Int, number: Int, stepCount : Int, testSeq : Int) -> Promise<DiagnosisProbRes> {
        let _path: String = Const.DOMAIN + "/m/v1/diagnosisStart"

        let _query: Parameters = [
            "diagnosisSeq" : diagnosisSeq,
            "nodeSeq" : nodeSeq,
            "number" : number,
            "stepCount" : stepCount,
            "testSeq" : testSeq
        ]
        
        return AF.request(_path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                          headers:
                            CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    
    func diagnosisFinishResult() -> Promise<DiagnosisFinishRes> {
        let _path: String = "/m/v1/diagnosis_finish_result"
//        let _body : Parameters = [:]
        
        return AF.request(Const.DOMAIN + _path, method: .post, parameters: nil,
                          encoding: URLEncoding.default, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    func diagnosisReport(userSeq : Int, diagnosisSeq: Int) -> Promise<DiagnosisFinishRes> {
        let _path: String = "/m/v1/diagnosis_report"
        let _query : Parameters = ["diagnosisSeq" : diagnosisSeq, "userSeq": userSeq]
        
        return AF.request(Const.DOMAIN + _path, method: .get, parameters: _query,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    func diagnosisReportFree(userSeq : Int) -> Promise<DiagnosisFinishRes> {
        let _path: String = "/m/v1/diagnosis_report"
        let _query : Parameters = ["userSeq": userSeq]
        
        return AF.request(Const.DOMAIN + _path, method: .get, parameters: _query,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    
    func questions(currSeq:Int, cunitSeq:Int, isCorrect:String, userSeq:Int64 ) -> Promise<QuestionsRes> {
        
        
        let _path: String = "/m/v1/curriculum/result/questions"
        let _query : Parameters = [
            "userSeq" : userSeq,
            "cunitSeq" : cunitSeq,
            "currSeq" : currSeq,
            "isCorrect" : isCorrect
        ]
        
        return AF.request(Const.DOMAIN + _path, method: .get, parameters: _query,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    
    func testQuestions(studyDay:String, isCorrect:String, userSeq:Int64 ) -> Promise<QuestionsRes> {
            
            
            let _path: String = "/m/v1/curriculum/testResult/questions"
            let _query : Parameters = [
                "userSeq" : userSeq,
                "studyDay" : studyDay,
                "isCorrect" : isCorrect
            ]
            
            return AF.request(Const.DOMAIN + _path, method: .get, parameters: _query,
                              encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        }
    
    func weeklyReport(userSeq: Int) -> Promise<WeeklyReportRes> {
        let _path: String = "/m/v1/weeklyReport"
        let _body : Parameters = ["userSeq" : userSeq]
        
        return AF.request(Const.DOMAIN + _path, method: .post, parameters: _body,
                          encoding: URLEncoding.default, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    
    func makeCurriculum(userSeq:Int64, currSeq:Int) -> Promise<ResCommon> {

        let coachSeq = 8372
        let _path: String = "/m/v1/make_curriculum"
        let _body : Parameters = [
            "userSeq" : userSeq,
            "currSeq" : currSeq,
            "coachSeq" : coachSeq
        ]
        
        return AF.request(Const.DOMAIN + _path, method: .get, parameters: _body,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
        
    }
    
    func studyProblemResult(userSeq:Int, cunitSeq:Int) -> Promise<StudyProblemResultRes>{
        
        let _path: String = "/m/v1/curriculumUnit/problem/result"
                   let _query : Parameters = [
                       "userSeq" : userSeq,
                       "cunitSeq" : cunitSeq
                   ]
        return AF.request(Const.DOMAIN + _path, method: .get, parameters: _query,
                                     encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
    }
    
    
}

