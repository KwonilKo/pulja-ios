//
//  ChatBotAPI.swift
//  pulja
//
//  Created by samdols on 2022/01/30.
//

import Foundation
import Alamofire
import PromiseKit

public class ChatBotAPI {
    
    static let shared = ChatBotAPI()
    
    /* 메세지 목록 가져오기 */
    func messages(userSeq : Int64 , type : String , messageSeq : Int = 0)-> Promise<MessageRes> {

        let _path: String = "/m/v1/bot/messages"

        let _query: Parameters = [
            "type" : type,
            "userSeq" : userSeq,
            "messageSeq" : messageSeq
        ]

        let _request = AF.request(Const.DOMAIN + _path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                                  headers: CommonUtil.shared.getHeaders())
        _request.validate()
        return _request.responseCodable()
    }
    
    /* 학습시간 등록 여부 가져오기 */
    func studyTime(userSeq : Int64)-> Promise<StudyTimeRes> {

        let _path: String = "/m/v1/bot/studyTime"

        let _query: Parameters = [
            "userSeq" : userSeq
        ]

        let _request = AF.request(Const.DOMAIN + _path,
                                  method:.get,
                                  parameters: _query,
                                  encoding: URLEncoding.queryString,
                                  headers: CommonUtil.shared.getHeaders())
        _request.validate()
        return _request.responseCodable()
    }
    
    func sendMessage(req : ChatBotReq, image : Data?) -> Promise<MessageRes> {
        
        let _path: String = "/m/v1/bot/message"
        
        
        return Promise { seal in

            
            AF.upload(
                multipartFormData: { multipartFormData in
                    
                    if let _customData = req.customData
                    {
                        multipartFormData.append(_customData.data(using: .utf8)!, withName: "customData")
                    }
                    if let _customType = req.customType
                    {
                        multipartFormData.append(_customType.data(using: .utf8)!, withName: "customType")
                    }
                    
                    if let _lastSeq = req.lastSeq , _lastSeq.count > 0
                    {
                        multipartFormData.append(_lastSeq.data(using: .utf8)!, withName: "lastSeq")
                    }
                    if let _message = req.message , _message.count > 0
                    {
                        multipartFormData.append(_message.data(using: .utf8)!, withName: "message")
                    }
                    if let _messageType = req.messageType
                    {
                        multipartFormData.append(_messageType.data(using: .utf8)!, withName: "messageType")
                    }
                    if let _userSeq = req.userSeq
                    {
                        multipartFormData.append(_userSeq.data(using: .utf8)!, withName: "userSeq")
                    }
                    
                    
                    if let imageData = image {
                        let fileName =  String(format: "%@.jpeg", CommonUtil.shared.getCurrentTime())
                        multipartFormData.append(imageData, withName: "file", fileName: fileName ,mimeType: "image/jpeg")
                    }
                    

                },
                to: Const.DOMAIN + _path ,
                method:.post,  headers: CommonUtil.shared.getMutipartHeaders()
            ).responseData() { response in
                
                switch response.result {
                
                    
                    
                    case .success(let value):
                    
                        let stCode = response.response?.statusCode
                        
                        if  stCode! < 300 {
                            let decoder = JSONDecoder()
                            do {
                                seal.fulfill(try decoder.decode(MessageRes.self, from: value))
                            } catch {
                                if Const.isProd == false {
                                    print("decode error::\(error)")
                                }
                                seal.reject(error)
                            }
                            
                        } else {
                            let decoder = JSONDecoder()
                            if let errorRsp = try? decoder.decode(ResCommon.self, from: value) {
                                
                                let code = errorRsp.statusCode ?? stCode!
                                let message = errorRsp.message ?? "Undefined Error"
                                let error = NSError(domain:"", code: code, userInfo:[ NSLocalizedDescriptionKey: message])
                                seal.reject(error)
                                
                            } else {
                                
                                let error = NSError(domain:"", code:stCode!, userInfo:[ NSLocalizedDescriptionKey: "Undefined Error" ])
                                seal.reject(error)
                            }

                        }
                        
                    case .failure(_):
                        let stCode = response.response?.statusCode
                        let decoder = JSONDecoder()
                        if let data = response.data, let errorRsp = try? decoder.decode(ResCommon.self, from: data ) {

                            let code = errorRsp.statusCode ?? stCode!
                            let message = errorRsp.message ?? "Undefined Error"
                            let error = NSError(domain:"", code: code, userInfo:[ NSLocalizedDescriptionKey: message])
                            seal.reject(error)

                        } else {

                            let error = NSError(domain:"", code:stCode ?? 999, userInfo:[ NSLocalizedDescriptionKey: "Undefined Error" ])
                            seal.reject(error)
                        }
                }
            }
           
        }
    }
    
}
