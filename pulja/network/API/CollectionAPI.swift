//
//  CollectionAPI.swift
//  pulja
//
//  Created by HUN on 2023/01/11.
//

import Foundation
import Alamofire
import PromiseKit

public class CollectionAPI {
    static let shared = CollectionAPI()
    
    func collectionDetail(collectionSeq: Int, userSeq: Int) -> Promise<CollectionDetailRes> {
        let _path : String = Const.DOMAIN + "/m/v2/collectionDetail"
        let _body : Parameters = [
            "collectionSeq" : collectionSeq,
            "userSeq" : userSeq
        ]
        
        return AF.request(_path, method: .get, parameters: _body,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    func collectionList(schoolTypeText: String, subjectSeq: Int?, userSeq: Int) -> Promise<CollectionListRes> {
        let _path : String = Const.DOMAIN + "/m/v2/collectionList"
        
        var _body: Parameters
        
        if let subject = subjectSeq {
            _body = [
                "schoolTypeText" : schoolTypeText,
                "subjectSeq" : subject,
                "userSeq" : userSeq
            ]
        } else {
            _body = [
                "schoolTypeText" : schoolTypeText,
                "userSeq" : userSeq
            ]
        }
        
        
        return AF.request(_path, method: .get, parameters: _body,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    func collectionSubject(userSeq: Int) -> Promise<CollectionSubjectRes> {
        let _path : String = Const.DOMAIN + "/m/v2/collectionSubject"
        let _body : Parameters = [
            "userSeq" : userSeq
        ]
        
        return AF.request(_path, method: .get, parameters: _body,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    func collectionStudyStart(cameFrom:String, collectionSeq:Int, userSeq: Int) -> Promise<CollectionStudyStartRes> {
           
       let _path : String = Const.DOMAIN + "/m/v2/collection/studyStart"
        
        let enc = URLEncoding(arrayEncoding: .noBrackets)
        
        let _query: Parameters = [
                "cameFrom" : cameFrom,
                "collectionSeq" : collectionSeq,
                "userSeq" : userSeq
        ]
       
       return AF.request(_path, method: .get, parameters: _query,
                         encoding: enc, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    func collectionStudyStep(duration: Double, problemSeq: Int, questionId: Int, userAnswer: String, userSeq: Int) -> Promise<CollectionStudyStartRes> {
        let _path : String = Const.DOMAIN + "/m/v2/collection/studyStep"
        let _body : Parameters = [
            "duration" : duration,
            "problemSeq" : problemSeq,
            "questionId" : questionId,
            "userAnswer" : userAnswer,
            "userSeq" : userSeq
        ]
        
        
        return AF.request(_path, method: .post, parameters: _body,
                          encoding: JSONEncoding.default, headers: CommonUtil.shared.getHeaders()) { $0.timeoutInterval = 10 }
        .validate().responseCodable()
    }
    
    func collectionStudyResult(problemSeq: Int) -> Promise<AIStudyResultRes> {
        let _path : String = Const.DOMAIN + "/m/v2/collection/studyResult"
        let _body : Parameters = [
            "problemSeq" : problemSeq
        ]
        
        return AF.request(_path, method: .get, parameters: _body,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    func collectionToday(userSeq: Int) -> Promise<CollectionTodayRes> {
        let _path : String = Const.DOMAIN + "/m/v2/collection/today"
        let _body : Parameters = [
            "userSeq" : userSeq
        ]
        
        return AF.request(_path, method: .get, parameters: _body,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
}
