//
//  CoachAPI.swift
//  pulja
//
//  Created by kwonilko on 2022/03/19.
//

import Foundation
import Alamofire
import PromiseKit
import SwiftyJSON


public class CoachAPI {
    
    static let shared = CoachAPI()
    
    
    func getCoachStudents(coachSeq: Int) -> Promise<StudentsScheduleRes> {
        
        let _path : String = "/m/v1/coach/students"
        let _body : Parameters = ["coachSeq" : coachSeq]
        let _request = AF.request(Const.DOMAIN + _path, method: .get, parameters: _body,
                                  encoding: URLEncoding.queryString,
                                         headers: CommonUtil.shared.getHeaders())
        
        _request.validate()
        return _request.responseCodable()
    
    }
    
    
    func getCoachHome(coachSeq: Int, endDate : String, startDate : String) -> Promise<CoachingHomeRes> {
        
        let _path : String = "/m/v1/coach/coaching"
        let _body : Parameters = ["coachSeq" : coachSeq, "endDate" : endDate, "startDate" : startDate]
        
        let _request = AF.request(Const.DOMAIN + _path, method: .get, parameters: _body,
                   encoding: URLEncoding.queryString,
                          headers: CommonUtil.shared.getHeaders())
        _request.validate()
        return _request.responseCodable()
    }
    
    
    func getStudyImage(coachSeq: Int, page: Int, size: Int, userSeq: Int) -> Promise<StudyPhotoRes> {
        let _path : String = "/m/v1/coach/studyImage"
       
        let sort = ["startTime,ASC", "studySeq,ASC"]
        
        let enc = URLEncoding(arrayEncoding: .noBrackets)
        
        let _body : Parameters = ["coachSeq" : coachSeq, "page" : page, "size" : size,
                                  "sort" : sort, "userSeq" : userSeq]
        
        let _request = AF.request(Const.DOMAIN + _path, method: .get, parameters: _body,
                                  encoding: enc, headers: CommonUtil.shared.getHeaders())
        _request.validate()
        return _request.responseCodable()
        
    }
    
    
}
