//
//  ChatAPI.swift
//  pulja
//
//  Created by 김병헌 on 2021/12/03.
//

import Foundation
import Alamofire
import PromiseKit

public class ChatAPI {
    
    static let shared = ChatAPI()
    
    /* */
    func chatUsers()-> Promise<ChatUserRes> {

        let _path: String = "/m/v1/chatUsers"


        let _request = AF.request(Const.DOMAIN + _path,
                                  method:.get,
                                  parameters: nil,
                                  headers: CommonUtil.shared.getHeaders())
        _request.validate()
        return _request.responseCodable()
    }
    
    
    
}
