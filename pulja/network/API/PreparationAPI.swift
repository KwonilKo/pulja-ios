//
//  PreparationAPI.swift
//  pulja
//
//  Created by kwonilko on 2022/08/02.
//

import Foundation
import Alamofire
import PromiseKit


public class PreparationAPI {
    
    static let shared = PreparationAPI()
    
    
    func selfStudy() -> Promise<SelfStudyRes> {
        
        let _path : String = Const.DOMAIN + "/m/v2/ai/selfStudy"
        
        return AF.request(_path, method: .get, parameters: nil,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
    }
    
    
    func aiStudyResult(problemSeq: Int) -> Promise<AIStudyRes> {
        let _path : String = Const.DOMAIN + "/m/v2/ai/studyResult"
        let _body : Parameters = [
            "problemSeq" : problemSeq
        ]
        
        return AF.request(_path, method: .get, parameters: _body,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
    }
    
    func weakVideoReaction(problemSeq: Int, reaction: Int, vlecSeq: Int) -> Promise<WeakVideoReactionRes> {
        
        let _path : String = Const.DOMAIN + "/m/v2/ai/weakVideoReaction"
        let _body : Parameters = [
            "problemSeq" : problemSeq,
            "reaction" : reaction,
            "vlecSeq" : vlecSeq
 
        ]
        
        return AF.request(_path, method: .get, parameters: _body,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
    }
    
    func studyStartWithUnit2(unit2Seq : Int) -> Promise<AIRecommendStepRes> {
        
        let _path : String = Const.DOMAIN + "/m/v2/ai/studyStart"
        let _body : Parameters = [
            "unit2Seq" : unit2Seq
        ]
        
        return AF.request(_path, method: .get, parameters: _body,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
    }
    
    
    func studyStart() -> Promise<AIRecommendStepRes> {
        
        let _path : String = Const.DOMAIN + "/m/v2/ai/studyStart"
        
        return AF.request(_path, method: .get, parameters: nil,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
    }
    
    func studyStep(problemSeq: Int, duration: Double, questionId: Int, userAnswer: String) -> Promise<AIRecommendNextRes> {
        let _path : String = Const.DOMAIN + "/m/v2/ai/studyStep"
        let _body : Parameters = [
            "duration" : duration,
            "problemSeq" : problemSeq,
            "questionId" : questionId,
            "userAnswer" : userAnswer
        ]
        
        
        return AF.request(_path, method: .post, parameters: _body,
                          encoding: JSONEncoding.default, headers: CommonUtil.shared.getHeaders()) { $0.timeoutInterval = 10 }
        .validate().responseCodable()
        
    }
    
    
    func weakCategoryResultQuestions(isCorrect: String, problemSeq : Int, resultType: String) -> Promise<QuestionsRes> {
        let _path : String = Const.DOMAIN + "/m/v2/weakCategory/result/questions"
        
        let _body : Parameters = [
            "isCorrect" : isCorrect,
            "problemSeq" : problemSeq,
            "resultType" : resultType
        ]
        
        
        return AF.request(_path, method: .get, parameters: _body,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
    }
    func weakCategoryStudyResult(problemSeq : Int) -> Promise<CurriculumStudyRes> {
        let _path : String = Const.DOMAIN + "/m/v2/weakCategoryStudyResult"
        let _body : Parameters = [
            "problemSeq" : problemSeq
        ]
        
        
        return AF.request(_path, method: .get, parameters: _body,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
    }
    
    
    func weakCategoryStudyInfo(problemSeq : Int) -> Promise<WeakCategoryStudyInfoRes> {
        let _path : String = Const.DOMAIN + "/m/v2/weakCategoryStudyInfo"
        let _body : Parameters = [
            "problemSeq" : problemSeq
        ]
        
        
        return AF.request(_path, method: .get, parameters: _body,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
    }
    
    
    func weakCategoryStudyStepInit(problemSeq : Int) -> Promise<AIRecommendStepRes> {
        let _path : String = Const.DOMAIN + "/m/v2/weakCategoryStudyStep"
        let _body : Parameters = [
            "problemSeq" : problemSeq
        ]
        
        
        return AF.request(_path, method: .get, parameters: _body,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
    }
    
    func weakCategoryStudyStep(problemSeq : Int, number: Int) -> Promise<AIRecommendStepRes> {
        let _path : String = Const.DOMAIN + "/m/v2/weakCategoryStudyStep"
        let _body : Parameters = [
            "problemSeq" : problemSeq,
            "number" : number
        ]
        
        
        return AF.request(_path, method: .get, parameters: _body,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
    }
    
    
    func weakCategoryStudyAnswer(problemSeq: Int, duration: Double, questionId: Int, userAnswer: String) -> Promise<AIRecommendStepRes> {
        let _path : String = Const.DOMAIN + "/m/v2/weakCategoryStudyAnswer"
        let _body : Parameters = [
            "duration" : duration,
            "problemSeq" : problemSeq,
            "questionId" : questionId,
            "userAnswer" : userAnswer
        ]
        
        
        return AF.request(_path, method: .post, parameters: _body,
                          encoding: JSONEncoding.default, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
    }
    
    
    
    
    func weakCategoryStudyVideo(problemSeq: Int) -> Promise<WeakCategoryStudyVideoRes> {
        
        let _path : String = "/m/v2/weakCategoryStudyVideo"
        let _body : Parameters = ["problemSeq" : problemSeq]
        
        
        return AF.request(Const.DOMAIN + _path, method: .get, parameters: _body,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
        
    }
    
    func diagnosisFinishResult(problemSeq: Int) -> Promise<AIDiagnosisFinishRes> {
        
        let _path : String = "/m/v2/ai/diagnosisFinishResult"
        let _body : Parameters = ["problemSeq" : problemSeq]
        
        
        return AF.request(Const.DOMAIN + _path, method: .post, parameters: _body,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
    }

    func myReport() -> Promise<MyReportRes> {

        let _path: String = Const.DOMAIN + "/m/v2/selfStudy/myReport"
        
        return AF.request(_path, method: .get, parameters: nil,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()

    }
    
    func myReportDetail(subjectSeq: Int) -> Promise<MyReportDetailRes> {
        
        let _path : String = Const.DOMAIN + "/m/v2/selfStudy/myReport/" + String(subjectSeq)
//        let _query : Parameters = [
//            "subjectSeq" : subjectSeq
//        ]
        
        let response = AF.request(_path,
                          method:.get,
                          parameters: nil,
                          encoding: URLEncoding.queryString,
                          headers:
                            CommonUtil.shared.getHeaders()).response {
            (res) in
        }
        response.validate()
        return response.responseCodable()
        
        
    }
    
    func todayStudy() -> Promise<TodayStudyRes> {
        
        let _path : String = Const.DOMAIN + "/m/v2/selfStudy/todayStudy"
        return AF.request(_path, method: .get, parameters: nil,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
    }
    
    func diagnosisStep1(unit2Seq: Int) -> Promise<DiagnosisStep1Res>
    {
        
        let _path : String = "/m/v2/ai/diagnosisStep1"
        let _body : Parameters = ["unit2Seq" : unit2Seq]
        
        
        return AF.request(Const.DOMAIN + _path, method: .get, parameters: _body,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    func diagnosisStart(problemSeq : Int) -> Promise<AIRecommendStepRes> {
        let _path : String = Const.DOMAIN + "/m/v2/ai/diagnosisStart"
        let _body : Parameters = [
            "problemSeq" : problemSeq
        ]
        
        
        return AF.request(_path, method: .get, parameters: _body,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
    }
    
    
    func diagnosisStudyAnswer(problemSeq: Int, duration: Double, questionId: Int, userAnswer: String, userSeq: Int) -> Promise<AIRecommendStepRes> {
        let _path : String = Const.DOMAIN + "/m/v2/ai/diagnosisStudyAnswer"
        let _body : Parameters = [
            "duration" : duration,
            "problemSeq" : problemSeq,
            "questionId" : questionId,
            "userAnswer" : userAnswer,
            "userSeq" : userSeq
        ]
        
        
        return AF.request(_path, method: .post, parameters: _body,
                          encoding: JSONEncoding.default, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
    }
    
    
    
    
    
    
}
