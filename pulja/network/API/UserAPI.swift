//
//  UserAPI.swift
//  pulja
//
//  Created by 고권일 on 2021/12/31.
//

import Foundation
import Alamofire
import PromiseKit
import SwiftyJSON

public class UserAPI {
    
    static let shared = UserAPI()
    
    
    func schoolList(schoolname : String) -> Promise<SchoolRes> {
        let _path : String = "/m/v1/user/schoolList"
        let _body : Parameters = ["schoolName" : schoolname]
        
        let response = AF.request(Const.DOMAIN + _path, method: .get, parameters: _body,
                   encoding: URLEncoding.queryString,
                          headers: CommonUtil.shared.getHeaders()).response {
            (res) in
            
        }
        response.validate()
        return response.responseCodable()
        
        
    }
    
    func changeInfo(phonenumber: String, birthday: String, schoolId: String, schoolName: String, schoolNum: Int,
                    schoolType: String, parentsPhone: String, user : User?) -> Promise<UserRes> {
        let _path : String = "/m/v1/user/changeInfo"
        var new_user : User? = user
        var _body : Parameters
        
        // 5개의 input중 입력받은 값만 변경
        if phonenumber != "" {  //내 전화번호 업데이트
            new_user?.phonenumber = phonenumber
        } else if birthday != "" {  //생년월일 업데이트
            new_user?.birthday = birthday
        } else if (schoolId != "" || schoolName != "" || schoolType != "" )  {   //학교, 학년 업데이트
            new_user?.schoolId = schoolId
            new_user?.schoolName = schoolName
            new_user?.schoolNum = schoolNum
            new_user?.schoolType = schoolType
            
        } else {    //부모님 전화번호 업데이트
            new_user?.parentsPhone = parentsPhone
        }
                    
        return AF.request(Const.DOMAIN + _path, method: .post, parameters: new_user,
                          encoder: JSONParameterEncoder.default, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    func changeSchoolInfo( schoolNum: Int, schoolType: String, mockTestGrade: Int, user : User?) -> Promise<UserRes> {
           let _path : String = "/m/v1/user/changeInfo"
           var new_user : User? = user
           var _body : Parameters
           
            new_user?.schoolNum = schoolNum
            new_user?.schoolType = schoolType
            new_user?.mockTestGrade = mockTestGrade
               
                       
           return AF.request(Const.DOMAIN + _path, method: .post, parameters: new_user,
                             encoder: JSONParameterEncoder.default, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
       }
    
    func changePassword(newPassword: String, oldPassword: String) -> Promise<ResCommon> {
        let _path : String = "/m/v1/user/changePassword"
        let _body : Parameters = ["newPassword" : newPassword, "oldPassword" : oldPassword]
        
        return AF.request(Const.DOMAIN + _path, method: .post, parameters: _body,
                          encoding: JSONEncoding.default, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    func myInfo()-> Promise<UserRes> {
        
        let _path: String = "/m/v1/user/myInfo"
            
        let response =
        AF.request(Const.DOMAIN + _path, method: .get, parameters: nil,
                   encoding: URLEncoding.default,
                   headers: CommonUtil.shared.getHeaders()).responseJSON { (rep)
            in
            
            
        }
        response.validate()
        return response.responseCodable()
    }
    
    func myInfo(userSeq: Int)-> Promise<UserRes> {
        
        let _path: String = "/m/v1/user/\(userSeq)"
        //let _body : Parameters = ["userSeq" : userSeq]
            
        let response =
        AF.request(Const.DOMAIN + _path, method: .get, parameters: nil,
                   encoding: URLEncoding.default,
                   headers: CommonUtil.shared.getHeaders()).response {
            (res) in
        }
        response.validate()
        return response.responseCodable()
    }
    
    
    func userNotification(userSeq : Int) -> Promise<UserNotificationRes> {
        let _path : String = "/m/v1/user/userNotification"
        let _body : Parameters = ["userSeq" : userSeq]
        
        let response = AF.request(Const.DOMAIN + _path, method: .get, parameters: _body,
                   encoding: URLEncoding.queryString,
                          headers: CommonUtil.shared.getHeaders()).response {
            (res) in
            
        }
        response.validate()
        return response.responseCodable()
        
    }
    
    func userNotificationUpdate(userSeq: Int, chatting: String, marketing: String, notice : String, study: String) -> Promise<UserNotificationRes> {
        let _path : String = "/m/v1/user/userNotification/"
        let _body: Parameters
        if chatting != "" {
            _body = ["chatting" : chatting, "userSeq": userSeq]
        } else if marketing != "" {
            _body = ["marketing" : marketing, "userSeq": userSeq]
        } else if notice != "" {
            _body = ["notice" : notice, "userSeq": userSeq]
        } else {
            _body = ["study" : study, "userSeq": userSeq]
        }
                
        return AF.request(Const.DOMAIN + _path, method: .post, parameters: _body,
                          encoding: JSONEncoding.default, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    
    
    func dropOut(dropOutReason: String, userSeq: Int) -> Promise<ResCommon> {
        let _path : String = "/m/v1/user/dropOut"
        let _body : Parameters = ["dropOutReason" : dropOutReason, "userSeq" : userSeq]
        
        return AF.request(Const.DOMAIN + _path, method: .post, parameters: _body,
                          encoding: JSONEncoding.default, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    
    func event_seven_free_info_req(schoolNum : Int , wantUnit : Int) -> Promise<ResCommon> {
        let _path : String = "/m/v1/event_seven_free_info_req"
        let _body : Parameters = ["schoolNum" : schoolNum, "wantUnit": wantUnit]
        
        return AF.request(Const.DOMAIN + _path, method: .post, parameters: _body,
                          encoding: JSONEncoding.default, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
        
    }
    
    func is_available_seven_req(userSeq : Int) -> Promise<ResCommon> {
        let _path : String = "/m/v1/is_available_seven_req"
        let _body : Parameters = ["userSeq" : userSeq]
        
        let response = AF.request(Const.DOMAIN + _path, method: .get, parameters: _body,
                   encoding: URLEncoding.queryString,
                          headers: CommonUtil.shared.getHeaders()).response {
            (res) in
            
        }
        response.validate()
        return response.responseCodable()
        
    }
    
    // 7일 체험 취소.
    func cancel_event_seven_free(userSeq : Int) -> Promise<ResCommon> {
        let _path : String = "/m/v1/cancel_event_seven_free"
        let _body : Parameters = ["userSeq" : userSeq]
        
        let response = AF.request(Const.DOMAIN + _path, method: .post, parameters: _body,
                   encoding: URLEncoding.queryString,
                          headers: CommonUtil.shared.getHeaders()).response {
            (res) in
            
        }
        response.validate()
        return response.responseCodable()
        
    }
    
    
    /**
            탈퇴 가능 여부 확인.
     */
    func dropOutCheckValidReq(userSeq : Int) -> Promise<DropoutCheckValidRes> {
        let _path : String = "/m/v1/dropout/checkValid"
        let _body : Parameters = ["userSeq" : userSeq]
        
        let response = AF.request(Const.DOMAIN + _path, method: .get, parameters: _body,
                   encoding: URLEncoding.queryString,
                          headers: CommonUtil.shared.getHeaders()).response {
            (res) in
            
        }
        response.validate()
        return response.responseCodable()
        
    }
    
    //탈퇴 못하는 회원 신청 로그.
    func makeEnquireEventLog() -> Promise<ResCommon> {
        let _path : String = "/m/v1/dropout/makeEnquireEventLog"
        
        let response = AF.request(Const.DOMAIN + _path, method: .post, parameters: nil,
                   encoding: URLEncoding.queryString,
                          headers: CommonUtil.shared.getHeaders()).response {
            (res) in
            
        }
        response.validate()
        return response.responseCodable()
        
    }
        
    
    
}
