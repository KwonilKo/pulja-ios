//
//  CurationAPI.swift
//  pulja
//
//  Created by 김병헌 on 2022/10/26.
//

import Foundation
import Alamofire
import PromiseKit


public class CurationAPI {
    
    static let shared = CurationAPI()
    
    
    
    func recentUnit2() -> Promise<RecommendUnitRes> {
        
        let _path : String = Const.DOMAIN + "/m/v2/recentUnit2Seq"
        
        return AF.request(_path, method: .get, parameters: nil,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
    }
    
    
    func recommendUnit2() -> Promise<RecommendUnitRes> {
            
        let _path : String = Const.DOMAIN + "/m/v2/recommendUnit2Seq"
        
        return AF.request(_path, method: .get, parameters: nil,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
    }
    
    
    
    func unit2List() -> Promise<Unit2ListRes> {
                
            let _path : String = Const.DOMAIN + "/m/v2/unit2List"
            
            return AF.request(_path, method: .get, parameters: nil,
                              encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
            
        }
    
    
    func studyStart(keywordSeqs:[Int], unit2Seqs:[Int], isContinuingStudy: Bool, cameFrom:String ) -> Promise<AIRecommendStepRes> {
           
       let _path : String = Const.DOMAIN + "/m/v2/ai3/studyStart"
        
        let enc = URLEncoding(arrayEncoding: .noBrackets)
        
        let _query: Parameters = [
                    "keywordSeqList" : keywordSeqs,
                    "unit2SeqList" : unit2Seqs,
                    "isContinuingStudy" : isContinuingStudy,
                    "cameFrom" : cameFrom,
                    "ver" : 2
                ]
       
       return AF.request(_path, method: .get, parameters: _query,
                         encoding: enc, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
       
   }
       
   func studyStep(problemSeq: Int, duration: Double, questionId: Int, userAnswer: String) -> Promise<AIRecommendNextRes> {
       let _path : String = Const.DOMAIN + "/m/v2/ai3/studyStep"
       let _body : Parameters = [
           "duration" : duration,
           "problemSeq" : problemSeq,
           "questionId" : questionId,
           "userAnswer" : userAnswer,
           "ver" : 2
       ]
       
       
       return AF.request(_path, method: .post, parameters: _body,
                         encoding: JSONEncoding.default, headers: CommonUtil.shared.getHeaders()) { $0.timeoutInterval = 10 }
       .validate().responseCodable()
       
   }
    
    
    
    
    func ai3StudyResult(problemSeq: Int) -> Promise<AIStudyResultRes> {
        let _path : String = Const.DOMAIN + "/m/v2/ai3/studyResult"
        let _body : Parameters = [
            "problemSeq" : problemSeq
        ]
        
        return AF.request(_path, method: .get, parameters: _body,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
        
    }
    
    
       
    func keywordFeedBack(feedbackMeasure: Int, problemSeq: Int, feedbackText: String) -> Promise<ResCommon> {
        let _path : String = Const.DOMAIN + "/m/v2/ai3/keywordFeedback"
        let _body : Parameters = [
            "feedbackMeasure" : feedbackMeasure,
            "problemSeq" : problemSeq,
            "feedbackText" : feedbackText
        ]
        
        return AF.request(_path, method: .post, parameters: _body,
                          encoding: JSONEncoding.default, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    
    func keywordFeedBackShow(problemSeq: Int) -> Promise<ResCommon> {
        let _path : String = Const.DOMAIN + "/m/v2/ai3/keywordFeedback/show"
        let _body : Parameters = [
            "problemSeq" : problemSeq
        ]
        
        return AF.request(_path, method: .post, parameters: _body,
                          encoding: JSONEncoding.default, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    func studyInfo(userSeq: Int) -> Promise<StudyInfoRes> {
        let _path : String = Const.DOMAIN + "/m/v2/ai3/studyInfo"
        let _body : Parameters = [
            "userSeq" : userSeq
        ]
        
        return AF.request(_path, method: .get, parameters: _body,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
    
    func studyHistory(userSeq: Int) -> Promise<StudyHistoryRes> {
        let _path : String = Const.DOMAIN + "/m/v2/ai3/studyHistory"
        let _body : Parameters = [
            "userSeq" : userSeq
        ]
        
        return AF.request(_path, method: .get, parameters: _body,
                          encoding: URLEncoding.queryString, headers: CommonUtil.shared.getHeaders()).validate().responseCodable()
    }
}
