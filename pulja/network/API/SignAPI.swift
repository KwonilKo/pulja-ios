//
//  SignAPI.swift
//  pulja
//
//  Created by 김병헌 on 2021/11/22.
//

import Foundation
import Alamofire
import PromiseKit

public class SignAPI {
    
    static let shared = SignAPI()
    
    /* */
    func signin(id:String, pw:String, fcmToken:String, uuid:String)-> Promise<SigninRes> {
        
        let _path: String = "/m/v1/signin"
           
           
        let _body = SigninReq(fcmToken:fcmToken, osType:"i", password:pw, userId: id, uuid:uuid);
           
           
        let _header: HTTPHeaders! = [
           "Accept": "application/json",
           "Content-Type":"application/json; charset=utf-8"
        ]

        let _request = AF.request(Const.DOMAIN + _path,
                                  method:.post,
                                  parameters: _body,
                                  encoder: JSONParameterEncoder.default,
                                  headers: _header)
        _request.validate()
        return _request.responseCodable()
    }
    
    func tokenRefresh(refresh : String ) -> Promise<TokenRes> {
        
        let _path: String = "/m/v1/refreshToken"
           
        let body  = TokenReq(accessToken: refresh, refreshToken: refresh, userSeq: 0)
           
        return AF.request(Const.DOMAIN + _path,
                          method:.post,
                          parameters: body,
                          encoder: JSONParameterEncoder.default,
                          headers: CommonUtil.shared.getHeaders()).responseCodable()
    }
    
    
    func authCodeReq(authCode:AuthCodeReq) -> Promise<ResCommon>{
        
        let _path: String = "/m/v1/authcode_request"
        
        return AF.request(Const.DOMAIN + _path,
                                  method:.post,
                                  parameters: authCode,
                                  encoder: JSONParameterEncoder.default,
                                  headers: CommonUtil.shared.getHeaders()).responseCodable()
    }
    
    func certPhone(authCode:AuthCodeReq) -> Promise<ResCommon>{
        let _path: String = "/m/v1/join/phone_cert"
        return AF.request(Const.DOMAIN + _path,
                                  method:.post,
                                  parameters: authCode,
                                  encoder: JSONParameterEncoder.default,
                                  headers: CommonUtil.shared.getHeaders()).responseCodable()

    }
    
    
    func validId(userId:String) -> Promise<ResCommon>{
            
        let _path: String = "/m/v1/validid"
        let _query: Parameters = [
            "userId" : userId
        ]
            
        return AF.request(Const.DOMAIN + _path,
                          method:.get,
                          parameters: _query,
                          encoding: URLEncoding.queryString,
                          headers:
                                CommonUtil.shared.getHeaders()).responseCodable()
        
    }
    
    func signup(signupReq:SignupReq)-> Promise<SigninRes> {
           
           let _path: String = "/m/v1/signup"
              
           let _header: HTTPHeaders! = [
              "Accept": "application/json",
              "Content-Type":"application/json; charset=utf-8"
           ]

           let _request = AF.request(Const.DOMAIN + _path,
                                     method:.post,
                                     parameters: signupReq,
                                     encoder: JSONParameterEncoder.default,
                                     headers: _header)
           _request.validate()
           return _request.responseCodable()
       
    }
    
    
    func fcmToken(fcmTokenReq:FcmTokenReq)-> Promise<ResCommon> {
        
        let _path: String = "/m/v1/fcmToken"
                
        var sendMethod: HTTPMethod = .post
        
        let _request = AF.request(Const.DOMAIN + _path,
                                  method: sendMethod,
                                  parameters: fcmTokenReq,
                                  encoder: JSONParameterEncoder.default,
                                  headers: CommonUtil.shared.getHeaders())
               
        _request.validate()
        return _request.responseCodable()
    }
    
    
    func restore(id:String, pw:String)-> Promise<ResCommon> {
            
        let _path: String = "/m/v1/dormant/restore"
        let _body = SigninReq(fcmToken:nil, osType:"i", password:pw, userId: id, uuid:nil);
        let _header: HTTPHeaders! = [
           "Accept": "application/json",
           "Content-Type":"application/json; charset=utf-8"
        ]
        
        let _request = AF.request(Const.DOMAIN + _path,
                                  method:.post,
                                  parameters: _body,
                                  encoder: JSONParameterEncoder.default,
                                  headers: _header)
        _request.validate()
        return _request.responseCodable()
    }
    
    func naverLogin(tokenType:String, accessToken:String)-> Promise<response_naver_login> {
        
        let _url: String = "https://openapi.naver.com/v1/nid/me"
        let authorization = "\(tokenType) \(accessToken)"
        
        let _header: HTTPHeaders! = [
           "Authorization": authorization
        ]

        let _request = AF.request(_url,
                                  method:.get,
                                  encoding: JSONEncoding.default,
                                  headers: _header)
        _request.validate()
        return _request.responseCodable()
    }
    
}
