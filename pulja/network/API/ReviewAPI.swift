//
//  ReviewAPI.swift
//  pulja
//
//  Created by 김병헌 on 2022/01/18.
//

import Foundation


import Foundation
import Alamofire
import PromiseKit

public class ReviewAPI {
    
    static let shared = ReviewAPI()
    
    
    func questions(reviewReq : ReviewQuestionReq, listType : String) -> Promise<ReviewQuestionRes> {
        
        
        var _path: String = "/m/v1/review/questions"
        
        if listType == "R" {
            _path = "/m/v1/review/questions"
        } else {
            _path = "/m/v1/review/bookmark/questions"
        }
        
        
        let _query: Parameters = reviewReq.dictionary!

        let _request = AF.request(Const.DOMAIN + _path,
                                         method:.get,
                                         parameters: _query,
                                         encoding: URLEncoding.queryString,
                                         headers: CommonUtil.shared.getHeaders())
               
        _request.validate()
               
        return _request.responseCodable()
        
    }
    
   
    
    func bookMark(bookMarkReq: BookMarkReq) -> Promise<BookMarkRes> {
        
        let _path: String = "/m/v1/review/bookmark"
        
        var sendMethod: HTTPMethod = .post
        
        if bookMarkReq.bookMarkSeq != nil {
            sendMethod = .delete
        }
        
        let _request = AF.request(Const.DOMAIN + _path,
                                  method: sendMethod,
                                  parameters: bookMarkReq,
                                  encoder: JSONParameterEncoder.default,
                                  headers: CommonUtil.shared.getHeaders())
               
        _request.validate()
        return _request.responseCodable()
    }
    
   
    
    
    
    
}
