//
//  AlamofireExtension.swift

import Foundation
import Alamofire
import PromiseKit

extension Alamofire.DataRequest {
    
    public func responseData() -> Promise<Data> {
        
        return Promise { seal in
            
            if Const.isProd == false {
                responseString(
                    encoding : .utf8,
                    completionHandler: { response in
                    
                    print("responseData request Info: "+(response.request?.description)!)
                    print("responseData Response Info: "+response.description)
                })
            }
            
                responseData() { response in
                
                    switch response.result {
                    
                        case .success( _):
                        
                            let stCode = response.response?.statusCode
                            let statusCode = String(describing: response.response?.statusCode)
                            if  stCode! < 300 {
                                print("ok status code : " +  statusCode)
                                switch response.result {
                                case .success(let value):
                                    
                                    seal.fulfill(value)
                                case .failure(let error):
                                    seal.reject(error)
                                }
                                
                            } else {
                                switch response.result {
                                case .success(let value):

                                    let str = String(data: value, encoding: .utf8) ?? ""
                                    let error = NSError(domain:"", code:stCode!, userInfo:[ NSLocalizedDescriptionKey: str])
                                    seal.reject(error)
                                case .failure( _):
                                    let error = NSError(domain:"", code:stCode!, userInfo:[ NSLocalizedDescriptionKey: CommonUtil.statusDesc[stCode!] ?? "Undefined Error"])
                                    seal.reject(error)
                                }
                                
                                print("status code : " +  statusCode)

                            }
                            
                        case .failure(let error):
                            
                            seal.reject(error)
                    }

                
            }
        }
    }
   
    // Return a Promise for a Codable
    public func responseCodable<T: Codable>() -> Promise<T> {
        
        return Promise { seal in
            
            if Const.isProd == false {
                responseString(
                    encoding : .utf8,
                    completionHandler: { response in
                    
                    print("responseCodable request Info: "+(response.request?.description)!)
                    print("responseCodable Response Info: "+response.description)
                })
            }
            
            responseData() { response in
                let stCode = response.response?.statusCode
                
//                let statusCode = String(describing: response.response?.statusCode)
                
                switch response.result {
                    case .success(let value):
                    
                        if  stCode! < 300 {
                            let decoder = JSONDecoder()
                            do {
                                seal.fulfill(try decoder.decode(T.self, from: value))
                            } catch {
                                if Const.isProd == false {
                                    print("decode error::\(error)")
//                                    print ("decode error::\(error.localizedDescription)")
                                }
                                seal.reject(error)
                            }
                        
                        } else { //400,401,403,500  failure에 걸리지 않고 성공한 경우에는 error message를 전달
                            let decoder = JSONDecoder()
                            if let errorRsp = try? decoder.decode(ResCommon.self, from: value) {
                                
                                let code = errorRsp.statusCode ?? stCode!
                                let message = errorRsp.message ?? "Undefined Error"
                                let error = NSError(domain:"", code: code, userInfo:[ NSLocalizedDescriptionKey: message])
                                seal.reject(error)
                                
                            } else {
                                
                                let error = NSError(domain:"", code:stCode!, userInfo:[ NSLocalizedDescriptionKey: "Undefined Error" ])
                                seal.reject(error)
                            }
                        }
                        
                    case .failure(let error):
                    
                        if stCode == 401{
                            //token refresh를 하고 기존 리퀘스트를 다시 날림.
                            if let refresh = CommonUtil.shared.getRefreshToken() {
                                   
                                SignAPI.shared.tokenRefresh(refresh: refresh).done { res->Void in
                                       
                                    if let accestoken = res.data?.accessToken, let refreshToken = res.data?.refreshToken, let request = response.request {
                                               
                                            CommonUtil.shared.setAccessToken(token: accestoken)
                                            CommonUtil.shared.setRefreshToken(token: refreshToken)
                                            CommonUtil.shared.setHeader()

                                               var originalRequest: URLRequest?
                                               do {
                                                   originalRequest = try request.asURLRequest()
                                                   let authorization = "" + accestoken
                                                   originalRequest?.setValue(authorization, forHTTPHeaderField: "Authorization")
                                                   
                                               } catch {
                                                   let error = NSError(domain:"", code:stCode!, userInfo:[ NSLocalizedDescriptionKey: CommonUtil.statusDesc[999] ?? "Undefined Error"])
                                                   seal.reject(error)
                                               }
                                               
                                               if let newRequest = originalRequest {
                                                   CommonAPI().retryRequest(request: newRequest, type: T.self).done{res->Void in
                                                       
                                                       let decoder = JSONDecoder()
                                                       if let data : Data = res as? Data {
    //                                                        print(String(data: data, encoding: .utf8))
                                                           do {
                                                               seal.fulfill(try decoder.decode(T.self, from: data))
                                                           } catch {
                                                               seal.reject(error)
                                                           }
                                                       } else {
                                                           let error = NSError(domain:"", code:999, userInfo:[ NSLocalizedDescriptionKey: CommonUtil.statusDesc[999] ?? "Undefined Error"])
                                                           seal.reject(error)
                                                       }
                                                       
                                                    }.catch{ error in
                                                        seal.reject(error)
                                                    }
                                               } else {
                                                   //////error
                                               }

                                           } else {
                                               //refresh토큰으로 accesstoken을 못가져온 경우 인트로로 보내야한다. (저장된 access , refresh token 모두 삭제 후 인트로로 이동)
                                               let error = NSError(domain:"", code:999, userInfo:[ NSLocalizedDescriptionKey: CommonUtil.statusDesc[999] ?? "Undefined Error"])
                                               seal.reject(error)
                                           }
                                       
                                       }.catch{ error in
                                           
                                           //refresh토큰으로 accesstoken을 못가져온 경우 인트로로 보내야한다. (저장된 access , refresh token 모두 삭제 후 인트로로 이동)
                                           let error = NSError(domain:"", code:999, userInfo:[ NSLocalizedDescriptionKey: CommonUtil.statusDesc[999] ?? "Undefined Error"])
                                           seal.reject(error)
                                       }
                                   
                               } else {
                                   
                                   let error = NSError(domain:"", code:stCode!, userInfo:[ NSLocalizedDescriptionKey: CommonUtil.statusDesc[998] ?? "Undefined Error"])
                                   seal.reject(error)
                                   
                               }
                        } else {
                            
                            
                            
                            let decoder = JSONDecoder()
                            if let data = response.data, let errorRsp = try? decoder.decode(ResCommon.self, from: data ) {

                                let code = errorRsp.statusCode ?? stCode!
                                let message = errorRsp.message ?? "Undefined Error"
                                let error = NSError(domain:"", code: code, userInfo:[ NSLocalizedDescriptionKey: message])
                                seal.reject(error)

                            } else {

                                let error = NSError(domain:"", code:stCode ?? 999, userInfo:[ NSLocalizedDescriptionKey: "Undefined Error" ])
                                seal.reject(error)
                            }
//                            seal.reject(error)
                        }
                        
                    
                }
            }
        }
    }
    
    
    
    public func responseCodableWithType<T: Codable>(_ type : T.Type) -> Promise<Any> {
            
            return Promise { seal in
                
                if Const.isProd == false {
                    responseString(
                        encoding : .utf8,
                        completionHandler: { response in
                        
                        print("responseCodableWithType request Info: "+(response.request?.description)!)
                        print("responseCodableWithType Response Info: "+response.description)
                    })
                }
    
                responseData() { response in
                    
                    switch response.result {
                        case .success(let value):
                        
                        let stCode = response.response?.statusCode
                          let statusCode = String(describing: response.response?.statusCode)
                          if  stCode! < 300 {
                              print("ok status code : " +  statusCode)
                              switch response.result {
                              case .success(let value):
                                  let decoder = JSONDecoder()
                                  //print(String(data: value, encoding: .utf8))
                                  do {
      //                                fulfill(try decoder.decode(T.self, from: value))
                                      seal.fulfill(value)
                                  } catch {
                                      seal.reject(error)
                                  }
                              case .failure(let error):
                                  seal.reject(error)
                              }
                              
                          } else {
                              switch response.result {
                                  
                              case .success(let value):
                                 
                                      let decoder = JSONDecoder()
                                      //print(String(data: value, encoding: .utf8))
                                      do {
                                          seal.fulfill(try decoder.decode(T.self, from: value))
                                      } catch {
                                          let error = NSError(domain:"", code:stCode!, userInfo:[ NSLocalizedDescriptionKey: CommonUtil.statusDesc[stCode!] ?? "Undefined Error"])
                                          seal.reject(error)
                                      }
                                  
                              case .failure(let e):
                                 
                                let error = NSError(domain:"", code:stCode!, userInfo:[ NSLocalizedDescriptionKey: CommonUtil.statusDesc[stCode!] ?? "Undefined Error"])
                                seal.reject(error)
                                  
                              }
                              
                              print("status code : " +  statusCode)

                          }
                        
                            if  stCode! < 300 {
                                let decoder = JSONDecoder()
                                do {
                                    seal.fulfill(try decoder.decode(T.self, from: value))
                                } catch {
                                    seal.reject(error)
                                }
                            
                            } else  { //400,401,403,500  failure에 걸리지 않고 성공한 경우에는 error message를 전달
                                let decoder = JSONDecoder()
                                if let errorRsp = try? decoder.decode(ResCommon.self, from: value) {
                                    
                                    let code = errorRsp.statusCode ?? stCode!
                                    let message = errorRsp.message ?? "Undefined Error"
                                    let error = NSError(domain:"", code: code, userInfo:[ NSLocalizedDescriptionKey: message])
                                    seal.reject(error)
                                } else {
                                    let error = NSError(domain:"", code:stCode!, userInfo:[ NSLocalizedDescriptionKey: "Undefined Error" ])
                                    seal.reject(error)
                                }
                                
                            }
                            
                        case .failure(let error):
                            seal.reject(error)
                    }
    
                    //timeout
                    
                   
                    
                }
            }
        }
}
    
